﻿namespace SFMTK.Modules.VersionControl
{
    /// <summary>
    /// Enables base version control features for <see cref="SFMTK.Contents.Content"/>, such as allowing for Git integration.
    /// <para/>While this feature is available for free, it can be extended with a paid module (e.g. reformatting sessions to diff-able data).
    /// </summary>
    public class VersionControlModule : IModule
    {
        //NYI: VersionControlModule - SFMTK.Modules.VersionControl.Git and SFMTK.Modules.Paid.VersionControl.Git

        public ModuleLoadingContext GetLoadingContext() => ModuleLoadingContext.StartupCommandLine;

        public void OnStartup(ModuleLoadingContext context)
        {
        }

        public bool OnLoad(ModuleLoadingContext context)
        {
            return true;
        }

        public bool OnUnload(ModuleUnloadingContext context)
        {
            return true;
        }

        public string[] GetDependencies() => null;

        public string GetParent() => null;
    }
}
