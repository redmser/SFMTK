﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SFMTK.Data;

namespace SFMTK.Contents.Tests
{
    [TestClass]
    public class ModelTests
    {
        [TestInitialize()]
        public void InitializeTests()
        {
            SFM.BasePath = "C:/ArbitraryPath/SourceFilmmaker/";

            this.TestModel = Activator.CreateInstance<ModelContent>();
            this.TestModel.FakeFile = true;
            this.TestModel.FilePath = "C:/ArbitraryPath/SourceFilmmaker/game/testmod/models/UnsavedContent.mdl";
        }

        public ModelContent TestModel { get; set; }

        [TestMethod()]
        public void MatchesPathTest()
        {
            Assert.IsTrue(this.TestModel.MatchesPath("C:/ArbitraryPath/SourceFilmmaker/game/testmod/models/UnsavedContent.mdl"));
            Assert.IsTrue(this.TestModel.MatchesPath("AnotherContent.dx80.vtx"));
            Assert.IsFalse(this.TestModel.MatchesPath("BarelyRight.vtx"));
            Assert.IsFalse(this.TestModel.MatchesPath("OtherType.vtf"));
        }

        [TestMethod()]
        public void MatchesContentsTest()
        {
            //NYI: ModelTests - true and false content matching files for materials and models
            Assert.Fail();
        }

        [TestMethod()]
        public void ErrorListTest()
        {
            //NYI: ModelTests - create a QC with an error/warning/info and see if error list contains it
            Assert.Fail();
        }

        [TestMethod()]
        public void LoadTest()
        {
            //NYI: ContentTests - test I/O using pre-made file and temporary file checks (could test dependencies here as well)
            Assert.Fail();
        }

        [TestMethod()]
        public void SaveTest()
        {
            Assert.Fail();
        }
    }
}
