﻿using System;
using SFMTK.Controls.ContentWidgets;
using SFMTK.Converters;

namespace SFMTK.Contents.Tests
{
    public class TestContent : Content
    {
        public override string MainExtension => ".test";

        protected override ContentWidget CreateWidgetInternal() => new ContentWidget { Content = this };
        public override DefaultContentConverter DefaultConverter => throw new NotImplementedException();
    }
}
