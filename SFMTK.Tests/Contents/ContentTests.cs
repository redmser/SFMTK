﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SFMTK.Properties;
using WeifenLuo.WinFormsUI.Docking;

namespace SFMTK.Contents.Tests
{
    [TestClass()]
    public class ContentTests
    {
        [TestInitialize()]
        public void InitializeTests()
        {
            SFM.BasePath = "C:/ArbitraryPath/SourceFilmmaker/";

            this.TestContent = Activator.CreateInstance<TestContent>();
            this.TestContent.FakeFile = true;
            this.TestContent.IsDirty = true;
            this.TestContent.FilePath = "C:/ArbitraryPath/SourceFilmmaker/game/testmod/models/UnsavedContent.test";

            Assert.IsNotNull(this.TestContent);
        }

        public TestContent TestContent { get; set; }

        [TestMethod()]
        public void GetFormattedNameTest()
        {
            Assert.AreEqual("C:/ArbitraryPath/SourceFilmmaker/game/testmod/models/UnsavedContent.test", this.TestContent.GetFormattedName(TitleFormat.FullPath, false));
            Assert.AreEqual("C:/ArbitraryPath/SourceFilmmaker/game/testmod/models/UnsavedContent.test*", this.TestContent.GetFormattedName(TitleFormat.FullPath, true));

            Assert.AreEqual("UnsavedContent.test", this.TestContent.GetFormattedName(TitleFormat.FileName, false));
            Assert.AreEqual("UnsavedContent.test*", this.TestContent.GetFormattedName(TitleFormat.FileName, true));

            Assert.AreEqual("testmod/models/UnsavedContent.test", this.TestContent.GetFormattedName(TitleFormat.LocalPath, false));
            Assert.AreEqual("testmod/models/UnsavedContent.test*", this.TestContent.GetFormattedName(TitleFormat.LocalPath, true));

            Assert.AreEqual("models/UnsavedContent.test", this.TestContent.GetFormattedName(TitleFormat.LocalPathWithoutMod, false));
            Assert.AreEqual("models/UnsavedContent.test*", this.TestContent.GetFormattedName(TitleFormat.LocalPathWithoutMod, true));

            this.TestContent.FilePath = null;
            Assert.IsNull(this.TestContent.GetFormattedName(TitleFormat.FileName, true));
        }

        [TestMethod()]
        public void FilePathFormattingTest()
        {
            this.TestContent.FakeFile = false;
            this.TestContent.FilePath = @"testmod/models/NewContent";
            Assert.AreEqual("testmod/models/NewContent.test", this.TestContent.FilePath);
        }

        [TestMethod()]
        public void GetNewNameTest()
        {
            var dpm = new DockPanelManager(new DockPanel(), null);
            Assert.AreEqual("Test1.test", dpm.GetNewContentName(this.TestContent));

            //TODO: ContentTests - add to dockpanel and test if naming scheme continues correctly
        }

        [TestMethod()]
        public void DependsOnTest()
        {
            //NYI: ContentTests - setup test content instance with dependencies
            Assert.Fail();
        }

        [TestMethod()]
        public void DependencyOfTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetDependenciesTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void CreateWidgetTest()
        {
            var testwidget = this.TestContent.CreateWidget();
            Assert.IsNotNull(testwidget);
            Assert.AreEqual(testwidget.Content, this.TestContent);
        }

        [TestMethod()]
        public void GetNewInstanceTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ContentByTypeTest()
        {
            Assert.IsInstanceOfType(Content.ContentByType(typeof(TestContent)), typeof(TestContent));
        }

        [TestMethod()]
        public void DefaultTypeNameTest()
        {
            Assert.Fail();
        }
    }
}