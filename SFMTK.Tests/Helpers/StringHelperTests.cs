﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SFMTK.Tests
{
    [TestClass()]
    public class StringHelperTests
    {
        [TestMethod()]
        public void GetPluralTest()
        {
            const string singular = "index";
            const string plural = "indices";

            //Regular plural
            Assert.AreEqual("5 indices", StringHelper.GetPlural(5, singular, plural, true));

            //Without space
            Assert.AreEqual("-2indices", StringHelper.GetPlural(-2, singular, plural, false));

            //Automatic plural
            Assert.AreEqual("0 indexs", StringHelper.GetPlural(0, singular, null, true));

            //Singular form
            Assert.AreEqual("1 index", StringHelper.GetPlural(1, singular, plural, true));
        }

        [TestMethod()]
        public void ContainsIgnoreCaseTest()
        {
            const string test = "FindThiS";

            //Base cases
            Assert.IsTrue(test.ContainsIgnoreCase("DTH"));
            Assert.IsFalse(test.ContainsIgnoreCase("Ignore"));

            //Null cases
            Assert.IsTrue(((string)null).ContainsIgnoreCase(null));
            Assert.IsFalse(((string)null).ContainsIgnoreCase(test));
            Assert.IsFalse(test.ContainsIgnoreCase(null));
        }

        [TestMethod()]
        public void FirstLetterToUpperCaseTest()
        {
            //Base cases
            Assert.AreEqual("LowerCase", "lowerCase".FirstLetterToUpperCase());
            Assert.AreEqual("LowerCase", "LowerCase".FirstLetterToUpperCase());

            //Null case, throws
            Assert.ThrowsException<ArgumentNullException>(() => ((string)null).FirstLetterToUpperCase());
        }

        [TestMethod()]
        public void GetNewUniqueNameTest()
        {
            //Base cases
            Assert.AreEqual("PRE2SUF", StringHelper.GetNewUniqueName("PRE", "SUF", new[] { "PRE1SUF" }));
            Assert.AreEqual("PRE4SUF", StringHelper.GetNewUniqueName("PRE", "SUF", new[] { "PRE0SUF", "PRE3SUF" }));

            //Empty and null
            Assert.AreEqual("1", StringHelper.GetNewUniqueName("", "", new string[] { }));
            Assert.AreEqual("1", StringHelper.GetNewUniqueName(null, null, new string[] { }));
            Assert.AreEqual("2", StringHelper.GetNewUniqueName("", "", new[] { "1" }));
            Assert.AreEqual("2", StringHelper.GetNewUniqueName(null, null, new[] { "1" }));
            Assert.AreEqual("1", StringHelper.GetNewUniqueName(null, null, null));
        }

        [TestMethod()]
        public void IntToLettersTest()
        {
            //Base cases
            Assert.AreEqual("A", StringHelper.IntToLetters(0, 1));
            Assert.AreEqual("C", StringHelper.IntToLetters(2, 1));
            Assert.AreEqual("Z", StringHelper.IntToLetters(25, 1));

            //Overflow and multiple digits
            Assert.AreEqual("AAAA", StringHelper.IntToLetters(0, 4));
            Assert.AreEqual("AAAF", StringHelper.IntToLetters(5, 4));
            Assert.AreEqual("BA", StringHelper.IntToLetters(26, 1));
            Assert.AreEqual("ACE", StringHelper.IntToLetters(56, 3)); // (0 * 26^2) + (2 * 26^1) + (4 * 26^0) = 56

            //Stupid cases
            Assert.AreEqual("", StringHelper.IntToLetters(0, 0));
            Assert.AreEqual("D", StringHelper.IntToLetters(-3, 1));
        }
    }
}