﻿using System;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SFMTK.Tests
{
    [TestClass()]
    public class IOHelperTests
    {
        [TestMethod()]
        public void GetStandardizedPathTest()
        {
            //Base case: filename only
            const string filename = "test.ok";
            const string nameonly = "test";
            Assert.AreEqual(filename, IOHelper.GetStandardizedPath(filename));
            Assert.AreEqual(nameonly, IOHelper.GetStandardizedPath(nameonly));

            //Base case: null and empty
            Assert.IsNull(IOHelper.GetStandardizedPath(null));
            Assert.AreEqual(string.Empty, IOHelper.GetStandardizedPath(string.Empty));

            //Backslashes to forwardslashes
            const string filepath = @"\path\to\a\file\test.ok";
            const string mixedpath = @"C:/test\to/a\path.txt";
            Assert.AreEqual(@"/path/to/a/file/test.ok", IOHelper.GetStandardizedPath(filepath));
            Assert.AreEqual(@"C:/test/to/a/path.txt", IOHelper.GetStandardizedPath(mixedpath));

            //Path to directory - ends with slash
            const string dirpath = @"E:\path\to\a\dir";
            const string dirends = @"E:\another\dir\path\";
            Assert.AreEqual(@"E:/path/to/a/dir/", IOHelper.GetStandardizedPath(dirpath));
            Assert.AreEqual(@"E:/another/dir/path/", IOHelper.GetStandardizedPath(dirends));
        }

        [TestMethod()]
        public void PathsEqualTest()
        {
            //Base case: null and empty
            Assert.IsTrue(IOHelper.PathsEqual(null, null));
            Assert.IsTrue(IOHelper.PathsEqual(string.Empty, string.Empty));

            //Different capitalization, watching case
            Assert.IsFalse(IOHelper.PathsEqual(@"path\to\file", @"PATH\TO\FILE", false));
            Assert.IsTrue(IOHelper.PathsEqual(@"path\to\file", @"path/to/file", false));

            //Ignoring case
            Assert.IsTrue(IOHelper.PathsEqual(@"path\to\file", @"PATH\TO\FILE", true));
            Assert.IsTrue(IOHelper.PathsEqual(@"path\to\file", @"path/to/file", true));
        }

        [TestMethod()]
        public void GetWindowsPathTest()
        {
            const string inputStd = @"C:/stand/ardized/test.abc";
            const string inputWin = @"\other\kind\of\path.def";

            //Conversion to windows path
            Assert.AreEqual(@"C:\stand\ardized\test.abc", IOHelper.GetWindowsPath(inputStd));
            Assert.AreEqual(inputWin, IOHelper.GetWindowsPath(inputWin));

            //Base case: null and empty
            Assert.AreEqual(string.Empty, IOHelper.GetWindowsPath(string.Empty));
            Assert.IsNull(IOHelper.GetWindowsPath(null));
        }

        [TestMethod()]
        public void GetAssociatedProgramTest()
        {
            var assoc1 = IOHelper.GetAssociatedProgram("txt");
            var assoc2 = IOHelper.GetAssociatedProgram(".txt");

            //Association should be set for txt files
            Assert.IsNotNull(assoc1);
            Assert.IsNotNull(assoc2);

            //Same association
            Assert.AreEqual(assoc1, assoc2);

            //Unknown associations return null
            Assert.IsNull(IOHelper.GetAssociatedProgram("thiswillfail"));
            Assert.IsNull(IOHelper.GetAssociatedProgram("thiswillfailtoo", true));

            //Invalid parameter
            Assert.ThrowsException<ArgumentNullException>(() => IOHelper.GetAssociatedProgram(null));
        }

        [TestMethod()]
        public void MakeValidFileNameTest()
        {
            const string unchanged  = "validname.bmp";
            const string willchange = @"invalid""\name.abc";

            Assert.AreEqual(unchanged, IOHelper.MakeValidFileName(unchanged));
            Assert.AreEqual(@"invalid__name.abc", IOHelper.MakeValidFileName(willchange));
            Assert.AreEqual(@"invalidTTname.abc", IOHelper.MakeValidFileName(willchange, 'T'));
            Assert.AreEqual(@"invalidname.abc", IOHelper.MakeValidFileName(willchange, null));
            Assert.ThrowsException<ArgumentException>(() => IOHelper.MakeValidFileName(willchange, '"'));
        }

        [TestMethod()]
        public void FixPathCaseTest()
        {
            //Path to file
            var path = Path.GetTempFileName();
            var pathedit = path.ToUpperInvariant();
            Assert.AreEqual(path, IOHelper.FixPathCase(pathedit));

            //Path to directory
            var dir = Path.GetTempPath();
            var diredit = dir.ToLowerInvariant();
            Assert.AreEqual(dir, IOHelper.FixPathCase(diredit));

            //Standardized path
            var std = dir.Replace('\\', '/');
            var stdedit = std.ToUpperInvariant();
            Assert.AreEqual(dir, IOHelper.FixPathCase(stdedit));

            //Invalid values
            Assert.IsNull(IOHelper.FixPathCase(null));
            Assert.ThrowsException<DirectoryNotFoundException>(() => IOHelper.FixPathCase("/INV/ALID P/ATH//"));
        }

        public static string BinaryFilePath => typeof(SFMTests).Assembly.Location;
        public static Encoding BinaryFileEncoding => Encoding.Default;
        public static string TextFilePath => @"C:/Users/desktop.ini";
        public static Encoding TextFileEncoding => Encoding.Unicode;

        [TestMethod()]
        public void ReadFileIfStringTest()
        {
            //Try reading a binary file
            Assert.IsNull(IOHelper.ReadFileIfString(BinaryFilePath, BinaryFileEncoding));

            //Reads contents of a non-binary file
            Assert.AreNotEqual(0, IOHelper.ReadFileIfString(TextFilePath, TextFileEncoding).Length);

            //Invalid parameters
            Assert.ThrowsException<ArgumentNullException>(() => IOHelper.ReadFileIfString(null));
            Assert.ThrowsException<FileNotFoundException>(() => IOHelper.ReadFileIfString(string.Empty));
            Assert.ThrowsException<FileNotFoundException>(() => IOHelper.ReadFileIfString("THIS IS INVALID"));
        }

        [TestMethod()]
        public void IsBinaryFileTest()
        {
            //Check if this assembly file is binary
            Assert.IsTrue(IOHelper.IsBinaryFile(BinaryFilePath, BinaryFileEncoding));

            //Check if default text file is non-binary
            Assert.IsFalse(IOHelper.IsBinaryFile(TextFilePath, TextFileEncoding));

            //Reading length is longer than file
            Assert.IsFalse(IOHelper.IsBinaryFile(TextFilePath, TextFileEncoding, int.MaxValue - 1));

            //Invalid parameters
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => IOHelper.IsBinaryFile(TextFilePath, TextFileEncoding, 0));
            Assert.ThrowsException<ArgumentNullException>(() => IOHelper.IsBinaryFile(null));
            Assert.ThrowsException<FileNotFoundException>(() => IOHelper.IsBinaryFile(string.Empty));
            Assert.ThrowsException<FileNotFoundException>(() => IOHelper.IsBinaryFile("THIS IS INVALID"));
        }
    }
}