﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SFMTK.Tests
{
    [TestClass]
    public class MathExTests
    {
        [TestMethod]
        public void ConsoleLinesNeededTest()
        {
            Assert.AreEqual(1, MathEx.ConsoleLinesNeeded("test", 8));
            Assert.AreEqual(2, MathEx.ConsoleLinesNeeded("test", 3));
            Assert.AreEqual(2, MathEx.ConsoleLinesNeeded("te\r\nst", 3));
            Assert.AreEqual(4, MathEx.ConsoleLinesNeeded("test\r\ntest", 3));
            Assert.AreEqual(1, MathEx.ConsoleLinesNeeded("", 5));
            Assert.AreEqual(4, MathEx.ConsoleLinesNeeded("reallongreallong", 4));
        }
    }
}
