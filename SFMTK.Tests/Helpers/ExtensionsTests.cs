﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Numerics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SFMTK.Tests
{
    [TestClass()]
    public class ExtensionsTests
    {
        [TestMethod()]
        public void ImageResizeTest()
        {
            //Generate an image
            var resultImage = new Bitmap(4, 4, PixelFormat.Format24bppRgb);
            using (Graphics grp = Graphics.FromImage(resultImage))
            {
                grp.FillRectangle(Brushes.White, 0, 0, 4, 4);
            }
            Assert.AreEqual(4, resultImage.Width);
            Assert.AreEqual(4, resultImage.Height);

            //Resize the image
            resultImage = resultImage.Resize(12, 16);
            Assert.AreEqual(12, resultImage.Width);
            Assert.AreEqual(16, resultImage.Height);

            resultImage = resultImage.Resize(1, 55);
            Assert.AreEqual(1, resultImage.Width);
            Assert.AreEqual(55, resultImage.Height);

            //Invalid resizes
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => resultImage.Resize(0, 3));
            Assert.ThrowsException<ArgumentNullException>(() => ((Image)null).Resize(9, 3));
        }

        [TestMethod()]
        public void VectorToColorTest()
        {
            //Base cases
            EqualColors(Color.FromArgb(0, 0, 0), new Vector3(0, 0, 0).ToColor(false));
            EqualColors(Color.FromArgb(0, 0, 0), new Vector3(0, 0, 0).ToColor(true));
            EqualColors(Color.FromArgb(255, 255, 255), new Vector3(255, 255, 255).ToColor(false));
            EqualColors(Color.FromArgb(255, 255, 255), new Vector3(1, 1, 1).ToColor(true));


            //Error checking
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Vector3(-5, 0, 0).ToColor(false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Vector3(0, 0, 300).ToColor(false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Vector3(-2, 0, 0).ToColor(true));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Vector3(0, 0, 1.25f).ToColor(true));



            bool EqualColors(Color c1, Color c2) => c1.ToArgb() == c2.ToArgb();
        }

        [TestMethod()]
        public void ListMaxMinOfTest()
        {
            //Generate example list
            var list = new Dictionary<string, int>
            {
                { "test", 5 },
                { "entry", 2 },
                { "max", 9 },
                { "cwacrw", 1 },
                { "min", -5 },
            };

            //Get extremes
            Assert.AreEqual("max", list.MaxOf(k => k.Value).Key);
            Assert.AreEqual("min", list.MinOf(k => k.Value).Key);
        }

        [TestMethod()]
        public void ListContentsEqualsTest()
        {
            //Generate the comparison lists
            var list1 = new[] { 1, 5, 8 };
            var list2 = new[] { 5, 8, 1};
            var list3 = new[] { 1, 5 };

            Assert.IsTrue(list1.ContentsEquals(list2));
            Assert.IsFalse(list1.ContentsEquals(list3));
            Assert.IsFalse(list2.ContentsEquals(list3));
        }

        [TestMethod()]
        public void ListMoveTest()
        {
            var list = new List<int> { 5, 10, 20 };
            list.Move(0, 2);
            Assert.AreEqual(10, list[0]);
            Assert.AreEqual(20, list[1]);
            Assert.AreEqual(5, list[2]);

            list.Move(2, 0);
            Assert.AreEqual(5, list[0]);
            Assert.AreEqual(10, list[1]);
            Assert.AreEqual(20, list[2]);

            //Out of range
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => list.Move(-1, 1));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => list.Move(0, 3));
        }
    }
}