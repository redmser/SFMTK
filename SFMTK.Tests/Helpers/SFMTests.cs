﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SFMTK.Tests
{
    [TestClass()]
    public class SFMTests
    {
        //NYI: SFMTests - write tests for the ModCrawler

        [TestInitialize()]
        public void InitializeTests()
        {
            SFM.BasePath = "C:/ArbitraryPath/SourceFilmmaker/";
        }

        [TestMethod()]
        public void ReloadGameInfoTest()
        {
            //Requirement: successful SFM detection
            SFM.BasePath = SFM.DetectSFMDirectory();
            Assert.IsTrue(Directory.Exists(SFM.BasePath));

            SFM.ReloadGameInfo();
            Assert.IsNotNull(SFM.GameInfo);
            Assert.IsTrue(SFM.GameInfo.Mods.Count > 0);
        }

        [TestMethod()]
        public void GetLocalPathTest()
        {
            //From full path
            const string fullpath = "C:/ArbitraryPath/SourceFilmmaker/game/testmod/models/testfile.mdl";
            Assert.AreEqual("game/testmod/models/testfile.mdl", SFM.GetLocalPath(fullpath, RelativeTo.SFMDir));
            Assert.AreEqual("testmod/models/testfile.mdl", SFM.GetLocalPath(fullpath, RelativeTo.GameDir));
            Assert.AreEqual("models/testfile.mdl", SFM.GetLocalPath(fullpath, RelativeTo.ModDir));

            //Custom relative dir
            Assert.AreEqual("game/testmod/models/testfile.mdl", SFM.GetLocalPath(fullpath, "C:/ArbitraryPath/SourceFilmmaker/"));
            Assert.AreEqual("SourceFilmmaker/game/testmod/models/testfile.mdl", SFM.GetLocalPath(fullpath, "C:/ArbitraryPath/"));
            Assert.AreEqual("testfile.mdl", SFM.GetLocalPath(fullpath, "C:/ArbitraryPath/SourceFilmmaker/game/testmod/models/"));

            //Using windows-styled paths
            var winpath = fullpath.Replace('/', '\\');
            Assert.AreEqual("game/testmod/models/testfile.mdl", SFM.GetLocalPath(winpath, RelativeTo.SFMDir));
            Assert.AreEqual("testmod/models/testfile.mdl", SFM.GetLocalPath(winpath, RelativeTo.GameDir));
            Assert.AreEqual("models/testfile.mdl", SFM.GetLocalPath(winpath, RelativeTo.ModDir));

            //Null and empty cases
            Assert.IsNull(SFM.GetLocalPath(null, RelativeTo.ModDir));
            Assert.AreEqual(string.Empty, SFM.GetLocalPath(string.Empty, RelativeTo.SFMDir));
            Assert.AreEqual("testdir", SFM.GetLocalPath("testdir", null));
        }

        [TestMethod()]
        public void GetFullPathTest()
        {
            const string fullpath = "C:/ArbitraryPath/SourceFilmmaker/game/testmod/models/testfile.mdl";

            //Basic cases
            Assert.AreEqual(fullpath, SFM.GetFullPath("SourceFilmmaker/game/testmod/models/testfile.mdl"));
            Assert.AreEqual(fullpath, SFM.GetFullPath("/game/testmod/models/testfile.mdl"));
            Assert.AreEqual(fullpath, SFM.GetFullPath("testmod/models/testfile.mdl"));

            //Separately specified mod
            Assert.AreEqual(fullpath, SFM.GetFullPath("/models/testfile.mdl", "testmod"));

            //Invalid cases
            Assert.IsNull(SFM.GetFullPath(null));
            Assert.IsNull(SFM.GetFullPath("filenameonly.blah"));
            Assert.IsNull(SFM.GetFullPath("filenameonly.blah", "thiswontmatter"));

            //Full path already given
            Assert.AreEqual(fullpath, SFM.GetFullPath(fullpath));
        }

        [TestMethod()]
        public void DetectSFMDirectoryIntegrationTest()
        {
            //Requirement: steam and SFM are installed on the testing computer
            var dir = SFM.DetectSFMDirectory();
            Assert.IsNotNull(dir);
            Assert.IsTrue(Directory.Exists(dir));
        }

        [TestMethod()]
        public void DetectSteamDirectoryIntegrationTest()
        {
            //Requirement: steam is installed on the testing computer
            var dir = SFM.DetectSteamDirectory();
            Assert.IsNotNull(dir);
            Assert.IsTrue(Directory.Exists(dir));
        }
    }
}