﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SFMTK.Parsing.KeyValues.Tests
{
    [TestClass()]
    public class KeyValuesTokenizerTests
    {
        //NYI: KeyValuesTokenizerTests - test writing tokens to stream

        [TestInitialize()]
        public void InitializeTests()
        {
            this.Tokenizer = new KeyValuesTokenizer();
            this.Tokenizer.Rules.IncludeComments = IncludeCommentsMode.DoNotInclude;
        }

        public KeyValuesTokenizer Tokenizer { get; set; }

        public static byte[] StringToData(string input) => System.Text.Encoding.UTF8.GetBytes(input);

        [TestMethod()]
        public void BasicParseTest()
        {
            //Read simple KV file
            const string kv = @"
testkey itsvalue
otherkey ""spaces in value""
";
            this.Tokenizer.Read(StringToData(kv));
            var nodes = this.Tokenizer.Parse().Cast<KVPair>().ToList();
            Assert.AreEqual(2, nodes.Count);
            Assert.AreEqual("testkey",         nodes[0].Key);
            Assert.AreEqual("itsvalue",        nodes[0].Value);
            Assert.AreEqual("otherkey",        nodes[1].Key);
            Assert.AreEqual("spaces in value", nodes[1].Value);
        }

        [TestMethod()]
        public void EmptyParseTest()
        {
            //Empty values in a KV file are valid
            //UNTESTED: KVTokenizerTests - are empty keys also allowed in the spec?
            const string kv = @"
"""" emptykey
emptyvalue """"
";
            this.Tokenizer.Read(StringToData(kv));
            var nodes = this.Tokenizer.Parse().Cast<KVPair>().ToList();
            Assert.AreEqual(2, nodes.Count);
            Assert.AreEqual("", nodes[0].Key);
            Assert.AreEqual("emptykey", nodes[0].Value);
            Assert.AreEqual("emptyvalue", nodes[1].Key);
            Assert.AreEqual("", nodes[1].Value);
        }

        [TestMethod()]
        public void CommentParseTest()
        {
            //Comments and more complex formatting
            const string kv = @"
testkey ""value1"" //ignored here
""testing spaces"" {} //neat ""stuff"" {}}{

//Simple comment without anything else
//Also pretty {]}{{}COOL
";
            this.Tokenizer.Read(StringToData(kv));
            var nodes = this.Tokenizer.Parse().Cast<KVPair>().ToList();
            Assert.AreEqual(2, nodes.Count);
            Assert.AreEqual("testkey", nodes[0].Key);
            Assert.AreEqual("value1", nodes[0].Value);
            Assert.AreEqual("testing spaces", nodes[1].Key);
            Assert.IsNull(nodes[1].Value);
        }

        [TestMethod()]
        public void IncludeCommentsParseTest()
        {
            //Comments in a KV file, include tokens
            const string kv = @"
//Simple comment
testkey ""value1"" //ignored here WEW
""testing spaces"" tester //neat ""stuff""
";
            this.Tokenizer.Rules.IncludeComments = IncludeCommentsMode.Include;

            this.Tokenizer.Read(StringToData(kv));
            var nodes = this.Tokenizer.Parse();
            Assert.AreEqual(5, nodes.Count);
            Assert.IsInstanceOfType(nodes[0], typeof(KVComment));
            Assert.IsInstanceOfType(nodes[1], typeof(KVPair));
            Assert.IsInstanceOfType(nodes[2], typeof(KVComment));
            Assert.IsInstanceOfType(nodes[3], typeof(KVPair));
            Assert.IsInstanceOfType(nodes[4], typeof(KVComment));
        }

        [TestMethod()]
        public void DollayKeyTest()
        {
            //Keys are marked with a dollar, everything after is stored as a value
            const string kv = @"
$key ""value1"" another ""more values"" $second valuesingle
$neat { ""nested"" $goodness }
";
            this.Tokenizer.Rules = KeyValuesRuleSet.QC;
            Assert.Fail("DollarKeys is not implemented yet");
            //NYI: KeyValuesParserTests - DollarKeys is currently not implemented
        }

        [TestMethod()]
        public void NestedParseTest()
        {
            //Nested KV file
            const string kv = @"
testkey {
innerkey {
""deeper key"" {
}}
}
";
            this.Tokenizer.Read(StringToData(kv));
            var nodes = this.Tokenizer.Parse().Cast<KVPair>().ToList();
            Assert.AreEqual(1, nodes.Count); //testkey
            Assert.AreEqual(1, nodes[0].Children.Count); //innerkey
            Assert.AreEqual(1, ((KVPair)nodes[0].Children[0]).Children.Count); //deeper key
            Assert.AreEqual(0, ((KVPair)((KVPair)nodes[0].Children[0]).Children[0]).Children.Count); //no elements
        }

        [TestMethod()]
        public void InvalidNoValueParseTest()
        {
            //Key without a value
            const string kv = @"
testkey itsvalue
otherkey
";
            this.Tokenizer.Read(StringToData(kv));
            Assert.ThrowsException<KeyValuesParsingException>(() => this.Tokenizer.Parse().Cast<KVPair>().ToList());
        }

        [TestMethod()]
        public void InvalidMissingParseTest()
        {
            //Start of nested keys without an end
            const string kv = @"
testkey {
otherkey blah
";
            this.Tokenizer.Read(StringToData(kv));
            Assert.ThrowsException<KeyValuesParsingException>(() => this.Tokenizer.Parse().Cast<KVPair>().ToList());
        }

        [TestMethod()]
        public void InvalidDoubleParseTest()
        {
            //Start of nested keys without a key
            const string kv = @"
testkey { {
otherkey blah } }
";
            this.Tokenizer.Read(StringToData(kv));
            Assert.ThrowsException<KeyValuesParsingException>(() => this.Tokenizer.Parse().Cast<KVPair>().ToList());
        }

        [TestMethod()]
        public void InvalidUnmatchedParseTest()
        {
            //End of nested keys without a start
            const string kv = @"
testkey test }
otherkey
";
            this.Tokenizer.Read(StringToData(kv));
            Assert.ThrowsException<KeyValuesParsingException>(() => this.Tokenizer.Parse().Cast<KVPair>().ToList());
        }

        [TestMethod()]
        public void InvalidLengthTest()
        {
            //Tokens above a length of 1024 characters are not permitted
            const string kvformat = @"
longtoken {0}";

            var kv = string.Format(kvformat, new string('A', 1025));

            //Exception with compatibility mode
            (this.Tokenizer.Rules as DefaultKeyValuesRuleSet).CompatibilityMode = true;
            Assert.ThrowsException<KeyValuesTokenizingException>(() => this.Tokenizer.Read(StringToData(kv)));

            //No exception without compatibility mode
            (this.Tokenizer.Rules as DefaultKeyValuesRuleSet).CompatibilityMode = false;
            this.Tokenizer.Read(StringToData(kv));
        }
    }
}