﻿namespace SFMTK.Parsing.KeyValues.Tests
{
    public class TestKeyValueData
    {
        /// <summary>
        /// How many members this instance will serialize.
        /// </summary>
        public const int SerializedMembers = 8;

        [KVSerialized]
        public string fieldtest = "fieldvalue";

        [KVSerialized]
        public string PropertyTest { get; set; } = "propertyvalue";

        [KVSerialized("differentname")]
        public string RenamedTest { get; set; } = "renamed";

        [KVSerialized("nested", null)]
        public string NestedTest1 { get; set; } = "inner1";

        [KVSerialized("nested", null)]
        public string NestedTest2 { get; set; } = "inner2";

        [KVSerialized]
        public string GetOnly => "example";

        [KVSerialized]
        public readonly string readonlyfield = "value";

        [KVSerialized]
        public string NullTest { get; set; } = null;

        [KVSerialized]
        public System.DateTime StructTest { get; set; } = new System.DateTime(2018, 3, 14);
    }
}
