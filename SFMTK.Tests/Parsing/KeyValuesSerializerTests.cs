﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SFMTK.Parsing.KeyValues.Tests
{
    [TestClass]
    public class KeyValuesSerializerTests
    {
        [TestInitialize()]
        public void InitializeTests()
        {
            this.Tokenizer = new KeyValuesTokenizer();
            this.Tokenizer.Rules.IncludeComments = IncludeCommentsMode.DoNotInclude;
            this.Serializer = new KeyValuesSerializer();
            this.Serializer.NullHandling = NullHandlingMode.NullValue;
            this.Data = new TestKeyValueData();
        }

        public KeyValuesTokenizer Tokenizer { get; set; }
        public KeyValuesSerializer Serializer { get; set; }
        public TestKeyValueData Data { get; set; }

        [TestMethod()]
        public void SimpleSerialize()
        {
            var tokens = new KeyValueList(this.Serializer.Serialize(this.Data));
            Assert.AreEqual(TestKeyValueData.SerializedMembers, tokens.Count);

            //Compare keys
            Assert.AreEqual(nameof(TestKeyValueData.fieldtest), tokens[nameof(TestKeyValueData.fieldtest)].Key);
            Assert.AreEqual(nameof(TestKeyValueData.PropertyTest), tokens[nameof(TestKeyValueData.PropertyTest)].Key);
            Assert.AreEqual(nameof(TestKeyValueData.GetOnly), tokens[nameof(TestKeyValueData.GetOnly)].Key);
            Assert.AreEqual(nameof(TestKeyValueData.readonlyfield), tokens[nameof(TestKeyValueData.readonlyfield)].Key);

            //Compare values
            Assert.AreEqual(this.Data.fieldtest, tokens[nameof(TestKeyValueData.fieldtest)].Value);
            Assert.AreEqual(this.Data.PropertyTest, tokens[nameof(TestKeyValueData.PropertyTest)].Value);
            Assert.AreEqual(this.Data.readonlyfield, tokens[nameof(TestKeyValueData.readonlyfield)].Value);
            Assert.AreEqual(this.Data.GetOnly, tokens[nameof(TestKeyValueData.GetOnly)].Value);
        }

        [TestMethod()]
        public void SimpleDeserialize()
        {
            var tokens = new KeyValueList();
            tokens.Add(new KVPair(nameof(TestKeyValueData.fieldtest), "newfield"));
            tokens.Add(new KVPair(nameof(TestKeyValueData.PropertyTest), "newproperty"));
            tokens.Add(new KVPair(nameof(TestKeyValueData.NullTest), "notnull"));

            var newdata = this.Serializer.Deserialize<TestKeyValueData>(tokens.GetPairs());

            //Check new values
            Assert.AreEqual("newfield", newdata.fieldtest);
            Assert.AreEqual("newproperty", newdata.PropertyTest);
            Assert.AreEqual("notnull", newdata.NullTest);
        }

        [TestMethod()]
        public void InvalidDeserializeGetOnly()
        {
            var tokens = new KeyValueList();
            tokens.Add(new KVPair(nameof(TestKeyValueData.GetOnly), "invalidvalue"));
            tokens.Add(new KVPair(nameof(TestKeyValueData.readonlyfield), "invalidvalue"));

            var newdata = this.Serializer.Deserialize<TestKeyValueData>(tokens.GetPairs());

            //Values should remain the same
            Assert.AreEqual(this.Data.GetOnly, newdata.GetOnly);
            Assert.AreEqual(this.Data.readonlyfield, newdata.readonlyfield);
        }

        [TestMethod()]
        public void NullHandlingSerialize()
        {
            //Skip null values
            this.Serializer.NullHandling = NullHandlingMode.Skip;
            var tokens1 = new KeyValueList(this.Serializer.Serialize(this.Data));
            Assert.IsFalse(tokens1.GetPairs().Any(t => t.Key == nameof(TestKeyValueData.NullTest)));

            //Write empty string
            this.Serializer.NullHandling = NullHandlingMode.NullValue;
            var tokens2 = new KeyValueList(this.Serializer.Serialize(this.Data));
            Assert.AreEqual(string.Empty, tokens2[nameof(TestKeyValueData.NullTest)].Value);
        }

        [TestMethod()]
        public void NullHandlingDeserialize()
        {
            //Setting PropertyTest to null
            var tokens = new KeyValueList();
            tokens.Add(new KVPair(nameof(TestKeyValueData.PropertyTest)));

            //Skip null values
            this.Serializer.NullHandling = NullHandlingMode.Skip;
            var obj1 = this.Serializer.Deserialize<TestKeyValueData>(tokens.GetPairs());
            Assert.AreEqual(this.Data.PropertyTest, obj1.PropertyTest);

            //Write null
            this.Serializer.NullHandling = NullHandlingMode.NullValue;
            var obj2 = this.Serializer.Deserialize<TestKeyValueData>(tokens.GetPairs());
            Assert.IsNull(obj2.PropertyTest);
        }

        [TestMethod()]
        public void NestedTokenSerialize()
        {
            //NYI: KVStests - nested tokens + rootkey
            Assert.Fail("Not done");
        }

        [TestMethod()]
        public void NestedTokenDeserialize()
        {
            //NYI: KVStests - nested tokens + rootkey
            Assert.Fail("Not done");
        }

        [TestMethod()]
        public void StructSerialize()
        {
            //NYI: KVStests - structs and classes
            Assert.Fail("Not done");
        }

        [TestMethod()]
        public void StructDeserialize()
        {
            //NYI: KVStests - structs and classes
            Assert.Fail("Not done");
        }
    }
}
