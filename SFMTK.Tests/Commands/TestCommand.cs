﻿namespace SFMTK.Commands.Tests
{
    /// <summary>
    /// A test command which can be set to cause an error.
    /// </summary>
    public class TestCommand : Command
    {
        public TestCommand() : base()
        {
            this.Name = "Test Command";
        }

        /// <summary>
        /// Whether this command should cause an error on execute/undo.
        /// </summary>
        public bool CausesError { get; set; }

        public const string ErrorString = "TEST ERROR";

        public override string Execute() => this.CausesError ? ErrorString : null;
        public override string Undo() => this.CausesError ? ErrorString : null;
        public override bool ShowInHistory => true;
    }
}
