﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SFMTK.Commands.Tests
{
    [TestClass()]
    public class CommandHistoryTests
    {
        [TestInitialize()]
        public void InitializeTests()
        {
            this.TestHistory = new CommandHistory();
            this.TestCommand = new TestCommand();
            this.TestErrorCommand = new TestCommand { CausesError = true };
        }

        public CommandHistory TestHistory { get; set; }
        public TestCommand TestCommand { get; set; }
        public TestCommand TestErrorCommand { get; set; }

        //TODO: CommandHistoryTests - should include a second command, so when doing undo/redo you can actually see the change of "active" command

        [TestMethod()]
        public void AddCommandTest()
        {
            Assert.IsTrue(this.TestHistory.AddCommand(this.TestCommand));
            Assert.AreSame(this.TestCommand, this.TestHistory.Commands.Single());

            //Null case
            Assert.ThrowsException<ArgumentNullException>(() => this.TestHistory.AddCommand(null));
        }

        [TestMethod()]
        public void ExecuteDirectlyTest()
        {
            Assert.IsNull(this.TestHistory.ExecuteDirectly(this.TestCommand));
            Assert.AreSame(this.TestCommand, this.TestHistory.Commands.Single());

            //Executed, can undo
            Assert.IsTrue(this.TestHistory.CanUndo);
            Assert.IsFalse(this.TestHistory.CanRedo);

            //Null case
            Assert.ThrowsException< ArgumentNullException>(() => this.TestHistory.ExecuteDirectly(null));
        }

        [TestMethod()]
        public void RedoDirectlyTest()
        {
            Assert.IsNull(this.TestHistory.ExecuteDirectly(this.TestCommand));
            Assert.IsNull(this.TestHistory.UndoDirectly());
            Assert.IsNull(this.TestHistory.RedoDirectly());

            //Did redo, can undo again
            Assert.IsTrue(this.TestHistory.CanUndo);
            Assert.IsFalse(this.TestHistory.CanRedo);
        }

        [TestMethod()]
        public void UndoDirectlyTest()
        {
            Assert.IsNull(this.TestHistory.ExecuteDirectly(this.TestCommand));
            Assert.IsNull(this.TestHistory.UndoDirectly());

            //Did undo, can redo
            Assert.IsFalse(this.TestHistory.CanUndo);
            Assert.IsTrue(this.TestHistory.CanRedo);
        }

        [TestMethod()]
        public void CantUndoDirectlyTest()
        {
            Assert.IsNull(this.TestHistory.UndoDirectly());
        }

        [TestMethod()]
        public void CantRedoDirectlyTest()
        {
            Assert.IsNull(this.TestHistory.RedoDirectly());
        }

        [TestMethod()]
        public void FailExecuteDirectlyTest()
        {
            Assert.AreEqual(TestCommand.ErrorString, this.TestHistory.ExecuteDirectly(this.TestErrorCommand));
            Assert.AreEqual(0, this.TestHistory.Commands.Count);

            //No commands: cant undo/redo
            Assert.IsFalse(this.TestHistory.CanUndo);
            Assert.IsFalse(this.TestHistory.CanRedo);
        }

        [TestMethod()]
        public void FailRedoDirectlyTest()
        {
            Assert.IsNull(this.TestHistory.ExecuteDirectly(this.TestCommand));
            Assert.IsNull(this.TestHistory.UndoDirectly());
            this.TestCommand.CausesError = true;
            Assert.AreEqual(TestCommand.ErrorString, this.TestHistory.RedoDirectly());

            //Failed to redo, can still redo
            Assert.IsFalse(this.TestHistory.CanUndo);
            Assert.IsTrue(this.TestHistory.CanRedo);
        }

        [TestMethod()]
        public void FailUndoDirectlyTest()
        {
            Assert.IsNull(this.TestHistory.ExecuteDirectly(this.TestCommand));
            this.TestCommand.CausesError = true;
            Assert.AreEqual(TestCommand.ErrorString, this.TestHistory.UndoDirectly());

            //Failed to undo, can still undo
            Assert.IsTrue(this.TestHistory.CanUndo);
            Assert.IsFalse(this.TestHistory.CanRedo);
        }

        [TestMethod()]
        public void UpdateBoundsTest()
        {
            this.TestHistory.ExecuteDirectly(this.TestCommand);
            this.TestHistory.ExecuteDirectly(this.TestCommand);
            this.TestHistory.UpdateBounds(1);
            Assert.AreEqual(1, this.TestHistory.Commands.Count);
            this.TestHistory.UpdateBounds(0);
            Assert.AreEqual(0, this.TestHistory.Commands.Count);
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => this.TestHistory.UpdateBounds(-1));
        }
    }
}