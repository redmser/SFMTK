﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using Newtonsoft.Json.Linq;

namespace GitlabVersioning
{
    /// <summary>
    /// Holds all tags of a certain GitLab project. Ordered by name in reverse alphabetical order (with a simple numbering naming scheme, this is the correct order from new to old).
    /// </summary>
    [Serializable]
    public class TagList : IReadOnlyList<Tag>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TagList" /> class, getting any data from the project specified by id. Specifying this is not needed for parsing JSON data from a string.
        /// </summary>
        /// <param name="projectid">The ID of the project (not using the IID here).</param>
        /// <param name="token">The personal access token OR private token to authenticate your connection. Do not keep this in public source code.</param>
        public TagList(int projectid, string token) : this()
        {
            this.ProjectId = projectid;
            this.AccessToken = token;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TagList"/> class. Without setting the project id or access token, you will not be able to make online API look-ups.
        /// </summary>
        public TagList()
        {

        }

        /// <summary>
        /// Gets the tag at the specified index.
        /// </summary>
        /// <param name="index">The index of the tag to get.</param>
        /// <exception cref="System.InvalidOperationException">Unable to load tag list.</exception>
        public Tag this[int index]
        {
            get
            {
                if (this.backingList == null)
                    if (!this.LoadTags())
                        throw new InvalidOperationException("Unable to load tag list.");
                return this.backingList[index];
            }
        }

        /// <summary>
        /// Gets the tag with the specified name.
        /// </summary>
        /// <param name="name">The name of the tag to get.</param>
        /// <param name="ignoreCase">If set to <c>true</c>, case is ignored for finding the tag's name.</param>
        /// <exception cref="System.InvalidOperationException">Unable to load tag list.</exception>
        public Tag this[string name, bool ignoreCase = false]
        {
            get
            {
                if (this.backingList == null)
                    if (!this.LoadTags())
                        throw new InvalidOperationException("Unable to load tag list.");
                return this.backingList.FirstOrDefault(t => t.TagName.Equals(name, ignoreCase ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture));
            }
        }

        /// <summary>
        /// Gets whether this tag list is initialized and can be iterated through.
        /// </summary>
        public bool IsInitialized => this.backingList != null;

        /// <summary>
        /// If a call to LoadTags or LoadProjectInfo returned false, this property will contain the last exception that made the action fail.
        /// <para />These methods will only throw any unexpected exceptions, or ones that are caused through an invalid state.
        /// </summary>
        /// <remarks>This property is not serialized.</remarks>
        public Exception LastError
        {
            get => this._lastError;
            private set => this._lastError = value;
        }

        [NonSerialized]
        private Exception _lastError;

        /// <summary>
        /// Which project this tag list should get information of. API-wide ID is used here, not the IID.
        /// </summary>
        public int ProjectId { get; set; } = -1;

        /// <summary>
        /// The list storing all tags.
        /// </summary>
        protected ReadOnlyCollection<Tag> backingList;

        /// <summary>
        /// Gets the number of elements in the collection. Tags are loaded synchronously if not already loaded.
        /// </summary>
        /// <exception cref="InvalidOperationException">Unable to load tag list.</exception>
        public int Count
        {
            get
            {
                if (this.backingList == null)
                    if (!this.LoadTags())
                        throw new InvalidOperationException("Unable to load tag list.");
                return this.backingList.Count;
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection. Tags are loaded synchronously if not already loaded.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="InvalidOperationException">Unable to load tag list.</exception>
        public IEnumerator<Tag> GetEnumerator() => GetEnumeratorInternal();

        /// <summary>
        /// Returns an enumerator that iterates through a collection. Tags are loaded synchronously if not already loaded.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="InvalidOperationException">Unable to load tag list.</exception>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumeratorInternal();

        /// <summary>
        /// Returns an enumerator that iterates through the collection. Tags are loaded synchronously if not already loaded.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="InvalidOperationException">Unable to load tag list.</exception>
        private IEnumerator<Tag> GetEnumeratorInternal()
        {
            if (this.backingList == null)
                if (!this.LoadTags())
                    throw new InvalidOperationException("Unable to load tag list.");

            return this.backingList.GetEnumerator();
        }

        /// <summary>
        /// The proxy to use for any internet requests. Defaults to none, resulting in faster lookup times but possible issues if not configured correctly.
        /// </summary>
        /// <remarks>This property is not serialized.</remarks>
        public IWebProxy Proxy
        {
            get => this._proxy;
            set => this._proxy = value;
        }

        [NonSerialized]
        private IWebProxy _proxy = WebRequest.DefaultWebProxy;

        /// <summary>
        /// Explicitly loads (or reloads) the list of tags into the backing list synchronously (blocking the calling thread). Returns whether the operation was successful.
        /// <para />Only call this after setting the ProjectId and AccessToken properties.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">No project id has been specified.</exception>
        public bool LoadTags()
        {
            //UNTESTED: Networking methods while being offline
            
            this.LastError = null;

            if (this.ProjectId <= 0)
                throw new InvalidOperationException("No project id has been specified.");

            //Get response synchronously
            HttpWebResponse resp = null;
            try
            {
                resp = Helper.GetResponse(string.Format(APIGetTagFormat, this.ProjectId), this.Timeout, this.AccessToken, this.Proxy);
            }
            catch (WebException ex)
            {
                //Invalid request
                var inresp = ex.Response as HttpWebResponse;
                if (inresp == null)
                    throw;

                //Normal errors
                if (inresp.StatusCode == HttpStatusCode.NotFound || inresp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    this.LastError = ex;
                    return false;
                }

                //Uncertain
                throw;
            }

            if (this.PreloadAbsoluteUrl && string.IsNullOrEmpty(this.ProjectUrl))
            {
                //Load project info now - don't reload since it's gonna be replaced anyway
                this.LoadProjectInfo(false);
            }

            //Create tags from response
            string json;
            using (var reader = new StreamReader(resp.GetResponseStream()))
                json = reader.ReadToEnd();
            return LoadTags(json);
        }

        /// <summary>
        /// Loads (or updates) the project information, such as its URL, synchronously (blocking the calling thread). Returns whether the operation was successful.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">No project id has been specified.</exception>
        public bool LoadProjectInfo() => this.LoadProjectInfo(true);

        /// <summary>
        /// Loads (or updates) the project information, such as its URL, synchronously (blocking the calling thread). Returns whether the operation was successful.
        /// </summary>
        /// <param name="reload">If set to <c>true</c>, reloads the URLs of all releases of the tag list.</param>
        /// <exception cref="System.InvalidOperationException">No project id has been specified.</exception>
        private bool LoadProjectInfo(bool reload)
        {
            //UNTESTED: calling LoadProjectInfo after loading tags (with/without preloading info)
            
            this.LastError = null;

            if (this.ProjectId <= 0)
                throw new InvalidOperationException("No project id has been specified.");

            //Get response synchronously
            HttpWebResponse resp = null;
            try
            {
                resp = Helper.GetResponse(string.Format(APIGetProjectFormat, this.ProjectId), this.Timeout, this.AccessToken, this.Proxy);
            }
            catch (WebException ex)
            {
                //Invalid request
                var inresp = ex.Response as HttpWebResponse;
                if (inresp == null)
                    throw;

                //Normal errors
                if (inresp.StatusCode == HttpStatusCode.NotFound || inresp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    this.LastError = ex;
                    return false;
                }

                //Uncertain
                throw;
            }

            //Get URL from JSON response
            string json;
            using (var reader = new StreamReader(resp.GetResponseStream()))
                json = reader.ReadToEnd();

            var obj = JObject.Parse(json);
            this.ProjectUrl = obj.Value<string>("web_url");
            if (this.ProjectUrl.Last() == '/')
                this.ProjectUrl = this.ProjectUrl.Substring(0, this.ProjectUrl.Length - 1);

            //If applicable, update all release urls as well
            if (reload)
            {
                foreach (var tag in this)
                {
                    foreach (var rel in tag.Releases)
                    {
                        rel.Url = this.ProjectUrl + rel.RelativeUrl;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Explicitly loads (or reloads) the list of tags into the backing list, given a JSON string containing a response with all tag info. Returns whether the operation was successful.
        /// </summary>
        /// <param name="json">The JSON data to read the tag info from.</param>
        public bool LoadTags(string json)
        {
            this.LastError = null;

            try
            {
                this.backingList = new ReadOnlyCollection<Tag>(this.GenerateTags(json));
            }
            catch (Exception ex)
            {
                this.LastError = ex;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns the list of tags deserialized from the JSON string.
        /// </summary>
        /// <param name="json">The JSON data to read the tag info from.</param>
        private List<Tag> GenerateTags(string json)
        {
            //Conversion logic is defined through the TagConverter
            var jarray = JArray.Parse(json);
            var list = jarray.ToObject<List<Tag>>();

            //Update list-specific properties
            foreach (var tag in list)
            {
                tag.Changes = Change.FromNotes(tag.ReleaseNotesSections.ToList(), this);
                tag.Releases = Release.FromNotes(tag.ReleaseNotesSections, this);
            }

            return list;
        }

        /// <summary>
        /// Gets or sets the personal access token OR private token to authenticate your connection. Do not keep this in public source code.
        /// </summary>
        /// <remarks>Sets the PRIVATE-TOKEN header on all HTTP requests instead of using a parameter.
        /// <para/>This property is not serialized.</remarks>
        public string AccessToken
        {
            get => this._accessToken;
            set => this._accessToken = value;
        }

        [NonSerialized]
        private string _accessToken;

        /// <summary>
        /// {0} is the ID of the project.
        /// </summary>
        private const string APIGetTagFormat = @"https://gitlab.com/api/v4/projects/{0}/repository/tags";

        /// <summary>
        /// {0} is the ID of the project.
        /// </summary>
        private const string APIGetProjectFormat = @"https://gitlab.com/api/v4/projects/{0}";

        /// <summary>
        /// Gets or sets the timeout for any HTTP requests, in milliseconds. GitLab's API may be slow at times. Defaults to 10 seconds.
        /// </summary>
        public int Timeout { get; set; } = 10000;

        /// <summary>
        /// Which character to use for any reformatted lists.
        /// </summary>
        public ListCharacter ListCharacter { get; set; } = ListCharacter.Minus;

        /// <summary>
        /// If ListCharacter is set to Custom, defines a custom list character or string.
        /// </summary>
        public string CustomListCharacter { get; set; }

        /// <summary>
        /// Whether to use indenting for this entry.
        /// </summary>
        public bool ListIndent { get; set; } = true;

        /// <summary>
        /// Which strings are accepted as change log section titles. Case-insensitive.
        /// <para />If set to an empty collection, the second header is assumed to be the changes header.
        /// </summary>
        public string[] ChangeLogHeaders { get; set; } = { "CHANGES", "CHANGELOG", "CHANGE LOG", "WHAT'S NEW" };

        /// <summary>
        /// Which strings are accepted as download section titles. Case-insensitive.
        /// <para />If set to an empty collection, the last header is assumed to be the release header.
        /// </summary>
        public string[] DownloadHeaders { get; set; } = { "DOWNLOAD", "DOWNLOADS", "RELEASE", "RELEASES" };

        /// <summary>
        /// Name of the category for any changes which are directly below the changes header, and not in any sub-group.
        /// </summary>
        public string DefaultChangeCategory { get; set; } = "General";

        /// <summary>
        /// The Url to this project's gitlab page. Needed for release downloads. Automatically updated on the first version check or when calling LoadProjectInfo.
        /// </summary>
        public string ProjectUrl { get; set; }

        /// <summary>
        /// In order to get the absolute URL for a release file, another API call has to be made.
        /// <para />If you wish to hard-code the link to the project (by setting ProjectUrl manually), or use the LoadProjectInfo calls to update the absolute URL, set this to false to make initial version checking faster.
        /// </summary>
        public bool PreloadAbsoluteUrl { get; set; } = true;
    }

    /// <summary>
    /// Predefined list characters for use in the changes list.
    /// </summary>
    public enum ListCharacter
    {
        /// <summary>
        /// Define a custom ListCharacter using the CustomListCharacter property.
        /// </summary>
        Custom,
        /// <summary>
        /// Uses the '-' character for lists, followed by a space.
        /// </summary>
        Minus,
        /// <summary>
        /// Uses the '-' character for lists.
        /// </summary>
        MinusNoSpace,
        /// <summary>
        /// Uses the '•' character for lists, followed by a space.
        /// </summary>
        BulletPoint,
        /// <summary>
        /// Uses the '•' character for lists.
        /// </summary>
        BulletPointNoSpace,
        /// <summary>
        /// Lists do not have any prefixed characters.
        /// </summary>
        None
    }
}
