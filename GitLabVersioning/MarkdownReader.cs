﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Text;

namespace GitlabVersioning
{
    /// <summary>
    /// Incredibly simple reader for markdown strings.
    /// </summary>
    internal class MarkdownReader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MarkdownReader"/> class.
        /// </summary>
        /// <param name="text">The markdown-formatted text to read.</param>
        public MarkdownReader(string text)
        {
            this.Text = text;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MarkdownReader"/> class.
        /// </summary>
        public MarkdownReader()
        {

        }

        /// <summary>
        /// The markdown-formatted text to read.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Collection of sections that are in the document, once parsed.
        /// </summary>
        public MarkdownSection[] Sections { get; private set; }

        /// <summary>
        /// Gets or sets a bitmask for every headline format which should be seen as the start of a new section.
        /// </summary>
        public HeadlineFormat HeadlineFormats { get; set; } = HeadlineFormat.Headlines;

        /// <summary>
        /// Parses the markdown document.
        /// </summary>
        public void Parse()
        {
            //TODO: Probably redo this (and release parsing) using a TRUE parser, and not cheap regex (does handle some edge cases wrongly)...
            //  -> most likely only way to *remove* markdown from something, too (since that should be an easy-to-do thing -> as a public helper method)
            var secs = new List<MarkdownSection>();
            
            var sec = new MarkdownSection { Title = "" };
            var sb = new StringBuilder();
            var lines = this.Text.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < lines.Length; i++)
            {
                var ln = lines[i];

                //Check if formatted as headline - get flags of formats
                var skiploop = false;
                foreach (var flag in Enum.GetValues(typeof(HeadlineFormat)).Cast<HeadlineFormat>().Where(f => (f & this.HeadlineFormats) != 0))
                {
                    //Get pattern
                    var patt = Helper.GetEnumDescription(flag);
                    var match = Regex.Match(ln, patt);

                    //Matches?
                    if (match.Success)
                    {
                        skiploop = true;
                        MatchHeadline(match.Groups[1].Value);
                        break;
                    }

                    if (i < lines.Length - 1)
                    {
                        //Special cases: ---- and ==== formatting
                        if (flag == HeadlineFormat.HeadlineOne)
                        {
                            //Check if all chars of next line are =
                            var next = lines[i + 1];
                            if (next.All(c => c == '='))
                            {
                                //Is a headline!
                                skiploop = true;
                                MatchHeadline(ln);
                                i++;
                                break;
                            }
                        }
                        else if (flag == HeadlineFormat.HeadlineTwo)
                        {
                            //Check if all chars of next line are -
                            var next = lines[i + 1];
                            if (next.All(c => c == '-'))
                            {
                                //Is a headline!
                                skiploop = true;
                                MatchHeadline(ln);
                                i++;
                                break;
                            }
                        }
                    }
                }

                if (skiploop)
                    continue;

                //Add to string builder
                sb.AppendLine(ln);
            }

            MatchHeadline(null);
            this.Sections = secs.ToArray();



            void MatchHeadline(string title)
            {
                //Add last section, create new one
                if (sec != null)
                {
                    //Remove last line terminator
                    if (sb.Length > 0)
                        sb.Length -= Environment.NewLine.Length;

                    sec.Contents = sb.ToString();
                    secs.Add(sec);
                    sb.Clear();
                }

                if (title == null)
                    return;

                sec = new MarkdownSection();
                sec.Title = title;
            }
        }

        /// <summary>
        /// Applies the list format to the specified string, returning the first matching regex result.
        /// </summary>
        /// <param name="text">The text to apply the format onto.</param>
        /// <param name="format">The format to use.</param>
        internal static Match ApplyListFormat(string text, ListFormat format)
        {
            foreach (var flag in Enum.GetValues(typeof(ListFormat)).Cast<ListFormat>().Where(f => (f & format) != 0))
            {
                //Get pattern
                var patt = Helper.GetEnumDescription(flag);
                var match = Regex.Match(text, patt);
                if (match.Success)
                    return match;
            }
            return null;
        }
    }

    /// <summary>
    /// Supported formats for detecting a line of markdown-formatted text as a headline. Patterns are stored in the Description attribute - Group 1 contains the text.
    /// </summary>
    [Flags]
    public enum HeadlineFormat
    {
        /// <summary>
        /// Do not parse headlines.
        /// </summary>
        None = 0,
        /// <summary>
        /// A headline with the format '# Title' or 'Title' followed by one or multiple '=' characters on the next line.
        /// </summary>
        [Description(@"#\s+(.+)")]
        HeadlineOne = 1,
        /// <summary>
        /// A headline with the format '## Title' or 'Title' followed by one or multiple '-' characters on the next line.
        /// </summary>
        [Description(@"##\s+(.+)")]
        HeadlineTwo = 2,
        /// <summary>
        /// A headline with the format '### Title'.
        /// </summary>
        [Description(@"###\s+(.+)")]
        HeadlineThree = 4,
        /// <summary>
        /// A headline with the format '#### Title'.
        /// </summary>
        [Description(@"####\s+(.+)")]
        HeadlineFour = 8,
        /// <summary>
        /// A headline with the format '##### Title'.
        /// </summary>
        [Description(@"#####\s+(.+)")]
        HeadlineFive = 16,
        /// <summary>
        /// A headline with the format '###### Title'.
        /// </summary>
        [Description(@"######\s+(.+)")]
        HeadlineSix = 32,
        /// <summary>
        /// A headline with the format '**Title**' or '__Title__', or the formats '***Title***' or '___Title___'.
        /// </summary>
        [Description(@"^[*_]?[*_]{2}(.+?)[*_]{2,3}$")]
        Bold = 64,
        /// <summary>
        /// A headline with the format '*Title*' or '_Title_', or the formats '***Title***' or '___Title___'.
        /// </summary>
        [Description(@"^(?:(?<=[*_]{2})|(?<![*_]))[*_](?![*_])(.+?)[*_]$")]
        Italics = 128,
        /// <summary>
        /// A headline with the format '>Title'.
        /// </summary>
        [Description(@"^>\s*(.+)")]
        Quote = 256,
        /// <summary>
        /// A headline with the format '`Title`'.
        /// </summary>
        [Description(@"^(?<!`)`{1,2}(?!`)(.+?)`{1,2}$")]
        Code = 512,
        /// <summary>
        /// A headline of any of the default headline formats.
        /// </summary>
        Headlines = HeadlineOne | HeadlineTwo | HeadlineThree | HeadlineFour | HeadlineFive | HeadlineSix
    }

    /// <summary>
    /// Supported formats for detecting a list of markdown-formatted text. Patterns are stored in the Description attribute - Group 1 contains the indenting, Group 2 contains the text.
    /// </summary>
    [Flags]
    public enum ListFormat
    {
        /// <summary>
        /// Do not parse lists.
        /// </summary>
        None = 0,
        /// <summary>
        /// A list with the format '- Text' or '* Text'. GitLab's task lists are included here since they use special formatting similar to this.
        /// </summary>
        [Description(@"(^\s*)[-*]\s+(.+)")]
        Unordered = 1,
        /// <summary>
        /// A list with the format '1. Text', '1) Text' or '#1 Text'.
        /// </summary>
        [Description(@"(^\s*)(?:\d+[.)]|#\d+)\s+(.+)")]
        Numbered = 2,
        /// <summary>
        /// A list with any list formatting used.
        /// </summary>
        All = Unordered | Numbered
    }
}
