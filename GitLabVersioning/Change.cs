﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace GitlabVersioning
{
    /// <summary>
    /// A change in a list of changes for showing up in the change log.
    /// </summary>
    [Serializable]
    public class Change
    {
        /// <summary>
        /// The category that this change was in. Determined using the headline that preceeded it, if any.
        /// </summary>
        public string Category { get; private set; }

        /// <summary>
        /// The content of the change, with markdown and custom list formatting as specified.
        /// </summary>
        public string Text
        {
            get
            {
                var indent = this.ListIndent ? this.TextMatch.Groups[1].Value : string.Empty;
                var ch = this.ListCharacter == ListCharacter.Custom ? this.CustomListCharacter : GetListCharacter();
                return $"{indent}{ch}{this.TextMatch.Groups[2].Value}";



                string GetListCharacter()
                {
                    switch (this.ListCharacter)
                    {
                        case ListCharacter.Minus:
                            return "- ";
                        case ListCharacter.BulletPoint:
                            return "• ";
                        case ListCharacter.MinusNoSpace:
                            return "-";
                        case ListCharacter.BulletPointNoSpace:
                            return "•";
                        case ListCharacter.None:
                            return string.Empty;
                        default:
                            return null;
                    }
                }
            }
        }

        /// <summary>
        /// Which character to use for prefixing this entry.
        /// </summary>
        public ListCharacter ListCharacter { get; set; } = ListCharacter.Minus;

        /// <summary>
        /// If ListCharacter is set to Custom, defines a custom list character or string.
        /// </summary>
        public string CustomListCharacter { get; set; }

        /// <summary>
        /// Whether to use indenting for this entry.
        /// </summary>
        public bool ListIndent { get; set; } = true;

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"[{this.Category}] {this.Text}";

        /// <summary>
        /// Gets the text as it was originally found in the markdown list, without redone list formatting.
        /// </summary>
        public string TextFormatted => this.TextMatch.Value;

        /// <summary>
        /// Gets or sets the original match of this instance, since it allows for easier storage of the different needed parts of the result.
        /// </summary>
        internal Match TextMatch { get; set; }

        /// <summary>
        /// Generates a new Change list from the specified token containing infos about the changes.
        /// </summary>
        /// <param name="sections">The markdown sections of the release notes.</param>
        /// <param name="list">TagList for reference values.</param>
        internal static Change[] FromNotes(List<MarkdownSection> sections, TagList list)
        {
            //Find main change log header
            var toparse = new List<MarkdownSection>();
            var specialfirst = false;
            if (list.ChangeLogHeaders.Length == 0)
            {
                //Get second section
                toparse.AddRange(sections.GetRange(1, sections.Count - 1));
            }
            else
            {
                for (var i = 0; i < sections.Count; i++)
                {
                    var section = sections[i];
                    var tit = section.Title.ToUpperInvariant();
                    if (list.ChangeLogHeaders.Any(c => c.ToUpperInvariant() == tit))
                    {
                        //Is valid header, gets all until end
                        toparse.AddRange(sections.GetRange(i, sections.Count - i));
                        specialfirst = true;
                        break;
                    }
                }
            }
            
            //No change headers
            if (!toparse.Any())
            {
                //Parse all sections
                toparse = sections;
            }

            if (string.IsNullOrEmpty(toparse.First().Title))
                specialfirst = true;

            //Generate list contents
            var chlist = new List<Change>();
            for (var i = 0; i < toparse.Count; i++)
            {
                var section = toparse[i];

                //Category name
                var cat = (specialfirst && i == 0) ? list.DefaultChangeCategory : section.Title;
                foreach (var ln in section.Contents.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    var apply = MarkdownReader.ApplyListFormat(ln, ListFormat.All);
                    if (apply != null)
                        chlist.Add(new Change
                        {
                           Category = cat, TextMatch = apply, CustomListCharacter = list.CustomListCharacter, ListCharacter = list.ListCharacter, ListIndent = list.ListIndent
                        });
                }
            }
            return chlist.ToArray();
        }
    }
}
