﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace GitlabVersioning
{
    /// <summary>
    /// A release file, as found in the release notes. Includes a URL to download the file.
    /// </summary>
    [Serializable]
    public class Release
    {
        /// <summary>
        /// The name of the file, as specified by the text displayed for the link itself in the release notes.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The description of the file, as specified by the text displayed in front of the link to the file. May be null if none is specified.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// The relative HTTP URL to download this file, as it was gotten from the markdown version directly.
        /// </summary>
        public string RelativeUrl { get; private set; }

        /// <summary>
        /// The absolute HTTP URL to download this file. May be null unless updated (if PreloadAbsoluteUrl is set to true, or after calling LoadProjectInfo on the initial TagList).
        /// <para />This link may not work with private projects unless when you are logged in.
        /// </summary>
        public string Url { get; internal set; }

        /// <summary>
        /// Gets the text as it was originally found in the markdown list.
        /// </summary>
        public string TextFormatted { get; private set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"{this.Description} [{this.Name}]({this.Url ?? this.RelativeUrl})";

        /// <summary>
        /// Generates a new Release list from the specified token containing infos about the releases.
        /// </summary>
        /// <param name="sections">The markdown sections of the release notes.</param>
        /// <param name="releases">Titles for the release header. If an empty collection, the last header is assumed to be the release header.</param>
        internal static Release[] FromNotes(MarkdownSection[] sections, TagList list)
        {
            //Find release header
            string contents = null;
            if (list.DownloadHeaders.Length == 0)
            {
                //Get last section
                contents = sections[sections.Length - 1].Contents;
            }
            else
            {
                foreach (var section in sections)
                {
                    var tit = section.Title.ToUpperInvariant();
                    if (list.DownloadHeaders.Any(c => c.ToUpperInvariant() == tit))
                    {
                        //Is valid header, gets contents!
                        contents = section.Contents;
                        break;
                    }
                }
            }

            //No release header
            if (contents == null)
            {
                //Get last section, it will find something
                contents = sections[sections.Length - 1].Contents;
            }

            //Empty list, is valid otherwise
            if (contents == string.Empty)
                return new Release[] { };

            //Generate list contents
            var rellist = new List<Release>();
            foreach (var ln in contents.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries))
            {
                //Create releases using pattern
                var apply = Regex.Match(ln, ReleaseFormat);
                if (apply.Success)
                {
                    var rel = new Release { TextFormatted = ln, Description = apply.Groups[1].Value, Name = apply.Groups[2].Value, RelativeUrl = apply.Groups[3].Value };
                    if (!string.IsNullOrEmpty(list.ProjectUrl))
                        rel.Url = list.ProjectUrl + rel.RelativeUrl;
                    rellist.Add(rel);
                }
            }
            return rellist.ToArray();
        }

        /// <summary>
        /// The regex format string for getting the release info.
        /// </summary>
        private const string ReleaseFormat = @"(.+?)?\s?\[(.+?)\]\((.+?)\)";
    }
}
