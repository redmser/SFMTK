﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SFMTK.Modules.Paid.SFMCleaner
{
    /// <summary>
    /// Helper for collecting all files relevant for SFMCleaner.
    /// </summary>
    public class FileCollector
    {
        /// <summary>
        /// Returns an <see cref="IEnumerable{T}"/> with the full path to all files in the SFM directory.
        /// </summary>
        private IEnumerable<CleanerFile> Collect()
        {
            //Retrieve all files in SFM folder
            if (IOHelper.IsNTFSDrive(SFM.BasePath[0].ToString()))
            {
                //Use MFT
                return IOHelper.GetNodesUsingMFT(IOHelper.GetWindowsPath(SFM.BasePath))
                    .Where(n => n != null)
                    .Select(n => new CleanerFile(n));
            }

            //Use regular IO
            return Directory.EnumerateFiles(SFM.BasePath, "*", SearchOption.AllDirectories).Select(f => new CleanerFile(IOHelper.GetStandardizedPath(f)));
        }

        /// <summary>
        /// Starts collecting all files relevant for SFMCleaner, if not already running/completed.
        /// </summary>
        /// <returns></returns>
        public Task Start()
        {
            if (_state == FileCollectorState.Initialized)
            { 
                //Start running
                _state = FileCollectorState.Running;
                return Task.Run(() =>
                {
                    foreach (var file in this.Collect())
                        lock (SFMCleanerModule.FileCollector.Items)
                            this.Items.Add(file);
                    _state = FileCollectorState.Finished;
                });
            }
            return Task.CompletedTask;
        }

        private FileCollectorState _state;

        /// <summary>
        /// Gets the files and folders relevant for SFMCleaner.
        /// </summary>
        public List<CleanerFile> Items { get; private set; } = new List<CleanerFile>();

        public IEnumerable<CleanerFile> Files => this.Items.Where(i => !i.IsFolder);

        public IEnumerable<CleanerFile> Folders => this.Items.Where(i => i.IsFolder);

        /// <summary>
        /// Gets the size of the specified folder.
        /// </summary>
        /// <param name="path">Folder to check for.</param>
        /// <returns></returns>
        public FileSize GetFolderSize(string path) => (ulong)this.Files.Where(f => f.FullPath.StartsWith(path)).Sum(f => (long)f.FileSize.Size);

        private enum FileCollectorState
        {
            /// <summary>
            /// No files are present yet, FileCollector is ready.
            /// </summary>
            Initialized,
            /// <summary>
            /// FileCollector is retrieving files.
            /// </summary>
            Running,
            /// <summary>
            /// FileCollector finished, files are listed.
            /// </summary>
            Finished
        }
    }
}
