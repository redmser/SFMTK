﻿using System.ComponentModel;
using System.Windows.Forms;

namespace SFMTK.Modules.Paid.SFMCleaner
{
    /// <summary>
    /// SFMCleaner allows to clean up your SFM installation by removing duplicate and unnecessary files.
    /// <para/>This module is only available for the paid release of SFMTK.
    /// </summary>
    public class SFMCleanerModule : IModule, IModuleThemeable
    {
        /// <summary>
        /// Gets the main form that this module will be applied on.
        /// </summary>
        public static SFMTK.Forms.Main MainForm { get; private set; }

        /// <summary>
        /// Gets the <see cref="T:SFMTK.Modules.Paid.SFMCleaner.FileCollector"/> used to get all files relevant for SFMCleaner.
        /// </summary>
        public static FileCollector FileCollector { get; private set; }

        /// <summary>
        /// Gets the loading context of the module, determining on which context the OnLoad method should be called.
        /// </summary>
        public ModuleLoadingContext GetLoadingContext() => ModuleLoadingContext.StartupUserInterface;

        /// <summary>
        /// Called when the module is told to load, either on startup
        /// (only called once, with the context specified on <see cref="M:SFMTK.Modules.IModule.GetLoadingContext" />) or when manually loaded (can be any context).
        /// <para />Returns whether the module has been loaded successfully and is now enabled.
        /// </summary>
        public bool OnLoad(ModuleLoadingContext context)
        {
            //Init collector
            FileCollector = new FileCollector();

            return true;
        }

        /// <summary>
        /// Called on every step of the startup procedure (entries in <see cref="T:SFMTK.Modules.ModuleLoadingContext" /> prefixed with <c>Startup</c>).
        /// <para />If the module is late-loaded (such as enabling in the settings), all steps of the startup procedure are called directly after one another.
        /// </summary>
        public void OnStartup(ModuleLoadingContext context)
        {
            if (context == ModuleLoadingContext.StartupUserInterface)
            {
                //UI additions
                MainForm = Program.MainForm;

                MainForm.InvokeIfRequired(() =>
                {
                    //SFMCleaner widget
                    MainForm.GetMenu("Tools").DropDownItems.Insert(3,
                        Program.MainCM.CreateToolStripMenuItem(SFMTK.Commands.CommandManager.GetWidgetCommand<Widgets.SFMCleanerWidget>()));
                });
            }
        }

        /// <summary>
        /// Called when the module is unloaded, after having been initialized.
        /// <para />Returns whether the module has been unloaded successfully and is now disabled.
        /// </summary>
        public bool OnUnload(ModuleUnloadingContext context)
        {
            return true;
        }

        public void ApplyComponentTheme(Control parent, Component component) { }
        public void ApplyControlTheme(Control parent, Component component) { }
        public void ApplyNonDefaultControlTheme(Control parent, Component component)
        {
            if (component is System.Windows.Forms.DataVisualization.Charting.Chart chart)
            {
                //ChartAreas
                foreach (var area in chart.ChartAreas)
                {
                    area.BackColor = UITheme.SelectedTheme.MainBackColor;
                }

                //Legends
                foreach (var legend in chart.Legends)
                {
                    legend.BackColor = UITheme.SelectedTheme.DarkBackColor;
                    legend.ForeColor = UITheme.SelectedTheme.MainTextColor;
                    legend.TitleForeColor = UITheme.SelectedTheme.GroupTextColor;
                }

                //Series
                foreach (var series in chart.Series)
                {
                    series.LabelForeColor = UITheme.SelectedTheme.MainTextColor;
                }

                //NYI: SFMCleaner - allow specifying chart palette (even custom?). colors specified above should be default-fallback-ed
            }
        }
        public void ApplySystemControlTheme(Control parent, Component component) { }

        public string[] GetDependencies() => new[] { Constants.PaidSFMTKModule };
        public string GetParent() => Constants.PaidSFMTKModule;
    }
}
