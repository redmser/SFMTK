﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;
using SFMTK.Widgets;

namespace SFMTK.Modules.Paid.SFMCleaner.Widgets
{
    public partial class SFMCleanerWidget : Widget
    {
        //TODO: SFMCleanerWidget - add another pie chart view: (units = file size) SFM's files | Removed useless files | Removed rarely used files

        /// <summary>
        /// Initializes a new instance of the <see cref="SFMCleanerWidget"/> class.
        /// </summary>
        public SFMCleanerWidget()
        {
            //Compiler-generated
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            //Start collection here, if not already started
            var task = SFMCleanerModule.FileCollector.Start();
            if (!task.IsCompleted)
            {
                //Start update timer
                this.loadingUpdateTimer.Start();

                //Tell it to stop when completed
                task.ContinueWith(FinishedLoading);
            }

            //Generate the missing data points
            var modsizeseries = this.sizeChart.Series[1];
            foreach (var mod in SFM.GameInfo.Mods)
            {
                //Add mod to second dataset
                modsizeseries.Points.Add(new DataPoint { Label = mod.Name });
            }

            var typesizeseries = this.sizeChart.Series[2];
            foreach (var type in SFMTK.Contents.Content.ContentList)
            {
                //Add type name to third dataset
                typesizeseries.Points.Add(new DataPoint { Label = SFMTK.Contents.Content.GetTypeName(type) });
            }

            base.OnLoad(e);



            void FinishedLoading(Task obj)
            {
                //Stop further update calls
                this.loadingUpdateTimer.Stop();

                //Wait for current update to finish
                SpinWait.SpinUntil(() => _updateChartLock == 0);

                //Do final UI update call
                this.InvokeIfRequired(() =>
                {
                    //Update all series
                    foreach (var series in this.sizeChart.Series)
                        this.UpdateChart(series);

                    //Hide loading panel
                    this.loadingChartPanel.Hide();

                    //Update chart drawing
                    this.sizeChart.Invalidate();
                });
            }
        }

        private void sizeViewRadioGroupPanel_SelectedChanged(object sender, EventArgs e)
        {
            //Change visible chart series
            string selected;
            switch (this.sizeViewRadioGroupPanel.Selected)
            {
                case 0:
                    selected = "DiskSpaceSeries";
                    break;
                case 1:
                    selected = "ModSpaceSeries";
                    break;
                case 2:
                    selected = "TypeSpaceSeries";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(selected));
            }

            foreach (var series in this.sizeChart.Series)
            {
                series.Enabled = series.Name == selected;
            }
        }

        private int _updateChartLock = 0;

        private void loadingUpdateTimer_Tick(object sender, EventArgs e) => this.TryUpdateChart();

        /// <summary>
        /// Updates the currently visible series of the chart without allowing multiple calls to this method at once.
        /// </summary>
        private void TryUpdateChart()
        {
            //Get all files known
            if (SFMCleanerModule.FileCollector.Items.Count > 0)
            {
                //Add data to visible chart series
                this.TryUpdateChart(this.sizeChart.Series.Single(s => s.Enabled));
            }
        }

        /// <summary>
        /// Updates the specified series of the chart without allowing multiple calls to this method at once.
        /// </summary>
        /// <param name="series">The series to update.</param>
        private void TryUpdateChart(Series series)
        {
            if (Interlocked.CompareExchange(ref _updateChartLock, 1, 0) != 0)
                return;

            try
            {
                this.UpdateChart(series);

                //Update chart drawing
                this.sizeChart.Invalidate();
            }
            finally
            {
                Interlocked.Exchange(ref _updateChartLock, 0);
            }
        }

        /// <summary>
        /// Updates the specified series of the chart. Does not invalidate the chart.
        /// </summary>
        /// <param name="series">The series to update.</param>
        private async void UpdateChart(Series series)
        {
            //Update series async (to avoid long calculations freezing UI)
            await Task.Run(() =>
            {
                switch (series.Name)
                {
                    case "DiskSpaceSeries":
                        //Compare SFM folder's total size to disk size
                        long sfmspace;
                        lock (SFMCleanerModule.FileCollector.Items)
                            sfmspace = SFMCleanerModule.FileCollector.Files.Sum(f => (long)f.FileSize.Size);
                        var freespace = new DriveInfo(SFM.BasePath[0].ToString()).AvailableFreeSpace;
                        var nonsfmspace = new DriveInfo(SFM.BasePath[0].ToString()).TotalSize - sfmspace - freespace;
                        series.Points[0].YValues[0] = sfmspace;
                        series.Points[1].YValues[0] = nonsfmspace;
                        series.Points[2].YValues[0] = freespace;
                        break;
                    case "ModSpaceSeries":
                        //Calculate size for each mod folder separately
                        for (var i = 0; i < SFM.GameInfo.Mods.Count; i++)
                        {
                            var mod = SFM.GameInfo.Mods[i];
                            series.Points[i].YValues[0] = SFMCleanerModule.FileCollector.GetFolderSize(mod.Path);
                        }
                        break;
                    case "TypeSpaceSeries":
                        //NYI: SFMCleanerWidget - Calculate size for each content type separately
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(series));
                }
            });

            //TODO: SFMCleanerWidget - cluster smallest values of Mod/TypeSpaceSeries into a "Other" point (set clustered points to invisible and yvalue 0)
        }
    }
}
