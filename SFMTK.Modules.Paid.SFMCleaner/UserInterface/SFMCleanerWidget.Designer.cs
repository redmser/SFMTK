﻿namespace SFMTK.Modules.Paid.SFMCleaner.Widgets
{
    partial class SFMCleanerWidget
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SFMCleanerWidget));
            this.loadingUpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.loadingChartPanel = new System.Windows.Forms.Panel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.sizeChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.sizeViewRadioGroupPanel = new SFMTK.Controls.RadioGroupPanel();
            this.invisibleControl1 = new SFMTK.Controls.InvisibleControl();
            this.label3 = new System.Windows.Forms.Label();
            this.viewDiskUsageRadioButton = new System.Windows.Forms.RadioButton();
            this.viewModsSizeRadioButton = new System.Windows.Forms.RadioButton();
            this.viewTypeSizeRadioButton = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.loadingChartPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeChart)).BeginInit();
            this.sizeViewRadioGroupPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // loadingUpdateTimer
            // 
            this.loadingUpdateTimer.Interval = 200;
            this.loadingUpdateTimer.Tick += new System.EventHandler(this.loadingUpdateTimer_Tick);
            // 
            // loadingChartPanel
            // 
            this.loadingChartPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loadingChartPanel.Controls.Add(this.progressBar1);
            this.loadingChartPanel.Controls.Add(this.label4);
            this.loadingChartPanel.Location = new System.Drawing.Point(218, 244);
            this.loadingChartPanel.Name = "loadingChartPanel";
            this.loadingChartPanel.Size = new System.Drawing.Size(160, 48);
            this.loadingChartPanel.TabIndex = 4;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(8, 24);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(140, 19);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Loading chart...";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Location = new System.Drawing.Point(149, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(697, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Reduce the size of your Source Filmmaker files by finding out what isn\'t needed.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "SFMCleaner";
            // 
            // sizeChart
            // 
            this.sizeChart.BackColor = System.Drawing.SystemColors.Control;
            chartArea1.Name = "MainChartArea";
            this.sizeChart.ChartAreas.Add(chartArea1);
            this.sizeChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.DockedToChartArea = "MainChartArea";
            legend1.IsDockedInsideChartArea = false;
            legend1.Name = "MainLegend";
            this.sizeChart.Legends.Add(legend1);
            this.sizeChart.Location = new System.Drawing.Point(0, 0);
            this.sizeChart.Name = "sizeChart";
            this.sizeChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series1.ChartArea = "MainChartArea";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series1.Legend = "MainLegend";
            series1.Name = "DiskSpaceSeries";
            dataPoint1.Label = "SFM Folder";
            dataPoint2.Label = "Occupied\\nSpace";
            dataPoint3.IsValueShownAsLabel = false;
            dataPoint3.Label = "Free Space";
            series1.Points.Add(dataPoint1);
            series1.Points.Add(dataPoint2);
            series1.Points.Add(dataPoint3);
            series2.ChartArea = "MainChartArea";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series2.Enabled = false;
            series2.Legend = "MainLegend";
            series2.Name = "ModSpaceSeries";
            series3.ChartArea = "MainChartArea";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series3.Enabled = false;
            series3.Legend = "MainLegend";
            series3.Name = "TypeSpaceSeries";
            this.sizeChart.Series.Add(series1);
            this.sizeChart.Series.Add(series2);
            this.sizeChart.Series.Add(series3);
            this.sizeChart.Size = new System.Drawing.Size(729, 511);
            this.sizeChart.TabIndex = 0;
            this.sizeChart.Text = "chart1";
            // 
            // sizeViewRadioGroupPanel
            // 
            this.sizeViewRadioGroupPanel.Controls.Add(this.invisibleControl1);
            this.sizeViewRadioGroupPanel.Controls.Add(this.label3);
            this.sizeViewRadioGroupPanel.Controls.Add(this.viewDiskUsageRadioButton);
            this.sizeViewRadioGroupPanel.Controls.Add(this.viewModsSizeRadioButton);
            this.sizeViewRadioGroupPanel.Controls.Add(this.viewTypeSizeRadioButton);
            this.sizeViewRadioGroupPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sizeViewRadioGroupPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.sizeViewRadioGroupPanel.Location = new System.Drawing.Point(738, 42);
            this.sizeViewRadioGroupPanel.Name = "sizeViewRadioGroupPanel";
            this.sizeViewRadioGroupPanel.Selected = 0;
            this.sizeViewRadioGroupPanel.Size = new System.Drawing.Size(116, 511);
            this.sizeViewRadioGroupPanel.TabIndex = 3;
            this.sizeViewRadioGroupPanel.SelectedChanged += new System.EventHandler(this.sizeViewRadioGroupPanel_SelectedChanged);
            // 
            // invisibleControl1
            // 
            this.invisibleControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.invisibleControl1.Location = new System.Drawing.Point(0, 0);
            this.invisibleControl1.Name = "invisibleControl1";
            this.invisibleControl1.Size = new System.Drawing.Size(116, 0);
            this.invisibleControl1.TabIndex = 3;
            this.invisibleControl1.TabStop = false;
            this.invisibleControl1.Text = "invisibleControl1";
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "View:";
            // 
            // viewDiskUsageRadioButton
            // 
            this.viewDiskUsageRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.viewDiskUsageRadioButton.Checked = true;
            this.viewDiskUsageRadioButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.viewDiskUsageRadioButton.Location = new System.Drawing.Point(3, 23);
            this.viewDiskUsageRadioButton.Name = "viewDiskUsageRadioButton";
            this.viewDiskUsageRadioButton.Size = new System.Drawing.Size(110, 24);
            this.viewDiskUsageRadioButton.TabIndex = 0;
            this.viewDiskUsageRadioButton.TabStop = true;
            this.viewDiskUsageRadioButton.Tag = "0";
            this.viewDiskUsageRadioButton.Text = "Show Disk Usage";
            this.viewDiskUsageRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.viewDiskUsageRadioButton.UseVisualStyleBackColor = true;
            // 
            // viewModsSizeRadioButton
            // 
            this.viewModsSizeRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.viewModsSizeRadioButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.viewModsSizeRadioButton.Location = new System.Drawing.Point(3, 53);
            this.viewModsSizeRadioButton.Name = "viewModsSizeRadioButton";
            this.viewModsSizeRadioButton.Size = new System.Drawing.Size(110, 23);
            this.viewModsSizeRadioButton.TabIndex = 1;
            this.viewModsSizeRadioButton.Tag = "1";
            this.viewModsSizeRadioButton.Text = "Group by Mods";
            this.viewModsSizeRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.viewModsSizeRadioButton.UseVisualStyleBackColor = true;
            // 
            // viewTypeSizeRadioButton
            // 
            this.viewTypeSizeRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.viewTypeSizeRadioButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.viewTypeSizeRadioButton.Location = new System.Drawing.Point(3, 82);
            this.viewTypeSizeRadioButton.Name = "viewTypeSizeRadioButton";
            this.viewTypeSizeRadioButton.Size = new System.Drawing.Size(110, 23);
            this.viewTypeSizeRadioButton.TabIndex = 4;
            this.viewTypeSizeRadioButton.Tag = "2";
            this.viewTypeSizeRadioButton.Text = "Group by File Type";
            this.viewTypeSizeRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.viewTypeSizeRadioButton.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.sizeViewRadioGroupPanel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(857, 556);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.loadingChartPanel);
            this.panel1.Controls.Add(this.sizeChart);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(729, 511);
            this.panel1.TabIndex = 6;
            // 
            // panel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel2, 2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(851, 33);
            this.panel2.TabIndex = 7;
            // 
            // SFMCleanerWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(857, 556);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MenuText = "SFM&Cleaner";
            this.Name = "SFMCleanerWidget";
            this.TabText = "SFMCleaner";
            this.Text = "SFMCleaner";
            this.loadingChartPanel.ResumeLayout(false);
            this.loadingChartPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeChart)).EndInit();
            this.sizeViewRadioGroupPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart sizeChart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Controls.RadioGroupPanel sizeViewRadioGroupPanel;
        private System.Windows.Forms.RadioButton viewDiskUsageRadioButton;
        private System.Windows.Forms.RadioButton viewModsSizeRadioButton;
        private System.Windows.Forms.Label label3;
        private Controls.InvisibleControl invisibleControl1;
        private System.Windows.Forms.RadioButton viewTypeSizeRadioButton;
        private System.Windows.Forms.Panel loadingChartPanel;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer loadingUpdateTimer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}