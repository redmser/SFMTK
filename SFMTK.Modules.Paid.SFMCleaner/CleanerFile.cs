﻿using System;
using System.IO;
using System.IO.Filesystem.Ntfs;

namespace SFMTK.Modules.Paid.SFMCleaner
{
    /// <summary>
    /// Represents a file to be handled by SFMCleaner. Caches most properties after the first get.
    /// </summary>
    public class CleanerFile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CleanerFile"/> class.
        /// </summary>
        /// <param name="node">The <see cref="INode"/> instance as the most data currently retrieved.</param>
        public CleanerFile(INode node)
        {
            if ((node.Attributes & Attributes.Directory) == 0)
            {
                //File
                this.FullPath = IOHelper.GetStandardizedPath(node.FullName);
                this.FileSize = node.Size;
                this.ChangedDate = node.LastChangeTime;
            }
            else
            {
                //Folder
                this.FullPath = IOHelper.GetStandardizedPath(node.FullName);
                this.FileSize = 0;
                this.ChangedDate = node.LastChangeTime;
                this.Info = new DirectoryInfo(this._fullPath);
                this.IsFolder = true;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CleanerFile" /> class.
        /// </summary>
        /// <param name="path">The full or local path to the file as the most data currently retrieved.</param>
        /// <param name="isLocal">If set to <c>true</c>, <paramref name="path" /> is a local path and not a full path.</param>
        /// <param name="isStandardized">If set to <c>true</c>, <paramref name="path"/> is already a standardized path.</param>
        public CleanerFile(string path, bool isLocal = false, bool isStandardized = false)
        {
            if (isLocal)
                this.LocalPath = isStandardized ? path : IOHelper.GetStandardizedPath(path);
            else
                this.FullPath = isStandardized ? path : IOHelper.GetStandardizedPath(path);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CleanerFile"/> class.
        /// </summary>
        /// <param name="path">A <see cref="FileSystemInfo"/> of the file as the most data currently retrieved.</param>
        public CleanerFile(FileSystemInfo info)
        {
            this.Info = info;
            if (info is DirectoryInfo)
            {
                //Can't have certain properties as folder
                this.FileSize = 0;
                this.IsFolder = true;
            }
        }

        /// <summary>
        /// Gets whether this <see cref="CleanerFile"/> points to a folder.
        /// </summary>
        public bool IsFolder { get; }

        /// <summary>
        /// Gets or sets the <see cref="T:System.IO.FileInfo"/> pointing to this file.
        /// </summary>
        public FileSystemInfo Info
        {
            get
            {
                if (this._info == null)
                {
                    //Create fileinfo using full path
                    this._info = new FileInfo(this.FullPath);
                }
                return this._info;
            }
            set => this._info = value;
        }
        private FileSystemInfo _info;

        /// <summary>
        /// Gets or sets the full path to this file.
        /// </summary>
        public string FullPath
        {
            get
            {
                if (this._fullPath == null)
                {
                    //Get full path using local path
                    if (this._localPath != null)
                        this._fullPath = SFM.GetFullPath(this._localPath);
                    else
                        this._fullPath = IOHelper.GetStandardizedPath(this._info.FullName);
                }
                return this._fullPath;
            }
            set => this._fullPath = value;
        }
        private string _fullPath;

        /// <summary>
        /// Gets or sets the local path to this file.
        /// </summary>
        public string LocalPath
        {
            get
            {
                if (this._localPath == null)
                {
                    //Get local path using full path
                    if (this._fullPath != null)
                        this._localPath = SFM.GetLocalPath(this._fullPath);
                }
                return this._localPath;
            }
            set => this._localPath = value;
        }
        private string _localPath;

        /// <summary>
        /// Gets or sets the size of this file.
        /// </summary>
        public FileSize FileSize
        {
            get
            {
                if (!this._fileSize.HasValue)
                {
                    //Retrieve filesize from fileinfo
                    this._fileSize = (ulong)(this.Info as FileInfo).Length;
                }
                return this._fileSize.Value;
            }
            set => this._fileSize = value;
        }
        private FileSize? _fileSize;

        /// <summary>
        /// Gets or sets the last write time to this file, in UTC.
        /// </summary>
        public DateTime ChangedDate
        {
            get
            {
                if (this._changedDate == DateTime.MinValue)
                {
                    //Retrieve changed date from fileinfo
                    this._changedDate = this.Info.LastWriteTimeUtc;
                }
                return this._changedDate;
            }
            set => this._changedDate = value;
        }
        private DateTime _changedDate;
    }
}
