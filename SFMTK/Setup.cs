﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace SFMTK
{
    /// <summary>
    /// A SFM launch setup.
    /// </summary>
    [Serializable]
    public class Setup
    {
        //NYI: Setup - show progress for setup apply and launch

        /// <summary>
        /// Initializes a new instance of the <see cref="Setup"/> class with an auto-generated name.
        /// </summary>
        public Setup() : this(null)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Setup"/> class with the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        public Setup(string name)
        {
            if (name == null)
                this.Name = StringHelper.GetNewUniqueName("Setup", null, Properties.Settings.Default.Setups.Select(s => s.Name));
            else
                this.Name = name;
        }

        /// <summary>
        /// Applies all settings of this setup to the SFM workspace. Be sure to call this even if the Active Setup is already set to this one, since the Setup might have changed meanwhile.
        /// </summary>
        public void Apply()
        {
            //Set active setup
            Properties.Settings.Default.ActiveSetup = this.Name;
        }

        /// <summary>
        /// Launches SFM using the launch settings assigned to this setup. The setup is NOT applied beforehand, meaning no changes are done to SFM's workspace!
        /// </summary>
        /// <param name="addparams">Additional launch parameters to pass to SFM.</param>
        public Process Launch(string addparams = "")
        {
            //Launch sfm.exe
            if (!File.Exists(SFM.ExecutablePath))
            {
                //Could not launch sfm.exe, not found
                FallbackTaskDialog.ShowDialog("Unable to launch Source Filmmaker: sfm.exe could not be found!" +
                    "\n\nPlease check the settings for the specified path to your installation of Source Filmmaker.", null, "Error launching SFM",
                    FallbackDialogIcon.Error, new FallbackDialogButton(System.Windows.Forms.DialogResult.OK));
            }

            //TODO: Setup - pass launch parameters specified by Setup itself as well
            return Process.Start(SFM.ExecutablePath, addparams);
        }

        /// <summary>
        /// Launches the specified ProcessStartInfo that points to SFM.
        /// </summary>
        public Process Launch(ProcessStartInfo psi)
        {
            var sfm = Process.Start(psi);
            if (sfm == null)
            {
                //Could not launch sfm.exe, not an executable
                FallbackTaskDialog.ShowDialog("Unable to launch Source Filmmaker: sfm.exe is not an executable!", null, "Error launching SFM",
                    FallbackDialogIcon.Error, new FallbackDialogButton(System.Windows.Forms.DialogResult.OK));
            }
            return sfm;
        }

        /// <summary>
        /// Gets or sets the user-given name of this setup.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets a default Setup instance.
        /// </summary>
        public static Setup Default { get; } = new Setup("Default");

        /// <summary>
        /// Gets the currently active Setup instance.
        /// </summary>
        public static Setup Current => Properties.Settings.Default.Setups?.FirstOrDefault(s => s.Name == Properties.Settings.Default.ActiveSetup) ?? Default;
    }
}
