﻿namespace SFMTK.CommandLine
{
    /// <summary>
    /// --launcher
    /// </summary>
    public static class LauncherCommands
    {
        public static void RunLauncher()
        {
            //Launcher is invalid in command-mode
            if (Program.Options.CommandMode)
            {
                Program.ShowCommandModeGuiError("launcher");
                return;
            }

            //Needs to quick-load some settings here
            Properties.Settings.LoadThemeSettings();

            //Show the launcher
            var entry = Forms.LauncherSelector.ShowLauncher();

            //Expected behavior: close the launcher and launch whatever was selected
            Program.Options.QuitAfterParse = true;

            if (entry is SFMTKLauncherEntry sle)
            {
                //NYI: LauncherCommands - Special case: can continue startup procedure, but append new command-line flags first
            }
            else
            {
                //Launch whatever
                entry?.Launch();
            }
        }
    }
}
