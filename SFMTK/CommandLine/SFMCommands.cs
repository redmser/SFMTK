﻿using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SFMTK.CommandLine
{
    /// <summary>
    /// --sfm-launch and --sfm-parameters
    /// </summary>
    public static class SFMCommands
    {
        private static string _sfmparameters;

        public static void RunSFMParameters(string parameters)
        {
            _sfmparameters = parameters;
        }

        public static void RunSFMLaunch()
        {
            //Launch sfm with parameters of setup and sfmparameters
            NLog.LogManager.GetCurrentClassLogger().Info("Launching SFM (Setup: {0})", Properties.Settings.Default.ActiveSetup);
            NLog.LogManager.GetCurrentClassLogger().Info("Additional arguments: {0}", _sfmparameters);

            //Needs to quick-load some settings here
            Properties.Settings.LoadThemeSettings();

            //Init progress window
            var progress = ProgressInfo.ShowProgress("Launching SFM",
                $"Preparing to launch Source Filmmaker...\n\nSetup: {Properties.Settings.Default.ActiveSetup}",
                true, new ProgressValue(true, ProgressBarState.Default));

            Process sfm = null;
            progress.Cancel.Token.Register(() =>
            {
                //On cancel, try closing SFM in advance
                if (sfm != null && !sfm.HasExited)
                {
                    //Try closing process since user didn't want to launch
                    sfm.Kill();
                }

                //Stop the message loop
                Application.Exit();
            });

            Task.Run(() =>
            {
                //Launch sfm
                sfm = Setup.Current.Launch(_sfmparameters);

                //Give the user a chance to cancel the launch
                System.Threading.Thread.Sleep(2000);

                //Stop the message loop
                Application.Exit();
            });

            //Allow the progress window to update
            Application.Run(progress.Window);

            //Once exited, finish up
            progress.FinishTask();
        }
    }
}
