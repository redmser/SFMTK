﻿using NLog;
using OrderCLParser;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SFMTK.CommandLine
{
    /// <summary>
    /// Containing class for all command line options to be specified by the user.
    /// </summary>
    public class CommandLineOptions
    {
        // Notes to anyone looking to extend SFMTK's command-line argument system:
        //  - New flags and options are to be added to the AllTokens list initializer (or using modules).
        //  - Anything non-positional is handled in ApplyOptions, order should not matter for these settings, and if possible they should be modifyable in the GUI as well.
        //  - Anything positional is handled in RunCommands (run even if not in CommandMode!), in order of how they were specified on the command-line.
        //    This should include commands, actions or property changes that can have an influence of later commands.
        //    If command mode is enabled, the program closes after all commands are run. Otherwise, the user interface initializes afterwards.

        /// <summary>
        /// Starts SFMTK in command mode - never displaying any GUI.
        /// </summary>
        public bool CommandMode { get; set; }

        /// <summary>
        /// Pauses before quitting command mode application.
        /// </summary>
        public bool PauseBeforeExit { get; set; }

        /// <summary>
        /// Whether to quit after finishing running commands.
        /// </summary>
        public bool QuitAfterParse { get; set; }

        /// <summary>
        /// Whether debug mode is enabled.
        /// </summary>
        public bool DebugMode { get; set; }
        //TODO: CommandLineOptions - more functionality for DebugMode

        /// <summary>
        /// Gets the tokens that were parsed.
        /// </summary>
        public List<CLToken> Tokens { get; } = new List<CLToken>();

        /// <summary>
        /// Gets all known command line tokens.
        /// </summary>
        public List<CLToken> AllTokens { get; } = new List<CLToken>
        {
            //Core flags and options (should always be first)
            new CLFlag('h', "help", false, "Shows command-line help"),
            new CLFlag('c', "command-mode", false, "Don't load user interface, use CLI instead.\nInclude twice (-cc) to wait for key press\nbefore closing."),
            new CLFlag('q', "quit", false, "Quit SFMTK after parsing all commandline flags\nUnlike command-mode, does not show cmd window\nand allows for UI elements to be shown."),
            new CLOption('m', "load-modules", true, null, "Loads the specified modules.\nSeparate multiple modules with slashes '/'."),
            new CLOption('\0', "unload-modules", true, null, "Unloads the specified modules.\nSeparate multiple modules with slashes '/'."),
            new CLFlag('\0', "debug", false, "Enables certain debug options in UI."),
            //Non-positional flags

            //Non-positional options

            //Positional flags
            new CLFlag('L', "launcher", true, "Opens a dialog to select a setup to launch\nSFM with, to open SFMTK or custom programs"),
            new CLFlag('l', "sfm-launch", true, "Launches SFM, reads previous --sfm-parameters"),
            new CLFlag('\0', "prompt", true, "Any files to be opened by SFMTK will spawn an\nappropriate prompting dialog deciding how to\nhandle the file before opening"),
            new CLFlag('\0', "install", true, $"If archives or {Constants.SFMTKExtension} files are specified,\nstarts installing them instead of opening SFMTK"),
            //Positional options
            new CLOption('s', "setup", true, null, "Applies the specified setup (by name)\nto the SFM install"),
            new CLOption('\0', "sfm-parameters", true, "", "Parameters to pass to SFM's launch"),
        };

        private static Logger _logger = LogManager.GetLogger("CommandLine");

        /// <summary>
        /// Adds the specified token to the list of known tokens.
        /// </summary>
        public void RegisterToken(CLToken token)
        {
            //Validate values if needed
            if (token is CLFlag flag)
                this.CheckDuplicateTokens(flag.ShortName, flag.Name);

            this.AllTokens.Add(token);
        }

        /// <summary>
        /// Adds the specified tokens to the list of known tokens.
        /// </summary>
        public void RegisterToken(IEnumerable<CLToken> tokens)
        {
            foreach (var token in tokens)
                this.RegisterToken(token);
        }

        /// <summary>
        /// Removes the specified token from the list of known tokens.
        /// </summary>
        public void UnregisterToken(CLToken token) => this.AllTokens.Remove(token);

        /// <summary>
        /// Removes the specified tokens from the list of known tokens.
        /// </summary>
        public void UnregisterToken(IEnumerable<CLToken> tokens)
        {
            foreach (var token in tokens)
                this.UnregisterToken(token);
        }

        private void CheckDuplicateTokens(char shortName, string longName)
        {
            if (shortName != '\0' && this.AllTokens.OfType<CLFlag>().Any(f => f.ShortName == shortName))
                throw new ArgumentException($"A flag with the short name -{shortName} already exists. Cannot register flag --{longName}!", nameof(shortName));

            if (this.AllTokens.OfType<CLFlag>().Any(f => f.Name == longName))
                throw new ArgumentException($"A flag with the long name -{longName} already exists. Cannot register flag!", nameof(longName));
        }

        /// <summary>
        /// Applies all command-line options that are not positional, but only actual option specifiers. Those are removed from the tokens list as well.
        /// <para/>Returns whether execution should continue.
        /// </summary>
        public bool ApplyOptions()
        {
            var tokens = this.Tokens.OfType<CLFlag>().Where(f => !f.Positional).ToList();
            
            //Is help in there?
            //TODO: CommandLineOptions - make --help a positional token, as if you for example want to get help about a module's commands,
            //  you can't since it isn't loaded. Speaking of which: allow to get help on a module's command-line arguments only somehow
            if (tokens.Any(t => t.Name == "help"))
            {
                //Has a parameter after it? That has to be the thing you wanna get help about!
                NativeMethods.ShowConsoleWindow();

                var helper = this.Tokens.OfType<CLImpliedValue>().FirstOrDefault();
                if (helper != null)
                {
                    var name = helper.Value.ToLowerInvariant().Replace("-", "").Replace(" ", "");
                    var found = this.AllTokens.FirstOrDefault(t => t.Name == name || (name.Length == 1 && t is CLFlag f && f.ShortName == name[0]));
                    if (found != null)
                    {
                        //Show that thing's help and heck it!
                        this.ShowHelp(found);
                    }
                    else
                    {
                        //NOOO
                        _logger.Warn($"Could not show help for flag or option \"{name}\", does not exist.");
                    }
                    return false;
                }

                //Show it and heck the program!
                this.ShowHelp();

                return false;
            }

            //Known options
            ParseFlag("command-mode", (v) => { this.PauseBeforeExit = v > 1; this.CommandMode = v > 0; });
            ParseFlag("quit", (v) => this.QuitAfterParse = v > 0);
            ParseFlag("debug", (v) => this.DebugMode = v > 0);

            //Check if any loaded module define their own options
            ModuleCommands.ParseModuleOptions(tokens);

            return true;



            void ParseFlag(string name, Action<int> action)
            {
                var toks = tokens.Where(t => t.Name == name).ToList();
                action(toks.Count);
                foreach (var tok in toks)
                    this.Tokens.Remove(tok);
            }

            void ParseOption(string name, Action<string> action)
            {
                var tok = (CLOption)tokens.FirstOrDefault(t => t.Name == name);
                action(tok.Value);
                if (tok != null)
                    this.Tokens.Remove(tok);
            }
        }

        /// <summary>
        /// Prints a general help string for all tokens to the console.
        /// </summary>
        public void ShowHelp()
        {
            Program.PrintLine($"{Program.GetExecutableName()} [OPTIONS] [FILES]\n\nList of options:");
            foreach (var tok in this.AllTokens)
            {
                this.ShowHelp(tok);
            }
        }

        /// <summary>
        /// Prints a help string for the specified token.
        /// </summary>
        /// <param name="token">Token to show help for.</param>
        public void ShowHelp(CLToken token)
        {
            string pre;
            if (token is CLFlag flag)
            {
                pre = $"--{token.Name}{(flag.ShortName != '\0' ? " or -" + flag.ShortName : "")}";
                if (token is CLOption opt)
                {
                    if (opt.Value == null)
                    {
                        //No value
                        pre += " <VALUE>";
                    }
                    else
                    {
                        //Default value
                        pre += " [VALUE]";
                    }
                }
            }
            else
                pre = $"--{token.Name}";

            pre += new string(' ', Math.Max(1, Console.BufferWidth / 3 - pre.Length));
            Program.PrintLine($"{pre}{token.Description.Trim().Replace("\n", Environment.NewLine + new string(' ', Console.BufferWidth / 3))}");
        }

        /// <summary>
        /// Runs all commands specified. For use both in command mode and outside.
        /// </summary>
        public void RunCommands()
        {
            foreach (var tok in this.Tokens)
            {
                var opt = tok as CLOption;

                switch (tok.Name)
                {
                    case "load-modules":
                        ModuleCommands.RunLoadModules(opt.Value);
                        break;
                    case "unload-modules":
                        ModuleCommands.RunUnloadModules(opt.Value);
                        break;
                    case "setup":
                        SetupCommands.RunSetup(opt.Value);
                        break;
                    case "launcher":
                        LauncherCommands.RunLauncher();
                        break;
                    case "sfm-parameters":
                        SFMCommands.RunSFMParameters(opt.Value);
                        break;
                    case "sfm-launch":
                        SFMCommands.RunSFMLaunch();
                        break;
                    case "install":
                        //NYI: CommandLineOptions --install
                        throw new NotImplementedException();
                    case "prompt":
                        //NYI: CommandLineOptions --prompt
                        throw new NotImplementedException();
                    default:
                        //Check if any loaded module defines this token
                        foreach (var module in Program.MainMM.LoadedModules.OfType<Modules.IModuleTokens>())
                        {
                            if (module.Tokens.Any(t => t.Name == tok.Name))
                                module.ParseCommandToken(tok);
                        }
                        break;
                }
            }
        }
    }
}
