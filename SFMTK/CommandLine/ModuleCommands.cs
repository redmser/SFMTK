﻿using System.Collections.Generic;
using System.Linq;
using OrderCLParser;
using SFMTK.Modules;

namespace SFMTK.CommandLine
{
    /// <summary>
    /// --load-modules and --unload-modules
    /// </summary>
    public static class ModuleCommands
    {
        //UNTESTED: ModuleCommands - load-modules and unload-modules, as well as using commands/options of them afterwards

        public static void RunLoadModules(string modules)
        {
            var names = modules.Split('/');
            foreach (var name in names)
            {
                //Check if module with this name exists
                var mod = Program.MainMM.LoadedModules.FirstOrDefault(m => m.GetName() == name);
                if (mod == null)
                {
                    //Could not find module, fatal
                    NLog.LogManager.GetCurrentClassLogger().Error("No module \"{0}\" exists, can't load.", name);
                    Program.ExitCode = ExitCodes.ModuleNotFound;
                    return;
                }

                //Load this module
                var success = Program.MainMM.LoadModule(mod, ModuleLoadingContext.Command, false);

                //If it defines any non-positional arguments, load them now
                ParseModuleOptions(Program.Options.Tokens.OfType<CLFlag>().Where(f => !f.Positional).ToList());
            }
        }

        public static void RunUnloadModules(string modules)
        {
            var names = modules.Split('/');
            foreach (var name in names)
            {
                //Check if module with this name is loaded
                var mod = Program.MainMM.LoadedModules.FirstOrDefault(m => m.GetName() == name);
                if (mod == null)
                {
                    //Could not find module
                    NLog.LogManager.GetCurrentClassLogger().Warn("No module \"{0}\" is loaded, can't unload.", name);
                    continue;
                }

                //Unload this module
                var success = Program.MainMM.UnloadModule(mod, ModuleUnloadingContext.Command, false);
            }
        }

        public static void ParseModuleOptions(IEnumerable<CLToken> tokens)
        {
            foreach (var module in Program.MainMM.LoadedModules.OfType<IModuleTokens>())
            {
                foreach (var token in module.Tokens.Where(t => tokens.Any(k => k.Name == t.Name)))
                {
                    module.ParseOptionToken(token);
                    Program.Options.Tokens.Remove(Program.Options.Tokens.Find(t => t.Name == token.Name));
                }
            }
        }
    }
}
