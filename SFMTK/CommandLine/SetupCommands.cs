﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace SFMTK.CommandLine
{
    /// <summary>
    /// --setup
    /// </summary>
    public static class SetupCommands
    {
        public static void RunSetup(string setupname)
        {
            //Progress setup
            var marquee = new ProgressValue(true, ProgressBarState.Default);
            var setupprogress = ProgressInfo.ShowProgress("Applying setup...", $"Applying setup {setupname}...", false, marquee);

            //NYI: SetupOptions - will need to move setup part to a task, since UI handling with Application.Run is impossible here
            //  -> won't it be better to 1) toss this behaviour into separate static classes and
            //     2) have a better framework to work with App.Run and App.Exit instead of manually doing so each time??

            Task.Run(() =>
            {
                while (ProgressInfo.IsBusy)
                {
                    setupprogress.ReportProgress(marquee);
                    System.Threading.Thread.Sleep(75);
                }
            });

            //Apply setup, check if exists
            var setup = Properties.Settings.Default.Setups.FirstOrDefault(s => s.Name == setupname);
            if (setup != null)
            {
                try
                {
                    setup.Apply();

                    NLog.LogManager.GetCurrentClassLogger().Info("Successfully applied setup!");
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error(ex, "Unable to apply setup!");
                    Program.ExitCode = ExitCodes.InvalidFunction;
                    setupprogress.FinishTask();
                    return;
                }
            }
            else
            {
                NLog.LogManager.GetCurrentClassLogger().Error("The specified setup \"{0}\" does not exist!", setupname);
                Program.ExitCode = ExitCodes.FileNotFound;
            }

            setupprogress.FinishTask();
        }
    }
}
