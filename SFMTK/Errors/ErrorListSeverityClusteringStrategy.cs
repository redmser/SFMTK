﻿using BrightIdeasSoftware;

namespace SFMTK.Errors
{
    /// <summary>
    /// Clustering strategy for error list severity column.
    /// </summary>
    public class ErrorListSeverityClusteringStrategy : ClusteringStrategy
    {
        public override object GetClusterKey(object model)
        {
            if (!(model is ContentError err))
                return null;
            return err.Severity;
        }

        public override string GetClusterDisplayLabel(ICluster cluster)
        {
            var severity = (ContentErrorSeverity)cluster.ClusterKey;
            string prefix;
            switch (severity)
            {
                case ContentErrorSeverity.Error:
                    prefix = "Error";
                    break;
                case ContentErrorSeverity.Warning:
                    prefix = "Warning";
                    break;
                case ContentErrorSeverity.Information:
                    prefix = "Information";
                    break;
                default:
                    prefix = "Unknown";
                    break;
            }

            return $"{prefix} ({StringHelper.GetPlural(cluster.Count, "entry", "entries")})";
        }
    }
}
