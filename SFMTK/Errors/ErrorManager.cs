﻿using System;
using System.Collections.Generic;
using SFMTK.Contents;
using SFMTK.Controls.ContentWidgets;
using SFMTK.Data;
using SFMTK.Widgets;

namespace SFMTK.Errors
{
    /// <summary>
    /// Keeps track of all errors of the current workspace.
    /// </summary>
    public class ErrorManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorManager"/> class.
        /// </summary>
        /// <param name="dpm">The <see cref="SFMTK.DockPanelManager"/> that should be used for context-dependent error management.</param>
        public ErrorManager(DockPanelManager dpm) => this.DockPanelManager = dpm;

        /// <summary>
        /// Gets or sets the <see cref="SFMTK.DockPanelManager"/> that this <see cref="ErrorManager"/>
        /// should use for context-dependent error management.
        /// </summary>
        public DockPanelManager DockPanelManager { get; set; }

        /// <summary>
        /// Gets all errors related to the current window context.
        /// </summary>
        public IEnumerable<ContentError> GetErrors()
        {
            //TODO: ErrorManager - different settings for getting errors (such as: only selected document versus all open documents)
            foreach (var c in this.DockPanelManager.OpenContent)
            {
                foreach (var e in GetErrors(c))
                {
                    yield return e;
                }
            }
        }

        /// <summary>
        /// Occurs when the list of errors has changed.
        /// </summary>
        public event EventHandler ErrorsChanged;

        /// <summary>
        /// Raises the <see cref="E:ErrorsChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public virtual void OnErrorsChanged(object sender, EventArgs e) => this.ErrorsChanged?.Invoke(sender, e);

        /// <summary>
        /// Gets all errors of the specified <see cref="ErrorHandler" />, given a corresponding data instance.
        /// </summary>
        /// <param name="errorhandler">The error handler to get errors for.</param>
        /// <param name="data">The data to check for errors.</param>
        public static IEnumerable<ContentError> GetErrors(ErrorHandler errorhandler, IData data) => errorhandler.Errors;

        /// <summary>
        /// Gets all errors of the specified <see cref="Content"/>.
        /// </summary>
        /// <param name="content">The content to get the errors of.</param>
        public static IEnumerable<ContentError> GetErrors(Content content) => content.Errors;

        /// <summary>
        /// Gets all errors of the specified <see cref="ContentWidget"/>.
        /// </summary>
        /// <param name="contentwidget">The content widget to get the errors of.</param>
        public static IEnumerable<ContentError> GetErrors(ContentWidget contentwidget) => contentwidget.Content.Errors;

        /// <summary>
        /// Gets all errors of the specified <see cref="ContentInfoWidget"/>.
        /// </summary>
        /// <param name="contentinfowidget">The content info widget to get the errors of.</param>
        public static IEnumerable<ContentError> GetErrors(ContentInfoWidget contentinfowidget) => contentinfowidget.Content.Errors;
    }
}
