﻿using BrightIdeasSoftware;

namespace SFMTK.Errors
{
    public class ErrorListCodeClusteringStrategy : ClusteringStrategy
    {
        public override object GetClusterKey(object model)
        {
            if (!(model is ContentError err))
                return null;
            return err.ID;
        }

        public override string GetClusterDisplayLabel(ICluster cluster)
        {
            var severity = (string)cluster.ClusterKey;
            return $"{severity} ({StringHelper.GetPlural(cluster.Count, "entry", "entries")})";
        }
    }
}
