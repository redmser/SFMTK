﻿using BrightIdeasSoftware;

namespace SFMTK.Errors
{
    /// <summary>
    /// Model filter for the error list.
    /// </summary>
    public class ErrorListFilter : IModelFilter
    {
        public ErrorListFilter(string filtertext = null)
        {
            this.FilterText = filtertext.Trim();
        }

        /// <summary>
        /// Should the given model be included when this filter is installed
        /// </summary>
        /// <param name="modelObject">The model object to consider</param>
        /// <returns>
        /// Returns true if the model will be included by the filter
        /// </returns>
        public bool Filter(object modelObject)
        {
            //Don't show anything that is not an error
            if (!(modelObject is ContentError err))
                return false;

            //Check if we are hidden
            if (err.Severity == ContentErrorSeverity.Hidden)
                return false;

            //Check if we have to filter
            if (!string.IsNullOrEmpty(this.FilterText))
            {
                //We do... check text and description
                if (!err.Text.ContainsIgnoreCase(this.FilterText) && !err.Description.ContainsIgnoreCase(this.FilterText))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Gets or sets the text to filter the error list for.
        /// </summary>
        public string FilterText { get; set; }
    }
}
