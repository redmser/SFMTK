﻿using System;

namespace SFMTK.Errors
{
    /// <summary>
    /// An attribute targeting an <see cref="ErrorHandler{TData}"/>, allowing it to automatically
    /// attach to a <see cref="Contents.Content"/> instance of that type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class ErrorHandlerContentTypeAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorHandlerContentTypeAttribute"/> class.
        /// </summary>
        /// <param name="types">The types of <see cref="Contents.Content"/> that this <see cref="ErrorHandler{TData}"/>
        /// should automatically attach to. Ensure that the generic type allows for this as well.</param>
        public ErrorHandlerContentTypeAttribute(params Type[] types) : base()
        {
            this.Types = types;
        }

        /// <summary>
        /// Gets the types of <see cref="Contents.Content"/> that this <see cref="ErrorHandler{TData}"/>
        /// should automatically attach to.
        /// </summary>
        public Type[] Types { get; }
    }
}
