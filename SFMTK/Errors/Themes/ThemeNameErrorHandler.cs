﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SFMTK.Errors.Themes
{
    /// <summary>
    /// Handles invalid UITheme names.
    /// </summary>
    [ErrorHandlerContentType(typeof(Contents.ThemeContent))]
    public class ThemeNameErrorHandler : ErrorHandler
    {
        /// <summary>
        /// Retrieves the list of <see cref="T:SFMTK.Errors.ContentError" /> instances that the data causes, without updating the error list cache.
        /// <para />The data instance will never be null for this call!
        /// </summary>
        public override IEnumerable<ContentError> GetErrors()
        {
            var theme = this.Data as UITheme;
            var name = theme.Name;

            //No name at all
            if (string.IsNullOrWhiteSpace(name))
            {
                yield return new ContentError("A theme's name may not be empty.", ContentErrorSeverity.Error, id: "ThemeNameEmpty");
                yield break;
            }

            //Invalid characters
            var invchar = name.FirstOrDefault(c => Path.GetInvalidFileNameChars().Contains(c));
            if (invchar != '\0')
            {
                yield return ContentError.ErrorInvalidFilename(invchar, "theme's name", "ThemeNameInvalid");
            }

            //Duplicate name
            //FIXME: ThemeNameErrorHandler - how to handle duplicate names? overwrite? disallow saving? ask what to do on save?
            if (UITheme.Themes.Any(t => t.Name == name))
            {
                yield return new ContentError($"A theme with the name \"{name}\" already exists.", ContentErrorSeverity.Warning, id: "ThemeNameDuplicate");
            }
        }
    }
}
