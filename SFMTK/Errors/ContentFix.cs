﻿using System.Drawing;

namespace SFMTK.Errors
{
    /// <summary>
    /// How a <see cref="ContentError"/> can be fixed.
    /// </summary>
    public class ContentFix
    {
        //TODO: ContentFix - show in error list, add shortcuts for executing the n-th fix

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentFix"/> class.
        /// </summary>
        public ContentFix()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentFix"/> class.
        /// </summary>
        /// <param name="text">Main text to display in association to this fix.</param>
        /// <param name="description">A short description about this fix, what it will do to fix the error.</param>
        /// <param name="command">The command that will be executed as a fix.</param>
        /// <param name="image">A display image for this fix instance, sized 16x16.</param>
        public ContentFix(string text, string description, Commands.CommandBase command, Image image = null) : this()
        {
            this.Text = text;
            this.Description = description;
            this.Command = command;
            this.Image = image;
        }

        /// <summary>
        /// Gets the main text to display in association to this fix.
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Gets or sets a short description about this fix, what it will do to fix the error.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets a display image for this fix instance, sized 16x16.
        /// </summary>
        public Image Image { get; set; }

        /// <summary>
        /// Gets or sets the command that will (by default) be executed as a fix.
        /// </summary>
        public Commands.CommandBase Command { get; set; }

        /// <summary>
        /// Executes the fix, returns whether that was done successfully.
        /// </summary>
        public virtual bool Execute()
        {
            var res = Program.MainCH.ExecuteDirectly(this.Command);
            //TODO: ContentFix - show error message to user
            return res == null;
        }
    }
}
