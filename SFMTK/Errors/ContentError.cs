﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using SFMTK.Contents;

namespace SFMTK.Errors
{
    /// <summary>
    /// An error related to a Content's state. Only subclass if additional behavior is necessary.
    /// </summary>
    /// <seealso cref="SFMTK.Contents.Content"/>
    public class ContentError
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentError"/> class.
        /// </summary>
        public ContentError()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentError" /> class.
        /// </summary>
        /// <param name="text">Main error text to display in association to this error.</param>
        /// <param name="severity">How severe this ContentError is. Hidden will make it not show up in the error list, but can still be checked for being in the ErrorList.</param>
        /// <param name="description">A short description about this error, why it was caused, and potentially how it can be fixed.
        /// <para />Does not have to be set to any value.</param>
        /// <param name="line">The line number in the corresponding Content file where the error is occuring.</param>
        /// <param name="id">An identifying string for selecting this ContentError out of the error list. Uses the Text property without spaces by default.</param>
        /// <exception cref="ArgumentNullException">The display text of the error is null.</exception>
        public ContentError(string text, ContentErrorSeverity severity, string description = null, int line = -1, string id = null) : this()
        {
            this.Text = text ?? throw new ArgumentNullException(nameof(text));
            this.ID = id ?? this.Text.Replace(" ", "");
            this.Severity = severity;
            this.Description = description;
            this.LineNumber = line;
        }

        /// <summary>
        /// Gets how severe this ContentError is.
        /// Hidden will make it not show up in the error list, but can still be checked for being in the ErrorList.
        /// </summary>
        public ContentErrorSeverity Severity { get; }

        /// <summary>
        /// Gets the main error text to display in association to this error.
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Gets or sets a short description about this error, why it was caused, and potentially how it can be fixed.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets an identifying string for selecting this ContentError out of the error list.
        /// Uses the Text property without spaces by default.
        /// </summary>
        public string ID { get; }

        /// <summary>
        /// Gets or sets the line number in the corresponding Content file where the error is occuring.
        /// Use -1 to indicate that the error is not associated with any particular line number.
        /// </summary>
        public int LineNumber { get; set; } = -1;

        /// <summary>
        /// Gets or sets the content instance that this error originates from.
        /// This property will be handled by the <see cref="ErrorManager"/> that invoked the <see cref="ErrorHandler"/>.
        /// </summary>
        public Content Content { get; set; }

        /// <summary>
        /// Gets a display image for this error instance, sized 16x16.
        /// </summary>
        public Image Image
        {
            get
            {
                switch (this.Severity)
                {
                    case ContentErrorSeverity.Error:
                        return Properties.Resources.cancel;
                    case ContentErrorSeverity.Warning:
                        return Properties.Resources.error;
                    case ContentErrorSeverity.Information:
                        return Properties.Resources.information;
                    default:
                        return null;
                }
            }
        }

        /// <summary>
        /// Returns a list of fixes that can be applied to fix this error.
        /// The first fix returned will be executed as the default fix.
        /// </summary>
        public virtual IEnumerable<ContentFix> GetFixes() { yield break; }

        /// <summary>
        /// Error to be raised when a string designated as a file name contains any character of the <see cref="Path.GetInvalidFileNameChars"/> array.
        /// </summary>
        /// <param name="invalid">Which character has been found.</param>
        /// <param name="what">Which field the file name is meant to be used in (e.g. "A <c>model's name</c> may not contain ...").</param>
        /// <param name="id">The ID of the error.</param>
        public static ContentError ErrorInvalidFilename(char invalid, string what = "filename", string id = null)
        {
            return new ContentError($"A {what} may not contain '{invalid}'.", ContentErrorSeverity.Error,
                    $"The following characters are not permitted in file names: {new string(Path.GetInvalidFileNameChars())}",
                    id: id);
        }
    }

    /// <summary>
    /// A list of ContentError instances which can be accessed by index or ID of the error.
    /// </summary>
    public class ContentErrorList : List<ContentError>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentErrorList"/> class.
        /// </summary>
        public ContentErrorList() : base()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentErrorList"/> class.
        /// </summary>
        /// <param name="errors">The errors to populate this list with.</param>
        public ContentErrorList(IEnumerable<ContentError> errors) : base(errors)
        {

        }

        /// <summary>
        /// Gets or sets the ContentError with the specified ID.
        /// </summary>
        /// <param name="id">The ID to look for.</param>
        public ContentError this[string id]
        {
            //FIXME: ContentErrorList - when overriding accessors, check whether LINQ gets are deep enough to allow setting as well
            get => this[this.IndexOf(this.FirstOrDefault(e => e.ID == id))];
            set => this[this.IndexOf(this.FirstOrDefault(e => e.ID == id))] = value;
        }
    }

    /// <summary>
    /// How severe the ContentError should be.
    /// </summary>
    public enum ContentErrorSeverity
    {
        /// <summary>
        /// Something is going to make the Content either not load at all, or be put into an unworkable state.
        /// </summary>
        Error,
        /// <summary>
        /// Something will still allow the Content to work, but may cause issues or unexpected behaviour.
        /// </summary>
        Warning,
        /// <summary>
        /// The user is informed about any improvable styling, shorter ways to format code or useful information about a command.
        /// </summary>
        Information,
        /// <summary>
        /// This error is not visible in the error list, but can be checked for to quickly verify a certain state.
        /// </summary>
        Hidden
    }
}
