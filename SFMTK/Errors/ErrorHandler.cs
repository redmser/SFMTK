﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using SFMTK.Data;

namespace SFMTK.Errors
{
    /// <summary>
    /// Attaches to a <see cref="SFMTK.Contents.Content"/> and emits <see cref="ContentError"/> instances
    /// based on different conditions of the data.
    /// </summary>
    public class ErrorHandler
    {
        /// <summary>
        /// Gets or sets the data object that should be watched.
        /// </summary>
        public virtual IData Data
        {
            get => this._data;
            set
            {
                if (this._data == value)
                    return;

                if (this._data != null)
                    this._data.PropertyChanged -= this.OnDataChanged;

                this._data = value;
                this.OnDataChanged(this, new PropertyChangedEventArgs(nameof(this.Data)));

                if (this._data != null)
                    this._data.PropertyChanged += this.OnDataChanged;
            }
        }
        private IData _data;

        /// <summary>
        /// Retrieves the list of <see cref="ContentError"/> instances that the data causes, without updating the error list cache.
        /// <para/>The data instance will never be null for this call!
        /// </summary>
        public virtual IEnumerable<ContentError> GetErrors()
        {
            yield break;
        }

        /// <summary>
        /// Gets the list of <see cref="ContentError"/> instances that the data causes.
        /// Updates as the underlying data changes, but does not emit events.
        /// </summary>
        public virtual ContentErrorList Errors { get; } = new ContentErrorList();

        /// <summary>
        /// Called when the underlying data changed. Forces a refresh of the error list.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        protected virtual void OnDataChanged(object sender, PropertyChangedEventArgs e)
        {
            //Update the error list if we have data
            //TODO: ErrorHandler - perf issues probably will occur when FULLY updating on ALL data changes, don't live-update error list constantly
            //  -> ideal solution would be to only update the error handlers that listen to certain change events, but that would be pretty
            //  hard to implement (and in some places, such as with raw text files, impossible)
            this.Errors.Clear();
            if (this.Data != null)
                this.Errors.AddRange(this.GetErrors());

            //Inform others
            this.OnErrorsChanged(new ErrorsChangedEventArgs(this.Errors.Any()));
        }

        /// <summary>
        /// Occurs when the list of errors has changed.
        /// </summary>
        public event ErrorsChangedEventHandler ErrorsChanged;

        /// <summary>
        /// Raises the <see cref="E:ErrorsChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="ErrorsChangedEventArgs"/> instance containing the event data.</param>
        protected virtual void OnErrorsChanged(ErrorsChangedEventArgs e) => this.ErrorsChanged?.Invoke(this, e);

        /// <summary>
        /// Gets all <see cref="ErrorHandler{TData}"/> default instances which have the <see cref="ErrorHandlerContentTypeAttribute"/>
        /// set to include the specified <paramref name="contentType"/>.
        /// </summary>
        /// <param name="contentType">The type of <see cref="Contents.Content"/> to get the handlers for.</param>
        public static IEnumerable<ErrorHandler> GetByAttribute(Type contentType)
        {
            if (contentType == null)
                throw new ArgumentNullException(nameof(contentType));

            foreach (var ehtype in Reflection.GetSubTypes(typeof(ErrorHandler), Modules.ModuleScope.LoadedModules))
            {
                var attr = ehtype.GetCustomAttributes(typeof(ErrorHandlerContentTypeAttribute), false).SingleOrDefault();

                //Check for valid type
                if (attr is ErrorHandlerContentTypeAttribute ctattr && ctattr.Types.Contains(contentType))
                {
                    //This error handler is alright, get a default instance
                    yield return (ErrorHandler)Activator.CreateInstance(ehtype);
                }
            }
        }
    }

    public delegate void ErrorsChangedEventHandler(object sender, ErrorsChangedEventArgs e);

    /// <summary>
    /// Event arguments for errors list changed.
    /// </summary>
    public class ErrorsChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorsChangedEventArgs"/> class.
        /// </summary>
        public ErrorsChangedEventArgs(bool haserrors) : base()
        {
            this.HasErrors = haserrors;
        }

        /// <summary>
        /// Gets or sets whether this <see cref="ErrorHandler"/> has any errors loaded.
        /// </summary>
        public bool HasErrors { get; set; }
    }

    /// <summary>
    /// A list of <see cref="IErrorHandler{IData}"/> instances.
    /// </summary>
    public class ErrorHandlerList : IList<ErrorHandler>
    {
        private List<ErrorHandler> _backingList = new List<ErrorHandler>();

        public ErrorHandler this[int index]
        {
            get => ((IList<ErrorHandler>)this._backingList)[index];
            set => ((IList<ErrorHandler>)this._backingList)[index] = value;
        }

        public int Count => ((IList<ErrorHandler>)this._backingList).Count;

        public bool IsReadOnly => ((IList<ErrorHandler>)this._backingList).IsReadOnly;

        public void Add(ErrorHandler item) => this.Add(item, true);

        public void Add(ErrorHandler item, bool notify)
        {
            ((IList<ErrorHandler>)this._backingList).Add(item);

            item.ErrorsChanged += this.OnErrorFound;
            if (notify)
                this.OnErrorFound(this, EventArgs.Empty);
        }

        public void AddRange(IEnumerable<ErrorHandler> items)
        {
            foreach (var item in items)
                this.Add(item, false);
            this.OnErrorFound(this, EventArgs.Empty);
        }

        public void Clear()
        {
            foreach (var item in this._backingList)
                this.Remove(item, false);
            this.OnErrorFound(this, EventArgs.Empty);
        }

        public bool Contains(ErrorHandler item) => ((IList<ErrorHandler>)this._backingList).Contains(item);
        public void CopyTo(ErrorHandler[] array, int arrayIndex) => throw new NotImplementedException(); //((IList<ErrorHandler>)this._backingList).CopyTo(array, arrayIndex);
        public IEnumerator<ErrorHandler> GetEnumerator() => ((IList<ErrorHandler>)this._backingList).GetEnumerator();
        public int IndexOf(ErrorHandler item) => ((IList<ErrorHandler>)this._backingList).IndexOf(item);
        public void Insert(int index, ErrorHandler item)
        {
            ((IList<ErrorHandler>)this._backingList).Insert(index, item);

            item.ErrorsChanged += this.OnErrorFound;
            this.OnErrorFound(this, EventArgs.Empty);
        }
        public bool Remove(ErrorHandler item) => this.Remove(item, true);

        public bool Remove(ErrorHandler item, bool notify)
        {
            if (((IList<ErrorHandler>)this._backingList).Remove(item))
            {
                item.ErrorsChanged -= this.OnErrorFound;

                if (notify)
                    this.OnErrorFound(this, EventArgs.Empty);
                return true;
            }
            return false;
        }
        public void RemoveAt(int index) => this.Remove(this[index]);
        IEnumerator IEnumerable.GetEnumerator() => ((IList<ErrorHandler>)this._backingList).GetEnumerator();

        /// <summary>
        /// Gets the errors of all error handlers in this list.
        /// </summary>
        public ContentErrorList Errors => new ContentErrorList(this.SelectMany(h => h.Errors));

        /// <summary>
        /// Occurs when any error handler has detected a new error.
        /// </summary>
        public event EventHandler ErrorFound;

        /// <summary>
        /// Raises the <see cref="E:ErrorFound" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnErrorFound(object sender, EventArgs e) => this.ErrorFound?.Invoke(sender, e);
    }
}
