﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Commands;
using SFMTK.Properties;

namespace SFMTK
{
    /// <summary>
    /// A layout file specifying how all dockable widgets and toolstrips are positioned.
    /// </summary>
    [Serializable]
    public class DockLayout
    {
        /// <summary>
        /// Path to the "last layout" file that saves whenever SFMTK exits / gets auto-reloaded on startup. Note that it is a relative path, meaning you'll have to do additional calls
        /// to get the absolute path.
        /// </summary>
        public const string LastLayoutFile = LastLayoutName + ".xml";

        /// <summary>
        /// Name of the "last layout" file that saves whenever SFMTK exits / gets auto-reloaded on startup, without extensions.
        /// </summary>
        public const string LastLayoutName = "lastLayout";

        /// <summary>
        /// Gets the list of loaded layouts.
        /// </summary>
        public static List<DockLayout> Layouts { get; } = new List<DockLayout>();

        private string _name;

        /// <summary>
        /// Initializes a new instance of the <see cref="DockLayout"/> class with the specified name and XML path.
        /// </summary>
        /// <param name="name">The name of the layout.</param>
        /// <param name="xmlpath">The xmlpath to the layout's data.</param>
        public DockLayout(string name, string xmlpath)
        {
            this._name = name;
            this._xmlPath = xmlpath;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DockLayout"/> class.
        /// </summary>
        public DockLayout()
        {

        }

        /// <summary>
        /// Loads all docklayouts from the XML files in their default save path to the settings key.
        /// </summary>
        public static void LoadLayouts()
        {
            DockLayout.Layouts.Clear();

            //Read from order list
            foreach (var name in Settings.Default.LayoutOrder)
            {
                try
                {
                    Layouts.Add(DockLayout.FromFilePath(name));
                }
                catch (Exception ex)
                {
                    if (ex is FileNotFoundException)
                    {
                        //Ignore silently, let user worry about this later
                    }
                    else
                    {
                        //UNTESTED: DockLayout load layouts error
                        FallbackTaskDialog.ShowDialogLog("Unable to load the list of window layouts:\n" + ex.Message, null,
                            "Load layouts failed", FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));
                    }
                }
            }

            //TODO: DockLayout - load any layouts which are found in the app dir but are not in the layout order, adding them to that list
        }

        /// <summary>
        /// Returns a new DockLayout instance filled with data from the given filename (excluding directory).
        /// </summary>
        /// <param name="name">The filename to parse. XML file has to exist.</param>
        /// <exception cref="FormatException">Could not parse the specified file as a layout file (filename checked only).</exception>
        /// <exception cref="FileNotFoundException">Could not find XML layout file.</exception>
        private static DockLayout FromFilePath(string name)
        {
            var path = Path.Combine(Application.StartupPath, name);
            if (!File.Exists(path))
                throw new FileNotFoundException("Could not find XML layout file.", path);

            try
            {
                var underscore = name.IndexOf('_');
                return new DockLayout(name.Substring(underscore + 1, name.Length - underscore - 5), path);
            }
            catch (Exception ex)
            {
                string msg;
                if (ex is IndexOutOfRangeException)
                    msg = "Invalid file name format for layout file";
                else
                    msg = "Unexpected error parsing layout file";

                throw new FormatException(msg, ex);
            }
        }

        private string _xmlPath;

        /// <summary>
        /// Gets the path to the layout's XML file.
        /// </summary>
        public string XmlPath
        {
            get => this._xmlPath; set
            {
                if (value == this._xmlPath)
                    return;

                this._xmlPath = value;
                this.OnXmlPathChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets the name of the dock layout to display.
        /// </summary>
        public string Name
        {
            get => this._name; set
            {
                if (value == this._name)
                    return;

                this._name = value;
                this.OnNameChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets the command assigned to this layout.
        /// </summary>
        public CommandBase Command => CommandManager.Commands.OfType<ApplyLayoutCommand>().FirstOrDefault(c => c.Layout == this);

        /// <summary>
        /// Gets or sets the shortcut for selecting this layout.
        /// </summary>
        public Keys Shortcut
        {
            get => this.Command.Shortcut;
            set => this.Command.Shortcut = value;
        }

        /// <summary>
        /// Gets or sets the shortcut string.
        /// </summary>
        public string ShortcutString
        {
            get => CommandManager.StringFromKeys(this.Shortcut);
            set => this.Shortcut = CommandManager.KeysFromString(value);
        }

        /// <summary>
        /// Loads this layout for the main dock panel.
        /// </summary>
        public void LoadLayout() => LoadLayout(Program.MainDPM);

        /// <summary>
        /// Loads this layout for the specified dock panel.
        /// </summary>
        public void LoadLayout(DockPanelManager manager)
        {
            //Check for the file
            if (!File.Exists(this.XmlPath))
            {
                //Missing XML file!
                if (FallbackTaskDialog.ShowDialog($"The file defining the layout \"{this.Name}\" can not be found.\n\n" +
                    "Would you like to remove the layout from the list?", null, "Layout not found", "Full Path: " + this.XmlPath, FallbackDialogIcon.Info,
                    new FallbackDialogButton(DialogResult.Yes), new FallbackDialogButton(DialogResult.No)) == DialogResult.Yes)
                {
                    //Remove layout entry
                    DockLayout.Layouts.Remove(this);
                }
                return;
            }

            //Detach all panels
            manager.CloseAll();

            //Load new layout
            DockLayout.LoadXML(manager, this.XmlPath, this.Name);

            if (Properties.Settings.Default.ToolbarPerLayout)
            {
                //BUG: DockLayout - messes up form on LoadSettings
                //ToolStripManager.LoadSettings(dpm.Form, this.Name);
            }
        }

        /// <summary>
        /// Loads the specified XML file into the given DockPanelManager.
        /// </summary>
        /// <param name="manager">The DockPanelManager to load the layout into.</param>
        /// <param name="xmlpath">Path to the XML file. It is not checked whether this file exists!</param>
        /// <param name="name">Name of the layout, for errors.</param>
        public static void LoadXML(DockPanelManager manager, string xmlpath, string name = "TEMP")
        {
            try
            {
                manager.DockPanel.LoadFromXml(xmlpath, manager.GetFromPersist);
            }
            catch (FormatException ex)
            {
                //UNTESTED: DockLayout persist parse error message
                FallbackTaskDialog.ShowDialogLog($"While trying to load the window layout \"{name}\", unknown data was received and no window could be created from it." +
                    "\n\nYour layout may be missing some windows due to this!", null, "Unable to parse layout data",
                    ex.Message + "\n" + ex.InnerException?.Message, FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));
            }
        }

        /// <summary>
        /// Saves the current dock panel layout for this layout.
        /// </summary>
        public void SaveLayout() => SaveLayout(Program.MainDPM);

        /// <summary>
        /// Saves the current dock panel layout for this layout.
        /// </summary>
        public void SaveLayout(DockPanelManager manager)
        {
            manager.DockPanel.SaveAsXml(this.XmlPath);
            ToolStripManager.SaveSettings(manager.Form, this.Name);
        }

        /// <summary>
        /// Occurs when the XML path changed.
        /// </summary>
        public event EventHandler XmlPathChanged;
        /// <summary>
        /// Occurs when the layout's shortcut changed.
        /// </summary>
        public event EventHandler ShortcutChanged;

        /// <summary>
        /// Occurs when the layout's name changed.
        /// </summary>
        public event EventHandler NameChanged;

        /// <summary>
        /// Called when the XML path changed.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnXmlPathChanged(EventArgs e) => this.XmlPathChanged?.Invoke(this, e);

        /// <summary>
        /// Called when the layout's shortcut changed.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnShortcutChanged(EventArgs e) => this.ShortcutChanged?.Invoke(this, e);

        /// <summary>
        /// Called when the layout's name changed.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnNameChanged(EventArgs e) => this.NameChanged?.Invoke(this, e);
    }
}
