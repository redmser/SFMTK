﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SFMTK.Parsing.KeyValues
{
    /// <summary>
    /// Tokenizer and parser for KeyValues data.
    /// </summary>
    public class KeyValuesTokenizer
    {
        //TODO: KeyValuesTokenizer - lazy-loading of KV files (only return the tokens we are looking for)

        /// <summary>
        /// The default buffer size for reading operations.
        /// </summary>
        public const int DefaultBufferSize = 8192;

        /// <summary>
        /// Reads KeyValues data and stores the result in the internal token list.
        /// </summary>
        /// <param name="data">The data to read.</param>
        public virtual void Read(byte[] data)
        {
            //Delegate reading job to our ruleset
            this.Rules.Read(this.Tokens, data);
        }

        /// <summary>
        /// Reads KeyValues data and stores the result in the internal token list.
        /// </summary>
        /// <param name="data">The data to read.</param>
        /// <param name="index">Where to start reading.</param>
        /// <param name="count">How many bytes to read.</param>
        public virtual void Read(byte[] data, int index, int count)
        {
            //Get the byte array to read
            var readdata = new byte[count];
            Array.Copy(data, index, readdata, 0, count);
            this.Read(readdata);
        }

        /// <summary>
        /// Reads KeyValues data from the specified stream and stores the result in the internal token list.
        /// </summary>
        /// <param name="stream">The stream to read KeyValues data from.</param>
        /// <param name="buffersize">How big the internal reading buffer should be.</param>
        public virtual void Read(Stream stream, int buffersize = DefaultBufferSize)
        {
            var buffer = new byte[buffersize];
            var readnum = 0;
            while (true)
            {
                readnum = stream.Read(buffer, 0, buffersize);
                if (readnum <= 0)
                    break;
                this.Read(buffer, 0, readnum);
            }
            this.Read();
        }

        /// <summary>
        /// Reads KeyValues data from the specified file (absolute path only) and stores the result in the internal token list.
        /// </summary>
        /// <param name="path">The file to read KeyValues data from.</param>
        /// <param name="buffersize">How big the internal reading buffer should be.</param>
        public virtual void Read(string path, int buffersize = DefaultBufferSize)
        {
            using (var fs = File.OpenRead(path))
            {
                this.Rules.ResolveMacroPaths.Add(Path.GetDirectoryName(path));
                this.Read(fs, buffersize);
            }
        }

        /// <summary>
        /// Finishes a reading operation by appending a newline.
        /// This allows for all buffers to flush correctly, although it should under normal circumstances not be a problem.
        /// </summary>
        protected virtual void Read() => this.Rules.Read(this.Tokens, new[] { (byte)'\n' });

        /// <summary>
        /// Writes the internal list of tokens to the specified stream.
        /// </summary>
        /// <param name="stream">Which stream to use for writing the KeyValues data.</param>
        public virtual void Write(Stream stream) => Write(stream, this.Tokens);

        /// <summary>
        /// Writes the specified list of tokens to the specified stream.
        /// </summary>
        /// <param name="stream">Which stream to use for writing the KeyValues data.</param>
        /// <param name="tokens">List of tokens to write.</param>
        public virtual void Write(Stream stream, IEnumerable<KVToken> tokens)
        {
            foreach (var token in tokens)
            {
                var str = token.ToString();
                var bytes = System.Text.Encoding.UTF8.GetBytes(str);
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        /// <summary>
        /// Gets a list of tokens that were read by the tokenizer.
        /// </summary>
        public virtual KeyValueList Tokens { get; } = new KeyValueList();

        /// <summary>
        /// Gets or sets which ruleset to use for tokenizing KeyValues data.
        /// Determines what is seen as valid or invalid data, as well as how data should be formatted.
        /// </summary>
        public KeyValuesRuleSet Rules { get; set; } = KeyValuesRuleSet.Default;

        /// <summary>
        /// Parses the internal list of tokens.
        /// Builds the hierchary and only keeps tokens that truly represent the KeyValue structure
        /// (<see cref="KVPair"/>, <see cref="KVComment"/> and <see cref="KVMacro"/>).
        /// </summary>
        public virtual KeyValueList Parse() => Parse(this.Tokens);

        /// <summary>
        /// Parses the specified list of tokens.
        /// Builds the hierchary and only keeps tokens that truly represent the KeyValue structure
        /// (<see cref="KVPair"/>, <see cref="KVComment"/> and <see cref="KVMacro"/>).
        /// </summary>
        /// <param name="tokens">The tokens to parse.</param>
        public virtual KeyValueList Parse(IEnumerable<KVToken> tokens) => new KeyValueList(this.Rules.Parse(tokens));
    }
}
