﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SFMTK.Parsing.KeyValues
{
    /// <summary>
    /// Represents a token of KeyValues data.
    /// </summary>
    public abstract class KVToken
    {
        /// <summary>
        /// Gets or sets the string value of the <see cref="KVToken"/>.
        /// </summary>
        public virtual string Value { get; set; }
    }

    /// <summary>
    /// A general string token.
    /// </summary>
    public class KVString : KVToken
    {
        public KVString() : base()
        {

        }

        public KVString(string value) : this()
        {
            this.Value = value;
        }

        public override string ToString() => $"\"{Value}\"";
    }

    /// <summary>
    /// A comment token. Includes the string as they were found in the data.
    /// </summary>
    public class KVComment : KVToken
    {
        public KVComment() : base()
        {

        }

        public KVComment(string value) : this()
        {
            this.Value = value;
        }

        public override string ToString() => $"//{Value}";
    }

    /// <summary>
    /// A macro. Includes the string as they were found in the data.
    /// </summary>
    public class KVMacro : KVToken
    {
        public KVMacro() : base()
        {

        }

        public KVMacro(string value) : this()
        {
            this.Value = value;
        }

        public override string ToString() => $"#{Value}";
    }

    /// <summary>
    /// An opening brace, signifying children.
    /// </summary>
    public class KVOpenBrace : KVToken
    {
        public KVOpenBrace() : base()
        {

        }

        public override string ToString() => "{";
    }

    /// <summary>
    /// A closing brace, signifying a list of children has finished.
    /// </summary>
    public class KVCloseBrace : KVToken
    {
        public KVCloseBrace() : base()
        {

        }

        public override string ToString() => "}";
    }

    /// <summary>
    /// A standard Key Value pair with optional hierchary.
    /// </summary>
    public class KVPair : KVToken, IEnumerable<KVToken>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KVPair"/> class with the specified key and a value of <c>null</c>.
        /// </summary>
        /// <param name="key">The string key of the <see cref="KVPair"/>. Every <see cref="KVPair"/> requires a key.</param>
        public KVPair(string key) : base()
        {
            this.Key = key;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KVPair"/> class with the specified key and value.
        /// </summary>
        /// <param name="key">The string key of the <see cref="KVPair"/>. Every <see cref="KVPair"/> requires a key.</param>
        /// <param name="value">The string value of the <see cref="KVPair"/>.</param>
        public KVPair(string key, string value) : this(key)
        {
            this.Value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KVPair"/> class with the specified key and children.
        /// </summary>
        /// <param name="key">The string key of the <see cref="KVPair"/>. Every <see cref="KVPair"/> requires a key.</param>
        /// <param name="children">The children that the key owns.</param>
        public KVPair(string key, params KVToken[] children) : this(key)
        {
            this.Children.AddRange(children);
        }

        /// <summary>
        /// Gets or sets the string value of the <see cref="KVPair"/>. May be <c>null</c> if <see cref="Children"/> are present.
        /// </summary>
        public override string Value
        {
            get
            {
                if (this.Children.Count > 0)
                    return null;
                return base.Value;
            }
            set => base.Value = value;
        }

        /// <summary>
        /// Gets a list of children that the key owns. This is never <c>null</c>.
        /// </summary>
        public KeyValueList Children { get; } = new KeyValueList();

        /// <summary>
        /// Gets or sets the string key of the <see cref="KVPair"/>. Every <see cref="KVPair"/> requires a key.
        /// </summary>
        public string Key { get; set; }

        public IEnumerator<KVToken> GetEnumerator() => this.Children.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => this.Children.GetEnumerator();

        /// <summary>
        /// Gets or sets the child <see cref="KVToken" /> at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        public KVToken this[int index]
        {
            get => this.Children[index];
            set => this.Children[index] = value;
        }

        /// <summary>
        /// Gets the child <see cref="KVPair"/> with the specified key, or <c>null</c> if it does not exist.
        /// </summary>
        /// <param name="key">The key to search for.</param>
        /// <param name="ignorecase">If set to <c>true</c>, casing is ignored. Source Engine defaults to <c>true</c>.</param>
        public KVPair this[string key, bool ignorecase = true]
        {
            get => this.Children.OfType<KVPair>().FirstOrDefault((e) => e.Key.Equals(key, ignorecase ?
                System.StringComparison.InvariantCultureIgnoreCase :
                System.StringComparison.InvariantCulture));
        }

        /// <summary>
        /// Returns the <see cref="KVPair"/> that has the following list of keys as the respective parents.
        /// </summary>
        /// <param name="keys">Path from first child's key of this <see cref="KVPair"/> to the <see cref="KVPair"/>'s key you wish to get.</param>
        /// <returns></returns>
        public KVPair GetByPath(params string[] keys)
        {
            if (!keys.Any())
                return this;
            return this[keys[0]].GetByPath(keys.Skip(1).ToArray());
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (Value == null)
                return $"\"{Key}\" {{ {string.Join("\n", Children)} }}";
            return $"\"{Key}\" \"{Value}\"";
        }
    }
}
