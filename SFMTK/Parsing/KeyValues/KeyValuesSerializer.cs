﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SFMTK.Parsing.KeyValues
{
    /// <summary>
    /// Allows for serializing and deserializing KeyValues data from or to .NET objects by use of the <see cref="KVSerializedAttribute"/>.
    /// </summary>
    public class KeyValuesSerializer
    {
        //TODO: KeyValuesSerializer - attaching the attribute to a class/struct will (de)serialize ALL public fields/properties using their default names

        /// <summary>
        /// Deserializes the specified tokens to a .NET object of the specified type.
        /// </summary>
        /// <typeparam name="T">The type of object to instanciate and populate. Requires a public, parameterless constructor.
        /// Properties and fields to deserialize should be marked with a <see cref="KVSerializedAttribute"/>.</typeparam>
        /// <param name="tokens">The tokens to use as KeyValues data.</param>
        public virtual T Deserialize<T>(IEnumerable<KVPair> tokens)
        {
            var sis = GetSerializeInfo<T>().Where(i => i.Attribute.Context.HasFlag(KVSerializeContext.Deserialize));
            if (!sis.Any())
                return default(T);

            var inst = System.Activator.CreateInstance<T>();
            foreach (var token in tokens)
            {
                if (this.NullHandling == NullHandlingMode.Skip && token.Value == null)
                    continue;

                //Check which info correlates to this token
                bool EffectivePath(KVSerializeInfo arg)
                {
                    //Get effective path
                    var path = arg.Attribute.Path;
                    if (path == null || path.Length == 0)
                        path = new[] { arg.Info.Name };

                    //Fit with token
                    var equal = token.Key.Equals(path[0], this.IgnoreCaseKey ?
                        System.StringComparison.InvariantCultureIgnoreCase :
                        System.StringComparison.InvariantCulture);
                    if (path.Length == 1)
                        return equal;
                    return equal && token.GetByPath(path.Skip(1).ToArray()) != null;
                }

                var si = sis.FirstOrDefault(EffectivePath);
                if (si == null)
                    continue;

                //Set value!
                SetValue(si.Info, inst, token.Value);
            }
            return inst;
        }

        /// <summary>
        /// Serializes the specified object to KeyValues data.
        /// </summary>
        /// <typeparam name="T">The type of object used to populate the data.
        /// Properties and fields to serialize should be marked with a <see cref="KVSerializedAttribute"/>.</typeparam>
        /// <param name="obj">The object to get the data values from.</param>
        public virtual IEnumerable<KVPair> Serialize<T>(T obj)
        {
            var sis = GetSerializeInfo<T>().Where(i => i.Attribute.Context.HasFlag(KVSerializeContext.Serialize));
            if (!sis.Any())
                return Enumerable.Empty<KVPair>();

            var toplevel = new KVPair(this.RootKey);
            foreach (var si in sis)
            {
                var value = GetValue(si.Info, obj);
                if (this.NullHandling == NullHandlingMode.Skip && value == null)
                    continue;

                //Effective path
                var path = si.Attribute.Path;
                if (path == null || path.Length == 0)
                    path = new[] { si.Info.Name };

                //Start at toplevel
                var parent = toplevel;

                //Take away path segments until we're at the end
                while (path.Length > 1)
                {
                    //Get child name to search for
                    var seg = path[0] ?? si.Info.Name;
                    
                    //Find next parent
                    var next = parent[seg, this.IgnoreCaseKey];
                    if (next == null)
                    {
                        //Create that node
                        next = new KVPair(seg);
                        parent.Children.Add(next);
                    }

                    //Cycle
                    parent = next;
                    path = path.Skip(1).ToArray();
                }

                //Create child
                var pair = new KVPair(path.Last() ?? si.Info.Name, value?.ToString() ?? "");
                parent.Children.Add(pair);
            }

            if (this.RootKey == null)
                return toplevel.Children.Cast<KVPair>();
            return new[] { toplevel };
        }

        /// <summary>
        /// Gets or sets how to handle <c>null</c> values for serializing and deserializing.
        /// </summary>
        public virtual NullHandlingMode NullHandling { get; set; }

        /// <summary>
        /// Gets or sets the key of the root element to create when serializing data.
        /// If <c>null</c> is used (default), all <see cref="KVToken"/>s are returned as a flat enumerable.
        /// Otherwise, the enumerable only contains a single <see cref="KVToken"/> with the specified key and the remaining elements as its children.
        /// </summary>
        public virtual string RootKey { get; set; }

        /// <summary>
        /// Gets or sets whether case matters for comparing key or path strings.
        /// Source Engine defaults to <c>true</c>.
        /// </summary>
        public virtual bool IgnoreCaseKey { get; set; } = true;

        protected static object GetValue(MemberInfo info, object instance)
        {
            if (info is PropertyInfo pi)
                return pi.GetValue(instance);
            else if (info is FieldInfo fi)
                return fi.GetValue(instance);
            return null;
        }

        protected static void SetValue(MemberInfo info, object instance, object value)
        {
            if (info is PropertyInfo pi && pi.CanWrite)
                pi.SetValue(instance, value);
            else if (info is FieldInfo fi && !fi.IsInitOnly)
                fi.SetValue(instance, value);
        }

        protected static IEnumerable<KVSerializedAttribute> GetAttributes(MemberInfo info) => info.GetCustomAttributes<KVSerializedAttribute>();

        protected static IEnumerable<KVSerializeInfo> GetSerializeInfo<T>()
        {
            var type = typeof(T);
            foreach (var member in type.GetFields().Cast<MemberInfo>().Concat(type.GetProperties()))
            {
                var attrs = GetAttributes(member);

                //In case we have multiple attributes
                foreach (var attr in attrs)
                {
                    yield return new KVSerializeInfo(member, attr);
                }
            }
        }

        protected class KVSerializeInfo
        {
            public KVSerializeInfo(MemberInfo info, KVSerializedAttribute attr)
            {
                this.Info = info;
                this.Attribute = attr;
            }

            public MemberInfo Info { get; set; }

            public KVSerializedAttribute Attribute { get; set; }
        }
    }

    /// <summary>
    /// How to handle <c>null</c> values for serializing and deserializing.
    /// </summary>
    public enum NullHandlingMode
    {
        /// <summary>
        /// Fields and properties with a <c>null</c> value are skipped.
        /// On serialize, the data will not be written.
        /// On deserialize, the field or property will not change value.
        /// </summary>
        Skip,
        /// <summary>
        /// Fields and properties with a <c>null</c> value will copy their value as best represented in the corresponding format.
        /// On serialize, an empty string is used instead.
        /// On deserialize, <c>null</c> is used. Note that KeyValues data can usually not have <c>null</c> values.
        /// </summary>
        NullValue
    }
}
