﻿using System.Collections.Generic;

namespace SFMTK.Parsing.KeyValues
{
    /// <summary>
    /// A <see cref="KeyValuesRuleSet"/> for QC files. Syntax allows for multiple values for a single key (starting with a dollar symbol).
    /// </summary>
    public class QCKeyValuesRuleSet : KeyValuesRuleSet
    {
        //UNTESTED: QCKVRS - do QC / VMT actually ignore newlines and only rely on the $key ?

        public override void Initialize() => throw new System.NotImplementedException();
        public override IEnumerable<KVToken> Parse(IEnumerable<KVToken> tokens) => throw new System.NotImplementedException();
        public override void Read(KeyValueList list, byte[] bytes) => throw new System.NotImplementedException();
    }
}
