﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SFMTK.Parsing.KeyValues
{
    /// <summary>
    /// The default <see cref="KeyValuesRuleSet"/>. Supports Key Value pairs, nested pairs, comments and macros.
    /// </summary>
    public class DefaultKeyValuesRuleSet : KeyValuesRuleSet
    {
        //NYI: DefaultKeyValuesRuleSet CompatibilityMode - possible compatibility cases (investigate further):
        //  - comments start with single slash
        //  - #include does not work, while #base does
        //  - Are escape sequences actually disabled in most stock-implemented KV parsers?

        /// <summary>
        /// Gets or sets whether compatibility mode is enabled.
        /// If enabled, the <see cref="DefaultKeyValuesRuleSet"/> will try to parse as close to Source's parser as is possible.
        /// <para/>This includes arbitrary limitations and buggy behavior that may not be wanted in real-world applications.
        /// <para/>For full compatibility, set <see cref="UseEscapeSequences"/> to <c>false</c> and <see cref="KeyValuesSerializer.IgnoreCaseKey"/> to <c>true</c>.
        /// </summary>
        public bool CompatibilityMode { get; set; } = true;

        /// <summary>
        /// Gets or sets whether the tokenizer understands escape sequences.
        /// Source Engine has this set to <c>false</c> by default.
        /// <para /> Known sequences: \t \n \\ \"
        /// </summary>
        public override bool UseEscapeSequences
        {
            get => this.CompatibilityMode ? false : base.UseEscapeSequences;
            set => base.UseEscapeSequences = value;
        }

        /// <summary>
        /// Initializes or resets the <see cref="KeyValuesRuleSet" />.
        /// </summary>
        public override void Initialize()
        {
            this.tmode = KeyValuesTokenizerMode.Default;
            this.pmode = KeyValuesParserMode.Default;
            this.ClearBuffer();
            this.escape = false;
            this.doublecharcheck = 0;
            this.current = null;
            this.readstr = null;
        }

        /// <summary>
        /// Reads the specified KeyValues data and passes important information back to the calling tokenizer.
        /// </summary>
        /// <param name="list">The list to feed data back to.</param>
        /// <param name="bytes">The data to read.</param>
        public override void Read(KeyValueList list, byte[] bytes)
        {
            this.readstr = System.Text.Encoding.UTF8.GetString(bytes);
            for (var i = 0; i < bytes.Length; i++)
            {
                byte by;

                if (doublecharcheck == 0)
                    by = bytes[i];
                else
                {
                    i--;
                    by = doublecharcheck;
                    doublecharcheck = 0;
                }

                switch (by)
                {
                    case BackslashChar:
                        //Are we using escape characters?
                        if (this.UseEscapeSequences && this.tmode != KeyValuesTokenizerMode.InComment)
                        {
                            if (this.escape)
                            {
                                //Escape backslash
                                this.AppendBuffer('\\');
                                this.escape = false;
                                break;
                            }

                            //Escape next char
                            this.escape = true;
                            continue;
                        }

                        //No escape chars? simply append
                        this.AppendBuffer('\\');
                        break;
                    case SlashChar:
                        //Peek for comment char
                        if (i == bytes.Length - 1)
                        {
                            //May not peek, let us wait instead
                            this.doublecharcheck = SlashChar;
                            break;
                        }
                        else if (bytes[i + 1] == SlashChar)
                        {
                            //We got a comment
                            i++;
                            FlushBuffer();
                            this.tmode = KeyValuesTokenizerMode.InComment;
                        }
                        else
                        {
                            //Just a slash
                            this.AppendBuffer('/');
                        }
                        break;
                    case HashChar:
                        //Determine based on mode
                        switch (this.tmode)
                        {
                            case KeyValuesTokenizerMode.Default:
                                //Parse as a macro
                                this.tmode = KeyValuesTokenizerMode.InMacro;
                                break;
                            default:
                                //Parse as a char
                                this.AppendBuffer('#');
                                break;
                        }
                        break;
                    case QuoteChar:
                        //Are we escaping?
                        if (this.escape)
                        {
                            //Escape quote
                            this.AppendBuffer('"');
                            this.escape = false;
                            break;
                        }

                        //Determine based on mode
                        switch (this.tmode)
                        {
                            case KeyValuesTokenizerMode.InString:
                                //End string
                                FlushBuffer();
                                this.tmode = KeyValuesTokenizerMode.Default;
                                break;
                            case KeyValuesTokenizerMode.InComment:
                                //Simple character
                                this.AppendBuffer('"');
                                break;
                            case KeyValuesTokenizerMode.Default:
                                //Start string
                                FlushBuffer();
                                this.tmode = KeyValuesTokenizerMode.InString;
                                break;
                        }
                        break;
                    case OpenBraceChar:
                        switch (this.tmode)
                        {
                            case KeyValuesTokenizerMode.Default:
                                //Is a token
                                FlushBuffer();
                                list.Add(new KVOpenBrace());
                                break;
                            default:
                                //Simple character
                                this.AppendBuffer('{');
                                break;
                        }
                        break;
                    case CloseBraceChar:
                        switch (this.tmode)
                        {
                            case KeyValuesTokenizerMode.Default:
                                //Is a token
                                FlushBuffer();
                                list.Add(new KVCloseBrace());
                                break;
                            default:
                                //Simple character
                                this.AppendBuffer('}');
                                break;
                        }
                        break;
                    default:
                        //Although similar, these two cases are not identical
                        //Newline should always flush the buffer, since you can't have strings or comments across lines
                        //But whitespace should only flush the buffer if we aren't in anything that accepts spaces as characters
                        if (IsNewline(by))
                        {
                            //Flush buffer (create string token if no quotes)
                            if (this.tmode == KeyValuesTokenizerMode.Default)
                                this.tmode = KeyValuesTokenizerMode.InString;
                            FlushBuffer();
                            this.tmode = KeyValuesTokenizerMode.Default;
                            break;
                        }
                        else if (IsWhitespace(by) && (this.tmode == KeyValuesTokenizerMode.Default || this.tmode == KeyValuesTokenizerMode.InMacro))
                        {
                            //Flush buffer (create string token)
                            this.tmode = KeyValuesTokenizerMode.InString;
                            FlushBuffer();
                            this.tmode = KeyValuesTokenizerMode.Default;
                            break;
                        }

                        //Add to buffer
                        this.AppendBuffer(this.readstr[i]);
                        break;
                }

                if (this.escape)
                {
                    //Invalid escape sequence
                    throw new KeyValuesTokenizingException($"Invalid escape sequence: \\{this.readstr[i]}", this.GetBuffer());
                }
            }


            
            void FlushBuffer()
            {
                switch (this.tmode)
                {
                    case KeyValuesTokenizerMode.Default:
                        break;
                    case KeyValuesTokenizerMode.InMacro:
                        //Create comment token
                        var macro = new KVMacro(this.GetBuffer());
                        list.Add(macro);
                        break;
                    case KeyValuesTokenizerMode.InComment:
                        //Create comment token
                        var comment = new KVComment(this.GetBuffer());
                        list.Add(comment);
                        break;
                    case KeyValuesTokenizerMode.InString:
                        if (this.bufferindex > 0)
                        {
                            //Create string token
                            var str = new KVString(this.GetBuffer());
                            list.Add(str);
                        }
                        break;
                }
                this.ClearBuffer();
                this.escape = false;
                this.doublecharcheck = 0;
            }
        }

        /// <summary>
        /// Parses the entire list of tokens to build hierchary and get rid of unneeded tokens.
        /// </summary>
        /// <param name="tokens">The token list to parse.</param>
        public override IEnumerable<KVToken> Parse(IEnumerable<KVToken> tokens)
        {
            var BLE = tokens.Reverse().ToList();

            foreach (var token in tokens)
            {
                if (token is KVComment)
                {
                    //Skip if not wanted
                    if (!this.IncludeComments.HasFlag(IncludeCommentsMode.Include))
                        continue;

                    //Skip if empty and not wanted
                    if (!this.IncludeComments.HasFlag(IncludeCommentsMode.EmptyComments) && string.IsNullOrWhiteSpace(token.Value))
                        continue;

                    if (this.IncludeComments.HasFlag(IncludeCommentsMode.Nested))
                    {
                        //Nested return
                        if (this.pstack.Count == 0)
                        {
                            //Yield return toplevel
                            yield return token;
                        }
                        else
                        {
                            //Add to children list
                            this.pstack.Peek().Children.Add(token);
                        }
                    }
                    else
                    {
                        //Flat return
                        yield return token;
                    }
                }
                else if (token is KVMacro)
                {
                    //We got a macro, which type?
                    if (token.Value.StartsWith("base", System.StringComparison.InvariantCultureIgnoreCase)
                        || token.Value.StartsWith("include", System.StringComparison.InvariantCultureIgnoreCase))
                    {
                        //Resolve an included file
                        if (this.ResolveMacroPaths != null && this.ResolveMacroPaths.Count > 0)
                        {
                            //Try resolving our macro
                            //UNTESTED: KVParsing - does macro parsing work just like KV parsing with how quotes/escaping etc are handled?
                            var value = token.Value.Substring(token.Value.IndexOf(' ') + 1).Replace("\"", "");

                            //UNTESTED: KVParsing - macro reading, does it correctly work?
                            var resolved = false;
                            foreach (var path in this.ResolveMacroPaths)
                            {
                                //Normalize and absolutify
                                var norm = SFM.GetFullPath(path) ?? IOHelper.GetStandardizedPath(path);

                                IEnumerable<KVToken> ResolveMacro()
                                {
                                    //Toss in all tokens as we got em
                                    var inner = new KeyValuesTokenizer();
                                    inner.Rules = (KeyValuesRuleSet)this.Clone();
                                    inner.Rules.Initialize();
                                    inner.Read(norm);

                                    //Mark as resolved
                                    resolved = true;

                                    //Return the tree
                                    return inner.Parse();
                                }

                                //Is it a file or a folder?
                                var what = IOHelper.IsFileOrFolder(norm);
                                if (what == FileAttributes.Directory)
                                {
                                    //Folder, search for the file inside
                                    var file = Directory.GetFiles(norm, value).SingleOrSafeDefault();
                                    if (file != null)
                                    {
                                        //Resolve the macro
                                        foreach (var inner in ResolveMacro())
                                        {
                                            if (this.pstack.Count == 0)
                                            {
                                                yield return inner;
                                            }
                                            else
                                            {
                                                this.pstack.Peek().Children.Add(inner);
                                            }
                                        }
                                        break;
                                    }
                                }
                                else if (what == FileAttributes.Normal)
                                {
                                    //File, check if names match
                                    if (Path.GetFileName(norm) == value)
                                    {
                                        //Resolve the macro
                                        foreach (var inner in ResolveMacro())
                                        {
                                            if (this.pstack.Count == 0)
                                            {
                                                yield return inner;
                                            }
                                            else
                                            {
                                                this.pstack.Peek().Children.Add(inner);
                                            }
                                        }
                                        break;
                                    }
                                }
                            }

                            if (resolved)
                                continue;
                        }
                    }

                    //Included without nesting
                    yield return token;
                }
                else if (token is KVOpenBrace)
                {
                    if (this.current == null || this.pmode == KeyValuesParserMode.Default)
                        throw new KeyValuesParsingException("Can't indent without a key.", null);

                    //Increase indent
                    this.pmode = KeyValuesParserMode.Default;
                    this.pstack.Push(this.current);
                    this.current = null;
                }
                else if (token is KVCloseBrace)
                {
                    if (this.pstack.Count == 0)
                        throw new KeyValuesParsingException("Indent may not be lower than 0. Too many closing braces.", this.current);

                    if (this.pmode == KeyValuesParserMode.KeyNeedsValue)
                        throw new KeyValuesParsingException($"Unfinished key needs value: {this.current.Key}", this.current);

                    //Decrease indent
                    var popped = this.pstack.Pop();
                    if (this.pstack.Count == 0)
                    {
                        yield return popped;
                    }
                    else
                    {
                        this.pstack.Peek().Children.Add(popped);
                    }
                }
                else if (token is KVString)
                {
                    //String token, what is its role?
                    switch (this.pmode)
                    {
                        case KeyValuesParserMode.Default:
                            //Start a new Key Value pair
                            this.current = new KVPair(token.Value);
                            this.pmode = KeyValuesParserMode.KeyNeedsValue;
                            break;
                        case KeyValuesParserMode.KeyNeedsValue:
                            //Got a value, finish pair
                            this.current.Value = token.Value;
                            this.pmode = KeyValuesParserMode.Default;

                            if (this.pstack.Count == 0)
                            {
                                //Yield return toplevel
                                yield return this.current;
                            }
                            else
                            {
                                //Add to children list
                                this.pstack.Peek().Children.Add(this.current);
                            }

                            this.current = null;
                            break;
                    }
                }
            } //foreach

            //Missing }
            if (this.pstack.Count > 0)
                throw new KeyValuesParsingException($"KeyValues data ends with {StringHelper.GetPlural(this.pstack.Count, "unclosed brace")}.", null);

            //Missing value for a key
            if (this.current != null)
                throw new KeyValuesParsingException($"Key \"{this.current.Key}\" does not have a value.", this.current);
        }

        /// <summary>
        /// Clears the <see cref="buffer"/>.
        /// </summary>
        protected void ClearBuffer()
        {
            System.Array.Clear(this.buffer, 0, this.bufferindex);
            this.bufferindex = 0;
        }

        /// <summary>
        /// Appends the specified character to the <see cref="buffer"/>.
        /// </summary>
        protected void AppendBuffer(char ch)
        {
            var comparison = this.CompatibilityMode ? SourceMaxBufferSize : TotalMaxBufferSize;
            if (this.bufferindex >= comparison)
                throw new KeyValuesTokenizingException($"Tokens may not be longer than {comparison} characters", this.GetBuffer());

            this.buffer[this.bufferindex] = ch;
            this.bufferindex++;
        }

        /// <summary>
        /// Gets the string representation of the <see cref="buffer"/>.
        /// </summary>
        protected string GetBuffer() => new string(this.buffer, 0, this.bufferindex);

        /// <summary>
        /// As specified in the Valve Developer Community, tokens may only be 1024 characters long.
        /// </summary>
        protected const int SourceMaxBufferSize = 1024;

        /// <summary>
        /// Actual maximum size of the char buffer, in case <see cref="CompatibilityMode"/> is disabled.
        /// </summary>
        protected const int TotalMaxBufferSize = ushort.MaxValue;

        /// <summary>
        /// String representation of the current read byte array of KeyValue data.
        /// </summary>
        protected string readstr;

        /// <summary>
        /// Current string buffer, used for building token string values.
        /// </summary>
        protected char[] buffer = new char[TotalMaxBufferSize];

        /// <summary>
        /// Index to write to the <see cref="buffer"/>.
        /// </summary>
        protected int bufferindex = 0;

        /// <summary>
        /// Whether the next character should be escaped.
        /// </summary>
        protected bool escape;

        /// <summary>
        /// If set to non-zero, the byte to check for being duplicate if the last buffer ended with a single character.
        /// </summary>
        /// <example>
        /// This is used for double-slash comments for example:
        /// If the read data array ends with a single slash, we can't peek to the next character and see if it's also a slash,
        /// so instead we have to fake the next read option starting with a slash, to see if the duplicate check then works out.
        /// </example>
        protected byte doublecharcheck = 0;

        /// <summary>
        /// Current mode of the tokenizer.
        /// </summary>
        protected KeyValuesTokenizerMode tmode;

        /// <summary>
        /// Current mode of the parser.
        /// </summary>
        protected KeyValuesParserMode pmode;

        /// <summary>
        /// Stack used for building the child hierchary when parsing KeyValue data.
        /// </summary>
        protected Stack<KVPair> pstack = new Stack<KVPair>();

        /// <summary>
        /// Currently constructed <see cref="KVPair"/>.
        /// </summary>
        protected KVPair current;
    }
}
