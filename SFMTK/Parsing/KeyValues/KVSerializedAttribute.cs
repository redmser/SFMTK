﻿using System;

namespace SFMTK.Parsing.KeyValues
{
    /// <summary>
    /// An attribute for properties or fields that should be populated on deserialize or used for serialize of KeyValues data. This class can not be inherited.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = true)]
    public sealed class KVSerializedAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KVSerializedAttribute"/> class used for both serialize and deserialize.
        /// </summary>
        /// <param name="keys">The path of keys that matches with this property or field.
        /// <para/>If left as an empty array, or a <c>null</c> value, the path is implied to be the name of the element.</param>
        public KVSerializedAttribute(params string[] keys) : this(KVSerializeContext.Both, keys)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KVSerializedAttribute" /> class used for both serialize and deserialize.
        /// </summary>
        /// <param name="context">Under which context the property or field should be linked to KeyValues data.</param>
        /// <param name="keys">The path of keys that matches with this property or field.
        /// <para/>If left as an empty array, or a <c>null</c> value, the path is implied to be the name of the element.</param>
        public KVSerializedAttribute(KVSerializeContext context, params string[] keys)
        {
            this.Context = context;
            this.Path = keys;
        }

        /// <summary>
        /// Gets the path of keys that matches with this property or field.
        /// <para/>If left as an empty array, or a <c>null</c> value, the path is implied to be the name of the element.
        /// </summary>
        public string[] Path { get; }

        /// <summary>
        /// Gets under which context the property or field should be linked to KeyValues data.
        /// </summary>
        public KVSerializeContext Context { get; }
    }

    /// <summary>
    /// Under which context the <see cref="KVSerializedAttribute"/> should function.
    /// </summary>
    [Flags]
    public enum KVSerializeContext
    {
        None = 0,
        Serialize = 1,
        Deserialize = 2,
        Both = Serialize | Deserialize
    }
}
