﻿using System;
using System.Collections.Generic;

namespace SFMTK.Parsing.KeyValues
{
    /// <summary>
    /// Defines how KeyValues data is structured and when it is valid.
    /// Although the format is supposed to be uniform, Valve have multiple similar formats with subtle differences that change how the parser works.
    /// </summary>
    public abstract class KeyValuesRuleSet : ICloneable
    {
        /// <summary>
        /// Gets the default <see cref="KeyValuesRuleSet"/>.
        /// </summary>
        public static KeyValuesRuleSet Default => new DefaultKeyValuesRuleSet();

        /// <summary>
        /// Gets a <see cref="KeyValuesRuleSet"/> for QC data.
        /// </summary>
        public static KeyValuesRuleSet QC => new QCKeyValuesRuleSet();

        protected const byte QuoteChar = (byte)'"';
        protected const byte SlashChar = (byte)'/';
        protected const byte BackslashChar = (byte)'\\';
        protected const byte HashChar = (byte)'#';
        protected const byte DollarChar = (byte)'$';
        protected const byte OpenBraceChar = (byte)'{';
        protected const byte CloseBraceChar = (byte)'}';
        protected const byte SpaceChar = (byte)' ';
        protected const byte ReturnChar = (byte)'\r';
        protected const byte NewlineChar = (byte)'\n';
        protected const byte TabChar = (byte)'\t';

        /// <summary>
        /// Returns whether the specified byte is whitespace. Newlines are not included here.
        /// </summary>
        protected static bool IsWhitespace(byte by) => by == SpaceChar || by == TabChar;

        /// <summary>
        /// Returns whether the specified byte is a newline.
        /// </summary>
        protected static bool IsNewline(byte by) => by == NewlineChar || by == ReturnChar;

        /// <summary>
        /// Gets or sets whether the tokenizer understands escape sequences.
        /// Source Engine has this set to <c>false</c> by default.
        /// <para/> Known sequences: \t \n \\ \"
        /// </summary>
        public virtual bool UseEscapeSequences { get; set; } = true;

        /// <summary>
        /// Gets or sets whether to include comments in the final KeyValues list.
        /// They will not be interpretted in any way.
        /// </summary>
        public virtual IncludeCommentsMode IncludeComments { get; set; } = IncludeCommentsMode.DoNotInclude;

        /// <summary>
        /// Gets a list of paths (directories or files, absolute or relative to SFM) used to resolve <c>#include</c> or <c>#base</c> macros.
        /// Any unknown macros (or if no paths are specified) will be left in as a <see cref="KVMacro"/> token.
        /// </summary>
        public virtual List<string> ResolveMacroPaths { get; } = new List<string>();

        /// <summary>
        /// Reads the specified KeyValues data and passes important information back to the calling tokenizer.
        /// </summary>
        /// <param name="list">The list to feed data back to.</param>
        /// <param name="bytes">The data to read.</param>
        public abstract void Read(KeyValueList list, byte[] bytes);

        /// <summary>
        /// Parses the entire list of tokens to build hierchary and get rid of unneeded tokens.
        /// </summary>
        /// <param name="tokens">The token list to parse.</param>
        public abstract IEnumerable<KVToken> Parse(IEnumerable<KVToken> tokens);

        /// <summary>
        /// Initializes or resets the <see cref="KeyValuesRuleSet"/>.
        /// </summary>
        public abstract void Initialize();

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            var obj = (KeyValuesRuleSet)this.MemberwiseClone();
            return this.DoClone(obj);
        }

        /// <summary>
        /// Allows for setting properties that <see cref="object.MemberwiseClone"/> would not get (called on <see cref="Clone"/>).
        /// </summary>
        /// <param name="ruleset">Base instance for cloned object.</param>
        protected virtual KeyValuesRuleSet DoClone(KeyValuesRuleSet ruleset)
        {
            ruleset.ResolveMacroPaths.AddRange(this.ResolveMacroPaths);
            return ruleset;
        }

        /// <summary>
        /// Current mode for tokenizing.
        /// </summary>
        protected enum KeyValuesTokenizerMode
        {
            Default,
            InComment,
            InString,
            InMacro
        }

        /// <summary>
        /// Current mode for parsing.
        /// </summary>
        protected enum KeyValuesParserMode
        {
            Default,
            KeyNeedsValue
        }
    }

    /// <summary>
    /// Flags on how and which comments should be included.
    /// </summary>
    [Flags]
    public enum IncludeCommentsMode
    {
        /// <summary>
        /// Comments are not included.
        /// </summary>
        DoNotInclude = 0,
        /// <summary>
        /// Comments are added to the parsing result the moment they get found.
        /// </summary>
        Include = 1,
        /// <summary>
        /// Comments are included directly where they were found in the hierchary.
        /// Note that in-line comments can not be distinguished.
        /// </summary>
        Nested = 3,
        /// <summary>
        /// Only empty comments are included.
        /// </summary>
        EmptyComments = 4
    }
}
