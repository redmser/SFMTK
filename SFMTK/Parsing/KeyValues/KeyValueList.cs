﻿using System.Collections.Generic;
using System.Linq;

namespace SFMTK.Parsing.KeyValues
{
    /// <summary>
    /// A list of keyvalues.
    /// </summary>
    public class KeyValueList : List<KVToken>
    {
        public KeyValueList() : base()
        {

        }

        public KeyValueList(IEnumerable<KVToken> collection) : base(collection)
        {

        }

        /// <summary>
        /// Gets the <see cref="KVPair" /> with the specified key.
        /// </summary>
        /// <param name="key">The key to search for.</param>
        /// <param name="ignorecase">If set to <c>true</c>, casing is ignored. Source Engine defaults to <c>true</c>.</param>
        public KVPair this[string key, bool ignorecase = true] => this.GetPairs()
            .First(k => k.Key.Equals(key, ignorecase ?
                System.StringComparison.InvariantCultureIgnoreCase :
                System.StringComparison.InvariantCulture));

        /// <summary>
        /// Gets only <see cref="KVPair"/> instances.
        /// </summary>
        public IEnumerable<KVPair> GetPairs() => this.OfType<KVPair>();
    }
}
