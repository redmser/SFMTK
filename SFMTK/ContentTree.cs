﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SFMTK
{
    /// <summary>
    /// All content files in form of a node tree, which can be saved and loaded to a file.
    /// </summary>
    public class ContentTree
    {
        //IMP: ContentTree - this system can be seen as sorta-obsolete, since NTFS allows for a very fast MFT lookup to get the list of files instead
        //  -> what about non-NTFS users? probably no good to implement a massive system like this, to be honest

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentTree"/> class.
        /// </summary>
        public ContentTree()
        {
            this.BasePath = SFM.BasePath;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentTree" /> class.
        /// </summary>
        /// <param name="path">The path to the node tree file.</param>
        /// <param name="load">If set to <c>true</c>, loads the specified node tree data file directly.</param>
        public ContentTree(string path, bool load) : this()
        {
            this.DataPath = path;

            if (load)
                this.Load();
        }

        /// <summary>
        /// Gets the active <see cref="ContentTreeSerializer"/>.
        /// </summary>
        private readonly ContentTreeSerializer _serializer = new ContentTreeSerializer();

        /// <summary>
        /// Gets the nodes loaded into the node tree.
        /// </summary>
        public List<ContentBrowserNode> Nodes { get; } = new List<ContentBrowserNode>();

        /// <summary>
        /// Gets or sets the path to the node tree file to load or save to by default.
        /// </summary>
        public string DataPath { get; set; }

        /// <summary>
        /// Gets SFM's base path as it was specified in the node tree data file. Will be updated after a Load call.
        /// </summary>
        public string BasePath { get; private set; }

        /// <summary>
        /// Loads the node tree.
        /// </summary>
        public void Load() => this.Load(this.DataPath);

        /// <summary>
        /// Loads the specified node tree file.
        /// </summary>
        /// <param name="path">Path to the node tree.</param>
        public void Load(string path)
        {
            //Update path, if valid
            this.CheckPath(path, false);
            this.DataPath = path;

            //Read data from file, update variables from file
            this.Nodes.Clear();
            this.Nodes.AddRange(this._serializer.Deserialize(path));
            this.BasePath = this._serializer.BasePath;
        }

        /// <summary>
        /// Saves the node tree.
        /// </summary>
        public void Save() => this.Save(this.DataPath);

        /// <summary>
        /// Saves the node tree to the specified file.
        /// </summary>
        /// <param name="path">Path to the node tree.</param>
        public void Save(string path)
        {
            //Update path, if valid
            this.CheckPath(path, true);
            this.DataPath = path;

            //Write data to file, pass current variables
            this._serializer.BasePath = SFM.BasePath;
            this._serializer.Serialize(this.Nodes, path);
        }

        /// <summary>
        /// Checks if the specified path is a valid node tree file.
        /// </summary>
        /// <param name="path"></param>
        private void CheckPath(string path, bool save)
        {
            //Check if any path is specified
            if (string.IsNullOrWhiteSpace(path))
                throw new ArgumentNullException(nameof(path));

            if (!save && !File.Exists(path))
            {
                //When loading, the file has to exist
                throw new FileNotFoundException("Could not find the specified node tree file.", path);
            }
            else if (save)
            {
                //When saving, create the directory that the file will be stored in
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            }
        }

        /// <summary>
        /// Serializes and deserializes <see cref="ContentTree"/> data in little-endian format.
        /// </summary>
        private class ContentTreeSerializer : IDisposable
        {
            //FIXME: ContentTreeSerializer - should this class rely on the ContentBrowserNode lists? why not use ContentTree directly

            /// <summary>
            /// Initializes a new instance of the <see cref="ContentTreeSerializer"/> class.
            /// </summary>
            public ContentTreeSerializer()
            {

            }

            private BinaryReader _reader;
            private BinaryWriter _writer;

            private bool _disposed;

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected void Dispose(bool disposing)
            {
                if (!_disposed)
                {
                    if (disposing)
                    {
                        //Managed resources
                        _reader.Dispose();
                        _writer.Dispose();
                    }
                    _disposed = true;
                }
            }

            ~ContentTreeSerializer()
            {
                Dispose(false);
            }

            /// <summary>
            /// Gets or sets SFM's base path, as it will be serialized to the node tree data file. Will be updated on Deserialize.
            /// </summary>
            public string BasePath { get; set; }

            /// <summary>
            /// The static header for the file format: ToolKit ContentTree.
            /// </summary>
            public const string ContentTreeHeader = "TKCT";

            /// <summary>
            /// The static footer for the file format. Show's over.
            /// </summary>
            public const string ContentTreeFooter = "OVER";

            /// <summary>
            /// The version of the ContentTree serialized data.
            /// </summary>
            public enum ContentTreeVersion : byte
            {
                /// <summary>
                /// An unknown version of the ContentTree. Most likely an invalid format.
                /// </summary>
                Unknown = 0,
                /// <summary>
                /// Latest version.
                /// </summary>
                Latest = 1

                //When updating the version, be sure to change the "Latest" number,
                //  create a new entry for the old version and write down what it *can't do* compared to the latest!
            }

            /// <summary>
            /// Serializes the specified nodes, writing the result to the specified stream.
            /// </summary>
            /// <param name="nodes">Root elements in the node list. Only nodes with a null Parent are included.</param>
            /// <param name="stream">The stream to write data on.</param>
            public void Serialize(IList<ContentBrowserNode> nodes, Stream stream)
            {
                //Check writability
                if (!stream.CanWrite)
                    throw new ArgumentException("Cannot write to specified stream for serializing.", nameof(stream));

                if (string.IsNullOrWhiteSpace(this.BasePath))
                    throw new InvalidDataException("SFM base path is not specified for serializing the node tree data.");

                //Initialize writer
                this._writer = new BinaryWriter(stream);

                //Header (4 bytes)
                this._writer.Write(ContentTreeHeader.ToCharArray());

                //Version (1 byte)
                this._writer.Write((byte)ContentTreeVersion.Latest);

                //SFM Base Path (Length-Prefixed)
                this._writer.Write(this.BasePath);

                //Node Count (4 bytes)
                this._writer.Write(nodes.Count);

                //Node List
                foreach (var node in nodes.Where(n => n.Parent == null))
                {
                    //Recursive writing
                    this.WriteNode(node);
                }

                //Footer (4 bytes)
                this._writer.Write(ContentTreeFooter.ToCharArray());
            }

            private void WriteNode(ContentBrowserNode node)
            {
                //Node Name (Length-Prefixed)
                this._writer.Write(node.Name);

                //Node Type (1 byte)
                this._writer.Write((byte)node.NodeType);

                //Data depending on Type
                switch (node.NodeType)
                {
                    case ContentBrowserNodeType.Undetected:
                        throw new InvalidDataException("Node may not be of undetected type when serializing!");
                    case ContentBrowserNodeType.File:
                        //Only can have type as a file

                        //File Type (Length-Prefixed)
                        this._writer.Write(node.FileType?.FullName ?? "[NULL]");

                        break;
                    case ContentBrowserNodeType.Directory:
                        //Only can have children as a directory

                        //Child Count (4 bytes)
                        this._writer.Write(node.Children.Count);

                        //Children
                        foreach (var child in node.Children)
                            this.WriteNode(child);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(node.NodeType));
                }
            }

            /// <summary>
            /// Serializes the specified nodes, returning the resulting data to serialize.
            /// </summary>
            /// <param name="nodes">Root elements in the node list. Only nodes with a null Parent are included.</param>
            public byte[] Serialize(IList<ContentBrowserNode> nodes)
            {
                var stream = new MemoryStream();
                this.Serialize(nodes, stream);
                return stream.GetBuffer();
            }

            /// <summary>
            /// Serializes the specified nodes, writing the result to the specified file.
            /// </summary>
            /// <param name="nodes">Root elements in the node list. Only nodes with a null Parent are included.</param>
            /// <param name="path">The path of the file to write to.</param>
            public void Serialize(IList<ContentBrowserNode> nodes, string path)
            {
                using (var file = File.OpenWrite(path))
                {
                    this.Serialize(nodes, file);
                }
            }

            /// <summary>
            /// Deserializes the ContentTree data from the specified stream, returning the stored nodes.
            /// </summary>
            /// <param name="stream">The stream to read the serialized data from.</param>
            public IEnumerable<ContentBrowserNode> Deserialize(Stream stream)
            {
                //Check readability
                if (!stream.CanRead)
                    throw new ArgumentException("Cannot read from specified stream for deserializing.", nameof(stream));

                //Initialize reader
                this._reader = new BinaryReader(stream);

                //Header (4 bytes)
                var header = this._reader.ReadChars(4);
                if (!header.SequenceEqual(ContentTreeHeader.ToCharArray()))
                    throw new FormatException("The header of the ContentTree file does not match: " + new string(header));

                //Version (1 byte)
                var vernum = this._reader.ReadByte();
                if (!Enum.IsDefined(typeof(ContentTreeVersion), vernum))
                    throw new FormatException("Unknown version in ContentTree file: " + vernum);
                var version = (ContentTreeVersion)vernum;

                //SFM Base Path (Length-Prefixed)
                this.BasePath = this._reader.ReadString();

                //Node Count (4 bytes)
                var nodecnt = this._reader.ReadInt32();

                //Node List
                for (var i = 0; i < nodecnt; i++)
                {
                    var node = this.ReadNode(null);
                    yield return node;
                }

                //Footer (4 bytes)
                var footer = this._reader.ReadChars(4);
                if (!footer.SequenceEqual(ContentTreeFooter.ToCharArray()))
                    throw new FormatException("The footer of the ContentTree file does not match: " + new string(footer));
            }

            private ContentBrowserNode ReadNode(ContentBrowserNode parent)
            {
                ContentBrowserNode node;

                //Node Name (Length-Prefixed)
                var name = this._reader.ReadString();

                //Node Type (1 byte)
                var nodetype = (ContentBrowserNodeType)this._reader.ReadByte();

                //Data depending on Type
                switch (nodetype)
                {
                    case ContentBrowserNodeType.Undetected:
                        throw new InvalidDataException("Node may not be of undetected type when deserializing!");
                    case ContentBrowserNodeType.File:
                        //Only can have type as a file

                        //File Type (Length-Prefixed)
                        var typestr = this._reader.ReadString();
                        node = new ContentBrowserNode(Path.GetFileName(name), parent, typestr);

                        break;
                    case ContentBrowserNodeType.Directory:
                        //Only can have children as a directory
                        node = new ContentBrowserNode(name, parent, true);

                        //Child Count (4 bytes)
                        var childcnt = this._reader.ReadInt32();

                        //Children
                        for (var i = 0; i < childcnt; i++)
                        {
                            node.Children.Add(this.ReadNode(node));
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(nodetype));
                }

                return node;
            }

            /// <summary>
            /// Deserializes the specified ContentTree data, returning the stored nodes.
            /// </summary>
            /// <param name="data">The byte array with the ContentTree data.</param>
            public IEnumerable<ContentBrowserNode> Deserialize(byte[] data)
            {
                foreach (var node in this.Deserialize(new MemoryStream(data)))
                    yield return node;
            }

            /// <summary>
            /// Deserializes the ContentTree data from the specified file, returning the stored nodes.
            /// </summary>
            /// <param name="path">The file to read the serialized data from.</param>
            public IEnumerable<ContentBrowserNode> Deserialize(string path)
            {
                using (var file = File.OpenRead(path))
                {
                    foreach (var node in this.Deserialize(file))
                        yield return node;
                }
            }
        }
    }
}
