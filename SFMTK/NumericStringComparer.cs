﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SFMTK
{
    /// <summary>
    /// A string comparer which tries to sort by any numbers inside of the string.
    /// </summary>
    public class NumericStringComparer : IComparer, IComparer<string>
    {
        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.Value Meaning Less than zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than <paramref name="y" />.
        /// </returns>
        public int Compare(string x, string y)
        {
            const string regex = @"\d*";
            var nx = Regex.Match(x, regex).Value;
            var ny = Regex.Match(y, regex).Value;
            if (int.TryParse(nx, out int ix) && int.TryParse(ny, out int iy))
                return nx.CompareTo(ny);
            return x.CompareTo(y);
        }

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.Value Meaning Less than zero <paramref name="x" /> is less than <paramref name="y" />. Zero <paramref name="x" /> equals <paramref name="y" />. Greater than zero <paramref name="x" /> is greater than <paramref name="y" />.
        /// </returns>
        public int Compare(object x, object y) => Compare(x.ToString(), y.ToString());
    }
}
