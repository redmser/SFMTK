﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace SFMTK.Commands
{
    /// <summary>
    /// A manager for all Commands in the given form it is placed in.
    /// </summary>
    /// <seealso cref="System.ComponentModel.Component" />
    /// <seealso cref="System.ComponentModel.IExtenderProvider" />
    [ProvideProperty("Command", typeof(Component))]
    public class CommandManager : Component, IExtenderProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandManager"/> class.
        /// </summary>
        public CommandManager()
        {
            CommandManager.Managers.Add(this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandManager"/> class.
        /// </summary>
        public CommandManager(CommandHistory history) : this()
        {
            this.CommandHistory = history;
        }

        /// <summary>
        /// Gets or sets the CommandHistory assigned to this manager.
        /// </summary>
        [Browsable(false)]
        public CommandHistory CommandHistory { get; set; }

        /// <summary>
        /// Gets a list of CommandManager instances.
        /// </summary>
        public static List<CommandManager> Managers { get; } = new List<CommandManager>();

        /// <summary>
        /// List of components and their assigned command instances.
        /// This list should only be edited through the designer or methods.
        /// </summary>
        [Browsable(false)]
        public Dictionary<Component, CommandBase> Assignments { get; } = new Dictionary<Component, CommandBase>();

        /// <summary>
        /// Gets a list of command instances - may have user-defined properties.
        /// </summary>
        public static List<CommandBase> Commands { get; } = new List<CommandBase>();

        static CommandManager() => InitializeCommandTypes();

        /// <summary>
        /// Initializes the list of known command types.
        /// Done only as the static constructor, if possible add instances manually instead.
        /// </summary>
        public static void InitializeCommandTypes()
        {
            Commands.Clear();

            //Get basic commands in - module's are added later on
            Commands.AddRange(Reflection.GetSubclassInstances<Command>(Modules.ModuleScope.None));

            //Add specialized commands
            for (var i = 0; i < 10; i++)
            {
                Commands.Add(new ApplyLayoutCommand(i));
            }

            //Add widget commands
            var widgets = Reflection.GetSubclassInstances<Widgets.Widget>(Modules.ModuleScope.None);
            Commands.AddRange(widgets.Where(w => w.GenerateShowCommand)
                .Select(w => new ShowWidgetCommand(w, w.Icon.ToBitmap().Resize(16,16))));

            //Dispose of the widgets we created
            foreach (var w in widgets)
                w.Dispose();
        }

        /// <summary>
        /// Gets the <see cref="ShowWidgetCommand"/> used to display the specified type of widget.
        /// </summary>
        /// <typeparam name="T">The type of widget that should be shown.</typeparam>
        public static ShowWidgetCommand GetWidgetCommand<T>() where T : Widgets.Widget
        {
            var w = Commands.OfType<ShowWidgetCommand>().FirstOrDefault(c => c.Widget.GetType() == typeof(T));
            if (w != null)
                return w;

            //HACK: CommandManager - create instance of ShowWidgetCommand if unknown
            var widget = Activator.CreateInstance<T>();
            var swc = new ShowWidgetCommand(widget, widget.Icon.ToBitmap().Resize(16, 16));
            Commands.Add(swc);
            widget.Dispose();
            return swc;
        }

        /// <summary>
        /// Specifies whether this object can provide its extender properties to the specified object.
        /// </summary>
        /// <param name="extendee">The <see cref="T:System.Object" /> to receive the extender properties.</param>
        /// <returns>
        /// true if this object can provide extender properties to the specified object; otherwise, false.
        /// </returns>
        public bool CanExtend(object extendee) => (extendee is Button) || (extendee is ToolStripItem);

        /// <summary>
        /// Designer call for getting the name of the command assigned to the specified Component.
        /// </summary>
        [ExtenderProvidedProperty]
        [DisplayName("Command")]
        public string GetCommand(Component extendee)
        {
            if (this.Assignments.TryGetValue(extendee, out CommandBase value))
            {
                var args = value?.FormatCommandArgs() ?? null;
                var rest = args == null ? string.Empty : (";" + args);
                return (value?.GetType()?.Name ?? string.Empty) + rest;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Designer call for setting the name of the command assigned to the specified Component.
        /// </summary>
        public void SetCommand(Component extendee, string value)
        {
            if (!this.Assignments.ContainsKey(extendee))
                this.Assignments.Add(extendee, null);
            if (string.IsNullOrWhiteSpace(value))
            {
                this.Assignments[extendee] = null;
                return;
            }

            //TODO: CommandManager - for performance and name-collision-prevention, user should include module namespace too (fallback to SFMTK's)
            //  -> no need to get module type then, since we can simply compare type strings in the commands list
            var split = value.Split(';').ToList();
            var type = Reflection.GetModuleType("Commands." + split[0]);
            split.RemoveAt(0);
            if (type == typeof(Command) || type == typeof(SpecializedCommand) || type == typeof(CommandGroup))
                throw new InvalidOperationException("May not use Command or CommandGroup.");

            //Initialize the command - we don't accept unknown commands
            var cmdinst = Commands.FirstOrDefault(c => c.GetType() == type);
            if (cmdinst == null)
                throw new ArgumentException($"The specified type {type.FullName} is not a known command type.");

            var cmd = cmdinst.InitializeCommand(split.ToArray());
            this.Assignments[extendee] = cmd;
            this.ApplyCommand(extendee, cmd);
        }

        /// <summary>
        /// Set the command of the specified component.
        /// </summary>
        public void SetCommand(Component extendee, CommandBase command)
        {
            if (!this.Assignments.ContainsKey(extendee))
                this.Assignments.Add(extendee, null);

            this.Assignments[extendee] = command;
            this.ApplyCommand(extendee, command);
        }

        /// <summary>
        /// Applies the properties of all commands to all their assigned Components.
        /// </summary>
        public void ApplyCommand()
        {
            foreach (var kvp in this.Assignments)
            {
                this.ApplyCommand(kvp.Key, kvp.Value);
            }
        }

        /// <summary>
        /// Applies the properties of the specified command to the specified Component.
        /// </summary>
        public void ApplyCommand(Component extendee, CommandBase apply)
        {
            if (apply == null)
                return;

            if (extendee is Button button)
            {
                button.Image = apply.Icon;
                button.Text = apply.Name;

                button.Click -= this.ClickCallback;
                button.Click += this.ClickCallback;

                //IMP: CommandManager - allow buttons to have keyboard shortcuts as well
                //  -> best approach may be setting form's KeyPreview = true and subscribing to the KeyDown event (how to check if button is visible?)
                //UNTESTED: CommandManager - set tooltiptext of button
                try
                {
                    var comps = ControlsHelper.GetComponents(button.FindForm());
                    var tooltip = comps.Components.OfType<ToolTip>().First();

                    string text;
                    var display = StringFromKeys(apply.Shortcut);
                    if (display == "None")
                        text = apply.Name.Replace("&", "");
                    else
                        text = $"{apply.Name.Replace("&", "")} ({display})";
                    tooltip.SetToolTip(button, text);
                }
                catch (Exception)
                {
                    //Can't add tooltip if no components / no ToolTip on form!
                }
            }
            else if (extendee is ToolStripButton tsb)
            {
                tsb.Image = apply.Icon;
                tsb.Text = apply.Name;
                var display = StringFromKeys(apply.Shortcut);
                if (display == "None")
                    tsb.ToolTipText = apply.Name.Replace("&", "");
                else
                    tsb.ToolTipText = $"{apply.Name.Replace("&", "")} ({display})";

                tsb.Click -= this.ClickCallback;
                tsb.Click += this.ClickCallback;
            }
            else if (extendee is ToolStripSplitButton tssb)
            {
                tssb.Image = apply.Icon;
                tssb.Text = apply.Name;
                var display = StringFromKeys(apply.Shortcut);
                if (display == "None")
                    tssb.ToolTipText = apply.Name.Replace("&", "");
                else
                    tssb.ToolTipText = $"{apply.Name.Replace("&", "")} ({display})";

                tssb.ButtonClick -= this.ClickCallback;
                tssb.ButtonClick += this.ClickCallback;
            }
            else if (extendee is ToolStripDropDownButton tsddb)
            {
                tsddb.Image = apply.Icon;
                tsddb.Text = apply.Name;
                var display = StringFromKeys(apply.Shortcut);
                if (display == "None")
                    tsddb.ToolTipText = apply.Name.Replace("&", "");
                else
                    tsddb.ToolTipText = $"{apply.Name.Replace("&", "")} ({display})";

                tsddb.Click -= this.ClickCallback;
                tsddb.Click += this.ClickCallback;
            }
            else if (extendee is ToolStripMenuItem item)
            {
                item.Image = apply.Icon;
                item.Text = apply.Name;
                item.ToolTipText = apply.ToolTipText;
                item.ShortcutKeys = apply.Shortcut;
                var display = StringFromKeys(apply.Shortcut);
                if (display == "None")
                    display = "";
                item.ShortcutKeyDisplayString = display;

                item.Click -= this.ClickCallback;
                item.Click += this.ClickCallback;
            }
            
            apply.ShortcutChanged -= this.RefreshElementsHandler;
            apply.ShortcutChanged += this.RefreshElementsHandler;
            apply.NameChanged -= this.RefreshElementsHandler;
            apply.NameChanged += this.RefreshElementsHandler;
            apply.IconChanged -= this.RefreshElementsHandler;
            apply.IconChanged += this.RefreshElementsHandler;
        }

        private void RefreshElementsHandler(object sender, EventArgs e)
        {
            var cmd = sender as Command;
            foreach (var kvp in this.Assignments.Where(kvp => kvp.Value == cmd))
            {
                this.ApplyCommand(kvp.Key, cmd);
            }
        }

        private void ClickCallback(object sender, EventArgs e)
        {
            var comp = sender as Component;
            if (comp == null)
                throw new ArgumentNullException(nameof(comp));

            //No click event for dropdowns
            if (comp is ToolStripMenuItem tsmi && tsmi.HasDropDownItems)
                return;

            if (this.Assignments.TryGetValue(comp, out CommandBase cmd))
            {
                //BUG: CommandManager - Document can be set wrongly under certain circumstances (e.g., selecting a document,
                //  pressing the Context Menu button of a dock pane, and executing one of its commands)
                cmd.Document = (Program.MainDP.ActiveDocument ?? Program.MainDP.ActiveContent) as WeifenLuo.WinFormsUI.Docking.DockContent;
                (this.CommandHistory ?? Program.MainCH).Execute(cmd);
            }
            else
            {
                throw new ArgumentNullException(nameof(cmd));
            }
        }

        /// <summary>
        /// Creates a new ToolStripMenuItem with the specified Command assigned.
        /// </summary>
        /// <param name="type">Type of the command.</param>
        public ToolStripMenuItem CreateToolStripMenuItem(Type type)
        {
            var tsmi = new ToolStripMenuItem();
            this.SetCommand(tsmi, type.Name);
            return tsmi;
        }

        /// <summary>
        /// Creates a new ToolStripMenuItem with the specified Command assigned.
        /// </summary>
        /// <param name="command">Instance of the command to apply.</param>
        public ToolStripMenuItem CreateToolStripMenuItem(CommandBase command)
        {
            var tsmi = new ToolStripMenuItem();
            this.SetCommand(tsmi, command);
            return tsmi;
        }

        /// <summary>
        /// Applies the specified key string, returning a corresponding <see cref="Keys"/> value.
        /// </summary>
        /// <param name="keystring">The string to convert to a <see cref="Keys"/> object.</param>
        /// <param name="validateShortcut">If set to <c>true</c>, ensures that the specified key string is a valid shortcut.</param>
        public static Keys KeysFromString(string keystring, bool validateShortcut = true)
        {
            if (string.IsNullOrWhiteSpace(keystring))
                return Keys.None;

            var ctrl = false;
            var shift = false;
            var alt = false;

            while (true)
            {
                if (keystring.StartsWith("ctrl", StringComparison.InvariantCultureIgnoreCase))
                {
                    ctrl = true;
                    keystring = keystring.Substring(5);
                }
                else if (keystring.StartsWith("control", StringComparison.InvariantCultureIgnoreCase))
                {
                    ctrl = true;
                    keystring = keystring.Substring(8);
                }
                else if (keystring.StartsWith("shift", StringComparison.InvariantCultureIgnoreCase))
                {
                    shift = true;
                    keystring = keystring.Substring(6);
                }
                else if (keystring.StartsWith("alt", StringComparison.InvariantCultureIgnoreCase))
                {
                    alt = true;
                    keystring = keystring.Substring(4);
                }
                else if (keystring.StartsWith("altgr", StringComparison.InvariantCultureIgnoreCase))
                {
                    ctrl = true;
                    alt = true;
                    keystring = keystring.Substring(6);
                }
                else
                {
                    //Get base key
                    var invalid = false;
                    var keystr = keystring.Trim();
                    if (int.TryParse(keystr, out int num))
                    {
                        //How to deal with numbers: 0 to 9 adds a D, others are invalid
                        if (num < 0 || num > 9)
                        {
                            //Invalidate
                            invalid = true;
                        }
                        else
                        {
                            //Make numbers valid without the D
                            keystr = "D" + keystr;
                        }
                    }
                    //TODO: Shortcut - accept key names with Oem

                    if (!invalid && Enum.TryParse(keystr, true, out Keys key))
                    {
                        var combined = key | (ctrl ? Keys.Control : Keys.None) | (shift ? Keys.Shift : Keys.None) | (alt ? Keys.Alt : Keys.None);
                        if (validateShortcut && !ToolStripManager.IsValidShortcut(combined))
                        {
                            //Is invalid shortcut
                            throw new ArgumentException("Shortcut is not valid! Be sure that any letter key uses control or alt modifiers.");
                        }

                        return combined;
                    }
                    else
                    {
                        //Invalid key!
                        throw new ArgumentException($"Invalid key name \"{keystr}\" specified!");
                    }
                }
            }
        }

        /// <summary>
        /// Returns the string for the specified Keys.
        /// </summary>
        /// <param name="keys"></param>
        public static string StringFromKeys(Keys keys)
        {
            var ctrl = ((keys & Keys.Control) != 0) ? "Ctrl+" : string.Empty;
            var shift = ((keys & Keys.Shift) != 0) ? "Shift+" : string.Empty;
            var alt = ((keys & Keys.Alt) != 0) ? "Alt+" : string.Empty;
            var basekey = keys & ~(Keys.Control | Keys.Shift | Keys.Alt);
            string keystr;
            switch (basekey)
            {
                case Keys.D0:
                case Keys.D1:
                case Keys.D2:
                case Keys.D3:
                case Keys.D4:
                case Keys.D5:
                case Keys.D6:
                case Keys.D7:
                case Keys.D8:
                case Keys.D9:
                    keystr = basekey.ToString()[1].ToString();
                    break;
                default:
                    keystr = basekey.ToString();
                    break;
            }

            return $"{ctrl}{shift}{alt}{keystr}";
        }

        /// <summary>
        /// Updates the enabled state of all children of the specified menuitem depending on the enabled state of the respective commands.
        /// <para/>If a button is specified, it is updated as it is.
        /// </summary>
        public static void UpdateCommandsEnabled(string context, Component menuitem, bool includeSelf)
        {
            if (menuitem is Button b)
            {
                //Get assigned command, if any
                foreach (var cm in CommandManager.Managers)
                {
                    if (cm.Assignments.TryGetValue(b, out CommandBase cmd) && cmd != null)
                    {
                        b.Enabled = cmd.CanBeExecuted(new CommandExecuteContext(b.Name, context));
                    }
                }
                return;
            }

            foreach (var subitem in ControlsHelper.RecurseItems(menuitem, includeSelf))
            {
                //Get assigned command, if any
                foreach (var cm in CommandManager.Managers)
                {
                    if (cm.Assignments.TryGetValue(subitem, out CommandBase cmd) && cmd != null)
                    {
                        subitem.Enabled = cmd.CanBeExecuted(new CommandExecuteContext(subitem.Name, context));
                    }
                }
            }
        }

        /// <summary>
        /// Updates the enabled state of all user elements using the commands of the specified types (includes subclasses).
        /// </summary>
        public static void UpdateCommandsEnabled(string context, params Type[] commandtypes)
        {
            foreach (var ty in commandtypes)
            {
                if (!typeof(Command).IsAssignableFrom(ty))
                    throw new InvalidCastException($"Can't convert type \"{ty.Name}\" to Command!");

                //Find commands of this type to update
                foreach (var cm in CommandManager.Managers)
                {
                    foreach (var kvp in cm.Assignments.Where(kvp => kvp.Value != null && ty.IsAssignableFrom(kvp.Value.GetType())))
                    {
                        var comp = kvp.Key;
                        if (comp is Control control)
                        {
                            //Control can be invoked directly
                            control.InvokeIfRequired(() => { control.Enabled = kvp.Value.CanBeExecuted(new CommandExecuteContext(control.Name, context)); });
                        }
                        else
                        {
                            //Component is a bit tougher
                            if (comp is ToolStripItem tsi)
                            {
                                //Invoke toolstrip instead
                                var ts = tsi.GetCurrentParent();
                                if (ts == null)
                                    tsi.Enabled = kvp.Value.CanBeExecuted(new CommandExecuteContext(tsi.Name, context));
                                else
                                    ts.InvokeIfRequired(() => { tsi.Enabled = kvp.Value.CanBeExecuted(new CommandExecuteContext(tsi.Name, context)); });
                            }
                            else
                            {
                                //Unknown type
                                throw new ArgumentException("Tried to set enabled state of an unknown component type " + comp.GetType().FullName, nameof(comp));
                            }
                        }
                    }
                }
            }
        }
    }
}
