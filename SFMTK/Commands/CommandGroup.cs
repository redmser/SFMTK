﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFMTK.Widgets;

namespace SFMTK.Commands
{
    /// <summary>
    /// A group of commands that get executed and undone together, and in order. This class cannot be inherited.
    /// </summary>
    public sealed class CommandGroup : Command
    {
        // Notes to anyone looking to extend the Command system:
        // - Use command groups for executing multiple commands as one, in order to avoid creating many unneeded commands.

        //TODO: CommandGroup - since it will probably be needed, add before/after execute/undo methods that are called as Actions (cancelling option for
        //                     the before call with an error string, and after should only show an error obviously). Will need to listen to the ContinueOnError setting.

        private Func<string> DisplayFunc { get; }

        /// <summary>
        /// The list of commands to be executed.
        /// </summary>
        public IEnumerable<Command> Commands { get; }

        /// <summary>
        /// Whether to continue execution/undo when any command errors. Default is ContinueOnUndo.
        /// </summary>
        public ContinueOnErrorSetting ContinueOnError { get; set; } = ContinueOnErrorSetting.ContinueOnUndo;

        /// <summary>
        /// Whether to reverse the order of command execution when undoing the CommandGroup.
        /// </summary>
        public bool ReverseOrderOnUndo { get; set; } = true;

        /// <summary>
        /// Creates a new command group that is to be executed for the specified document.
        /// </summary>
        /// <param name="display">Display text of this CommandGroup.</param>
        /// <param name="commands">List of commands part of this group, executed in the order specified.</param>
        public CommandGroup(string display, IEnumerable<Command> commands) : base()
        {
            this.Commands = commands;
            this.DisplayFunc = () => display;
            this.Name = this.DisplayText;
        }

        /// <summary>
        /// Creates a new command group that is to be executed for the specified document. The display name is determined using a function.
        /// </summary>
        /// <param name="doc">Document to execute commands on. May be null, if no document is active. The commands have to determine by themself whether this context is important.</param>
        /// <param name="displayfunc">A function for determining the display text of this CommandGroup.</param>
        /// <param name="commands">List of commands part of this group, executed in the order specified.</param>
        public CommandGroup(Func<string> displayfunc, IEnumerable<Command> commands) : base()
        {
            this.Commands = commands;
            this.DisplayFunc = displayfunc;
            this.Name = this.DisplayText;
        }

        /// <summary>
        /// Display name of this action.
        /// </summary>
        public override string DisplayText => this.DisplayFunc.Invoke();

        /// <summary>
        /// Executes or redoes the action group. This should save any information needed to undo it at any point later on. This should not be called manually.
        /// <para />Returns whether the actions were successful (will add a new entry to the history).
        /// </summary>
        public override string Execute()
        {
            var errors = new StringBuilder();
            foreach (var action in this.Commands)
            {
                var curr = action.Execute();
                if (curr == null)
                    continue;

                //ERROR: act depending on setting
                switch (this.ContinueOnError)
                {
                    case ContinueOnErrorSetting.AlwaysContinue:
                    case ContinueOnErrorSetting.ContinueOnExecute:
                        errors.AppendLine(curr);
                        continue;
                    default:
                        return curr;
                }
            }
            return errors.Length <= 0 ? null : errors.ToString();
        }

        /// <summary>
        /// Undoes whatever the action group did in reverse order. This should not be executed before executing the action. This should not be called manually.
        /// <para />Returns whether the undo was successful (will take a step backwards in the history).
        /// </summary>
        public override string Undo()
        {
            var errors = new StringBuilder();
            var list = this.ReverseOrderOnUndo ? this.Commands.Reverse<Command>() : this.Commands;
            foreach (var action in list)
            {
                var curr = action.Undo();
                if (curr == null)
                    continue;

                //ERROR: act depending on setting
                switch (this.ContinueOnError)
                {
                    case ContinueOnErrorSetting.AlwaysContinue:
                    case ContinueOnErrorSetting.ContinueOnUndo:
                        errors.AppendLine(curr);
                        continue;
                    default:
                        return curr;
                }
            }
            return errors.Length <= 0 ? null : errors.ToString();
        }
    }

    /// <summary>
    /// How to act when an error occurs in a CommandGroup.
    /// </summary>
    public enum ContinueOnErrorSetting
    {
        /// <summary>
        /// Continues to undo until the end of the CommandGroup, but cancels instantly when executing.
        /// </summary>
        ContinueOnUndo,
        /// <summary>
        /// Continues to execute until the end of the CommandGroup, but cancels instantly when undoing.
        /// </summary>
        ContinueOnExecute,
        /// <summary>
        /// Silently ignores all errors until the CommandGroup is done, where it will print all errors that occurred.
        /// </summary>
        AlwaysContinue,
        /// <summary>
        /// Instantly stops the CommandGroup once any Command errors, printing out the first error that occurred.
        /// </summary>
        NeverContinue
    }
}
