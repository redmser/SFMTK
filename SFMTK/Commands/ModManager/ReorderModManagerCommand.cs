﻿using SFMTK.Data;

namespace SFMTK.Commands
{
    public class ReorderModManagerCommand : Command
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReorderModManagerCommand"/> class.
        /// </summary>
        public ReorderModManagerCommand() : base()
        {
            this.Name = "Reorder Mod";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReorderModManagerCommand" /> class.
        /// </summary>
        /// <param name="mod">The mod to be moved (for getting the "current" index).</param>
        /// <param name="oldIndex">The old index of the mod.</param>
        /// <param name="newIndex">The new index of the mod.</param>
        public ReorderModManagerCommand(Mod mod, int oldIndex, int newIndex) : this()
        {
            this.Mod = mod;
            this.OldIndex = oldIndex;
            this.NewIndex = newIndex;
        }

        /// <summary>
        /// Gets or sets the mod to be moved (for getting the "current" index).
        /// </summary>
        public Mod Mod { get; set; }

        /// <summary>
        /// Gets or sets the old index of the mod.
        /// </summary>
        public int OldIndex { get; set; }

        /// <summary>
        /// Gets or sets the new index of the mod.
        /// </summary>
        public int NewIndex { get; set; }

        /// <summary>
        /// Gets the current index of the mod.
        /// </summary>
        public int CurrentIndex => SFM.GameInfo.Mods.IndexOf(this.Mod);

        public override string DisplayText => $"Reorder Mod \"{this.Mod.Name}\"";
        public override string Category => "Mod Manager";
        public override bool ShortcutEditable => false;

        public override string Execute()
        {
            if (this.Mod == null)
                return "No mod assigned to the command.";

            if (this.CurrentIndex < 0)
                return "Mod not found in GameInfo!";

            //Move mod from current place to new index
            SFM.GameInfo.Mods.Move(this.CurrentIndex, this.NewIndex);
            return null;
        }

        public override string Undo()
        {
            if (this.Mod == null)
                return "No mod assigned to the command.";

            if (this.CurrentIndex < 0)
                return "Mod not found in GameInfo!";

            //Move mod from current place to old index
            SFM.GameInfo.Mods.Move(this.CurrentIndex, this.OldIndex);
            return null;
        }
    }
}
