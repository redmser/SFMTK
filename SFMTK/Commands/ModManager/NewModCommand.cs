﻿using SFMTK.Forms;
using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class NewModCommand : Command
    {
        public NewModCommand() : base()
        {
            this.Name = "&New Mod...";
            this.Icon = Properties.Resources.add;
        }

        public override string Category => "Mod Manager";
        public override string ToolTipText => "Create a new mod and optionally import content into it";

        public override string Execute()
        {
            var newmod = new NewMod();
            if (newmod.ShowDialog(Program.MainForm) == DialogResult.OK)
            {
                //Add new mod to list
                SFM.GameInfo.Mods.Add(newmod.Mod);
            }
            return null;
        }
    }
}
