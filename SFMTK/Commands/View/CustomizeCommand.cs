﻿using SFMTK.Forms;

namespace SFMTK.Commands
{
    public class CustomizeCommand : Command
    {
        public CustomizeCommand() : base()
        {
            this.Name = "&Customize...";
        }

        public override string Category => "View";
        public override string ToolTipText => "Opens an editor for customizing all toolbars";

        public override string Execute()
        {
            //Open toolstrip customizer
            var cust = new CustomizeToolstrips();
            cust.Show(Program.MainForm);
            return null;
        }
    }
}
