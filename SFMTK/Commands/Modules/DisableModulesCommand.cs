﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class DisableModulesCommand : ModulesListCommand
    {
        public DisableModulesCommand() : base()
        {
            this.Name = "&Disable selected";
            this.SetDefaultShortcut(Keys.Control | Keys.D);
            this.Icon = Properties.Resources.cross;
        }

        public override string ListName => "Disable selected Modules";

        public override string Execute()
        {
            //Disable selected modules in list
            var mpp = this.GetModulesPreferencePage();
            foreach (var ms in mpp.SelectedModules)
            {
                //Set action based on current Load state
                ms.Action = ms.Load ? Modules.ModuleSettingAction.Toggle : Modules.ModuleSettingAction.None;
            }

            //Update list
            mpp.UpdateList();
            return null;
        }
    }
}
