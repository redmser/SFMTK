﻿using System.Linq;
using SFMTK.Controls.PreferencePages;

namespace SFMTK.Commands
{
    public abstract class ModulesListCommand : Command
    {
        public override string Category => "Module Settings";

        public override bool CanBeExecuted(CommandExecuteContext context)
        {
            //Check for known context
            if (context.FormName != nameof(ModulePreferencePage))
                return false;

            return GetModulesPreferencePage()?.SelectedModules?.Any() ?? false;
        }

        protected ModulePreferencePage GetModulesPreferencePage()
        {
            //Get preferencepage in question - can't check context here
            return (Program.MainForm.PreferencesDialog?.ActivePage as ModulePreferencePage);
        }
    }
}
