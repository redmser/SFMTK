﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class InstallModulesCommand : Command
    {
        public InstallModulesCommand() : base()
        {
            this.Name = "&Install Modules...";
            this.SetDefaultShortcut(Keys.Control | Keys.O);
            this.Icon = Properties.Resources.plugin_add;
        }

        public override string ListName => "Install Modules";

        public override string Category => "Module Settings";

        public override string Execute()
        {
            //NYI: ModulePreferencePage - install module dialog (file/folder/zip)
            //  -> only add new modulesetting to list, with Action = Install
            throw new System.NotImplementedException();
        }
    }
}
