﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class EnableModulesCommand : ModulesListCommand
    {
        //TODO: Enable/DisableModulesCommand - allow undoing these settings changes -> should be handled in preference's command history instead

        public EnableModulesCommand() : base()
        {
            this.Name = "&Enable selected";
            this.SetDefaultShortcut(Keys.Control | Keys.E);
            this.Icon = Properties.Resources.tick;
        }

        public override string ListName => "Enable selected Modules";

        public override string Execute()
        {
            //Enable selected modules in list
            var mpp = this.GetModulesPreferencePage();
            foreach (var ms in mpp.SelectedModules)
            {
                //Set action based on current Load state
                ms.Action = ms.Load ? Modules.ModuleSettingAction.None : Modules.ModuleSettingAction.Toggle;
            }

            //Update list
            mpp.UpdateList();
            return null;
        }
    }
}
