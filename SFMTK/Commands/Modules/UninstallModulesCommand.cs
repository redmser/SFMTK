﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class UninstallModulesCommand : ModulesListCommand
    {
        public UninstallModulesCommand() : base()
        {
            this.Name = "&Uninstall selected";
            this.SetDefaultShortcut(Keys.Delete);
            this.Icon = Properties.Resources.delete;
        }

        public override string ListName => "Uninstall selected Modules";

        public override string Execute()
        {
            //Mark selected modules for uninstall
            var mpp = this.GetModulesPreferencePage();
            foreach (var ms in mpp.SelectedModules)
            {
                //Set uninstall action state
                ms.Action = Modules.ModuleSettingAction.Uninstall;
            }

            //Update list
            mpp.UpdateList();
            return null;
        }
    }
}
