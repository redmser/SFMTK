﻿using System.IO;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Modules;

namespace SFMTK.Commands
{
    public class BrowseToModuleCommand : ModulesListCommand
    {
        public BrowseToModuleCommand() : base()
        {
            this.Name = "&Browse to module...";
            this.SetDefaultShortcut(Keys.Control | Keys.B);
            this.Icon = Properties.Resources.folder;
        }

        public override string ListName => "Browse to selected Module";

        public override string Category => "Module Settings";

        public override string Execute()
        {
            var selected = this.GetModulesPreferencePage()?.SelectedModules.ToList();
            if (selected != null && selected.Count == 1)
            {
                //If one selected, open explorer with dll selected
                var mod = Program.MainMM.GetModuleByName(selected[0].FullName);
                IOHelper.OpenInExplorer(mod.GetAssembly().CodeBase, false);
                return null;
            }

            //Open modules folder only
            IOHelper.OpenInExplorer(IOHelper.GetSFMTKDirectory(Constants.ModulesPath), false);
            return null;
        }

        public override bool CanBeExecuted(CommandExecuteContext context) => true;
    }
}
