﻿using BrightIdeasSoftware;

namespace SFMTK.Commands
{
    /// <summary>
    /// Model filter for commands. Searches in certain properties without watching upper or lower-case.
    /// </summary>
    public class CommandFilter : IModelFilter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShortcutFilter"/> class.
        /// </summary>
        /// <param name="text">The text to filter for.</param>
        public CommandFilter(string text)
        {
            this.FilterText = text;
        }

        /// <summary>
        /// The text to filter the shortcut entries after.
        /// </summary>
        public string FilterText { get; set; }

        /// <summary>
        /// Should the given model be included when this filter is installed
        /// </summary>
        /// <param name="modelObject">The model object to consider</param>
        /// <returns>
        /// Returns true if the model will be included by the filter
        /// </returns>
        public bool Filter(object modelObject)
        {
            if (modelObject is Command cmd)
            {
                var txt = this.FilterText;
                if (cmd.Name.ContainsIgnoreCase(txt) || cmd.InternalName.ContainsIgnoreCase(txt) || cmd.ToolTipText.ContainsIgnoreCase(txt))
                    return true;
            }
            return false;
        }
    }
}
