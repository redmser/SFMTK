﻿using ICSharpCode.AvalonEdit;

namespace SFMTK.Commands
{
    public class CutTextEditorCommand : Command
    {
        public CutTextEditorCommand() : base()
        {
            this.Name = "Cut Text";
            this.Icon = Properties.Resources.cut;
        }

        public CutTextEditorCommand(TextEditor editor) : this()
        {
            this.TextEditor = editor;
        }

        public override string ToolTipText => "Cuts the selected text segment";
        public override bool ShortcutEditable => false;

        public TextEditor TextEditor { get; }
        private int CaretOffset { get; set; }
        private int SelectionStart { get; set; }
        private int SelectionLength { get; set; }

        public override string Execute()
        {
            if (this.TextEditor == null)
                return "No text editor assigned to this command.";

            this.CaretOffset = this.TextEditor.CaretOffset;
            this.SelectionStart = this.TextEditor.SelectionStart;
            this.SelectionLength = this.TextEditor.SelectionLength;
            this.TextEditor.Cut();
            return null;
        }

        public override string Undo()
        {
            if (this.TextEditor == null)
                return "No text editor assigned to this command.";

            if (!this.TextEditor.CanUndo)
                return "Can not undo change, undo history is empty.";

            this.TextEditor.Undo();
            this.TextEditor.CaretOffset = this.CaretOffset;
            this.TextEditor.SelectionStart = this.SelectionStart;
            this.TextEditor.SelectionLength = this.SelectionLength;
            return null;
        }
    }
}
