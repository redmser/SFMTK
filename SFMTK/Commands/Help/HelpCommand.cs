﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    /// <summary>
    /// Used to display a help page, defined in <c>helppages.cfg</c>. Syntax: <c>HelpCommand;pagename</c>
    /// <para/>If the <c>pagename</c> ends with an exclamation mark "<c>!</c>", the command is formatted for use on a button with a label.
    /// </summary>
    public class HelpCommand : Command
    {
        public HelpCommand() : this(null, false)
        {

        }

        public HelpCommand(string page, bool showName) : base()
        {
            this.SetDefaultShortcut(Keys.F1);
            this.Icon = Properties.Resources.help;
            this.HelpPage = page;
            this.Name = showName ? "&Show help..." : string.Empty;
            this._showName = showName;
        }

        private bool _showName;

        public override string ListName => "Open contextual help";

        public override string Category => "Help";

        /// <summary>
        /// Gets or sets the name of the help page to show.
        /// </summary>
        public string HelpPage { get; set; }

        public override string Execute()
        {
            if (this.HelpPage == null)
                throw new System.ArgumentNullException(nameof(this.HelpPage));

            //Show help page specified
            Forms.HelpPageDialog.ShowHelp(this.HelpPage);
            return null;
        }

        public override bool CanBeExecuted(CommandExecuteContext context) => false;

        public override CommandBase InitializeCommand(string[] args)
        {
            if (args.Length == 0)
                return new HelpCommand();

            var page = args[0];
            var showName = page.EndsWith("!");
            if (showName)
                page = page.Substring(0, page.Length - 1);
            var cmd = new HelpCommand(page, showName);
            return cmd;
        }

        public override string FormatCommandArgs() => this.HelpPage + (this._showName ? "!" : string.Empty);
    }
}
