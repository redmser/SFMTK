﻿namespace SFMTK.Commands
{
    public class DonateCommand : Command
    {
        public DonateCommand() : base()
        {
            this.Name = "&Donate!";
            this.Icon = Properties.Resources.heart;
        }

        public override string Category => "Help";
        public override string ToolTipText => "If you like this software, a donation is always appreciated!";

        public override string Execute()
        {
            new Forms.Donate().Show(Program.MainForm);
            return null;
        }
    }
}
