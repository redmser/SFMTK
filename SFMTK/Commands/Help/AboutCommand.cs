﻿namespace SFMTK.Commands
{
    public class AboutCommand : Command
    {
        public AboutCommand() : base()
        {
            this.Name = "&About...";
            this.Icon = Properties.Resources.information;
        }

        public override string Category => "Help";
        public override string ToolTipText => "Information about the program and who made it possible";

        public override string Execute()
        {
            new Forms.About().Show(Program.MainForm);
            return null;
        }
    }
}
