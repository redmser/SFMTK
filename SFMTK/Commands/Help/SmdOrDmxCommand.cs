﻿namespace SFMTK.Commands
{
    public class SmdOrDmxCommand : Command
    {
        public SmdOrDmxCommand() : base()
        {
            this.Name = "&SMD or DMX?";
            this.Icon = Properties.Resources.boxquestion;
        }

        public override string Category => "Help";
        public override string ToolTipText => "Opens a wizard to help you decide whether to use the SMD or DMX format for models.";

        public override string Execute()
        {
            //NYI: SmdOrDmxCommand - dialog using wizard system
            throw new System.NotImplementedException();

            //Show the wizard
            //new Forms.SmdOrDmx().Show(Program.MainForm);
            //return null;
        }
    }
}
