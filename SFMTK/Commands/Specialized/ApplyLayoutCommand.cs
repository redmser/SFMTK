﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class ApplyLayoutCommand : SpecializedCommand
    {
        public ApplyLayoutCommand() : this(-1)
        {
            
        }

        public ApplyLayoutCommand(int layoutIndex) : base()
        {
            this.Index = layoutIndex;
            var key = IndexToNumberKey(layoutIndex);
            if (key != Keys.None)
                this.SetDefaultShortcut(key);
            


            Keys IndexToNumberKey(int index)
            {
                if (index > 9)
                    return Keys.None;
                if (index >= 0 && index < 9)
                    return Keys.Control | (index + Keys.D1);
                return Keys.Control | Keys.D0;
            }
        }

        public override string Name => "&" + (this.Layout?.Name ?? "Layout " + (this.Index + 1));

        public override string ListName
        {
            get
            {
                if (this.Layout == null)
                    return $"Apply Layout {this.Index + 1}";
                return $"Apply Layout {this.Index + 1} ({this.Layout.Name})";
            }
        }

        /// <summary>
        /// Gets the index of the window layout that this command will apply.
        /// </summary>
        public int Index { get; }

        /// <summary>
        /// Gets the window layout that this command will apply.
        /// </summary>
        public DockLayout Layout => (this.Index < 0 || this.Index >= DockLayout.Layouts.Count) ? null : DockLayout.Layouts[this.Index];

        public override string Category => "Window";
        public override string ToolTipText => $"Applies the window layout {this.Layout?.Name ?? "#" + (this.Index + 1)}";

        public override string Execute()
        {
            //Apply the layout
            this.Layout.LoadLayout();
            return null;
        }
    }
}
