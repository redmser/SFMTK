﻿using SFMTK.Widgets;
using System;
using System.Drawing;

namespace SFMTK.Commands
{
    public class ShowWidgetCommand : SpecializedCommand
    {
        public ShowWidgetCommand() : this(null, null)
        {

        }

        public ShowWidgetCommand(Widget widget, Image image) : base()
        {
            this.Widget = widget;
            this.SetDefaultShortcut(widget.DefaultShortcut);
            this.Icon = image;
        }

        public override string ToolTipText => $"{(MultipleInstances ? "Opens a new" : "Shows the")} {Name}";

        /// <summary>
        /// Gets or sets the widget to add.
        /// </summary>
        public Widget Widget { get; set; }

        /// <summary>
        /// Gets or sets whether Add or TryAdd should be used (deciding whether to allow a widget of that type to exist multiple times).
        /// </summary>
        public bool MultipleInstances { get; set; }

        /// <summary>
        /// Gets or sets the DockPanelManager to add the widget to.
        /// </summary>
        public DockPanelManager DockPanelManager
        {
            get => this._dpm ?? Program.MainDPM;
            set => this._dpm = value;
        }
        private DockPanelManager _dpm;

        /// <summary>
        /// Returns a new Widget instance to use.
        /// </summary>
        public Widget GetNewWidget() => (Widget)Activator.CreateInstance(this.Widget.GetType());

        public override string Category => "View";

        public override string Name => this.Widget.MenuText;

        public override string ListName => $"{(MultipleInstances ? "Open" : "Show")} {this.Widget.MenuText}";

        public override string Execute()
        {
            if (this.MultipleInstances)
            {
                this.DockPanelManager.Add(this.GetNewWidget());
            }
            else
            {
                this.DockPanelManager.TryAdd(this.GetNewWidget());
            }

            return null;
        }

        public override CommandBase InitializeCommand(string[] args)
        {
            if (args.Length == 0)
                return new ShowWidgetCommand();

            //TODO: ShowWidgetCommand - use full type name for widgets, to avoid type name collisions. fallback to SFMTK's namespace
            var wtype = Reflection.GetModuleType("Widgets." + args[0]);
            var widget = (Widget)Activator.CreateInstance(wtype);
            var cmd = new ShowWidgetCommand(widget, widget.Icon.ToBitmap());
            return cmd;
        }

        public override string FormatCommandArgs() => this.Widget?.GetType()?.Name ?? null;
    }
}
