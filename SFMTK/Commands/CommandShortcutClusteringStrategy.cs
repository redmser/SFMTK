﻿using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace SFMTK.Commands
{
    /// <summary>
    /// How shortcut keys should be clustered for filtering.
    /// </summary>
    /// <seealso cref="BrightIdeasSoftware.ClusteringStrategy" />
    public class CommandShortcutClusteringStrategy : ClusteringStrategy
    {
        //FIXME: CommandShortcutClusteringStrategy - this strategy does not make sense, since shortcuts with CTRL for example can
        //  also use other modifiers in combination, AND still are either modified or default shortcuts!
        //  -> manually implement the interface and cluster accordingly

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandShortcutClusteringStrategy"/> class.
        /// </summary>
        public CommandShortcutClusteringStrategy(CommandManager manager)
        {
            this.CommandManager = manager;
        }

        /// <summary>
        /// Gets the command manager assigned to this clustering strategy, for reading duplicate shortcut keys.
        /// </summary>
        public CommandManager CommandManager { get; }

        /// <summary>
        /// Get the cluster key by which the given model will be partitioned by this strategy
        /// </summary>
        /// <param name="model"></param>
        public override object GetClusterKey(object model)
        {
            var command = ((CommandBase)model);
            var keys = command.Shortcut;
            if (keys == Keys.None)
                return 0; //Keys.None
            if (CommandManager.Commands.Count(c => c.Shortcut == keys) > 1)
                return 1; //Duplicate
            if ((keys & Keys.Control) != 0)
                return 2; //CTRL
            if ((keys & Keys.Shift) != 0)
                return 3; //SHIFT
            if ((keys & Keys.Alt) != 0)
                return 4; //ALT
            if (!command.UsesDefaultShortcut)
                return 5; //Changed
            return 6; //Other
        }

        /// <summary>
        /// Gets the display label that the given cluster should use
        /// </summary>
        /// <param name="cluster"></param>
        public override string GetClusterDisplayLabel(ICluster cluster)
        {
            var basestr = GetClusterDisplayLabelInternal((int)cluster.ClusterKey);
            return $"{basestr} ({StringHelper.GetPlural(cluster.Count, "shortcut")})";
        }

        private static string GetClusterDisplayLabelInternal(int key)
        {
            switch (key)
            {
                case 0:
                    return "No Shortcut";
                case 1:
                    return "Duplicate Shortcut";
                case 2:
                    return "CTRL + ...";
                case 3:
                    return "SHIFT + ...";
                case 4:
                    return "ALT + ...";
                case 5:
                    return "User-Defined Shortcut";
                default:
                    return "Default Shortcut";
            }
        }
    }
}
