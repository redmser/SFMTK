﻿namespace SFMTK.Commands
{
    public class CommandExecuteContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandExecuteContext"/> class.
        /// </summary>
        public CommandExecuteContext(string sender, string form)
        {
            this.SenderName = sender;
            this.FormName = form;
        }

        public string SenderName { get; }
        public string FormName { get; }
    }
}
