﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace SFMTK.Commands
{
    /// <summary>
    /// Represents any command in the program that can be undone or redone, and is used to represent the same actions in different contexts.
    /// </summary>
    public abstract class Command : CommandBase
    {
        // Notes to anyone looking to extend the Command system:
        // - Commands should have a way to be undone and redone, unless ShowInHistory is set to false.
        // - If you wish to execute multiple commands as one, use a CommandGroup.
    }

    /// <summary>
    /// A command which should not be initialized using its default constructor, for example if it accepts parameters.
    /// </summary>
    public abstract class SpecializedCommand : CommandBase
    {

    }

    /// <summary>
    /// Base of all command types. Should only be inherited from if a new type is definitely required.
    /// </summary>
    public abstract class CommandBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandBase"/> class.
        /// </summary>
        protected CommandBase()
        {

        }

        /// <summary>
        /// How the command should be displayed in the Undo History. This description should include as much information about the action itself (how much it changed, etc.).
        /// </summary>
        public virtual string DisplayText => this.Name.Replace("&", "");

        /// <summary>
        /// Gets or sets the general name of the Command. This should be a static name which is to be displayed on menu items and similar.
        /// </summary>
        /// <remarks>Overriding should only be allowed on auto-generated menu items where the NameChanged event is irrelevant.</remarks>
        public virtual string Name
        {
            get => this._name;
            set
            {
                if (this._name == value)
                    return;

                this._name = value;
                this.OnNameChanged(EventArgs.Empty);
            }
        }
        private string _name;

        /// <summary>
        /// Gets the name of the Command as it should be shown in the Command list. This may be different from the Name if that includes context-sensitive content.
        /// </summary>
        public virtual string ListName => this.Name;

        /// <summary>
        /// Gets the type name of this Command.
        /// </summary>
        public string InternalName => this.GetType().Name;

        /// <summary>
        /// Gets the tooltiptext for any menu items associated with this Command.
        /// </summary>
        public virtual string ToolTipText => this.Name.Replace("&", "");

        /// <summary>
        /// Gets or sets an icon for this Command. To be used for any menu items that this Command is displayed on.
        /// </summary>
        public Image Icon
        {
            get => this._icon;
            set
            {
                if (this._icon == value)
                    return;

                this._icon = value;
                this.OnIconChanged(EventArgs.Empty);
            }
        }
        private Image _icon;

        /// <summary>
        /// Gets the category of the Command. Used to categorize inside of the Preferences dialog.
        /// </summary>
        public virtual string Category => "Other";

        /// <summary>
        /// Returns whether, based on the given context, this Command may be executed. If false, corresponding user elements should be disabled.
        /// </summary>
        public virtual bool CanBeExecuted(CommandExecuteContext context) => true;

        /// <summary>
        /// Gets or sets the shortcut assigned to this Command.
        /// </summary>
        public Keys Shortcut
        {
            get => this._shortcut;
            set
            {
                if (this._shortcut == value)
                    return;

                this._shortcut = value;
                this.OnShortcutChanged(EventArgs.Empty);
            }
        }
        private Keys _shortcut;

        /// <summary>
        /// Gets or sets the shortcut in string representation.
        /// </summary>
        public string ShortcutString
        {
            get => CommandManager.StringFromKeys(this.Shortcut);
            set => this.Shortcut = CommandManager.KeysFromString(value);
        }

        /// <summary>
        /// Sets the default shortcut for this Command.
        /// </summary>
        protected void SetDefaultShortcut(Keys shortcut)
        {
            this.Shortcut = shortcut;
            this.DefaultShortcut = shortcut;
        }

        /// <summary>
        /// Whether the shortcut has been changed from the default.
        /// </summary>
        public bool UsesDefaultShortcut => this.Shortcut == this.DefaultShortcut;

        /// <summary>
        /// Gets the default shortcut keys.
        /// </summary>
        public Keys DefaultShortcut { get; private set; }

        /// <summary>
        /// Occurs when shortcut changed.
        /// </summary>
        public event EventHandler ShortcutChanged;

        /// <summary>
        /// Raises the <see cref="E:ShortcutChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnShortcutChanged(EventArgs e) => this.ShortcutChanged?.Invoke(this, e);

        /// <summary>
        /// Occurs when the name changed.
        /// </summary>
        public event EventHandler NameChanged;

        /// <summary>
        /// Raises the <see cref="E:NameChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnNameChanged(EventArgs e) => this.NameChanged?.Invoke(this, e);

        /// <summary>
        /// Occurs when the icon changed.
        /// </summary>
        public event EventHandler IconChanged;

        /// <summary>
        /// Raises the <see cref="E:IconChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnIconChanged(EventArgs e) => this.IconChanged?.Invoke(this, e);

        /// <summary>
        /// Executes (or redoes) the command. This should save any information needed to undo it at any point later on. This should not be called manually, as the command has to be added to the CommandHistory.
        /// <para />Returns null if the action was successful (will add a new entry to the history), otherwise a string containing what went wrong.
        /// </summary>
        public abstract string Execute();

        /// <summary>
        /// Undoes whatever the command did. This should not be executed before executing the command. This should not be called manually, as the command has to be added to the CommandHistory.
        /// <para />Returns null if the undo was successful (will take a step backwards in the history), otherwise a string containing what went wrong.
        /// </summary>
        public virtual string Undo() => throw new NotImplementedException();

        /// <summary>
        /// Whether this command should show in the CommandHistory. If false, will also not be undone/redone.
        /// </summary>
        public virtual bool ShowInHistory => false;

        /// <summary>
        /// The document that this command is executed on.
        /// </summary>
        public DockContent Document { get; set; }

        /// <summary>
        /// Gets whether the shortcut of this command can be edited. Set to false for any command which needs other context in order to be executed (such as drag&amp;drop).
        /// </summary>
        public virtual bool ShortcutEditable => true;

        /// <summary>
        /// Given the remaining argument string, initializes the Command.
        /// </summary>
        /// <param name="args">The remaining arguments, may be null if none specified.</param>
        public virtual CommandBase InitializeCommand(string[] args) => this;

        /// <summary>
        /// Gets the arguments of this command, as they would be specified for InitializeCommand calls. Use null to specify no arguments (empty string is something else!)
        /// </summary>
        public virtual string FormatCommandArgs() => null;

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"{this.Name} ({this.InternalName})";
    }
}
