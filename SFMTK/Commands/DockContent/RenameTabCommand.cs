﻿namespace SFMTK.Commands
{
    public class RenameTabCommand : Command
    {
        public RenameTabCommand() : base()
        {
            this.Name = "&Rename Tab...";
            this.Icon = Properties.Resources.pencil;
        }

        public override string Category => "Tabs";
        public override string ToolTipText => "Renames the selected tab";

        public override string Execute()
        {
            var input = Forms.RenamePrompt.ShowPrompt("Rename Tab", "Please enter the new name for the tab:", this.Document.TabText);
            if (input != null)
            {
                this.Document.Text = input;
                this.Document.TabText = input;
            }
            return null;
        }
    }
}
