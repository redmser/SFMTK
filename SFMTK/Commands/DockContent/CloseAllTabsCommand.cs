﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class CloseAllTabsCommand : Command
    {
        public CloseAllTabsCommand() : base()
        {
            this.Name = "Close &all Tabs";
            this.SetDefaultShortcut(Keys.Control | Keys.Shift | Keys.W);
        }

        public override string Category => "Tabs";
        public override string ToolTipText => "Closes all active tabs";

        public override bool CanBeExecuted(CommandExecuteContext context)
        {
            //Check if a document is selected
            if (Program.MainDP.ActiveDocument == null)
                return false;
            return true;
        }

        public override string Execute()
        {
            //TODO: CloseAllTabsCommand - only close anything in the document space (showing tabs on top etc), not "all dockable things"
            Program.MainDPM.CloseAll();
            return null;
        }
    }
}
