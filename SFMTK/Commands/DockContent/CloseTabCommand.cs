﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class CloseTabCommand : Command
    {
        public CloseTabCommand() : base()
        {
            this.Name = "&Close Tab";
            this.SetDefaultShortcut(Keys.Control | Keys.W);
            this.Icon = Properties.Resources.cancel;
        }

        public override string Category => "Tabs";
        public override string ToolTipText => "Closes the active tab";

        public override bool CanBeExecuted(CommandExecuteContext context)
        {
            //Check if a document is selected
            if (Program.MainDP.ActiveDocument == null)
                return false;
            return true;
        }

        public override string Execute()
        {
            this.Document.Close();
            return null;
        }
    }
}
