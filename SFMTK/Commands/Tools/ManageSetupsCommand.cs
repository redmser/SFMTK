﻿using SFMTK.Forms;

namespace SFMTK.Commands
{
    public class ManageSetupsCommand : Command
    {
        public ManageSetupsCommand() : base()
        {
            this.Name = "&Manage Setups...";
            this.Icon = Properties.Resources.computer_edit;
        }

        public override string Category => "Tools";
        public override string ToolTipText => "Opens an editor for creating new setups or editing existing ones";

        public override string Execute()
        {
            //Open setup manager
            new Setups().Show(Program.MainForm);
            return null;
        }
    }
}
