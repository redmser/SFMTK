﻿using System;

namespace SFMTK.Commands
{
    public class QuickLaunchCommand : Command
    {
        public QuickLaunchCommand() : base()
        {
            this.Name = "&Quick-Launch";
            this.Icon = Properties.Resources.arrow_go;
            this.SetDefaultShortcut(System.Windows.Forms.Keys.F11);
        }

        public override string Category => "Tools";
        public override string ToolTipText
        {
            get
            {
                var setup = Setup.Current?.Name;
                var suffix = setup != null ? $" ({setup})" : "";
                return $"Launches SFM using the currently selected setup{suffix}";
            }
        }

        public override string Execute()
        {
            try
            {
                //Apply and launch
                //NYI: QuickLaunchCommand - show progress for setup apply and launch
                Setup.Current.Apply();
                Setup.Current.Launch();
            }
            catch (Exception ex)
            {
                return $"Could not launch SFM using setup {Setup.Current.Name}: {ex.Message}";
            }
            return null;
        }
    }
}
