﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace SFMTK.Commands
{
    /// <summary>
    /// History of all Commands. Use to undo or redo any Commands.
    /// </summary>
    public class CommandHistory
    {
        /// <summary>
        /// Creates a new CommandHistory with the capacity taken from the current config value.
        /// </summary>
        public CommandHistory()
        {
            this.DataSource = new CommandHistoryDataSource(this);
            CommandHistories.Add(this);
        }

        /// <summary>
        /// Capacity of the CommandHistory.
        /// </summary>
        private int MaxSize => Properties.Settings.Default.UndoSteps;

        /// <summary>
        /// List of all executed Commands. First entry is the oldest, new ones get added to the end.
        /// </summary>
        public List<CommandBase> Commands { get; } = new List<CommandBase>();

        /// <summary>
        /// Last executed/undone Command index in the list.
        /// </summary>
        private int CommandIndex { get; set; } = -1;

        /// <summary>
        /// The last executed Command. Would be the next undone Command. Returns null if the Command history is empty.
        /// </summary>
        public CommandBase UndoCommand => this.CommandIndex < 0 ? null : this.Commands[this.CommandIndex];

        /// <summary>
        /// The last undone Command. Would be the next redone Command. Returns null if the Command history is empty.
        /// </summary>
        public CommandBase RedoCommand => this.CommandIndex < -1 ? null : this.Commands[this.CommandIndex + 1];

        /// <summary>
        /// Executes the specified Command, adding it to the history if the command has this flag set.
        /// <para />May open a message box if the command did not execute correctly.
        /// </summary>
        /// <exception cref="ArgumentNullException">Tried to execute a null command.</exception>
        /// <param name="command"></param>
        public void Execute(CommandBase command)
        {
            var err = ExecuteDirectly(command);
            if (err != null)
            {
                ShowCommandErrors(command, err, CommandErrorsVerb.Execute);
            }
        }

        /// <summary>
        /// Executes the specified Command, adding it to the history if the command has this flag set.
        /// <para />Returns null if the specified command executed successfully, otherwise a string to display as an error message.
        /// </summary>
        /// <param name="command"></param>
        /// <exception cref="ArgumentNullException">Tried to execute a null command.</exception>
        public string ExecuteDirectly(CommandBase command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command), "Tried to execute a null command.");

            if (!command.ShowInHistory)
            {
                //Ignore CommandHistory steps - simply execute
                return command.Execute();
            }

            //Remove all history entries between CommandIndex (excluding since that was not undone) and the last entry, since you can't have two "undo branches"
            if (this.Commands.Count > 0)
                this.Commands.RemoveRange(this.CommandIndex + 1, this.Commands.Count - this.CommandIndex - 1);

            //Execute Command
            var err = command.Execute();
            if (err == null)
            {
                //Valid execution context
                this.Commands.Add(command);
                this.UpdateBounds();
                this.CommandIndex = this.Commands.Count - 1;
                this.CheckCanUndoRedoChanged();
            }
            return err;
        }

        /// <summary>
        /// Moves forwards through the Command history, redoing the next entry.
        /// <para />May open a message box if the command did not execute correctly.
        /// </summary>
        public void Redo()
        {
            var err = RedoDirectly();
            if (err != null)
            {
                ShowCommandErrors(this.RedoCommand, err, CommandErrorsVerb.Redo);
            }
        }

        /// <summary>
        /// Moves forwards through the Command history, redoing the next entry.
        /// <para />Returns null if the redo was successful, otherwise a string to display as an error message.
        /// </summary>
        public string RedoDirectly()
        {
            if (!this.CanRedo)
                return null;

            //Needs a command to redo
            if (this.RedoCommand == null)
            {
                this.CheckCanUndoRedoChanged(false);
                return null;
            }

            //Move forward
            var err = this.RedoCommand.Execute();
            if (err == null)
            {
                this.CommandIndex++;
                this.CheckCanUndoRedoChanged();
            }
            return err;
        }

        /// <summary>
        /// Moves backwards through the Command history, undoing the last entry.
        /// <para />May open a message box if the command did not execute correctly.
        /// </summary>
        public void Undo()
        {
            var err = UndoDirectly();
            if (err != null)
            {
                ShowCommandErrors(this.UndoCommand, err, CommandErrorsVerb.Undo);
            }
        }

        /// <summary>
        /// Moves backwards through the Command history, undoing the last entry.
        /// <para />Returns null if the undo was successful, otherwise a string to display as an error message.
        /// </summary>
        public string UndoDirectly()
        {
            if (!this.CanUndo)
                return null;

            //Needs a command to undo
            if (this.UndoCommand == null)
            {
                this.CheckCanUndoRedoChanged(false);
                return null;
            }

            var err = this.UndoCommand.Undo();
            if (err == null)
            {
                //Move back
                this.CommandIndex--;
                this.CheckCanUndoRedoChanged();
            }
            return err;
        }

        /// <summary>
        /// Adds the specified Command to the history without executing it.
        /// <para/>Returns whether the command was added to the history.
        /// </summary>
        /// <param name="cmd">The command to add.</param>
        public bool AddCommand(Command command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command), "Tried to execute a null command.");

            if (!command.ShowInHistory)
                return false;

            this.Commands.Add(command);
            this.UpdateBounds();
            this.CommandIndex = this.Commands.Count - 1;
            this.CheckCanUndoRedoChanged();
            return true;
        }

        /// <summary>
        /// Displays an error message about invalid command execution, undo or redo. The message string should be gotten from one of the "...Directly" calls.
        /// </summary>
        /// <param name="cmd">The command which failed to execute.</param>
        /// <param name="message">Error message to display. Should be gotten from one of the "...Directly" calls.</param>
        /// <param name="verb">Which verb/context to use in the message.</param>
        private static void ShowCommandErrors(CommandBase cmd, string message, CommandErrorsVerb verb)
        {
            var verbstr = Enum.GetName(typeof(CommandErrorsVerb), verb).ToLowerInvariant();
            FallbackTaskDialog.ShowDialogLog($"One or more errors occurred trying to {verbstr} the command \"{cmd?.DisplayText ?? "invalid command"}\".",
                null, $"{verbstr.FirstLetterToUpperCase()} error", message, FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));
        }

        /// <summary>
        /// Which verb to use for the command error.
        /// </summary>
        private enum CommandErrorsVerb
        {
            Undo,
            Redo,
            Execute
        }

        private void CheckCanUndoRedoChanged(bool historyChanged = true)
        {
            if (this.CanRedo != this._lastCanRedo || this.CanUndo != this._lastCanUndo)
            {
                //UPDATE!
                this._lastCanRedo = this.CanRedo;
                this._lastCanUndo = this.CanUndo;
                this.OnCanUndoRedoChanged(EventArgs.Empty);
            }

            //Potentially changed history after these checks
            if (historyChanged)
                this.OnCommandHistoryChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Whether an Command in the history can be undone.
        /// </summary>
        public bool CanUndo => this.CommandIndex > -1;

        /// <summary>
        /// Whether an Command in the history can be redone.
        /// </summary>
        public bool CanRedo => this.CommandIndex < this.Commands.Count - 1;

        private bool _lastCanUndo = false;
        private bool _lastCanRedo = false;

        /// <summary>
        /// The string to display for an undo control. Contains the command's display text for undoing it.
        /// </summary>
        public string UndoDisplayText => this.CanUndo ? $"Undo ({this.Commands[this.CommandIndex].DisplayText})" : "Undo";

        /// <summary>
        /// The string to display for a redo control. Contains the command's display text for redoing it.
        /// </summary>
        public string RedoDisplayText => this.CanRedo ? $"Redo ({this.Commands[this.CommandIndex + 1].DisplayText})" : "Redo";

        /// <summary>
        /// Invoked when CanUndo or CanRedo changed.
        /// </summary>
        public event EventHandler CanUndoRedoChanged;

        /// <summary>
        /// Raises the <see cref="E:CanUndoRedoChanged" /> event.
        /// </summary>
        protected virtual void OnCanUndoRedoChanged(EventArgs e) => this.CanUndoRedoChanged?.Invoke(this, e);

        /// <summary>
        /// Invoked when a command was executed or undone which is part of the CommandHistory.
        /// </summary>
        public event EventHandler CommandHistoryChanged;

        /// <summary>
        /// Raises the <see cref="E:CommandHistoryChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnCommandHistoryChanged(EventArgs e) => this.CommandHistoryChanged?.Invoke(this, e);

        /// <summary>
        /// A usable data source representing this command history.
        /// </summary>
        public CommandHistoryDataSource DataSource { get; }

        /// <summary>
        /// Limits the length of the command history using the config value.
        /// </summary>
        public void UpdateBounds() => UpdateBounds(this.MaxSize);

        /// <summary>
        /// Limits the length of the command history to the specified amount.
        /// </summary>
        /// <param name="amount"></param>
        public void UpdateBounds(int amount)
        {
            if (amount < 0)
                throw new ArgumentOutOfRangeException(nameof(amount), amount, "Bounds have to be > 0");

            while (this.Commands.Count > amount)
            {
                this.Commands.RemoveAt(0);
                this.CommandIndex--;
            }
        }

        /// <summary>
        /// Limits the length of all command histories using the config value.
        /// </summary>
        public static void UpdateAllBounds()
        {
            foreach (var ch in CommandHistories)
            {
                ch.UpdateBounds();
            }
        }

        /// <summary>
        /// List of command history instances. Gets registered when created.
        /// </summary>
        public static List<CommandHistory> CommandHistories { get; } = new List<CommandHistory>();
    }

    /// <summary>
    /// A data source to use for a virtual list displaying command history.
    /// </summary>
    public class CommandHistoryDataSource : IVirtualListDataSource
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandHistoryDataSource"/> class.
        /// </summary>
        /// <param name="history">The history.</param>
        public CommandHistoryDataSource(CommandHistory history)
        {
            this._history = history;
        }

        private CommandHistory _history;

        public void AddObjects(ICollection modelObjects) => ThrowModifyException("added to");
        public object GetNthObject(int n) => this._history.Commands[n];
        public int GetObjectCount() => this._history.Commands.Count;
        public int GetObjectIndex(object model) => this._history.Commands.IndexOf((Command)model);
        public void InsertObjects(int index, ICollection modelObjects) => ThrowModifyException("inserted to");
        public void PrepareCache(int first, int last) { }
        public void RemoveObjects(ICollection modelObjects) => ThrowModifyException("removed from");
        public int SearchText(string value, int first, int last, OLVColumn column)
        {

            for (var i = first; i <= last; i++)
            {
                var cmd = (Command)this.GetNthObject(i);
                if (cmd.DisplayText.ContainsIgnoreCase(value))
                    return i;
            }
            return -1;
        }
        public void SetObjects(IEnumerable collection) => ThrowModifyException("replaced");
        public void Sort(OLVColumn column, SortOrder order) { }
        public void UpdateObject(int index, object modelObject) => ThrowModifyException("updated");

        /// <summary>
        /// Throws a modify history exception.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <exception cref="InvalidOperationException"></exception>
        private static void ThrowModifyException(string action) => throw new InvalidOperationException($"Command History may not be {action} from its data source.");
    }
}
