﻿using System.Linq;
using System.Windows.Forms;
using SFMTK.Contents;
using SFMTK.Forms;

namespace SFMTK.Commands
{
    public class OpenFileCommand : Command
    {
        public OpenFileCommand() : base()
        {
            this.SetDefaultShortcut(Keys.Control | Keys.O);
            this.Name = "&Open...";
            this.Icon = Properties.Resources.folder;
        }

        public override string Category => "File";
        public override string ToolTipText => "Open content for editing or analysis";

        public override string Execute()
        {
            if (Properties.Settings.Default.NativeFileBrowser)
            {
                //Native file browser
                var res = IOHelper.OpenFileDialog(true, true);
                if (res != null)
                {
                    //Open selected file(s)
                    Content.OpenFile(res.ToList(), null);
                }
            }
            else
            {
                //Open custom content browser window
                var browser = new ContentBrowserWindow(true, false);
                var res = browser.ShowDialog(Program.MainForm);
                if (res == DialogResult.OK)
                {
                    //Open selected file(s)
                    foreach (var file in browser.SelectedFiles)
                    {
                        if (file.FileType == null)
                            throw new System.ArgumentException("Could not determine the corresponding content type for the selected file.");

                    //FIXME: Main - use the collection OpenFile call in order to only have single error messages
                    //Also... check TODO inside the openfileinternal method, for vvvvvvvvvvvvvvvvv
                    Content.OpenFile(file.NodePath, Content.ContentByType(file.FileType).GetType());
                    }
                }
            }
            return null;
        }
    }
}
