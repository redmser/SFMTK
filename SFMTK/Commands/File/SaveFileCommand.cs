﻿using System.Windows.Forms;
using SFMTK.Contents;

namespace SFMTK.Commands
{
    public class SaveFileCommand : Command
    {
        //TODO: SaveFileCommand - implement a save operation with the content browser

        public SaveFileCommand() : base()
        {
            this.SetDefaultShortcut(Keys.Control | Keys.S);
            this.Name = "&Save";
            this.Icon = Properties.Resources.disk;
        }

        public override string Category => "File";
        public override string ToolTipText => "Save the currently active content";

        public override bool CanBeExecuted(CommandExecuteContext context)
        {
            if (context.FormName == nameof(Forms.Main))
            {
                //In main form, check for selected content
                var active = Program.MainDPM.ActiveContent;
                return active?.Content != null;
            }
            return false;
        }

        public override string Execute()
        {
            //Save active content
            var active = Program.MainDPM.ActiveContent;
            var content = active?.Content;
            if (content != null)
                Content.SaveFile(content);
            return null;
        }
    }
}
