﻿using System.Windows.Forms;
using SFMTK.Contents;

namespace SFMTK.Commands
{
    public class SaveFileAsCommand : Command
    {
        public SaveFileAsCommand() : base()
        {
            this.SetDefaultShortcut(Keys.Control | Keys.Shift | Keys.S);
            this.Name = "Save &as...";
        }

        public override string Category => "File";
        public override string ToolTipText => "Save the currently active content to a new file or format";

        public override bool CanBeExecuted(CommandExecuteContext context)
        {
            if (context.FormName == nameof(Forms.Main))
            {
                //In main form, check for selected content
                var active = Program.MainDPM.ActiveContent;
                return active != null;
            }
            return false;
        }

        public override string Execute()
        {
            //Save active content as
            var active = Program.MainDPM.ActiveContent;
            var content = active?.Content;
            if (content != null)
                Content.SaveFileAs(content);
            return null;
        }
    }
}
