﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Contents;
using SFMTK.Properties;
using SFMTK.Widgets;

namespace SFMTK.Commands
{
    public class SaveAllFilesCommand : Command
    {
        public SaveAllFilesCommand() : base()
        {
            this.SetDefaultShortcut(Keys.Control | Keys.Alt | Keys.S);
            this.Name = "Sav&e all";
            this.Icon = Resources.disk_multiple;
        }

        public override string Category => "File";
        public override string ToolTipText => "Save all open content";

        public override bool CanBeExecuted(CommandExecuteContext context)
        {
            if (context.FormName == nameof(Forms.Main))
            {
                //In main form, check for any open content
                return Program.MainDPM.DockContentsTyped<ContentInfoWidget>().Any();
            }
            return false;
        }

        public override string Execute()
        {
            //Save all content
            var failed = new List<string>();
            foreach (var content in Program.MainDPM.DockContentsTyped<ContentInfoWidget>().Select(i => i.Content))
            {
                var res = Content.SaveFile(content, false);
                if (!res)
                {
                    //Add to error list
                    failed.Add(content.GetFormattedName(Settings.Default.FullTitleFormat, false));
                }
            }

            if (failed.Count > 0)
            {
                //UNTESTED: SaveAllFilesCommand - Show combined error
                Content.ShowSaveError("The following content could not be saved:\n\n" + string.Join("\n", failed));
            }
            return null;
        }
    }
}
