﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class CopyFullPathCommand : Command
    {
        public CopyFullPathCommand() : base()
        {
            this.Name = "C&opy Full Path";
        }

        public override string Category => "File";
        public override string ToolTipText => "Copies the full path to the Content into the Clipboard";

        public override bool CanBeExecuted(CommandExecuteContext context)
        {
            if (context.FormName == nameof(Forms.Main))
            {
                //In main form, check for selected content
                var active = Program.MainDPM.ActiveContent;
                return active?.Content != null && !active.Content.FakeFile;
            }
            return false;
        }

        public override string Execute()
        {
            var active = Program.MainDPM.ActiveContent;
            Clipboard.SetText(active.Content.FilePath);
            return null;
        }
    }
}
