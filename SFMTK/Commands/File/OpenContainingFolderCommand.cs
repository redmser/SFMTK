﻿namespace SFMTK.Commands
{
    public class OpenContainingFolderCommand : Command
    {
        public OpenContainingFolderCommand() : base()
        {
            this.Name = "&Open Containing Folder";
        }

        public override string Category => "File";
        public override string ToolTipText => "Opens the folder that this file lies in using the Explorer";

        public override bool CanBeExecuted(CommandExecuteContext context)
        {
            if (context.FormName == nameof(Forms.Main))
            {
                //In main form, check for selected content
                var active = Program.MainDPM.ActiveContent;
                return active?.Content != null && !active.Content.FakeFile;
            }
            return false;
        }

        public override string Execute()
        {
            var active = Program.MainDPM.ActiveContent;
            IOHelper.OpenInExplorer(active.Content.FilePath, false);
            return null;
        }
    }
}
