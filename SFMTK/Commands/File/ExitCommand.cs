﻿namespace SFMTK.Commands
{
    public class ExitCommand : Command
    {
        public ExitCommand() : base()
        {
            this.Name = "E&xit";
        }

        public override string Category => "File";
        public override string ToolTipText => "Exit the program";

        public override string Execute()
        {
            //Close the SFMTK window
            Program.MainForm.Close();
            return null;
        }
    }
}
