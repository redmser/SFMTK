﻿using System.Windows.Forms;
using SFMTK.Forms;
using System;
using SFMTK.Contents;

namespace SFMTK.Commands
{
    public class NewFileCommand : Command
    {
        public NewFileCommand() : base()
        {
            this.SetDefaultShortcut(Keys.Control | Keys.N);
            this.Name = "&New...";
            this.Icon = Properties.Resources.page;
        }

        public override string Category => "File";
        public override string ToolTipText => "Create new content, either from existing files or from scratch";

        public override string Execute()
        {
            //Show new content dialog
            var ncdiag = new NewContent();
            var res = ncdiag.ShowDialog(Program.MainForm);
            if (res == DialogResult.OK)
            {
                //Add the new content
                var nc = (Content)Activator.CreateInstance(ncdiag.ContentType);
                nc.FakeFile = true;
                ncdiag.ApplySettings(nc);
                Program.MainDPM.AddContent(nc);
            }
            return null;
        }
    }
}
