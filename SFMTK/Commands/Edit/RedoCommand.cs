﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class RedoCommand : Command
    {
        public RedoCommand() : base()
        {
            this.Name = "&Redo";
            this.Icon = Properties.Resources.arrow_redo;
            this.SetDefaultShortcut(Keys.Control | Keys.Shift | Keys.Z);
        }

        public override string Category => "Edit";
        public override string ToolTipText => "Redoes the last undone change";

        public override bool CanBeExecuted(CommandExecuteContext context) => Program.MainCH.CanRedo;

        public override string Execute()
        {
            //Let the CommandHistory call an redo
            Program.MainCH.Redo();
            return null;
        }
    }
}
