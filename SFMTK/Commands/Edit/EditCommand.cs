﻿namespace SFMTK.Commands
{
    /// <summary>
    /// A Command which can write or read the underlying data using the IEditableData interface.
    /// </summary>
    public abstract class EditCommand : Command
    {
        /// <summary>
        /// Gets whether this Command can be executed based on the state of the EditableData.
        /// </summary>
        protected abstract bool CanExecuteBasedOnData { get; }

        /// <summary>
        /// Gets which Command to execute through this Command.
        /// </summary>
        protected abstract Command UnderlyingCommand { get; }

        /// <summary>
        /// Returns whether, based on the given context, this Command may be executed. If false, corresponding user elements should be disabled.
        /// </summary>
        /// <param name="context"></param>
        public override bool CanBeExecuted(CommandExecuteContext context)
        {
            if (context.FormName == nameof(Forms.Main))
            {
                //Check for active dockcontent being editable
                if (Program.MainDP.ActiveDocument is IEditableData editable)
                    return this.CanExecuteBasedOnData;
            }
            return false;
        }

        /// <summary>
        /// Gets the editable data that is to be modified by this command.
        /// </summary>
        public IEditableData EditableData => Program.MainDP.ActiveDocument as IEditableData;

        public override string Category => "Edit";

        public override bool ShowInHistory => true;
        public override string DisplayText => this.UnderlyingCommand.DisplayText;
        public override string Execute() => this.UnderlyingCommand.Execute();
        public override string Undo() => this.UnderlyingCommand.Undo();
    }
}
