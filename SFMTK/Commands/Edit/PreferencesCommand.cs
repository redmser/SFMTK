﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class PreferencesCommand : Command
    {
        public PreferencesCommand() : base()
        {
            this.Name = "&Preferences...";
            this.Icon = Properties.Resources.cog;
            this.SetDefaultShortcut(Keys.F10);
        }

        public override string Category => "Edit";
        public override string ToolTipText => "Opens the application preferences";

        public override string Execute()
        {
            //Open settings dialog
            Program.MainForm.OpenSettings();
            return null;
        }
    }
}
