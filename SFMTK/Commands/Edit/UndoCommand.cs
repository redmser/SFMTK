﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class UndoCommand : Command
    {
        public UndoCommand() : base()
        {
            this.Name = "&Undo";
            this.Icon = Properties.Resources.arrow_undo;
            this.SetDefaultShortcut(Keys.Control | Keys.Z);
        }

        public override string Category => "Edit";
        public override string ToolTipText => "Undoes the last change";

        //TODO: Undo/RedoCommand - execute for the commandhistory this command is called from
        public override bool CanBeExecuted(CommandExecuteContext context) => Program.MainCH.CanUndo;

        public override string Execute()
        {
            //Let the CommandHistory call an undo
            Program.MainCH.Undo();
            return null;
        }
    }
}
