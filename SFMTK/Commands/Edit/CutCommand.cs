﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class CutCommand : EditCommand
    {
        public CutCommand() : base()
        {
            this.Name = "Cu&t";
            this.Icon = Properties.Resources.cut;
            this.SetDefaultShortcut(Keys.Control | Keys.X);
        }

        public override string ToolTipText => "Cuts the selected element(s)";
        protected override bool CanExecuteBasedOnData => this.EditableData.CanDelete && this.EditableData.CanCopy;
        protected override Command UnderlyingCommand => this.EditableData.CutCommand;
    }
}
