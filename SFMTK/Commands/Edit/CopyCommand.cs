﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class CopyCommand : EditCommand
    {
        public CopyCommand() : base()
        {
            this.Name = "&Copy";
            this.Icon = Properties.Resources.page_copy;
            this.SetDefaultShortcut(Keys.Control | Keys.C);
        }

        public override string ToolTipText => "Copies the selected element(s)";
        protected override bool CanExecuteBasedOnData => this.EditableData.CanCopy;
        protected override Command UnderlyingCommand => this.EditableData.CopyCommand;
    }
}
