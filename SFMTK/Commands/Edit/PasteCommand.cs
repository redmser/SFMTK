﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class PasteCommand : EditCommand
    {
        public PasteCommand() : base()
        {
            this.Name = "&Paste";
            this.Icon = Properties.Resources.page_white_paste;
            this.SetDefaultShortcut(Keys.Control | Keys.V);
        }

        public override string ToolTipText => "Pastes element(s) from the clipboard";
        protected override bool CanExecuteBasedOnData => this.EditableData.CanPaste;
        protected override Command UnderlyingCommand => this.EditableData.PasteCommand;
    }
}
