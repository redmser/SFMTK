﻿using System.Windows.Forms;

namespace SFMTK.Commands
{
    public class DeleteCommand : EditCommand
    {
        public DeleteCommand() : base()
        {
            this.Name = "&Delete";
            this.Icon = Properties.Resources.cross;
            this.SetDefaultShortcut(Keys.Delete);
        }

        public override string ToolTipText => "Deletes the selected element(s)";
        protected override bool CanExecuteBasedOnData => this.EditableData.CanDelete;
        protected override Command UnderlyingCommand => this.EditableData.DeleteCommand;
    }
}
