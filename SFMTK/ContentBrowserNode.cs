﻿using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using BrightIdeasSoftware;
using SFMTK.Controls;
using SFMTK.Contents;
using System;

namespace SFMTK
{
    /// <summary>
    /// A tree view node inside of the content browser.
    /// </summary>
    public class ContentBrowserNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentBrowserNode" /> class serving as a content file with the specified path.
        /// </summary>
        /// <param name="path">The standardized local path to the content file. Should be relative to the parent directory.</param>
        /// <param name="parent">The parent of this node.</param>
        /// <param name="type">The TypeName of content. Use null to make it autodetect the type.</param>
        public ContentBrowserNode(string path, ContentBrowserNode parent, string type)
        {
            this.Parent = parent;
            this.FileTypeName = type;
            //IMP: ContentBrowserNode - is the Undetected type used inconsistently? Only files can be undetected!!!
            this.NodeType = type == null ? ContentBrowserNodeType.Undetected : ContentBrowserNodeType.File;
            this.Name = path;
            //FIXME: ContentBrowserNode - since memory does not seem to be a problem, try to cache the (local)nodepath and mod indices, to speed up initialization here
            this.ModName = this.Parent?.ModName ?? this.LocalNodePath.Substring(0, this.LocalNodePath.IndexOf('/'));
            this.ModIndex = SFM.GameInfo.Mods.IndexOf(SFM.GameInfo.GetMod(this.ModName));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentBrowserNode" /> class serving as a directory with the specified path.
        /// </summary>
        /// <param name="path">The standardized local path of the directory. Should be relative to the parent directory.</param>
        /// <param name="parent">The parent of this node.</param>
        /// <param name="isDirName">If set to <c>true</c>, the path parameter is interpreted as the name of the last directory. Used for deserializing.</param>
        public ContentBrowserNode(string path, ContentBrowserNode parent, bool isDirName)
        {
            this.Parent = parent;
            this.NodeType = ContentBrowserNodeType.Directory;
            this.Children = new HashSet<ContentBrowserNode>(NodeComparer);
            if (this.Parent == null)
            {
                //Game folder should not have a mod
                this.Name = "game";
                this.ModName = null;
                this.ModIndex = -1;
            }
            else
            {
                //Regular mod gettage
                this.Name = isDirName ? path : GetLastDirName(path);
                this.ModName = this.Parent?.ModName ?? this.LocalNodePath.Substring(0, this.LocalNodePath.IndexOf('/'));
                this.ModIndex = SFM.GameInfo.Mods.IndexOf(SFM.GameInfo.GetMod(this.ModName));
            }
        }

        /// <summary>
        /// Returns the name of the last directory in the tree, if the format is standardized (including the trailing slash!).
        /// </summary>
        /// <param name="dir">The standardized absolute path of the directory.</param>
        private static string GetLastDirName(string dir)
        {
            var i = dir.LastIndexOf('/', dir.Length - 2) + 1;
            return dir.Substring(i, dir.Length - i - 1);
        }

        /// <summary>
        /// Gets the list of ContentBrowserNode children inside of this folder, or null if the node is a file.
        /// </summary>
        public HashSet<ContentBrowserNode> Children { get; set; }

        /// <summary>
        /// Gets the parent of this node.
        /// </summary>
        public ContentBrowserNode Parent { get; set;  }

        /// <summary>
        /// Gets the image to display next to this node.
        /// </summary>
        public Bitmap GetImage()
        {
            switch (this.NodeType)
            {
                case ContentBrowserNodeType.Directory:
                    return Properties.Resources.folder;
                case ContentBrowserNodeType.File:
                    //"Other" files
                    if (this.FileType == null)
                        return new Bitmap(Properties.Resources.question);
                    return Content.ContentByType(this.FileType).SmallImage;
                default:
                    return Properties.Resources.cross;
            }
        }

        /// <summary>
        /// Whether this node can be expanded.
        /// </summary>
        public bool CanExpand => this.Children?.Any() ?? false;

        /// <summary>
        /// Gets the full path to this node (file or folder).
        /// </summary>
        public string NodePath => string.Concat(SFM.GamePath, this.LocalNodePath);

        /// <summary>
        /// Gets the local path to this node (file or folder), relative to the game directory.
        /// </summary>
        public string LocalNodePath => this.Parent == null ? "" :
            string.Concat(this.Parent.LocalNodePath, this.Name, this.NodeType == ContentBrowserNodeType.Directory ? "/" : "");

        /// <summary>
        /// Gets the file/folder name of this node.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the name of the mod that this node is located in.
        /// </summary>
        public string ModName { get; set; }

        /// <summary>
        /// Gets the <see cref="Data.Mod"/> associated with this node.
        /// </summary>
        public Data.Mod Mod => SFM.GameInfo.GetMod(this.ModName);

        /// <summary>
        /// Gets the index of the mod in the loading order.
        /// </summary>
        public int ModIndex { get; set; }

        /// <summary>
        /// Gets or sets the Type of the content of this node (may be null if unknown).
        /// </summary>
        public Type FileType
        {
            get
            {
                if (!this._fileTypeInvalid && this._fileType == null)
                {
                    this._fileType = Reflection.GetType(this.FileTypeName, false);
                    if (this._fileType == null)
                        this._fileTypeInvalid = true;
                }
                return this._fileType;
            }
            set
            {
                this._fileTypeInvalid = false;
                this._fileType = value;
                this.FileTypeName = value?.FullName;
            }
        }
        private Type _fileType;
        private bool _fileTypeInvalid;

        /// <summary>
        /// Gets or sets the full name of the FileType.
        /// </summary>
        public string FileTypeName { get; set; }

        /// <summary>
        /// Gets or sets the file type as a display text (may be null if not yet detected).
        /// </summary>
        public string DisplayType { get; set; }

        /// <summary>
        /// Gets or sets whether this node is visible in the final filtering step.
        /// </summary>
        internal bool Visible { get; set; }

        /// <summary>
        /// Updates the filtering state of this node.
        /// </summary>
        internal void UpdateFilter(FilterControlSettings fs)
        {
            //BUG: ContentBrowserNode - if entering a total garbage path filter AND having a type filter, certain files are ALWAYS shown
            //  -> is this related to the "dynamic loading code" of the nodes? will they not show if searching after finished loading?

            if (fs.HasPathFilter)
            {
                //Path filter
                var pathvalid = FilterControl.StringMatches(this.LocalNodePath, fs.PathFilter, fs.PathRegex);
                if (pathvalid)
                {
                    TreeVisible(this);
                    return;
                }
            }

            if (fs.HasTypeFilter)
            {
                //Type filter
                var typesvalid = fs.TypeFilter.Any(t => t == this.FileType);
                if (typesvalid)
                {
                    TreeVisible(this);
                    return;
                }
            }

            //No filter succeeded
            this.Visible = false;



            void TreeVisible(ContentBrowserNode node)
            {
                if (node == null)
                    return;

                node.Visible = true;
                TreeVisible(node.Parent);
            }
        }

        /// <summary>
        /// Whether this node is a folder or a file.
        /// </summary>
        public ContentBrowserNodeType NodeType { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"{{{this.NodeType} {this.LocalNodePath}}}";

        /// <summary>
        /// Gets a content instance for this node. Is cached after a first retrieval.
        /// </summary>
        public Content Content
        {
            get
            {
                if (this._content == null)
                {
                    this._content = (Content)Activator.CreateInstance(this.FileType);
                    if (this._content != null)
                        this._content.FilePath = this.NodePath;
                }
                return this._content;
            }
        }
        private Content _content;

        /// <summary>
        /// Gets whether the Content property has Content loaded.
        /// </summary>
        public bool HasContent => this._content != null;

        /// <summary>
        /// Gets the default equality comparer for two <see cref="ContentBrowserNode"/> instances.
        /// </summary>
        public static IEqualityComparer<ContentBrowserNode> NodeComparer { get; } = new ContentBrowserNodeComparer();

        /// <summary>
        /// Compares two ContentBrowserNodes by checking their paths.
        /// </summary>
        /// <seealso cref="System.Collections.Generic.IEqualityComparer{SFMTK.ContentBrowserNode}" />
        private class ContentBrowserNodeComparer : IEqualityComparer<ContentBrowserNode>
        {
            private static IEqualityComparer<string> _c = EqualityComparer<string>.Default;

            public bool Equals(ContentBrowserNode l, ContentBrowserNode r) => _c.Equals(l.NodePath, r.NodePath);
            
            public int GetHashCode(ContentBrowserNode rule) => _c.GetHashCode(rule.NodePath);
        }
    }

    /// <summary>
    /// The type of the node.
    /// </summary>
    public enum ContentBrowserNodeType
    {
        /// <summary>
        /// Not yet detected, FileType is set to <c>null</c>.
        /// </summary>
        Undetected,
        /// <summary>
        /// Node is a file/content, FileType is a Type of a Content, or <c>null</c> if misc file.
        /// </summary>
        File,
        /// <summary>
        /// Node is directory, FileType is set to <c>null</c>.
        /// </summary>
        Directory
    }

    /// <summary>
    /// A filter for ContentBrowserNode instances. Call the UpdateFilter method on all ContentBrowserNodes whenever filtering.
    /// </summary>
    public class ContentBrowserNodeFilter : IModelFilter
    {
        /// <summary>
        /// Should the given model be included when this filter is installed
        /// </summary>
        /// <param name="modelObject">The model object to consider</param>
        /// <returns>
        /// Returns true if the model will be included by the filter
        /// </returns>
        public bool Filter(object modelObject)
        {
            if (modelObject is ContentBrowserNode node)
                return node.Visible;

            return false;
        }
    }
}
