﻿using System;

namespace SFMTK
{
    /// <summary>
    /// Wraps a <see cref="ulong"/> to allow for easily displaying the value as a file size.
    /// </summary>
    [Serializable]
    public struct FileSize : IComparable, IEquatable<FileSize>
    {
        /// <summary>
        /// Creates a new FileSize with the specified size, in bytes.
        /// </summary>
        public FileSize(ulong size)
        {
            this.Size = size;
        }

        private const long KiloByteFactor = 1000;
        private const long MegaByteFactor = KiloByteFactor * 1000;
        private const long GigaByteFactor = MegaByteFactor * 1000;
        private const long TeraByteFactor = GigaByteFactor * 1000;

        /// <summary>
        /// Size in bytes.
        /// </summary>
        public readonly ulong Size;

        private static readonly FileSize _zero = new FileSize(0);

        /// <summary>
        /// Returns a FileSize of Zero.
        /// </summary>
        public static FileSize Zero
        {
            get { return FileSize._zero; }
        }

        /// <summary>
        /// Parses the specified string representation of a FileSize.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static FileSize Parse(string value)
        {
            //Remove whitespace
            value = value.ToLowerInvariant().Replace(" ", "").Replace("\t", "").Replace("bytes", "byte").Replace(",", ".");

            //Try if it's a standalone bytes number
            ulong onlyval;
            if (ulong.TryParse(value, out onlyval))
                return new FileSize(onlyval);

            //Try with units
            if (value.EndsWith("byte"))
                return new FileSize(ulong.Parse(value.Substring(0, value.Length - 4)));

            if (value.EndsWith("kb"))
                return new FileSize((ulong)Math.Round(double.Parse(value.Substring(0, value.Length - 2)) * FileSize.KiloByteFactor));

            if (value.EndsWith("mb"))
                return new FileSize((ulong)Math.Round(double.Parse(value.Substring(0, value.Length - 2)) * FileSize.MegaByteFactor));

            if (value.EndsWith("gb"))
                return new FileSize((ulong)Math.Round(double.Parse(value.Substring(0, value.Length - 2)) * FileSize.GigaByteFactor));

            if (value.EndsWith("tb"))
                return new FileSize((ulong)Math.Round(double.Parse(value.Substring(0, value.Length - 2)) * FileSize.TeraByteFactor));

            //Could not figure out format
            throw new FormatException(string.Format("Could not create FileSize object from string \"{0}\".", value));
        }

        /// <summary>
        /// Parses the specified value, returning whether it was successful.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool TryParse(string value, out FileSize result)
        {
            try
            {
                result = FileSize.Parse(value);
                return true;
            }
            catch
            {
                result = FileSize.Zero;
                return false;
            }
        }

        /// <summary>
        /// Whether parsing would work with the specified value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool CouldParse(string value)
        {
            try
            {
                FileSize.Parse(value);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> containing a fully qualified type name.
        /// </returns>
        public override string ToString()
        {
            if (this.Size == 1)
                return "1 byte";

            var val = (double)this.Size;
            var unit = "bytes";

            //neat table
            if (val >= 1000)
            {
                val /= 1000;
                unit = "KB";

                if (val >= 1000)
                {
                    val /= 1000;
                    unit = "MB";

                    if (val >= 1000)
                    {
                        val /= 1000;
                        unit = "GB";

                        if (val >= 1000)
                        {
                            val /= 1000;
                            unit = "TB";
                        }
                    }
                }
            }

            return string.Format("{0} {1}", Math.Round(val, 2), unit);
        }

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance precedes, follows, or occurs in the same position in the sort order as the other object.
        /// </summary>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance precedes <paramref name="obj"/> in the sort order. Zero This instance occurs in the same position in the sort order as <paramref name="obj"/>. Greater than zero This instance follows <paramref name="obj"/> in the sort order. 
        /// </returns>
        /// <param name="obj">An object to compare with this instance. </param><exception cref="T:System.ArgumentException"><paramref name="obj"/> is not the same type as this instance. </exception>
        public int CompareTo(object obj)
        {
            if (obj is FileSize)
                return this.Size.CompareTo(((FileSize)obj).Size);

            if (obj is ulong)
                return this.Size.CompareTo(obj);

            if (obj is string)
                return this.Size.CompareTo(ulong.Parse((string)obj));

            return this.Size.CompareTo((ulong)obj);
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(FileSize other)
        {
            return this.Size == other.Size;
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <returns>
        /// true if <paramref name="obj"/> and this instance are the same type and represent the same value; otherwise, false. 
        /// </returns>
        /// <param name="obj">The object to compare with the current instance. </param>
        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(null, obj))
                return false;
            return obj is FileSize && this.Equals((FileSize)obj);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is the hash code for this instance.
        /// </returns>
        public override int GetHashCode()
        {
            return this.Size.GetHashCode();
        }

        public static implicit operator ulong(FileSize fs)
        {
            return fs.Size;
        }

        public static implicit operator FileSize(ulong num)
        {
            return new FileSize(num);
        }
    }
}
