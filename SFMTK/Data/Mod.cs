﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using BrightIdeasSoftware;

namespace SFMTK.Data
{
    /// <summary>
    /// A mod entry of the gameinfo.txt file.
    /// </summary>
    [Serializable]
    public class Mod
    {
        /// <summary>
        /// Gets or sets the name of this mod.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets whether this mod is enabled.
        /// </summary>
        /// <remarks>This property is not serialized since it's directly readable from the commented state of the gameinfo.txt entry.</remarks>
        public bool Enabled
        {
            get => this._enabled;
            set => this._enabled = value;
        }
        [NonSerialized]
        private bool _enabled;

        /// <summary>
        /// Gets or sets the category name of this mod. Stored in a separate settings key.
        /// </summary>
        public string Category { get; set; } = "Default";

        /// <summary>
        /// Gets or sets a short description of the mod, specified as a comment after the mod name. Stored in a separate settings key.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets the real path containing the files of this mod.
        /// </summary>
        public string Path => this.CustomPath ?? $"{SFM.GamePath}{this.Name}/";

        /// <summary>
        /// Gets or sets the customized real path to this mod's directory. May be null if default is used instead.
        /// <para/>Its name and location does not have to correspond to standards, as symlinks can be used to point back to it.
        /// </summary>
        public string CustomPath
        {
            get => this._customPath;
            set
            {
                if (this._customPath == value)
                    return;

                this._customPath = IOHelper.GetStandardizedPath(value);
                this.UpdateIsSymlink();
            }
        }
        private string _customPath;

        /// <summary>
        /// Gets whether this mod's location does not correspond with SFM's default location for mods.
        /// </summary>
        public bool IsSymlink { get; private set; }

        private void UpdateIsSymlink() => this.IsSymlink = this.CustomPath != null && IOHelper.IsSymbolicLink(this.CustomPath);

        /// <summary>
        /// Initializes a new instance of the <see cref="Mod" /> class.
        /// </summary>
        /// <param name="name">The name of the mod.</param>
        /// <param name="enabled">Whether this mod is enabled - loading it when launching SFM (<c>false</c> indicates that the entry is commented out).</param>
        public Mod(string name, bool enabled)
        {
            this.Name = name;
            this.Enabled = enabled;

            //Check if symlinked
            var sym = IOHelper.GetSymbolicLinkTarget(this.Path, true);
            this._customPath = IOHelper.GetStandardizedPath(sym);
            this.IsSymlink = sym != null;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"{(this.Enabled ? "" : "// ")}{(string.IsNullOrEmpty(this.Category) ? "" : $"[{this.Category}] ")}{this.Name}{(string.IsNullOrEmpty(this.Description) ? "" : $": {this.Description}")}";
    }

    /// <summary>
    /// Represents a list of mods.
    /// </summary>
    public class ModList : ObservableCollection<Mod>
    {
        /// <summary>
        /// Adds a range of mods to the collection.
        /// </summary>
        /// <param name="enumerable">The mods to add.</param>
        public void AddRange(IEnumerable<Mod> enumerable)
        {
            foreach (var mod in enumerable)
            {
                this.Items.Add(mod);
            }
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }

    /// <summary>
    /// A ModelFilter for mods.
    /// </summary>
    public class ModFilter : IModelFilter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ModFilter"/> class.
        /// </summary>
        /// <param name="text">The text to filter for.</param>
        public ModFilter(string text)
        {
            this.FilterText = text;
        }

        /// <summary>
        /// The text to filter the shortcut entries after.
        /// </summary>
        public string FilterText { get; set; }

        /// <summary>
        /// Should the given model be included when this filter is installed
        /// </summary>
        /// <param name="modelObject">The model object to consider</param>
        /// <returns>
        /// Returns true if the model will be included by the filter
        /// </returns>
        public bool Filter(object modelObject)
        {
            if (modelObject is Mod mod)
            {
                var txt = this.FilterText;
                if (mod.Name.ContainsIgnoreCase(txt) || mod.Category.ContainsIgnoreCase(txt) || mod.Description.ContainsIgnoreCase(txt))
                    return true;
            }
            return false;
        }
    }
}
