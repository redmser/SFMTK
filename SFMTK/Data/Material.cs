﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SFMTK.Data
{
    /// <summary>
    /// Materials define how meshes or geometry are rendered by giving them properties and assigning textures and shaders.
    /// </summary>
    /// <remarks>
    /// Since a VMT file can not define anything but a shader, we're treating them as the same thing, though this class should technically be called Shader.
    /// </remarks>
    public class Material : IData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Material"/> class.
        /// </summary>
        public Material(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Gets or sets the name of this shader.
        /// </summary>
        public string Name
        {
            get => this._name;
            set
            {
                if (this._name == value)
                    return;

                this._name = value;
                this.OnPropertyChanged();
            }
        }
        private string _name;

        /// <summary>
        /// Gets a formatted description of this shader and how to use it.
        /// <para/>&lt;v&gt; tags point to a Valve Developer Community page of that name (optional page attribute).
        /// <para/>&lt;s&gt; refers to a shader of that name (optional name attribute).
        /// <para/>&lt;h&gt; refers to a help page of that name (optional name attribute).
        /// <para/>&lt;p&gt; refers to a shader parameter of that name (optional name attribute).
        /// </summary>
        [JsonProperty("Description")]
        public string FormattedDescription { get; private set; }

        /// <summary>
        /// Gets an unformatted version of the description of this shader and how to use it.
        /// </summary>
        [JsonIgnore]
        public string Description => StringHelper.StripXmlTags(this.FormattedDescription);

        /// <summary>
        /// Gets a list of shaders that function as fallbacks to this shader (such as specific DirectX versions).
        /// </summary>
        [JsonProperty]
        public IReadOnlyCollection<string> Fallbacks { get; private set; }

        /// <summary>
        /// Gets the list of shader parameters. Shader parameters with their default values are also included.
        /// </summary>
        [JsonProperty("Parameters"), JsonConverter(typeof(ShaderParameterConverter))]
        public List<ShaderParameter> AllParameters { get; private set; }

        /// <summary>
        /// Gets all shader parameters that do not use the default value.
        /// </summary>
        [JsonIgnore]
        public IEnumerable<ShaderParameter> Parameters => this.AllParameters.Where(p => p.IsValueModified);

        /// <summary>
        /// Gets the shader parameter with the specified name (including the dollar symbol), or null if it does not exist.
        /// </summary>
        /// <param name="name">The name of the shader parameter, including the dollar symbol at the beginning.</param>
        /// <param name="includeDefaults">If set to <c>true</c>, search also includes parameters that are not specified (to get their default value).</param>
        public ShaderParameter GetParameter(string name, bool includeDefaults = false)
            => this.Parameters.FirstOrDefault(p => (p.IsValueModified || includeDefaults) && p.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));

        /// <summary>
        /// Gets the value of the specified shader parameter (including the dollar symbol).
        /// </summary>
        /// <typeparam name="T">Type of value that the shader parameter holds.</typeparam>
        /// <param name="name">Name of the shader parameter, including dollar symbol.</param>
        /// <param name="throwOnNotFound">If set to <c>true</c>, throws a <see cref="ArgumentNullException"/> if the parameter is not found. Otherwise returns the default value of <typeparamref name="T"/>.</param>
        public T GetValue<T>(string name, bool throwOnNotFound = true)
        {
            var param = this.GetParameter(name);
            if (param == null)
            {
                if (throwOnNotFound)
                    throw new ArgumentNullException(nameof(name));
                return default(T);
            }

            return this.GetValue<T>(param);
        }

        private T GetValue<T>(ShaderParameter param)
        {
            if (typeof(T) == typeof(string))
                return (T)(object)param.Value;
            else if (typeof(T) == typeof(Vector3))
                return (T)(object)((Vector3ShaderParameter)param).Vector3Value;
            else if (typeof(T) == typeof(Color))
                return (T)(object)((ColorShaderParameter)param).ColorValue;
            else if (typeof(T) == typeof(int))
                return (T)(object)((IntShaderParameter)param).IntValue;
            else if (typeof(T) == typeof(decimal) || typeof(T) == typeof(double) || typeof(T) == typeof(float))
                return (T)(object)((DecimalShaderParameter)param).DecimalValue;

            throw new InvalidOperationException($"No shader parameter accepts values of type \"{typeof(T).Name}\".");
        }

        /// <summary>
        /// Sets the value of the specified shader parameter (including the dollar symbol). Returns the old value of the shader parameter.
        /// </summary>
        /// <typeparam name="T">Type of value that the shader parameter holds.</typeparam>
        /// <param name="name">Name of the shader parameter, including dollar symbol.</param>
        /// <param name="value">New value for the shader parameter.</param>
        /// <param name="throwOnNotFound">If set to <c>true</c>, throws a <see cref="ArgumentNullException"/> if the parameter is not found. Otherwise returns the default value of <typeparamref name="T"/>.</param>
        public T SetValue<T>(string name, T value, bool throwOnNotFound = false)
        {
            var param = this.GetParameter(name);
            T oldvalue;
            if (param == null)
            {
                if (throwOnNotFound)
                    throw new ArgumentNullException(nameof(name));
                oldvalue = default(T);
            }
            else
            {
                oldvalue = this.GetValue<T>(param);
            }

            this.SetValue<T>(param, value);
            return oldvalue;
        }

        private void SetValue<T>(ShaderParameter param, T value)
        {
            if (typeof(T) == typeof(string))
            {
                param.Value = (string)(object)value;
                return;
            }
            else if (typeof(T) == typeof(Vector3))
            {
                ((Vector3ShaderParameter)param).Vector3Value = (Vector3)(object)value;
                return;
            }
            else if (typeof(T) == typeof(Color))
            {
                ((ColorShaderParameter)param).ColorValue = (Color)(object)value;
                return;
            }
            else if (typeof(T) == typeof(int))
            {
                ((IntShaderParameter)param).IntValue = (int)(object)value;
                return;
            }
            else if (typeof(T) == typeof(decimal) || typeof(T) == typeof(double) || typeof(T) == typeof(float))
            {
                ((DecimalShaderParameter)param).DecimalValue = (decimal)(object)value;
                return;
            }

            throw new InvalidOperationException($"No shader parameter accepts values of type \"{typeof(T).Name}\".");
        }

        /// <summary>
        /// Occurs when a property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when a property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) => this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        /// <summary>
        /// Returns whether the specified text is a known shader name.
        /// </summary>
        /// <param name="text">The text to check for.</param>
        /// <remarks>Use upper-case entries for higher performance, when possible.</remarks>
        public static bool IsShader(string text)
            => KnownShaders.Any(s => s.Name.Equals(text, StringComparison.InvariantCultureIgnoreCase));

        /// <summary>
        /// Gets a list of known shader names.
        /// </summary>
        public static IReadOnlyCollection<Material> KnownShaders { get; private set; }
        //FIXME: Material - populate the "hardcoded" list of known shaders using the shaders included in source 2013
        //  -> toolload vmt in SFM's console opens a visual editor with ALL shaders and ALL shader params INCLUDING their type AND default value (maybe even description???)
        //  -> see if we can copy-paste this / or generate a VMT from it / or simply manually copy it all

        /// <summary>
        /// Path to a config file containing the list of known shaders.
        /// </summary>
        public const string KnownShadersFile = "knownshaders.cfg";

        /// <summary>
        /// Refreshes the list of known shaders and corresponding shader parameters from file.
        /// </summary>
        public static void UpdateKnownShaders()
        {
            if (!File.Exists(KnownShadersFile))
                throw new FileNotFoundException($"Could not find {KnownShadersFile} file!", KnownShadersFile);

            //IMP: Shader - list of known shaders and shader parameters should be loaded dynamically, and not all at once! json supports this!!
            var txt = File.ReadAllText(KnownShadersFile);
            var obj = JObject.Parse(txt);

            //Parse the groups first - this step is done so the ShaderParameterConverter.ParameterGroups collection is populated
            //Groups have to be defined before they can be used. This also goes for recursively defined groups.
            ShaderParameterConverter.ParameterGroups.Clear();

            var shaderparams = obj["ParameterGroups"] as JArray;
            var serializer = new JsonSerializer();
            serializer.Converters.Add(new ShaderParameterConverter());
            shaderparams.ToObject<List<ShaderParameter>>(serializer);

            //Parse known shaders and their parameters
            var knownshaders = obj["KnownShaders"] as JArray;
            KnownShaders = knownshaders.ToObject<List<Material>>();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"{this.Name} - {StringHelper.GetPlural(this.Parameters.Count(), "parameter")}, {this.AllParameters.Count} total";
    }

    /// <summary>
    /// A special kind of shader which can replace certain material definitions. Full definition is not required for a SFM context.
    /// </summary>
    public class ReplacementsMaterial : Material
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReplacementsMaterial"/> class.
        /// </summary>
        public ReplacementsMaterial() : base("Replacements")
        {

        }
    }
}
