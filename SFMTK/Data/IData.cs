﻿using System.ComponentModel;

namespace SFMTK.Data
{
    /// <summary>
    /// An interface for data which can notify the viewport about any changes in the visualized data.
    /// </summary>
    /// <remarks>PropertyChanged should be called recursively. Any data-representing classes inside of an instance of an object should also implement IData accordingly.</remarks>
    public interface IData : INotifyPropertyChanged
    {

    }
}
