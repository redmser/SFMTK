﻿using System.Drawing;

namespace SFMTK.Data
{
    /// <summary>
    /// A texture mask, containing values on each pixel position.
    /// </summary>
    public abstract class MaskTexture
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaskTexture"/> class.
        /// </summary>
        protected MaskTexture()
        {

        }

        /// <summary>
        /// Creates a new <see cref="MaskTexture"/> with pixels that only have a single value.
        /// </summary>
        /// <param name="value">The value to use for all pixels on the mask.</param>
        public static MaskTexture FromValue(byte value) => new FilledMaskTexture(value);

        /// <summary>
        /// Creates a new <see cref="MaskTexture"/> from the specified black and white bitmap.
        /// </summary>
        /// <param name="bmp">Source bitmap to create mask from.</param>
        public static MaskTexture FromBitmap(Bitmap bmp) => new BitmapMaskTexture(bmp);

        /// <summary>
        /// Gets the value of the specified pixel.
        /// </summary>
        /// <param name="x">X-coordinate of the pixel.</param>
        /// <param name="y">Y-coordinate of the pixel.</param>
        public abstract byte GetPixel(int x, int y);

        /// <summary>
        /// Gets the value of the specified pixel.
        /// </summary>
        /// <param name="p">Location of the pixel.</param>
        public byte GetPixel(Point p) => this.GetPixel(p.X, p.Y);

        /// <summary>
        /// Gets the value of the specified pixel.
        /// </summary>
        /// <param name="x">X-coordinate of the pixel.</param>
        /// <param name="y">Y-coordinate of the pixel.</param>
        public byte this[int x, int y] => this.GetPixel(x, y);

        /// <summary>
        /// Simple check for whether the value of the specified pixel can be gotten.
        /// </summary>
        /// <param name="x">X-coordinate of the pixel.</param>
        /// <param name="y">Y-coordinate of the pixel.</param>
        public abstract bool CanGetPixel(int x, int y);

        /// <summary>
        /// Simple check for whether the value of the specified pixel can be gotten.
        /// </summary>
        /// <param name="p">Location of the pixel.</param>
        public bool CanGetPixel(Point p) => this.CanGetPixel(p.X, p.Y);

        /// <summary>
        /// A <see cref="MaskTexture"/> with a single value. Does not check for out-of-bounds coordinates, as no backing image is used.
        /// </summary>
        private class FilledMaskTexture : MaskTexture
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="FilledMaskTexture"/> class with all pixels set to 0.
            /// </summary>
            public FilledMaskTexture() : base()
            {

            }

            /// <summary>
            /// Initializes a new instance of the <see cref="FilledMaskTexture"/> class with the specified value.
            /// </summary>
            /// <param name="value">The value of all pixels of the mask.</param>
            public FilledMaskTexture(byte value) : this()
            {
                this.Value = value;
            }

            /// <summary>
            /// Gets the value of the specified pixel.
            /// </summary>
            /// <param name="x">X-coordinate of the pixel.</param>
            /// <param name="y">Y-coordinate of the pixel.</param>
            public override byte GetPixel(int x, int y) => this.Value;

            /// <summary>
            /// Always returns true.
            /// </summary>
            public override bool CanGetPixel(int x, int y) => true;
            //TODO: MaskTexture - should the FilledMaskTexture do bounds-checking for CanGetPixel?

            /// <summary>
            /// Gets or sets the value used for the entire mask.
            /// </summary>
            public byte Value { get; set; }
        }

        /// <summary>
        /// A <see cref="MaskTexture"/> which uses an image to retrieve pixel values.
        /// </summary>
        private class BitmapMaskTexture : MaskTexture
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="BitmapMaskTexture"/> class with no bitmap.
            /// </summary>
            public BitmapMaskTexture() : base()
            {

            }

            /// <summary>
            /// Initializes a new instance of the <see cref="BitmapMaskTexture"/> class with the specified backing image.
            /// </summary>
            /// <param name="bmp">The image to retrieve pixel values from.</param>
            public BitmapMaskTexture(Bitmap bmp) : this()
            {
                this.Bitmap = bmp;
            }

            /// <summary>
            /// Gets or sets the bitmap to contain the pixel values. Only the red channel of the image is used.
            /// </summary>
            public Bitmap Bitmap { get; set; }

            /// <summary>
            /// Gets the value of the specified pixel.
            /// </summary>
            /// <param name="x">X-coordinate of the pixel.</param>
            /// <param name="y">Y-coordinate of the pixel.</param>
            public override byte GetPixel(int x, int y) => this.Bitmap.GetPixel(x, y).R;

            /// <summary>
            /// Simple check for whether the specified pixel is on the bitmap and whether a backing image is specified.
            /// </summary>
            /// <param name="x">X-coordinate of the pixel.</param>
            /// <param name="y">Y-coordinate of the pixel.</param>
            public override bool CanGetPixel(int x, int y)
            {
                //Simple null check
                if (this.Bitmap == null)
                    return false;

                //Same check as this.Bitmap.GetPixel() already does
                if (x < 0 || y < 0 || x >= this.Bitmap.Width || y >= this.Bitmap.Height)
                    return false;

                return true;
            }
        }
    }
}
