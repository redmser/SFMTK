﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SFMTK.Data
{
    /// <summary>
    /// Parses the <see cref="Material.AllParameters"/> list by resolving group references.
    /// </summary>
    public class ShaderParameterConverter : JsonConverter
    {
        /// <summary>
        /// Gets a list of known shader parameter groups (defined in the <c>ParameterGroups</c> section), where their names are associated to the parameters they define.
        /// </summary>
        public static Dictionary<string, List<ShaderParameter>> ParameterGroups { get; } = new Dictionary<string, List<ShaderParameter>>();

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        /// <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType) => objectType == typeof(List<ShaderParameter>);

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader" /> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being read.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>
        /// The object value.
        /// </returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var list = new List<ShaderParameter>();
            var grouprefs = new List<string>();
            var array = JArray.Load(reader);

            //IMP: ShaderParameterConverter - when instanciating the ShaderParameters, ensure the type value is being read to
            //  determine which type to create, as well as which value property to set given the value in the config
            //  -> some types don't determine the input data type directly, but merely what values are to be expected and how to browse for them
            //     (e.g. texture paths versus string values)

            //Iterate through shader parameters
            foreach (var token in array)
            {
                //Has to be a JObject
                if (!(token is JObject obj))
                    continue;

                //What are we parsing, Groups or Shader's Params?
                if (obj.TryGetValue("Parameters", out var pars))
                {
                    //We are parsing the groups! Store this in the static dict
                    var recurser = new JsonSerializer();
                    recurser.Converters.Add(new ShaderParameterConverter());
                    ParameterGroups.Add(obj.Value<string>("Name"), pars.ToObject<List<ShaderParameter>>(recurser));
                    continue;
                }

                //Is a group reference?
                if (obj.TryGetValue("GroupReference", out var group))
                {
                    //Resolve later
                    grouprefs.Add(group.ToObject<string>());
                }
                else
                {
                    //Is a parameter, parse as is
                    list.Add(obj.ToObject<ShaderParameter>());
                }
            }

            //Resolve group references
            foreach (var groupname in grouprefs)
            {
                //Does this group exist?
                var parameters = ParameterGroups[groupname];

                //Add its parameters
                list.AddRange(parameters);
            }

            return list;
        }

        /// <summary>
        /// Throws a <see cref="InvalidOperationException"/>, as writing the known shaders file is not required.
        /// </summary>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => throw new InvalidOperationException();
    }
}
