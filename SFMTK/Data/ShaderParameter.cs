﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Numerics;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace SFMTK.Data
{
    /// <summary>
    /// A shader parameter accepting string values.
    /// </summary>
    public class ShaderParameter : IData
    {
        /// <summary>
        /// Gets or sets the full name (key) of the shader parameter, including the dollar symbol.
        /// </summary>
        public string Name
        {
            get => this._name;
            set
            {
                if (this._name == null)
                {
                    //First define, should not trigger any event
                    this._name = value;
                    return;
                }

                if (this._name == value)
                    return;

                this._name = value;
                this.OnPropertyChanged();
            }
        }
        private string _name;

        /// <summary>
        /// Gets or sets the string value of the shader parameter, formatted as in the file.
        /// </summary>
        [JsonProperty("DefaultValue")]
        public string Value
        {
            get => this._value;
            set
            {
                if (this._value == null)
                {
                    //First define, should not trigger any event
                    this._value = value;
                    return;
                }

                if (this._value == value)
                    return;

                this._value = value;
                //IMP: ShaderParameter - this will be invalid if setting back to the original default value
                //  -> this has to be overridden for other shader parameters, as STRING formatting for Vec3 shouldn't matter!
                this.IsValueModified = true;
                this.OnPropertyChanged();
            }
        }
        private string _value;

        /// <summary>
        /// Gets whether the <see cref="Value"/> of this shader parameter is different from the default value.
        /// </summary>
        public bool IsValueModified { get; private set; }

        /// <summary>
        /// Gets a formatted description of this shader parameter and how to use it.
        /// <para/>&lt;v&gt; tags point to a Valve Developer Community page of that name (optional page attribute).
        /// <para/>&lt;s&gt; refers to a shader of that name (optional name attribute).
        /// <para/>&lt;h&gt; refers to a help page of that name (optional name attribute).
        /// <para/>&lt;p&gt; refers to a shader parameter of that name (optional name attribute).
        /// </summary>
        [JsonProperty("Description")]
        public string FormattedDescription { get; private set; }

        /// <summary>
        /// Gets an unformatted version of the description of this shader parameter and how to use it.
        /// </summary>
        [JsonIgnore]
        public string Description => StringHelper.StripXmlTags(this.FormattedDescription);

        /// <summary>
        /// Gets which shader parameter is required in the VMT definition in order for this parameter to make sense.
        /// <para/>This will not be treated as an error, as most parameters have default values.
        /// </summary>
        [JsonProperty("Requires")]
        public string RequiredParameter { get; private set; }

        /// <summary>
        /// Occurs when a property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when a property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) => this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        public override string ToString() => $"\"{this.Name}\" \"{this.Value}\"";
    }

    /// <summary>
    /// A shader parameter accepting decimal values.
    /// </summary>
    public class DecimalShaderParameter : ShaderParameter
    {
        /// <summary>
        /// Gets or sets the numeric value of the shader parameter.
        /// </summary>
        public virtual decimal DecimalValue
        {
            get => decimal.Parse(this.Value);
            set => this.Value = value.ToString();
        }
    }

    /// <summary>
    /// A shader parameter accepting integer values.
    /// </summary>
    public class IntShaderParameter : ShaderParameter
    {
        /// <summary>
        /// Gets or sets the numeric value of the shader parameter.
        /// </summary>
        public virtual int IntValue
        {
            get => int.Parse(this.Value);
            set => this.Value = value.ToString();
        }
    }

    /// <summary>
    /// A shader parameter accepting Vector3 values.
    /// </summary>
    public class Vector3ShaderParameter : ShaderParameter
    {
        //UNTESTED: Vector3ShaderParameter - how does source handle { } arrays with values outside of the 0 - 255 range?
        //  -> if it works the same way as unbounded [ ] arrays, this type can be combined with the ColorShaderParameter again

        /// <summary>
        /// Gets or sets the Vector3 value of the shader parameter.
        /// </summary>
        public virtual Vector3 Vector3Value
        {
            get
            {
                //Strip for numbers only
                var trim = this.Value.Trim();
                var split = trim.Substring(1, trim.Length - 2).Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                return new Vector3(float.Parse(split[0]),
                                   float.Parse(split[1]),
                                   float.Parse(split[2]));
            }
            set
            {
                this.Value = $"[{value.X:0.###} {value.Y:0.###} {value.Z:0.###}]";
            }
        }
    }

    /// <summary>
    /// A shader parameter accepting Color values. This class does not work with the Color structure, since its values are bounded, while Source does not clamp in most cases.
    /// </summary>
    public class ColorShaderParameter : Vector3ShaderParameter
    {
        /// <summary>
        /// Gets or sets the Vector3 value of the shader parameter. Uses color values from 0 to 255, but is not clamped.
        /// </summary>
        public override Vector3 Vector3Value
        {
            get
            {
                var trim = this.Value.Trim();
                bool isDecimals;
                if (trim[0] == '[')
                {
                    isDecimals = true;
                }
                else if (trim[0] == '{')
                {
                    isDecimals = false;
                }
                else
                {
                    throw new FormatException("Invalid color string - does not start with [ or {");
                }

                //Strip for numbers only
                var split = trim.Substring(1, trim.Length - 2).Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                var red = float.Parse(split[0]);
                var green = float.Parse(split[1]);
                var blue = float.Parse(split[2]);
                if (isDecimals)
                {
                    red *= 255;
                    green *= 255;
                    blue *= 255;
                }
                return new Vector3(red, green, blue);
            }
            set
            {
                if (this.PreferDecimals)
                    this.Value = $"[{value.X / 255:0.###} {value.Y / 255:0.###} {value.Z / 255:0.###}]";
                else
                    this.Value = $"{{{value.X} {value.Y} {value.Z}}}";
            }
        }

        /// <summary>
        /// Gets or sets the color values of this shader parameter. You should get values from the Vector3Value property instead, as Colors are clamped here.
        /// </summary>
        public virtual Color ColorValue
        {
            get => this.Vector3Value.ToColor();
            set => this.Vector3Value = new Vector3(value.R, value.G, value.B);
        }

        /// <summary>
        /// Gets or sets whether this shader parameter should prefer decimal values. Colors can be specified using either [0 to 1] arrays (false) or {0 to 255} arrays (true).
        /// </summary>
        public bool PreferDecimals { get; set; } = false;
    }
}
