﻿using System;
using System.IO;
using System.Linq;
using SFMTK.Parsing.KeyValues;

namespace SFMTK.Data
{
    /// <summary>
    /// A gameinfo.txt file, mostly located in usermod, which contains info about which mods are loaded and in which order.
    /// </summary>
    public class GameInfo
    {
        /// <summary>
        /// List of mods currently loaded by this GameInfo, in order of definition.
        /// </summary>
        public ModList Mods { get; } = new ModList();

        /// <summary>
        /// Full path to the gameinfo.txt file.
        /// </summary>
        private string Path { get; set; }

        /// <summary>
        /// Returns the first mod in this GameInfo instance with the specified name, or null if the mod does not exist.
        /// </summary>
        /// <param name="name">Name of the mod to retrieve.</param>
        public Mod GetMod(string name) => this.Mods.FirstOrDefault(m => m.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));

        /// <summary>
        /// Initializes a new instance of the <see cref="GameInfo"/> class.
        /// </summary>
        public GameInfo() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameInfo"/> class.
        /// </summary>
        /// <param name="path">The full path to the gameinfo.txt file.</param>
        /// <param name="load">If set to <c>true</c>, loads the gameinfo.txt file after creation.</param>
        public GameInfo(string path, bool load = true) : this()
        {
            this.Path = path;

            if (load)
                this.Load();
        }

        /// <summary>
        /// Loads this GameInfo instance.
        /// </summary>
        /// <exception cref="FileNotFoundException">Could not find the gameinfo.txt specified by the path.</exception>
        public void Load()
        {
            if (string.IsNullOrWhiteSpace(this.Path) || !File.Exists(this.Path))
                throw new FileNotFoundException("Could not find gameinfo.txt file.", this.Path);

            var kvparser = new KeyValuesTokenizer();
            kvparser.Rules.IncludeComments = IncludeCommentsMode.Nested;
            kvparser.Read(this.Path);
            var kvs = kvparser.Parse();

            //We only need all game entries
            var games = kvs["GameInfo"].GetByPath("FileSystem", "SearchPaths").Children;

            //Add all mods
            //UNTESTED: GameInfo - what Keys are supported in the SearchPaths? can we use our own "Begin/EndCategory" "Name" and "Description" keys?
            //  -> else just include this info as a key with children, on the same level of indent as SearchPaths
            this.Mods.Clear();
            this.Mods.AddRange(games.Select(kv =>
            {
                var name = kv.Value;
                var loaded = true;
                if (kv is KVComment)
                {
                    //Remove the Game string from name
                    //HACK: GameInfo - it may be nicer to actually re-tokenize these nodes
                    var basename = name.Replace("\"", "").Trim();
                    if (!basename.StartsWith("game", StringComparison.InvariantCultureIgnoreCase))
                        return null;
                    name = basename.Substring(4).TrimStart();
                    loaded = false;
                }
                else if (kv is KVPair pair && !pair.Key.Equals("game", StringComparison.InvariantCultureIgnoreCase))
                {
                    //Not a game entry
                    return null;
                }

                if (name == "|gameinfo_path|.")
                {
                    //Gets the name of the mod folder that this gameinfo.txt is inside of
                    name = SFM.GetLocalPath(System.IO.Path.GetDirectoryName(this.Path), RelativeTo.GameDir);
                    name = name.Substring(0, name.Length - 1); //Remove trailing slash
                }
                return new Mod(name, loaded);
            }).Where(m => m != null));
        }

        /// <summary>
        /// Saves this GameInfo instance.
        /// </summary>
        public void Save()
        {
            //NYI: GameInfo - save gameinfo instance based on mod list
            throw new NotImplementedException();
        }

        /// <summary>
        /// Updates the path of this GameInfo instance.
        /// </summary>
        /// <param name="newpath"></param>
        public void UpdatePath(string newpath) => this.Path = newpath;
    }
}
