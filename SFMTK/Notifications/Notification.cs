﻿using SFMTK.Properties;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;

namespace SFMTK.Notifications
{
    /// <summary>
    /// A notification to display for anything noteworthy to the user.
    /// </summary>
    [Serializable]
    public abstract class Notification
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Notification" /> class.
        /// </summary>
        /// <param name="title">A short summary of the notification to display as a title.</param>
        /// <param name="color">Which color the notification should have. Used for both icons and any other identifying coloring.</param>
        /// <param name="priority">The priority of this notification. Higher values indicate a higher importance.</param>
        public Notification(string title, NotificationColor color, int priority = 0)
        {
            this.Title = title;
            this.Color = color;
            this.Priority = priority;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Notification"/> class.
        /// </summary>
        public Notification() { }

        /// <summary>
        /// Gets or sets a short summary of the notification to display as a title.
        /// </summary>
        public virtual string Title { get; set; }

        /// <summary>
        /// Gets or sets the priority of this notification. Higher values indicate a higher importance.
        /// </summary>
        public virtual int Priority { get; set; }

        /// <summary>
        /// Gets or sets which color the notification should have. Used for both default icons and any other identifying coloring.
        /// </summary>
        public virtual NotificationColor Color { get; set; }

        /// <summary>
        /// Gets or sets the image used to display this notification. If null is used (default), icon is determined from the color selected.
        /// </summary>
        public virtual Image Icon
        {
            get
            {
                if (this._customIcon != null)
                    return this._customIcon;

                switch (this.Color)
                {
                    case NotificationColor.Blue:
                        return Resources.flag_blue;
                    case NotificationColor.Green:
                        return Resources.flag_green;
                    case NotificationColor.Pink:
                        return Resources.flag_pink;
                    case NotificationColor.Red:
                        return Resources.flag_red;
                    case NotificationColor.Yellow:
                        return Resources.flag_yellow;
                    default:
                        return Resources.question;
                }
            }
            set
            {
                this._customIcon = value;
            }
        }
        private Image _customIcon;

        /// <summary>
        /// Called when the notification was clicked. Returns whether the notification should be removed after exiting the method.
        /// </summary>
        public abstract bool OnClick();
    }

    /// <summary>
    /// A collection of notifications.
    /// </summary>
    [Serializable]
    public class NotificationCollection : ObservableCollection<Notification>
    {
        /// <summary>
        /// Gets the first notification with the specified type.
        /// </summary>
        public Notification this[Type type] => this.FirstOrDefault(n => n.GetType() == type);
    }

    /// <summary>
    /// Which color the notification should have.
    /// </summary>
    public enum NotificationColor
    {
        /// <summary>
        /// Notification uses a blue colored flag.
        /// </summary>
        Blue,
        /// <summary>
        /// Notification uses a green colored flag. Intended for updates available.
        /// </summary>
        Green,
        /// <summary>
        /// Notification uses a pink colored flag.
        /// </summary>
        Pink,
        /// <summary>
        /// Notification uses a red colored flag. Intended for any non-critical errors.
        /// </summary>
        Red,
        /// <summary>
        /// Notification uses a yellow colored flag. Intended for important information.
        /// </summary>
        Yellow
    }
}
