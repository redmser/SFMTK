﻿namespace SFMTK.Notifications
{
    /// <summary>
    /// Notification used for debugging.
    /// </summary>
    public class TestNotification : Notification
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestNotification"/> class.
        /// </summary>
        public TestNotification() : base("Test notification", NotificationColor.Pink, 9)
        {

        }

        /// <summary>
        /// Called when the notification was clicked. Returns whether the notification should be removed after exiting the method.
        /// </summary>
        public override bool OnClick()
        {
            var res = FallbackTaskDialog.ShowDialog($"Clicked on notification \"{this.Title}\"!\n\nDo you want to remove this notification?", null, "Success!",
                FallbackDialogIcon.Info, new FallbackDialogButton(System.Windows.Forms.DialogResult.Yes), new FallbackDialogButton(System.Windows.Forms.DialogResult.No));

            return res == System.Windows.Forms.DialogResult.Yes;
        }
    }
}
