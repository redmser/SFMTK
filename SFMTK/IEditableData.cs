﻿using SFMTK.Commands;

namespace SFMTK
{
    /// <summary>
    /// Defines data which can be edited using <see cref="EditCommand"/>s.
    /// </summary>
    public interface IEditableData
    {
        /// <summary>
        /// Gets a command used to cut from the underlying data. Has to support undo! DisplayText is taken from Command.
        /// </summary>
        Command CutCommand { get; }

        /// <summary>
        /// Gets a command used to copy from the underlying data. Should not be undo-able (do not show in history). DisplayText is taken from Command.
        /// </summary>
        Command CopyCommand { get; }

        /// <summary>
        /// Gets a command used to paste from the underlying data. Has to support undo! DisplayText is taken from Command.
        /// </summary>
        Command PasteCommand { get; }

        /// <summary>
        /// Gets a command used to delete from the underlying data. Has to support undo! DisplayText is taken from Command.
        /// </summary>
        Command DeleteCommand { get; }

        /// <summary>
        /// Gets whether the user can currently delete any of the underlying data for cutting or deleting.
        /// This may for example be <c>false</c> if the data is read-only.
        /// </summary>
        bool CanDelete { get; }

        /// <summary>
        /// Gets whether the user can currently read the underlying data for copying.
        /// This may for example be <c>false</c> if the data is loading, or no data is present.
        /// </summary>
        bool CanCopy { get; }

        /// <summary>
        /// Gets whether the user can paste data using the current state of the clipboard.
        /// This may for example be <c>false</c> if the data is read-only, or if nothing relevant is in the clipboard.
        /// </summary>
        bool CanPaste { get; }
    }
}
