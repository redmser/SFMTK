﻿namespace SFMTK.Modules
{
    /// <summary>
    /// Any class which implements this interface is recognized as a SFMTK module and can be loaded from the Modules directory.
    /// <para/>Check the documentation in the source code (as well as the related interfaces) for more information.
    /// </summary>
    public interface IModule
    {
        // Notes to anyone looking to create their own modules:
        // - Any assembly placed in the Modules directory that host a single class which implements IModule is loaded as a module.
        // - Dependencies are resolved from the same directory as the module (or any other probing paths set). If it can't be found there, SFMTK's directory is used instead.
        // - The loading context of a module determines when it is loaded, and what it loads
        //   - Not every loading context of the startup procedure is always triggered (e.g. GUI in command mode) - use this to your advantage
        //     and only load what really is needed when it is needed
        //   - By specifying multiple loading contexts (flags enum), the OnLoad method is called multiple times (the "initialized" parameter lets you know
        //     whether the module was unloaded before this OnLoad call)
        // - The module hosting class should only have the default constructor. This should not change state, as OnLoad is called in that case.
        // - Following possibilities exist to extend SFMTK using modules:
        //   - To extend MainForm's ToolStrips / MenuStrip / StatusStrip
        //     - Use the members GetToolbar / mainMenuBar / mainStatusStrip respectively
        //     - The MergeAction/MergeIndex properties may help to add new members without relying on load order or external item changes
        //     - Anything that you add inside of OnLoad should also be removed again at OnUnload
        //     - You can show new notifications by calling Main's AddNotification method (inherit from the Notification class for creating your own types)
        //   - Create new widgets by inheriting from Widget and accessing MainForm's DockPanelManager to show them
        //     - If your module's namespace is Baz.Modules.Test, your widget has to be in the Baz.Modules.Test.Widgets namespace to be recognized automatically
        //     - If the GenerateShowCommand = true (default), you can set a control's Command property to "ShowWidgetCommand;WIDGETNAME" for opening the widget
        //   - Add new preference pages by implementing the IModuleSettings interface on your module
        //     - Be sure that your preference page defines the required properties (such as Path, Image or ExtendControl)
        //     - If you wish to add settings to existing preference pages, use the AdditionalProperties property
        //   - New settings can be added like usual (App.config etc.). Additionally implement IModuleSettings and be sure to save the settings when unloading.
        //   - Add new command-line arguments by implementing the IModuleTokens interface on your module
        //     - Since not many possible short names exist, you're best off using '\0' for none instead.
        //   - New content types and converters can be added by creating classes that are inheriting from the corresponding base classes
        //     - In order to register these changes, check the IModuleContents and IModuleConverters interfaces
        //   - In order to add new commands which can be undone and rebound, inherit from any subclass of CommandBase and use the IModuleCommands interface
        //     - If your module's namespace is Baz.Modules.Test, your command has to be in the Baz.Modules.Test.Commands
        // - Example post-build event for moving build results to SFMTK's modules directory (excludes all xml and pdb files):
        //     robocopy "$(TargetDir.TrimEnd('\'))" "<PATH TO SFMTK>\Modules\$(TargetName)" /S /E /COPYALL /XO /XF "*.xml" "*.pdb"
        //     IF %ERRORLEVEL% GEQ 8 exit 1
        //     exit 0

        //TODO: IModule - allow specifying a load order, not only through dependencies, but also sort of a priority
        //TODO: IModule - replace extending tool/menu/statusstrip elements with an interface of sorts (to avoid incorrect (un)loading code, and hardcoded indices)

        /// <summary>
        /// Gets the loading context of the module, determining on which context the OnLoad method should be called.
        /// </summary>
        /// <remarks>If <c>None</c> is used, this module will only load if manually prompted.</remarks>
        ModuleLoadingContext GetLoadingContext();

        /// <summary>
        /// Called when the module is told to load, either on startup
        /// (only called once, with the context specified on <see cref="GetLoadingContext"/>) or when manually loaded (can be any context).
        /// <para />Returns whether the module has been loaded successfully and is now enabled.
        /// </summary>
        bool OnLoad(ModuleLoadingContext context);

        /// <summary>
        /// Called when the module is unloaded, after having been initialized.
        /// <para/>Returns whether the module has been unloaded successfully and is now disabled.
        /// </summary>
        bool OnUnload(ModuleUnloadingContext context);

        /// <summary>
        /// Called on every step of the startup procedure (entries in <see cref="ModuleLoadingContext"/> prefixed with <c>Startup</c>),
        /// including the one selected with <see cref="GetLoadingContext"/>.
        /// <para/>If the module is late-loaded (such as enabling in the settings), all steps of the startup procedure are called directly after one another.
        /// <para/>Be sure to use <see cref="ControlsHelper.InvokeIfRequired"/>, to ensure that cross-thread usage will not break anything.
        /// </summary>
        void OnStartup(ModuleLoadingContext context);

        /// <summary>
        /// Returns the full names of all modules that this module should depend on (or <c>null</c> / an empty array if no dependencies exist).
        /// <para/>A module with dependencies requires files or interfaces provided by those modules.
        /// <para/>If one or more dependency could not be found, this module can not be loaded.
        /// </summary>
        string[] GetDependencies();

        /// <summary>
        /// Returns the full name of the parent module (or <c>null</c> if this module is a top-level module).
        /// Causes the module to appear as a child of the parent in the module list (tree-structure).
        /// <para/>Mostly used for organizing dependencies in a cleaner way, but does not require the parent to be loaded.
        /// <para/>If the parent could not be found, this module appears at root-level instead.
        /// </summary>
        string GetParent();
    }

    /// <summary>
    /// Context for when a module is to be loaded.
    /// </summary>
    public enum ModuleLoadingContext
    {
        /// <summary>
        /// No or an unknown context caused this module to load.
        /// </summary>
        None,
        /// <summary>
        /// Start-up procedure: Loaded before parsing the command-line arguments, as the module may define tokens using the <see cref="IModuleTokens"/> interface.
        /// </summary>
        StartupCommandLine,
        /// <summary>
        /// Start-up procedure: Loaded while the user-interface of the main dialog is loading, as it is extended by the module in some way
        /// (such as by adding a new toolstrip or menuitem).
        /// </summary>
        StartupUserInterface,
        /// <summary>
        /// Start-up procedure: Loaded asynchronously after the user-interface of the main dialog finished loading,
        /// as the module does not require immediate integration to SFMTK.
        /// </summary>
        StartupLazyLoaded,
        /// <summary>
        /// Loaded by the "New Modules Found" dialog that shows up when starting SFMTK with the module not known to SFMTK yet.
        /// <para/>This context can be used to introduce how the module works in the user interface.
        /// </summary>
        NewModule,
        /// <summary>
        /// Loaded by the module being installed through any method in the settings dialog.
        /// <para/>This context can be used to introduce how the module works in the user interface.
        /// </summary>
        SettingsInstall,
        /// <summary>
        /// Loaded by the module being enabled (after having been disabled by the user) or reloaded from inside the settings dialog.
        /// </summary>
        SettingsLoad,
        /// <summary>
        /// This module was loaded by the --load-modules command.
        /// </summary>
        Command
    }

    /// <summary>
    /// Context for when a module is unloaded.
    /// </summary>
    public enum ModuleUnloadingContext
    {
        /// <summary>
        /// No or an unknown context caused this module to load.
        /// </summary>
        None,
        /// <summary>
        /// SFMTK is shutting down. All resources should be disposed, as is possible.
        /// </summary>
        Shutdown,
        /// <summary>
        /// This module is being disabled in the settings.
        /// </summary>
        Disable,
        /// <summary>
        /// This module is being uninstalled from the settings. After this unload, the files associated with this module
        /// will also be removed - be sure to release any locks!
        /// </summary>
        Uninstall,
        /// <summary>
        /// The --unload-modules command was used to disable this module.
        /// </summary>
        Command
    }
}
