﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace SFMTK.Modules
{
    /// <summary>
    /// This module creates new toolbars which can be customized and shown using the View menu.
    /// Requires the <see cref="ModuleLoadingContext.StartupUserInterface"/> loading context.
    /// </summary>
    public interface IModuleToolBars : IModule
    {
        /// <summary>
        /// Gets the toolbars to be registered for this module. They are to be added to the main form, customizable by the user and shown in the View menu.
        /// <para/>If <c>null</c> is returned, the list is automatically populated with all classes inheriting from <see cref="ToolStrip"/>.
        /// </summary>
        IEnumerable<ToolStrip> GetToolBars();
    }
}
