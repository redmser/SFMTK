﻿using BrightIdeasSoftware;

namespace SFMTK.Modules
{
    /// <summary>
    /// <see cref="IModelFilter"/> for <see cref="ModuleSetting"/>s, by name.
    /// </summary>
    public class ModuleSettingFilter : IModelFilter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleSettingFilter"/> class.
        /// </summary>
        public ModuleSettingFilter() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleSettingFilter"/> class.
        /// </summary>
        /// <param name="searchtext">The text to search for in the module's name.</param>
        public ModuleSettingFilter(string searchtext) : this()
        {
            this.SearchText = searchtext;
        }

        /// <summary>
        /// Should the given model be included when this filter is installed
        /// </summary>
        /// <param name="modelObject">The model object to consider</param>
        /// <returns>
        /// Returns true if the model will be included by the filter
        /// </returns>
        public bool Filter(object modelObject)
        {
            var ms = modelObject as ModuleSetting;
            if (ms == null)
                return false;
            return ms.FullName.ContainsIgnoreCase(this.SearchText);
        }

        /// <summary>
        /// Gets or sets the text to search for in the module's name.
        /// </summary>
        public string SearchText { get; set; }
    }
}
