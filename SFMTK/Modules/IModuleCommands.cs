﻿using System.Collections.Generic;
using SFMTK.Commands;

namespace SFMTK.Modules
{
    /// <summary>
    /// Allows a module to define commands. These can be undone/redone, bound shortcut keys to, or associated with customized menu entries or buttons.
    /// <para/>By adding this interface and setting <see cref="CommandList"/> to <c>null</c>,
    /// all classes inheriting <see cref="CommandBase"/> will automatically be recognized as commands, if they are in the <c>Commands</c> namespace.
    /// </summary>
    public interface IModuleCommands : IModule
    {
        /// <summary>
        /// Gets the default command list to be registered for this module. These can be undone/redone, bound shortcut keys to, or associated with customized menu entries or buttons.
        /// <para/>If set to <c>null</c>, the list is automatically populated with all classes inheriting from <see cref="CommandBase"/>.
        /// </summary>
        IEnumerable<CommandBase> CommandList { get; set; }
    }
}
