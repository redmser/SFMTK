﻿using System.Collections.Generic;
using SFMTK.Converters;

namespace SFMTK.Modules
{
    /// <summary>
    /// Allows a module to define converters. They allow the user to import or export content data, as well as converting it between internal types.
    /// <para/>By adding this interface and setting <see cref="ConverterList"/> to <c>null</c>,
    /// all classes inheriting <see cref="ContentConverter"/> will automatically be recognized as commands, if they are in the <c>Converters</c> namespace.
    /// </summary>
    public interface IModuleConverters : IModule
    {
        /// <summary>
        /// Gets the default content converter list to be registered for this module. They allow the user to import or export content data, as well as converting it between internal types.
        /// <para/>If set to <c>null</c>, the list is automatically populated with all classes inheriting from <see cref="ContentConverter"/>.
        /// </summary>
        IEnumerable<ContentConverter> ConverterList { get; set; }
    }
}
