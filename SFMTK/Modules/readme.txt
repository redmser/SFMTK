﻿This folder contains all modules to be loaded by SFMTK.
Follow the installation instructions of the downloaded module (mostly placing a folder or .dll file inside of this directory).

A module can extend the behavior of SFMTK, such as by adding new commands, windows, file formats, etc.
If you wish to create your own (and know a bit about programming), check out SFMTK.Modules.IModule in the source code.

Be aware that modules can execute any code on your computer - only load modules from trustworthy sources!
