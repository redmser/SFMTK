﻿//  Uncomment to enable debugging ModulesFound dialog.
//  Shows on startup of SFMTK. Hitting "Load Modules" may add duplicate settings.
//#define DEBUG_MODULESFOUND

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace SFMTK.Modules
{
    /// <summary>
    /// Manages the modules which are currently loaded, how they extend SFMTK, and allows for (un)loading modules.
    /// </summary>
    public class ModuleManager
    {
        //IMP: ModuleManager - ensure that members exposed by IModule are allowed to be null or throw exceptions at all times
        //  without any risk of SFMTK crashing with it

        /// <summary>
        /// Gets the full names of the default modules that get loaded when no ModuleSettings have been initialized yet.
        /// </summary>
        public static List<string> GetDefaultModules() => new List<string>
        {
            "SFMTK.Modules.Network",
            "SFMTK.Modules.Paid",
            "SFMTK.Modules.Paid.SFMCleaner",
            "SFMTK.Modules.Paid.Wizards",
        };

        /// <summary>
        /// The maximum amount of preference pages that can be defined in the <see cref="IModuleSettings"/> interface.
        /// </summary>
        public const int MaxPreferencePages = 256;

        /// <summary>
        /// The maximum amount of command-line tokens that can be defined in the <see cref="IModuleTokens"/> interface.
        /// </summary>
        public const int MaxTokens = 2048;

        /// <summary>
        /// The maximum amount of additional properties that can be defined in the <see cref="IModuleSettings"/> interface.
        /// </summary>
        public const int MaxAdditionalProperties = 2048;

        /// <summary>
        /// The maximum amount of toolbars that can be defined in the <see cref="IModuleToolBars"/> interface.
        /// </summary>
        public const int MaxToolBars = 128;

        /// <summary>
        /// Loads the specified module now, regardless of the loading contexts that the module listens to. Returns whether loading was successful.
        /// </summary>
        /// <param name="module">The module to load.</param>
        /// <param name="context">The context that this module should be loaded for.</param>
        /// <param name="permanent">If set to <c>true</c>, saves this change in the settings as well.</param>
        /// <exception cref="ModuleLoadException">One or more dependencies of a module could not be found or loaded.</exception>
        /// <exception cref="ModulePropertyException">A property specified in a module or interface has an invalid value.</exception>
        public bool LoadModule(IModule module, ModuleLoadingContext context, bool permanent)
        {
            //Module may not be loaded
            bool? success = null;
            if (!this.LoadedModules.Contains(module, ModuleComparer))
            {
                //Are all of our dependencies satisfied?
                if (!AreDependenciesMet(module))
                    throw new ModuleLoadException($"One or more dependencies of {module.GetDisplayText()} could not be found or are not loaded.");

                //First load! Command-line tokens
                if (module is IModuleTokens modtok)
                {
                    //Validate first
                    if (modtok.Tokens == null)
                        ThrowModulePropertyException(modtok, "Tokens is null");

                    var tokens = modtok.Tokens.ToList();
                    if (tokens.Count > MaxTokens)
                        ThrowModulePropertyException(modtok, $"Tokens specifies too many entries ({tokens.Count} > {MaxTokens})");

                    //Register tokens
                    Program.Options.RegisterToken(tokens);
                }

                //Modules with settings
                if (module is IModuleSettings modset)
                {
                    //Set default settings of module
                    modset.SetDefaultSettings();
                }

                //Only add toolbars if of correct type AND load call is on UI
                if (module is IModuleToolBars modtool && context == ModuleLoadingContext.StartupUserInterface)
                {
                    var toolbars = modtool.GetToolBars()?.ToList();
                    if (toolbars == null)
                        toolbars = Reflection.GetSubclassInstances<ToolStrip>(module.GetAssembly()).ToList();

                    if (toolbars.Count > MaxToolBars)
                        ThrowModulePropertyException(modtool, $"ToolBars specifies too many entries ({toolbars.Count} > {MaxToolBars})");

                    foreach (var ts in toolbars)
                    {
                        //BUG: ModuleManager - adding a new toolbar will always make it visible, even if differently defined in the toolbarsettings that get loaded

                        //Add to form
                        Program.MainForm.Controls.Add(ts);

                        //Check if MainForm already finished initializing
                        if (Program.MainForm.ToolBars.Any())
                        {
                            //Already initialized... Late-init me
                            //FIXME: ModuleManager - re-initializing all toolbars like this is not the best way to go
                            //  simply try to re-sort the contextmenu list (which is why we are rebuilding in the first place)
                            //  OR build the full toolbar list only after all modules are loaded
                            Program.MainForm.InitializeToolBars(true);
                        }
                    }
                }

                //Load commands of module
                if (module is IModuleCommands modcom)
                {
                    if (modcom.CommandList == null)
                        modcom.CommandList = Reflection.GetSubclassInstances<Commands.Command>(module.GetAssembly()).ToList();
                    Commands.CommandManager.Commands.AddRange(modcom.CommandList);
                }

                //Load converters of module
                if (module is IModuleConverters modconv)
                {
                    if (modconv.ConverterList == null)
                        modconv.ConverterList = Reflection.GetSubclassInstances<Converters.ContentConverter>(module.GetAssembly()).ToList();
                    Converters.ContentConverter.ConverterList.AddRange(modconv.ConverterList);
                }

                //Load content types of module
                if (module is IModuleContents modcont)
                {
                    if (modcont.ContentList == null)
                        modcont.ContentList = Reflection.GetSubclassInstances<Contents.Content>(module.GetAssembly()).ToList();
                    Contents.Content.ContentList.AddRange(modcont.ContentList);
                }

                //Load the module
                success = module.OnLoad(context);
                if (success.Value)
                {
                    this._loadedModules.Add(module);
                    if (permanent)
                        ModuleManager.SetModuleLoad(module, true);
                }

                //HACK: ModuleManager - detecting if startup procedure
                if (!context.ToString().StartsWith("Startup"))
                {
                    //Do startup procedure loading at once, since module did not load during startup
                    module.OnStartup(ModuleLoadingContext.StartupCommandLine);
                    module.OnStartup(ModuleLoadingContext.StartupUserInterface);
                    Program.MainForm.BackgroundTasks.AddTask(Tasks.BackgroundTask.FromDelegate(() =>
                        module.OnStartup(ModuleLoadingContext.StartupLazyLoaded), $"Loading {module.GetName()}...", false, false));
                }
            }

            //Return success value
            return success ?? true;
        }

        /// <summary>
        /// Returns whether the dependencies of the specified module are satisfied.
        /// </summary>
        public bool AreDependenciesMet(IModule module)
        {
            //No dependencies?
            var deps = module.GetDependencies();
            if (deps == null || !deps.Any())
                return true;

            //All dependencies exist and loaded?
            return deps.All(d =>
            {
                var mod = this.GetModuleByName(d, loadedOnly: true);
                return mod != null;
            });
        }

        /// <summary>
        /// Calls the startup method of the specified module.
        /// </summary>
        /// <param name="module">The module to inform about the current startup step.</param>
        /// <param name="context">Which startup step to inform about. Should be one of the startup values.</param>
        public void StartupModule(IModule module, ModuleLoadingContext context)
        {
            //Module has to be loaded at this stage
            module.OnStartup(context);
        }

        /// <summary>
        /// Unloads the specified module. Returns whether unloading was attempted/successful.
        /// </summary>
        /// <param name="module">The module to unload.</param>
        /// <param name="context">The context that this module should be unloaded for.</param>
        /// <param name="permanent">If set to <c>true</c>, saves this change in the settings as well.</param>
        public bool UnloadModule(IModule module, ModuleUnloadingContext context, bool permanent)
        {
            if (!this.LoadedModules.Contains(module, ModuleComparer))
            {
                UpdateModuleSettings();

                return false;
            }

            //Unload tokens
            if (module is IModuleTokens modtok)
                Program.Options.UnregisterToken(modtok.Tokens);

            //TODO: ModuleManager - would removing everything of a module's namespace/assembly be better?
            //  so instead of removing the previously-added instance (which may be different on next get),
            //  try removing every type that is from a module's namespace or assembly, and is in that list

            //Remove toolstrips
            if (Program.MainForm != null && module is IModuleToolBars modtool)
            {
                foreach (var ts in modtool.GetToolBars().Select(t => t.GetType()))
                    Program.MainForm.RemoveToolBar(ts.GetType());
            }

            //Remove commands
            if (module is IModuleCommands modcom)
            {
                foreach (var c in modcom.CommandList)
                    Commands.CommandManager.Commands.Remove(c);
            }

            //Remove content
            if (module is IModuleContents modcont)
            {
                foreach (var c in modcont.ContentList)
                    Contents.Content.ContentList.Remove(c);
            }

            //Remove converters
            if (module is IModuleConverters modconv)
            {
                foreach (var c in modconv.ConverterList)
                    Converters.ContentConverter.ConverterList.Remove(c);
            }

            //TODO: ModuleManager - remove showwidgetcommands pointing to module's widgets

            //Unload module
            var success = module.OnUnload(context);
            if (success)
            {
                this._loadedModules.Remove(module);

                UpdateModuleSettings();
            }

            return success;



            void UpdateModuleSettings()
            {
                //Update the module setting if needed
                if (permanent)
                {
                    if (context == ModuleUnloadingContext.Uninstall)
                    {
                        //Remove the module setting, since we don't want an unknown module to float around
                        var modname = module.GetName();
                        Properties.Settings.Default.ModuleSettings.Remove(Properties.Settings.Default.ModuleSettings.First(ms => ms.FullName == modname));
                    }
                    else
                    {
                        //Simply unload
                        ModuleManager.SetModuleLoad(module, false);
                    }
                }

                //Uninstalling means to also remove all assembly references
                if (context == ModuleUnloadingContext.Uninstall)
                {
                    this._allModules.Remove(module);

                    //Ensure that all assembly references are unloaded before continuing
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                }
            }
        }

        /// <summary>
        /// Throws a <see cref="ModulePropertyException"/>, where a specific property of a module's interface is set up incorrectly.
        /// </summary>
        /// <typeparam name="TModule">The interface type that contains the property in question.</typeparam>
        /// <param name="module">The module instance where the error is happening.</param>
        /// <param name="error">The error string to display. Should contain the name of the property causing the error.</param>
        public static void ThrowModulePropertyException<TModule>(TModule module, string error) where TModule : IModule
            => throw new ModulePropertyException($"Could not load {typeof(TModule).Name} of {module.GetName()}: {error}");

        /// <summary>
        /// Called every time a certain loading context has been reached, loading all modules that wait for this context and may load based on the user settings.
        /// </summary>
        /// <param name="context">The context that has been reached.</param>
        public void ReachedStartupStep(ModuleLoadingContext context) => this.ReachedStartupStep(context, Properties.Settings.Default.ModuleSettings);

        /// <summary>
        /// Called every time a certain loading context has been reached, loading all modules that wait for this context and may load based on the specified settings.
        /// </summary>
        /// <param name="context">The startup step that has been reached.</param>
        /// <param name="settings">The settings that decide which modules may load.</param>
        public void ReachedStartupStep(ModuleLoadingContext context, IEnumerable<ModuleSetting> settings)
        {
            foreach (var mod in this.AllModules)
            {
                //Check if setting tells it to load too
                var sett = settings.FirstOrDefault(s => s.FullName == mod.GetName());
                var validsetting = sett != null && sett.Load;
                if (mod.GetLoadingContext() == context && validsetting)
                {
                    //Check dependencies
                    if (!AreDependenciesMet(mod))
                    {
                        sett.Load = false;
                        continue;
                    }
                    this.LoadModule(mod, context, false);
                }

                //Do startup call - check if the module is already loaded
                if (validsetting || mod.IsLoaded())
                    this.StartupModule(mod, context);
            }
        }

        /// <summary>
        /// Gets a module given its full name.
        /// </summary>
        /// <param name="name">The full name of the module, excluding the .dll extension.</param>
        /// <param name="loadedOnly">If set to <c>true</c>, only checks for modules that are currently loaded.</param>
        public IModule GetModuleByName(string name, bool loadedOnly = false)
        {
            var modules = loadedOnly ? this._loadedModules : this._allModules;
            return modules.FirstOrDefault(m => m.GetName() == name);
        }

        /// <summary>
        /// Gets the currently loaded modules, in their loading order.
        /// </summary>
        public IReadOnlyList<IModule> LoadedModules => this._loadedModules;
        private List<IModule> _loadedModules = new List<IModule>();

        /// <summary>
        /// Gets the cached list of all modules found in the modules directory, in their loading order.
        /// </summary>
        public IReadOnlyList<IModule> AllModules => this._allModules;
        private List<IModule> _allModules = new List<IModule>();

        /// <summary>
        /// Refreshes the full list of modules. No modules are loaded, only new modules are found and old modules are removed from the list.
        /// </summary>
        /// <param name="reloadAssemblies">If set to <c>true</c>, reloads the assemblies of known modules as well.</param>
        /// <exception cref="DirectoryNotFoundException">The modules directory was not found, no modules were loaded.</exception>
        /// <remarks>
        /// This does not update the list of loaded mods - invalid modules may still be found in this list.
        /// </remarks>
        public void RefreshModulesList(bool reloadAssemblies) => this.RefreshModulesList(reloadAssemblies, false);

        internal void RefreshModulesList(bool reloadAssemblies, bool firstload)
        {
            var modpath = IOHelper.GetSFMTKDirectory(Constants.ModulesPath);
            if (!Directory.Exists(modpath))
                throw new DirectoryNotFoundException("The modules directory was not found, no modules were loaded.");

            if (!firstload)
            {
                //Initialize module settings property entry in here
                Properties.Settings.UpdateDefaultModuleSettings();
            }

            var modules = this._allModules;
            foreach (var file in Directory.EnumerateFiles(modpath, "*.dll", SearchOption.AllDirectories))
            {
                //Check if the assembly exposes a class with IModule
                try
                {
                    //If module is already known, don't reload if not forced to
                    var path = Path.Combine(Application.StartupPath, file);

                    //Don't reload if user doesn't want or if the file is already loaded.
                    //FIXME: ModuleManager - is the path equality check with the assembly really needed? Feels like getting fullname is way easier
                    if (!firstload && (!reloadAssemblies || modules.Any(m => IOHelper.PathsEqual(m.GetAssembly().CodeBase, path))))
                        continue;

                    //IMP: ModuleManager - do not give full permissions to a loaded module assembly (e.g. reflection, process.start) without prompting user first!
                    //  -> have a safe interface that can be accessed by modules, powered by SFMTK, to do these things without losing control
                    //IMP: ModuleManager - in order to allow unloading specific assemblies, load each assembly into its own AppDomain, and later on unload that AppDomain
                    /*
                        AppDomain dom = AppDomain.CreateDomain("some");     
                        AssemblyName assemblyName = new AssemblyName();
                        assemblyName.CodeBase = pathToAssembly;
                        Assembly assembly = dom.Load(assemblyName);
                        Type [] types = assembly.GetTypes();
                        AppDomain.Unload(dom);
                    */
                    //  -> alternative Assembly.Load(File.ReadAllBytes(filePath)); screams like memory leaking though
                    var ass = Assembly.LoadFile(path);
                    var types = ass.GetTypes();
                    var modtype = types.SingleOrDefault(t => typeof(IModule).IsAssignableFrom(t));
                    if (modtype != null)
                    {
                        //Reload first
                        modules.RemoveAll(m => IOHelper.PathsEqual(m.GetAssembly().CodeBase, path));
                        modules.Add((IModule)Activator.CreateInstance(modtype));
                    }
                }
                catch (BadImageFormatException)
                {
                    //The library may either not have IModule, it may be a reference of another module, or it may be native code
                }
            }

            //Order our new module list by dependencies
            this._allModules = ReorderModuleList(modules);

            //TODO: ModuleManager - remove all module instances that are no longer in the module folder (keep the settings though!)
        }

        private static List<IModule> ReorderModuleList(List<IModule> modules)
        {
            var depth = new Dictionary<IModule, int>();
            foreach (var module in modules)
            {
                var deps = module.GetDependencies();
                var maxdepth = GetDepth(module, modules);
                depth.Add(module, maxdepth);
            }
            return depth.Select(d => new KeyValuePair<IModule, int>(d.Key, d.Value)).OrderBy(d => d.Value).Select(d => d.Key).ToList();
        }

        private static int GetDepth(IModule mod, List<IModule> modules, int step = 0)
        {
            var deps = mod?.GetDependencies();
            if (deps == null || deps.Length == 0)
                return step;
            return deps.Max(d => GetDepth(modules.FirstOrDefault(m => m.GetName() == d), modules, ++step));
        }

        /// <summary>
        /// Checks for any new modules that aren't part of the known modules list. If any are found, a dialog is opened which allows the user to select which to load.
        /// <para/>The selected modules will be loaded after closing the dialog and the settings will be updated.
        /// </summary>
        public void CheckNewModules()
        {
            var newmods = this.CheckNewModules(Properties.Settings.Default.ModuleSettings, true);
            Properties.Settings.Default.ModuleSettings.AddRange(newmods);
        }

        /// <summary>
        /// Checks for any modules that aren't part of the specified known modules list. If any are found, a dialog is opened which allows the user to select which to load.
        /// <para/>The selected modules will optionally be loaded after closing the dialog and the newly created settings that can be stored will be returned.
        /// </summary>
        /// <param name="settings">Which modules are known to SFMTK.</param>
        /// <param name="loadModules">Whether to load the modules after the user chose to load them.</param>
        public IEnumerable<ModuleSetting> CheckNewModules(IEnumerable<ModuleSetting> settings, bool loadModules)
        {
#if DEBUG_MODULESFOUND
            var newmods = this.AllModules.Select(m => new ModuleSetting(m.GetName(), true)).ToList();
#else
            var newmods = NewModules().ToList();
            if (!newmods.Any())
                return new ModuleSetting[] { };
#endif

            //Open dialog for it
            return FoundModules(newmods, loadModules);



            IEnumerable<ModuleSetting> NewModules()
            {
                foreach (var mod in this.AllModules)
                {
                    var name = mod.GetName();
                    if (!settings.Any(s => s.FullName == name))
                        yield return new ModuleSetting(name, true);
                }
            }
        }

        private IEnumerable<ModuleSetting> FoundModules(IEnumerable<ModuleSetting> newmods, bool loadModules)
        {
            var found = new Forms.ModulesFound(newmods);
            var res = found.ShowDialog(Program.MainForm);
            if (res == DialogResult.OK)
            {
                var sel = found.AllSettings.Where(s => s.Load);
                if (loadModules)
                {
                    //Load the selected modules
                    foreach (var mod in sel.Select(s => this.GetModuleByName(s.FullName)))
                        this.LoadModule(mod, ModuleLoadingContext.NewModule, false);
                }
                return found.AllSettings;
            }
            else
            {
                //Cancelled? Return new settings with load set to false, so the modules are known, but not loaded!
                foreach (var sett in newmods)
                    sett.Load = false;
                return newmods;
            }
        }

        /// <summary>
        /// Unloads all loaded modules.
        /// </summary>
        /// <param name="context">The context that this module should be unloaded for.</param>
        /// <param name="permanent">If set to <c>true</c>, this change is saved in the properties as well.
        public void UnloadModules(ModuleUnloadingContext context, bool permanent = false)
        {
            foreach (var mod in this.LoadedModules.ToList())
                this.UnloadModule(mod, context, permanent);
        }

        /// <summary>
        /// Loads or reloads all modules based on the current module settings.
        /// </summary>
        /// <param name="unload">The unloading context of this reload operation.</param>
        /// <param name="load">The loading context of this reload operation.</param>
        public void ReloadModules(ModuleUnloadingContext unload, ModuleLoadingContext load) => this.ReloadModules(Properties.Settings.Default.ModuleSettings, unload, load);

        /// <summary>
        /// Loads or reloads the modules that correspond to the specified module settings. Use null to (re)load all known modules instead.
        /// </summary>
        /// <param name="settings">The list of ModuleSettings that determine which modules should be loaded.</param>
        /// <param name="unload">The unloading context of this reload operation.</param>
        /// <param name="load">The loading context of this reload operation.</param>
        public void ReloadModules(IEnumerable<ModuleSetting> settings, ModuleUnloadingContext unload, ModuleLoadingContext load)
        {
            //Unload all modules
            this.UnloadModules(unload, false);
            //TODO: ModuleManager - what if a module didn't want to unload here?

            IEnumerable<IModule> enumerator;
            if (settings == null)
            {
                //Go through all modules
                enumerator = this.AllModules;
            }
            else
            {
                //Filter based on settings
                enumerator = settings.Where(s => s.Load).Select(s => this.AllModules.FirstOrDefault(m => s.FullName == m.GetName())).Where(m => m != null);
            }

            //Load modules (either by settings, or all)
            foreach (var mod in enumerator)
                this.LoadModule(mod, load, false);
        }

        /// <summary>
        /// Sets whether to load the module specified by its setting, updating the properties value. Returns whether the entry was not found in the settings list already.
        /// </summary>
        /// <param name="setting">The ModuleSetting to look for.</param>
        /// <param name="value">Whether the module should be loaded.</param>
        public static bool SetModuleLoad(ModuleSetting setting, bool value) => SetModuleLoad(setting.FullName, value);

        /// <summary>
        /// Sets whether to load the module specified by an instance, updating the properties value. Returns whether the entry was not found in the settings list already.
        /// </summary>
        /// <param name="module">The module to look for.</param>
        /// <param name="value">Whether the module should be loaded.</param>
        public static bool SetModuleLoad(IModule module, bool value) => SetModuleLoad(module.GetName(), value);

        /// <summary>
        /// Sets whether to load the module specified by its name, updating the properties value. Returns whether the entry was not found in the settings list already.
        /// </summary>
        /// <param name="name">The name of the module to look for.</param>
        /// <param name="value">Whether the module should be loaded.</param>
        public static bool SetModuleLoad(string name, bool value)
        {
            //Find module setting in list
            var found = Properties.Settings.Default.ModuleSettings.FirstOrDefault(s => s.FullName == name);
            if (found == null)
            {
                //Did not find, add new!
                Properties.Settings.Default.ModuleSettings.Add(new ModuleSetting(name, value));
                return true;
            }

            //Found, update!
            found.Load = value;
            return false;
        }

        /// <summary>
        /// Gets the modules of the specified scope.
        /// </summary>
        public IEnumerable<IModule> GetModules(ModuleScope scope)
        {
            //No modules
            var list = new List<IModule>();
            if (scope == ModuleScope.None)
                return list;

            //All modules
            if (scope == ModuleScope.AllModules)
            {
                list.AddRange(this.AllModules);
                return list;
            }

            if (scope.HasFlag(ModuleScope.LoadedModules))
                list.AddRange(this.LoadedModules);

            if (scope.HasFlag(ModuleScope.UnloadedModules))
                list.AddRange(this.AllModules.Except(this.LoadedModules));

            return list;
        }

        private FileSystemWatcher _watcher;

        internal void StartDirectoryWatcher()
        {
            var modpath = IOHelper.GetSFMTKDirectory(Constants.ModulesPath);
            if (!Directory.Exists(modpath))
                return;
            _watcher = IOHelper.StartDirectoryWatcher(modpath, includeSubdirectories: false);
            _watcher.Created += CreatedModuleDirectory;
            _watcher.Deleted += DeletedModuleDirectory;
        }

        internal void StopDirectoryWatcher()
        {
            if (_watcher != null)
                IOHelper.StopDirectoryWatcher(_watcher);
        }

        //UNTESTED: ModuleManager - found a new module, or deleted a known module, at runtime

        private void CreatedModuleDirectory(object sender, FileSystemEventArgs e)
        {
            //Is it a module directory?
            if (!IsModuleDirectory(e.FullPath))
                return;

            //Tell user about the found modules once he's back
            this._foundNames.Add(e.Name);
            Program.MainForm.RemindMe(WatchFoundNewModule);
        }

        private void WatchFoundNewModule()
        {
            var settings = this._foundNames.Select(n => new ModuleSetting(n, true));
            this._foundNames.Clear();

            FoundModules(settings, true);
        }

        private readonly List<string> _foundNames = new List<string>();

        private void DeletedModuleDirectory(object sender, FileSystemEventArgs e)
        {
            //Check if a module with this name exists
            var mod = this.GetModuleByName(e.Name);
            if (mod == null)
                return;

            //Remove that module + its setting
            this.UnloadModule(mod, ModuleUnloadingContext.Uninstall, true);
        }

        private static bool IsModuleDirectory(string path)
        {
            path = IOHelper.GetStandardizedPath(path);

            //Is a directory?
            if (IOHelper.IsFileOrFolder(path) != FileAttributes.Directory)
                return false;

            //Has DLL files in it?
            return Directory.EnumerateFiles(path, "*.dll").Any();
        }

        /// <summary>
        /// Gets the default equality comparer for comparing two modules.
        /// </summary>
        public static IEqualityComparer<IModule> ModuleComparer { get; } = new ModuleEqualityComparer();

        /// <summary>
        /// Gets the default equality comparer for comparing two module settings.
        /// </summary>
        public static IEqualityComparer<ModuleSetting> ModuleSettingComparer { get; } = new ModuleSettingEqualityComparer();

        /// <summary>
        /// Compares modules by their name.
        /// </summary>
        private class ModuleEqualityComparer : IEqualityComparer<IModule>
        {
            /// <summary>
            /// Determines whether the specified objects are equal.
            /// </summary>
            /// <param name="x">The first object of type <paramref name="T" /> to compare.</param>
            /// <param name="y">The second object of type <paramref name="T" /> to compare.</param>
            /// <returns>
            /// true if the specified objects are equal; otherwise, false.
            /// </returns>
            public bool Equals(IModule x, IModule y) => x.GetName() == y.GetName();

            /// <summary>
            /// Returns a hash code for this instance.
            /// </summary>
            /// <param name="obj">The object.</param>
            /// <returns>
            /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
            /// </returns>
            public int GetHashCode(IModule obj) => obj.GetName().GetHashCode();
        }

        /// <summary>
        /// Compares module settings by their name.
        /// </summary>
        private class ModuleSettingEqualityComparer : IEqualityComparer<ModuleSetting>
        {
            /// <summary>
            /// Determines whether the specified objects are equal.
            /// </summary>
            /// <param name="x">The first object of type <paramref name="T" /> to compare.</param>
            /// <param name="y">The second object of type <paramref name="T" /> to compare.</param>
            /// <returns>
            /// true if the specified objects are equal; otherwise, false.
            /// </returns>
            public bool Equals(ModuleSetting x, ModuleSetting y) => x.FullName == y.FullName;

            /// <summary>
            /// Returns a hash code for this instance.
            /// </summary>
            /// <param name="obj">The object.</param>
            /// <returns>
            /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
            /// </returns>
            public int GetHashCode(ModuleSetting obj) => obj.FullName.GetHashCode();
        }
    }

    [Flags]
    public enum ModuleScope
    {
        /// <summary>
        /// Object is not a module or not known to SFMTK.
        /// </summary>
        None = 0,
        /// <summary>
        /// The module is loaded.
        /// </summary>
        LoadedModules = 1,
        /// <summary>
        /// The module is not loaded, but only known.
        /// </summary>
        UnloadedModules = 2,
        /// <summary>
        /// Object is a module.
        /// </summary>
        AllModules = LoadedModules | UnloadedModules
    }
}
