﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SFMTK.Modules
{
    /// <summary>
    /// A simple collection of <see cref="ModuleSetting"/>s.
    /// </summary>
    [System.Serializable]
    public class ModuleSettingCollection : ICollection<ModuleSetting>
    {
        private readonly List<ModuleSetting> _backingList = new List<ModuleSetting>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleSettingCollection"/> class.
        /// </summary>
        public ModuleSettingCollection()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleSettingCollection"/> class.
        /// </summary>
        /// <param name="collection">The collection to initialize the list with.</param>
        public ModuleSettingCollection(IEnumerable<ModuleSetting> collection) : this()
        {
            this.AddRange(collection);
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count => this._backingList.Count;

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly => false;

        /// <summary>
        /// Gets or sets the <see cref="ModuleSetting"/> with the specified full name.
        /// </summary>
        public ModuleSetting this[string name]
        {
            get => this._backingList.FirstOrDefault(s => s.FullName == name);
            set => this[name] = value;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(ModuleSetting item)
        {
            if (this.Any(s => s.FullName == item.FullName))
                return;

            this._backingList.Add(item);
        }

        /// <summary>
        /// Adds a collection of <see cref="ModuleSetting"/> to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="collection">The collection to add.</param>
        public void AddRange(IEnumerable<ModuleSetting> collection)
        {
            foreach (var sett in collection)
                this.Add(sett);
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear() => this._backingList.Clear();

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(ModuleSetting item) => this._backingList.Contains(item);

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.Generic.ICollection`1" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins.</param>
        public void CopyTo(ModuleSetting[] array, int arrayIndex) => this._backingList.CopyTo(array, arrayIndex);

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(ModuleSetting item) => this._backingList.Remove(item);

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// An enumerator that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<ModuleSetting> GetEnumerator() => this._backingList.GetEnumerator();

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator() => this._backingList.GetEnumerator();
    }
}
