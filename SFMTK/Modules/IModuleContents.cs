﻿using System.Collections.Generic;
using SFMTK.Contents;

namespace SFMTK.Modules
{
    /// <summary>
    /// Allows a module to define new content types. If they should be shown in the New Content dialog, make them inherit <see cref="IInstantiatable"/>.
    /// <para/>By adding this interface and setting <see cref="ContentList"/> to <c>null</c>,
    /// all classes inheriting <see cref="Content"/> will automatically be recognized as commands, if they are in the <c>Contents</c> namespace.
    /// </summary>
    public interface IModuleContents : IModule
    {
        /// <summary>
        /// Gets the default content instances to be registered for this module. If they should be shown in the New Content dialog, make them inherit <see cref="IInstantiatable"/>.
        /// <para/>If set to <c>null</c>, the list is automatically populated with all classes inheriting from <see cref="Content"/>.
        /// </summary>
        IEnumerable<Content> ContentList { get; set; }
    }
}
