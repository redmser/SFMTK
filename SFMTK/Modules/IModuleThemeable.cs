﻿using System.ComponentModel;
using System.Windows.Forms;

namespace SFMTK.Modules
{
    /// <summary>
    /// The module contains controls which should always be themed accodingly. Access <see cref="UITheme.SelectedTheme"/> to retrieve properties about the theme.
    /// </summary>
    public interface IModuleThemeable : IModule
    {
        //NYI: IModuleThemeable - allow defining new properties which can be themed (maybe edit the JSON deserialization a bit to allow collecting
        //  unknown properties in a class instance, then sorting that by modules? may be stupid to work with in the end)
        //TODO: IModuleThemeable - if graph theming is changed, may add it to another interface

        /// <summary>
        /// Applies this theme to the specified control which is part of the specified form.
        /// Use this call for anything which is part of the form itself, mostly of type <see cref="Control"/>.
        /// </summary>
        /// <param name="parent">The parent control on which this Component is on.</param>
        /// <param name="component">The component to apply the theme to.</param>
        void ApplyControlTheme(Control parent, Component component);

        /// <summary>
        /// Should contain any theming which is not "default", meaning the standard color of this type is not
        /// equal to the expected system default and will be re-themed at all times.
        /// </summary>
        /// <param name="parent">The parent control on which this Component is on.</param>
        /// <param name="component">The component to apply the theme to.</param>
        void ApplyNonDefaultControlTheme(Control parent, Component component);

        /// <summary>
        /// Applies the system-default theming to the specified component which is part of the specified form.
        /// Use this call for any control which needs further properties set to return to its system-default theming.
        /// </summary>
        /// <param name="parent">The parent control on which this Component is on.</param>
        /// <param name="component">The component to apply the theme to.</param>
        void ApplySystemControlTheme(Control parent, Component component);

        /// <summary>
        /// Applies this theme to the specified component which is part of the specified form.
        /// Use this call for anything which is part of the components list of the form (such as ToolTip).
        /// </summary>
        /// <param name="parent">The parent control on which this Component is on.</param>
        /// <param name="component">The component to apply the theme to.</param>
        void ApplyComponentTheme(Control parent, Component component);
    }
}
