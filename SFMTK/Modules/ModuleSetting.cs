﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SFMTK.Modules
{
    /// <summary>
    /// A config setting on the state of a module.
    /// </summary>
    [Serializable]
    public class ModuleSetting
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleSetting"/> class.
        /// </summary>
        public ModuleSetting()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleSetting"/> class.
        /// </summary>
        public ModuleSetting(string fullname, bool load = true)
        {
            this.FullName = fullname;
            this.Load = load;
        }

        /// <summary>
        /// Gets or sets the full name of the module that this setting is assigned to.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets whether this module should be loaded when its loading context applies.
        /// </summary>
        public bool Load { get; set; }

        /// <summary>
        /// Gets the module instance with this name from the main module manager.
        /// </summary>
        public IModule Module => Program.MainMM.GetModuleByName(this.FullName);

        /// <summary>
        /// Returns whether this <see cref="ModuleSetting"/> should be shown as a top-level node in any list of settings.
        /// </summary>
        /// <param name="fulllist">A list of all module settings that are currently known.</param>
        public bool GetTopLevel(IEnumerable<ModuleSetting> fulllist = null)
        {
            //Show <UNKNOWN> module top-level
            if (this.Module == null)
                return true;

            //Show module with no parent, or a parent that is not in the full list
            var parent = this.Module.GetParent();
            return string.IsNullOrWhiteSpace(parent) || !(fulllist ?? Properties.Settings.Default.ModuleSettings).Any(s => s.FullName == parent);
        }

        /// <summary>
        /// Gets all children of this instance.
        /// </summary>
        /// <param name="fulllist">A list of all module settings that are currently known.</param>
        public IEnumerable<ModuleSetting> GetChildren(IEnumerable<ModuleSetting> fulllist = null)
        {
            return (fulllist ?? Properties.Settings.Default.ModuleSettings).Where(m => m.Module != null && m.Module.GetParent() == this.FullName);
        }

        /// <summary>
        /// Gets or sets the action that this ModuleSetting will be doing when applying the settings.
        /// </summary>
        public ModuleSettingAction Action
        {
            get => this._action;
            set => this._action = value;
        }
        [NonSerialized]
        private ModuleSettingAction _action;

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => this.GetDisplayText();
    }

    /// <summary>
    /// The action that a ModuleSetting will be doing when applying the settings.
    /// </summary>
    public enum ModuleSettingAction
    {
        /// <summary>
        /// No action.
        /// </summary>
        None,
        /// <summary>
        /// Toggles the loaded state.
        /// </summary>
        Toggle,
        /// <summary>
        /// Loads the module for the first time.
        /// </summary>
        Install,
        /// <summary>
        /// Unloads the module and removes its associated files.
        /// </summary>
        Uninstall
    }
}
