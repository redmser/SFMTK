﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using SFMTK.Controls.PreferencePages;

namespace SFMTK.Modules
{
    /// <summary>
    /// This module adds new settings keys using its own App.config file.
    /// </summary>
    public interface IModuleSettings : IModule
    {
        /// <summary>
        /// Sets the default settings values for any keys that can't easily be set otherwise (such as instances of a class).
        /// <para/>Compare if the value is null to avoid overriding an already-set value.
        /// </summary>
        void SetDefaultSettings();

        /// <summary>
        /// Returns the additional property controls to add to existing preference pages. Key is the path to the PreferencePage, Value is the user control to append.
        /// </summary>
        Dictionary<string, Control> GetAdditionalProperties();

        /// <summary>
        /// Returns the preference pages to add to the settings dialog. Use instances deriving from the <see cref="PreferencePage"/> class to create new pages.
        /// <para/>Be sure to set the unique properties to customize the page's display in the tree view.
        /// </summary>
        IEnumerable<PreferencePage> GetPages();

        /// <summary>
        /// Gets an instance pointing to the default settings instance of this module.
        /// This is used to detect property changes in the Preferences.
        /// <para/>Mostly points to an instance of your module's custom <see cref="Properties.Settings.Default"/>.
        /// </summary>
        INotifyPropertyChanged CustomSettings { get; }
    }
}
