﻿using BrightIdeasSoftware;

namespace SFMTK.Modules
{
    /// <summary>
    /// <see cref="ClusteringStrategy"/> for loaded state of <see cref="ModuleSetting"/>s.
    /// </summary>
    public class ModuleSettingLoadedClusteringStrategy : ClusteringStrategy
    {
        public override object GetClusterKey(object model)
        {
            if (!(model is ModuleSetting ms))
                return false;
            return ms.Module.IsLoaded();
        }

        public override string GetClusterDisplayLabel(ICluster cluster)
        {
            var loaded = (bool)cluster.ClusterKey;
            var prefix = "Not loaded";
            if (loaded)
                prefix = "Loaded";
            return $"{prefix} ({StringHelper.GetPlural(cluster.Count, "module")})";
        }
    }
}
