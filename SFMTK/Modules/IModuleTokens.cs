﻿using OrderCLParser;
using System.Collections.Generic;

namespace SFMTK.Modules
{
    /// <summary>
    /// Allows a module to define command line tokens (uses the <see cref="OrderCLParser"/> library).
    /// Your module must load at the <see cref="ModuleLoadingContext.StartupCommandLine"/> loading context in order to initialize the tokens in advance.
    /// </summary>
    public interface IModuleTokens : IModule
    {
        /// <summary>
        /// Gets the tokens which will be registered when loading the module. Usually, <see cref="CLFlag"/> and <see cref="CLOption"/> are used.
        /// <para/>Avoid implementing this property with the expression-bodied syntax, as references may get lost.
        /// </summary>
        IEnumerable<CLToken> Tokens { get; }

        /// <summary>
        /// Called when a positional token defined by this module was found while parsing the command-line arguments during the startup procedure.
        /// </summary>
        /// <param name="token">The token that was found by the parser.</param>
        void ParseCommandToken(CLToken token);

        /// <summary>
        /// Called when a non-positional token defined by this module was found while parsing the command-line arguments during the startup procedure.
        /// <para/>It is consumed from the list of tokens in the command-line after returning from this method.
        /// </summary>
        /// <param name="token">The token that was found by the parser.</param>
        void ParseOptionToken(CLToken token);
    }
}
