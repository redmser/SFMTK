﻿using System.Linq;
using System.Reflection;

namespace SFMTK.Modules
{
    /// <summary>
    /// Extension methods for modules.
    /// </summary>
    public static class ModuleExtensions
    {
        /// <summary>
        /// Returns whether this module is loaded.
        /// </summary>
        /// <param name="module">The module to check.</param>
        /// <param name="manager">Manager to check if the module is loaded in. Use null to fallback to the main form's module manager.</param>
        public static bool IsLoaded(this IModule module, ModuleManager manager = null) => (manager ?? Program.MainMM).LoadedModules.Contains(module, ModuleManager.ModuleComparer);

        /// <summary>
        /// Returns the assembly that this module is loaded from.
        /// </summary>
        public static Assembly GetAssembly(this IModule module) => module.GetType().Assembly;

        /// <summary>
        /// Returns the base namespace of the module.
        /// </summary>
        public static string GetNamespace(this IModule module) => module.GetType().Namespace;

        /// <summary>
        /// Returns the full name of this module. Defined in assembly.
        /// </summary>
        public static string GetName(this IModule module) => module.GetAssemblyName().Name;

        /// <summary>
        /// Returns the file version of this module. Defined in assembly.
        /// </summary>
        public static string GetVersion(this IModule module)
        {
            var version = module.GetVersionInfo().FileVersion;
            if (string.IsNullOrWhiteSpace(version))
                return version;

            //Exclude the build number
            return version.Substring(0, version.LastIndexOf('.'));
        }

        /// <summary>
        /// Returns the company or individual that created this module. Defined in assembly.
        /// </summary>
        public static string GetCompany(this IModule module) => module.GetVersionInfo().CompanyName;

        /// <summary>
        /// Returns the description of this module (if any). Defined in assembly.
        /// </summary>
        public static string GetDescription(this IModule module) => module.GetAssembly()
            .GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false)
            .OfType<AssemblyDescriptionAttribute>()
            .FirstOrDefault()?.Description;

        /// <summary>
        /// Returns the title of this module (if any), used as the display text. Defined in assembly.
        /// </summary>
        public static string GetTitle(this IModule module) => module.GetAssembly()
            .GetCustomAttributes(typeof(AssemblyTitleAttribute), false)
            .OfType<AssemblyTitleAttribute>()
            .FirstOrDefault()?.Title;

        /// <summary>
        /// Returns a string used to display this <see cref="ModuleSetting"/> instance.
        /// </summary>
        public static string GetDisplayText(this ModuleSetting ms)
        {
            var mod = ms.Module;
            return mod?.GetTitle() ?? $"{ms.FullName} <UNKNOWN>";
        }


        /// <summary>
        /// Returns a string used to display this <see cref="IModule"/> instance.
        /// If possible, use the <see cref="ModuleSetting"/> overload for more precise results.
        /// </summary>
        public static string GetDisplayText(this IModule module)
        {
            return module?.GetTitle() ?? module?.GetName() ?? "<UNKNOWN MODULE>";
        }

        /// <summary>
        /// Returns the AssemblyName object of this module's assembly.
        /// </summary>
        private static AssemblyName GetAssemblyName(this IModule module) => module.GetAssembly().GetName();

        /// <summary>
        /// Returns the FileVersionInfo object of this module's assembly.
        /// </summary>
        private static System.Diagnostics.FileVersionInfo GetVersionInfo(this IModule module)
            => System.Diagnostics.FileVersionInfo.GetVersionInfo(module.GetAssembly().Location);
    }
}
