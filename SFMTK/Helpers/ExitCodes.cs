﻿namespace SFMTK
{
    /// <summary>
    /// Helper for common exit codes.
    /// </summary>
    public enum ExitCodes
    {
        None = 0,
        Success = 0,
        InvalidFunction = 1,
        FileNotFound = 2,
        PathNotFound = 3,
        BadEnvironment = 10,
        ModuleNotFound = 126,
        MultipleFaultViolation = 640
    }
}
