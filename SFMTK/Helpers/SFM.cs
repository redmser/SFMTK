﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Win32;
using SFMTK.Data;

namespace SFMTK
{
    /// <summary>
    /// Common interactions with SFM's data files and file system.
    /// </summary>
    public static class SFM
    {
        //TODO: SFM - methods for I/O in sfm dir context:
        //  - input path to file, get list of mods in which this file exists (in correct order)
        //  - input directory, get list of mods in which this directory exists (in correct order)
        
        /// <summary>
        /// Base SFM path, containing folders such as game or _CommonRedist. Guaranteed to use forward slashes and ends with a slash.
        /// </summary>
        public static string BasePath
        {
            get => Properties.Settings.Default.SFMPath;
            set => Properties.Settings.Default.SFMPath = value;
        }

        /// <summary>
        /// Called when the base path has been updated. Raises the <see cref="BasePathChanged"/> after reloading the gameinfo.txt file.
        /// </summary>
        public static void UpdateBasePath()
        {
            ReloadGameInfo();
            SFM.BasePathChanged?.Invoke(null, EventArgs.Empty);
        }

        /// <summary>
        /// Reloads the game info file. Does not raise the <see cref="BasePathChanged"/> event, which informs other controls about new mod information.
        /// </summary>
        public static void ReloadGameInfo()
        {
            //Update gameinfo path
            try
            {
                GameInfo = new GameInfo(GameInfoPath);
            }
            catch (FileNotFoundException)
            {
                //Invalid path
                GameInfo = null;
            }
        }

        /// <summary>
        /// Occurs when the BasePath property changed value.
        /// </summary>
        public static event EventHandler BasePathChanged;

        /// <summary>
        /// Gets the path to steam's common directory, derived from SFM's base path. Guaranteed to use forward slashes and ends with a slash.
        /// </summary>
        public static string CommonPath => BasePath.Substring(0, BasePath.Length - 16);

        /// <summary>
        /// Gets the SFM game directory, containing all mods and SourceFilmmaker.exe. Guaranteed to use forward slashes and ends with a slash.
        /// </summary>
        public static string GamePath => BasePath + "game/";

        /// <summary>
        /// Gets the SFM binaries directory, containing most misc executables. Guaranteed to use forward slashes and ends with a slash.
        /// </summary>
        public static string BinPath => GamePath + "bin/";

        /// <summary>
        /// Gets the SFM usermod directory, the default mod folder, containing the gameinfo.txt to be read. Guaranteed to use forward slashes and ends with a slash.
        /// </summary>
        public static string UsermodPath => GamePath + "usermod/";

        /// <summary>
        /// Gets the SFM gameinfo.txt path inside of the usermod directory. Guaranteed to use forward slashes.
        /// </summary>
        public static string GameInfoPath => UsermodPath + "gameinfo.txt";

        /// <summary>
        /// Gets the path to SFM's executable file, located in the game directory.
        /// </summary>
        public static string ExecutablePath => GamePath + "sfm.exe";

        /// <summary>
        /// Gets the current gameinfo data. May be null if an invalid SFM path is set or the gameinfo.txt can't be found.
        /// </summary>
        public static GameInfo GameInfo { get; private set; }

        /// <summary>
        /// Gets whether all paths and data structures are correctly set up for regular SFM use.
        /// </summary>
        public static bool ValidPaths => !string.IsNullOrWhiteSpace(BasePath) && GameInfo != null;

        /// <summary>
        /// Returns the specified path (file or directory) relative to the mod's directory.
        /// </summary>
        /// <param name="path">Absolute path to the file (may use windows formatting).</param>
        public static string GetLocalPath(string path) => GetLocalPath(path, RelativeTo.ModDir);

        /// <summary>
        /// Returns the specified path (file or directory) relative to the specified directory.
        /// </summary>
        /// <param name="path">Absolute path to the file (may use windows formatting).</param>
        /// <param name="relative">Which folder this path should be relative to.</param>
        public static string GetLocalPath(string path, RelativeTo relative)
        {
            string rel;
            switch (relative)
            {
                case RelativeTo.GameDir:
                    rel = GamePath;
                    break;
                case RelativeTo.SFMDir:
                    rel = BasePath;
                    break;
                case RelativeTo.ModDir:
                    if (string.IsNullOrWhiteSpace(path))
                        return path;

                    //Get name of mod directory
                    path = IOHelper.GetStandardizedPath(path);
                    path = path.Replace(GamePath, string.Empty);
                    return path.Substring(path.IndexOf('/') + 1);
                default:
                    throw new ArgumentOutOfRangeException(nameof(relative), relative, null);
            }
            return GetLocalPath(path, rel);
        }

        /// <summary>
        /// Returns the specified path (file or directory) relative to the specified directory.
        /// </summary>
        /// <param name="path">Absolute path to the file (may use windows formatting).</param>
        /// <param name="relative">Relative directory (must be standardized).</param>
        public static string GetLocalPath(string path, string relative)
        {
            if (string.IsNullOrEmpty(relative))
                return path;

            path = IOHelper.GetStandardizedPath(path);
            return path.Replace(relative, string.Empty);
        }

        /// <summary>
        /// Gets whether the specified path is inside of SFM's folder structure (starting in the SourceFilmmaker folder itself).
        /// </summary>
        public static bool IsInSFMFolder(string path) => IOHelper.GetStandardizedPath(path).Contains(SFM.BasePath);

        /// <summary>
        /// Returns the full path from the specified local path. Returns null if unable to get the full path from what was specified.
        /// </summary>
        /// <param name="path">Local path to the file. May only start in SFM's directory up until first level game directory.</param>
        /// <param name="loadedOnly">Whether to only return paths to files whose mod is enabled.</param>
        /// <param name="game">The mod name in which this file is supposed to exist in. If null, it is tried to find the file in each loaded mod instead (ordered as in gameinfo).</param>
        public static string GetFullPath(string path, string game = null, bool loadedOnly = false)
        {
            if (string.IsNullOrWhiteSpace(path))
                return path;

            //Check if already a full path
            if (path[1] == ':')
                return path;

            //Normalize path
            path = IOHelper.GetStandardizedPath(path);
            if (path.StartsWith("/"))
                path = path.Substring(1);

            var i = path.IndexOf('/');
            if (i < 0)
            {
                //No slash in path, can't get a full path
                return null;
            }

            var initseg = path.Substring(0, i).ToLowerInvariant();
            switch (initseg)
            {
                case "sourcefilmmaker":
                    //Is a path relative to sfm
                    return CommonPath + path;
                case "game":
                    //Is a path relative to game
                    return BasePath + path;
                default:
                    //Is a known-to-SFM folder?
                    if (!SFM.IsKnownName(initseg))
                    {
                        //Unknown name, assume to be a plain game folder, try to find file
                        return GamePath + path;
                    }

                    //Specified game?
                    if (game != null)
                    {
                        //Is loaded?
                        var mod = GameInfo?.GetMod(game);
                        if (!loadedOnly || (mod != null && mod.Enabled))
                            return mod.Path + path;
                        return null;
                    }

                    //Check in which game folder the file in question exists
                    foreach (var mod in GameInfo.Mods)
                    {
                        if (loadedOnly && !mod.Enabled)
                            continue;

                        var fulldir = mod.Path + path;
                        if (File.Exists(fulldir))
                            return fulldir;
                    }
                    return null;
            }
        }

        /// <summary>
        /// Returns whether this folder's name is one known to SFM (such as <c>materials</c>).
        /// <para />Only includes folders which are found directly inside of mod directories (non-recursively).
        /// </summary>
        /// <param name="name">The folder name to look for (not a path, meaning no slashes), ignoring case on comparison.</param>
        public static bool IsKnownName(string name) => _knownNames.Contains(name, StringComparer.InvariantCultureIgnoreCase);

        /// <summary>
        /// The known names for a folder directly located in a mod directory.
        /// </summary>
        private static readonly string[] _knownNames = {"models", "materials", "scripts", "elements", "sound", "particles", "media", "cfg", "maps", "cubemap_screenshots",
            "downloads", "expressions", "reslists", "resource", "scenes", "gfx", "classes", "parts", "vguiedit" };

        /// <summary>
        /// Returns the automatically detected Source Filmmaker installation directory (BasePath), or null if no SFM installation was found inside of any Steam installation.
        /// <para />No checks are done for a valid SFM installation here, but simply for a folder check.
        /// </summary>
        /// <param name="throwOnNotFound">If set to <c>true</c>, throws an exception when no SFM installation is found as well.</param>
        /// <exception cref="FileLoadException">Only thrown if throwOnNotFound = true: No installation of SFM has been found.</exception>
        /// <exception cref="FileNotFoundException">Steam's config.vdf file could not be found.</exception>
        /// <exception cref="DirectoryNotFoundException">Steam's installation directory could not be found.</exception>
        public static string DetectSFMDirectory(bool throwOnNotFound = true)
        {
            //Auto-detect SFM folder
            var steamfolder = DetectSteamDirectory();

            //No reg key found
            if (steamfolder == null || !Directory.Exists(steamfolder))
                throw new DirectoryNotFoundException("Steam's installation directory could not be found.");

            //1. Check in base steam folder
            var steamsfm = Path.Combine(steamfolder, "SteamApps/common/SourceFilmmaker/");
            if (Directory.Exists(steamsfm))
                return IOHelper.GetStandardizedPath(steamsfm, true);

            //2. Check other library folders
            var configfile = Path.Combine(steamfolder, "config/config.vdf");

            //No config found
            if (!File.Exists(configfile))
                throw new FileNotFoundException("Steam's config.vdf file could not be found.");

            var configcontents = File.ReadAllLines(configfile);
            foreach (var linebase in configcontents)
            {
                //Fix line
                var line = linebase.TrimStart('\t', ' ').Replace("\"", "");
                if (!line.StartsWith("BaseInstallFolder"))
                    continue;

                //Get Base Install Folder
                var basefolder = line.Substring(line.LastIndexOf('\t') + 1).Replace("\\\\", "/");
                var confsfm = Path.Combine(basefolder, "SteamApps/common/SourceFilmmaker/");
                if (Directory.Exists(confsfm))
                {
                    return IOHelper.GetStandardizedPath(confsfm, true);
                }
            }

            //No sfm install found
            if (throwOnNotFound)
                throw new FileLoadException("No installation of SFM has been found.");
            return null;
        }

        /// <summary>
        /// Returns the automatically detected Steam installation directory (folder containing Steam.exe), or null if none could be detected.
        /// <para/>No checks are done for a valid Steam installation here, but simply for a folder check from registry. Be sure to verify the folder in advance.
        /// </summary>
        //FIXME: SFM - are there any instances where Steam does not use the registry entry? in such cases, should a fallback check be implemented
        //  which checks for all common "Program Files" directories (on the different drives)
        public static string DetectSteamDirectory() => IOHelper.GetStandardizedPath((string)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Valve\Steam", "SteamPath", null), true);

        /// <summary>
        /// Returns every directory that has been detected to contain movie session files (in standardized format), or an empty enumerable if none could be detected.
        /// <para/>The returned paths should not be recursed, as movie renders are sub-directories of the session directory by default.
        /// </summary>
        public static IEnumerable<string> DetectSessionDirectories()
        {
            //Can recurse?
            if (!SFM.ValidPaths)
                yield break;

            //Collect duplicates
            var dupes = new List<string>();

            //Check all elements/sessions folders for every mod
            //TODO: SFM DetectSessionDirectories - this does not take into account symlinked mod folders
            foreach (var dir in Directory.EnumerateDirectories(SFM.GamePath))
            {
                var sessdir = Path.Combine(dir, "elements", "sessions");
                if (ValidSessionDirectory(sessdir))
                {
                    //No need for a duplicate check, since each folder obviously is different here
                    var std = IOHelper.GetStandardizedPath(sessdir, true);
                    yield return std;
                    dupes.Add(std);
                }
            }

            //Check recently used files for their directories
            var recent = (string[])Registry.GetValue(@"HKEY_CURRENT_USER\Software\Valve\SourceFilmmaker\FileDialogs\SessionDocument", "recentFileList", null);
            if (recent != null)
            {
                //Is already a string split by newline, do same check as above
                foreach (var path in recent)
                {
                    var sessdir = Path.GetDirectoryName(path);
                    if (ValidSessionDirectory(sessdir))
                    {
                        //Duplicate check
                        var std = IOHelper.GetStandardizedPath(sessdir, true);
                        if (!dupes.Contains(std))
                        {
                            yield return std;
                            dupes.Add(std);
                        }
                    }
                }
            }

            //Check "new session" directory
            var newsess = (string)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Valve\SourceFilmmaker\NewSessionWizard", "Directory", null);
            if (newsess != null)
            {
                //Check at once here, since format could be messed up by user
                var std = IOHelper.GetStandardizedPath(newsess, true);
                if (ValidSessionDirectory(std) && !dupes.Contains(std))
                {
                    yield return std;
                    dupes.Add(std);
                }
            }



            bool ValidSessionDirectory(string sessdir) => Directory.Exists(sessdir) && Directory.EnumerateFiles(sessdir, "*.dmx").Any();
        }
    }

    /// <summary>
    /// Which folder another folder or file can be seen relative to, in SFM context.
    /// </summary>
    public enum RelativeTo
    {
        /// <summary>
        /// Folder path relative to "game" directory. Final directory includes mod name and all sub folders.
        /// </summary>
        GameDir,
        /// <summary>
        /// Folder path relative to "SourceFilmmaker" directory. Final directory includes "game" directory, mod name and all sub folders.
        /// </summary>
        SFMDir,
        /// <summary>
        /// Folder path relative to mod directory. Final directory includes only sub folders.
        /// </summary>
        ModDir
    }
}
