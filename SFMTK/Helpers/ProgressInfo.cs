﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace SFMTK
{
    /// <summary>
    /// Helper class for displaying progress information.
    /// </summary>
    public static class ProgressInfo
    {
        //TODO: ProgressInfo - where needed, show progress bar on taskbar icon (win7 api)

        //TODO: ProgressInfo - check which properties changed between progress reports (especially for CLI) to avoid redrawing too much

        /// <summary>
        /// Creates a progress instance which allows the caller to show progress depending on the program's current display state.
        /// </summary>
        /// <param name="title">The title of the progress window.</param>
        /// <param name="text">The text to display above the progress bar.</param>
        /// <param name="canCancel">If set to <c>true</c>, the user may cancel this progress operation.</param>
        /// <param name="values">The initial ProgressValues, determining state of the progress window.</param>
        /// <remarks>
        /// Handling CLI (Command-Mode) and WinForms is not very different from one another.
        /// After calling ShowProgress, the progress bar/progress window will be shown with no progress.
        /// Every call to Report updates the display with the given values.
        /// 
        /// If you call ShowProgress while no Application is running (no SFMTK dialog), it's a bit different:
        /// After the call, you have to ensure that you call Application.Run on the same thread.
        /// Progress handling should be done over a separate thread!
        /// 
        /// Pressing cancel in both cases will do nothing more than requesting a cancel, you will have to handle it
        /// yourself in order to truly cancel the action.
        /// </remarks>
        public static ProgressHandler ShowProgress(string title, string text, bool canCancel, ProgressValue values = default(ProgressValue))
        {
            if (Program.Options.CommandMode)
                return ShowCommandLineProgress(title, text, canCancel, values);
            return ShowDialogProgress(title, text, canCancel, values);
        }

        public static ProgressHandler ShowDialogProgress(string title, string text, bool canCancel, ProgressValue values = default(ProgressValue))
        {
            Program.InitGUI();

            //Init instances
            IsBusy = true;
            var dialog = new Forms.ProgressWindow(title, text, values.Marquee, !values.Marquee);
            var tokensrc = new CancellationTokenSource();
            var progress = new Progress<ProgressValue>(OnProgress);
            _renderprogress = new object();

            //Properties
            dialog.CanCancel = canCancel;
            dialog.CloseOnCancel = false;

            //Events
            if (canCancel)
                dialog.CancelClick += OnCancel;

            dialog.Show(Program.MainForm);

            return new ProgressHandler(progress, tokensrc, dialog);



            void OnProgress(ProgressValue obj)
            {
                lock (_renderprogress)
                {
                    if (obj.IsDone)
                    {
                        //Cleanup UI
                        if (!dialog.IsDisposed)
                            dialog.ForceClose();
                        IsBusy = false;
                        return;
                    }

                    if (!IsBusy || dialog.IsDisposed)
                        return;

                    dialog.Maximum = (int)obj.Maximum;
                    dialog.Value = (int)obj.Value;
                    dialog.Progress.Style = obj.Marquee ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks;
                    dialog.State = obj.State;
                    dialog.Text = obj.Name;
                    dialog.MainLabel.Text = obj.Text;
                }
            }

            void OnCancel(object sender, EventArgs e)
            {
                tokensrc.Cancel();
            }
        }

        public static ProgressHandler ShowCommandLineProgress(string title, string text, bool canCancel, ProgressValue values = default(ProgressValue))
        {
            //Init instances
            IsBusy = true;
            var tokensrc = new CancellationTokenSource();
            var progress = new Progress<ProgressValue>(OnProgress);
            _marqueePos = 5;
            _condrawY = Console.WindowHeight - 1;
            _renderprogress = new object();

            //Events
            if (canCancel)
                Console.CancelKeyPress += OnCancel;
            else
                Console.CancelKeyPress += (s, e) => e.Cancel = true;

            //Show progress bar framework
            var oldposX = Console.CursorLeft;
            var oldposY = Console.CursorTop;

            Console.SetCursorPosition(0, _condrawY);
            Console.Write("0%  {0}{1}{0}", ProgressCharacterSet[1], ProgressCharacterSet[2]);
            if (text == null)
                text = "Progress:";
            Console.SetCursorPosition(0, _condrawY - MathEx.ConsoleLinesNeeded(text));
            Console.Write(text);
            Console.Title = "[0%] SFMTK - " + title;

            Console.SetCursorPosition(oldposX, oldposY);

            return new ProgressHandler(progress, tokensrc, null);



            void OnProgress(ProgressValue obj)
            {
                lock (_renderprogress)
                {
                    if (!IsBusy)
                        return;

                    //TODO: ProgressInfo - it may make more sense not to show the percentage if we're marquee-ing
                    if (obj.IsDone)
                    {
                        //UNTESTED: ProgressInfo - Cleanup UI
                        var max = MathEx.ConsoleLinesNeeded(text) + 1;
                        for (var y = 0; y > max ; y++)
                        {
                            Console.SetCursorPosition(0, _condrawY - y);
                            Console.Write(new string(' ', Console.WindowWidth));
                        }
                        IsBusy = false;
                        return;
                    }

                    var oldpos2X = Console.CursorLeft;
                    var oldpos2Y = Console.CursorTop;

                    //Update title
                    var perc = (obj.Percentage.ToString("N0") + "%");
                    Console.Title = $"[{perc}] SFMTK - {obj.Name ?? title}";

                    //NYI: ProgressInfo - update Text displayed using progressvalue

                    //Set color
                    Console.ForegroundColor = GetColorFromState(obj.State);

                    if (obj.Marquee)
                    {
                        //Marquee
                        Console.SetCursorPosition(0, _condrawY);
                        var width = Console.BufferWidth - 6;

                        //FIXME: ProgressInfo - nobody likes the pop-in on marquee, I'm sure of it
                        _marqueePos++;
                        if (_marqueePos >= Console.BufferWidth)
                            _marqueePos = 5;

                        var barfilledwidth = (int)(MarqueeWidth / 100 * width);
                        if (_marqueePos + barfilledwidth >= Console.BufferWidth - 1)
                            barfilledwidth = Console.BufferWidth - _marqueePos - 1;
                        var barfilled = new string(ProgressCharacterSet[0], barfilledwidth);

                        var barprewidth = _marqueePos - 5;
                        var barpre = barprewidth <= 0 ? "" : new string(ProgressCharacterSet[2], barprewidth);

                        var barpostwidth = width - (barprewidth + barfilledwidth);
                        var barpost = barpostwidth <= 0 ? "" : new string(ProgressCharacterSet[2], barpostwidth);

                        var bar = string.Concat(barpre, barfilled, barpost);
                        Console.Write("{2}{0}{1}{0}", ProgressCharacterSet[1], bar, perc.PadRight(4));
                    }
                    else
                    {
                        //Redraw whole bar
                        Console.SetCursorPosition(0, _condrawY);
                        var width = Console.BufferWidth - 6;

                        var barfilled = new string(ProgressCharacterSet[0], (int)(obj.Percentage / 100 * width));
                        var bar = barfilled + new string(ProgressCharacterSet[2], width - barfilled.Length);
                        Console.Write("{2}{0}{1}{0}", ProgressCharacterSet[1], bar, perc.PadRight(4));
                    }

                    Console.SetCursorPosition(oldpos2X, oldpos2Y);
                }
            }

            void OnCancel(object sender, ConsoleCancelEventArgs e)
            {
                //Do not quit the process
                e.Cancel = true;
                tokensrc.Cancel();
            }
        }

        private static int _marqueePos;

        private static object _renderprogress;

        private static int _condrawY;

        /// <summary>
        /// Gets whether any Progress is currently being displayed.
        /// </summary>
        public static bool IsBusy { get; private set; }

        /// <summary>
        /// Gets or sets the marquee width for command line, as a percentage between 0 and 100.
        /// </summary>
        public static decimal MarqueeWidth { get; set; } = 15;

        private static ConsoleColor GetColorFromState(ProgressBarState state)
        {
            switch (state)
            {
                default:
                    return ConsoleColor.White;
                case ProgressBarState.Error:
                    return ConsoleColor.Red;
                case ProgressBarState.Paused:
                    return ConsoleColor.Yellow;
            }
        }

        /// <summary>
        /// Gets or sets the characters used to show progress bars in the commandline. [0] represents the bar itself, [1] is the border, [2] is the lack of a bar (spacing).
        /// </summary>
        public static string ProgressCharacterSet { get; set; } = "\u2588|_";
    }
}
