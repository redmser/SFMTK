﻿using SFMTK.Modules;
using System;
using System.Diagnostics;

namespace SFMTK
{
    /// <summary>
    /// Tries to use SFMTK.Modules.Network for any network-related calls, unless it does not exist.
    /// </summary>
    public static class NetworkFallback
    {
        //IMP: NetworkFallback - this system of fallbacks is completely useless! attempt a more dynamic approach
        //  that allows for a "module isn't loaded, do you want to load it now?" message instead, and doesn't need you to
        //  re-create each method/member you want to use!
        //  -> even then, if the network module is disabled, the user doesnt WANT anything to use networking!!

        /// <summary>
        /// Updates the reference to the Network module.
        /// </summary>
        public static void UpdateNetworkFallback()
            => _networkHelper = Program.MainMM.GetModuleByName("SFMTK.Modules.Network", true)?.GetAssembly()?.GetType("NetworkHelper");

        /// <summary>
        /// Opens a web browser for displaying the specified URL. Whether the external or internal browser is used depends on the settings key <c>ExternalBrowser</c>.
        /// <para />Ensure in advance whether the URL is valid, and doesn't try to do weird stuff!
        /// </summary>
        /// <param name="url">The URL to open.</param>
        public static void ShowWebsite(string url)
        {
            UpdateNetworkFallback();

            if (_networkHelper == null)
            {
                //Cheap fallback
                try
                {
                    Process.Start(url);
                }
                catch (Exception)
                {
                    //Just enable that module instead
                    ShowModuleMissingError(url);
                }
            }
            else
            {
                //Use helper call
                _networkHelper.GetMethod("ShowWebsite").Invoke(null, new[] { url });
            }
        }

        /// <summary>
        /// Opens the specified URL in a new external browser window.
        /// </summary>
        /// <param name="url">The URL to open the browser with.</param>
        /// <param name="suggestChange">If set to <c>true</c>, the error message may suggest changing to the internal browser.</param>
        public static void OpenInBrowser(string url, bool suggestChange = false)
        {
            UpdateNetworkFallback();

            if (_networkHelper == null)
            {
                //Cheap fallback
                try
                {
                    Process.Start(url);
                }
                catch (Exception)
                {
                    //Just enable that module instead
                    ShowModuleMissingError(url);
                }
            }
            else
            {
                //Use helper call
                _networkHelper.GetMethod("OpenInBrowser").Invoke(null, new object[] { url, suggestChange });
            }
        }

        private static void ShowModuleMissingError(string url)
        {
            FallbackTaskDialog.ShowDialogLog($"Unable to open browser to page \"{url}\".\n\nPlease enable the SFMTK.Modules.Network module!",
                        "Can't open browser!", "Unable to open browser", FallbackDialogIcon.Error, new FallbackDialogButton(System.Windows.Forms.DialogResult.OK));
        }

        private static Type _networkHelper;
    }
}
