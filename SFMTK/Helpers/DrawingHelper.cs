﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace SFMTK
{
    /// <summary>
    /// Helper class for using graphics, drawing, bitmaps and similar.
    /// </summary>
    public static class DrawingHelper
    {
        /// <summary>
        /// Gets an image that can be used in case of a bitmap loading error. It is a red cross with transparent background, 16x16 pixels in size.
        /// </summary>
        public static Bitmap GetErrorImage() => Properties.Resources.cross;

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap Resize(this Image image, int width, int height)
        {
            if (image == null)
                throw new ArgumentNullException(nameof(image));

            if (width <= 0)
                throw new ArgumentOutOfRangeException(nameof(width));

            if (height <= 0)
                throw new ArgumentOutOfRangeException(nameof(height));

            if (image.Width == width && image.Height == height)
                return new Bitmap(image);

            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }


        /// <summary>
        /// Returns a new MSAGL color from the specified Color struct.
        /// </summary>
        /// <param name="color">The color to convert.</param>
        public static Microsoft.Msagl.Drawing.Color ToMsaglColor(this System.Drawing.Color color) => new Microsoft.Msagl.Drawing.Color(color.A, color.R, color.G, color.B);

        /// <summary>
        /// Returns a new Media.Color from the specified Color struct.
        /// </summary>
        /// <param name="color">The color to convert.</param>
        public static System.Windows.Media.Color ToMediaColor(this Color color) => System.Windows.Media.Color.FromRgb(color.R, color.G, color.B);

        /// <summary>
        /// Converts this Vector3 instance to a Color instance. Clamps values as the Color structure does not accept out-of-bounds values.
        /// </summary>
        /// <param name="decimalValues">If set to <c>true</c>, the values of this Vector3 are decimal values from 0 to 1, instead of integers from 0 to 255.</param>
        /// <remarks>Decimal places of the components when <paramref cref="decimalValues"/> is <c>false</c> are ignored.</remarks>
        public static Color ToColor(this System.Numerics.Vector3 vector, bool decimalValues = false)
        {
            //Bounds checking
            if (decimalValues)
            {
                //Between 0 and 1
                ValidRange(0, 1);
            }
            else
            {
                //Between 0 and 255
                ValidRange(0, 255);
            }

            var factor = decimalValues ? 255 : 1;
            return Color.FromArgb((int)MathEx.Clamp(vector.X * factor, 0, 255),
                                  (int)MathEx.Clamp(vector.Y * factor, 0, 255),
                                  (int)MathEx.Clamp(vector.Z * factor, 0, 255));



            void ValidRange(float min, float max)
            {
                if (vector.X < min || vector.X > max || vector.Y < min || vector.Y > max || vector.Z < min || vector.Z > max)
                    throw new ArgumentOutOfRangeException($"Vector {vector} is not within bounds of a color.");
            }
        }
    }
}
