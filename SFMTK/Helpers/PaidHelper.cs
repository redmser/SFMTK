﻿using System.Windows.Forms;

namespace SFMTK
{
    /// <summary>
    /// Helper class for hiding features implemented in free modules that have to be paid for to gain access to them.
    /// </summary>
    public static class PaidHelper
    {
        /// <summary>
        /// Returns the <see cref="Program.IsFullVersion" /> property.
        /// If <c>false</c>, also shows a message box about needing the full version to use this feature.
        /// </summary>
        /// <param name="featurename">Optional name of the feature that requires full version.</param>
        public static bool CheckFullVersion(string featurename = null)
        {
            var fv = Program.IsFullVersion;
            if (fv)
                return true;
            if (featurename == null)
                featurename = "This feature";
            var res = FallbackTaskDialog.ShowDialog($"{featurename} requires the full version of SFMTK!\n\nWould you like to learn more?",
                "Paid version required!", "Feature requires full version", FallbackDialogIcon.Info,
                new FallbackDialogButton("Learn more", DialogResult.OK), new FallbackDialogButton(DialogResult.Cancel));
            if (res == DialogResult.OK)
            {
                //NYI: PaidHelper - learn more about paid version
                throw new System.NotImplementedException();
            }
            return false;
        }
    }
}
