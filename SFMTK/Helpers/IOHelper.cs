using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Filesystem.Ntfs;
using System.Linq;
using System.Text;
using SFMTK.Contents;
using SFMTK.Converters;

namespace SFMTK
{
    /// <summary>
    /// Helper class of all I/O operations and methods related to the file system.
    /// </summary>
    public static partial class IOHelper
    {
        /// <summary>
        /// Returns a standardized variant of the specified path. Uses front-slashes and directories always end with a slash as well.
        /// </summary>
        /// <param name="path">The input path to reformat.</param>
        /// <param name="fixCaps">If set to <c>true</c>, updates the capitalization of the input path to how the folders in the file system are capitalized.</param>
        public static string GetStandardizedPath(string path, bool fixCaps = false)
        {
            const string fileuri = "file:///";

            //Return if invalid path, or path has no directory (= is a filename only)
            if (String.IsNullOrWhiteSpace(path) || (!path.Contains("\\") && !path.Contains("/")))
                return path;
            if (fixCaps)
                return GetStandardizedPath(FixPathCase(path));

            //Remove fileuri if needed
            if (path.StartsWith(fileuri))
                path = path.Substring(fileuri.Length);

            //Replace consecutive (back)slashes with just single slashes
            path = System.Text.RegularExpressions.Regex.Replace(path, @"[/\\]+", "/");

            //Add trailing / to directories
            if (!path.EndsWith("/") && string.IsNullOrEmpty(Path.GetExtension(path)))
                path = path + "/";

            return path;
        }

        /// <summary>
        /// Returns a path that fits Windows' convention of paths (backslashes).
        /// </summary>
        /// <param name="path">The input path to reformat.</param>
        public static string GetWindowsPath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return path;
            return path.Replace("/", "\\"); 
        }

        /// <summary>
        /// Returns the full command line for the specified extension, or null if no program is associated to the specified extension.
        /// <para />This may freeze the UI thread, so be sure to use an asynchronous context if needed!
        /// </summary>
        /// <param name="extension">Extension name, with or without the period.</param>
        /// <exception cref="ArgumentNullException">Extension has not been specified.</exception>
        public static string GetAssociatedProgram(string extension)
        {
            if (string.IsNullOrEmpty(extension))
                throw new ArgumentNullException("Extension has not been specified.");
            if (extension[0] != '.')
                extension = "." + extension;
            return GetAssociatedProgram(extension, false);
        }

        /// <summary>
        /// Returns the full command line for the specified extension, or null if no program is associated to the specified extension.
        /// <para />This may freeze the UI thread, so be sure to use an asynchronous context if needed!
        /// </summary>
        /// <param name="name">Extension or file type name, with period.</param>
        /// <param name="istypename">Whether the specified string is a file type name or an extension.</param>
        public static string GetAssociatedProgram(string name, bool istypename)
        {
            if (name == null)
                return null;

            if (istypename)
            {
                //Use ftype to get associated program
                var stream = ExecuteReadStream("ftype", name);

                //Check if error or not
                if (stream[name.Length] == '=')
                {
                    //Valid!
                    return stream.Substring(name.Length + 1);
                }

                return null;
            }
            else
            {
                return GetAssociatedProgram(FileTypeFromExtension(name), true);
            }
        }

        /// <summary>
        /// Returns whether the given paths are the same.
        /// </summary>
        /// <param name="path1">Path to compare, any format.</param>
        /// <param name="path2">Path to compare, any format.</param>
        /// <param name="ignoreCase">Whether to ignore case for comparison. Windows does not care for case, but may be different for other operating systems.</param>
        /// <returns></returns>
        public static bool PathsEqual(string path1, string path2, bool ignoreCase = true)
        {
            if (path1 == null)
                return path2 == null;

            return GetStandardizedPath(path1).Equals(GetStandardizedPath(path2), ignoreCase ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Gets the folder containing SFMTK's executable.
        /// </summary>
        public static string SFMTKDirectory => AppDomain.CurrentDomain.BaseDirectory;

        /// <summary>
        /// Returns the absolute path of the specified relative path, with the root being SFMTK's folder.
        /// </summary>
        public static string GetSFMTKDirectory(string path) => Path.Combine(SFMTKDirectory, path);

        /// <summary>
        /// Gets the associated file type given an extension.
        /// </summary>
        /// <param name="extension">The extension to run "assoc" on.</param>
        public static string FileTypeFromExtension(string extension)
        {
            var stream = ExecuteReadStream("assoc", extension);

            //Check if error or not
            if (stream[extension.Length] == '=')
            {
                //Valid!
                return stream.Substring(extension.Length + 1);
            }

            return null;
        }

        private static string ExecuteReadStream(string file, string args)
        {
            var info = new ProcessStartInfo("cmd.exe", $"/C \"{file} {args}\"");
            info.CreateNoWindow = true;
            info.UseShellExecute = false;
            info.RedirectStandardOutput = true;
            info.RedirectStandardError = true;
            info.WindowStyle = ProcessWindowStyle.Hidden;

            var proc = Process.Start(info);
            var ln = proc.StandardOutput.ReadLine() ?? proc.StandardError.ReadLine();
            return ln;
        }

        /// <summary>
        /// Updates the specified file extension to run the given application.
        /// <para />This may freeze the UI thread, so be sure to use an asynchronous context if needed!
        /// </summary>
        /// <param name="extension">Extension name, with or without the period.</param>
        /// <param name="application">New path to the application used to run this file type.
        /// Use quotes around paths with spaces, and add the argument "%1" to pass as input files.</param>
        public static void SetAssociatedProgram(string extension, string application)
        {
            if (string.IsNullOrEmpty(extension))
                throw new ArgumentNullException("Extension has not been specified.");
            if (string.IsNullOrEmpty(application))
                throw new ArgumentNullException("New application path has not been specified.");

            if (extension[0] != '.')
                extension = "." + extension;
            SetAssociatedProgram(extension, null, application);
        }

        /// <summary>
        /// Updates the specified file extension to use the given association, optionally updating the application it points to.
        /// <para />This may freeze the UI thread, so be sure to use an asynchronous context if needed!
        /// </summary>
        /// <param name="extension">Extension name, with period.</param>
        /// <param name="association">New filetype association. Use null if you do not wish to update it.</param>
        /// <param name="application">New application path. Use null if you do not wish to update it.</param>
        /// <param name="istypename">Whether the name parameter is an extension or a file type.</param>
        public static void SetAssociatedProgram(string extension, string association, string application)
        {
            if (association != null)
            {
                //Update assoc
                ExecuteReadStream("assoc", $"{extension}={association}");
            }
            else
            {
                //Set association for updating ftype value
                association = GetAssociatedProgram(extension);
            }

            if (application != null)
            {
                //Update application string
                ExecuteReadStream("ftype", $"{association}={application}");
            }
        }

        /// <summary>
        /// Opens the specified path in the explorer. Returns whether it was able to find the absolute path to the file.
        /// </summary>
        /// <param name="path">Path to a file or folder, in any format.</param>
        /// <param name="findRelative">If set to <c>true</c>, the path may be a relative path and will attempted to be resolved.</param>
        /// <param name="showError">Whether to show an error message if the file could not be viewed.</param>
        public static bool OpenInExplorer(string path, bool findRelative, bool showError = true)
        {
            //Check if any path specified
            var abspath = path;
            if (string.IsNullOrEmpty(abspath))
                return false;

            //Remove URI, if any
            const string fileuri = "file:///";
            if (abspath.StartsWith(fileuri))
                abspath = abspath.Substring(fileuri.Length);

            //Try path as it is
            if (TryOpen())
                return true;

            //Try directory only
            abspath = Path.GetDirectoryName(abspath);
            if (TryOpen())
                return true;

            //Try path as relative path
            if (findRelative)
            {
                abspath = SFM.GetFullPath(abspath);
                if (TryOpen())
                    return true;
            }

            //Error: unknown path format
            if (showError)
                FallbackTaskDialog.ShowDialog("Could not find the specified directory or file.", "Unable to view file!", "Unable to find file",
                    FallbackDialogIcon.Error, new FallbackDialogButton(System.Windows.Forms.DialogResult.OK));
            return false;



            bool TryOpen()
            {
                if (abspath == null)
                {
                    //Instantly cancel
                    return false;
                }
                else if (File.Exists(abspath))
                {
                    //Open explorer at file location
                    Process.Start("explorer.exe", $"/select,\"{IOHelper.GetWindowsPath(abspath)}\"");
                    return true;
                }
                else if (Directory.Exists(abspath))
                {
                    //Open explorer at directory
                    Process.Start("explorer.exe", $"\"{IOHelper.GetWindowsPath(abspath)}\"");
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Strips any invalid characters out of the specified text to turn it into a valid file name.
        /// </summary>
        /// <param name="text">Input text to strip.</param>
        /// <param name="replacement">Which char to replace any invalid characters with. May be null, stripping the invalid characters instead.</param>
        /// <exception cref="ArgumentException">Replacement character is an invalid character for a filename.</exception>
        public static string MakeValidFileName(string text, char? replacement = '_')
        {
            var invalids = Path.GetInvalidFileNameChars();
            if (invalids.Any(i => i == replacement))
                throw new ArgumentException("Replacement character is an invalid character for a filename.");

            var sb = new StringBuilder(text.Length);
            var changed = false;
            foreach (var c in text)
            {
                if (invalids.Contains(c))
                {
                    changed = true;
                    var repl = replacement ?? '\0';
                    if (repl != '\0')
                        sb.Append(repl);
                }
                else
                {
                    sb.Append(c);
                }
            }

            //Result would be empty?
            if (sb.Length == 0)
                return replacement?.ToString() ?? "_";
            return changed ? sb.ToString() : text;
        }

        /// <summary>
        /// Returns the contents of the specified file if it is a text file. Returns null for binary files.
        /// </summary>
        /// <param name="path">The path of the file to read.</param>
        /// <param name="encoding">The encoding to use. Null for default encoding.</param>
        /// <exception cref="ArgumentNullException">path - Tried reading a null file.</exception>
        /// <exception cref="FileNotFoundException">path - Unable to open an empty string as a path.</exception>
        public static string ReadFileIfString(string path, Encoding encoding = null)
        {
            if (path == null)
                throw new ArgumentNullException(nameof(path), "Tried reading a null file.");

            if (path == string.Empty)
                throw new FileNotFoundException(nameof(path), "Unable to open an empty string as a path.");

            var enc = encoding ?? Encoding.Default;
            if (IsBinaryFile(path, enc))
                return null;

            //Return file happily
            return File.ReadAllText(path, enc);
        }

        /// <summary>
        /// Returns whether the specified file is a binary file by reading the first few bytes, checking for control characters.
        /// </summary>
        /// <param name="path">The path to the file to verify.</param>
        /// <param name="encoding">The encoding of the file. Null to use default encoding.</param>
        /// <param name="readlen">How many characters are read from the file at maximum until it is determined whether the file is binary.</param>
        /// <exception cref="ArgumentOutOfRangeException">Reading length must be greater than zero.</exception>
        /// <exception cref="ArgumentNullException">Tried reading a null file.</exception>
        /// <exception cref="FileNotFoundException">The specified file was not found. -or- Unable to open an empty string as a path.</exception>
        public static bool IsBinaryFile(string path, Encoding encoding = null, int readlen = 64)
        {
            if (readlen <= 0)
                throw new ArgumentOutOfRangeException(nameof(readlen), "Reading length must be greater than zero.");

            if (path == null)
                throw new ArgumentNullException(nameof(path), "Tried reading a null file.");

            if (path == string.Empty)
                throw new FileNotFoundException(nameof(path), "Unable to open an empty string as a path.");

            //Read X characters and look for control chars
            var enc = encoding ?? Encoding.Default;
            using (var file = File.OpenRead(path))
            {
                var len = (int)Math.Min(file.Length, readlen);
                var arr = new byte[len];
                file.Read(arr, 0, len);
                foreach (var ch in enc.GetChars(arr))
                {
                    if (ch != '\r' && ch != '\n' && ch != '\t' && char.IsControl(ch))
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns whether the specified path is a file or a folder.
        /// Possible values:
        /// <para/>Offline (path does not exist)
        /// <para/>Directory (is a folder)
        /// <para/>Normal (is a file)
        /// </summary>
        public static FileAttributes IsFileOrFolder(string path)
        {
            if (!File.Exists(path) && !Directory.Exists(path))
                return FileAttributes.Offline;

            var attr = File.GetAttributes(path);

            if (attr.HasFlag(FileAttributes.Directory))
                return FileAttributes.Directory;
            else
                return FileAttributes.Normal;
        }

        /// <summary>
        /// Returns whether the specified path / directory name is valid and does not contain any invalid characters.
        /// </summary>
        /// <param name="path">The path to check for.</param>
        public static bool IsValidDirectoryName(string path)
        {
            var invalid = Path.GetInvalidPathChars().Concat(Path.GetInvalidFileNameChars());
            return !path.Any(c => invalid.Contains(c));
        }

        /// <summary>
        /// Gets the correct case of a path, as it exists in the file system.
        /// </summary>
        /// <param name="path">Full path to the file or folder.</param>
        public static string FixPathCase(string path)
        {
            if (string.IsNullOrEmpty(path))
                return path;

            if (!Directory.Exists(Path.GetDirectoryName(path)))
                throw new DirectoryNotFoundException("Directory of path \"" + path + "\" not found, could not correct path case.");

            return StringHelper.FirstLetterToUpperCase(FixPathCaseInternal(path));
        }

        /// <summary>
        /// Gets the correct case of a path, as it exists in the file system.
        /// </summary>
        /// <param name="path">The file or folder.</param>
        private static string FixPathCaseInternal(string path)
        {
            var parentdir = Path.GetDirectoryName(path);
            var childname = Path.GetFileName(path);
            if (Object.ReferenceEquals(parentdir, null))
                return path.TrimEnd(new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar });
            if (Directory.Exists(parentdir))
            {
                var fileorfolder = Directory.GetFileSystemEntries(parentdir, childname).FirstOrDefault();
                if (!Object.ReferenceEquals(fileorfolder, null))
                {
                    childname = Path.GetFileName(fileorfolder);
                }
            }
            return FixPathCaseInternal(parentdir) + Path.DirectorySeparatorChar + childname;
        }

        /// <summary>
        /// Generates a filter string (to be used by file dialogs) using the name of the format and its format filter.
        /// </summary>
        /// <param name="formatname">Friendly name of the format.</param>
        /// <param name="formatfilter">A format filter in the format <c>*.ex1;*.ex2;...</c></param>
        public static string GenerateFilterString(string formatname, string formatfilter) => $"{formatname} ({formatfilter})|{formatfilter}";

        /// <summary>
        /// Generates a filter string (to be used by file dialogs) using the name of the format and its format filters.
        /// </summary>
        /// <param name="formatname">Friendly name of the format.</param>
        /// <param name="formatfilters">Format filters in the format <c>*.ext</c></param>
        public static string GenerateFilterString(string formatname, params string[] formatfilters) => GenerateFilterString(formatname, string.Join(";", formatfilters));

        /// <summary>
        /// Generates the filter string (to be used by file dialogs) for the specified content converters.
        /// </summary>
        /// <param name="converters">The converters to consider for generating the filter string.</param>
        public static string GenerateFilterString(IEnumerable<ContentConverter> converters)
        {
            var formats = new HashSet<string>();

            foreach (var cc in converters)
            {
                //The same converter is still allowed to specify different formats (even if rarely done so)
                //hence avoiding the use of "else if" here
                if (cc is IImportContent iic)
                    formats.Add(GenerateFilterString(iic.FormatName, iic.FormatFilter));
                if (cc is IExportContent iec)
                    formats.Add(GenerateFilterString(iec.FormatName, iec.FormatFilter));
            }

            return string.Join("|", formats);
        }

        /// <summary>
        /// Opens a Windows save file dialog for saving content, returning null if cancelled or the standardized path to the file otherwise.
        /// </summary>
        /// <param name="content">Content to get the base data from. Use null to allow saving to all content types.</param>
        /// <param name="promptOverwrite">If set to <c>true</c>, asks the user whether to overwrite the given file (if it exists already).
        /// <c>false</c> to silently continue and overwrite.</param>
        /// <param name="showExporterSettings">If set to <c>true</c>, and the selected content exporter implements
        /// <see cref="IExporterSettings"/>, shows these settings in a new dialog. If <c>false</c>, uses default settings instead.</param>
        public static string SaveFileDialog(Content content, bool promptOverwrite, bool showExporterSettings)
        {
            //Select path
            var sfd = new Ookii.Dialogs.Wpf.VistaSaveFileDialog();
            sfd.AddExtension = true;
            sfd.CheckPathExists = true;
            sfd.DefaultExt = content?.MainExtension;
            sfd.Filter = content == null ? GenerateFilterString(ContentConverter.GetExporters(false))
                : GenerateFilterString(ContentConverter.GetExporters(false, content.Data.GetType(), true));

            var init = content?.FilePath;
            if (content == null || content.FakeFile || string.IsNullOrEmpty(init) || (!File.Exists(init) && !Directory.Exists(Path.GetDirectoryName(init))))
            {
                init = SFM.GamePath;
                if (content != null)
                    sfd.FileName = Path.GetFileName(content.FilePath);
            }
            sfd.InitialDirectory = IOHelper.GetWindowsPath(init);
            sfd.OverwritePrompt = promptOverwrite;
            sfd.Title = $"Save {Content.GetTypeName(content) ?? "file"} as";

            var res = sfd.ShowDialog();
            if (res ?? false)
            {
                //NYI: IOHelper - if exporter has any (implements interface), show instance settings in a new dialog -> same for importer below
                //  -> can this method still only return filename then? might have to instead return a tuple or the like, additionally storing the contentconverter used
                return IOHelper.GetStandardizedPath(sfd.FileName);
            }
            return null;
        }

        /// <summary>
        /// Opens a Windows open file dialog for opening content, returning null if cancelled or the standardized path(s) to the file(s) otherwise.
        /// </summary>
        /// <param name="multiSelect">If set to <c>true</c>, allow multiple selections.</param>
        /// <param name="showImporterSettings">If set to <c>true</c>, and the selected content importer implements
        /// <see cref="IImporterSettings"/>, shows these settings in a new dialog. If <c>false</c>, uses default settings instead.</param>
        public static IEnumerable<string> OpenFileDialog(bool multiSelect, bool showImporterSettings)
        {
            //Select path
            var ofd = new Ookii.Dialogs.Wpf.VistaOpenFileDialog();
            ofd.AddExtension = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            ofd.Filter = GenerateFilterString(ContentConverter.GetImporters(false));
            ofd.Multiselect = multiSelect;
            ofd.InitialDirectory = IOHelper.GetWindowsPath(SFM.GamePath);
            ofd.Title = "Open file" + (multiSelect ? "s" : "");

            var res = ofd.ShowDialog();
            if (res ?? false)
            {
                //Show instance settings
                return ofd.FileNames.Select(f => IOHelper.GetStandardizedPath(f));
            }
            return null;
        }

        /// <summary>
        /// Gets whether the specified drive (by letter) is a NTFS drive.
        /// </summary>
        /// <param name="driveName">A valid drive path or drive letter. This can be either uppercase or lowercase, 'a' to 'z'. A null value is not valid.</param>
        public static bool IsNTFSDrive(string driveName)
        {
            var driveinfo = new DriveInfo(driveName);
            return driveinfo.DriveFormat == "NTFS";
        }

        /// <summary>
        /// Returns all nodes in the specified directory using the MFT.
        /// <para/>This only works on NTFS drives, be sure to check for this in advance using <see cref="IsNTFSDrive" />.
        /// </summary>
        /// <param name="rootPath">Only gets files from this folder. This also determines the drive to read from. Wildcards aren't supported.</param>
        public static IEnumerable<INode> GetNodesUsingMFT(string rootPath, RetrieveMode mode = RetrieveMode.StandardInformations)
        {
            var read = new NtfsReader(new DriveInfo(rootPath[0].ToString()), mode);
            return read.GetNodes(rootPath);
        }

        /// <summary>
        /// Returns all files in the specified directory using the MFT. More efficient than <see cref="GetNodesUsingMFT(string)" /> if you do not need additional properties.
        /// <para />This only works on NTFS drives, be sure to check for this in advance using <see cref="IsNTFSDrive" />.
        /// </summary>
        /// <param name="rootPath">Only gets files from this folder. This also determines the drive to read from. Wildcards aren't supported.</param>
        /// <param name="fullPath">If set to <c>true</c>, gets the full path to the file instead of only its file name.</param>
        public static ParallelQuery<string> GetFilesUsingMFT(string rootPath, bool fullPath)
        {
            var read = new NtfsReader(new DriveInfo(rootPath[0].ToString()), RetrieveMode.Minimal);
            var nodes = read.GetNodesDirectly();

            //Reformat to name
            var files = fullPath ? nodes.AsParallel().Select(n => read.GetNodeFullName(n))
                                 : nodes.AsParallel().Select(n => read.GetNodeName(n));

            if (rootPath.Length > 3)
            {
                //Filter return items (since rootPath is not just a drive name)
                var winpath = IOHelper.GetWindowsPath(rootPath);
                return files.Where(p => p.StartsWith(winpath, StringComparison.InvariantCultureIgnoreCase));
            }

            //Simply return all found
            return files;
        }

        #region Symlinks

        /// <summary>
        /// Returns whether the specified path to a file or folder contains a symbolic link.
        /// </summary>
        /// <param name="path">The path to check for a symbolic link. The file or folder specified must exist.</param>
        public static bool IsSymbolicLink(string path) => NativeMethods.SymbolicLink.GetTarget(path) != null;

        /// <summary>
        /// Returns whether the specified file or folder contains a symbolic link.
        /// </summary>
        /// <param name="info">The <see cref="FileSystemInfo"/> to check for a symbolic link. The file or folder specified must exist.</param>
        public static bool IsSymbolicLink(FileSystemInfo info) => IsSymbolicLink(info.FullName);

        /// <summary>
        /// Gets the real path of the specified path.
        /// </summary>
        /// <param name="path">The path to a file or folder which exists on disk.</param>
        /// <param name="allowNull">If set to <c>true</c>, and the target is not a symbolic link,
        /// returns <c>null</c> instead of the value of <paramref name="path"/>.</param>
        public static string GetSymbolicLinkTarget(string path, bool allowNull = false)
        {
            try
            {
                return NativeMethods.SymbolicLink.GetTarget(path) ?? (allowNull ? null : path);
            }
            catch (FileNotFoundException)
            {
                return (allowNull ? null : path);
            }
        }

        /// <summary>
        /// Gets the real path of the specified file or folder.
        /// </summary>
        /// <param name="info">The <see cref="FileSystemInfo"/> of a file or folder which exists on disk.</param>
        /// <param name="allowNull">If set to <c>true</c>, and the target is not a symbolic link,
        /// returns <c>null</c> instead of the value of <paramref name="path"/>.</param>
        public static string GetSymbolicLinkTarget(FileSystemInfo info, bool allowNull = false) => GetSymbolicLinkTarget(info.FullName, allowNull);

        #endregion Symlinks

        #region Directory Watcher

        //TODO: IOHelper - how can different filter sets of FileSystemWatchers be solved effectively?

        /// <summary>
        /// Creates a new <see cref="FileSystemWatcher"/> for the specified directory.
        /// Its properties should not be changed, as the watcher will be re-used whenever possible.
        /// <para/>Be sure to call a corresponding <see cref="StopDirectoryWatcher(FileSystemWatcher)"/> method afterwards.
        /// </summary>
        /// <param name="path">Directory to watch.</param>
        public static FileSystemWatcher StartDirectoryWatcher(string path, bool includeSubdirectories = false)
        {
            path = GetStandardizedPath(path);
            var watcher = _watchers.Keys.FirstOrDefault(w => w.Path == path);
            if (watcher == null)
            {
                //New watcher
                watcher = new FileSystemWatcher(path, "*") { IncludeSubdirectories = includeSubdirectories, EnableRaisingEvents = true };
                _watchers.Add(watcher, 1);
            }
            else
            {
                //Existing watcher, increment
                _watchers[watcher]++;
            }
            return watcher;
        }

        /// <summary>
        /// Tells a <see cref="FileSystemWatcher"/> created with a <see cref="StartDirectoryWatcher(string)"/> call that it is no longer used.
        /// </summary>
        /// <param name="watcher">The watcher that is no longer in use.</param>
        public static void StopDirectoryWatcher(FileSystemWatcher watcher)
        {
            //Decrement usage count
            var newval = --_watchers[watcher];

            //Remove if unused
            if (newval <= 0)
            {
                _watchers.Remove(watcher);
                watcher.Dispose();
            }
        }

        /// <summary>
        /// Tells a <see cref="FileSystemWatcher"/> created with a <see cref="StartDirectoryWatcher(string)"/> call that it is no longer used.
        /// </summary>
        /// <param name="path">Directory that is no longer being watched.</param>
        public static void StopDirectoryWatcher(string path)
        {
            path = GetStandardizedPath(path);
            StopDirectoryWatcher(_watchers.Keys.FirstOrDefault(w => w.Path == path));
        }

        /// <summary>
        /// Keeps track of how many users each cached <see cref="FileSystemWatcher"/> has, removing unneeded ones.
        /// </summary>
        private static readonly Dictionary<FileSystemWatcher, int> _watchers = new Dictionary<FileSystemWatcher, int>();

        internal static void DisposeDirectoryWatchers()
        {
            foreach (var fsw in _watchers.Keys)
                fsw.Dispose();
            _watchers.Clear();
        }

        #endregion Directory Watcher
    }
}