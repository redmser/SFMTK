using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SFMTK.Properties;

namespace SFMTK
{
    /// <summary>
    /// Helper class for any string-related modifications or comparisons.
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// Depending on the count, returns the plural or singular of the specified thing.
        /// <para />Resulting format: <code>"[count] [thing][s]"</code> - unless a specific plural version is defined.
        /// </summary>
        /// <param name="count"></param>
        /// <param name="thing"></param>
        /// <param name="plural">Plural form, instead of appending s.</param>
        /// <param name="space">Space between the count and the thing?</param>
        public static string GetPlural(int count, string thing, string plural = null, bool space = true)
        {
            var spc = space ? " " : string.Empty;
            if (count == 1)
                return $"{count}{spc}{thing}";
            var trail = thing + "s";
            if (plural != null)
                trail = plural;
            return $"{count}{spc}{trail}";
        }

        /// <summary>
        /// Same to a regular string's Contains call, but ignoring case.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="contain"></param>
        public static bool ContainsIgnoreCase(this string value, string contain)
        {
            if (value == null && contain == null)
                return true;
            if (value == null || contain == null)
                return false;

            return value.ToUpperInvariant().Contains(contain.ToUpperInvariant());
        }

        /// <summary>
        /// Returns the input string with the first character converted to uppercase
        /// </summary>
        public static string FirstLetterToUpperCase(this string s)
        {
            if (string.IsNullOrEmpty(s))
                throw new ArgumentNullException(nameof(s), "Can't capitalize empty or null string.");

            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        /// <summary>
        /// Given a list of existing names (must not fit normal name format), generates a new name with the given prefix and a count number greater than the existing ones.
        /// </summary>
        /// <param name="prefix">Name prefix for count.</param>
        /// <param name="suffix">Name suffix for count.</param>
        /// <param name="existingNames">List of already existing names.</param>
        /// <example>If <paramref name="existingNames" /> were Test, Model1, Model2 and Default, with a <paramref name="prefix" /> of Model,
        /// a call to this method would return Model3 as the new unique name.</example>
        public static string GetNewUniqueName(string prefix, string suffix, IEnumerable<string> existingNames)
        {
            if (existingNames == null)
                return ApplyPrefixSuffix(1);

            //Find the name with the highest number
            var names = existingNames.Where(s => !string.IsNullOrEmpty(s) && (
                (!string.IsNullOrEmpty(prefix) && s.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase) && s != prefix) ||
                (!string.IsNullOrEmpty(suffix) && s.EndsWith(suffix, StringComparison.InvariantCultureIgnoreCase) && s != suffix))
                ).Select(s =>
                { if (!string.IsNullOrEmpty(prefix)) s = s.Replace(prefix, ""); if (!string.IsNullOrEmpty(suffix)) s = s.Replace(suffix, ""); return s; });
            if (!names.Any())
                return ApplyPrefixSuffix(1);

            var highest = names.Max(s => int.Parse(s));
            return ApplyPrefixSuffix(highest + 1);



            string ApplyPrefixSuffix(int number) => (prefix ?? "") + number + (suffix ?? "");
        }

        /// <summary>
        /// Formats the given path using the specified TitleFormat. Returns null for a null path.
        /// </summary>
        /// <param name="filePath">The file path to format.</param>
        /// <param name="format">The format to format the path to.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">format is unknown</exception>
        public static string FormatPath(string filePath, TitleFormat format)
        {
            if (filePath == null)
                return null;

            //FIXME: StringHelper.FormatPath - instead of filename fallback, actually try to get a relative path that makes sense out of it somehow
            if (!SFM.IsInSFMFolder(filePath))
                format = TitleFormat.FileName;

            switch (format)
            {
                case TitleFormat.FullPath:
                    return filePath;
                case TitleFormat.LocalPath:
                    return SFM.GetLocalPath(filePath, RelativeTo.GameDir);
                case TitleFormat.LocalPathWithoutMod:
                    return SFM.GetLocalPath(filePath, RelativeTo.ModDir);
                case TitleFormat.FileName:
                    return Path.GetFileName(filePath);
            }
            throw new ArgumentOutOfRangeException(nameof(format));
        }

        /// <summary>
        /// Converts the specified number to a letter sequence, with the specified amount of digits. Basically a conversion to Base-26 with padding.
        /// </summary>
        /// <param name="number">The number to convert.</param>
        /// <param name="digits">The amount of letters to use for this number as padding.</param>
        public static string IntToLetters(int number, int digits)
        {
            //Weird case
            if (digits <= 0)
                return string.Empty;

            //Magic number 26 as the alphabet's length
            var sb = new StringBuilder(26);

            //Fix up number
            number = Math.Abs(number);

            //Calculate characters
            do
            {
                sb.Insert(0, GetChar(number % 26));
                number = number / 26;
            } while (number > 0);

            //Padding for remaining characters
            while (sb.Length < digits)
            {
                sb.Insert(0, 'A');
            };

            return sb.ToString();



            char GetChar(int num)
            {
                //Magic number 65 as the starting index of the upper-case alphabet (for ASCII)
                return (char)(num + 65);
            }
        }

        /// <summary>
        /// Returns the input string without any XML tags.
        /// </summary>
        public static string StripXmlTags(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;
            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }
}