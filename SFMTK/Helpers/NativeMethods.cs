﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Win32.SafeHandles;

namespace SFMTK
{
    /// <summary>
    /// Class focused around interops with P/Invoke and general WinAPI handling.
    /// Other helper classes include wrappers for some of these native methods.
    /// </summary>
    public static class NativeMethods
    {
#pragma warning disable IDE1006 // Naming Styles
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

        /// <summary>
        /// Gets whether the created console (<see cref="ShowConsoleWindow"/>) is attached to an existing CLI.
        /// </summary>
        public static bool IsAttachedConsole { get; private set; } = false;

        /// <summary>
        /// Gets whether a console window is displayed for this application.
        /// </summary>
        public static bool IsConsoleVisible { get; private set; } = false;

        /// <summary>
        /// Hides a previously shown console window associated to this WinForms application.
        /// </summary>
        public static void HideConsoleWindow()
        {
            if (!IsConsoleVisible)
                return;

            IsConsoleVisible = false;

            //BUG: NativeMethods - freeing an AttachedConsole is all sorts of messed up!
            //  - when including the Read() statement (from -cc), it takes two enter-presses to call this method
            //  - with both -c and -cc, it takes another enter press to truly return control to the console, despite the PostMessage call below
            //  - after closing with -c, the "folder name part>" does not show up until another enter press, even with the executable already closed at that point

            //Remove the console
            FreeConsole();

            if (IsAttachedConsole)
            {
                //Return control to console
                var hWnd = System.Diagnostics.Process.GetCurrentProcess().MainWindowHandle;
                PostMessage(hWnd, WM_KEYDOWN, VK_RETURN, 0);
            }
        }

        /// <summary>
        /// Shows a console window associated to this WinForms application.
        /// </summary>
        public static void ShowConsoleWindow()
        {
            if (IsConsoleVisible)
                return;

            IsConsoleVisible = true;

            //Create the console by attaching to existing console
            IsAttachedConsole = AttachConsole(-1);
            if (!IsAttachedConsole)
            {
                //Failed, alloc a new one
                AllocConsole();

#if DEBUG
                //Redirect stdout
                var defaultStdout = new IntPtr(7);
                var currentStdout = GetStdHandle(StdOutputHandle);

                //Reset stdout - ensures it works even when debugging
                if (currentStdout != defaultStdout)
                    SetStdHandle(StdOutputHandle, defaultStdout);

                //Reset out
                var writer = new System.IO.StreamWriter(Console.OpenStandardOutput())
                { AutoFlush = true };
                Console.SetOut(writer);

                //Same for stdin
                //BUG: NativeMethods - stdin currently doesn't want to be re-routed (same for stderr. should probably be used for PrintError calls!)
                /*var defaultStdin = new IntPtr(7);
                var currentStdin = GetStdHandle(StdInputHandle);

                //Reset stdin - ensures it works even when debugging
                if (currentStdin != defaultStdin)
                    SetStdHandle(StdInputHandle, defaultStdin);

                //Reset in
                var reader = new System.IO.StreamReader(Console.OpenStandardInput());
                Console.SetIn(reader);*/
#endif
            }

            Console.WriteLine();
        }

        internal const int VK_RETURN = 0x0D;

        internal const int WM_KEYDOWN = 0x100;

        internal const uint StdInputHandle = 0xFFFFFFF4;

        internal const uint StdOutputHandle = 0xFFFFFFF5;

        [DllImport("kernel32.dll")]
        internal static extern int AllocConsole();

        [DllImport("kernel32.dll")]
        internal static extern bool AttachConsole(int pid);

        [DllImport("kernel32.dll")]
        internal static extern bool FreeConsole();

        [DllImport("kernel32.dll")]
        internal static extern IntPtr GetStdHandle(uint nStdHandle);

        [DllImport("kernel32.dll")]
        internal static extern void SetStdHandle(uint nStdHandle, IntPtr handle);

        /// <summary>
        /// Represents a class which monitors keyboard events at a higher level than forms can.
        /// </summary>
        public class KeyboardHook : IDisposable
        {
            //UNTESTED: NativeMethods - can KeyboardHook's "Window" be replaced with a form? does it remove the global-functionality of hotkeys?

            /// <summary>
            /// Initializes a new instance of the <see cref="KeyboardHook"/> class.
            /// </summary>
            public KeyboardHook()
            {
                // register the event of the inner native window.
                this._window.KeyPressed += (s, e) => this.KeyPressed?.Invoke(this, e);
            }

            /// <summary>
            /// A hot key has been pressed.
            /// </summary>
            public event EventHandler<KeyPressedEventArgs> KeyPressed;

            /// <summary>
            /// Helper method which returns the keys bitmask excluding any modifier keys.
            /// </summary>
            public static Keys GetBaseKeys(Keys keys) => keys & ~(Keys.Control | Keys.Shift | Keys.Alt);

            /// <summary>
            /// Helper method which returns a ModifierKeys instance based on the bitmask of the keys.
            /// </summary>
            public static ModifierKeys GetModifierKeys(Keys keys)
            {
                ModifierKeys mods = 0;
                if ((keys & Keys.Control) != 0)
                    mods |= ModifierKeys.Control;
                if ((keys & Keys.Shift) != 0)
                    mods |= ModifierKeys.Shift;
                if ((keys & Keys.Alt) != 0)
                    mods |= ModifierKeys.Alt;
                return mods;
            }

            public void Dispose()
            {
                // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// <summary>
            /// Registers a hotkey in the system.
            /// </summary>
            /// <param name="modifier">The modifiers that are associated with the hotkey.</param>
            /// <param name="key">The key itself that is associated with the hotkey.</param>
            public void RegisterHotKey(ModifierKeys modifier, Keys key)
            {
                // increment the counter.
                this._currentId++;

                // register the hot key.
                if (!RegisterHotKey(this._window.Handle, this._currentId, (uint)modifier, (uint)key))
                    throw new InvalidOperationException("Couldn't register the hotkey. " + modifier + "+" + key);
            }

            /// <summary>
            /// The enumeration of possible modifiers.
            /// </summary>
            [Flags]
            public enum ModifierKeys : int
            {
                Alt = 1,
                Control = 2,
                Shift = 4,
                Win = 8
            }

            /// <summary>
            /// EventArgs for the event that is fired after the hot key has been pressed.
            /// </summary>
            public class KeyPressedEventArgs : EventArgs
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="KeyPressedEventArgs"/> class.
                /// </summary>
                /// <param name="modifier">The modifier keys which were held.</param>
                /// <param name="key">The key which was pressed.</param>
                public KeyPressedEventArgs(ModifierKeys modifier, Keys key)
                {
                    this.Modifier = modifier;
                    this.Key = key;
                }

                /// <summary>
                /// Gets the keys pressed for the hotkey.
                /// </summary>
                public Keys Key { get; }

                /// <summary>
                /// Gets the modifier keys which were held when the hotkey was triggered.
                /// </summary>
                public ModifierKeys Modifier { get; }
            }

            internal const int WM_HOTKEY = 0x0312;

            [DllImport("user32.dll")]
            internal static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

            [DllImport("user32.dll")]
            internal static extern bool UnregisterHotKey(IntPtr hWnd, int id);

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        this._window.Dispose();
                    }

                    // unregister all the registered hot keys.
                    for (int i = this._currentId; i > 0; i--)
                    {
                        UnregisterHotKey(this._window.Handle, i);
                    }

                    disposedValue = true;
                }
            }

            ~KeyboardHook()
            {
                // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
                Dispose(false);
            }

            private int _currentId;

            private Window _window = new Window();

            private bool disposedValue = false;

            /// <summary>
            /// Represents the window that is used internally to get the messages.
            /// </summary>
            private class Window : NativeWindow, IDisposable
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="Window"/> class.
                /// </summary>
                public Window()
                {
                    //Create the handle for the window.
                    this.CreateHandle(new CreateParams());
                }

                public event EventHandler<KeyPressedEventArgs> KeyPressed;

                // This code added to correctly implement the disposable pattern.
                public void Dispose()
                {
                    // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
                    Dispose(true);
                    GC.SuppressFinalize(this);
                }

                protected virtual void Dispose(bool disposing)
                {
                    if (!disposedValue)
                    {
                        this.DestroyHandle();
                        disposedValue = true;
                    }
                }

                /// <summary>
                /// Overridden to get the notifications.
                /// </summary>
                protected override void WndProc(ref Message m)
                {
                    base.WndProc(ref m);

                    //Check if we got a hotkey pressed
                    if (m.Msg == WM_HOTKEY)
                    {
                        //Get the keys
                        Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);
                        ModifierKeys modifier = (ModifierKeys)((int)m.LParam & 0xFFFF);

                        //Invoke the event to notify the parent
                        this.KeyPressed?.Invoke(this, new KeyPressedEventArgs(modifier, key));
                    }
                }
                ~Window()
                {
                    // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
                    Dispose(false);
                }

                private bool disposedValue = false; // To detect redundant calls
            }

            // To detect redundant calls
        }

        /// <summary>
        /// Class for trapping a window inside another, by changing the parent of the child window.
        /// </summary>
        public class WindowTrapper
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="WindowTrapper"/> class, set up to allow trapping a specific child control
            /// (specified using a pointer) inside of another parent control.
            /// </summary>
            /// <param name="parent"></param>
            /// <param name="child"></param>
            public WindowTrapper(Control parent, IntPtr child)
            {
                this.ParentControl = parent;
                this.ChildControl = child;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="WindowTrapper"/> class without any parent or child assigned.
            /// </summary>
            public WindowTrapper()
            {

            }

            /// <summary>
            /// Handle of the Child to trap.
            /// <para />Use the TrapChild method in order to set this child's parent to a specified parent.
            /// </summary>
            public IntPtr ChildControl { get; set; }

            /// <summary>
            /// Parent Control (mostly a Panel). Use null to set to desktop instead.
            /// <para />Use the TrapChild method in order to set the child's parent to the one specified here.
            /// </summary>
            public Control ParentControl { get; set; }

            /// <summary>
            /// Whether to restore a window's style when releasing it to the desktop, or using fallback values instead.
            /// </summary>
            public bool RestoreStyle { get; set; }

            /// <summary>
            /// Is the child currently trapped?
            /// </summary>
            public bool Trapped { get; private set; } = false;

            /// <summary>
            /// Resizes the Child to fit the size of the Parent control. Will take focus.
            /// </summary>
            public void ResizeChild()
            {
                if (!ValidParams())
                    return;

                if (this.Trapped && this._lastsize != this.ParentControl.ClientSize)
                {
                    SetWindowPos(this.ChildControl, HWND_TOP, 0, 0, this.ParentControl.ClientSize.Width, this.ParentControl.ClientSize.Height, SetWindowPosFlags.DoNotActivate);
                    this._lastsize = this.ParentControl.ClientSize;
                }
            }

            /// <summary>
            /// Traps the Child in the Parent control.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">Child hWnd may not be &lt;= 0.</exception>
            public void TrapChild()
            {
                ValidParams(true);

                this.Trapped = this.ParentControl != null;

                if (this.ParentControl == null)
                {
                    //Restore style
                    if (this.RestoreStyle)
                        SetWindowLong(this.ChildControl, -16, this._laststyle);
                    else
                        SetWindowLong(this.ChildControl, -16, (int)(WindowStyles.WS_CAPTION | WindowStyles.WS_VISIBLE | WindowStyles.WS_SYSMENU | WindowStyles.WS_MAXIMIZEBOX | WindowStyles.WS_MINIMIZEBOX | WindowStyles.WS_SIZEBOX));

                    //Desktop
                    SetParent(this.ChildControl, GetDesktopWindow());

                    //Reset to original size
                    SetWindowPos(this.ChildControl, HWND_TOP, this._originalsize.Left, this._originalsize.Top,
                        this._originalsize.Width, this._originalsize.Height, SetWindowPosFlags.DoNotActivate);
                }
                else
                {
                    //Make borderless
                    this._laststyle = GetWindowLong(this.ChildControl, -16);
                    SetWindowLong(this.ChildControl, -16, (int)(WindowStyles.WS_VISIBLE | WindowStyles.WS_BORDER | WindowStyles.WS_MAXIMIZE));

                    //Set original size
                    RECT currrect = new RECT();
                    GetWindowRect(this.ChildControl, ref currrect);
                    this._originalsize = currrect;

                    //Control
                    SetParent(this.ChildControl, this.ParentControl.Handle);
                    ResizeChild();
                }
            }

            /// <summary>
            /// List of window position flags, accepted by a <see cref="SetWindowPos"/> call.
            /// </summary>
            [Flags]
            public enum SetWindowPosFlags
            {
                /// <summary>If the calling thread and the thread that owns the window are attached to different input queues,
                /// the system posts the request to the thread that owns the window. This prevents the calling thread from
                /// blocking its execution while other threads process the request.</summary>
                /// <remarks>SWP_ASYNCWINDOWPOS</remarks>
                SynchronousWindowPosition = 0x4000,

                /// <summary>Prevents generation of the WM_SYNCPAINT message.</summary>
                /// <remarks>SWP_DEFERERASE</remarks>
                DeferErase = 0x2000,

                /// <summary>Draws a frame (defined in the window's class description) around the window.</summary>
                /// <remarks>SWP_DRAWFRAME</remarks>
                DrawFrame = 0x20,

                /// <summary>Applies new frame styles set using the SetWindowLong function. Sends a WM_NCCALCSIZE message to
                /// the window, even if the window's size is not being changed. If this flag is not specified, WM_NCCALCSIZE
                /// is sent only when the window's size is being changed.</summary>
                /// <remarks>SWP_FRAMECHANGED</remarks>
                FrameChanged = 0x20,

                /// <summary>Hides the window.</summary>
                /// <remarks>SWP_HIDEWINDOW</remarks>
                HideWindow = 0x80,

                /// <summary>Does not activate the window. If this flag is not set, the window is activated and moved to the
                /// top of either the topmost or non-topmost group (depending on the setting of the hWndInsertAfter
                /// parameter).</summary>
                /// <remarks>SWP_NOACTIVATE</remarks>
                DoNotActivate = 0x10,

                /// <summary>Discards the entire contents of the client area. If this flag is not specified, the valid
                /// contents of the client area are saved and copied back into the client area after the window is sized or
                /// repositioned.</summary>
                /// <remarks>SWP_NOCOPYBITS</remarks>
                DoNotCopyBits = 0x100,

                /// <summary>Retains the current position (ignores X and Y parameters).</summary>
                /// <remarks>SWP_NOMOVE</remarks>
                IgnoreMove = 0x2,

                /// <summary>Does not change the owner window's position in the Z order.</summary>
                /// <remarks>SWP_NOOWNERZORDER</remarks>
                DoNotChangeOwnerZOrder = 0x200,

                /// <summary>Does not redraw changes. If this flag is set, no repainting of any kind occurs. This applies to
                /// the client area, the nonclient area (including the title bar and scroll bars), and any part of the parent
                /// window uncovered as a result of the window being moved. When this flag is set, the application must
                /// explicitly invalidate or redraw any parts of the window and parent window that need redrawing.</summary>
                /// <remarks>SWP_NOREDRAW</remarks>
                DoNotRedraw = 0x8,

                /// <summary>Same as the SWP_NOOWNERZORDER flag.</summary>
                /// <remarks>SWP_NOREPOSITION</remarks>
                DoNotReposition = 0x200,

                /// <summary>Prevents the window from receiving the WM_WINDOWPOSCHANGING message.</summary>
                /// <remarks>SWP_NOSENDCHANGING</remarks>
                DoNotSendChangingEvent = 0x400,

                /// <summary>Retains the current size (ignores the cx and cy parameters).</summary>
                /// <remarks>SWP_NOSIZE</remarks>
                IgnoreResize = 0x1,

                /// <summary>Retains the current Z order (ignores the hWndInsertAfter parameter).</summary>
                /// <remarks>SWP_NOZORDER</remarks>
                IgnoreZOrder = 0x4,

                /// <summary>Displays the window.</summary>
                /// <remarks>SWP_SHOWWINDOW</remarks>
                ShowWindow = 0x40
            }

            /// <summary>
            /// List of window styles accepted by a <see cref="SetWindowLong"/> call.
            /// </summary>
            [Flags]
            public enum WindowStyles : long
            {
                WS_OVERLAPPED = 0,
                WS_POPUP = 2147483648L,
                WS_CHILD = 1073741824,
                WS_MINIMIZE = 536870912,
                WS_VISIBLE = 268435456,
                WS_DISABLED = 134217728,
                WS_CLIPSIBLINGS = 67108864,
                WS_CLIPCHILDREN = 33554432,
                WS_MAXIMIZE = 16777216,
                WS_BORDER = 8388608,
                WS_DLGFRAME = 4194304,
                WS_VSCROLL = 2097152,
                WS_HSCROLL = 1048576,
                WS_SYSMENU = 524288,
                WS_THICKFRAME = 262144,
                WS_GROUP = 131072,
                WS_TABSTOP = 65536,

                WS_MINIMIZEBOX = 131072,
                WS_MAXIMIZEBOX = 65536,

                WS_CAPTION = WS_BORDER | WS_DLGFRAME,
                WS_TILED = WS_OVERLAPPED,
                WS_ICONIC = WS_MINIMIZE,
                WS_SIZEBOX = WS_THICKFRAME,
                WS_TILEDWINDOW = WS_OVERLAPPEDWINDOW,

                WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
                WS_POPUPWINDOW = WS_POPUP | WS_BORDER | WS_SYSMENU,
                WS_CHILDWINDOW = WS_CHILD,

                WS_EX_DLGMODALFRAME = 1,
                WS_EX_NOPARENTNOTIFY = 4,
                WS_EX_TOPMOST = 8,
                WS_EX_ACCEPTFILES = 16,
                WS_EX_TRANSPARENT = 32,

                WS_EX_MDICHILD = 64,
                WS_EX_TOOLWINDOW = 128,
                WS_EX_WINDOWEDGE = 256,
                WS_EX_CLIENTEDGE = 512,
                WS_EX_CONTEXTHELP = 1024,

                WS_EX_RIGHT = 4096,
                WS_EX_LEFT = 0,
                WS_EX_RTLREADING = 8192,
                WS_EX_LTRREADING = 0,
                WS_EX_LEFTSCROLLBAR = 16384,
                WS_EX_RIGHTSCROLLBAR = 0,

                WS_EX_CONTROLPARENT = 65536,
                WS_EX_STATICEDGE = 131072,
                WS_EX_APPWINDOW = 262144,

                WS_EX_OVERLAPPEDWINDOW = WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE,
                WS_EX_PALETTEWINDOW = WS_EX_WINDOWEDGE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST,

                WS_EX_LAYERED = 524288,

                /// <summary>
                /// Disable inheritence of mirroring by children
                /// </summary>
                WS_EX_NOINHERITLAYOUT = 1048576,

                /// <summary>
                /// Right to left mirroring
                /// </summary>
                WS_EX_LAYOUTRTL = 4194304,

                WS_EX_COMPOSITED = 33554432,
                WS_EX_NOACTIVATE = 67108864
            }

            internal static readonly IntPtr HWND_BOTTOM = new IntPtr(1);

            internal static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);

            internal static readonly IntPtr HWND_TOP = new IntPtr(0);

            internal static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);

            [DllImport("user32.dll", SetLastError = true)]
            internal static extern IntPtr GetDesktopWindow();

            [DllImport("user32.dll", SetLastError = true)]
            internal static extern int GetWindowLong(IntPtr hWnd, int nIndex);

            [DllImport("user32.dll", SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            internal static extern bool GetWindowRect(IntPtr hWnd, ref RECT rect);

            [DllImport("user32.dll", SetLastError = true)]
            internal static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

            [DllImport("user32.dll", SetLastError = true)]
            internal static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

            [DllImport("user32.dll", SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            internal static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);

            /// <summary>
            /// A rectangle struct which can be passed to DLL calls.
            /// </summary>
            [StructLayout(LayoutKind.Sequential)]
            internal struct RECT
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="RECT"/> struct.
                /// </summary>
                /// <param name="rect">The rectangle used as a base.</param>
                public RECT(Rectangle rect) : this(rect.Left, rect.Top, rect.Right, rect.Bottom)
                {
                }

                /// <summary>
                /// Initializes a new instance of the <see cref="RECT"/> struct.
                /// </summary>
                /// <param name="left">The left coordinate of the rectangle.</param>
                /// <param name="top">The top coordinate of the rectangle.</param>
                /// <param name="right">The right coordinate of the rectangle.</param>
                /// <param name="bottom">The bottom coordinate of the rectangle.</param>
                public RECT(int left, int top, int right, int bottom)
                {
                    this.Left = left;
                    this.Top = top;
                    this.Right = right;
                    this.Bottom = bottom;
                }

                /// <summary>
                /// Gets or sets the left coordinate of the rectangle.
                /// </summary>
                public int Left { get; set; }

                /// <summary>
                /// Gets or sets the top coordinate of the rectangle.
                /// </summary>
                public int Top { get; set; }

                /// <summary>
                /// Gets or sets the right coordinate of the rectangle.
                /// </summary>
                public int Right { get; set; }

                /// <summary>
                /// Gets or sets the bottom coordinate of the rectangle.
                /// </summary>
                public int Bottom { get; set; }

                /// <summary>
                /// Gets or sets the x coordinate of the rectangle.
                /// </summary>
                public int X
                {
                    get => this.Left; set
                    {
                        this.Right = this.Right - this.Left + value;
                        this.Left = value;
                    }
                }

                /// <summary>
                /// Gets or sets the y coordinate of the rectangle.
                /// </summary>
                public int Y
                {
                    get => this.Top;
                    set
                    {
                        this.Bottom = this.Bottom - this.Top + value;
                        this.Top = value;
                    }
                }

                /// <summary>
                /// Gets or sets the height of the rectangle.
                /// </summary>
                public int Height
                {
                    get => this.Bottom - this.Top;
                    set => this.Bottom = value + this.Top;
                }

                /// <summary>
                /// Gets or sets the width of the rectangle.
                /// </summary>
                public int Width
                {
                    get => this.Right - this.Left;
                    set => this.Right = value + this.Left;
                }

                /// <summary>
                /// Gets or sets the location of the rectangle.
                /// </summary>
                public Point Location
                {
                    get => new Point(this.Left, this.Top); set
                    {
                        this.Right = this.Right - this.Left + value.X;
                        this.Bottom = this.Bottom - this.Top + value.Y;
                        this.Left = value.X;
                        this.Top = value.Y;
                    }
                }

                /// <summary>
                /// Gets or sets the size of the rectangle.
                /// </summary>
                public Size Size
                {
                    get => new Size(this.Width, this.Height); set
                    {
                        this.Right = value.Width + this.Left;
                        this.Bottom = value.Height + this.Top;
                    }
                }

                /// <summary>
                /// Performs an implicit conversion from <see cref="RECT"/> to <see cref="Rectangle"/>.
                /// </summary>
                /// <param name="rect">The rect.</param>
                /// <returns>
                /// The result of the conversion.
                /// </returns>
                public static implicit operator Rectangle(RECT rect)
                {
                    return new Rectangle(rect.Left, rect.Top, rect.Width, rect.Height);
                }

                /// <summary>
                /// Performs an implicit conversion from <see cref="Rectangle"/> to <see cref="RECT"/>.
                /// </summary>
                /// <param name="rect">The rect.</param>
                /// <returns>
                /// The result of the conversion.
                /// </returns>
                public static implicit operator RECT(Rectangle rect)
                {
                    return new RECT(rect.Left, rect.Top, rect.Right, rect.Bottom);
                }

                /// <summary>
                /// Implements the operator ==.
                /// </summary>
                /// <param name="r1">The r1.</param>
                /// <param name="r2">The r2.</param>
                /// <returns>
                /// The result of the operator.
                /// </returns>
                public static bool operator ==(RECT r1, RECT r2)
                {
                    return r1.Equals(r2);
                }

                /// <summary>
                /// Implements the operator !=.
                /// </summary>
                /// <param name="r1">The r1.</param>
                /// <param name="r2">The r2.</param>
                /// <returns>
                /// The result of the operator.
                /// </returns>
                public static bool operator !=(RECT r1, RECT r2)
                {
                    return !r1.Equals(r2);
                }

                /// <summary>
                /// Returns a <see cref="System.String" /> that represents this instance.
                /// </summary>
                /// <returns>
                /// A <see cref="System.String" /> that represents this instance.
                /// </returns>
                public override string ToString() => "{Left: " + this.Left + "; " + "Top: " + this.Top + "; Right: " + this.Right + "; Bottom: " + this.Bottom + "}";

                /// <summary>
                /// Equalses the specified rect.
                /// </summary>
                /// <param name="rect">The rect.</param>
                public bool Equals(RECT rect)
                {
                    return rect.Left == this.Left && rect.Top == this.Top && rect.Right == this.Right && rect.Bottom == this.Bottom;
                }

                /// <summary>
                /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
                /// </summary>
                /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
                /// <returns>
                ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
                /// </returns>
                public override bool Equals(object obj)
                {
                    if (obj is RECT)
                    {
                        return Equals((RECT)obj);
                    }
                    else if (obj is Rectangle)
                    {
                        return Equals(new RECT((Rectangle)obj));
                    }

                    return false;
                }

                /// <summary>
                /// Returns the hash code for this instance.
                /// </summary>
                /// <returns>
                /// A 32-bit signed integer that is the hash code for this instance.
                /// </returns>
                public override int GetHashCode() => ((Rectangle)this).GetHashCode();
            }

            private Size _lastsize;
            private int _laststyle;
            private RECT _originalsize;

            /// <summary>
            /// Checks if the parameters are correctly set.
            /// </summary>
            /// <param name="throwex">Whether to throw an exception if they're not, or simply return false.</param>
            /// <exception cref="ArgumentOutOfRangeException">Child hWnd may not be &lt;= 0.</exception>
            private bool ValidParams(bool throwex = false)
            {
                if (this.ChildControl.ToInt64() <= 0)
                {
                    if (throwex)
                        throw new ArgumentOutOfRangeException("Child hWnd may not be <= 0.");
                    return false;
                }
                return true;
            }
        }

        [DllImport("user32.dll", EntryPoint = "PostMessageA")]
        internal static extern bool PostMessage(IntPtr hWnd, uint msg, int wParam, int lParam);

        [DllImport("user32.dll")] //Start/Stop drawing controls
        internal static extern int SendMessage(IntPtr hWnd, int wMsg, bool wParam, int lParam);

        [DllImport("user32.dll")] //ProgressBar State
        internal static extern IntPtr SendMessage(IntPtr hWnd, uint wMsg, IntPtr wParam, IntPtr lParam);

        /// <summary>
        /// Support for symbolic links. Taken from https://github.com/michaelmelancon/symboliclinksupport (MIT License)
        /// </summary>
        internal static class SymbolicLink
        {
            public static void CreateDirectoryLink(string linkPath, string targetPath)
            {
                if (!CreateSymbolicLink(linkPath, targetPath, targetIsADirectory) || Marshal.GetLastWin32Error() != 0)
                {
                    try
                    {
                        Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
                    }
                    catch (COMException exception)
                    {
                        throw new IOException(exception.Message, exception);
                    }
                }
            }

            public static void CreateFileLink(string linkPath, string targetPath)
            {
                if (!CreateSymbolicLink(linkPath, targetPath, targetIsAFile))
                {
                    Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
                }
            }

            public static bool Exists(string path)
            {
                if (!Directory.Exists(path) && !File.Exists(path))
                {
                    return false;
                }
                string target = GetTarget(path);
                return target != null;
            }

            public static string GetTarget(string path)
            {
                SymbolicLinkReparseData reparseDataBuffer;

                using (SafeFileHandle fileHandle = GetFileHandle(path))
                {
                    if (fileHandle.IsInvalid)
                    {
                        Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
                    }
                    int outBufferSize = Marshal.SizeOf<SymbolicLinkReparseData>();
                    IntPtr outBuffer = IntPtr.Zero;
                    try
                    {
                        outBuffer = Marshal.AllocHGlobal(outBufferSize);
                        bool success = DeviceIoControl(
                            fileHandle.DangerousGetHandle(), ioctlCommandGetReparsePoint, IntPtr.Zero, 0,
                            outBuffer, outBufferSize, out int bytesReturned, IntPtr.Zero);

                        fileHandle.Dispose();

                        if (!success)
                        {
                            if (((uint)Marshal.GetHRForLastWin32Error()) == pathNotAReparsePointError)
                            {
                                return null;
                            }
                            Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
                        }

                        reparseDataBuffer = Marshal.PtrToStructure<SymbolicLinkReparseData>(outBuffer);
                    }
                    finally
                    {
                        Marshal.FreeHGlobal(outBuffer);
                    }
                }
                if (reparseDataBuffer.ReparseTag != symLinkTag)
                {
                    return null;
                }

                string target = System.Text.Encoding.Unicode.GetString(reparseDataBuffer.PathBuffer,
                    reparseDataBuffer.PrintNameOffset, reparseDataBuffer.PrintNameLength);

                return target;
            }

            [DllImport("kernel32.dll", SetLastError = true)]
            internal static extern SafeFileHandle CreateFile(
                string lpFileName,
                uint dwDesiredAccess,
                uint dwShareMode,
                IntPtr lpSecurityAttributes,
                uint dwCreationDisposition,
                uint dwFlagsAndAttributes,
                IntPtr hTemplateFile);

            [DllImport("kernel32.dll", SetLastError = true)]
            internal static extern bool CreateSymbolicLink(string lpSymlinkFileName, string lpTargetFileName, int dwFlags);

            [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            internal static extern bool DeviceIoControl(
                IntPtr hDevice,
                uint dwIoControlCode,
                IntPtr lpInBuffer,
                int nInBufferSize,
                IntPtr lpOutBuffer,
                int nOutBufferSize,
                out int lpBytesReturned,
                IntPtr lpOverlapped);

            /// <remarks>
            /// Refer to http://msdn.microsoft.com/en-us/library/windows/hardware/ff552012%28v=vs.85%29.aspx
            /// </remarks>
            [StructLayout(LayoutKind.Sequential)]
            internal struct SymbolicLinkReparseData
            {
                // Not certain about this!
                private const int maxUnicodePathLength = 260 * 2;

                public uint ReparseTag;
                public ushort ReparseDataLength;
                public ushort Reserved;
                public ushort SubstituteNameOffset;
                public ushort SubstituteNameLength;
                public ushort PrintNameOffset;
                public ushort PrintNameLength;
                public uint Flags;

                [MarshalAs(UnmanagedType.ByValArray, SizeConst = maxUnicodePathLength)]
                public byte[] PathBuffer;
            }

            private const uint fileFlagsForOpenReparsePointAndBackupSemantics = 0x02200000;
            private const uint genericReadAccess = 0x80000000;
            private const int ioctlCommandGetReparsePoint = 0x000900A8;
            private const uint openExisting = 0x3;
            private const uint pathNotAReparsePointError = 0x80071126;
            private const uint shareModeAll = 0x7; // Read, Write, Delete
            private const uint symLinkTag = 0xA000000C;
            private const int targetIsADirectory = 1;
            private const int targetIsAFile = 0;
            private static SafeFileHandle GetFileHandle(string path)
            {
                return CreateFile(path, genericReadAccess, shareModeAll, IntPtr.Zero, openExisting,
                    fileFlagsForOpenReparsePointAndBackupSemantics, IntPtr.Zero);
            }
        }
    }
}
