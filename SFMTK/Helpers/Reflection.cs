﻿using SFMTK.Modules;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Linq;

namespace SFMTK
{
    /// <summary>
    /// Helper class for reflection methods.
    /// </summary>
    public static class Reflection
    {
        /// <summary>
        /// General <see cref="BindingFlags"/> for reflection, including all access modifiers and both instance and static members.
        /// </summary>
        public static BindingFlags GeneralFlags => BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static;

        /// <summary>
        /// Gets SFMTK's executing <see cref="Assembly"/>.
        /// </summary>
        public static Assembly SFMTKAssembly { get; } = typeof(SFMTK.Program).Assembly;

        /// <summary>
        /// Returns an instance of each class which inherits from the specified type <typeparamref name="T" /> using the default constructor of that type.
        /// </summary>
        /// <typeparam name="T">The type to get subclass instances for.</typeparam>
        /// <param name="checkmodules">If set, doesn't only check by the assembly the type comes from, but also all module assemblies.</param>
        /// <param name="onlymodules">If <paramref name="checkmodules"/> is set, and this parameter is set to <c>true</c>, all other assembly checks are skipped.</param>
        public static IEnumerable<T> GetSubclassInstances<T>(ModuleScope checkmodules, bool onlymodules = false)
        {
            foreach (var t in GetSubTypes(typeof(T), checkmodules, onlymodules))
            {
                T value = default(T);
                try
                {
                    value = (T)Activator.CreateInstance(t, true);
                }
                catch (MissingMethodException)
                {
                    //Missing constructor (due to being an abstract class or having no public default constructor) - ignore!
                }
                if (value != null)
                    yield return value;
            }
        }

        /// <summary>
        /// Returns all classes which inhert from the specified <see cref="Type"/>, itself not included.
        /// </summary>
        /// <param name="t">The type to check inheritance for.</param>
        /// <param name="checkmodules">If set, doesn't only check by the assembly the type comes from, but also all module assemblies.</param>
        /// <param name="onlymodules">If <paramref name="checkmodules"/> is set, and this parameter is set to <c>true</c>, all other assembly checks are skipped.</param>
        /// <returns></returns>
        public static IEnumerable<Type> GetSubTypes(Type t, ModuleScope checkmodules, bool onlymodules = false)
        {
            if (checkmodules != ModuleScope.None)
            {
                //Check multiple assemblies (SFMTK assembly, type's assembly and module assemblies)
                var baselist = onlymodules ? new Assembly[] { } : new[] { t.Assembly, SFMTKAssembly };
                var asslist = new List<Assembly>(baselist)
                    .Concat(Program.MainMM.GetModules(checkmodules)
                    .Select(m => m.GetAssembly()))
                    .Where(a => a != null)
                    .Distinct(AssemblyComparer);

                foreach (var ass in asslist)
                    foreach (var ty in GetSubTypes(ass, t))
                        yield return ty;
            }
            else
            {
                //Check assembly that the type comes from
                foreach (var ty in GetSubTypes(t.Assembly, t))
                    yield return ty;
            }
        }

        /// <summary>
        /// Returns an instance of each type which inherits from the specified type <typeparamref name="T" /> located in the specified <see cref="Assembly"/>
        /// using the default constructor of that type.
        /// </summary>
        /// <typeparam name="T">The type to get subclass instances for.</typeparam>
        /// <param name="a">The assembly to get check the types for. This does not have to be the same as the assembly of <typeparamref name="T"/>.</param>
        public static IEnumerable<T> GetSubclassInstances<T>(Assembly a)
        {
            foreach (var t in GetSubTypes(a, typeof(T)))
            {
                T value = default(T);
                try
                {
                    value = (T)Activator.CreateInstance(t, true);
                }
                catch (MissingMethodException)
                {
                    //Missing constructor (due to being an abstract class or having no public default constructor) - ignore!
                }
                if (value != null)
                    yield return value;
            }
        }

        /// <summary>
        /// Returns all classes which inhert from the specified <see cref="Type"/> and are located in the specified <see cref="Assembly"/>, itself not included.
        /// </summary>
        /// <param name="a">The assembly to get check the types for. This does not have to be the same as the assembly of <paramref name="t"/>.</param>
        /// <param name="t">The type to check inheritance for.</param>
        public static IEnumerable<Type> GetSubTypes(Assembly a, Type t)
        {
            foreach (var ty in a.GetTypes())
            {
                if (ty.IsSubclassOf(t))
                    yield return ty;
            }
        }

        /// <summary>
        /// Get a <see cref="Type"/> by providing its full name (excluding assembly info).
        /// <para/>If you wish to get a type of which the namespace is not fully known, use <see cref="GetModuleType(string, bool, bool, bool)"/> instead.
        /// </summary>
        public static Type GetType(string name, bool includeModules = true, bool throwOnError = true, bool ignoreCase = false)
        {
            if (string.IsNullOrEmpty(name))
                return null;

            //Try SFMTK assembly first
            var type = GetType(name, SFMTKAssembly, ignoreCase);
            if (type != null)
                return type;

            //Go through modules in loading order
            foreach (var mod in Program.MainMM.LoadedModules)
            {
                var asm = mod.GetAssembly();
                if (asm == null)
                    continue;

                type = GetType(name, asm, ignoreCase);
                if (type != null)
                    return type;
            }

            //Tried everything, determine end-result
            if (throwOnError)
            {
                var who = includeModules ? "No loaded modules or SFMTK's assembly knows" : "SFMTK does not know";
                throw new TypeLoadException($"{who} the type \"{name}\".");
            }
            return null;
        }

        private static Type GetType(string name, Assembly ass, bool ignoreCase) => ass.GetType(name, false, ignoreCase);

        /// <summary>
        /// Gets a <see cref="Type"/> by providing its name as well as the in-common namespace.
        /// </summary>
        /// <example>If you wish to get a widget named Test, you specify <c>Widgets.Test</c> as the name.
        /// If a module uses the namespace <c>SFMTK.Modules.Foo</c>, the full type name would be <c>SFMTK.Modules.Foo.Widgets.Test</c>.</example>
        public static Type GetModuleType(string name, bool includeSFMTK = true, bool throwOnError = true, bool ignoreCase = false)
        {
            if (string.IsNullOrEmpty(name))
                return null;

            //Try SFMTK assembly first
            var type = SFMTKAssembly.GetType($"SFMTK.{name}", false, ignoreCase);
            if (type != null)
                return type;

            //Go through modules in loading order
            foreach (var mod in Program.MainMM.LoadedModules)
            {
                type = GetType(mod);
                if (type != null)
                    return type;
            }

            //Tried everything, determine end-result
            if (throwOnError)
                throw new TypeLoadException($"No loaded modules know the type \"{name}\".");
            return null;



            Type GetType(IModule mod) => mod.GetAssembly().GetType($"{mod.GetNamespace()}.{name}", false, ignoreCase);
        }

        /// <summary>
        /// Resolves the <see cref="Assembly"/> in question by trying both SFMTK's directory and the relative directory for dependencies.
        /// </summary>
        internal static Assembly OnModuleAssemblyResolve(object sender, ResolveEventArgs e)
        {
            string probingPath;
            var assName = new AssemblyName(e.Name);
            if (e.RequestingAssembly != null)
            {
                //Get dependency using relative folder
                probingPath = Path.GetDirectoryName(e.RequestingAssembly.Location);
            }
            else
            {
                //Try same-name dll folder (bug in loading mechanism, it seems)
                var shitname = Path.Combine(Application.StartupPath, "Modules", assName.Name, assName.Name + ".dll");
                if (File.Exists(shitname))
                {
                    var assy = Assembly.LoadFile(shitname);
                    return assy;
                }

                //Get base library using SFMTK's folder
                probingPath = Application.StartupPath;
            }

            var newPath = Path.Combine(probingPath, assName.Name);
            if (!newPath.EndsWith(".dll"))
            {
                newPath = newPath + ".dll";
            }
            if (File.Exists(newPath))
            {
                var assy = Assembly.LoadFile(newPath);
                return assy;
            }

            //Failed!
            return null;
        }

        /// <summary>
        /// Gets the default equality comparer for comparing two <see cref="Assembly"/> instances.
        /// </summary>
        public static IEqualityComparer<Assembly> AssemblyComparer { get; } = new AssemblyEqualityComparer();

        private class AssemblyEqualityComparer : IEqualityComparer<Assembly>
        {
            /// <summary>
            /// Determines whether the specified objects are equal.
            /// </summary>
            /// <param name="x">The first object of type <paramref name="T" /> to compare.</param>
            /// <param name="y">The second object of type <paramref name="T" /> to compare.</param>
            /// <returns>
            /// true if the specified objects are equal; otherwise, false.
            /// </returns>
            public bool Equals(Assembly x, Assembly y) => x.FullName == y.FullName;

            /// <summary>
            /// Returns a hash code for this instance.
            /// </summary>
            /// <param name="obj">The object.</param>
            /// <returns>
            /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
            /// </returns>
            public int GetHashCode(Assembly obj) => obj.GetHashCode();
        }
    }
}
