﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using SFMTK.Contents;
using SFMTK.Widgets;
using SFMTK.Commands;

namespace SFMTK
{
    /// <summary>
    /// Management class for easy docking.
    /// </summary>
    public class DockPanelManager
    {
        /// <summary>
        /// Creates a DockPanelManager which manages the specified panel.
        /// </summary>
        public DockPanelManager(DockPanel panel, CommandManager cm)
        {
            this.DockPanel = panel;
            this.CommandManager = cm;
            DockPanelManagers.Add(this);
        }

        /// <summary>
        /// Gets the CommandManager assigned to this DockPanelManager.
        /// </summary>
        public CommandManager CommandManager { get; }

        /// <summary>
        /// List of all initialized DockPanelManagers.
        /// </summary>
        public static List<DockPanelManager> DockPanelManagers { get; } = new List<DockPanelManager>();

        /// <summary>
        /// List of all dock contents on the dockpanel.
        /// </summary>
        public IEnumerable<DockContent> DockContents => this.DockPanel.Contents.Cast<DockContent>();

        /// <summary>
        /// List of dock contents with the specified type.
        /// </summary>
        /// <typeparam name="T">The type of DockContent to return.</typeparam>
        /// <param name="exact">If set to <c>true</c>, only return exactly the specified type T, not allowing any subclasses.</param>
        public IEnumerable<T> DockContentsTyped<T>(bool exact = false)
        {
            if (exact)
                return this.DockContents?.Where(x => x.GetType() == typeof(T)).Cast<T>();
            else
                return this.DockContents?.OfType<T>();
        }

        /// <summary>
        /// The dockpanel that should be managed by this manager.
        /// </summary>
        public DockPanel DockPanel { get; }

        /// <summary>
        /// Gets the form of the DockPanel.
        /// </summary>
        public Form Form => this.DockPanel.FindForm();

        /// <summary>
        /// List of content info widgets whose contents are unsaved.
        /// </summary>
        public IEnumerable<ContentInfoWidget> UnsavedContents => this.DockContentsTyped<ContentInfoWidget>().Where(w => w.Content != null && w.Content.IsDirty);

        /// <summary>
        /// Gets the currently selected ContentInfoWidget.
        /// </summary>
        public ContentInfoWidget ActiveContent => this.DockPanel.ActiveDocument as ContentInfoWidget ?? this.DockPanel.ActiveContent as ContentInfoWidget;

        /// <summary>
        /// Gets all opened content.
        /// </summary>
        public IEnumerable<Content> OpenContent => this.DockContentsTyped<ContentInfoWidget>().Select(w => w.Content);

        /// <summary>
        /// Returns a default name for a new content. May be duplicate if not all contents exist in form of a widget on this DockPanel.
        /// </summary>
        /// <param name="content">The content to get the name for.</param>
        public string GetNewContentName(Content content)
        {
            return StringHelper.GetNewUniqueName(Content.GetTypeName(content), content.MainExtension,
                this.OpenContent.Where(c => c.FakeFile && c?.GetType() == content?.GetType()).Select(c => c?.GetFormattedName(Properties.TitleFormat.FileName, false)));
        }

        /// <summary>
        /// Adds a new dock content to the DockPanel, no matter if it exists or not.
        /// <para/>Default behaviour is to only add the content if it does not already exist in the dock panel (see <see cref="TryAdd"/>)
        /// </summary>
        /// <param name="dock">The DockContent to add.</param>
        /// <exception cref="ArgumentNullException">The specified widget is null.</exception>
        public void Add(DockContent dock)
        {
            if (dock == null)
                throw new ArgumentNullException(nameof(dock), "Tried to add a null DockContent.");

            //Add TabContextMenuStrip
            if (dock.TabPageContextMenuStrip == null)
                dock.TabPageContextMenuStrip = new ContextMenuStrip();
            dock.TabPageContextMenuStrip.Items.AddRange(this.GetTabPageContextMenuStripItems(dock));
            
            //Hover texts
            dock.TabPageContextMenuStrip.ShowItemToolTips = false;
            foreach (var item in dock.TabPageContextMenuStrip.Items.OfType<ToolStripMenuItem>())
            {
                Program.MainForm.AddStatusHoverInfo(item);
            }

            //Enabled state
            CommandManager.UpdateCommandsEnabled(nameof(Forms.Main), dock.TabPageContextMenuStrip, false);
            //UNTESTED: DockPanelManager - loading context menu strip from persist?

            if (dock is Widget w)
                w.InitializeInstance();

            //Show on panel
            dock.Show(this.DockPanel);

            //Apply theme to new content
            WindowManager.AfterCreateWidget(dock);
        }

        private ToolStripItem[] GetTabPageContextMenuStripItems(DockContent content)
        {
            var items = new List<ToolStripItem>();

            //TODO: DockPanelManager - assign a ContextMenuStrip as a property, and copy its Items, to allow user to configure this strip as well
            //TODO: DockPanelManager - get certain menu items off here into the Window menu (main menu strip) as well
            if (content is ContentInfoWidget)
            {
                //Document uses different context menu
                items.Add(this.CommandManager.CreateToolStripMenuItem(typeof(SaveFileCommand)));
                items.Add(new ToolStripSeparator());
                items.Add(this.CommandManager.CreateToolStripMenuItem(typeof(CopyFullPathCommand)));
                items.Add(this.CommandManager.CreateToolStripMenuItem(typeof(CopyLocalPathCommand)));
                items.Add(this.CommandManager.CreateToolStripMenuItem(typeof(OpenContainingFolderCommand)));
                items.Add(new ToolStripSeparator());
                items.Add(this.CommandManager.CreateToolStripMenuItem(typeof(CloseTabCommand)));
            }
            else
            {
                //Generic context menu
                items.Add(this.CommandManager.CreateToolStripMenuItem(typeof(RenameTabCommand)));
                items.Add(new ToolStripSeparator());
                items.Add(this.CommandManager.CreateToolStripMenuItem(typeof(CloseTabCommand)));
            }

            return items.ToArray();
        }

        /// <summary>
        /// Adds a new dock content to the DockPanel, displaying the specified Content's widget inside.
        /// If the content is already shown in the DockPanel, gives focus to it, unless force is set to <c>true</c>.
        /// <para />Returns whether the dock content was created.
        /// </summary>
        /// <param name="content">The Content, of which its widget is tried to be added or focussed.</param>
        /// <param name="forceAdd">If set to <c>true</c>, adds the widget even if it already exists.</param>
        /// <exception cref="ArgumentNullException">Tried to add a null ContentInfoWidget.</exception>
        public bool AddContent(Content content, bool forceAdd = false)
            => AddContent(content?.CreateContainer() ?? throw new ArgumentNullException(nameof(content), "Tried to add a null ContentWidget."), forceAdd);

        /// <summary>
        /// Adds a new dock content to the DockPanel, displaying the specified ContentWidget inside.
        /// If the content is already shown in the DockPanel, gives focus to it, unless force is set to <c>true</c>.
        /// <para />Returns whether the dock content was created.
        /// </summary>
        /// <param name="ciw">The ContentInfoWidget to add or focus.</param>
        /// <param name="forceAdd">If set to <c>true</c>, adds the widget even if it already exists.</param>
        /// <exception cref="ArgumentNullException">Tried to add a null ContentWidget.</exception>
        public bool AddContent(ContentInfoWidget ciw, bool forceAdd = false)
        {
            var ccont = ciw.Content;
            ciw.ShowHint = ciw.ContentWidget.PreferredStartPosition;

            var res = true;
            if (forceAdd)
                this.Add(ciw);
            else
                res = this.TryAddInternal(ciw, widget => widget.Content?.FilePath == ccont.FilePath);

            if (res)
            {
                //HACK: DockPanelManager - Add event directly when created, referring to main form
                ccont.FilePathChanged += (s, e) => Program.MainForm.UpdateTitle();
                ccont.FilePathChanged += (s, e) => CommandManager.UpdateCommandsEnabled(nameof(Forms.Main), ciw.TabPageContextMenuStrip, false);
                ccont.FakeFileChanged += (s, e) => CommandManager.UpdateCommandsEnabled(nameof(Forms.Main), ciw.TabPageContextMenuStrip, false);

                //Update error handlers
                ccont.ErrorHandlers.ErrorFound += Program.MainEM.OnErrorsChanged;
                foreach (var handler in Errors.ErrorHandler.GetByAttribute(ccont.GetType()))
                {
                    handler.Data = ccont.Data;
                    ccont.ErrorHandlers.Add(handler);
                }
            }
            return res;
        }

        /// <summary>
        /// Adds the specified dock content to the DockPanel if one of that type does not already exist.
        /// Else grants the first of its type in the list focus. Behaviour is overwritten by the MultipleDocks settings key.
        /// <para />Returns whether the dock content was created.
        /// </summary>
        /// <param name="dock">The DockContent to try adding.</param>
        public bool TryAdd(DockContent dock)
            => this.TryAddInternal(dock, widget => widget.GetType() == dock.GetType());

        /// <summary>
        /// Adds the specified dock content to the DockPanel if the exists function does not return any.
        /// Else grants the exists entry focus. Behaviour is overwritten by the MultipleDocks settings key.
        /// <para />Returns whether the dock content was created.
        /// </summary>
        /// <param name="dock">The DockContent you are trying to add.</param>
        /// <param name="exists">A function which would get the first existing DockContent, which will be focussed if any is found.</param>
        /// <exception cref="ArgumentNullException">dc - Tried to add a null DockContent.</exception>
        private bool TryAddInternal<T>(T dock, Func<T, bool> exists) where T : DockContent
        {
            if (dock == null)
                throw new ArgumentNullException(nameof(dock), "Tried to add a null DockContent.");

            //Skip if multiple docks are allowed
            if (!Properties.Settings.Default.MultipleDocks)
            {
                var existDC = this.DockContents.OfType<T>().FirstOrDefault(exists);

                if (existDC != null)
                {
                    //Exists, give focus
                    this.Show(existDC);

                    return false;
                }
            }

            //Does not exist, add
            this.Add(dock);
            return true;
        }

        /// <summary>
        /// Focuses the specified DockContent if it is on the main DockPanel.
        /// </summary>
        /// <param name="dock">The DockContent to focus.</param>
        public void Show(DockContent dock)
        {
            if (dock == null)
                throw new ArgumentNullException(nameof(dock), "Tried to show a null DockContent.");

            //Expand autohide
            if (DockHelper.IsDockStateAutoHide(dock.DockState))
                this.DockPanel.ActiveAutoHideContent = dock;

            //Give focus
            dock.Activate();
        }

        /// <summary>
        /// Returns a new dock content instance based on the specified persistString. If a Widget is loaded, will already be initialized with the persist data.
        /// <para/> May return null when expected parsing errors have occurred.
        /// </summary>
        /// <param name="persistString">The persist string that was stored with this DockContent or Widget.</param>
        public DockContent GetFromPersist(string persistString)
        {
            if (string.IsNullOrWhiteSpace(persistString))
                throw new ArgumentNullException(nameof(persistString), "Tried to add a null DockContent from persist string.");

            try
            {
                //TODO: DockPanelManager - persiststring should include name of module that this dockcontent type is from (in order to avoid naming conflicts, and invalid loads!)
                //Load persist data
                var index = persistString.IndexOf(' ');
                string type;
                string persist;
                if (index < 0)
                {
                    //Only type name as persist data
                    type = persistString;
                    persist = null;
                }
                else
                {
                    //Found extra persist data
                    type = persistString.Substring(0, index);
                    persist = persistString.Substring(index + 1);
                }

                //Create the dock content
                var dc = (DockContent)Activator.CreateInstance(Reflection.GetType(type));

                //Further initialization
                if (dc is Widget w)
                {
                    //Check for a successful initialize
                    if (!w.InitializePersist(persist))
                        return null;
                }

                //Has to apply the theme when created
                UITheme.ApplyTheme(dc);
                return dc;
            }
            catch (Exception ex)
            {
                throw new FormatException("Unable to deserialize persist string: " + persistString, ex);
            }
        }

        /// <summary>
        /// Closes all opened DockContent, allowing for layout and theme changes.
        /// </summary>
        public void CloseAll()
        {
            //TODO: DockPanelManager - hope to find a better solution to applying themes and layouts, since reopening certain widgets may be troublesome
            foreach (var dc in this.DockContents.ToList())
            {
                dc.Close();
            }
            while (this.DockPanel.FloatWindows.Any())
            {
                this.DockPanel.FloatWindows[0].Close();
            }
            while (this.DockPanel.Panes.Any())
            {
                this.DockPanel.Panes[0].Dispose();
            }
        }

        /// <summary>
        /// Temporarily closes all DockPanels, allowing you to reopen them using a subsequent <see cref="TemporaryOpenContent"/> call.
        /// </summary>
        public void TemporaryCloseContent()
        {
            this.IsReloadingContent = true;

            //Check if any panels are open
            if (this.DockPanel.Contents.Count == 0 && this.DockPanel.Panes.Count == 0 && this.DockPanel.FloatWindows.Count == 0)
            {
                //Fake close
                this._tempcontent = new TemporaryContent(null);
                return;
            }

            //Close all
            this._tempcontent = new TemporaryContent(this);
            this.CloseAll();
        }

        /// <summary>
        /// Opens all temporarily closed DockPanels, but only if you closed them before using a <see cref="TemporaryCloseContent"/> call.
        /// </summary>
        /// <exception cref="InvalidOperationException">Tried to call TemporaryOpenContent without calling TemporaryCloseContent beforehand!</exception>
        public void TemporaryOpenContent()
        {
            try
            {
                //Verify
                if (this._tempcontent == null)
                    throw new InvalidOperationException("Cannot call TemporaryOpenContent without calling TemporaryCloseContent beforehand!");

                if (this._tempcontent.DockContents == null)
                    return;

                //Reopen!
                this._tempcontent.TemporaryLayout.LoadLayout(this);
            }
            finally
            {
                //Clear tempcontent
                this.IsReloadingContent = false;
                this._tempcontent = null;
            }
        }

        private TemporaryContent _tempcontent;

        /// <summary>
        /// Gets whether a <see cref="TemporaryCloseContent"/> call has stored any temporary content for later reloading.
        /// </summary>
        public bool IsReloadingContent { get; private set; }

        private class TemporaryContent
        {
            public TemporaryContent(DockPanelManager dpm)
            {
                if (dpm == null)
                    return;

                this.DockContents = dpm.DockContents.ToList();
                this.TemporaryLayout = new DockLayout("TEMP", Path.GetTempFileName());
                this.TemporaryLayout.SaveLayout(dpm);
            }

            public IEnumerable<DockContent> DockContents { get; }

            public DockLayout TemporaryLayout { get; }
        }
    }
}
