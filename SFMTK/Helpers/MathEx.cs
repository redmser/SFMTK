﻿using System;
using System.Linq;

namespace SFMTK
{
    /// <summary>
    /// Helper class for more complex mathematical operations.
    /// </summary>
    public static class MathEx
    {
        /// <summary>
        /// Returns the input value clamped to the minimum and maximum values.
        /// </summary>
        /// <param name="input">Value to clamp.</param>
        /// <param name="min">Lowest value for the input.</param>
        /// <param name="max">Highest value for the input.</param>
        public static decimal Clamp(decimal input, decimal min, decimal max) => Math.Max(Math.Min(input, max), min);

        /// <summary>
        /// Returns the input value clamped to the minimum and maximum values.
        /// </summary>
        /// <param name="input">Value to clamp.</param>
        /// <param name="min">Lowest value for the input.</param>
        /// <param name="max">Highest value for the input.</param>
        public static float Clamp(float input, float min, float max) => Math.Max(Math.Min(input, max), min);

        /// <summary>
        /// Calculates how many lines the specified text needs when rendering it using a Write call, given a certain buffer width.
        /// </summary>
        /// <remarks>Its own line is included, meaning even an empty string will return 1.</remarks>
        /// <param name="text">Text to be displayed.</param>
        /// <param name="width">Width of the console buffer. Use -1 to get current width instead.</param>
        public static int ConsoleLinesNeeded(string text, int width = -1)
        {
            if (string.IsNullOrEmpty(text))
                return 1;

            if (width <= 0)
                width = Console.BufferWidth;

            var split = text.Replace("\r", "").Split(new[] { '\n' });
            var wrap = split.Sum(ln => ln.Length / width);
            //FIXME: MathEx ConsoleLinesNeeded - perfectdivision was added to counter certain edge-cases found in tests, but does not feel like the correct solution
            //  to the problem. Why does it happen, and how can it be ensured that this is always correctly calculated?
            var perfectdivision = split.Length == 1 && wrap > 1 && (width % wrap) == 0;
            return split.Length + wrap - (perfectdivision ? 1 : 0);
        }
    }
}
