﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SFMTK
{
    /// <summary>
    /// Container for all Extension methods.
    /// </summary>
    public static class Extensions
    {
        // Notes to anyone looking to extend the Extensions class:
        // - May only contain extension methods (first parameter "this").
        // - If it makes more sense to have an extension method in a Helper class, do so, since they can be statically invoked as well.
        // - Any other general-use methods should be placed into respective Helper classes.
        // - Helper classes should not be part of any Helpers namespace, since that would be counterproductive in practice.

        /// <summary>
        /// Returns info about the exception, in a cleanly formatted way.
        /// <para />Includes message, stack trace with line numbers, and recurses inner exceptions.
        /// </summary>
        /// <param name="ex">The exception to display the info of.</param>
        /// <param name="includeStackTrace">If set to <c>true</c>, includes the stack trace as well.</param>
        public static string GetInfo(this Exception ex, bool includeStackTrace = true)
        {
            var exinfo = "None";
            if (ex != null)
            {
                //Try to produce stacktrace
                exinfo = $"{ex.GetType().FullName}: {ex.Message}";
                if (!includeStackTrace)
                    return exinfo;

                try
                {
                    var st = new System.Diagnostics.StackTrace(ex, true);
                    exinfo += "\n" + st.ToString();
                }
                catch (Exception)
                {
                    exinfo = ex.ToString();
                }
            }
            return exinfo;
        }
        
        /// <summary>
        /// Returns the object with the highest value of the given property.
        /// </summary>
        /// <typeparam name="T">Type of objects.</typeparam>
        /// <param name="enumerable">The enumerable of objects.</param>
        /// <param name="property">The property to get the maximum of.</param>
        public static T MaxOf<T>(this IEnumerable<T> enumerable, Func<T, int> property)
        {
            if (enumerable == null || !enumerable.Any())
                return default(T);
            return enumerable.Aggregate((i1, i2) => property(i1) > property(i2) ? i1 : i2);
        }

        /// <summary>
        /// Returns the object with the lowest value of the given property.
        /// </summary>
        /// <typeparam name="T">Type of objects.</typeparam>
        /// <param name="enumerable">The enumerable of objects.</param>
        /// <param name="property">The property to get the minimum of.</param>
        public static T MinOf<T>(this IEnumerable<T> enumerable, Func<T, int> property)
        {
            if (enumerable == null || !enumerable.Any())
                return default(T);
            return enumerable.Aggregate((i1, i2) => property(i1) < property(i2) ? i1 : i2);
        }

        /// <summary>
        /// Same as SequenceEquals except order does not matter.
        /// </summary>
        /// <typeparam name="T">Type of the lists.</typeparam>
        /// <param name="list1">First list to compare.</param>
        /// <param name="list2">Second list to compare.</param>
        public static bool ContentsEquals<T>(this IEnumerable<T> list1, IEnumerable<T> list2)
        {
            var cnt = new Dictionary<T, int>();
            foreach (T s in list1)
            {
                if (cnt.ContainsKey(s))
                {
                    cnt[s]++;
                }
                else
                {
                    cnt.Add(s, 1);
                }
            }
            foreach (T s in list2)
            {
                if (cnt.ContainsKey(s))
                {
                    cnt[s]--;
                }
                else
                {
                    return false;
                }
            }
            return cnt.Values.All(c => c == 0);
        }

        /// <summary>
        /// Returns the topmost element of the Stack, or null if the Stack is empty.
        /// </summary>
        /// <typeparam name="T">The type of stack.</typeparam>
        /// <param name="stack">The stack to peek from.</param>
        public static T TryPeek<T>(this Stack<T> stack)
        {
            try
            {
                return stack.Peek();
            }
            catch (InvalidOperationException)
            {
                return default(T);
            }
        }

        /// <summary>
        /// Returns the only element of a sequence, or a default value if the sequence is empty or contains more than a single value.
        /// </summary>
        /// <typeparam name="T">Type of the objects in the sequence.</typeparam>
        /// <param name="enumerable">The sequence.</param>
        /// <param name="predicate">Optional predicate to compare elements with.</param>
        public static T SingleOrSafeDefault<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate = null)
        {
            try
            {
                if (predicate == null)
                    return enumerable.SingleOrDefault();
                return enumerable.SingleOrDefault(predicate);
            }
            catch (InvalidOperationException)
            {
                return default(T);
            }
        }

        /// <summary>
        /// Moves an item in the list.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <param name="oldIndex">The old index of the item.</param>
        /// <param name="newIndex">The new index of the item.</param>
        public static void Move(this IList list, int oldIndex, int newIndex)
        {
            if (oldIndex < 0 || oldIndex >= list.Count)
                throw new ArgumentOutOfRangeException(nameof(oldIndex));
            if (newIndex < 0 || newIndex > list.Count)
                throw new ArgumentOutOfRangeException(nameof(newIndex));
            if (oldIndex == newIndex)
                return;

            var item = list[oldIndex];
            list.RemoveAt(oldIndex);

            //The actual index could have shifted due to the removal
            //if (newIndex > oldIndex)
            //    newIndex--;

            list.Insert(newIndex, item);
        }
    }
}
