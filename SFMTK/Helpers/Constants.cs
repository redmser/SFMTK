﻿namespace SFMTK
{
    /// <summary>
    /// Container for all constant values global to SFMTK.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Name of the SFM executable.
        /// </summary>
        public const string SFMExecutable = "sfm.exe";

        /// <summary>
        /// Extension used by SFMTK content package files.
        /// </summary>
        public const string SFMTKExtension = ".sfm";

        /// <summary>
        /// Extension used by SFM's DMX format.
        /// </summary>
        public const string DMXExtension = ".dmx";

        /// <summary>
        /// The color of the SFMTK logo.
        /// </summary>
        public static readonly System.Drawing.Color SFMTKLogoColor = System.Drawing.Color.FromArgb(92, 162, 225);

        /// <summary>
        /// A URL for SFMTK's gitlab project page.
        /// </summary>
        public const string GitLabURL = @"https://gitlab.com/redmser/SFMTK";

        /// <summary>
        /// A URL for SFMTK's wiki pages (append wiki page name to string).
        /// </summary>
        public const string GitLabWikiURL = GitLabURL + @"/wikis/";

        /// <summary>
        /// A URL for my paypal donation page.
        /// </summary>
        public const string DonatePaypalURL = @"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=B7K6KJ3VECKKG&lc=US";

        /// <summary>
        /// A URL for my bitcoin donation page.
        /// </summary>
        public const string DonateBitcoinURL = @"https://blockchain.info/payment_request?address=" + DonateBitcoinAddress + "&message=Donations%20to%20RedMser";

        /// <summary>
        /// My bitcoin wallet address.
        /// </summary>
        public const string DonateBitcoinAddress = @"1JdK6dMdBU19c2jSCSqWcSKMAJk2T8RXqU";

        /// <summary>
        /// A URL for the license used by SFMTK - GNU GPL v3.0.
        /// </summary>
        public const string LicenseURL = "https://www.gnu.org/licenses/gpl-3.0.txt";

        /// <summary>
        /// Gets the amount of spaces inside of a tab, by default.
        /// </summary>
        public const int TabWidth = 4;

        /// <summary>
        /// The ID to use for the node which, when double-clicked, should expand the list of Contents using a certain other Content.
        /// </summary>
        public const string FindUsagesNodeId = "_findusages";

        /// <summary>
        /// Path to the help pages data.
        /// </summary>
        public const string HelpPagesPath = "Help";

        /// <summary>
        /// Path to the modules directory.
        /// </summary>
        public const string ModulesPath = "Modules";

        /// <summary>
        /// Path to the autosave database directory.
        /// </summary>
        public const string AutosavePath = "Autosave";

        /// <summary>
        /// Filter string allowing for all file types.
        /// </summary>
        public const string DefaultFilterString = "All types (*.*)|*.*";

        /// <summary>
        /// A string for using the active setup.
        /// A question mark symbol can be used to identify the string value, as they are invalid in setup names (as they are for filenames).
        /// </summary>
        public const string SetupSelectionActive = "?ACTIVE";

        /// <summary>
        /// A string for prompting the user for which setup to select.
        /// A question mark symbol can be used to identify the string value, as they are invalid in setup names (as they are for filenames).
        /// </summary>
        public const string SetupSelectionPrompt = "?PROMPT";

        /// <summary>
        /// Name of the file association for .sfm files.
        /// </summary>
        public const string SFMFileAssoc = "sfmtksfmfile";

        /// <summary>
        /// Name of the file association for .dmx files.
        /// </summary>
        public const string DMXFileAssoc = "sfmtkdmxfile";

        /// <summary>
        /// Name of the clipboard format for storing mods.
        /// </summary>
        public const string ModClipboardFormat = "sfmtkmod";

        /// <summary>
        /// Extension for autosave files.
        /// </summary>
        public const string AutosaveExtension = ".autosave";

        /// <summary>
        /// The full name of the core module for all paid features of SFMTK.
        /// </summary>
        public const string PaidSFMTKModule = "SFMTK.Modules.Paid";
    }
}
