using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using BrightIdeasSoftware;
using SFMTK.Commands;

namespace SFMTK
{
    /// <summary>
    /// Helper for all Control-related actions, mostly working with children of a parent Control.
    /// </summary>
    public static class ControlsHelper
    {
        /// <summary>
        /// Appends a line of text to the <see cref="TextBoxBase"/>.
        /// </summary>
        public static void AppendLine(this TextBoxBase textbox, string text)
        {
            textbox.AppendText(text + "\n");
        }

        private const int WM_SETREDRAW = 11;

        /// <summary>
        /// Suspends drawing the specified <see cref="Control"/>.
        /// </summary>
        /// <param name="control">The control to stop redrawing.</param>
        public static void SuspendDrawing(this Control control)
        {
            NativeMethods.SendMessage(control.Handle, WM_SETREDRAW, false, 0);
        }

        /// <summary>
        /// Resumes drawing the specified <see cref="Control"/>, after having suspended such using a <see cref="SuspendDrawing"/> call.
        /// </summary>
        /// <param name="control">The control to start redrawing.</param>
        public static void ResumeDrawing(this Control control)
        {
            NativeMethods.SendMessage(control.Handle, WM_SETREDRAW, true, 0);
            control.Refresh();
        }

        private const int PBM_SETSTATE = 1040;

        /// <summary>
        /// Changes the state of the specified progress bar. Ensure that, for any state besides default, marquee is disabled. Otherwise, no bar will show up at all!
        /// </summary>
        /// <remarks>
        /// Guidelines on using progress bar colors (from https://msdn.microsoft.com/en-us/library/dn742475.aspx):
        /// 
        /// - Use red or yellow progress bars only to indicate the progress status, not the final results of a task.
        ///   A red or yellow progress bar indicates that users need to take some action to complete the task.
        ///   If the condition isn't recoverable, leave the progress bar green and display an error message.
        /// - Turn the progress bar red when there is a user recoverable condition that prevents making further progress.
        ///   Display a message to explain the problem and recommend a solution.
        /// - Turn the progress bar yellow to indicate either that the user has paused the task or that there is a condition
        ///   that is impeding progress but progress is still taking place (as, for example, with poor network connectivity).
        ///   If the user has paused, change the Pause button label to Resume.
        ///   If progress is impeded, display a message to explain the problem and recommend a solution.
        /// </remarks>
        /// <param name="pBar">The progress bar to change state of.</param>
        /// <param name="state">New state for the progress bar. Changes its color.</param>
        public static void SetState(this ProgressBar pBar, ProgressBarState state)
        {
            NativeMethods.SendMessage(pBar.Handle, PBM_SETSTATE, (IntPtr)state, IntPtr.Zero);
        }

        /// <summary>
        /// Calls the specified code with invoke if it is required.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="action"></param>
        public static void InvokeIfRequired(this ISynchronizeInvoke obj, MethodInvoker action)
        {
            if (obj.InvokeRequired)
            {
                var args = new object[0];
                obj.Invoke(action, args);
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// Applies the specified <see cref="ProgressValue"/> to the <see cref="ToolStripProgressBar"/>.
        /// </summary>
        public static void ApplyProgress(this ToolStripProgressBar progress, ProgressValue value) => progress.ProgressBar.ApplyProgress(value);

        /// <summary>
        /// Applies the specified <see cref="ProgressValue"/> to the <see cref="ProgressBar"/>.
        /// </summary>
        public static void ApplyProgress(this ProgressBar progress, ProgressValue value)
        {
            progress.Maximum = (int)value.Maximum;
            progress.Value = (int)value.Value;
            progress.Style = value.Marquee ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks;
            progress.SetState(value.State);
        }

        /// <summary>
        /// Returns the control with the specified name, recursing through the specified parent control.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="name"></param>
        /// <param name="comp"></param>
        public static Control GetControlWithName(Control parent, string name, StringComparison comp = StringComparison.InvariantCulture)
        {
            foreach (var ctrl in RecurseControlsAndItems(parent).OfType<Control>())
            {
                if (ctrl.Name.Equals(name, comp))
                    return ctrl;
            }
            return null;
        }

        /// <summary>
        /// Returns the header <see cref="ToolStrip"/> of the <see cref="PropertyGrid"/>.
        /// </summary>
        public static ToolStrip GetToolStrip(this PropertyGrid propertygrid)
        {
            return (ToolStrip)typeof(PropertyGrid).GetField("toolStrip", Reflection.GeneralFlags).GetValue(propertygrid);
        }

        /// <summary>
        /// Recurses all <see cref="ToolStripItem"/>s of either a specified <see cref="ToolStrip"/> or a specified <see cref="ToolStripItem"/>.
        /// </summary>
        /// <param name="component"></param>
        public static IEnumerable<ToolStripItem> RecurseItems(Component component, bool includeSelf = false)
        {
            if (includeSelf && component is ToolStripItem tsi)
                yield return tsi;

            //Check type
            if (component is ToolStripMenuItem tsmi)
            {
                //Recurse dropdowns
                foreach (var item in tsmi.DropDownItems.OfType<ToolStripItem>())
                {
                    yield return item;
                    foreach (var inner in RecurseItems(item))
                    {
                        yield return inner;
                    }
                }
            }
            else if (component is ToolStripSplitButton tssb)
            {
                //Recurse dropdowns
                foreach (var item in tssb.DropDownItems.OfType<ToolStripItem>())
                {
                    yield return item;
                    foreach (var inner in RecurseItems(item))
                    {
                        yield return inner;
                    }
                }
            }
            else if (component is ToolStripDropDownButton tsddb)
            {
                //Recurse dropdowns
                foreach (var item in tsddb.DropDownItems.OfType<ToolStripItem>())
                {
                    yield return item;
                    foreach (var inner in RecurseItems(item))
                    {
                        yield return inner;
                    }
                }
            }
            else if (component is ToolStrip ts)
            {
                //Recurse items
                foreach (var item in ts.Items.OfType<ToolStripItem>())
                {
                    yield return item;
                    foreach (var inner in RecurseItems(item))
                    {
                        yield return inner;
                    }
                }
            }
        }

        /// <summary>
        /// Recurses all child <see cref="Control"/>s of the specified <see cref="Control"/>.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="includeSelf">Lazily include specified control in enumerable.</param>
        public static IEnumerable<Control> RecurseControls(Control control, bool includeSelf = true)
        {
            if (includeSelf)
                yield return control;

            if (control is TabControl tc)
            {
                foreach (var tab in tc.TabPages.OfType<TabPage>())
                {
                    foreach (var ch in RecurseControls(tab))
                    {
                        yield return ch;
                    }
                }
            }
            else
            {
                foreach (var ctrl in control.Controls.OfType<Control>())
                {
                    foreach (var ch in RecurseControls(ctrl))
                    {
                        yield return ch;
                    }
                }
            }
        }

        /// <summary>
        /// Recurses all child <see cref="Control"/> of the specified <see cref="Control"/> as well as recursing through items of any <see cref="ToolStrip"/> found.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="includeSelf">Lazily include specified control in enumerable.</param>
        public static IEnumerable<Component> RecurseControlsAndItems(Control control, bool includeSelf = true)
        {
            foreach (var ch in RecurseControls(control, includeSelf))
            {
                foreach (var i in RecurseItems(ch))
                {
                    yield return i;
                }
                yield return ch;

                if (ch.ContextMenuStrip != null)
                    yield return ch.ContextMenuStrip;
            }
        }

        /// <summary>
        /// Returns the value of the <c>components</c> field, if it exists within the given <see cref="Control"/>.
        /// </summary>
        /// <remarks>This field is used to store a list of <see cref="Component"/>s, mostly for <see cref="Form"/>s or <see cref="UserControl"/>s.</remarks>
        public static IContainer GetComponents(Control control)
        {
            if (control == null)
                throw new ArgumentNullException(nameof(control));

            if (!(control is Form || control is UserControl))
                return null;

            var comp = control.GetType().GetField("components", Reflection.GeneralFlags);
            if (comp == null)
                return null;

            return comp.GetValue(control) as IContainer;
        }

        /// <summary>
        /// Returns the tooltip text of the given <see cref="Control"/>.
        /// </summary>
        /// <remarks>If multiple <see cref="ToolTip"/> components exist, the value of the first one will be used.</remarks>
        /// <param name="control">The control to get the tooltip text for.</param>
        /// <param name="componentsContainer">The container which owns the components (including the tooltip).</param>
        public static string GetTooltipText(Control control, Control componentsContainer)
        {
            if (componentsContainer == null)
                componentsContainer = control.FindForm();

            var comp = GetComponents(componentsContainer);
            if (comp == null)
                return null;

            var tt = comp.Components.OfType<ToolTip>().FirstOrDefault();
            if (tt == null)
                return null;

            return tt.GetToolTip(control);
        }
        
        /// <summary>
        /// Resizes this column to fit in the free space, without modifying the FillsFreeSpace property.
        /// </summary>
        /// <param name="column">The column to fit.</param>
        public static void FillFreeSpace(this OLVColumn column)
        {
            var olv = column.ListView as ObjectListView;
            if (olv == null)
                throw new ArgumentException("This column is not part of an ObjectListView", nameof(column));

            if (_resizemethod == null)
                throw new MissingMethodException("Could not retrieve the ResizeFreeSpaceFillingColumns method. Signature may have changed with an update of the ObjectListView.");

            //Has to have free space property set
            var ffs = column.FillsFreeSpace;
            column.FillsFreeSpace = true;

            //Force a resize
            _resizemethod.Invoke(olv, null);

            //Reset free space property
            column.FillsFreeSpace = ffs;
        }

        private static readonly MethodInfo _resizemethod = typeof(ObjectListView).GetMethods(Reflection.GeneralFlags)
            .FirstOrDefault(m => m.Name == "ResizeFreeSpaceFillingColumns" && !m.GetParameters().Any());
    }
}