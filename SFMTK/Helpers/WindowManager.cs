﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;
using WeifenLuo.WinFormsUI.Docking;

namespace SFMTK
{
    /// <summary>
    /// Helps assigning window properties correctly after their creation.
    /// A call to <see cref="AfterCreateForm"/> has to be made by the user, at the end of a form's constructor.
    /// </summary>
    public static class WindowManager
    {
        /// <summary>
        /// Routine to execute at the end of a form's constructor.
        /// </summary>
        /// <param name="form">The form that was created. You mostly want to pass <c>this</c> here.</param>
        public static void AfterCreateForm(Form form) => AfterCreateForm(form, true);

        internal static void AfterCreateMain() => AfterCreateForm(Program.MainForm, false);

        private static void AfterCreateForm(Form form, bool hookevent)
        {
            //Load stored window properties
            LoadCoords(form);
            LoadOlvStates(form);

            //Hook form close to save things
            if (hookevent)
            {
                form.FormClosing += (s, e) =>
                {
                    if (!e.Cancel)
                    {
                        SaveCoords(form);
                        SaveOlvStates(form);

                        HackyWorkaround();
                    }
                };
            }

            //Apply the UI theme
            UITheme.ApplyTheme(form);
        }

        /// <summary>
        /// Routine to execute after a widget has been created.
        /// You usually don't need to call this, as the <see cref="DockPanelManager"/> does so (if you add your widget using it).
        /// </summary>
        /// <param name="widget">The widget that was created.</param>
        public static void AfterCreateWidget(DockContent widget)
        {
            //Load stored widget properties
            LoadOlvStates(widget);

            //Hook up close events
            widget.FormClosing += (s, e) =>
            {
                if (!e.Cancel)
                {
                    SaveOlvStates(widget);

                    HackyWorkaround();
                }
            };

            //Apply the UI theme
            UITheme.ApplyTheme(widget);
        }

        private static void HackyWorkaround()
        {
            //HACK: WindowManager - Save settings after coords to ensure they're stored. in reality, we want to either find an
            //  event that fires earlier than FormClosing (since only after Main's FormClosed will the children be closed)
            //  OR save the application settings only after ALL forms are truly closed (such as in the last line of Program.cs)
            //  without double-saving everything
            Properties.Settings.Default.Save();
        }

        public static void LoadCoords(Form form)
        {
            //Coords
            var formtype = form.GetType().FullName;
            var coords = Properties.Settings.Default.Geometry.FirstOrDefault(w => w.Form == formtype);
            if (coords.Form != null)
                coords.Load(form);
        }

        public static void LoadOlvStates(Control ctrl)
        {
            //State (can be multiple per form)
            var formtype = ctrl.GetType().FullName;
            var ctrls = ControlsHelper.RecurseControls(ctrl, false);
            var olvs = ctrls.OfType<ObjectListView>();
            var states = Properties.Settings.Default.OlvStates.Where(w => w.Control == formtype);
            foreach (var state in states)
            {
                //Get named OLV
                var olv = olvs.FirstOrDefault(o => o.Name == state.ListView);
                if (olv != null)
                    state.Load(olv);
            }
        }

        /// <summary>
        /// Saves the window geometry of the specified form.
        /// This is automatically called for all forms that were initialized using <see cref="AfterCreateForm"/>.
        /// </summary>
        /// <param name="form">The form to save the geometry for.</param>
        public static void SaveCoords(Form form)
        {
            var formtype = form.GetType().FullName;
            Properties.Settings.Default.Geometry.RemoveAll(c => c.Form == formtype);

            //Add geometry info
            Properties.Settings.Default.Geometry.Add(WindowCoords.SaveCoords(form));
        }

        /// <summary>
        /// Saves the state of all <see cref="ObjectListView"/> controls of the specified form.
        /// This is automatically called for all forms that were initialized using <see cref="AfterCreateForm"/>.
        /// </summary>
        /// <param name="ctrl">The form to save the states for.</param>
        public static void SaveOlvStates(Control ctrl)
        {
            var formtype = ctrl.GetType().FullName;
            Properties.Settings.Default.OlvStates.RemoveAll(c => c.Control == formtype);

            foreach (var olv in ControlsHelper.RecurseControls(ctrl, false).OfType<ObjectListView>())
            {
                //Add state info
                Properties.Settings.Default.OlvStates.Add(WindowOlvState.SaveState(olv, ctrl));
            }
        }
    }

    /// <summary>
    /// Condensed information regarding a form's coordinates and state, loaded and saved automatically.
    /// </summary>
    [Serializable]
    public struct WindowCoords
    {
        /// <summary>
        /// Gets or sets the stored window geometry of the form.
        /// </summary>
        public Rectangle Geometry { get; set; }

        /// <summary>
        /// Gets or sets the location of the stored window.
        /// </summary>
        public Point Location
        {
            get => this.Geometry.Location;
            set => this.Geometry = new Rectangle(value, this.Size);
        }

        /// <summary>
        /// Gets or sets the size of the stored window.
        /// </summary>
        public Size Size
        {
            get => this.Geometry.Size;
            set => this.Geometry = new Rectangle(this.Location, value);
        }

        /// <summary>
        /// Gets or sets the stored window state of the form.
        /// </summary>
        public FormWindowState State { get; set; }

        /// <summary>
        /// Gets or sets the full type name of the form that these coordinates belong to.
        /// </summary>
        public string Form { get; set; }

        public static void LoadCoords(Form form, WindowCoords coords)
        {
            form.WindowState = coords.State;
            form.Location = coords.Location;
            form.Size = coords.Size;
        }

        public void Load(Form form) => LoadCoords(form, this);
        
        public static WindowCoords SaveCoords(Form form)
        {
            var coords = new WindowCoords();
            coords.Save(form);
            return coords;
        }

        public void Save(Form form)
        {
            this.State = form.WindowState;
            if (this.State == FormWindowState.Normal)
            {
                this.Location = form.Location;
                this.Size = form.Size;
            }
            else
            {
                this.Location = form.RestoreBounds.Location;
                this.Size = form.RestoreBounds.Size;
            }
            this.Form = form.GetType().FullName;
        }
    }

    /// <summary>
    /// Stores the state of a <see cref="ObjectListView"/>'s columns of a certain form.
    /// </summary>
    [Serializable]
    public struct WindowOlvState
    {
        /// <summary>
        /// Gets or sets the full type name of the control that the <see cref="ObjectListView"/> belongs to.
        /// </summary>
        public string Control { get; set; }

        /// <summary>
        /// Gets or sets the name of the <see cref="ObjectListView"/> that the state is saved of.
        /// </summary>
        public string ListView { get; set; }

        /// <summary>
        /// Gets or sets the stored state.
        /// </summary>
        public byte[] State { get; set; }

        public void Save(ObjectListView olv, Control ctrl)
        {
            if (olv.Name == null)
                throw new ArgumentException("Name property of ObjectListView may not be empty.");

            var parent = ctrl ?? olv.FindForm() ?? olv.Parent;
            this.Control = parent.GetType().FullName;
            this.ListView = olv.Name;
            this.State = olv.SaveState();
        }

        public static WindowOlvState SaveState(ObjectListView olv, Control ctrl)
        {
            var state = new WindowOlvState();
            state.Save(olv, ctrl);
            return state;
        }

        public static void LoadState(ObjectListView olv, WindowOlvState state) => olv.RestoreState(state.State);

        public void Load(ObjectListView olv) => LoadState(olv, this);
    }
}
