﻿using System.Collections.Generic;
using System.IO;
using SFMTK.Contents;
using SFMTK.Properties;
using System.Linq;

namespace SFMTK
{
    /// <summary>
    /// Manages the internal auto save file (if required) and keeps track of the autosaved content.
    /// </summary>
    public class AutoSaveManager
    {
        //TODO: AutoSaveManager - when any content's FilePathChanged, update the internal autosave database entry for it as well!

        /// <summary>
        /// File name of the autosave database.
        /// </summary>
        public const string AutosaveDatabase = "autosaved.adb";

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoSaveManager"/> class, loading the autosave database.
        /// </summary>
        public AutoSaveManager()
        {
            this.Database = new AutoSaveDatabase(Path.Combine(this.DatabaseLocation, AutosaveDatabase), false);

            try
            {
                Directory.CreateDirectory(this.DatabaseLocation);
                this.Database.Load();
            }
            catch (FileNotFoundException)
            {
                //Ignore, since the database may not exist at this stage
            }
        }

        /// <summary>
        /// Autosaves the specified content, even if autosaving is disabled in the config.
        /// </summary>
        /// <param name="content">The content to autosave.</param>
        public void Autosave(IEnumerable<Content> content)
        {
            foreach (var c in content)
            {
                this.Autosave(c);
            }
        }

        /// <summary>
        /// Autosaves the specified content, even if autosaving is disabled in the config.
        /// </summary>
        /// <param name="content">The content to autosave.</param>
        public void Autosave(Content content)
        {
            var res = this.AutosaveDirectly(content);
            if (res != null)
                Content.ShowSaveError(res);
        }

        /// <summary>
        /// Autosaves the specified content, even if autosaving is disabled in the config. Returns an error message, or <c>null</c> if everything went fine.
        /// </summary>
        /// <param name="content">The content to autosave.</param>
        public string AutosaveDirectly(Content content)
        {
            //TODO: AutoSaveManager - error handling similar to that of other manager-level systems (e.g. CommandHistory - "directly" method returns string to display)

            //If content has invalid path, don't try saving (this is okay though!)
            if (content.FakeFile || !Directory.Exists(Path.GetDirectoryName(content.FilePath)))
                return null;

            //Determine how to autosave
            switch (Settings.Default.AutosaveMode)
            {
                case AutoSaveMode.Internal:
                    //TODO: AutoSaveManager - if the file being saved here is not in the SFM structure, be sure to
                    //  EITHER save an absolute path for it, or to do another way of autosaving it.

                    //Add database entry
                    this.AutosaveInternal(content);
                    break;
                case AutoSaveMode.Individual:
                    //Create/Replace autosave file
                    var path = content.FilePath;
                    if (!path.EndsWith(Constants.AutosaveExtension))
                        path += Constants.AutosaveExtension;

                    try
                    {
                        content.Save(path, false);
                    }
                    catch (ContentIOException ex)
                    {
                        return ex.Message;
                    }
                    break;
                case AutoSaveMode.Override:
                    //Simply save the content
                    try
                    {
                        content.Save();
                    }
                    catch (ContentIOException ex)
                    {
                        return ex.Message;
                    }
                    break;
            }
            return null;
        }

        private void AutosaveInternal(Content content)
        {
            //Check if already in database
            var found = this.Database.Files.FirstOrDefault(f => f.FullPath == content.FilePath);
            if (found == null)
            {
                //Add to database
                found = new AutoSavedContent(content.FilePath);

                //FIXME: AutoSaveManager - check for guid conflicts, as the guid HAS to be unique at this stage
            }

            //Create or update file storing info
            try
            { 
                //UNTESTED: AutoSaveManager - does the save method / setting filepath ever append an extension that we don't want?
                content.Save(IOHelper.GetStandardizedPath(this.GetAutosaveDataFile(found)), false);
            }
            catch (ContentIOException ex)
            {
                Content.ShowSaveError(ex.Message);
            }
        }

        /// <summary>
        /// Gets the full path to the specified autosaved content's data file.
        /// </summary>
        public string GetAutosaveDataFile(AutoSavedContent content)
            => Path.Combine(this.DatabaseLocation, content.Id.ToString() + Constants.AutosaveExtension);

        /// <summary>
        /// Verification step for the internal autosaves (if enabled). Will update the MissingFile property.
        /// </summary>
        public void ScanAutosaveDatabase()
        {
            foreach (var file in this.Database.Files)
            {
                file.UpdateMissingState();
            }
        }

        /// <summary>
        /// Loads the internal autosave database.
        /// </summary>
        public void LoadDatabase() => this.Database.Load();

        /// <summary>
        /// Saves the internal autosave database.
        /// </summary>
        public void SaveDatabase() => this.Database.Save();

        /// <summary>
        /// Gets the autosave database for internal autosaved files.
        /// </summary>
        public AutoSaveDatabase Database { get; }

        /// <summary>
        /// Gets or sets the full path to the database directory.
        /// </summary>
        public string DatabaseLocation
        {
            get => this._databaseLocation;
            set
            {
                if (this._databaseLocation == value)
                    return;

                this._databaseLocation = value;
                this.Database.DataPath = value;
            }
        }
        private string _databaseLocation = IOHelper.GetSFMTKDirectory(Constants.AutosavePath);
    }
}
