﻿using System.Collections.Generic;
using SFMTK.Data;

namespace SFMTK
{
    /// <summary>
    /// Base class for going through the files of mod folders, optionally returning only the one of the first loaded mod.
    /// </summary>
    public abstract class ModCrawler
    {
        //IMP: ModCrawler - option to show ALL dirs/files inside of an absolute path, instead of only the mods
        //TODO: ModCrawler - find out if NTFS is actually faster than IO if most of our time spent is rebuilding the tree structure as objects
        //  -> might be worth it to serialize and update data live due to that?

        /// <summary>
        /// Initializes a new instance of the <see cref="ModCrawler"/> class.
        /// </summary>
        protected ModCrawler()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModCrawler" /> class.
        /// </summary>
        /// <param name="mods">The list of mods to iterate through. Order determines which files to include.</param>
        protected ModCrawler(IEnumerable<Mod> mods)
        {
            this.ModList = mods;
        }

        /// <summary>
        /// Gets or sets the list of mods that is to be iterated.
        /// </summary>
        public IEnumerable<Mod> ModList { get; set; }

        /// <summary>
        /// Gets or sets whether to return absolute paths for the files.
        /// </summary>
        /// <remarks>If set to <c>true</c>, standardized paths are returned. Otherwise, these paths are relative to the game directory.</remarks>
        public bool AbsolutePath { get; set; }

        /// <summary>
        /// Gets or sets whether, if a file exists in multiple mods, only the loaded file's path will be returned.
        /// </summary>
        public bool CheckDuplicates { get; set; } = true;

        /// <summary>
        /// Crawls through all mods in the ModList, returning files in standardized format.
        /// </summary>
        public abstract IEnumerable<string> CrawlAll();

        /// <summary>
        /// Creates a new <see cref="ModCrawler"/> which best works given the current computer setup.
        /// </summary>
        public static ModCrawler CreateNew()
        {
            if (IOHelper.IsNTFSDrive(SFM.BasePath[0].ToString()))
                return new NTFSModCrawler();
            return new IOModCrawler();
        }

        /// <summary>
        /// Creates a new <see cref="ModCrawler" /> which will read the given mods and best works given the current computer setup.
        /// </summary>
        /// <param name="mods">The list of mods that is to be iterated, in loading order.</param>
        /// <returns></returns>
        public static ModCrawler CreateNew(IEnumerable<Mod> mods)
        {
            var mc = ModCrawler.CreateNew();
            mc.ModList = mods;
            return mc;
        }
    }
}
