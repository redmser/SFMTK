﻿using System.Collections.Generic;
using System.IO;
using SFMTK.Data;

namespace SFMTK
{
    /// <summary>
    /// Crawls through certain mod folders using default IO calls, returing all files inside - based on the loading order specified.
    /// </summary>
    public class IOModCrawler : ModCrawler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IOModCrawler"/> class.
        /// </summary>
        public IOModCrawler() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IOModCrawler"/> class.
        /// </summary>
        /// <param name="mods">The list of mods to iterate through. Order determines which files to include.</param>
        public IOModCrawler(IEnumerable<Mod> mods) : base(mods)
        {
        }

        /// <summary>
        /// Crawls through all mods in the ModList, returning files in standardized format.
        /// </summary>
        public override IEnumerable<string> CrawlAll()
        {
            if (this.CheckDuplicates)
            {
                if (this.AbsolutePath)
                    throw new System.NotImplementedException("Currently cannot crawl mods as absolute paths when checking for duplicates.");

                //Check for duplicates
                var files = new HashSet<string>();
                foreach (var mod in this.ModList)
                {
                    foreach (var file in this.CrawlMod(mod, files))
                    {
                        //BUG: IOModCrawler - duplicate checking does not work yet, as the files added to the HashSet
                        //  have different mod names (obviously). perhaps add strings to hashset *without* the mod name? or compare everything after the first slash?
                        //  depends on which is faster (string.substring versus string.replace)
                        files.Add(file);
                        yield return file;
                    }
                }
            }
            else
            {
                //No need to do duplicate checking here
                foreach (var mod in this.ModList)
                {
                    foreach (var file in this.CrawlMod(mod, null))
                    {
                        yield return file;
                    }
                }
            }
        }

        /// <summary>
        /// Crawls through the specified mod.
        /// </summary>
        /// <param name="mod">The mod to crawl through.</param>
        /// <param name="previous">Previous list of files, to know which files are duplicates.</param>
        public IEnumerable<string> CrawlMod(Mod mod, HashSet<string> previous)
        {
            var abspath = mod.Path;

            if (!Directory.Exists(abspath))
                yield break;

            foreach (var file in Directory.EnumerateFiles(abspath, "*", SearchOption.AllDirectories))
            {
                if (previous == null || !previous.Contains(file))
                {
                    if (this.AbsolutePath)
                        yield return IOHelper.GetStandardizedPath(file);
                    else
                        yield return SFM.GetLocalPath(file, RelativeTo.GameDir);
                }
            }
        }
    }
}
