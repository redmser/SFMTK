﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// <see cref="DynamicToolStripItem"/> for populating the list of active notifications.
    /// </summary>
    public class NotificationsDynamicToolStripItem : DynamicToolStripItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationsDynamicToolStripItem"/> class.
        /// </summary>
        public NotificationsDynamicToolStripItem() : base()
        {
            this.AutoReloadDynamicItems = false;
        }

        /// <summary>
        /// Returns the list of <see cref="ToolStripItem" />s that this item would be replaced
        /// with if it were to be displayed as an entry somewhere.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<ToolStripItem> GetDynamicItems()
        {
            if (Properties.Settings.Default.Notifications.Count == 0)
            {
                //No nots
                yield return new ToolStripMenuItem("No new notifications") { Enabled = false };
            }
            else
            {
                //List of nots
                foreach (var not in Properties.Settings.Default.Notifications)
                {
                    var tsmi = new ToolStripMenuItem(not.Title, not.Icon, (s, e) => OnClickNotification(not));
                    yield return tsmi;
                }
            }
        }

        private static void OnClickNotification(Notifications.Notification not)
        {
            if (not.OnClick())
                Properties.Settings.Default.Notifications.Remove(not);
        }
    }
}
