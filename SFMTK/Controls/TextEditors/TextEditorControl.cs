﻿using System.Windows.Forms;
using System;
using System.ComponentModel;
using System.IO;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using System.Windows.Input;
using ICSharpCode.AvalonEdit.CodeCompletion;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SFMTK.Properties;
using SFMTK.Commands;

namespace SFMTK.Controls.TextEditors
{
    /// <summary>
    /// An advanced text editor supporting syntax highlighting, code completion and the like. Use a corresponding sub-class for such additional features.
    /// </summary>
    /// <seealso cref="SFMTK.IEditableData" />
    /// <seealso cref="System.Windows.Forms.UserControl" />
    /// <seealso cref="SFMTK.IThemeable" />
    public partial class TextEditorControl : UserControl, IThemeable, IEditableData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TextEditorControl"/> class.
        /// </summary>
        public TextEditorControl()
        {
            //Compiler-generated
            InitializeComponent();

            //Add text editor to elementhost
            this.TextEditor = new TextEditor();
            this.elementHost.Child = this.TextEditor;

            //Apply properties
            this.LoadSettings();

            //Events
            this.TextEditor.TextArea.TextEntering += this.TextEditor_TextEntering;
            this.TextEditor.TextArea.TextEntered += this.TextEditor_TextEntered;
            this.TextEditor.TextArea.MouseRightButtonUp += this.TextArea_MouseRightButtonUp;
            this.TextEditor.TextArea.SelectionChanged += this.TextArea_SelectionChanged;

            //Remove reimplemented default shortcuts, as we have reimplemented them using the Command system
            ApplicationCommands.Cut.InputGestures.Clear();
            ApplicationCommands.Copy.InputGestures.Clear();
            ApplicationCommands.Paste.InputGestures.Clear();
            ApplicationCommands.Delete.InputGestures.Clear();

            //IMP: TextEditorControl - rethink this command stuff before moving on!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //BUG: TextEditorControl - editing text is not an action which gets managed by the command system
            //  As of such, any text changes followed by undoing one of the "global" actions can cause issues
        }

        public void LoadSettings()
        {
            //HACK: TextEditorControl - not doing WPF databinding since it's scary
            this.TextEditor.Options.HighlightCurrentLine = Settings.Default.TextEditorHighlightCurrentLine;
            this.TextEditor.Options.AllowScrollBelowDocument = Settings.Default.TextEditorAllowScrollBelowDocument;
            this.TextEditor.Options.CutCopyWholeLine = Settings.Default.TextEditorCutCopyWholeLine;
            this.TextEditor.Options.EnableVirtualSpace = Settings.Default.TextEditorEnableVirtualSpace;
            this.TextEditor.Options.ShowEndOfLine = Settings.Default.TextEditorShowEndOfLine;
            this.TextEditor.Options.ShowSpaces = Settings.Default.TextEditorShowWhitespace;
            this.TextEditor.Options.ShowTabs = Settings.Default.TextEditorShowWhitespace;
            this.TextEditor.Options.ConvertTabsToSpaces = Settings.Default.TextEditorConvertTabsToSpaces;
        }

        private void TextArea_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            //Create context menu
            this.contextMenuStrip.Show(System.Windows.Forms.Cursor.Position);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.UserControl.Load" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            //Invoke event first
            base.OnLoad(e);

            if (this.DesignMode || this._initialized)
                return;

            //Initializing control
            this.LoadSyntaxHighlighting();
            this.LoadFolding();
            this._initialized = true;
        }

        private bool _initialized;

        /// <summary>
        /// Gets the underlying text editor control.
        /// </summary>
        [Browsable(false)]
        public TextEditor TextEditor { get; }

        /// <summary>
        /// Gets or sets the theme currently active on this control. This property is never set to the same value that it already has.
        /// </summary>
        public string ActiveTheme { get; set; }

        /// <summary>
        /// Loads or reloads the corresponding syntax highlighting file. By default, uses the specified language name.
        /// </summary>
        protected virtual void LoadSyntaxHighlighting()
        {
            var filename = this.Language;
            if (filename == null)
            {
                //Reset highlighting
                this.TextEditor.SyntaxHighlighting = null;
            }
            else
            {
                //Apply highlighting
                using (var xshd_stream = File.OpenRead(Path.Combine(Application.StartupPath, $"{filename}-Mode.xshd")))
                using (var xshd_reader = new System.Xml.XmlTextReader(xshd_stream))
                {
                    this.TextEditor.SyntaxHighlighting = HighlightingLoader.Load(xshd_reader, HighlightingManager.Instance);
                }
            }
        }

        /// <summary>
        /// Loads or reloads the corresponding folding info. By default, uses no folding.
        /// </summary>
        protected virtual void LoadFolding() { }

        /// <summary>
        /// Gets whether the specified TextCompositionEventArgs should allow for the code completion window to be shown.
        /// <para/>Should check for certain keys (such as $ for VMT) being pressed.
        /// </summary>
        protected virtual bool ShouldShowCodeCompletion(TextCompositionEventArgs e) => false;

        /// <summary>
        /// Gets whether to insert the selected code completion entry when entering text according to the specified TextCompositionEventArgs.
        /// <para/>Should check for keys (such as ") being pressed, which signify the end of the string.
        /// </summary>
        protected virtual bool ShouldInsertSelectedCodeCompletion(TextCompositionEventArgs e) =>
            !(char.IsLetterOrDigit(e.Text[0]) || e.Text[0] == '\\' || e.Text[0] == '/' || e.Text[0] == '.');

        /// <summary>
        /// Gets the name of the language of this TextEditorControl. May be null for no language (plain text).
        /// </summary>
        public virtual string Language => null;
        
        /// <summary>
        /// Gets the currently open completion window, if any.
        /// </summary>
        protected CompletionWindow CompletionWindow { get; private set; }

        /// <summary>
        /// Returns code completion information for the given text entering action.
        /// </summary>
        protected virtual IEnumerable<ICompletionData> GetCodeCompletionData(TextCompositionEventArgs e) { yield break; }

        /// <summary>
        /// The character entered to show the code completion window.
        /// </summary>
        private char _showChar;

        protected virtual void TextEditor_TextEntering(object sender, TextCompositionEventArgs e)
        {
            //TODO: TextEditorControl - create a shortcut for showing code completion
            if (this.ShouldShowCodeCompletion(e))
            {
                //Show code completion window!
                this._showChar = e.Text[0];
                this.CompletionWindow = new CompletionWindow(this.TextEditor.TextArea);
                foreach (var entry in GetCodeCompletionData(e))
                {
                    //Populate completion data
                    this.CompletionWindow.CompletionList.CompletionData.Add(entry);
                }
                this.CompletionWindow.Show();
                this.CompletionWindow.Closed += delegate {
                    this.CompletionWindow = null;
                };
            }
        }

        protected virtual void TextEditor_TextEntered(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Length > 0 && e.Text[0] != this._showChar && this.CompletionWindow != null && this.ShouldInsertSelectedCodeCompletion(e))
            {
                this.CompletionWindow.CompletionList.RequestInsertion(e);
            }
            else
            {
                this._showChar = char.MinValue;
            }
        }
        
        //NYI: TextEditorControl - IEditableData interface
        private void TextArea_SelectionChanged(object sender, EventArgs e) => Commands.CommandManager.UpdateCommandsEnabled(nameof(Forms.Main), typeof(EditCommand));

        public Command CutCommand => new CutTextEditorCommand(this.TextEditor);
        public Command CopyCommand => throw new NotImplementedException();
        public Command PasteCommand => throw new NotImplementedException();
        public Command DeleteCommand => throw new NotImplementedException();
        public bool CanDelete => (Settings.Default.TextEditorCutCopyWholeLine || this.TextEditor.SelectionLength > 0) && !this.TextEditor.IsReadOnly;
        public bool CanCopy => Settings.Default.TextEditorCutCopyWholeLine || this.TextEditor.SelectionLength > 0;
        public bool CanPaste => Clipboard.ContainsText();
    }
}
