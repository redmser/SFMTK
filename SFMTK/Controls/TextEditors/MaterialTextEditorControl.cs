﻿using System.Collections.Generic;
using System.Windows.Input;
using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Folding;

namespace SFMTK.Controls.TextEditors
{
    /// <summary>
    /// A text editor for material files (VMT).
    /// </summary>
    /// <seealso cref="SFMTK.Controls.TextEditors.TextEditorControl" />
    public class MaterialTextEditorControl : TextEditorControl
    {
        /// <summary>
        /// Gets the language of this TextEditorControl.
        /// </summary>
        public override string Language => "VMT";

        /// <summary>
        /// Loads or reloads the corresponding folding info. By default, uses no folding.
        /// </summary>
        protected override void LoadFolding()
        {
            //Do folding using braces
            var foldingManager = FoldingManager.Install(this.TextEditor.TextArea);
            var foldingStrategy = new BraceFoldingStrategy();
            foldingStrategy.UpdateFoldings(foldingManager, this.TextEditor.Document);
        }

        /// <summary>
        /// Gets whether the specified TextCompositionEventArgs should allow for the code completion window to be shown.
        /// <para />Should check for certain keys (such as $ for VMT) being pressed.
        /// </summary>
        protected override bool ShouldShowCodeCompletion(TextCompositionEventArgs e) => e.Text[0] == '$' || e.Text[0] == '%';
        //TODO: MaterialTextEditorControl - show code complete when entering shader name or paths

        /// <summary>
        /// Gets whether to insert the selected code completion entry when entering text according to the specified TextCompositionEventArgs.
        /// <para />Should check for keys (such as ") being pressed, which signify the end of the string.
        /// </summary>
        protected override bool ShouldInsertSelectedCodeCompletion(TextCompositionEventArgs e) => base.ShouldInsertSelectedCodeCompletion(e);

        /// <summary>
        /// Returns code completion information for the given text entering action.
        /// </summary>
        protected override IEnumerable<ICompletionData> GetCodeCompletionData(TextCompositionEventArgs e)
        {
            //NYI: MaterialTextEditorControl - match shader parameters (syntax highlighting and code completion) depending on the shader used
            //  -> different images for which data types the parameters accept! reflect such in the description as well
            throw new System.NotImplementedException();
        }
    }
}
