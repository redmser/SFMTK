﻿using System;
using System.Windows.Media;
using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;

namespace SFMTK.Controls.TextEditors
{
    /// <summary>
    /// Simple implementation of the <see cref="ICompletionData"/> interface.
    /// </summary>
    public class CodeCompletionData : ICompletionData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CodeCompletionData"/> class.
        /// </summary>
        /// <param name="text">Display text.</param>
        /// <param name="description">Short description.</param>
        /// <param name="image">The displayed image.</param>
        public CodeCompletionData(string text, string description, ImageSource image)
        {
            this.Text = text;
            this.Description = description;
            this.Image = image;
        }

        /// <summary>
        /// Gets the image.
        /// </summary>
        public ImageSource Image { get; }

        /// <summary>
        /// Gets the text. This property is used to filter the list of visible elements.
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// The displayed content. This can be the same as <see cref="Text"/>, or a WPF UIElement if
        /// you want to display rich content.
        /// </summary>
        public object Content => this.Text;

        /// <summary>
        /// Gets the description, as text or WPF UIElement.
        /// </summary>
        public object Description { get; }

        /// <summary>
        /// Gets the priority. This property is used in the selection logic. You can use it to prefer selecting those items
        /// which the user is accessing most frequently.
        /// </summary>
        public double Priority => 0;

        /// <summary>
        /// Perform the completion.
        /// </summary>
        /// <param name="textArea">The text area on which completion is performed.</param>
        /// <param name="completionSegment">The text segment that was used by the completion window if
        /// the user types (segment between CompletionWindow.StartOffset and CompletionWindow.EndOffset).</param>
        /// <param name="insertionRequestEventArgs">The EventArgs used for the insertion request.
        /// These can be TextCompositionEventArgs, KeyEventArgs, MouseEventArgs, depending on how
        /// the insertion was triggered.</param>
        public void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs)
        {
            textArea.Document.Replace(completionSegment, this.Text);
        }
    }
}
