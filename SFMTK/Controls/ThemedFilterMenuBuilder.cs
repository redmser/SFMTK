﻿using System.Windows.Forms;
using BrightIdeasSoftware;

namespace SFMTK.Controls
{
    /// <summary>
    /// A <see cref="FilterMenuBuilder"/> which themes itself according to the currently selected theme. Automatically applied to every "uncustomized" ObjectListView.
    /// </summary>
    /// <seealso cref="BrightIdeasSoftware.FilterMenuBuilder" />
    public class ThemedFilterMenuBuilder : FilterMenuBuilder
    {
        /// <summary>
        /// Create a Filter menu on the given tool tip for the given column in the given ObjectListView.
        /// </summary>
        /// <param name="strip"></param>
        /// <param name="listView"></param>
        /// <param name="column"></param>
        /// <returns>
        /// The strip that should be shown to the user
        /// </returns>
        /// <remarks>
        /// This is the main entry point into this class.
        /// </remarks>
        public override ToolStripDropDown MakeFilterMenu(ToolStripDropDown strip, ObjectListView listView, OLVColumn column)
        {
            var menu = base.MakeFilterMenu(strip, listView, column);
            UITheme.SelectedTheme?.ApplyControlTheme(listView.FindForm(), menu);
            return menu;
        }
    }
}
