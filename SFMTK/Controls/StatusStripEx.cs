﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// A status strip which supports different program states as well as prioritizing of multiple statuses.
    /// </summary>
    /// <remarks>This control updates the Text property, which is not used directly by a status strip, but can easily be watched (TextChanged event) and applied accordingly.</remarks>
    public class StatusStripEx : StatusStrip
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatusStripEx"/> class.
        /// </summary>
        public StatusStripEx()
        {
            //Notify about changes to live-update text
            this.Entries.CollectionChanged += this.Entries_CollectionChanged;
        }

        /// <summary>
        /// The entries displayed on this status strip. Should be removed once no longer relevant.
        /// </summary>
        [Browsable(false)]
        public StatusStripEntryCollection Entries { get; } = new StatusStripEntryCollection();

        private IEnumerable<StatusStripEntry> _entriesIgnored => this.Entries.Where(e => e.Text != null);

        private void Entries_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.UpdateText();

            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                //Add event handlers
                foreach (var item in e.NewItems)
                {
                    ((INotifyPropertyChanged)item).PropertyChanged += this.Entry_PropertyChanged;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                //Remove event handlers
                foreach (var item in e.OldItems)
                {
                    ((INotifyPropertyChanged)item).PropertyChanged -= this.Entry_PropertyChanged;
                }
            }
        }

        private void Entry_PropertyChanged(object sender, PropertyChangedEventArgs e) => this.UpdateText();

        /// <summary>
        /// Updates the display text and state of the status strip. Automatically called when the entries list changes.
        /// </summary>
        public void UpdateText()
        {
            this.DisplayText = this._entriesIgnored.MaxOf(e => e.Priority)?.Text ?? this.DefaultText;
            this.State = this._entriesIgnored.MaxOf(e => (int)e.State)?.State ?? StatusStripState.Default;
        }

        /// <summary>
        /// Updates the color of the status bar according to the State value, if the theme supports it. Automatically called when the state changes.
        /// </summary>
        public void UpdateStatusColor()
        {
            Color? color;
            switch (this.State)
            {
                case StatusStripState.Compiling:
                    color = UITheme.SelectedTheme?.StatusBarCompilingBackColor;
                    break;
                case StatusStripState.Loading:
                    color = UITheme.SelectedTheme?.StatusBarLoadingBackColor;
                    break;
                case StatusStripState.RunningSFM:
                    color = UITheme.SelectedTheme?.StatusBarRunningSFMBackColor;
                    break;
                default:
                    color = UITheme.SelectedTheme?.StatusBarBackColor;
                    break;
            }

            this.BackColor = color ?? Color.Empty;
        }

        /// <summary>
        /// Gets or sets the string to display as Text when the status strip has nothing to display.
        /// </summary>
        [Description("The text to display when the status strip has nothing to display.")]
        public string DefaultText { get; set; } = Properties.Resources.DefaultStatusLabel;

        /// <summary>
        /// Gets or sets the text to be displayed.
        /// </summary>
        [Browsable(false)]
        public string DisplayText
        {
            get => this._display;
            set
            {
                if (this._display == value)
                    return;

                this._display = value;
                this.OnDisplayTextChanged(EventArgs.Empty);
            }
        }
        private string _display;

        /// <summary>
        /// Gets the current state of the status strip. Determined using the Entries internal priority list.
        /// </summary>
        public StatusStripState State
        {
            get => this._state;
            private set
            {
                if (this._state == value)
                    return;

                this._state = value;
                this.UpdateStatusColor();
            }
        }
        private StatusStripState _state = StatusStripState.Default;

        /// <summary>
        /// Raises the <see cref="E:DisplayTextChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnDisplayTextChanged(EventArgs e) => this.DisplayTextChanged?.Invoke(this, e);

        /// <summary>
        /// Occurs when display text changed.
        /// </summary>
        public event EventHandler DisplayTextChanged;
    }

    /// <summary>
    /// A collection for status strip entries.
    /// </summary>
    public class StatusStripEntryCollection : ObservableCollection<StatusStripEntry>
    {
        /// <summary>
        /// Adds the specified entry to the collection.
        /// </summary>
        /// <param name="text">The display text of this message. If the text is null, this entry is ignored completely (including state).</param>
        /// <param name="priority">The priority of this message. A higher number indicates a higher priority - the message with the highest priority is shown by default.</param>
        /// <param name="state">The state of this message, showing the user how important this action is.
        /// <para />Unlike text, state actually has a different, internal order which determines the status bar color.</param>
        /// <param name="decaytime">The time, in milliseconds, until this status entry will be removed from the strip automatically. Use -1 to never let it decay.</param>
        public void Add(string text, int priority = 0, StatusStripState state = StatusStripState.Default, int decaytime = -1)
            => this.Add(new StatusStripEntry(text, priority, state, decaytime));

        /// <summary>
        /// Removes the first entry with the specified text.
        /// </summary>
        /// <param name="text">Text to search for (case-sensitive).</param>
        public void Remove(string text) => this.Remove(this.FirstOrDefault(e => e.Text == text));

        /// <summary>
        /// Removes all entries with the specified state.
        /// </summary>
        /// <param name="state"></param>
        public void Remove(StatusStripState state)
        {
            foreach (var entry in this.Where(e => e.State == state).Reverse())
                this.Remove(entry);
        }
    }

    /// <summary>
    /// A status to be displayed inside of a StatusStripEx.
    /// </summary>
    public class StatusStripEntry : INotifyPropertyChanged
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatusStripEntry" /> class.
        /// </summary>
        /// <param name="text">The display text of this message. If the text is null, this entry is ignored completely (including state).</param>
        /// <param name="priority">The priority of this message. A higher number indicates a higher priority - the message with the highest priority is shown by default.</param>
        /// <param name="state">The state of this message, showing the user how important this action is.
        /// <para />Unlike text, state actually has a different, internal order which determines the status bar color.</param>
        /// <param name="decaytime">The time, in milliseconds, until this status entry will be removed from the strip automatically. Use -1 to never let it decay.</param>
        public StatusStripEntry(string text, int priority = 0, StatusStripState state = StatusStripState.Default, int decaytime = -1)
        {
            this.Text = text;
            this.Priority = priority;
            this.State = state;
            this.DecayTime = decaytime;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusStripEntry"/> class.
        /// </summary>
        public StatusStripEntry() { }

        /// <summary>
        /// Gets or sets the display text of this message. If the text is null, this entry is ignored completely (including state).
        /// </summary>
        /// <value>
        /// The current text of this message.
        /// </value>
        public string Text
        {
            get => this._text;
            set
            {
                if (this._text == value)
                    return;

                this._text = value;
                this.OnPropertyChanged();
            }
        }
        private string _text;

        /// <summary>
        /// Gets or sets the state of this message, showing the user how important this action is.
        /// <para />Unlike text, state actually has a different, internal order which determines the status bar color.
        /// </summary>
        /// <value>
        /// The state of this message.
        /// </value>
        public StatusStripState State
        {
            get => this._state;
            set
            {
                if (this._state == value)
                    return;

                this._state = value;
                this.OnPropertyChanged();
            }
        }
        private StatusStripState _state;

        /// <summary>
        /// Gets or sets the priority of this message. A higher number indicates a higher priority - the message with the highest priority is shown by default.
        /// </summary>
        /// <value>
        /// The current priority of this message.
        /// </value>
        public int Priority
        {
            get => this._priority;
            set
            {
                if (this._priority == value)
                    return;

                this._priority = value;
                this.OnPropertyChanged();
            }
        }
        private int _priority;

        /// <summary>
        /// Gets or sets the time, in milliseconds, until this status entry will be removed from the strip automatically. Use -1 to never let it decay.
        /// </summary>
        /// <value>
        /// The decay time.
        /// </value>
        public int DecayTime { get; set; }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises the <see cref="E:PropertyChanged" /> event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null) => this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}

namespace SFMTK
{
    /// <summary>
    /// State of the status bar, determining its color for themes that support it.
    /// </summary>
    public enum StatusStripState
    {
        /// <summary>
        /// Default status bar state, nothing special going on, program is idle.
        /// </summary>
        Default = 0,

        /// <summary>
        /// SFMTK either launched SFM or detected a running instance of SFM.
        /// </summary>
        RunningSFM = 1,

        /// <summary>
        /// Something in the program is loading. Mostly not modal, allowing you to use the program in the meantime.
        /// </summary>
        Loading = 2,

        /// <summary>
        /// The application is compiling files.
        /// </summary>
        Compiling = 3
    }
}
