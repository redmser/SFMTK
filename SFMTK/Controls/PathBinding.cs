﻿using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// Binding used for reformatting paths.
    /// </summary>
    public class PathBinding : Binding
    {
        public PathBinding(string propertyName, object dataSource, string dataMember, DataSourceUpdateMode dataSourceUpdateMode)
            : base(propertyName, dataSource, dataMember, true, dataSourceUpdateMode)
        {

        }

        protected override void OnFormat(ConvertEventArgs cevent) => cevent.Value = IOHelper.GetStandardizedPath(cevent.Value.ToString());

        protected override void OnParse(ConvertEventArgs cevent) => cevent.Value = IOHelper.GetStandardizedPath(cevent.Value.ToString());
    }
}
