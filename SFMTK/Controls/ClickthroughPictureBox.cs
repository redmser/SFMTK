﻿using System;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// A <see cref="PictureBox"/> which can be clicked through, to be used as an overlay.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.PictureBox" />
    public partial class ClickthroughPictureBox : PictureBox
    {
        /// <summary>
        /// Whether this picture box can be clicked through.
        /// </summary>
        public bool IsClickthrough { get; set; } = true;

        /// <summary>
        /// Processes Windows messages.
        /// </summary>
        /// <param name="m">The Windows <see cref="T:System.Windows.Forms.Message" /> to process.</param>
        protected override void WndProc(ref Message m)
        {
            //Not clickthru
            if (!this.IsClickthrough)
            {
                base.WndProc(ref m);
                return;
            }

            //Clickthru
            const int WM_NCHITTEST = 0x0084;
            const int HTTRANSPARENT = (-1);

            if (m.Msg == WM_NCHITTEST)
            {
                //Pass any hittest
                m.Result = (IntPtr)HTTRANSPARENT;
            }
            else
            {
                //Handle normally
                base.WndProc(ref m);
            }
        }
    }
}
