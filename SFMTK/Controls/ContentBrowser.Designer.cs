﻿namespace SFMTK.Controls
{
    partial class ContentBrowser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.verticalSplitContainer = new System.Windows.Forms.SplitContainer();
            this.mainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.modComboBox = new SFMTK.Controls.CheckedComboBox();
            this.contentTreeListView = new BrightIdeasSoftware.TreeListView();
            this.nameContentColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.modContentColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.nodeTypeContentColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.fileTypeContentColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.modOrderContentColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.cancelButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pathTextBox = new System.Windows.Forms.TextBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.filterControl = new SFMTK.Controls.FilterControl();
            this.viewPathButton = new System.Windows.Forms.Button();
            this.buildingProgressBar = new System.Windows.Forms.ProgressBar();
            this.buildingProgressLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.rescanMenuButton = new SFMTK.Controls.MenuButton();
            this.rescanContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.reloadAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reloadChangedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reloadSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoSplitContainer = new System.Windows.Forms.SplitContainer();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dependencyGraphViewer = new SFMTK.Controls.DependencyGraphViewer();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.verticalSplitContainer)).BeginInit();
            this.verticalSplitContainer.Panel1.SuspendLayout();
            this.verticalSplitContainer.Panel2.SuspendLayout();
            this.verticalSplitContainer.SuspendLayout();
            this.mainTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contentTreeListView)).BeginInit();
            this.rescanContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.infoSplitContainer)).BeginInit();
            this.infoSplitContainer.Panel1.SuspendLayout();
            this.infoSplitContainer.Panel2.SuspendLayout();
            this.infoSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // verticalSplitContainer
            // 
            this.verticalSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.verticalSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.verticalSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.verticalSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.verticalSplitContainer.Name = "verticalSplitContainer";
            // 
            // verticalSplitContainer.Panel1
            // 
            this.verticalSplitContainer.Panel1.Controls.Add(this.mainTableLayoutPanel);
            this.verticalSplitContainer.Panel1MinSize = 170;
            // 
            // verticalSplitContainer.Panel2
            // 
            this.verticalSplitContainer.Panel2.Controls.Add(this.infoSplitContainer);
            this.verticalSplitContainer.Size = new System.Drawing.Size(814, 604);
            this.verticalSplitContainer.SplitterDistance = 528;
            this.verticalSplitContainer.SplitterWidth = 3;
            this.verticalSplitContainer.TabIndex = 0;
            // 
            // mainTableLayoutPanel
            // 
            this.mainTableLayoutPanel.ColumnCount = 6;
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.mainTableLayoutPanel.Controls.Add(this.label3, 0, 4);
            this.mainTableLayoutPanel.Controls.Add(this.label1, 0, 0);
            this.mainTableLayoutPanel.Controls.Add(this.modComboBox, 1, 0);
            this.mainTableLayoutPanel.Controls.Add(this.contentTreeListView, 0, 1);
            this.mainTableLayoutPanel.Controls.Add(this.cancelButton, 3, 6);
            this.mainTableLayoutPanel.Controls.Add(this.label2, 0, 5);
            this.mainTableLayoutPanel.Controls.Add(this.pathTextBox, 1, 5);
            this.mainTableLayoutPanel.Controls.Add(this.browseButton, 4, 5);
            this.mainTableLayoutPanel.Controls.Add(this.filterControl, 0, 3);
            this.mainTableLayoutPanel.Controls.Add(this.viewPathButton, 5, 5);
            this.mainTableLayoutPanel.Controls.Add(this.buildingProgressBar, 0, 2);
            this.mainTableLayoutPanel.Controls.Add(this.buildingProgressLabel, 4, 2);
            this.mainTableLayoutPanel.Controls.Add(this.okButton, 2, 6);
            this.mainTableLayoutPanel.Controls.Add(this.rescanMenuButton, 3, 0);
            this.mainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainTableLayoutPanel.Name = "mainTableLayoutPanel";
            this.mainTableLayoutPanel.RowCount = 7;
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.mainTableLayoutPanel.Size = new System.Drawing.Size(524, 600);
            this.mainTableLayoutPanel.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mainTableLayoutPanel.SetColumnSpan(this.label3, 5);
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 548);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(489, 3);
            this.label3.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "&Mods:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // modComboBox
            // 
            this.modComboBox.CheckOnClick = true;
            this.mainTableLayoutPanel.SetColumnSpan(this.modComboBox, 2);
            this.modComboBox.DisplayCount = SFMTK.Controls.DisplayCountMode.Both;
            this.modComboBox.DisplayFunction = null;
            this.modComboBox.DisplayMember = "Name";
            this.modComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modComboBox.DropDownHeight = 1;
            this.modComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.modComboBox.DropDownWidth = 1;
            this.modComboBox.FormattingEnabled = true;
            this.modComboBox.IntegralHeight = false;
            this.modComboBox.Location = new System.Drawing.Point(58, 3);
            this.modComboBox.Name = "modComboBox";
            this.modComboBox.Size = new System.Drawing.Size(338, 21);
            this.modComboBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.modComboBox, "Filter content based on which mod they are in");
            this.modComboBox.ValueSeparator = ", ";
            // 
            // contentTreeListView
            // 
            this.contentTreeListView.AllColumns.Add(this.nameContentColumn);
            this.contentTreeListView.AllColumns.Add(this.modContentColumn);
            this.contentTreeListView.AllColumns.Add(this.nodeTypeContentColumn);
            this.contentTreeListView.AllColumns.Add(this.fileTypeContentColumn);
            this.contentTreeListView.AllColumns.Add(this.modOrderContentColumn);
            this.contentTreeListView.CellEditUseWholeCell = false;
            this.contentTreeListView.CheckBoxes = true;
            this.contentTreeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameContentColumn,
            this.modContentColumn,
            this.fileTypeContentColumn});
            this.mainTableLayoutPanel.SetColumnSpan(this.contentTreeListView, 6);
            this.contentTreeListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.contentTreeListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentTreeListView.FullRowSelect = true;
            this.contentTreeListView.HideSelection = false;
            this.contentTreeListView.HierarchicalCheckboxes = true;
            this.contentTreeListView.Location = new System.Drawing.Point(3, 27);
            this.contentTreeListView.Name = "contentTreeListView";
            this.contentTreeListView.ShowGroups = false;
            this.contentTreeListView.ShowImagesOnSubItems = true;
            this.contentTreeListView.Size = new System.Drawing.Size(518, 470);
            this.contentTreeListView.TabIndex = 3;
            this.contentTreeListView.UseCompatibleStateImageBehavior = false;
            this.contentTreeListView.UseFiltering = true;
            this.contentTreeListView.View = System.Windows.Forms.View.Details;
            this.contentTreeListView.VirtualMode = true;
            this.contentTreeListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.contentTreeListView_ItemChecked);
            this.contentTreeListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.contentTreeListView_ItemSelectionChanged);
            // 
            // nameContentColumn
            // 
            this.nameContentColumn.AspectName = "Name";
            this.nameContentColumn.DisplayIndex = 1;
            this.nameContentColumn.Groupable = false;
            this.nameContentColumn.ImageAspectName = "";
            this.nameContentColumn.Text = "Name";
            this.nameContentColumn.UseFiltering = false;
            this.nameContentColumn.Width = 100;
            // 
            // modContentColumn
            // 
            this.modContentColumn.AspectName = "Mod.Name";
            this.modContentColumn.DisplayIndex = 0;
            this.modContentColumn.Groupable = false;
            this.modContentColumn.Text = "Mod";
            // 
            // nodeTypeContentColumn
            // 
            this.nodeTypeContentColumn.AspectName = "NodeType";
            this.nodeTypeContentColumn.IsVisible = false;
            this.nodeTypeContentColumn.Text = "Type";
            // 
            // fileTypeContentColumn
            // 
            this.fileTypeContentColumn.AspectName = "DisplayType";
            this.fileTypeContentColumn.Text = "File Type";
            // 
            // modOrderContentColumn
            // 
            this.modOrderContentColumn.AspectName = "ModIndex";
            this.modOrderContentColumn.DisplayIndex = 3;
            this.modOrderContentColumn.IsVisible = false;
            this.modOrderContentColumn.Text = "Order";
            this.modOrderContentColumn.UseFiltering = false;
            // 
            // cancelButton
            // 
            this.mainTableLayoutPanel.SetColumnSpan(this.cancelButton, 3);
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cancelButton.Location = new System.Drawing.Point(399, 576);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(0);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(125, 24);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(0, 552);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Full &Path:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pathTextBox
            // 
            this.mainTableLayoutPanel.SetColumnSpan(this.pathTextBox, 3);
            this.pathTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pathTextBox.Location = new System.Drawing.Point(55, 555);
            this.pathTextBox.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.Size = new System.Drawing.Size(415, 20);
            this.pathTextBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.pathTextBox, "File path to the selected content");
            // 
            // browseButton
            // 
            this.browseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browseButton.Image = global::SFMTK.Properties.Resources.folder;
            this.browseButton.Location = new System.Drawing.Point(470, 552);
            this.browseButton.Margin = new System.Windows.Forms.Padding(0);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(25, 24);
            this.browseButton.TabIndex = 2;
            this.toolTip.SetToolTip(this.browseButton, "Browse for a content file using your native file browser");
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // filterControl
            // 
            this.mainTableLayoutPanel.SetColumnSpan(this.filterControl, 6);
            this.filterControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filterControl.Expanded = false;
            this.filterControl.Location = new System.Drawing.Point(2, 522);
            this.filterControl.Margin = new System.Windows.Forms.Padding(2);
            this.filterControl.Name = "filterControl";
            this.filterControl.Size = new System.Drawing.Size(520, 24);
            this.filterControl.TabIndex = 4;
            this.filterControl.ExpandedChanged += new System.EventHandler(this.filterControl_ExpandedChanged);
            // 
            // viewPathButton
            // 
            this.viewPathButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewPathButton.Image = global::SFMTK.Properties.Resources.arrow_go;
            this.viewPathButton.Location = new System.Drawing.Point(495, 552);
            this.viewPathButton.Margin = new System.Windows.Forms.Padding(0);
            this.viewPathButton.Name = "viewPathButton";
            this.viewPathButton.Size = new System.Drawing.Size(29, 24);
            this.viewPathButton.TabIndex = 3;
            this.toolTip.SetToolTip(this.viewPathButton, "Open in Explorer");
            this.viewPathButton.UseVisualStyleBackColor = true;
            this.viewPathButton.Click += new System.EventHandler(this.viewPathButton_Click);
            // 
            // buildingProgressBar
            // 
            this.mainTableLayoutPanel.SetColumnSpan(this.buildingProgressBar, 4);
            this.buildingProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buildingProgressBar.Location = new System.Drawing.Point(3, 500);
            this.buildingProgressBar.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.buildingProgressBar.Name = "buildingProgressBar";
            this.buildingProgressBar.Size = new System.Drawing.Size(464, 20);
            this.buildingProgressBar.TabIndex = 4;
            // 
            // buildingProgressLabel
            // 
            this.buildingProgressLabel.AutoSize = true;
            this.mainTableLayoutPanel.SetColumnSpan(this.buildingProgressLabel, 2);
            this.buildingProgressLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buildingProgressLabel.Location = new System.Drawing.Point(470, 500);
            this.buildingProgressLabel.Margin = new System.Windows.Forms.Padding(0);
            this.buildingProgressLabel.Name = "buildingProgressLabel";
            this.buildingProgressLabel.Size = new System.Drawing.Size(54, 20);
            this.buildingProgressLabel.TabIndex = 5;
            this.buildingProgressLabel.Text = "0%";
            this.buildingProgressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.okButton.Location = new System.Drawing.Point(274, 576);
            this.okButton.Margin = new System.Windows.Forms.Padding(0);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(125, 24);
            this.okButton.TabIndex = 4;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // rescanMenuButton
            // 
            this.mainTableLayoutPanel.SetColumnSpan(this.rescanMenuButton, 3);
            this.rescanMenuButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rescanMenuButton.DrawSeparationLine = false;
            this.rescanMenuButton.Location = new System.Drawing.Point(399, 0);
            this.rescanMenuButton.Margin = new System.Windows.Forms.Padding(0);
            this.rescanMenuButton.Menu = this.rescanContextMenuStrip;
            this.rescanMenuButton.MenuCancelsClick = false;
            this.rescanMenuButton.Name = "rescanMenuButton";
            this.rescanMenuButton.Size = new System.Drawing.Size(125, 24);
            this.rescanMenuButton.SplitButton = false;
            this.rescanMenuButton.TabIndex = 2;
            this.rescanMenuButton.Text = "&Rescan Content";
            this.toolTip.SetToolTip(this.rescanMenuButton, "Reloads the list of content shown in the browser");
            this.rescanMenuButton.UseVisualStyleBackColor = true;
            // 
            // rescanContextMenuStrip
            // 
            this.rescanContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reloadAllToolStripMenuItem,
            this.reloadChangedToolStripMenuItem,
            this.reloadSelectedToolStripMenuItem});
            this.rescanContextMenuStrip.Name = "rescanContextMenuStrip";
            this.rescanContextMenuStrip.Size = new System.Drawing.Size(152, 70);
            this.rescanContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.rescanContextMenuStrip_Opening);
            // 
            // reloadAllToolStripMenuItem
            // 
            this.reloadAllToolStripMenuItem.Name = "reloadAllToolStripMenuItem";
            this.reloadAllToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.reloadAllToolStripMenuItem.Text = "Reload all";
            this.reloadAllToolStripMenuItem.Click += new System.EventHandler(this.reloadAllToolStripMenuItem_Click);
            // 
            // reloadChangedToolStripMenuItem
            // 
            this.reloadChangedToolStripMenuItem.Name = "reloadChangedToolStripMenuItem";
            this.reloadChangedToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.reloadChangedToolStripMenuItem.Text = "Reload changed";
            this.reloadChangedToolStripMenuItem.Click += new System.EventHandler(this.reloadChangedToolStripMenuItem_Click);
            // 
            // reloadSelectedToolStripMenuItem
            // 
            this.reloadSelectedToolStripMenuItem.Name = "reloadSelectedToolStripMenuItem";
            this.reloadSelectedToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.reloadSelectedToolStripMenuItem.Text = "Reload selected";
            this.reloadSelectedToolStripMenuItem.Click += new System.EventHandler(this.reloadSelectedToolStripMenuItem_Click);
            // 
            // infoSplitContainer
            // 
            this.infoSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.infoSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.infoSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.infoSplitContainer.Name = "infoSplitContainer";
            this.infoSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // infoSplitContainer.Panel1
            // 
            this.infoSplitContainer.Panel1.Controls.Add(this.label5);
            // 
            // infoSplitContainer.Panel2
            // 
            this.infoSplitContainer.Panel2.Controls.Add(this.label4);
            this.infoSplitContainer.Panel2.Controls.Add(this.dependencyGraphViewer);
            this.infoSplitContainer.Size = new System.Drawing.Size(283, 604);
            this.infoSplitContainer.SplitterDistance = 482;
            this.infoSplitContainer.SplitterWidth = 3;
            this.infoSplitContainer.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(222, 26);
            this.label5.TabIndex = 0;
            this.label5.Text = "TODO show some info about the content and\r\nhopefully a kind of preview too";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "&Dependencies:";
            // 
            // dependencyGraphViewer
            // 
            this.dependencyGraphViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dependencyGraphViewer.Content = null;
            this.dependencyGraphViewer.Location = new System.Drawing.Point(4, 20);
            this.dependencyGraphViewer.Name = "dependencyGraphViewer";
            this.dependencyGraphViewer.Size = new System.Drawing.Size(272, 100);
            this.dependencyGraphViewer.TabIndex = 1;
            // 
            // ContentBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.verticalSplitContainer);
            this.Name = "ContentBrowser";
            this.Size = new System.Drawing.Size(814, 604);
            this.Load += new System.EventHandler(this.ContentBrowser_Load);
            this.verticalSplitContainer.Panel1.ResumeLayout(false);
            this.verticalSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.verticalSplitContainer)).EndInit();
            this.verticalSplitContainer.ResumeLayout(false);
            this.mainTableLayoutPanel.ResumeLayout(false);
            this.mainTableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contentTreeListView)).EndInit();
            this.rescanContextMenuStrip.ResumeLayout(false);
            this.infoSplitContainer.Panel1.ResumeLayout(false);
            this.infoSplitContainer.Panel1.PerformLayout();
            this.infoSplitContainer.Panel2.ResumeLayout(false);
            this.infoSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.infoSplitContainer)).EndInit();
            this.infoSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer verticalSplitContainer;
        private System.Windows.Forms.TableLayoutPanel mainTableLayoutPanel;
        private System.Windows.Forms.Label label1;
        private SFMTK.Controls.CheckedComboBox modComboBox;
        private System.Windows.Forms.ToolTip toolTip;
        private BrightIdeasSoftware.TreeListView contentTreeListView;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox pathTextBox;
        private System.Windows.Forms.Button browseButton;
        private BrightIdeasSoftware.OLVColumn nameContentColumn;
        private BrightIdeasSoftware.OLVColumn modContentColumn;
        private System.Windows.Forms.SplitContainer infoSplitContainer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private DependencyGraphViewer dependencyGraphViewer;
        private FilterControl filterControl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button viewPathButton;
        private System.Windows.Forms.ProgressBar buildingProgressBar;
        private System.Windows.Forms.Label buildingProgressLabel;
        private BrightIdeasSoftware.OLVColumn nodeTypeContentColumn;
        private BrightIdeasSoftware.OLVColumn fileTypeContentColumn;
        private BrightIdeasSoftware.OLVColumn modOrderContentColumn;
        private MenuButton rescanMenuButton;
        private System.Windows.Forms.ContextMenuStrip rescanContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem reloadAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reloadChangedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reloadSelectedToolStripMenuItem;
        internal System.Windows.Forms.Button cancelButton;
        internal System.Windows.Forms.Button okButton;
    }
}
