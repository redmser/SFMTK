﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace SFMTK.Controls
{
    /// <summary>
    /// A RearrangingDropSink which fixes most of its issues.
    /// </summary>
    public class RearrangingDropSinkEx : RearrangingDropSink
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RearrangingDropSinkEx"/> class.
        /// </summary>
        public RearrangingDropSinkEx() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RearrangingDropSinkEx"/> class.
        /// </summary>
        /// <param name="allowFromOtherLists">If set to <c>true</c> allow dropping items from other lists.</param>
        public RearrangingDropSinkEx(bool allowFromOtherLists) : base(allowFromOtherLists) { }

        /// <summary>
        /// Whether to auto-rebuild the contents of any DataListView once their contents were changed.
        /// </summary>
        public bool UpdateDataListView { get; set; } = true;

        /// <summary>
        /// Trigger OnModelCanDrop
        /// </summary>
        /// <param name="args"></param>
        protected override void OnModelCanDrop(ModelDropEventArgs args)
        {
            base.OnModelCanDrop(args);

            //If target is index 0, count is 1 and reordering the list, do not allow drop
            if (args.SourceListView == this.ListView && args.DropTargetIndex == 0 && this.ListView.GetItemCount() == 1)
            {
                args.Effect = DragDropEffects.None;
                args.DropTargetLocation = DropTargetLocation.None;
            }
        }

        /// <summary>
        /// Do the work of processing the dropped items
        /// </summary>
        /// <param name="args"></param>
        public override void RearrangeModels(ModelDropEventArgs args)
        {
            if (this.ListView is DataListView dlv)
            {
                //DataSource needs different rearranging
                if (dlv.DataSource is IList src)
                {
                    //IList insert/remove
                    switch (args.DropTargetLocation)
                    {
                        case DropTargetLocation.AboveItem:
                            foreach (var item in args.SourceModels.Cast<object>())
                            {
                                var index = src.IndexOf(item);
                                MoveItem(src, index, args.DropTargetIndex);
                            }
                            break;
                        case DropTargetLocation.BelowItem:
                            foreach (var item in args.SourceModels.Cast<object>())
                            {
                                var index = src.IndexOf(item);
                                MoveItem(src, index, args.DropTargetIndex + 1);
                            }
                            break;
                        case DropTargetLocation.Background:
                            foreach (var item in args.SourceModels)
                            {
                                var index = src.IndexOf(item);
                                src.Add(item);
                            }
                            break;
                        default:
                            return;
                    }

                    if (args.SourceListView != this.ListView)
                    {
                        foreach (var item in args.SourceModels)
                        {
                            src.Remove(item);
                        }
                    }

                    //Update listview
                    if (this.UpdateDataListView)
                        dlv.BuildList(true);
                }
                else
                {
                    //Can't handle this data type
                    throw new NotSupportedException("The datasource has to be of type IList to allow a rearrange.");
                }
            }
            else
            {
                //Simply move/add
                base.RearrangeModels(args);
            }



            void MoveItem(IList src, int start, int end)
            {
                if (end > start)
                    end--;

                if (src is Data.ModList lst)
                {
                    //Different implementation!
                    lst.Move(start, end);
                }
                else
                {
                    //Extension method
                    src.Move(start, end);
                }
            }
        }
    }
}
