﻿using System.ComponentModel;
using System.Windows.Forms;
using SFMTK.Properties;

namespace SFMTK.Controls
{
    /// <summary>
    /// A control for displaying the error/valid state of another control.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.UserControl" />
    public partial class ErrorMarker : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorMarker" /> class.
        /// </summary>
        public ErrorMarker() : this(true)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorMarker"/> class.
        /// </summary>
        /// <param name="includeDefaults">If set to <c>true</c>, include a default set of images.</param>
        public ErrorMarker(bool includeDefaults)
        {
            //Compiler-generated
            InitializeComponent();

            if (includeDefaults)
            {
                //Init image list
                var list = new ImageList();
                list.Images.Add("Valid", Resources.tick);
                list.Images.Add("Info", Resources.information);
                list.Images.Add("Warning", Resources.error);
                list.Images.Add("Error", Resources.cross);
                this.StateImages = list;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorMarker"/> class with the specified State and Text.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="text">The text.</param>
        public ErrorMarker(ErrorState state, string text) : this()
        {
            this.SetState(state, text);
        }

        /// <summary>
        /// Sets the error marker to the specified state.
        /// </summary>
        /// <param name="state">The new state.</param>
        /// <param name="text">The corresponding display text.</param>
        protected virtual void SetState(ErrorState state, string text)
        {
            this.State = state;
            this.Text = text;
        }

        /// <summary>
        /// Gets or sets the error state.
        /// </summary>
        [Description("Current error state."), DefaultValue(ErrorState.None)]
        public virtual ErrorState State
        {
            get => this._state;
            set
            {
                if (this._state == value)
                    return;

                this._state = value;
                this.UpdateState();
            }
        }

        private ErrorState _state = ErrorState.None;

        /// <summary>
        /// Gets or sets the text associated with this control.
        /// </summary>
        [Description("Text to display when hovering over the control."), DefaultValue(""), Browsable(true)]
        public override string Text
        {
            get => base.Text;
            set
            {
                base.Text = value;
                this.toolTip.SetToolTip(this.statePictureBox, value);
            }
        }

        /// <summary>
        /// Gets the images to display for each corresponding state.
        /// </summary>
        [Description("List of images to display for each of the states.")]
        public virtual ImageList StateImages { get; }

        /// <summary>
        /// Updates the image of the error marker based on the State.
        /// </summary>
        protected virtual void UpdateState()
        {
            switch (this.State)
            {
                case ErrorState.Warning:
                    this.toolTip.ToolTipIcon = ToolTipIcon.Warning;
                    break;
                case ErrorState.Error:
                    this.toolTip.ToolTipIcon = ToolTipIcon.Error;
                    break;
                default:
                    this.toolTip.ToolTipIcon = ToolTipIcon.Info;
                    break;
            }

            var statestr = this.State.ToString();
            this.toolTip.ToolTipTitle = statestr;
            this.statePictureBox.Image = this.State == ErrorState.None ? null : this.StateImages.Images[statestr];
        }
    }

    /// <summary>
    /// Severity of the error marker state.
    /// </summary>
    public enum ErrorState
    {
        /// <summary>
        /// The specified information is valid.
        /// </summary>
        Valid,
        /// <summary>
        /// While the information is valid, something of interest can be said about it.
        /// </summary>
        Info,
        /// <summary>
        /// The information is theoretically valid, but something may cause issues with it.
        /// </summary>
        Warning,
        /// <summary>
        /// The information is not valid and has to be fixed.
        /// </summary>
        Error,
        /// <summary>
        /// No state for this error marker.
        /// </summary>
        None
    }
}
