﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// A text box control which is made for searching.
    /// </summary>
    public class SearchTextBox : UserControl
    {
        //TODO: SearchTextBox - CTRL+F (customizable shortcut) should highlight the most convenient visible search textbox (or toolstrip variant)

        /// <summary>
        /// The clickable button on the searchbox.
        /// </summary>
        public ClickthroughPictureBox Button;

        /// <summary>
        /// The underlying textbox control.
        /// </summary>
        public TextBox TextBox;

        public SearchTextBox()
        {
            //Compiler-generated
            InitializeComponent();

            this.UpdateImage();
            this.Button.Click += this.OnButtonClicked;
        }

        private void InitializeComponent()
        {
            this.TextBox = new System.Windows.Forms.TextBox();
            this.Button = new SFMTK.Controls.ClickthroughPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Button)).BeginInit();
            this.SuspendLayout();
            // 
            // TextBox
            // 
            this.TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextBox.Location = new System.Drawing.Point(0, 0);
            this.TextBox.Name = "TextBox";
            this.TextBox.Size = new System.Drawing.Size(150, 20);
            this.TextBox.TabIndex = 0;
            this.TextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            this.TextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyUp);
            // 
            // Button
            // 
            this.Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button.IsClickthrough = true;
            this.Button.Location = new System.Drawing.Point(132, 2);
            this.Button.Name = "Button";
            this.Button.Size = new System.Drawing.Size(16, 16);
            this.Button.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Button.TabIndex = 1;
            this.Button.TabStop = false;
            // 
            // SearchTextBox
            // 
            this.Controls.Add(this.Button);
            this.Controls.Add(this.TextBox);
            this.Name = "SearchTextBox";
            this.Size = new System.Drawing.Size(150, 20);
            ((System.ComponentModel.ISupportInitialize)(this.Button)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        /// <summary>
        /// Gets or sets the text associated with this control.
        /// </summary>
        public new string Text
        {
            get => this.TextBox.Text;
            set => this.TextBox.Text = value;
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //TODO: SearchTextBox - detect these key presses even if form has Accept/Cancel buttons
            if (e.KeyCode == Keys.Enter)
                this.OnEnterPressed(EventArgs.Empty);
            else if (e.KeyCode == Keys.Escape)
                this.OnEscapePressed(EventArgs.Empty);
        }

        /// <summary>
        /// Whether the textbox should be cleared when the button is pressed. It is only clickable when the textbox has any contents with this mode.
        /// </summary>
        [Description("Whether the textbox should be cleared when the button is pressed."), DefaultValue(true)]
        public bool ClearOnButton { get; set; } = true;

        /// <summary>
        /// The image to display inside the text box when nothing is entered into the search bar.
        /// </summary>
        [Description("The image to display inside the text box when nothing is entered into the search bar.")]
        public virtual Image SearchImage
        {
            get => this._searchImage;
            set
            {
                if (this._searchImage == value)
                    return;

                this._searchImage = value;
                this.UpdateImage();
            }
        }
        private Image _searchImage;

        /// <summary>
        /// The image to display inside the text box when something is entered into the search bar. Acts as a clear button for the text box.
        /// </summary>
        [Description("The image to display inside the text box when something is entered into the search bar. Acts as a clear button for the text box.")]
        public virtual Image ClearImage
        {
            get => this._clearImage;
            set
            {
                if (this._clearImage == value)
                    return;

                this._clearImage = value;
                this.UpdateImage();
            }
        }
        private Image _clearImage;

        /// <summary>
        /// Raises the <see cref="E:ButtonClicked" /> event.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected virtual void OnButtonClicked(object sender, EventArgs e)
        {
            this.ButtonClicked?.Invoke(sender, e);

            if (this.ClearOnButton)
                this.Text = "";

            //Refocus the textbox
            this.Focus();
        }

        /// <summary>
        /// Raises the <see cref="E:EnterPressed" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnEnterPressed(EventArgs e) => this.EnterPressed?.Invoke(this, e);

        /// <summary>
        /// Raises the <see cref="E:EscapePressed" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnEscapePressed(EventArgs e)
        {
            this.EscapePressed?.Invoke(this, e);

            //Clear textbox
            this.Text = "";
        }

        /// <summary>
        /// Occurs when the button inside of the search box was clicked. Raised even if nothing is entered in the text box.
        /// </summary>
        [Description("The button inside of the search box was clicked.")]
        public event EventHandler ButtonClicked;

        /// <summary>
        /// Occurs when the escape key was pressed. This event may not be fired if the form has a Cancel button set.
        /// </summary>
        [Description("Occurs when the escape key was pressed.")]
        public event EventHandler EscapePressed;

        /// <summary>
        /// Occurs when the enter key was pressed. This event may not be fired if the form has an Accept button set.
        /// </summary>
        [Description("Occurs when the enter key was pressed.")]
        public event EventHandler EnterPressed;

        private void TextBox_TextChanged(object sender, EventArgs e) => this.UpdateImage();

        private void UpdateImage()
        {
            this.Button.Image = string.IsNullOrEmpty(this.Text) ? this.SearchImage : this.ClearImage;

            if (this.ClearOnButton)
                this.Button.IsClickthrough = string.IsNullOrEmpty(this.Text);
        }
    }
}
