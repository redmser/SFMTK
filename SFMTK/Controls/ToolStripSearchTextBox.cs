﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// A ToolStripTextBox designed to be used as a search bar, including a button to clear the search box.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.ToolStripTextBox" />
    [DefaultEvent("ButtonClicked")]
    public class ToolStripSearchTextBox : ToolStripTextBox, IThemeable
    {
        //BUG: ToolStripSearchTextBox - image does not show up when inside overflow menu

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolStripSearchTextBox"/> class.
        /// </summary>
        public ToolStripSearchTextBox()
        {
            this.UpdateImage();
            this.UpdateContainingForm();
            this.Button.Click += this.OnButtonClicked;
        }

        private Control _containing;

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ToolStripControlHost.KeyUp" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event data.</param>
        protected override void OnKeyUp(KeyEventArgs e)
        {
            //TODO: ToolStripSearchTextBox - detect these key presses even if form has Accept/Cancel buttons
            base.OnKeyUp(e);

            if (e.KeyCode == Keys.Enter)
                this.OnEnterPressed(EventArgs.Empty);
            else if (e.KeyCode == Keys.Escape)
                this.OnEscapePressed(EventArgs.Empty);
        }

        /// <summary>
        /// The button to display. Uses IsClickthrough to determine when it is clickable.
        /// </summary>
        public virtual ClickthroughPictureBox Button { get; } = new ClickthroughPictureBox { Size = new Size(16, 16), SizeMode = PictureBoxSizeMode.Zoom, Location = new Point(-16, -16) };

        /// <summary>
        /// Whether the textbox should be cleared when the button is pressed. It is only clickable when the textbox has any contents with this mode.
        /// </summary>
        [Description("Whether the textbox should be cleared when the button is pressed."), DefaultValue(true)]
        public bool ClearOnButton { get; set; } = true;

        /// <summary>
        /// The image to display inside the text box when nothing is entered into the search bar.
        /// </summary>
        [Description("The image to display inside the text box when nothing is entered into the search bar.")]
        public virtual Image SearchImage
        {
            get => this._searchImage;
            set
            {
                if (this._searchImage == value)
                    return;

                this._searchImage = value;
                this.UpdateImage();
            }
        }
        private Image _searchImage;

        /// <summary>
        /// The image to display inside the text box when something is entered into the search bar. Acts as a clear button for the text box.
        /// </summary>
        [Description("The image to display inside the text box when something is entered into the search bar. Acts as a clear button for the text box.")]
        public virtual Image ClearImage
        {
            get => this._clearImage;
            set
            {
                if (this._clearImage == value)
                    return;

                this._clearImage = value;
                this.UpdateImage();
            }
        }

        public string ActiveTheme { get; set; }

        private Image _clearImage;

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the event data.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            //Reposition on every draw operation
            //TODO: ToolStripSearchTextBox - try to only update button if location of toolstrip or textbox changes (or width!)
            this.Button.Location = new Point(this.Parent.Bounds.Left + this.Bounds.Right - 17, this.Parent.Bounds.Top + this.Bounds.Bottom - 21);
        }

        /// <summary>
        /// Raises the <see cref="E:ButtonClicked" /> event.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected virtual void OnButtonClicked(object sender, EventArgs e)
        {
            this.ButtonClicked?.Invoke(sender, e);

            if (this.ClearOnButton)
                this.Text = "";

            //Refocus the textbox
            this.Focus();
        }

        /// <summary>
        /// Raises the <see cref="E:EnterPressed" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnEnterPressed(EventArgs e) => this.EnterPressed?.Invoke(this, e);

        /// <summary>
        /// Raises the <see cref="E:EscapePressed" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnEscapePressed(EventArgs e)
        {
            this.EscapePressed?.Invoke(this, e);

            //Clear textbox
            this.Text = "";
        }

        /// <summary>
        /// Occurs when the button inside of the search box was clicked. Raised even if nothing is entered in the text box.
        /// </summary>
        [Description("The button inside of the search box was clicked.")]
        public event EventHandler ButtonClicked;

        /// <summary>
        /// Occurs when the escape key was pressed. This event may not be fired if the form has a Cancel button set.
        /// </summary>
        [Description("Occurs when the escape key was pressed.")]
        public event EventHandler EscapePressed;

        /// <summary>
        /// Occurs when the enter key was pressed. This event may not be fired if the form has an Accept button set.
        /// </summary>
        [Description("Occurs when the enter key was pressed.")]
        public event EventHandler EnterPressed;

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ToolStripItem.TextChanged" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnTextChanged(EventArgs e)
        {
            this.UpdateImage();
            base.OnTextChanged(e);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ParentChanged" /> event.
        /// </summary>
        /// <param name="oldParent">The original parent of the item.</param>
        /// <param name="newParent">The new parent of the item.</param>
        protected override void OnParentChanged(ToolStrip oldParent, ToolStrip newParent)
        {
            base.OnParentChanged(oldParent, newParent);
            this.UpdateContainingForm();
        }

        private void UpdateImage()
        {
            this.Button.Image = string.IsNullOrEmpty(this.Text) ? this.SearchImage : this.ClearImage;

            if (this.ClearOnButton)
                this.Button.IsClickthrough = string.IsNullOrEmpty(this.Text);
        }

        private void UpdateContainingForm()
        {
            if (this.DesignMode)
                return;

            //Check if added to ToolStrip
            if (this.Parent != null)
            {
                //Remove picture control from old form
                if (this._containing != null)
                    this._containing.Controls.Remove(this.Button);

                //Find container through hierchary
                this._containing = this.Parent;
                while (this._containing != null && !(this._containing is ScrollableControl || this._containing is GroupBox))
                {
                    this._containing = this._containing.Parent;
                }

                //Found anything?
                if (this._containing == null)
                    return;

                //Add control to container
                var container = FindContainer(this._containing);
                if (container == null)
                    return;

                container.Controls.Add(this.Button);
                this.Button.BringToFront();
            }



            Control FindContainer(Control ctrl)
            {
                if (ctrl is ToolStripPanel || ctrl is ToolStrip)
                    return FindContainer(ctrl.Parent);
                return ctrl;
            }
        }
    }
}
