﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// Provides a way for a <see cref="ToolStripItem"/> to dynamically expand into a set of items every time its parent wants to show it.
    /// </summary>
    public abstract class DynamicToolStripItem : ToolStripItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicToolStripItem"/> class.
        /// </summary>
        public DynamicToolStripItem() : base()
        {
            this.AutoSize = false;
        }

        /// <summary>
        /// Gets the size and location of the item.
        /// </summary>
        public override Rectangle Bounds => Rectangle.Empty;

        /// <summary>
        /// Gets the default size of the item.
        /// </summary>
        protected override Size DefaultSize => Size.Empty;

        /// <summary>
        /// Gets the default margin of an item.
        /// </summary>
        protected override Padding DefaultMargin => Padding.Empty;

        /// <summary>
        /// Gets the internal spacing characteristics of the item.
        /// </summary>
        protected override Padding DefaultPadding => Padding.Empty;

        /// <summary>
        /// Gets or sets whether items should automatically be reloaded every time they are to be displayed.
        /// If set to <c>false</c>, they will only reload on manual refresh, or on re-layout.
        /// </summary>
        public bool AutoReloadDynamicItems
        {
            get => this._autoReloadItems;
            set
            {
                if (this._autoReloadItems == value)
                    return;

                this._autoReloadItems = value;

                if (value)
                    this.ReloadDynamicItems();
            }
        }
        private bool _autoReloadItems = true;

        /// <summary>
        /// Forces the list of items displayed to be reloaded as soon as they need to be shown again.
        /// </summary>
        /// <remarks>
        /// The Items / DropDownItems collection of the owner ToolStrip may not be updated
        /// until the control needs to redo its layouting (mostly done when the child items are to be shown).
        /// </remarks>
        public void ReloadDynamicItems() => SetLayoutRequired(this._lastOwner, true);

        /// <summary>
        /// Returns the list of <see cref="ToolStripItem"/>s that this item would be replaced
        /// with if it were to be displayed as an entry somewhere.
        /// </summary>
        public abstract IEnumerable<ToolStripItem> GetDynamicItems();

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Layout" /> event and re-adds our items to the parent.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.LayoutEventArgs" /> that contains the event data.</param>
        protected override void OnLayout(LayoutEventArgs e)
        {
            //Add our dynamic items
            AddItems(e.AffectedControl as ToolStrip ?? this.Owner);
        }

        /// <summary>
        /// Refreshes the list of items. This item has to be located inside of the <see cref="ToolStrip"/> specified.
        /// </summary>
        /// <param name="owner">The <see cref="ToolStrip"/> to add the items to.</param>
        protected void AddItems(ToolStrip owner)
        {
            //Remove old items of previous owner
            if (this._lastOwner != null)
            {
                foreach (var item in this._addedItems)
                    this._lastOwner.Items.Remove(item);
            }

            //We are about to regenerate the item list
            this.OnRegenerate(EventArgs.Empty);

            //Update owner and list of items to add
            this._lastOwner = owner;
            this._addedItems = this.GetDynamicItems().ToArray();

            //Calculate offset index, since there's other items in here too
            var offset = this._lastOwner.Items.IndexOf(this);

            //Add our items
            for (var i = 0; i < this._addedItems.Length; i++)
                this._lastOwner.Items.Insert(i + offset, this._addedItems[i]);

            //Auto-reload if wanted (by telling the layout engine to redo layout ASAP)
            //FIXME: DynamicToolStripItem - setting layout required here will instantly cause a second reload of the items, even if unneeded
            //  -> how can we delay it so it'll only refresh after a re-open?
            if (this.AutoReloadDynamicItems)
                SetLayoutRequired(this._lastOwner, true);
        }

        /// <summary>
        /// Sets the <c>LayoutRequired</c> property of the specified <see cref="ToolStrip"/>.
        /// </summary>
        protected void SetLayoutRequired(ToolStrip owner, bool value)
        {
            if (owner == null)
                return;
            this._layoutRequired.SetValue(owner, value);
        }

        private readonly PropertyInfo _layoutRequired = typeof(ToolStrip).GetProperty("LayoutRequired", BindingFlags.Instance | BindingFlags.NonPublic);

        /// <summary>
        /// Items that we have added to a menu.
        /// </summary>
        protected ToolStripItem[] _addedItems;

        /// <summary>
        /// Last owner <see cref="ToolStrip"/> of this item. Our items are part of this owner.
        /// </summary>
        protected ToolStrip _lastOwner;

        /// <summary>
        /// Occurs when the list of dynamic <see cref="ToolStripItem"/>s is regenerated and
        /// needs to be displayed (a call to <see cref="GetDynamicItems"/> will follow).
        /// </summary>
        public event EventHandler Regenerate;

        /// <summary>
        /// Raises the <see cref="E:Regenerate" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnRegenerate(EventArgs e) => this.Regenerate?.Invoke(this, e);
    }

    /// <summary>
    /// Debugging class for <see cref="DynamicToolStripItem"/>
    /// </summary>
    public class TestDynamicTSMI : DynamicToolStripItem
    {
        public override IEnumerable<ToolStripItem> GetDynamicItems()
        {
            var random = new Random();
            for (var i = 0; i < 10; i++)
            {
                yield return new ToolStripMenuItem((random.NextDouble() * 100).ToString(), null, (s, e) => MessageBox.Show($"Clicked item #{(i + 1)}!"));
            }
        }
    }
}
