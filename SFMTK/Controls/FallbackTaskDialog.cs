﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Ookii.Dialogs.Wpf;

/*

License agreement for Ookii.Dialogs.

Copyright © Sven Groot (Ookii.org) 2009
All rights reserved.


Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met:

1) Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
2) Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 
3) Neither the name of the ORGANIZATION nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.

*/

namespace SFMTK
{
    /// <summary>
    /// Class for calling Windows Vista+ task dialogs, which may need to fallback to a regular MessageBox.
    /// </summary>
    public class FallbackTaskDialog : TaskDialog
    {
        //IMP: FallbackTaskDialog - why use Ookii.Dialogs.Wpf instead of the WinForms version? What's different?
        //TODO: FallbackTaskDialog - parameter to include Exception (passing to Logger, better formatting, ...)

        /// <summary>
        /// Initializes a new instance of the <see cref="FallbackTaskDialog"/> without any properties.
        /// </summary>
        public FallbackTaskDialog() : base()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FallbackTaskDialog"/> with text properties. Uses the info icon and an OK button.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        public FallbackTaskDialog(string text, string headline, string caption)
            : this(text, headline, caption, FallbackDialogIcon.Info, new[] { new FallbackDialogButton(DialogResult.OK) })
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FallbackTaskDialog"/> with basic properties.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="icon">The big icon of the dialog box.</param>
        /// <param name="buttons">The buttons to include on the dialog box.</param>
        public FallbackTaskDialog(string text, string headline, string caption, FallbackDialogIcon icon, FallbackDialogButton[] buttons)
            : this(text, headline, caption, null, icon, buttons)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FallbackTaskDialog"/> with most important properties.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="extraInfo">The information footer to include additional (mostly error) information in.</param>
        /// <param name="icon">The big icon of the dialog box.</param>
        /// <param name="buttons">The buttons to include on the dialog box.</param>
        public FallbackTaskDialog(string text, string headline, string caption, string extraInfo, FallbackDialogIcon icon, FallbackDialogButton[] buttons)
        {
            this.Content = text;
            this.WindowTitle = caption;
            this.SetIcon(icon);
            this.ExpandedInformation = extraInfo;
            this.MainInstruction = headline;
            foreach (var button in buttons)
            {
                this.Buttons.Add(button);
            }
        }

        /// <summary>
        /// Creates a <see cref="FallbackTaskDialog"/> with text properties. Uses the info icon and an OK button.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        public static DialogResult ShowDialog(string text, string headline, string caption)
            => ShowDialog(text, headline, caption, FallbackDialogIcon.Info, new FallbackDialogButton(DialogResult.OK));

        /// <summary>
        /// Creates a <see cref="FallbackTaskDialog"/> with basic properties.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="icon">The big icon of the dialog box.</param>
        /// <param name="buttons">The buttons to include on the dialog box.</param>
        public static DialogResult ShowDialog(string text, string headline, string caption, FallbackDialogIcon icon, params FallbackDialogButton[] buttons)
            => ShowDialog(text, headline, caption, null, icon, buttons);

        /// <summary>
        /// Creates a <see cref="FallbackTaskDialog"/> with most important properties.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="extraInfo">The information footer to include additional (mostly error) information in.</param>
        /// <param name="icon">The big icon of the dialog box.</param>
        /// <param name="buttons">The buttons to include on the dialog box.</param>
        public static DialogResult ShowDialog(string text, string headline, string caption, string extraInfo, FallbackDialogIcon icon, params FallbackDialogButton[] buttons)
            => new FallbackTaskDialog(text, headline, caption, extraInfo, icon, buttons).ShowDialogOrFallback().Result;

        /// <summary>
        /// Creates a <see cref="FallbackTaskDialog"/> with text properties. Uses the info icon and an OK button.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        public static DialogResult ShowDialogLog(string text, string headline, string caption)
            => ShowDialogLog(text, headline, caption, FallbackDialogIcon.Info, new FallbackDialogButton(DialogResult.OK));

        /// <summary>
        /// Creates a <see cref="FallbackTaskDialog"/> with basic properties.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="icon">The big icon of the dialog box.</param>
        /// <param name="buttons">The buttons to include on the dialog box.</param>
        public static DialogResult ShowDialogLog(string text, string headline, string caption, FallbackDialogIcon icon, params FallbackDialogButton[] buttons)
            => ShowDialogLog(text, headline, caption, null, icon, buttons);

        /// <summary>
        /// Creates a <see cref="FallbackTaskDialog"/> with most important properties.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="extraInfo">The information footer to include additional (mostly error) information in.</param>
        /// <param name="icon">The big icon of the dialog box.</param>
        /// <param name="buttons">The buttons to include on the dialog box.</param>
        public static DialogResult ShowDialogLog(string text, string headline, string caption, string extraInfo, FallbackDialogIcon icon, params FallbackDialogButton[] buttons)
        {
            var td = new FallbackTaskDialog(text, headline, caption, extraInfo, icon, buttons);
            td.LogDialog();
            return td.ShowDialogOrFallback().Result;
        }

        /// <summary>
        /// Writes a log entry based on the dialog's properties.
        /// </summary>
        public virtual void LogDialog()
        {
            var logger = NLog.LogManager.GetLogger(this.WindowTitle.Replace(" ", ""));
            switch (this.icon)
            {
                default:
                    logger.Info("{0} {1}", this.MainInstruction, this.Content);
                    break;
                case FallbackDialogIcon.Warning:
                    logger.Warn("{0} {1}", this.MainInstruction, this.Content);
                    break;
                case FallbackDialogIcon.Error:
                    logger.Error("{0} {1}", this.MainInstruction, this.Content);
                    break;
            }
        }

        /// <summary>
        /// Shows the task dialog, falling back if needed.
        /// </summary>
        public virtual FallbackDialogButton ShowDialogOrFallback()
        {
            if (!Properties.Settings.Default.ForceFallbackDialogs && TaskDialog.OSSupportsTaskDialogs)
            {
                try
                {
                    //Show task dialog
                    return (FallbackDialogButton)this.ShowDialog();
                }
                catch (Exception ex)
                {
                    //Show a fallback
                    return new FallbackDialogButton(this.ShowFallbackDialog(ex)) { FallbackDialog = true };
                }
            }
            else
            {
                //Show a fallback
                return new FallbackDialogButton(this.ShowFallbackDialog()) { FallbackDialog = true };
            }
        }

        /// <summary>
        /// Displays a regular fallback message box for this task dialog.
        /// </summary>
        /// <param name="ex">The exception to include, in case it tried to display a TaskDialog instead but failed.</param>
        private DialogResult ShowFallbackDialog(Exception ex = null)
        {
            //Show regular msgbox
            var text = this.MainInstruction + "\n\n" ?? "";
            text += this.Content;
            text += this.ExpandedInformation ?? "";
            var hasYesNo = false;
            var hasCancel = false;

            //Remove URLs
            text = Regex.Replace(text, "(<a href=\".*\">)(.+)(</a>)", m => m.Groups[2].Value, RegexOptions.Singleline | RegexOptions.IgnoreCase);

            //Notify user about exception
            if (ex != null)
            {
                text += "\n\n\n(ERROR - Could not show TaskDialog! Exception info:\n" + ex.GetInfo();
            }

            //Apply button types
            foreach (FallbackDialogButton bt in this.Buttons)
            {
                if (bt.Result == DialogResult.Yes)
                    hasYesNo = true;

                if (bt.Result == DialogResult.Cancel)
                    hasCancel = true;
            }

            //Apply icon
            var icon = MessageBoxIcon.None;
            switch (this.MainIcon)
            {
                case TaskDialogIcon.Information:
                    icon = MessageBoxIcon.Information;
                    break;
                case TaskDialogIcon.Shield:
                case TaskDialogIcon.Warning:
                    icon = MessageBoxIcon.Warning;
                    break;
                case TaskDialogIcon.Error:
                    icon = MessageBoxIcon.Error;
                    break;
            }

            //Ugly way to show msgbox now
            if (hasYesNo)
                return MessageBox.Show(text, this.WindowTitle, hasCancel ? MessageBoxButtons.YesNoCancel : MessageBoxButtons.YesNo, icon);
            else
                return MessageBox.Show(text, this.WindowTitle, hasCancel ? MessageBoxButtons.OKCancel : MessageBoxButtons.OK, icon);
        }

        private void SetIcon(FallbackDialogIcon icon)
        {
            this.icon = icon;
            switch (this.icon)
            {
                case FallbackDialogIcon.None:
                    this.MainIcon = TaskDialogIcon.Custom;
                    this.CustomMainIcon = null;
                    break;
                case FallbackDialogIcon.Error:
                    this.MainIcon = TaskDialogIcon.Error;
                    break;
                case FallbackDialogIcon.Question:
                    this.MainIcon = TaskDialogIcon.Custom;
                    //NYI: FallbackTaskDialog - question icon
                    break;
                case FallbackDialogIcon.Warning:
                    this.MainIcon = TaskDialogIcon.Warning;
                    break;
                case FallbackDialogIcon.Info:
                    this.MainIcon = TaskDialogIcon.Information;
                    break;
                case FallbackDialogIcon.Custom:
                    this.MainIcon = TaskDialogIcon.Custom;
                    break;
            }
        }

        protected FallbackDialogIcon icon;
    }

    /// <summary>
    /// Main icon for a <see cref="FallbackTaskDialog"/>.
    /// </summary>
    public enum FallbackDialogIcon
    {
        None,
        Info,
        Question,
        Warning,
        Error,
        Custom
    }
}
