﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Contents;

namespace SFMTK.Controls
{
    /// <summary>
    /// <see cref="DynamicToolStripItem"/> for a list of content types to create a new file of.
    /// </summary>
    public class ContentsDynamicToolStripItem : DynamicToolStripItem
    {
        //TODO: ContentsDynamicToolStripItem - disable autoreload, only refresh when modules define(d) content types
        //TODO: ContentsDynamicToolStripItem - switch to command system for creating the new content

        /// <summary>
        /// Returns the list of <see cref="ToolStripItem" />s that this item would be replaced
        /// with if it were to be displayed as an entry somewhere.
        /// </summary>
        public override IEnumerable<ToolStripItem> GetDynamicItems()
        {
            //Add content dropdown entries
            foreach (Content cnt in Content.ContentList.OfType<IInstantiatable>())
            {
                var name = Content.GetTypeName(cnt);
                var tsmi = new ToolStripMenuItem(name, cnt.Image, NewContent);
                tsmi.Tag = cnt.GetType();
                tsmi.ToolTipText = "Creates a new " + name;
                yield return tsmi;
            }
        }

        /// <summary>
        /// Called when a new content menu entry was clicked, adding the content gotten from the name to the dock panel.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void NewContent(object sender, System.EventArgs e)
        {
            var tag = ((ToolStripMenuItem)sender).Tag;
            var nc = (Content)System.Activator.CreateInstance((System.Type)tag);
            nc.FakeFile = true;
            nc.FilePath = nc.GetNewName();
            //NYI: ContentsDynamicToolStripItem - before adding new content, allow user to specify the settings he'd usually be able to specify in the NewContent dialog
            Program.MainDPM.AddContent(nc);
        }
    }
}
