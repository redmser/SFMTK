﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// <see cref="DynamicToolStripItem"/> for populating the list of setups.
    /// </summary>
    public class SetupsDynamicToolStripItem : DynamicToolStripItem
    {
        //TODO: SetupsDynamicToolStripItem - disable auto-reload, only refresh item list whenever the setup list was changed
        //TODO: SetupsDynamicToolStripItem - replace Apply/LaunchSetup methods with Command system substitution

        public SetupsDynamicToolStripItem(bool launch) : base()
        {
            this.LaunchSFM = launch;
        }

        /// <summary>
        /// Gets or sets whether the selected setup should be applied only, or launch SFM as well.
        /// </summary>
        public bool LaunchSFM { get; set; }

        /// <summary>
        /// Returns the list of <see cref="ToolStripItem" />s that this item would be replaced
        /// with if it were to be displayed as an entry somewhere.
        /// </summary>
        public override IEnumerable<ToolStripItem> GetDynamicItems()
        {
            foreach (var setup in Properties.Settings.Default.Setups)
                yield return MenuItemFromSetup(setup.Name);
        }

        private ToolStripMenuItem MenuItemFromSetup(string setup)
        {
            var action = (this.LaunchSFM ? (EventHandler)this.LaunchSetup : this.ApplySetup);
            var tsmi = new ToolStripMenuItem(setup, null, action);
            tsmi.Name = setup;
            tsmi.ToolTipText = (this.LaunchSFM ? $"Launches SFM with the {setup} setup" : $"Applies the {setup} setup");

            if (setup == Properties.Settings.Default.ActiveSetup)
                tsmi.Checked = true;

            return tsmi;
        }

        private void ApplySetup(object sender, EventArgs e)
        {
            var setupname = ((ToolStripItem)sender).Name;
            var setup = GetSetup(setupname);
            setup.Apply();
        }

        private void LaunchSetup(object sender, EventArgs e)
        {
            var setupname = ((ToolStripItem)sender).Name;
            var setup = GetSetup(setupname);
            setup.Apply();
            setup.Launch();
        }

        private static Setup GetSetup(string name)
        {
            var setup = Properties.Settings.Default.Setups.FirstOrDefault(s => s.Name == name);
            if (setup == null)
            {
                //Cant find setup!
                FallbackTaskDialog.ShowDialog("No setup with this name could be found.", "Unknown setup!", "Unknown setup name",
                    FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));
                return null;
            }
            return setup;
        }
    }
}
