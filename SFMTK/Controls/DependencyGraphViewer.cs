﻿using System;
using System.Windows.Forms;
using Microsoft.Msagl.Drawing;
using Microsoft.Msagl.GraphViewerGdi;
using SFMTK.Contents;

namespace SFMTK.Controls
{
    /// <summary>
    /// A control to display a dependency graph of a certain Content.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.UserControl" />
    public partial class DependencyGraphViewer : UserControl
    {
        //TODO: DependencyGraphViewer - design of dependency graph, especially how to easily show which mod
        //  one file is loaded by (maybe multiple mod names inside of one "content" block?)
        //TODO: DependencyGraphViewer - allowing panning the view using space-drag or middleclick
        //TODO: DependencyGraphViewer - disable editing of edges (when clicking on them)
        //TODO: DependencyGraphViewer - toolbar including: filter content by type, filter content by name, reset node layout

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyGraphViewer"/> class.
        /// </summary>
        public DependencyGraphViewer()
        {
            //Compiler-generated
            InitializeComponent();

            //Create viewer
            this.GraphViewer = new GViewer();
            this.GraphViewer.Dock = DockStyle.Fill;
            this.GraphViewer.ToolBarIsVisible = false;
            this.GraphViewer.DoubleClick += this.GraphViewer_DoubleClick;

            this.GraphViewer.Visible = false;
            this.Controls.Add(this.GraphViewer);
        }

        private void GraphViewer_DoubleClick(object sender, System.EventArgs e)
        {
            if (this.GraphViewer.SelectedObject is Node node)
            {
                if (node.Id == Constants.FindUsagesNodeId)
                {
                    //NYI: DependencyGraphViewer - find usages of this content, expand the node graph
                    node.LabelText = "Finding usages...";
                }
                else
                {
                    //Open content
                    if (node.Id == this.Content.FilePath)
                    {
                        //TODO: DependencyGraphViewer - Own content doubleclicked, what do?
                    }
                    else
                    {
                        //Open the content in a new tab
                        try
                        {
                            Program.MainDPM.AddContent(Content.NewContentOfType(node.Id, true, null));
                        }
                        catch (ArgumentException)
                        {
                            //NYI: DependencyGraphViewer - ask user of type if unknown (standardize this?)
                            throw new NotImplementedException();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyGraphViewer"/> class displaying the specified Content instance.
        /// </summary>
        /// <param name="content">The content.</param>
        public DependencyGraphViewer(Content content) : this()
        {
            this.Content = content;
        }

        /// <summary>
        /// The content to display in this graph.
        /// </summary>
        public Content Content
        {
            get => this._content;
            set
            {
                if (this._content == value)
                    return;

                this._content = value;
                this.UpdateGraph();
            }
        }
        private Content _content;

        private void UpdateGraph()
        {
            if (this.Content == null)
            {
                //No content assigned to graph, hide the viewer
                this.GraphViewer.Visible = false;
            }
            else
            {
                //Show viewer with graph
                this.GraphViewer.Graph = this.Content.DependencyGraph;
                this.GraphViewer.Visible = true;
            }
        }

        /// <summary>
        /// The graph viewer which shows the dependency graph in this control.
        /// </summary>
        public GViewer GraphViewer { get; }
    }
}
