﻿using System;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Contents;
using SFMTK.Properties;

namespace SFMTK.Controls.ContentWidgets
{
    public partial class ThemeContentWidget : ContentWidget
    {
        //TODO: ThemeContentWidget - hide the ContentInfoWidget toolbar or at least remove the toggle button no one needs
        //TODO: ThemeContentWidget - add a button to select this theme as the active theme
        //TODO: ThemeContentWidget - toggle to switch to "textual editing mode" -> edit json file in a regular ol' text editor
        //TODO: ThemeContentWidget - if possible, separate certain logic things into different "tabs" (such as theming of lists)
        //TODO: ThemeContentWidget - if possible, show a different color for all items that inherit their value from the base OR from another property
        //TODO: ThemeContentWidget - ask to save if closing dialog with pending changes - be sure to actually undo the changes if user decides to
        //  -> similarly, ask to save if switching theme to another one (OR, if possible, mark all changed themes with a star and
        //     handle unsaved themes like normal content... maybe I should actually handle it just like content???
        //BUG: ThemeContentWidget - rightclick menu shows even if no property is rightclicked (either disable items then, or don't show context menu)

        public ThemeContentWidget() : base()
        {
            //Compiler-generated
            InitializeComponent();

            //Edit toolstrip
            var toolstrip = this.mainPropertyGrid.GetToolStrip();

            //Hide the "property pages" button, since it's greyed out anyway
            toolstrip.Items.OfType<ToolStripButton>().Last().Visible = false;

            //Add the save button to the toolstrip
            var button = new ToolStripButton("", Resources.disk, this.SaveTheme);
            button.ToolTipText = "Save";
            toolstrip.Items.Add(button);

            toolstrip.Items.Add(new ToolStripSeparator());

            //Add tabs to toolstrip
            button = new ToolStripButton("General", Resources.bricks, this.SelectTab, "generalTSB");
            button.CheckOnClick = true;
            button.Checked = true;
            toolstrip.Items.Add(button);

            button = new ToolStripButton("Docking", Resources.application_side_contract, this.SelectTab, "dockingTSB");
            button.CheckOnClick = true;
            toolstrip.Items.Add(button);
        }

        public override Content Content
        {
            get => base.Content;
            set
            {
                //FIXME: ThemeContentWidget - kind of a bad system to work with
                base.Content = value;
                this.UpdateSelectedObject();
            }
        }

        public UITheme Theme => (UITheme)this.Content?.Data;

        private void UpdateReadOnly() => this.mainPropertyGrid.ReadOnly = this.Theme == UITheme.SystemDefaultTheme;

        private void SaveTheme(object sender, EventArgs e)
        {
            //NYI: ThemeContentWidget - save theme (should otherwise reset theme changes, after prompted, to what they used to be when closing dialog!)
            throw new NotImplementedException();
        }

        private void cloneButton_Click(object sender, EventArgs e)
        {
            //NYI: ThemeContentWidget - clone selected theme, creating new one with current's settings
            throw new NotImplementedException();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            //NYI: ThemeContentWidget - delete the selected theme (what if it's the current one? fallback to default? or to "source" theme?)
            throw new NotImplementedException();
        }

        /// <summary>
        /// Selects a new tab, changes which object to edit in the PropertyGrid - based on the sender's name.
        /// </summary>
        private void SelectTab(object sender, EventArgs e)
        {
            foreach (var item in this.mainPropertyGrid.GetToolStrip().Items.OfType<ToolStripButton>())
            {
                item.Checked = false;
            }

            var tsb = ((ToolStripButton)sender);
            tsb.Checked = true;

            this._currentTab = tsb.Name;
            this.UpdateSelectedObject();
        }

        private string _currentTab = "generalTSB";

        private void UpdateSelectedObject()
        {
            switch (this._currentTab)
            {
                //BUG: ThemeContentWidget - cannot edit certain properties, most likely related to the way they are implemented
                //  -> Custom themes should allow setting each property as well, easy implementation with auto get/set inits?
                case "generalTSB":
                    this.mainPropertyGrid.SelectedObject = this.Theme;
                    break;
                case "dockingTSB":
                    //TODO: ThemeContentWidget - better editing of strip theme (probably combined with creating a new class to allow for serializing it)
                    this.mainPropertyGrid.SelectedObject = this.Theme.StripTheme;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(this._currentTab));
            }
        }

        private void mainPropertyGrid_PropertyValueChanged(object sender, PropertyValueChangedEventArgs e)
        {
            //IData notify
            this.Theme.OnPropertyChanged(this, e.ChangedItem.Label);

            //Reload theme of all open forms
            //FIXME: ThemeContentWidget - do not reload its own theme! maybe use system default at all times as well, not sure?
            //UITheme.ApplyThemeToAll(true);
        }

        private void mainPropertyGrid_SelectedObjectsChanged(object sender, EventArgs e) => this.UpdateReadOnly();

        //CHANGE THIS TO COMMAND SYSTEM, OMG

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //NYI: ThemeContentWidget - open editor for selected value (possible?)
            throw new NotImplementedException();
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //NYI: ThemeContentWidget - Reset to default value (either as defined by base theme, else by UIThemeColors.cs)
            throw new NotImplementedException();
        }
    }
}
