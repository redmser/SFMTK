﻿namespace SFMTK.Controls.ContentWidgets
{
    partial class ThemeContentWidget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainPropertyGrid = new SFMTK.Controls.ReadOnlyPropertyGrid();
            this.propertyGridContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propertyGridContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainPropertyGrid
            // 
            this.mainPropertyGrid.ContextMenuStrip = this.propertyGridContextMenuStrip;
            this.mainPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPropertyGrid.Location = new System.Drawing.Point(0, 0);
            this.mainPropertyGrid.Name = "mainPropertyGrid";
            this.mainPropertyGrid.ReadOnly = false;
            this.mainPropertyGrid.Size = new System.Drawing.Size(284, 262);
            this.mainPropertyGrid.TabIndex = 1;
            this.mainPropertyGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.mainPropertyGrid_PropertyValueChanged);
            this.mainPropertyGrid.SelectedObjectsChanged += new System.EventHandler(this.mainPropertyGrid_SelectedObjectsChanged);
            // 
            // propertyGridContextMenuStrip
            // 
            this.propertyGridContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.resetToolStripMenuItem});
            this.propertyGridContextMenuStrip.Name = "propertyGridContextMenuStrip";
            this.propertyGridContextMenuStrip.Size = new System.Drawing.Size(103, 48);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Image = global::SFMTK.Properties.Resources.pencil;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.editToolStripMenuItem.Text = "&Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Image = global::SFMTK.Properties.Resources.arrow_undo;
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.resetToolStripMenuItem.Text = "&Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // ThemeContentWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainPropertyGrid);
            this.Name = "ThemeContentWidget";
            this.PreferredStartPosition = WeifenLuo.WinFormsUI.Docking.DockState.Float;
            this.propertyGridContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ReadOnlyPropertyGrid mainPropertyGrid;
        private System.Windows.Forms.ContextMenuStrip propertyGridContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
    }
}
