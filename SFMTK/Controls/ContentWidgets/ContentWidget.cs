﻿using System.ComponentModel;
using System.Windows.Forms;
using SFMTK.Contents;
using WeifenLuo.WinFormsUI.Docking;

namespace SFMTK.Controls.ContentWidgets
{
    /// <summary>
    /// A widget used to display content.
    /// </summary>
    public partial class ContentWidget : UserControl, IThemeable
    {
        // Notes to anyone looking to extend the Content system:
        // - Every type of content may create their own ContentWidget subclass if needed.
        // - Some properties exist to extend beyond the constraints of the ContentWidget.

        //IMP: ContentWidget - rename this, as it's incredibly irritating to work with ContentWidget and ContentInfoWidget even though the former ISN'T EVEN A WIDGET
        //FIXME: ContentWidget - menu merging will most likely not work with this system and has to be done differently once needed

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentWidget"/> class.
        /// </summary>
        public ContentWidget()
        {
            //Compiler-generated
            InitializeComponent();
        }

        /// <summary>
        /// The content object assigned to this widget.
        /// </summary>
        public virtual Content Content { get; set; }

        /// <summary>
        /// If set, extends the default ContentInfoWidget header ToolStrip with items from this ToolStrip.
        /// </summary>
        public virtual ToolStrip HeaderStrip { get; }
        //TODO: ContentWidget - can this be done using toolstrip merging? would allow for specifying index, old/new content etc

        /// <summary>
        /// Gets or sets how the ContentInfoWidget should be spawned.
        /// </summary>
        [Description("How the owning ContentInfoWidget should be created.")]
        public DockState PreferredStartPosition { get; set; } = DockState.Document;

        /// <summary>
        /// Gets or sets the theme currently active on this control. This property is never set to the same value that it already has.
        /// </summary>
        public string ActiveTheme { get; set; }
    }
}
