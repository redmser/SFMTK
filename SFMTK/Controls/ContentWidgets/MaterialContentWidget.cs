﻿using SFMTK.Commands;

namespace SFMTK.Controls.ContentWidgets
{
    /// <summary>
    /// A ContentWidget for material content.
    /// </summary>
    /// <seealso cref="SFMTK.Contents.ContentWidget" />
    public partial class MaterialContentWidget : ContentWidget, IEditableData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaterialContentWidget"/> class.
        /// </summary>
        public MaterialContentWidget() : base()
        {
            //Compiler-generated
            InitializeComponent();

            //Link dirty state to content
            this.textEditor.TextEditor.Document.PropertyChanged += (s, e) => this.Content.IsDirty = !this.textEditor.TextEditor.Document.UndoStack.IsOriginalFile;
        }

        public Command CutCommand => ((IEditableData)this.textEditor).CutCommand;
        public Command CopyCommand => ((IEditableData)this.textEditor).CopyCommand;
        public Command PasteCommand => ((IEditableData)this.textEditor).PasteCommand;
        public Command DeleteCommand => ((IEditableData)this.textEditor).DeleteCommand;
        public bool CanDelete => ((IEditableData)this.textEditor).CanDelete;
        public bool CanCopy => ((IEditableData)this.textEditor).CanCopy;
        public bool CanPaste => ((IEditableData)this.textEditor).CanPaste;
    }
}
