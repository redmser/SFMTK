﻿using System.Windows.Forms;

namespace SFMTK.Controls.ContentWidgets
{
    public partial class ContentWidget : UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ContentWidget
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Name = "ContentWidget";
            this.Text = "Content";
            this.ResumeLayout(false);

        }
    }
}
