﻿using System;
using System.IO;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// An entry in the DetectedSesssions list.
    /// </summary>
    public partial class DetectedSessionEntry : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DetectedSessionEntry"/> class.
        /// </summary>
        public DetectedSessionEntry()
        {
            //Compiler-generated
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DetectedSessionEntry"/> class.
        /// </summary>
        /// <param name="path">The default path.</param>
        public DetectedSessionEntry(string path) : this()
        {
            this.directoryTextBox.Text = path;
        }

        /// <summary>
        /// Gets or sets the path of this entry.
        /// </summary>
        public string Path
        {
            get => this.directoryTextBox.Text;
            set
            {
                this.directoryTextBox.Text = value;

                //TODO: DetectedSessionEntry - Scroll to end of textbox
                //this.directoryTextBox.ScrollToCaret();
                this.OnPathChanged(EventArgs.Empty);
            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            //Select path
            var fbd = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            fbd.Description = "Browse for a new movie session path.";
            if (Directory.Exists(this.Path))
                fbd.SelectedPath = IOHelper.GetWindowsPath(this.Path);
            else
                fbd.SelectedPath = SFM.GamePath;

            var res = fbd.ShowDialog();
            if (res ?? false)
                this.Path = fbd.SelectedPath;
        }

        private void deleteButton_Click(object sender, EventArgs e) => this.OnDeleteClick(EventArgs.Empty);

        /// <summary>
        /// Gives focus to the textbox of this control.
        /// </summary>
        public void GiveFocus()
        {
            this.Focus();
            this.directoryTextBox.Focus();
        }

        /// <summary>
        /// Occurs when the delete button has been clicked.
        /// </summary>
        public event EventHandler DeleteClick;

        /// <summary>
        /// Raises the <see cref="E:DeleteClick" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnDeleteClick(EventArgs e) => this.DeleteClick?.Invoke(this, e);

        /// <summary>
        /// Occurs when the path has changed.
        /// </summary>
        public event EventHandler PathChanged;

        /// <summary>
        /// Raises the <see cref="E:PathChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected virtual void OnPathChanged(EventArgs e) => this.PathChanged?.Invoke(this, e);
    }
}
