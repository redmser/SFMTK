﻿using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// Binding by casting values between two types.
    /// </summary>
    public class CastBinding<TData, TControl> : Binding
    {
        public CastBinding(string propertyName, object dataSource, string dataMember, DataSourceUpdateMode dataSourceUpdateMode)
            : base(propertyName, dataSource, dataMember, true, dataSourceUpdateMode)
        {
        }

        protected override void OnFormat(ConvertEventArgs cevent)
        {
            cevent.Value = (TControl)cevent.Value;
        }

        protected override void OnParse(ConvertEventArgs cevent)
        {
            cevent.Value = (TData)cevent.Value;
        }
    }
}
