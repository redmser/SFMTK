﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace SFMTK.Controls
{
    /// <summary>
    /// Wrapper for the ColumnComparer, allowing it to take a custom comparer.
    /// </summary>
    public class ColumnComparerWrapper : IComparer, IComparer<OLVListItem>
    {
        /// <summary>
        /// Create a ColumnComparer that will order the rows in a list view according
        /// to the values in a given column
        /// </summary>
        /// <param name="col">The column whose values will be compared</param>
        /// <param name="order">The ordering for column values</param>
        public ColumnComparerWrapper(IComparer comp, OLVColumn col, SortOrder order)
        {
            this._comp = comp;
            this._column = col;
            this._sortOrder = order;
        }

        /// <summary>
        /// Compare two rows
        /// </summary>
        /// <param name="x">row1</param>
        /// <param name="y">row2</param>
        /// <returns>An ordering indication: -1, 0, 1</returns>
        public int Compare(object x, object y) => this.Compare((OLVListItem)x, (OLVListItem)y);

        /// <summary>
        /// Compare two rows
        /// </summary>
        /// <param name="x">row1</param>
        /// <param name="y">row2</param>
        /// <returns>An ordering indication: -1, 0, 1</returns>
        public int Compare(OLVListItem x, OLVListItem y)
        {
            if (this._sortOrder == SortOrder.None)
                return 0;

            int result = 0;
            object x1 = this._column.GetValue(x.RowObject);
            object y1 = this._column.GetValue(y.RowObject);

            // Handle nulls. Null values come last
            bool xIsNull = (x1 == null || x1 == DBNull.Value);
            bool yIsNull = (y1 == null || y1 == DBNull.Value);
            if (xIsNull || yIsNull)
            {
                if (xIsNull && yIsNull)
                    result = 0;
                else
                    result = (xIsNull ? -1 : 1);
            }
            else
            {
                result = this.CompareValues(x1, y1);
            }

            if (this._sortOrder == SortOrder.Descending)
                result = 0 - result;

            return result;
        }

        /// <summary>
        /// Compare the actual values to be used for sorting
        /// </summary>
        /// <param name="x">The aspect extracted from the first row</param>
        /// <param name="y">The aspect extracted from the second row</param>
        /// <returns>An ordering indication: -1, 0, 1</returns>
        public int CompareValues(object x, object y)
        {
            if (this._comp != null)
                return this._comp.Compare(x, y);

            // Force case insensitive compares on strings
            if (x is string xAsString)
                return string.Compare(xAsString, (string)y, StringComparison.CurrentCultureIgnoreCase);
            else
            {
                var comparable = x as IComparable;
                if (comparable != null)
                    return comparable.CompareTo(y);
                else
                    return 0;
            }
        }

        private OLVColumn _column;
        private SortOrder _sortOrder;
        private IComparer _comp;
    }
}
