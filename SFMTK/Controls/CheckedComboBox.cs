﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// A combobox which shows items in a checkbox list instead.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.ComboBox" />
    public class CheckedComboBox : ComboBox
    {
        //BUG: CheckedComboBox - holding leftclick and moving the mouse down to the list does not work (insta-closes)

        /// <summary>
        /// Internal class to represent the dropdown list of the CheckedComboBox
        /// </summary>
        public class CheckboxDropdown : Form
        {
            internal class CheckboxDropdownEventArgs : EventArgs
            {
                /// <summary>
                /// Whether to assign values.
                /// </summary>
                public bool AssignValues { get; set; }

                /// <summary>
                /// Original EventArgs.
                /// </summary>
                public EventArgs EventArgs { get; set; }

                /// <summary>
                /// Initializes a new instance of the <see cref="CheckboxDropdownEventArgs"/> class.
                /// </summary>
                /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
                /// <param name="assignValues">Assign values.</param>
                public CheckboxDropdownEventArgs(EventArgs e, bool assignValues) : base()
                {
                    this.EventArgs = e;
                    this.AssignValues = assignValues;
                }
            }

            /// <summary>
            /// A custom CheckedListBox being shown within the dropdown form representing the dropdown list of the CheckedComboBox.
            /// </summary>
            public class CustomCheckedListBox : CheckedListBox
            {
                private int _curSelIndex = -1;

                /// <summary>
                /// Initializes a new instance of the <see cref="CustomCheckedListBox"/> class.
                /// </summary>
                public CustomCheckedListBox() : base()
                {
                    this.SelectionMode = SelectionMode.One;
                    //this.HorizontalScrollbar = true;
                }

                /// <summary>
                /// Intercepts the keyboard input, [Enter] confirms a selection and [Esc] cancels it.
                /// </summary>
                /// <param name="e">The Key event arguments</param>
                protected override void OnKeyDown(KeyEventArgs e)
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        // Enact selection.
                        ((CheckboxDropdown)this.Parent).OnDeactivate(new CheckboxDropdownEventArgs(null, true));
                        e.Handled = true;

                    }
                    else if (e.KeyCode == Keys.Escape)
                    {
                        // Cancel selection.
                        ((CheckboxDropdown)this.Parent).OnDeactivate(new CheckboxDropdownEventArgs(null, false));
                        e.Handled = true;

                    }
                    else if (e.KeyCode == Keys.Delete)
                    {
                        // Delete unchecks all, [Shift + Delete] checks all.
                        for (int i = 0; i < Items.Count; i++)
                        {
                            SetItemChecked(i, e.Shift);
                        }
                        e.Handled = true;
                    }
                    // If no Enter or Esc keys presses, let the base class handle it.
                    base.OnKeyDown(e);
                }

                /// <summary>
                /// Raises the <see cref="E:System.Windows.Forms.Control.MouseMove" /> event.
                /// </summary>
                /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the event data.</param>
                protected override void OnMouseMove(MouseEventArgs e)
                {
                    base.OnMouseMove(e);
                    int index = IndexFromPoint(e.Location);
                    if ((index >= 0) && (index != _curSelIndex))
                    {
                        _curSelIndex = index;
                        SetSelected(index, true);
                    }
                }

            }

            private CheckedComboBox _ccbParent;

            // Keeps track of whether checked item(s) changed, hence the value of the CheckedComboBox as a whole changed.
            // This is simply done via maintaining the old string-representation of the value(s) and the new one and comparing them!
            private string _oldStrValue = "";
            public bool ValueChanged
            {
                get
                {
                    string newStrValue = _ccbParent.Text;
                    if ((this._oldStrValue.Length > 0) && (newStrValue.Length > 0))
                    {
                        return (this._oldStrValue.CompareTo(newStrValue) != 0);
                    }
                    else
                    {
                        return (this._oldStrValue.Length != newStrValue.Length);
                    }
                }
            }

            /// <summary>
            /// Array holding the checked states of the items. This will be used to reverse any changes if user cancels selection.
            /// </summary>
            private bool[] _checkedStateArr;

            /// <summary>
            /// Whether the dropdown is closed.
            /// </summary>
            private bool _dropdownClosed = true;

            /// <summary>
            /// The checkedlistbox to display in the dropdown.
            /// </summary>
            public CustomCheckedListBox List { get; set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="CheckboxDropdown"/> class.
            /// </summary>
            /// <param name="ccbParent">The CCB parent.</param>
            public CheckboxDropdown(CheckedComboBox ccbParent)
            {
                this._ccbParent = ccbParent;
                InitializeComponent();
                this.ShowInTaskbar = false;
                // Add a handler to notify our parent of ItemCheck events.
                this.List.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.List_ItemCheck);
            }

            /// <summary>
            /// Create a CustomCheckedListBox which fills up the entire form area.
            /// </summary>
            private void InitializeComponent()
            {
                this.List = new CustomCheckedListBox();
                this.SuspendLayout();
                // 
                // cclb
                // 
                this.List.BorderStyle = System.Windows.Forms.BorderStyle.None;
                this.List.Dock = System.Windows.Forms.DockStyle.Fill;
                this.List.FormattingEnabled = true;
                this.List.Location = new System.Drawing.Point(0, 0);
                this.List.Name = "cclb";
                this.List.Size = new System.Drawing.Size(47, 15);
                this.List.TabIndex = 0;
                // 
                // Dropdown
                // 
                this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                this.BackColor = System.Drawing.SystemColors.Menu;
                this.ClientSize = new System.Drawing.Size(47, 16);
                this.ControlBox = false;
                this.Controls.Add(this.List);
                this.ForeColor = System.Drawing.SystemColors.ControlText;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
                this.MinimizeBox = false;
                this.Name = "ccbParent";
                this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
                this.ResumeLayout(false);
            }

            /// <summary>
            /// Gets the checked items formatted to a string.
            /// </summary>
            public string GetCheckedItemsStringValue()
            {
                StringBuilder sb = new StringBuilder("");
                for (int i = 0; i < List.CheckedItems.Count; i++)
                {
                    sb.Append(List.GetItemText(List.CheckedItems[i])).Append(_ccbParent.ValueSeparator);
                }
                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - _ccbParent.ValueSeparator.Length, _ccbParent.ValueSeparator.Length);
                }
                return sb.ToString();
            }

            /// <summary>
            /// Closes the dropdown portion and enacts any changes according to the specified boolean parameter.
            /// <para/> NOTE: even though the caller might ask for changes to be enacted, this doesn't necessarily mean
            ///         that any changes have occurred as such.
            /// <para/> Caller should check the ValueChanged property of the
            ///         CheckedComboBox (after the dropdown has closed) to determine any actual value changes.
            /// </summary>
            /// <param name="enactChanges"></param>
            public void CloseDropdown(bool enactChanges)
            {
                if (_dropdownClosed)
                {
                    return;
                }

                // Perform the actual selection and display of checked items.
                if (enactChanges)
                {
                    // Set the text portion equal to the string comprising all checked items (if any, otherwise empty!).
                    _ccbParent.UpdateText();

                }
                else
                {
                    // Caller cancelled selection - need to restore the checked items to their original state.
                    for (int i = 0; i < List.Items.Count; i++)
                    {
                        List.SetItemChecked(i, _checkedStateArr[i]);
                    }
                }
                // From now on the dropdown is considered closed. We set the flag here to prevent OnDeactivate() calling
                // this method once again after hiding this window.
                _dropdownClosed = true;
                // Set the focus to our parent CheckedComboBox and hide the dropdown check list.
                _ccbParent.Focus();
                this.Hide();
                // Notify CheckedComboBox that its dropdown is closed. (NOTE: it does not matter which parameters we pass to
                // OnDropDownClosed() as long as the argument is CCBoxEventArgs so that the method knows the notification has
                // come from our code and not from the framework).
                _ccbParent.OnDropDownClosed(new CheckboxDropdownEventArgs(null, false));
            }

            /// <summary>
            /// Raises the <see cref="E:System.Windows.Forms.Form.Activated" /> event.
            /// </summary>
            /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
            protected override void OnActivated(EventArgs e)
            {
                base.OnActivated(e);
                _dropdownClosed = false;
                // Assign the old string value to compare with the new value for any changes.
                _oldStrValue = _ccbParent.Text;
                // Make a copy of the checked state of each item, in cace caller cancels selection.
                _checkedStateArr = new bool[List.Items.Count];
                for (int i = 0; i < List.Items.Count; i++)
                {
                    _checkedStateArr[i] = List.GetItemChecked(i);
                }
            }

            /// <summary>
            /// Raises the <see cref="E:System.Windows.Forms.Form.Deactivate" /> event.
            /// </summary>
            /// <param name="e">The <see cref="T:System.EventArgs" /> that contains the event data.</param>
            protected override void OnDeactivate(EventArgs e)
            {
                base.OnDeactivate(e);
                CheckboxDropdownEventArgs ce = e as CheckboxDropdownEventArgs;
                if (ce != null)
                {
                    CloseDropdown(ce.AssignValues);

                }
                else
                {
                    // If not custom event arguments passed, means that this method was called from the
                    // framework. We assume that the checked values should be registered regardless.
                    CloseDropdown(true);
                }
            }

            /// <summary>
            /// Handles the ItemCheck event of the List control.
            /// </summary>
            /// <param name="sender">The source of the event.</param>
            /// <param name="e">The <see cref="ItemCheckEventArgs"/> instance containing the event data.</param>
            private void List_ItemCheck(object sender, ItemCheckEventArgs e) => this._ccbParent.ItemCheck?.Invoke(sender, e);

        }

        /// <summary>
        /// A form-derived object representing the drop-down list of the checked combo box.
        /// </summary>
        public CheckboxDropdown Popup { get; }

        /// <summary>
        /// The valueSeparator character(s) between the ticked elements as they appear in the text portion of the CheckedComboBox.
        /// </summary>
        public string ValueSeparator { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the check boxes should be toggled when an item is selected.
        /// </summary>
        [DefaultValue(false), Description("Whether the check boxes should be toggled when an item is selected.")]
        public bool CheckOnClick
        {
            get => this.Popup.List.CheckOnClick;
            set => this.Popup.List.CheckOnClick = value;
        }

        /// <summary>
        /// Gets or sets a string that specifies a property of the objects contained in the list box whose contents you want to display.
        /// </summary>
        [Description("A property of the objects contained in the list box whose contents you want to display.")]
        public new string DisplayMember
        {
            get => this.Popup.List.DisplayMember;
            set => this.Popup.List.DisplayMember = value;
        }

        /// <summary>
        /// Collection of items in the ListBox.
        /// </summary>
        public new CheckedListBox.ObjectCollection Items => this.Popup.List.Items;

        /// <summary>
        /// Collection of checked items in the ListBox.
        /// </summary>
        public CheckedListBox.CheckedItemCollection CheckedItems => this.Popup.List.CheckedItems;

        /// <summary>
        /// Collection of checked indexes in the ListBox.
        /// </summary>
        public CheckedListBox.CheckedIndexCollection CheckedIndices => this.Popup.List.CheckedIndices;

        /// <summary>
        /// Whether the values of any checkboxes have changed.
        /// </summary>
        public bool HasValueChanged => this.Popup.ValueChanged;

        /// <summary>
        /// Event handler for when an item check state changes.
        /// </summary>
        public event ItemCheckEventHandler ItemCheck;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckedComboBox"/> class.
        /// </summary>
        public CheckedComboBox() : base()
        {
            this.DrawMode = DrawMode.Normal;
            // Default value separator.
            this.ValueSeparator = ", ";
            // This prevents the actual ComboBox dropdown to show, although it's not strickly-speaking necessary.
            // But including this remove a slight flickering just before our dropdown appears (which is caused by
            // the empty-dropdown list of the ComboBox which is displayed for fractions of a second).
            this.DropDownHeight = 1;
            this.DropDownStyle = ComboBoxStyle.DropDownList;
            this.Popup = new CheckboxDropdown(this);
            // CheckOnClick style for the dropdown (NOTE: must be set after dropdown is created).
            this.CheckOnClick = true;
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ComboBox.DropDown" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnDropDown(EventArgs e)
        {
            base.OnDropDown(e);

            if (!Popup.Visible)
            {
                var rect = RectangleToScreen(this.ClientRectangle);
                Popup.Location = new System.Drawing.Point(rect.X, rect.Y + this.Size.Height);
                int count = Popup.List.Items.Count;
                if (count > this.MaxDropDownItems)
                {
                    count = this.MaxDropDownItems;
                }
                else if (count == 0)
                {
                    count = 1;
                }
                Popup.Size = new System.Drawing.Size(this.Size.Width, (Popup.List.ItemHeight) * count + 2);
                Popup.Show(this);
                //BUG: CheckedComboBox - list is not focussed when opened
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ComboBox.DropDownClosed" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnDropDownClosed(EventArgs e)
        {
            // Call the handlers for this event only if the call comes from our code - NOT the framework's!
            // NOTE: that is because the events were being fired in a wrong order, due to the actual dropdown list
            //       of the ComboBox which lies underneath our dropdown and gets involved every time.
            if (e is CheckboxDropdown.CheckboxDropdownEventArgs)
            {
                base.OnDropDownClosed(e);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.KeyDown" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event data.</param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                // Signal that the dropdown is "down". This is required so that the behaviour of the dropdown is the same
                // when it is a result of user pressing the Down_Arrow (which we handle and the framework wouldn't know that
                // the list portion is down unless we tell it so).
                // NOTE: all that so the DropDownClosed event fires correctly!                
                OnDropDown(null);
            }
            // Make sure that certain keys or combinations are not blocked.
            e.Handled = !e.Alt && !(e.KeyCode == Keys.Tab) &&
                !((e.KeyCode == Keys.Left) || (e.KeyCode == Keys.Right) || (e.KeyCode == Keys.Home) || (e.KeyCode == Keys.End));

            base.OnKeyDown(e);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.KeyPress" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.KeyPressEventArgs" /> that contains the event data.</param>
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            e.Handled = true;
            base.OnKeyPress(e);
        }

        /// <summary>
        /// Gets whether an item is checked.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">index - value out of range</exception>
        public bool GetItemChecked(int index)
        {
            if (index < 0 || index > Items.Count)
            {
                throw new ArgumentOutOfRangeException("index", "value out of range");
            }
            else
            {
                return Popup.List.GetItemChecked(index);
            }
        }

        /// <summary>
        /// Sets whether an item is checked.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="isChecked">Whether the item should be checked.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">index - value out of range</exception>
        public void SetItemChecked(int index, bool isChecked)
        {
            if (index < 0 || index > Items.Count)
            {
                throw new ArgumentOutOfRangeException("index", "value out of range");
            }
            else
            {
                Popup.List.SetItemChecked(index, isChecked);
                // Need to update the Text.
                UpdateText();
            }
        }

        /// <summary>
        /// Gets the item checked state.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">index - value out of range</exception>
        public CheckState GetItemCheckState(int index)
        {
            if (index < 0 || index > Items.Count)
            {
                throw new ArgumentOutOfRangeException("index", "value out of range");
            }
            else
            {
                return Popup.List.GetItemCheckState(index);
            }
        }

        /// <summary>
        /// Sets the item checked state.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="state">The state to set.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">index - value out of range</exception>
        public void SetItemCheckState(int index, CheckState state)
        {
            if (index < 0 || index > Items.Count)
            {
                throw new ArgumentOutOfRangeException("index", "value out of range");
            }
            else
            {
                Popup.List.SetItemCheckState(index, state);
                // Need to update the Text.
                UpdateText();
            }
        }

        /// <summary>
        /// Updates the text display.
        /// </summary>
        /// <param name="text"></param>
        internal void UpdateText()
        {
            //Determine text
            string text = null;
            if (this.DisplayFunction != null)
                text = this.DisplayFunction(this.CheckedItems.Cast<object>());
            if (text == null)
                text = this.FormatDisplayString(this.GetDisplayString());

            if (this.DropDownStyle == ComboBoxStyle.DropDownList)
            {
                //Apply text (has to be in list in order to be recognized when using dropdownlist style)
                base.Items.Clear();
                base.Items.Add(text);
                this.SelectedIndex = 0;
            }
            else
            {
                //Simply set text
                this.SelectedIndex = -1;
                this.Text = text;
            }
        }

        /// <summary>
        /// A function to determine the display text given the list of checked items. May return null as a fallback.
        /// <para/>Does not format item counts on its own (no calls to this.FormatDisplayString) unless using null value.
        /// </summary>
        [Browsable(false)]
        public Func<IEnumerable<object>, string> DisplayFunction { get; set; }

        /// <summary>
        /// Returns the default display string for the currently checked items.
        /// </summary>
        public string GetDisplayString() => this.Popup.GetCheckedItemsStringValue();

        /// <summary>
        /// Formats the given display string using the set format.
        /// </summary>
        /// <param name="text">The text to format.</param>
        public string FormatDisplayString(string text) => this.FormatDisplayString(text, this.DisplayCount);

        /// <summary>
        /// Formats the given display string using the given format.
        /// </summary>
        /// <param name="text">The text to format.</param>
        /// <param name="mode">The mode to use.</param>
        public string FormatDisplayString(string text, DisplayCountMode mode)
        {
            switch (mode)
            {
                case DisplayCountMode.DisplayChecked:
                    return $"({this.CheckedItems.Count}) {text}";
                case DisplayCountMode.DisplayTotal:
                    return $"({this.Items.Count}) {text}";
                case DisplayCountMode.Both:
                    return $"({this.CheckedItems.Count}/{this.Items.Count}) {text}";
                default:
                    return text;
            }
        }

        /// <summary>
        /// Gets or sets whether and how to display the amount of certain items in the list.
        /// </summary>
        [Description("Whether and how to display the amount of certain items in the list.")]
        public DisplayCountMode DisplayCount { get; set; } = DisplayCountMode.DisplayChecked;
    }

    /// <summary>
    /// Which counts should be displayed.
    /// </summary>
    public enum DisplayCountMode
    {
        /// <summary>
        /// Amount of checked items.
        /// </summary>
        DisplayChecked,
        /// <summary>
        /// Amount of total items.
        /// </summary>
        DisplayTotal,
        /// <summary>
        /// Both checked item count and total item count.
        /// </summary>
        Both,
        /// <summary>
        /// Do not show counts.
        /// </summary>
        NoCount
    }
}
