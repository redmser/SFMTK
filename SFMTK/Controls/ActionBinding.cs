﻿using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// Binding used for invoking an action when the control value is updated.
    /// </summary>
    public class ActionBinding : Binding
    {
        public ActionBinding(string propertyName, object dataSource, string dataMember, DataSourceUpdateMode dataSourceUpdateMode, BindingCompleteEventHandler action)
            : base(propertyName, dataSource, dataMember, true, dataSourceUpdateMode)
        {
            this.Handler = action;
            this.BindingComplete += this.Handler;
        }

        public BindingCompleteEventHandler Handler { get; set; }
    }
}
