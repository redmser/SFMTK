﻿using System.Collections;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace SFMTK.Controls
{
    public class EnhancedObjectListView : ObjectListView
    {
        public override ToolStripDropDown MakeColumnSelectMenu(ToolStripDropDown strip)
        {
            //Do not allow any empty-string menu entries, use whatever other string we can
            var menu = base.MakeColumnSelectMenu(strip);
            foreach (var item in ControlsHelper.RecurseItems(menu))
            {
                if (string.IsNullOrEmpty(item.Text) && item.Tag is OLVColumn col)
                    item.Text = col.ToolTipText ?? col.Name;
            }
            return menu;
        }
    }

    public class EnhancedTreeListView : TreeListView
    {
        /// <summary>
        /// Gets or sets whether toggling a header checkbox should not only update the top-level objects, but also all children.
        /// </summary>
        public bool HeaderCheckBoxUpdatesChildren { get; set; } = true;

        public override ToolStripDropDown MakeColumnSelectMenu(ToolStripDropDown strip)
        {
            //Do not allow any empty-string menu entries, use whatever other string we can
            var menu = base.MakeColumnSelectMenu(strip);
            foreach (var item in ControlsHelper.RecurseItems(menu))
            {
                if (string.IsNullOrEmpty(item.Text) && item.Tag is OLVColumn col)
                    item.Text = col.ToolTipText ?? col.Name;
            }
            return menu;
        }

        protected override void OnHeaderCheckBoxChanging(HeaderCheckBoxChangingEventArgs args)
        {
            base.OnHeaderCheckBoxChanging(args);

            if (args.Cancel || !this.HeaderCheckBoxUpdatesChildren)
                return;

            if (args.Column.HeaderCheckBoxUpdatesRowCheckBoxes)
            {
                //Update children's checkboxes too
                foreach (var child in Recurse())
                {
                    this.SetObjectCheckedness(child, args.NewCheckState);
                }
            }



            IEnumerable Recurse()
            {
                foreach (var toplevel in this.Objects)
                {
                    foreach (var child in RecurseChildren(toplevel))
                    {
                        yield return child;
                    }
                }
            }

            IEnumerable RecurseChildren(object parent)
            {
                foreach (var child in this.GetChildren(parent))
                {
                    yield return child;
                    foreach (var inner in RecurseChildren(child))
                    {
                        yield return inner;
                    }
                }
            }
        }
    }
}
