﻿using SFMTK.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// A control for filtering options at content layer.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.UserControl" />
    public partial class FilterControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FilterControl"/> class.
        /// </summary>
        public FilterControl()
        {
            //Compiler-generated
            InitializeComponent();

            //Add tooltip to textbox too
            this.toolTip1.SetToolTip(this.pathSearchTextBox.TextBox, this.toolTip1.GetToolTip(this.pathSearchTextBox));
            this.toolTip1.SetToolTip(this.filterSearchTextBox.TextBox, this.toolTip1.GetToolTip(this.filterSearchTextBox));

            //Events
            this.filterSearchTextBox.TextBox.TextChanged += this.PathTextChanged;
            this.pathSearchTextBox.TextBox.TextChanged += this.PathTextChanged;

            //TypeList
            //FIXME: FilterControl - only call FilterSettingChanged for the types list IF any checkbox did change value!
            this.typesCheckedComboBox.Items.AddRange(Contents.Content.ContentList.Select(c => c.GetType()).ToArray());
            this.typesCheckedComboBox.DisplayMember = "TypeName";
            this.typesCheckedComboBox.DisplayFunction = this.typesCheckedComboBox_DisplayFunction;

            //Filter Settings
            var settings = Settings.Default.DefaultFilterSettings;
            if (settings == null)
            {
                //Default filter settings: enable all types
                for (var i = 0; i < this.typesCheckedComboBox.Items.Count; i++)
                {
                    this.typesCheckedComboBox.SetItemChecked(i, true);
                }

                //Update display
                this.typesCheckedComboBox.UpdateText();

                //Update settings instance
                this.UpdateFilterSettings();
            }
            else
            {
                //Load stored filter settings
                this.FilterSettings = Settings.Default.DefaultFilterSettings;
            }
        }

        private void PathTextChanged(object sender, EventArgs e) => OnFilterSettingsChanged(EventArgs.Empty);

        /// <summary>
        /// Updates the filter settings properties based on the state of the control itself.
        /// </summary>
        private void UpdateFilterSettings()
        {
            if (this.FilterSettings == null)
                this.FilterSettings = new FilterControlSettings();

            this.FilterSettings.PathFilter = (this.Expanded ? this.pathSearchTextBox.Text : this.filterSearchTextBox.Text).ToUpperInvariant();
            this.FilterSettings.TypeFilter = this.typesCheckedComboBox.CheckedItems.Cast<Type>();
            this.FilterSettings.PathRegex = this.pathRegexCheckBox.Checked;

            //HACK: FilterControl - passing HasTypeFilter as a parameter. determine where the list of items comes from and compare based on that!
            this.FilterSettings.UpdateRequirements(this.typesCheckedComboBox.CheckedItems.Count != this.typesCheckedComboBox.Items.Count);
        }

        /// <summary>
        /// Gets or sets the settings of this FilterControl.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public FilterControlSettings FilterSettings
        {
            //UNTESTED: FilterControl - loading and saving filter settings, either between restarts, but also when closing and reopening the browser dialog
            //  -> what about pressing Cancel on the browser? this should probably reset to the old value!
            //  -> the basic CTRL+O content browser should NOT save between *restarts*! but maybe consider saving certain properties between reopening the dialog

            get => Settings.Default.DefaultFilterSettings;
            set
            {
                Settings.Default.DefaultFilterSettings = value;

                //Update controls
                this.pathSearchTextBox.Text = value.PathFilter;
                this.pathRegexCheckBox.Checked = value.PathRegex;
                foreach (var check in value.TypeFilter)
                {
                    var index = this.typesCheckedComboBox.Items.IndexOf(check);
                    if (index >= 0)
                        this.typesCheckedComboBox.SetItemChecked(index, true);
                }

                //Update display of checkbox list
                this.typesCheckedComboBox.UpdateText();
            }
        }

        /// <summary>
        /// String match function given the corresponding parameters.
        /// </summary>
        /// <param name="input">The input string to validate.</param>
        /// <param name="pattern">The pattern provided through a filter textbox. Has to be upper-case, since all comparisons here are case-insensitive.</param>
        /// <param name="regex">Whether to use Regexes for comparisons.</param>
        public static bool StringMatches(string input, string pattern, bool regex)
        {
            //Always match with no pattern or no input
            if (string.IsNullOrEmpty(pattern) || string.IsNullOrEmpty(input))
                return true;

            if (regex)
            {
                //Use regex to compare
                try
                {
                    return Regex.IsMatch(input, pattern, RegexOptions.IgnoreCase);
                }
                catch (ArgumentException)
                {
                    //Invalid syntax - do not match
                    //TODO: FilterControl - find a way to notify user of an invalid filtering value (such as by red highlighting on the textbox)
                    return false;
                }
            }
            else
            {
                //Simple string "contains" comparison
                return input.ToUpperInvariant().Contains(pattern);
            }
        }

        /// <summary>
        /// Used as a generic handler for any filter-setting-changing event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void FilterSettingChanged(object sender, EventArgs e) => OnFilterSettingsChanged(EventArgs.Empty);

        private string typesCheckedComboBox_DisplayFunction(IEnumerable<object> arg)
        {
            if (!arg.Any())
                return this.typesCheckedComboBox.FormatDisplayString("No types");

            var checkedtypes = arg.Cast<Type>();
            var alltypes = this.typesCheckedComboBox.Items.Cast<Type>();

            if (checkedtypes.ContentsEquals(alltypes))
            {
                //All types are selected
                return this.typesCheckedComboBox.FormatDisplayString("All types");
            }

            return null;
        }

        private void expandButton_Click(object sender, EventArgs e)
        {
            this._ignoreChanges = true;

            //Copy simple filter text to path text
            this.pathSearchTextBox.Text = this.filterSearchTextBox.Text;

            //Hide small table, show big table
            this.Expanded = true;

            this._ignoreChanges = false;
        }

        private void collapseButton_Click(object sender, EventArgs e)
        {
            this._ignoreChanges = true;

            //Copy path text to simple filter text
            this.filterSearchTextBox.Text = this.pathSearchTextBox.Text;

            //Hide big table, show small table
            this.Expanded = false;

            this._ignoreChanges = false;
        }

        /// <summary>
        /// Gets the height of the collapsed filter control.
        /// </summary>
        public const int CollapsedHeight = 24;

        /// <summary>
        /// Gets the height of the expanded filter control.
        /// </summary>
        public const int ExpandedHeight = 48;

        /// <summary>
        /// Gets the preferred height of this control, determined from its current state.
        /// </summary>
        public int PreferredHeight => this.Expanded ? ExpandedHeight : CollapsedHeight;

        /// <summary>
        /// Gets or sets whether this filter view is expanded, showing advanced options.
        /// </summary>
        public bool Expanded
        {
            get => this.expandedTableLayoutPanel.Visible;
            set
            {
                if (this.Expanded == value)
                    return;

                this.expandedTableLayoutPanel.Visible = value;
                this.collapsedTableLayoutPanel.Visible = !value;

                OnExpandedChanged(EventArgs.Empty);
            }
        }

        private bool _ignoreChanges = false;

        /// <summary>
        /// Occurs when expanded or collapsed.
        /// </summary>
        public event EventHandler ExpandedChanged;

        /// <summary>
        /// Occurs when the filter settings have been changed and the filtering process should start again.
        /// </summary>
        public event EventHandler FilterSettingsChanged;

        /// <summary>
        /// Raises the <see cref="E:ExpandedChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnExpandedChanged(EventArgs e) => this.ExpandedChanged?.Invoke(this, e);

        /// <summary>
        /// Raises the <see cref="E:FilterSettingsChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnFilterSettingsChanged(EventArgs e)
        {
            if (this._ignoreChanges)
                return;

            this.UpdateFilterSettings();
            this.FilterSettingsChanged?.Invoke(this, e);
        }
    }

    /// <summary>
    /// The settings stored in a filter control.
    /// </summary>
    [Serializable]
    public class FilterControlSettings
    {
        /// <summary>
        /// Gets or sets the current filter text for paths.
        /// </summary>
        public string PathFilter { get; set; }

        /// <summary>
        /// Gets or sets the list of content types to be shown.
        /// </summary>
        public IEnumerable<Type> TypeFilter { get; set; } = new List<Type>();

        /// <summary>
        /// Gets or sets whether the path string uses regex.
        /// </summary>
        public bool PathRegex { get; set; }

        /// <summary>
        /// Gets whether filtering is currently to be done.
        /// </summary>
        public bool HasFiltering => this._hasFiltering;
        [NonSerialized]
        private bool _hasFiltering;

        /// <summary>
        /// Gets whether a path filter is present.
        /// </summary>
        public bool HasPathFilter => this._hasPathFilter;
        [NonSerialized]
        private bool _hasPathFilter;

        /// <summary>
        /// Gets whether a type filter is present.
        /// </summary>
        public bool HasTypeFilter => this._hasTypeFilter;
        [NonSerialized]
        private bool _hasTypeFilter;

        /// <summary>
        /// Updates the properties which determine whether certain types of filtering are needed to be done.
        /// </summary>
        public void UpdateRequirements(bool hasTypeFilter)
        {
            this._hasPathFilter = !string.IsNullOrEmpty(this.PathFilter);
            this._hasTypeFilter = hasTypeFilter;
            this._hasFiltering = this.HasPathFilter || this.HasTypeFilter;
        }
    }
}
