﻿using System.Windows.Forms;
using Ookii.Dialogs.Wpf;

namespace SFMTK
{
    /// <summary>
    /// A TaskDialogButton allowing to fallback to a DialogResult when not supported.
    /// </summary>
    /// <seealso cref="TaskDialogButton" />
    public class FallbackDialogButton : TaskDialogButton
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FallbackDialogButton" /> class.
        /// </summary>
        /// <param name="type">The result when clicking on the button.</param>
        /// <param name="executeOnRemember">If set to <c>true</c>, clicks on the button when the remember checkbox was checked and this button was to be remembered.</param>
        public FallbackDialogButton(DialogResult type, bool executeOnRemember = true)
        {
            this.ButtonType = FromResult(type);
            this.Result = type;
            this.ExecuteOnRemember = executeOnRemember;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FallbackDialogButton" /> class.
        /// </summary>
        /// <param name="text">The custom text to display on the button.</param>
        /// <param name="result">The result when clicking the button.</param>
        /// <param name="executeOnRemember">If set to <c>true</c>, clicks on the button when the remember checkbox was checked and this button was to be remembered.</param>
        public FallbackDialogButton(string text, DialogResult result, bool executeOnRemember = true)
        {
            this.ButtonType = ButtonType.Custom;
            this.Text = text;
            this.Result = result;
            this.ExecuteOnRemember = executeOnRemember;
        }

        /// <summary>
        /// Gets or sets the result to return.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public DialogResult Result { get; set; }

        /// <summary>
        /// If true, "clicks" on the button if the decision is to be remembered. If false, the dialog is simply shown again, with the checkbox checked already.
        /// </summary>
        public bool ExecuteOnRemember { get; }

        /// <summary>
        /// Gets or sets whether this button was pressed in a fallback dialog, instead of a task dialog.
        /// </summary>
        public bool FallbackDialog { get; set; }

        /// <summary>
        /// Returns the corresponding DialogResult from the specified ButtonType. Returns None if unknown value.
        /// </summary>
        /// <param name="type"></param>
        internal static DialogResult FromButtonType(ButtonType type)
        {
            switch (type)
            {
                case ButtonType.Close:
                case ButtonType.Cancel:
                    return DialogResult.Cancel;
                case ButtonType.Ok:
                    return DialogResult.OK;
                case ButtonType.Yes:
                    return DialogResult.Yes;
                case ButtonType.No:
                    return DialogResult.No;
                case ButtonType.Retry:
                    return DialogResult.Retry;
                default:
                    return DialogResult.None;
            }
        }

        /// <summary>
        /// Returns the corresponding ButtonType from the specified DialogResult. Returns Custom if unknown value.
        /// </summary>
        /// <param name="type"></param>
        internal static ButtonType FromResult(DialogResult type)
        {
            switch (type)
            {
                case DialogResult.Abort:
                case DialogResult.Cancel:
                case DialogResult.Ignore:
                    return ButtonType.Cancel;
                case DialogResult.OK:
                    return ButtonType.Ok;
                case DialogResult.No:
                    return ButtonType.No;
                case DialogResult.Yes:
                    return ButtonType.Yes;
                case DialogResult.Retry:
                    return ButtonType.Retry;
                default:
                    return ButtonType.Custom;
            }
        }
    }
}
