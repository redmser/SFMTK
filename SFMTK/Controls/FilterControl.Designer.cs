﻿namespace SFMTK.Controls
{
    partial class FilterControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.collapsedTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.expandButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.filterSearchTextBox = new SFMTK.Controls.SearchTextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pathSearchTextBox = new SFMTK.Controls.SearchTextBox();
            this.pathRegexCheckBox = new System.Windows.Forms.CheckBox();
            this.typesCheckedComboBox = new SFMTK.Controls.CheckedComboBox();
            this.collapseButton = new System.Windows.Forms.Button();
            this.expandedTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.collapsedTableLayoutPanel.SuspendLayout();
            this.expandedTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // collapsedTableLayoutPanel
            // 
            this.collapsedTableLayoutPanel.ColumnCount = 3;
            this.collapsedTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.collapsedTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.collapsedTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.collapsedTableLayoutPanel.Controls.Add(this.expandButton, 2, 0);
            this.collapsedTableLayoutPanel.Controls.Add(this.label1, 0, 0);
            this.collapsedTableLayoutPanel.Controls.Add(this.filterSearchTextBox, 1, 0);
            this.collapsedTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.collapsedTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.collapsedTableLayoutPanel.Name = "collapsedTableLayoutPanel";
            this.collapsedTableLayoutPanel.RowCount = 2;
            this.collapsedTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.collapsedTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.collapsedTableLayoutPanel.Size = new System.Drawing.Size(202, 172);
            this.collapsedTableLayoutPanel.TabIndex = 0;
            // 
            // expandButton
            // 
            this.expandButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.expandButton.Image = global::SFMTK.Properties.Resources.resultset_up;
            this.expandButton.Location = new System.Drawing.Point(178, 0);
            this.expandButton.Margin = new System.Windows.Forms.Padding(0);
            this.expandButton.Name = "expandButton";
            this.expandButton.Size = new System.Drawing.Size(24, 24);
            this.expandButton.TabIndex = 3;
            this.toolTip1.SetToolTip(this.expandButton, "Show advanced search options");
            this.expandButton.UseVisualStyleBackColor = true;
            this.expandButton.Click += new System.EventHandler(this.expandButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Filter:";
            // 
            // filterSearchTextBox
            // 
            this.filterSearchTextBox.ClearImage = null;
            this.filterSearchTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filterSearchTextBox.Location = new System.Drawing.Point(39, 1);
            this.filterSearchTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.filterSearchTextBox.Name = "filterSearchTextBox";
            this.filterSearchTextBox.SearchImage = null;
            this.filterSearchTextBox.Size = new System.Drawing.Size(138, 22);
            this.filterSearchTextBox.TabIndex = 2;
            this.toolTip1.SetToolTip(this.filterSearchTextBox, "What text to search for in a content\'s path");
            // 
            // pathSearchTextBox
            // 
            this.pathSearchTextBox.ClearImage = null;
            this.pathSearchTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pathSearchTextBox.Location = new System.Drawing.Point(41, 1);
            this.pathSearchTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.pathSearchTextBox.Name = "pathSearchTextBox";
            this.pathSearchTextBox.SearchImage = null;
            this.pathSearchTextBox.Size = new System.Drawing.Size(136, 22);
            this.pathSearchTextBox.TabIndex = 1;
            this.toolTip1.SetToolTip(this.pathSearchTextBox, "What text to search for in a content\'s path");
            // 
            // pathRegexCheckBox
            // 
            this.pathRegexCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.pathRegexCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pathRegexCheckBox.Image = global::SFMTK.Properties.Resources.tag;
            this.pathRegexCheckBox.Location = new System.Drawing.Point(178, 0);
            this.pathRegexCheckBox.Margin = new System.Windows.Forms.Padding(0);
            this.pathRegexCheckBox.Name = "pathRegexCheckBox";
            this.pathRegexCheckBox.Size = new System.Drawing.Size(24, 24);
            this.pathRegexCheckBox.TabIndex = 2;
            this.toolTip1.SetToolTip(this.pathRegexCheckBox, "Use Regex (Regular Expressions) for matching the path");
            this.pathRegexCheckBox.UseVisualStyleBackColor = true;
            this.pathRegexCheckBox.CheckedChanged += new System.EventHandler(this.FilterSettingChanged);
            // 
            // typesCheckedComboBox
            // 
            this.typesCheckedComboBox.CheckOnClick = true;
            this.typesCheckedComboBox.DisplayCount = SFMTK.Controls.DisplayCountMode.Both;
            this.typesCheckedComboBox.DisplayFunction = null;
            this.typesCheckedComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.typesCheckedComboBox.DropDownHeight = 1;
            this.typesCheckedComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typesCheckedComboBox.FormattingEnabled = true;
            this.typesCheckedComboBox.IntegralHeight = false;
            this.typesCheckedComboBox.Location = new System.Drawing.Point(41, 25);
            this.typesCheckedComboBox.Margin = new System.Windows.Forms.Padding(1);
            this.typesCheckedComboBox.Name = "typesCheckedComboBox";
            this.typesCheckedComboBox.Size = new System.Drawing.Size(136, 21);
            this.typesCheckedComboBox.TabIndex = 4;
            this.toolTip1.SetToolTip(this.typesCheckedComboBox, "Which content types are shown");
            this.typesCheckedComboBox.ValueSeparator = ", ";
            this.typesCheckedComboBox.DropDownClosed += new System.EventHandler(this.FilterSettingChanged);
            // 
            // collapseButton
            // 
            this.collapseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.collapseButton.Image = global::SFMTK.Properties.Resources.resultset_down;
            this.collapseButton.Location = new System.Drawing.Point(178, 24);
            this.collapseButton.Margin = new System.Windows.Forms.Padding(0);
            this.collapseButton.Name = "collapseButton";
            this.collapseButton.Size = new System.Drawing.Size(24, 148);
            this.collapseButton.TabIndex = 5;
            this.toolTip1.SetToolTip(this.collapseButton, "Hide advanced search options");
            this.collapseButton.UseVisualStyleBackColor = true;
            this.collapseButton.Click += new System.EventHandler(this.collapseButton_Click);
            // 
            // expandedTableLayoutPanel
            // 
            this.expandedTableLayoutPanel.ColumnCount = 3;
            this.expandedTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.expandedTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.expandedTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.expandedTableLayoutPanel.Controls.Add(this.label2, 0, 0);
            this.expandedTableLayoutPanel.Controls.Add(this.pathSearchTextBox, 1, 0);
            this.expandedTableLayoutPanel.Controls.Add(this.pathRegexCheckBox, 2, 0);
            this.expandedTableLayoutPanel.Controls.Add(this.label3, 0, 1);
            this.expandedTableLayoutPanel.Controls.Add(this.typesCheckedComboBox, 1, 1);
            this.expandedTableLayoutPanel.Controls.Add(this.collapseButton, 2, 1);
            this.expandedTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.expandedTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.expandedTableLayoutPanel.Name = "expandedTableLayoutPanel";
            this.expandedTableLayoutPanel.RowCount = 2;
            this.expandedTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.expandedTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.expandedTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.expandedTableLayoutPanel.Size = new System.Drawing.Size(202, 172);
            this.expandedTableLayoutPanel.TabIndex = 3;
            this.expandedTableLayoutPanel.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "&Path:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 29);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "&Type:";
            // 
            // FilterControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.expandedTableLayoutPanel);
            this.Controls.Add(this.collapsedTableLayoutPanel);
            this.Name = "FilterControl";
            this.Size = new System.Drawing.Size(202, 172);
            this.collapsedTableLayoutPanel.ResumeLayout(false);
            this.collapsedTableLayoutPanel.PerformLayout();
            this.expandedTableLayoutPanel.ResumeLayout(false);
            this.expandedTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel collapsedTableLayoutPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolTip toolTip1;
        private SearchTextBox filterSearchTextBox;
        private System.Windows.Forms.TableLayoutPanel expandedTableLayoutPanel;
        private System.Windows.Forms.Label label2;
        private SearchTextBox pathSearchTextBox;
        private System.Windows.Forms.CheckBox pathRegexCheckBox;
        private System.Windows.Forms.Label label3;
        private CheckedComboBox typesCheckedComboBox;
        private System.Windows.Forms.Button collapseButton;
        private System.Windows.Forms.Button expandButton;
    }
}
