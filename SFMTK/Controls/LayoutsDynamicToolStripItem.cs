﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Commands;

namespace SFMTK.Controls
{
    /// <summary>
    /// <see cref="DynamicToolStripItem"/> populating the list of window layouts.
    /// </summary>
    public class LayoutsDynamicToolStripItem : DynamicToolStripItem
    {
        //TODO: LayoutsDynamicToolStripItem - disable autoreload, only refresh whenever the list of layouts changed

        /// <summary>
        /// Returns the list of <see cref="ToolStripItem" />s that this item would be replaced
        /// with if it were to be displayed as an entry somewhere.
        /// </summary>
        public override IEnumerable<ToolStripItem> GetDynamicItems()
        {
            if (DockLayout.Layouts.Count == 0)
            {
                //No layouts
                yield return new ToolStripMenuItem("No layouts") { Enabled = false };
            }
            else
            {
                //List of layouts
                for (var i = 0; i < DockLayout.Layouts.Count; i++)
                {
                    //Add layout
                    yield return Program.MainCM.CreateToolStripMenuItem(CommandManager.Commands.OfType<ApplyLayoutCommand>().First(c => c.Index == i));
                }
            }
        }
    }
}
