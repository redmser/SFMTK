﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using SFMTK.Data;
using SFMTK.Contents;

namespace SFMTK.Controls
{
    /// <summary>
    /// A user control for browsing for local SFM content, similar to SFM's model browser. Only works with correctly set-up SFM paths.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.UserControl" />
    public partial class ContentBrowser : UserControl, IThemeable
    {
        //BUG: ContentBrowser - if the children are exposed while still loading, they will only be "recalculated" after a full rebuild - how to avoid this?
        //  -> find a way to trigger a periodic "recheck children property" for each collapsed node
        //TODO: ContentBrowser - since we are showing all files (mod-duplicates included), show somewhere (possible dependency graph) that there are
        //  content instances with the same path in other mods!
        //TODO: ContentBrowser - type filter dropdown checkboxes should include "unknown" files (any non-content files)
        //TODO: ContentBrowser - how should Full Path textbox be handled in the case of multiselect?
        //TODO: ContentBrowser - add button to toggle between treeview and a flat view (alas sfm)
        //TODO: ContentBrowser - add button to toggle between root elements of treeview being mods or to merge all directories (alas sfm)
        //  -> is there a way to accomplish this either using simply properties for formatting, or converting the data, instead of having to re-populate the node tree??
        //TODO: ContentBrowser - workflow should allow to, once opening browser, enter LOCAL path to content (into filter box), press enter and the content is opened
        //  -> should pressing CTRL+O a second time open the native file browser?
        //TODO: ContentBrowser - add advanced interactions to mod filter list (possibly for all checkedcomboboxes):
        //  - Doubleclick to select a single mod (tell user about this since it's different)
        //  - Have quick-buttons to select all mods/all enabled mods/no mods
        //  - Possibly an option to use a normal combobox instead, alas regular modelbrowser
        //  -> if we used OLV, we could work with context menus as well, or even treelistviews for nesting etc
        //NYI: ContentBrowser - cache the node tree, and do a "silent add/remove of content" (see above) when opening (or option to only do so automatically on restart, or never)
        //  -> ContentTree class is already in place for this, simply update mod crawler for the updating scenario (see TODO on ModCrawler header)
        //TODO: ContentBrowser - include a toggle to switch to "Browse Sessions" mode - uses directories from SFM.DetectSessionDirectories()
        //  -> similarly, needs a mode for browsing ANY file on the computer, not just in SFM's folder context (see workflow TODO above, with secondary CTRL+O)
        //NYI: ContentBrowser - if dialog is opened for saving/loading, find all fitting importers/exporters
        //  and display their instancesettings (of type Control) + tooltiptext

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentBrowser"/> class.
        /// </summary>
        public ContentBrowser()
        {
            //Compiler-generated
            InitializeComponent();

            //Setup async
            this._crawlProgress = new Progress<int>(this.UpdateProgress);

            //Setup the timer
            //FIXME: ContentBrowser - when regex search is enabled for any filters, auto-updating timers shouldn't really be a thing (or at least be pretty slow instead)
            this._filterDelayTimer = new System.Timers.Timer(250);
            this._filterDelayTimer.AutoReset = false;
            this._filterDelayTimer.Elapsed += this.FilterControl_FilterSettingsUpdate;

            //Filter settings
            this.filterControl.FilterSettingsChanged += this.FilterControl_FilterSettingsChanged;

            //Setup the treeview
            this.contentTreeListView.CanExpandGetter = (o) => ((ContentBrowserNode)o).CanExpand;
            this.contentTreeListView.ChildrenGetter = (o) => ((ContentBrowserNode)o).Children;
            //FIXME: ContentBrowser - Is using imagelists faster/uses less RAM? should do so regardless, as the rendering shouldnt be determined by the node!
            this.nameContentColumn.ImageGetter = (o) => ((ContentBrowserNode)o).GetImage();

            //Content should be sorted by three properties: load order, then node type (directories top), then node name
            //  Since OLV only includes a primary and secondary sort column (of which the latter does not even seem to work)
            //  I have decided to take advantage of string sorting to come up with this mess. The numbers have to be converted
            //  to letters since numeric sorting uses character position instead of value for ordering.

            //UNTESTED: ContentBrowser - since this is implied sorting, how would the user get the default sorting back once he decides to manually re-sort by another column?
            this.contentTreeListView.PrimarySortColumn = new BrightIdeasSoftware.OLVColumn
            { AspectGetter = (o) => { var n = (ContentBrowserNode)o; return $"{StringHelper.IntToLetters(n.ModIndex, 2)}{n.NodeType}{n.Name}"; } };

            if (SFM.ValidPaths)
            {
                var task = this.SetupBrowser();
            }
        }

        private void ContentBrowser_Load(object sender, EventArgs e)
        {
            //After columns have settled, fill remaining space
            this.nameContentColumn.FillFreeSpace();
        }

        private async Task SetupBrowser()
        {
            //Clear tree view
            this.contentTreeListView.ClearObjects();

            //Populate mods list
            this.modComboBox.Items.Clear();
            this.modComboBox.Items.AddRange(SFM.GameInfo.Mods.ToArray());
            this.modComboBox.DisplayFunction = this.ModComboBox_DisplayFunction;

            //Setup checkboxes from config
            if (true)
            {
                //Generate mod filter from current mod setup
                for (var i = 0; i < this.modComboBox.Items.Count; i++)
                {
                    this.modComboBox.SetItemChecked(i, ((Mod)this.modComboBox.Items[i]).Enabled);
                }
            }
            else
            {
                //NYI: ContentBrowser - load mod filter from settings
            }
            this.modComboBox.UpdateText();

            //Start scanning content
            await this.StartScan();
        }

        private Task _crawlTask;
        private CancellationTokenSource _crawlCancel = new CancellationTokenSource();
        private readonly Progress<int> _crawlProgress;

        /// <summary>
        /// (Re)starts scanning through the mod folders for content. Should only be called if a valid SFM path is given.
        /// </summary>
        public async Task StartScan()
        {
            //Cancel any ongoing crawl
            await this.CancelScan();

            //TODO: ContentBrowser - while scanning for content, change the rescan button to a split menu button (default option being Cancel!)

            //Crawl mod directories
            ((IProgress<int>)this._crawlProgress).Report(0);
            this._crawlCancel = new CancellationTokenSource();
            this._crawlTask = Task.Factory.StartNew(() => this.CrawlModDirectories(this._crawlCancel.Token, this._crawlProgress));

            //Show a percentage barÉ
            this.mainTableLayoutPanel.RowStyles[2].Height = 20;

            await this._crawlTask;

            //Remove percentage barÉ
            this.mainTableLayoutPanel.RowStyles[2].Height = 0;
        }

        /// <summary>
        /// Cancels any ongoing mod directory scan.
        /// </summary>
        public async Task CancelScan()
        {
            if (this._crawlTask != null && !this._crawlTask.IsCompleted)
            {
                this._crawlCancel.Cancel();
                await this._crawlTask;
            }
        }

        private async void FilterControl_FilterSettingsUpdate(object sender, System.Timers.ElapsedEventArgs e)
        {
            //TODO: ContentBrowser - option to include all folders in search results, instead of only those with results inside (much faster)
            //  ... how is this done in preferences? does the listview figure out what to show by itself???
            //IMP: ContentBrowser - separate all "UpdateFilter" calls, so instead only re-check the properties which were actually changed
            //  -> needs separated Visible values for nodes, one for each filter setting

            //Update filtering options
            var root = this.contentTreeListView.Objects.Cast<ContentBrowserNode>().Single();
            await Task.Factory.StartNew(() =>
            {
                root.UpdateFilter(this.filterControl.FilterSettings);
                RecurseFilter(root);
            });

            //Update UI
            //FIXME: ContentBrowser - why is this invoke needed? research about synchronizationcontext pls
            this.Invoke(new Action(() =>
            {
                if (this.filterControl.FilterSettings.HasFiltering)
                {
                    //Filter the items
                    this.contentTreeListView.AdditionalFilter = new ContentBrowserNodeFilter();
                    //BUG: ContentBrowser - highlighting search results is very limited right now
                    //  -> allow highlighting other columns as well (such as if type is the same), and do regex search if needed
                    this.contentTreeListView.TreeColumnRenderer.Filter =
                        new BrightIdeasSoftware.TextMatchFilter(this.contentTreeListView, this.filterControl.FilterSettings.PathFilter, StringComparison.InvariantCultureIgnoreCase);
                }
                else
                {
                    //Show all items
                    this.contentTreeListView.AdditionalFilter = null;
                }
            }));



            void RecurseFilter(ContentBrowserNode node)
            {
                if (node.Children == null)
                    return;

                foreach (var ch in node.Children.Reverse())
                {
                    ch.UpdateFilter(this.filterControl.FilterSettings);
                    RecurseFilter(ch);
                }
            }
        }

        private void FilterControl_FilterSettingsChanged(object sender, EventArgs e)
        {
            //Delayed update filtering (to avoid many freezes!)
            this._filterDelayTimer.Stop();
            this._filterDelayTimer.Start();
        }

        private readonly System.Timers.Timer _filterDelayTimer;

        /// <summary>
        /// Gets or sets whether this browser should allow for selecting multiple contents.
        /// <para/>If true, uses the checkbox view (on treeview, flatview has normal multiselect). If false, uses a simple select system in either case.
        /// </summary>
        public bool MultiSelect { get; set; } = false;

        /// <summary>
        /// Gets or sets whether to allow the user to create new files.
        /// </summary>
        public bool AllowNewFiles { get; set; }

        /// <summary>
        /// Gets or sets the full path text. Will try to select the corresponding content in the list, if possible.
        /// </summary>
        public string FullPath
        {
            get => this.pathTextBox.Text;
            set => this.pathTextBox.Text = value;
            //TODO: ContentBrowser - setting FullPath should also select the content in the list (if not found, should try until list is finished loading!)
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control. This property is never set to the same value that it already has.
        /// </summary>
        public string ActiveTheme { get; set; }

        /// <summary>
        /// Gets a list of selected files.
        /// </summary>
        public IEnumerable<ContentBrowserNode> SelectedFiles { get; private set; }

        private void CrawlModDirectories(CancellationToken token, IProgress<int> progress)
        {
            //Setup root
            var crawler = ModCrawler.CreateNew(SFM.GameInfo.Mods);
            crawler.CheckDuplicates = false;

            var rootnode = new ContentBrowserNode(null, null, false);
            foreach (var mod in SFM.GameInfo.Mods)
            {
                rootnode.Children.Add(new ContentBrowserNode(mod.Name, rootnode, true));
            }
            this.contentTreeListView.AddObject(rootnode);

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var modnames = SFM.GameInfo.Mods.Select(m => m.Name).ToList();
            foreach (var file in crawler.CrawlAll())
            {
                //Add to corresponding parents
                var newn = AddNodeChild(file, rootnode);

                if (stopwatch.ElapsedMilliseconds > 500)
                {
                    //Report progress
                    stopwatch.Restart();
                    var modid = modnames.IndexOf(newn.ModName) + 1;
                    progress.Report((int)Math.Round(((float)modid / SFM.GameInfo.Mods.Count) * 100));

                    //Check for cancel
                    if (token.IsCancellationRequested)
                        return;
                }
            }



            ContentBrowserNode AddNodeChild(string path, ContentBrowserNode parent)
            {
                //Get first bit
                var i = path.IndexOf('/');
                var bit = path.Substring(0, i);

                //Find node with that bit as name
                //BUG: ContentBrowser - a directory formatted ".text" will be recognized as a file and not a directory!
                var node = parent.Children.FirstOrDefault(n => n.Name == bit);
                if (node == null)
                {
                    //Create node with directory name and continue
                    node = new ContentBrowserNode(bit, parent, true);
                    parent.Children.Add(node);
                }

                //Cut off last dir
                var dir = path.Substring(i + 1);

                //Is only filename left?
                if (!dir.Contains("/"))
                {
                    //Create node for file
                    var newn = new ContentBrowserNode(dir, node, null);
                    //IMP: ContentBrowser - remove ALL instances of working with **System.Type** for Contents - ONLY use TypeName OR a content instance!!
                    newn.FileType = Content.DetectFileType(newn.LocalNodePath, null);
                    //FIXME: ContentBrowser - ContentByType call is not needed since an instance is already worked with in the DetectFileType call
                    newn.DisplayType = newn.FileType == null ? "Other" : Content.GetTypeName(Content.ContentByType(newn.FileType));
                    newn.NodeType = ContentBrowserNodeType.File;
                    node.Children.Add(newn);
                    return newn;
                }
                else
                {
                    //Go further
                    return AddNodeChild(dir, node);
                }
            }
        }

        private void UpdateProgress(int progress)
        {
            //Calculate and update loading progress
            if (progress > 0)
                this.buildingProgressBar.Style = ProgressBarStyle.Continuous;
            else
                this.buildingProgressBar.Style = ProgressBarStyle.Marquee;
            this.buildingProgressLabel.Text = $"{progress}%";
            this.buildingProgressBar.Value = progress;
        }

        private string ModComboBox_DisplayFunction(IEnumerable<object> enabled)
        {
            if (!enabled.Any())
                return this.modComboBox.FormatDisplayString("No mods");

            var checkedMods = enabled.Cast<Mod>().Select(m => m.Name);
            var enabledMods = SFM.GameInfo.Mods.Where(m => m.Enabled).Select(m => m.Name);
            if (checkedMods.ContentsEquals(enabledMods))
            {
                //All enabled mods are checked
                return this.modComboBox.FormatDisplayString("Available mods");
            }

            var allMods = SFM.GameInfo.Mods.Select(m => m.Name);
            if (checkedMods.ContentsEquals(allMods))
            {
                //All mods are checked
                return this.modComboBox.FormatDisplayString("All mods");
            }

            //Default string
            return null;
        }

        private void browseButton_Click(object sender, System.EventArgs e)
        {
            //Native file browser to open content
            var res = IOHelper.OpenFileDialog(false, false)?.SingleOrDefault();
            if (!string.IsNullOrEmpty(res))
                this.pathTextBox.Text = res;
        }

        private void contentTreeListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            this.SelectedFiles = this.contentTreeListView.CheckedObjectsEnumerable.Cast<ContentBrowserNode>();
        }

        private void filterControl_ExpandedChanged(object sender, EventArgs e)
        {
            //Update height of filtercontrol row - add 4 due to margins
            this.mainTableLayoutPanel.RowStyles[3].Height = this.filterControl.PreferredHeight + 4;
        }

        private void viewPathButton_Click(object sender, EventArgs e)
        {
            //Open specified path in explorer
            IOHelper.OpenInExplorer(this.pathTextBox.Text, true);
        }

        private void contentTreeListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            //Check if we're a valid content file
            var node = (ContentBrowserNode)this.contentTreeListView.SelectedObject;
            if (node.NodeType == ContentBrowserNodeType.Directory)
                return;

            //Update dependency graph
            this.dependencyGraphViewer.Content = node.Content;
        }

        private void reloadAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TODO: ContentBrowser - reload gameinfo here (check for new mods)

            //Clear current content list
            this.contentTreeListView.ClearObjects();

            //Reload all assets
            var task = this.SetupBrowser();
        }

        private void reloadChangedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //NYI: ContentBrowser - add new files and remove non-existing files while keeping the UI in-tact
            //throw new NotImplementedException();
            var ct = new ContentTree("C:/tree.ct", false);
            ct.Nodes.AddRange(this.contentTreeListView.Objects.OfType<ContentBrowserNode>());
            ct.Save();
        }

        private void reloadSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //NYI: ContentBrowser - reload only selected content (remove missing files, force load dependency info and the like)
            throw new NotImplementedException();
        }

        private void rescanContextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Disable "reload selected" if nothing is selected
            this.reloadSelectedToolStripMenuItem.Enabled = this.contentTreeListView.SelectedObjects.Count > 0;
        }
    }
}
