﻿using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;
using SFMTK.Properties;

namespace SFMTK
{
    /// <summary>
    /// Similar to the FallbackTaskDialog, except it implements a "Remember this decision" checkbox, which gets stored and managed in the application settings.
    /// </summary>
    public class FallbackRememberDialog : FallbackTaskDialog
    {
        //IMP: FallbackRememberDialog - can extra info be shown together with the verification text?? -> reset all buttons in preferences + method overload

        /// <summary>
        /// Default label for the checkbox. Use for actual decisions.
        /// </summary>
        public static string RememberDecisionText => Resources.RememberThisDecision;

        /// <summary>
        /// A standard label for the checkbox. Use when at least one button has <see cref="ExecuteOnRemember"/> set to <c>false</c>, or it's an information of sorts.
        /// </summary>
        public static string DontAskAgainText => Resources.DontAskAgain;

        /// <summary>
        /// A standard label for the checkbox. Mostly used for errors.
        /// </summary>
        public static string DontNotifyAgainText => Resources.DontNotifyAgain;

        /// <summary>
        /// Initializes a new instance of the <see cref="FallbackRememberDialog"/> class without any properties.
        /// </summary>
        public FallbackRememberDialog()
        {
            this.VerificationText = RememberDecisionText;
            this.ExpandedInformation = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FallbackRememberDialog" /> class with text properties. Uses the info icon and an OK button.
        /// <para />The checkbox text is defaulted, and the identifier is taken from the caption, unless otherwise specified.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="checkbox">Text to display on the "remember" checkbox.</param>
        /// <param name="identifier">How the dialog box should be identified when verifying/resetting the checkboxes config entries.</param>
        public FallbackRememberDialog(string text, string headline, string caption, string checkbox = null, string identifier = null) : base(text, headline, caption)
        {
            this.VerificationText = checkbox ?? RememberDecisionText;
            this.ExpandedInformation = null;
            this.Identifier = identifier;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FallbackRememberDialog"/> class with basic properties.
        /// <para/>The checkbox text is defaulted, and the identifier is taken from the caption if they are null.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="checkbox">Text to display on the "remember" checkbox.</param>
        /// <param name="identifier">How the dialog box should be identified when verifying/resetting the checkboxes config entries.</param>
        /// <param name="icon">The big icon of the dialog box.</param>
        /// <param name="buttons">The buttons to include on the dialog box.</param>
        public FallbackRememberDialog(string text, string headline, string caption, string checkbox, string identifier, FallbackDialogIcon icon, params FallbackDialogButton[] buttons)
            : base(text, headline, caption, icon, buttons)
        {
            this.VerificationText = checkbox ?? RememberDecisionText;
            this.ExpandedInformation = null;
            this.Identifier = identifier;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FallbackRememberDialog"/> class with most important properties.
        /// <para/>The checkbox text is defaulted, and the identifier is taken from the caption if they are null.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="extrainfo">The information footer to include additional (mostly error) information in.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="checkbox">Text to display on the "remember" checkbox.</param>
        /// <param name="identifier">How the dialog box should be identified when verifying/resetting the checkboxes config entries.</param>
        /// <param name="icon">The big icon of the dialog box.</param>
        /// <param name="buttons">The buttons to include on the dialog box.</param>
        public FallbackRememberDialog(string text, string headline, string caption, string extrainfo, string checkbox, string identifier, FallbackDialogIcon icon, params FallbackDialogButton[] buttons)
            : base(text, headline, caption, extrainfo, icon, buttons)
        {
            this.VerificationText = checkbox ?? RememberDecisionText;
            this.ExpandedInformation = null;
            this.Identifier = identifier;
        }

        /// <summary>
        /// Creates a <see cref="FallbackRememberDialog"/> with text properties. Uses the info icon and an OK button.
        /// <para/>The checkbox text is defaulted, and the identifier is taken from the caption, unless otherwise specified.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="checkbox">Text to display on the "remember" checkbox.</param>
        /// <param name="identifier">How the dialog box should be identified when verifying/resetting the checkboxes config entries.</param>
        public static DialogResult ShowDialog(string text, string headline, string caption, string checkbox = null, string identifier = null)
            => ShowDialog(text, headline, caption, checkbox ?? RememberDecisionText, identifier, FallbackDialogIcon.Info, new FallbackDialogButton(DialogResult.OK));

        /// <summary>
        /// Creates a <see cref="FallbackRememberDialog" /> with basic properties.
        /// <para />The checkbox text is defaulted, and the identifier is taken from the caption if they are null.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="checkbox">Text to display on the "remember" checkbox.</param>
        /// <param name="identifier">How the dialog box should be identified when verifying/resetting the checkboxes config entries.</param>
        /// <param name="icon">The big icon of the dialog box.</param>
        /// <param name="buttons">The buttons to include on the dialog box.</param>
        public static DialogResult ShowDialog(string text, string headline, string caption, string checkbox, string identifier,
            FallbackDialogIcon icon, params FallbackDialogButton[] buttons)
                => ShowDialog(text, headline, caption, null, checkbox ?? RememberDecisionText, identifier, icon, buttons);

        /// <summary>
        /// Creates a <see cref="FallbackRememberDialog" /> with most important properties.
        /// <para />The checkbox text is defaulted, and the identifier is taken from the caption if they are null.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="extraInfo">The information footer to include additional (mostly error) information in.</param>
        /// <param name="checkbox">Text to display on the "remember" checkbox.</param>
        /// <param name="identifier">How the dialog box should be identified when verifying/resetting the checkboxes config entries.</param>
        /// <param name="icon">The big icon of the dialog box.</param>
        /// <param name="buttons">The buttons to include on the dialog box.</param>
        public static DialogResult ShowDialog(string text, string headline, string caption, string extraInfo, string checkbox, string identifier,
            FallbackDialogIcon icon, params FallbackDialogButton[] buttons)
                => new FallbackRememberDialog(text, headline, caption, extraInfo, checkbox ?? RememberDecisionText, identifier, icon, buttons).ShowDialogOrFallback().Result;

        /// <summary>
        /// Creates a <see cref="FallbackRememberDialog"/> with text properties. Uses the info icon and an OK button.
        /// <para/>The checkbox text is defaulted, and the identifier is taken from the caption, unless otherwise specified.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="checkbox">Text to display on the "remember" checkbox.</param>
        /// <param name="identifier">How the dialog box should be identified when verifying/resetting the checkboxes config entries.</param>
        public static DialogResult ShowDialogLog(string text, string headline, string caption, string checkbox = null, string identifier = null)
            => ShowDialogLog(text, headline, caption, checkbox ?? RememberDecisionText, identifier, FallbackDialogIcon.Info, new FallbackDialogButton(DialogResult.OK));

        /// <summary>
        /// Creates a <see cref="FallbackRememberDialog" /> with basic properties.
        /// <para />The checkbox text is defaulted, and the identifier is taken from the caption if they are null.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="checkbox">Text to display on the "remember" checkbox.</param>
        /// <param name="identifier">How the dialog box should be identified when verifying/resetting the checkboxes config entries.</param>
        /// <param name="icon">The big icon of the dialog box.</param>
        /// <param name="buttons">The buttons to include on the dialog box.</param>
        public static DialogResult ShowDialogLog(string text, string headline, string caption, string checkbox, string identifier,
            FallbackDialogIcon icon, params FallbackDialogButton[] buttons)
                => ShowDialogLog(text, headline, caption, null, checkbox ?? RememberDecisionText, identifier, icon, buttons);

        /// <summary>
        /// Creates a <see cref="FallbackRememberDialog" /> with most important properties.
        /// <para />The checkbox text is defaulted, and the identifier is taken from the caption if they are null.
        /// </summary>
        /// <param name="text">Main text body of the dialog box.</param>
        /// <param name="headline">The headline of the dialog box, emphasizing a certain short title phrase.</param>
        /// <param name="caption">The title of the dialog box, mostly used to quickly summarize why the dialog box appeared.</param>
        /// <param name="extraInfo">The information footer to include additional (mostly error) information in.</param>
        /// <param name="checkbox">Text to display on the "remember" checkbox.</param>
        /// <param name="identifier">How the dialog box should be identified when verifying/resetting the checkboxes config entries.</param>
        /// <param name="icon">The big icon of the dialog box.</param>
        /// <param name="buttons">The buttons to include on the dialog box.</param>
        public static DialogResult ShowDialogLog(string text, string headline, string caption, string extraInfo, string checkbox, string identifier,
            FallbackDialogIcon icon, params FallbackDialogButton[] buttons)
        {
            var td = new FallbackRememberDialog(text, headline, caption, extraInfo, checkbox ?? RememberDecisionText, identifier, icon, buttons);
            td.LogDialog();
            return td.ShowDialogOrFallback().Result;
        }


        /// <summary>
        /// How the dialog box should be identified when verifying/resetting the checkboxes config entries.
        /// </summary>
        public string Identifier
        {
            get => this._identifier ?? this.WindowTitle;
            set => this._identifier = value;
        }

        private string _identifier;

        /// <summary>
        /// Shows the task dialog, falling back if needed.
        /// </summary>
        public override FallbackDialogButton ShowDialogOrFallback()
        {
            //Check settings for state
            var result = Settings.Default.RememberDialogSettings.FirstOrDefault(s => s.Identifier == this.Identifier);
            if (result != null)
            {
                //Directly return stored result here and now
                if (result.Execute)
                    return new FallbackDialogButton(result.Result);

                //Only check the box
                this.IsVerificationChecked = true;
            }

            //Show dialog
            var newres = base.ShowDialogOrFallback();

            //Update result if checked
            if (this.IsVerificationChecked)
            {
                if (result == null)
                {
                    //Add remember
                    Settings.Default.RememberDialogSettings.Add(new RememberDialogSetting(this.Identifier, newres.Text, newres.Result, newres.ExecuteOnRemember));
                }
                else
                {
                    //Update existing
                    result.Label = newres.Text;
                    result.Result = newres.Result;
                    result.Execute = newres.ExecuteOnRemember;
                }
            }

            return newres;
        }
    }

    /// <summary>
    /// A setting stored for a specific <see cref="FallbackRememberDialog"/>, identified uniquely.
    /// </summary>
    [System.Serializable]
    public class RememberDialogSetting
    {
        //TODO: RememberDialogSetting - may add a way to specify a "group", to easily categorize these inside of the settings. maybe delimiter in identifier string?

        /// <summary>
        /// Initializes a new instance of the <see cref="RememberDialogSetting"/> class.
        /// </summary>
        public RememberDialogSetting()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RememberDialogSetting" /> class.
        /// </summary>
        /// <param name="identifier">The identifier of the dialog.</param>
        /// <param name="label">The display text of the setting.</param>
        /// <param name="result">The result that is to be remembered when opening this dialog again.</param>
        /// <param name="execute">If set to <c>true</c>, executes this option as well.</param>
        public RememberDialogSetting(string identifier, string label, DialogResult result, bool execute)
        {
            this.Identifier = identifier;
            this.Result = result;
            this.Execute = execute;
            this.Label = label;
        }

        /// <summary>
        /// Gets the identifier of the dialog.
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Gets the result that is to be remembered when opening this dialog again.
        /// </summary>
        public DialogResult Result { get; set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="RememberDialogSetting"/> will execute the button press.
        /// </summary>
        public bool Execute { get; set; }

        /// <summary>
        /// Custom button label, if any. Otherwise returns the DialogResult string.
        /// </summary>
        public string Label
        {
            get => string.IsNullOrEmpty(this._label) ? this.Result.ToString() : this._label;
            set => this._label = value;
        }

        private string _label;
    }

    /// <summary>
    /// A filter for RememberDialogSetting instances, checking for the State and Name without watching upper or lower-case.
    /// </summary>
    /// <seealso cref="IModelFilter" />
    public class RememberDialogSettingFilter : IModelFilter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RememberDialogSettingFilter"/> class.
        /// </summary>
        /// <param name="text">The text to filter for.</param>
        public RememberDialogSettingFilter(string text)
        {
            this.FilterText = text;
        }

        /// <summary>
        /// The text to filter the shortcut entries after.
        /// </summary>
        public string FilterText { get; set; }

        /// <summary>
        /// Should the given model be included when this filter is installed
        /// </summary>
        /// <param name="modelObject">The model object to consider</param>
        /// <returns>
        /// Returns true if the model will be included by the filter
        /// </returns>
        public bool Filter(object modelObject)
        {
            if (modelObject is RememberDialogSetting rds)
            {
                var txt = this.FilterText;
                if (rds.Label.ContainsIgnoreCase(txt) || rds.Identifier.ContainsIgnoreCase(txt))
                    return true;
            }
            return false;
        }
    }
}
