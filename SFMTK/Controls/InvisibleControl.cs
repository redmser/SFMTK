﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// A control used for filling space in a FlowLayoutPanel.
    /// </summary>
    public sealed class InvisibleControl : Control
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvisibleControl"/> class.
        /// </summary>
        public InvisibleControl()
        {
            this.TabStop = false;
        }

        /// <summary>
        /// Gets or sets the edges of the container to which a control is bound and determines how a control is resized with its parent.
        /// </summary>
        public override AnchorStyles Anchor { get => AnchorStyles.None; set { return; } }

        /// <summary>
        /// Gets or sets which control borders are docked to its parent control and determines how a control is resized with its parent.
        /// </summary>
        public override DockStyle Dock { get => DockStyle.None; set { return; } }

        /// <summary>
        /// Coordinates of this Control always return Point.Empty.
        /// </summary>
        public new Point Location
        {
            get => Point.Empty;
            set => base.Location = Point.Empty;
        }

        private Orientation _orientation = Orientation.Horizontal;

        /// <summary>
        /// Gets or sets the orientation of the control.
        /// </summary>
        [DefaultValue(typeof(Orientation), "Horizontal")]
        public Orientation Orientation
        {
            get => this._orientation; set
            {
                if (this._orientation == value)
                    return;
                this._orientation = value;
                this.ChangeSize();
            }
        }

        /// <summary>
        /// Always returns Padding.Empty.
        /// </summary>
        protected override Padding DefaultMargin => Padding.Empty;

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" /> and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
                this.SetParent(null);
            base.Dispose(disposing);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.HandleCreated" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnHandleCreated(EventArgs e)
        {
            // This seems to be needed for IDE support, as OnParentChanged does not seem
            // to fire if the control is dropped onto a surface for the first time
            base.OnHandleCreated(e);
            this.ChangeSize();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the event data.</param>
        protected override void OnPaint(PaintEventArgs e) { }

        /// <summary>
        /// Paints the background of the control.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains information about the control to paint.</param>
        protected override void OnPaintBackground(PaintEventArgs e) { }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ParentChanged" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);

            // Unsubscribe from the previous parent's Resize event, if applicable
            // Subscribe to the new parent's Resize event
            this.SetParent(this.Parent);

            // Resize our control according to the new parent dimensions
            this.ChangeSize();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Resize" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            // We don't really want to be resized, so deal with it
            this.ChangeSize();
        }

        // Make this a default handler signature with optional params, so that this can
        // directly subscribe to the parent resize event, but also be called without parameters
        private void ChangeSize(object sender = null, EventArgs e = null)
        {
            Rectangle client = this.Parent?.ClientRectangle ?? new Rectangle(0, 0, 10, 10);
            Size proposedSize = this._orientation == Orientation.Horizontal
              ? new Size(client.Width, 0)
              : new Size(0, client.Height);
            if (!this.Size.Equals(proposedSize))
                this.Size = proposedSize;
        }

        // Handles reparenting
        private Control _boundParent;
        private void SetParent(Control parent)
        {
            if (this._boundParent != null)
                this._boundParent.Resize -= this.ChangeSize;
            this._boundParent = parent;
            if (this._boundParent != null)
                this._boundParent.Resize += this.ChangeSize;
        }
    }
}
