﻿using System;
using System.Drawing;
using BrightIdeasSoftware;

namespace SFMTK.Controls
{
    /// <summary>
    /// An overlay which can be used in an ObjectListView for the EmptyListMsg, to keep a standardized feeling between them.
    /// </summary>
    /// <seealso cref="BrightIdeasSoftware.TextOverlay" />
    public class EmptyListOverlay : TextOverlay
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmptyListOverlay"/> class.
        /// </summary>
        public EmptyListOverlay(ObjectListView listview, bool border = false)
        {
            this.Font = SystemFonts.DefaultFont;
            this.BackColor = Color.Empty;
            this.BorderWidth = border ? 1 : 0;
            this.Text = listview.EmptyListMsg;
            this.CornerRounding = 4;
            this.Offset = Size.Empty;
            this.Alignment = ContentAlignment.MiddleCenter;
            this.ReferenceCorner = ContentAlignment.MiddleCenter;

            if (UITheme.SelectedTheme == null)
                return;

            this.TextColor = UITheme.SelectedTheme.MainTextColor;
            this.BorderColor = UITheme.SelectedTheme.MainTextColor;
        }
    }
}
