﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// A PropertyGrid which can be made read-only.
    /// </summary>
    public class ReadOnlyPropertyGrid : PropertyGrid
    {
        private bool _readOnly;

        /// <summary>
        /// Whether the PropertyGrid is read-only.
        /// </summary>
        public bool ReadOnly
        {
            get => this._readOnly; set
            {
                this._readOnly = value;
                this.UpdateReadOnly();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.PropertyGrid.SelectedObjectsChanged" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnSelectedObjectsChanged(EventArgs e)
        {
            this.UpdateReadOnly();
            base.OnSelectedObjectsChanged(e);
        }

        private void UpdateReadOnly()
        {
            if (this.SelectedObject != null)
            {
                TypeDescriptor.AddAttributes(this.SelectedObject, new Attribute[] { new ReadOnlyAttribute(this._readOnly) });
                this.Refresh();
            }
        }
    }
}
