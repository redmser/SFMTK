﻿using System;

namespace SFMTK.Controls.PreferencePages
{
    public partial class TextEditorPreferencePage : PreferencePage
    {
        //NYI: TextEditorPreferencePage - (live-)update the text editor preferences

        public TextEditorPreferencePage()
        {
        }

        public override void OnFirstSelect(object sender, EventArgs e)
        {
            InitializeComponent();

            base.OnFirstSelect(sender, e);
        }

        public override string Path => "Editors/Text Editor";

        public override System.Drawing.Image Image => Properties.Resources.page;

        public override System.Windows.Forms.Control ExtendControl => this.mainFlowLayoutPanel;

        private void templateEditorButton_Click(object sender, EventArgs e)
        {
            //NYI: TextEditorPreferencePage - open file template editor (select per content type, store caret position too)
            throw new NotImplementedException();
        }

        private void syntaxHighlightingButton_Click(object sender, EventArgs e)
        {
            //NYI: TextEditorPreferencePage - open syntax highlighting editor (tab inside of theme editor!)
            throw new NotImplementedException();
        }
    }
}
