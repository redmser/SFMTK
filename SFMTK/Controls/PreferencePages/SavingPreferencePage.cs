﻿using SFMTK.Forms;
using System;
using System.Windows.Forms;

namespace SFMTK.Controls.PreferencePages
{
    public partial class SavingPreferencePage : PreferencePage
    {
        //NYI: SavingPreferencePage - auto-compiling on save (+ setup config)
        //TODO: SavingPreferencePage - change the "which contents to consider" list from hardcoded to booleans on all content types "IsCompiled"
        //FIXME: SavingPreferencePage - inconsistent compile on save groupbox doesn't disable from checkbox, while autosaving one does
        //  -> if the edit button is the problem, just move to-be-disabled content to its own disabling-panel

        public SavingPreferencePage()
        {
        }

        public override string Path => "Content/Saving";

        public override System.Drawing.Image Image => Properties.Resources.disk;

        public override Control ExtendControl => this.mainFlowLayoutPanel;

        public override void OnFirstSelect(object sender, EventArgs e)
        {
            InitializeComponent();

            var autosavemodebinding = new CastBinding<Properties.AutoSaveMode, int>("Selected", Properties.Settings.Default, "AutosaveMode", DataSourceUpdateMode.OnPropertyChanged);
            this.autosaveModeRadioGroupPanel.DataBindings.Add(autosavemodebinding);
            var compileonsavebinding = new CastBinding<Properties.CompileOnSaveCancel, int>("Selected", Properties.Settings.Default, "CompileOnSaveCancel", DataSourceUpdateMode.OnPropertyChanged);
            this.autoCompileRadioGroupPanel.DataBindings.Add(compileonsavebinding);

            base.OnFirstSelect(sender, e);
        }

        private void autoCompileViewButton_Click(object sender, EventArgs e)
        {
            //Open current setup's settings on the first tab
            new EditSetup(Setup.Current).ShowDialog(this);
        }
    }
}
