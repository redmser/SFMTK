﻿using System.Drawing;

namespace SFMTK.Controls.PreferencePages
{
    public class PreferencePageNode : IPreferencePage
    {
        public PreferencePageNode(string path, Image img = null, string targetpath = null, bool isRoot = false)
        {
            this.Path = path;
            this.Image = img ?? Properties.Resources.folder;
            this.TargetPage = targetpath ?? path;
            this.IsRoot = isRoot;
        }

        /// <summary>
        /// Gets or sets whether this element is a root element in the tree view.
        /// </summary>
        public bool IsRoot { get; set; }

        /// <summary>
        /// Gets or sets the text associated with this control.
        /// </summary>
        public string Text
        {
            get
            {
                if (this.Path.EndsWith("/"))
                    this.Path = this.Path.Substring(0, this.Path.Length - 1);

                var lastind = this.Path.LastIndexOf('/');
                if (lastind < 0)
                    return this.Path;
                return this.Path.Substring(lastind + 1);
            }
            set => this.Path = value;
        }

        /// <summary>
        /// Gets or sets the path to this preference page. Use slashes '/' to signify nested pages.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets an optional image to associate to the preference page.
        /// </summary>
        public Image Image { get; set; }

        /// <summary>
        /// Gets the path to the page to select.
        /// </summary>
        public string TargetPage { get; }

        /// <summary>
        /// Gets or sets whether this node should be visible after filtering through a search.
        /// </summary>
        public bool Filter { get; set; }
    }
}
