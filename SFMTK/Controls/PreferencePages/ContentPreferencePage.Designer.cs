﻿namespace SFMTK.Controls.PreferencePages
{
    partial class ContentPreferencePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.invisibleControl3 = new SFMTK.Controls.InvisibleControl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.hideTitleNameCheckBox = new System.Windows.Forms.CheckBox();
            this.fullTitleComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabTitleComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.newMenuCheckBox = new System.Windows.Forms.CheckBox();
            this.nativeFileBrowserCheckBox = new System.Windows.Forms.CheckBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.mainFlowLayoutPanel.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainFlowLayoutPanel
            // 
            this.mainFlowLayoutPanel.AutoScroll = true;
            this.mainFlowLayoutPanel.Controls.Add(this.invisibleControl3);
            this.mainFlowLayoutPanel.Controls.Add(this.groupBox5);
            this.mainFlowLayoutPanel.Controls.Add(this.newMenuCheckBox);
            this.mainFlowLayoutPanel.Controls.Add(this.nativeFileBrowserCheckBox);
            this.mainFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.mainFlowLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainFlowLayoutPanel.Name = "mainFlowLayoutPanel";
            this.mainFlowLayoutPanel.Size = new System.Drawing.Size(311, 281);
            this.mainFlowLayoutPanel.TabIndex = 2;
            this.mainFlowLayoutPanel.WrapContents = false;
            // 
            // invisibleControl3
            // 
            this.invisibleControl3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.invisibleControl3.Location = new System.Drawing.Point(0, 0);
            this.invisibleControl3.Name = "invisibleControl3";
            this.invisibleControl3.Size = new System.Drawing.Size(311, 0);
            this.invisibleControl3.TabIndex = 0;
            this.invisibleControl3.TabStop = false;
            this.invisibleControl3.Text = "invisibleControl3";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.hideTitleNameCheckBox);
            this.groupBox5.Controls.Add(this.fullTitleComboBox);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.tabTitleComboBox);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(305, 96);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Display && Naming";
            // 
            // hideTitleNameCheckBox
            // 
            this.hideTitleNameCheckBox.AutoSize = true;
            this.hideTitleNameCheckBox.Location = new System.Drawing.Point(12, 72);
            this.hideTitleNameCheckBox.Name = "hideTitleNameCheckBox";
            this.hideTitleNameCheckBox.Size = new System.Drawing.Size(119, 17);
            this.hideTitleNameCheckBox.TabIndex = 4;
            this.hideTitleNameCheckBox.Text = "&Hide name from title";
            this.toolTip.SetToolTip(this.hideTitleNameCheckBox, "If checked, does not show the name of the currently worked on content in the titl" +
        "e bar of SFMTK");
            this.hideTitleNameCheckBox.UseVisualStyleBackColor = true;
            // 
            // fullTitleComboBox
            // 
            this.fullTitleComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fullTitleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fullTitleComboBox.FormattingEnabled = true;
            this.fullTitleComboBox.Items.AddRange(new object[] {
            "Full path",
            "Local path (with mod name)",
            "Local path (without mod name)",
            "File name only"});
            this.fullTitleComboBox.Location = new System.Drawing.Point(56, 44);
            this.fullTitleComboBox.Name = "fullTitleComboBox";
            this.fullTitleComboBox.Size = new System.Drawing.Size(241, 21);
            this.fullTitleComboBox.TabIndex = 3;
            this.toolTip.SetToolTip(this.fullTitleComboBox, "How content files should display their name in SFMTK\'s title and in any list of o" +
        "pened content.");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "&Full title:";
            // 
            // tabTitleComboBox
            // 
            this.tabTitleComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabTitleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tabTitleComboBox.FormattingEnabled = true;
            this.tabTitleComboBox.Items.AddRange(new object[] {
            "Full path",
            "Local path (with mod name)",
            "Local path (without mod name)",
            "File name only"});
            this.tabTitleComboBox.Location = new System.Drawing.Point(56, 17);
            this.tabTitleComboBox.Name = "tabTitleComboBox";
            this.tabTitleComboBox.Size = new System.Drawing.Size(241, 21);
            this.tabTitleComboBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.tabTitleComboBox, "How content files should display their name in the tab\'s title.");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "&Tab title:";
            // 
            // newMenuCheckBox
            // 
            this.newMenuCheckBox.AutoSize = true;
            this.newMenuCheckBox.Checked = global::SFMTK.Properties.Settings.Default.NewMenu;
            this.newMenuCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.newMenuCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "NewMenu", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.newMenuCheckBox.Location = new System.Drawing.Point(3, 105);
            this.newMenuCheckBox.Name = "newMenuCheckBox";
            this.newMenuCheckBox.Size = new System.Drawing.Size(134, 17);
            this.newMenuCheckBox.TabIndex = 0;
            this.newMenuCheckBox.Text = "Show \"&New\" submenu";
            this.toolTip.SetToolTip(this.newMenuCheckBox, "If enabled, the File -> New entry is a menu for all content types, instead of a s" +
        "imple menu item");
            this.newMenuCheckBox.UseVisualStyleBackColor = true;
            // 
            // nativeFileBrowserCheckBox
            // 
            this.nativeFileBrowserCheckBox.AutoSize = true;
            this.nativeFileBrowserCheckBox.Checked = global::SFMTK.Properties.Settings.Default.NativeFileBrowser;
            this.nativeFileBrowserCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "NativeFileBrowser", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.nativeFileBrowserCheckBox.Location = new System.Drawing.Point(3, 128);
            this.nativeFileBrowserCheckBox.Name = "nativeFileBrowserCheckBox";
            this.nativeFileBrowserCheckBox.Size = new System.Drawing.Size(133, 17);
            this.nativeFileBrowserCheckBox.TabIndex = 1;
            this.nativeFileBrowserCheckBox.Text = "Use native file &browser";
            this.toolTip.SetToolTip(this.nativeFileBrowserCheckBox, "Whether to use your OS\' file browser or one similar to SFM for loading and saving" +
        " content");
            this.nativeFileBrowserCheckBox.UseVisualStyleBackColor = true;
            // 
            // ContentPreferencePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainFlowLayoutPanel);
            this.Name = "ContentPreferencePage";
            this.Size = new System.Drawing.Size(311, 281);
            this.mainFlowLayoutPanel.ResumeLayout(false);
            this.mainFlowLayoutPanel.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel mainFlowLayoutPanel;
        private InvisibleControl invisibleControl3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox hideTitleNameCheckBox;
        private System.Windows.Forms.ComboBox fullTitleComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox tabTitleComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox newMenuCheckBox;
        private System.Windows.Forms.CheckBox nativeFileBrowserCheckBox;
        private System.Windows.Forms.ToolTip toolTip;
    }
}