﻿namespace SFMTK.Controls.PreferencePages
{
    partial class TextEditorPreferencePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.invisibleControl4 = new SFMTK.Controls.InvisibleControl();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.templateEditorButton = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.syntaxHighlightingButton = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.highlightCurrentLineCheckBox = new System.Windows.Forms.CheckBox();
            this.scrollBelowDocumentCheckBox = new System.Windows.Forms.CheckBox();
            this.cutCopyWholeLineCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.tabsToSpacesCheckBox = new System.Windows.Forms.CheckBox();
            this.showWhitespaceCheckBox = new System.Windows.Forms.CheckBox();
            this.showEndOfLineCheckBox = new System.Windows.Forms.CheckBox();
            this.virtualSpaceCheckBox = new System.Windows.Forms.CheckBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.mainFlowLayoutPanel.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainFlowLayoutPanel
            // 
            this.mainFlowLayoutPanel.AutoScroll = true;
            this.mainFlowLayoutPanel.Controls.Add(this.invisibleControl4);
            this.mainFlowLayoutPanel.Controls.Add(this.groupBox10);
            this.mainFlowLayoutPanel.Controls.Add(this.groupBox11);
            this.mainFlowLayoutPanel.Controls.Add(this.highlightCurrentLineCheckBox);
            this.mainFlowLayoutPanel.Controls.Add(this.scrollBelowDocumentCheckBox);
            this.mainFlowLayoutPanel.Controls.Add(this.cutCopyWholeLineCheckBox);
            this.mainFlowLayoutPanel.Controls.Add(this.groupBox12);
            this.mainFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.mainFlowLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainFlowLayoutPanel.Name = "mainFlowLayoutPanel";
            this.mainFlowLayoutPanel.Size = new System.Drawing.Size(385, 421);
            this.mainFlowLayoutPanel.TabIndex = 0;
            this.mainFlowLayoutPanel.WrapContents = false;
            // 
            // invisibleControl4
            // 
            this.invisibleControl4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.invisibleControl4.Location = new System.Drawing.Point(0, 0);
            this.invisibleControl4.Name = "invisibleControl4";
            this.invisibleControl4.Size = new System.Drawing.Size(385, 0);
            this.invisibleControl4.TabIndex = 9;
            this.invisibleControl4.TabStop = false;
            this.invisibleControl4.Text = "invisibleControl4";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.templateEditorButton);
            this.groupBox10.Controls.Add(this.label17);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox10.Location = new System.Drawing.Point(3, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(379, 93);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "File Templates";
            // 
            // templateEditorButton
            // 
            this.templateEditorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.templateEditorButton.Location = new System.Drawing.Point(8, 57);
            this.templateEditorButton.Name = "templateEditorButton";
            this.templateEditorButton.Size = new System.Drawing.Size(168, 27);
            this.templateEditorButton.TabIndex = 1;
            this.templateEditorButton.Text = "Open File &Template Editor...";
            this.templateEditorButton.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.Location = new System.Drawing.Point(8, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(364, 36);
            this.label17.TabIndex = 0;
            this.label17.Text = "By specifying a custom file template, you can change what a new content file of a" +
    "ny type should contain by default.";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.syntaxHighlightingButton);
            this.groupBox11.Controls.Add(this.label19);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox11.Location = new System.Drawing.Point(3, 102);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(379, 118);
            this.groupBox11.TabIndex = 13;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Syntax Highlighting";
            // 
            // syntaxHighlightingButton
            // 
            this.syntaxHighlightingButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.syntaxHighlightingButton.Location = new System.Drawing.Point(8, 82);
            this.syntaxHighlightingButton.Name = "syntaxHighlightingButton";
            this.syntaxHighlightingButton.Size = new System.Drawing.Size(168, 27);
            this.syntaxHighlightingButton.TabIndex = 1;
            this.syntaxHighlightingButton.Text = "Edit &Syntax Highlighting...";
            this.syntaxHighlightingButton.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.Location = new System.Drawing.Point(8, 24);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(360, 58);
            this.label19.TabIndex = 0;
            this.label19.Text = "Syntax highlighting changes the color of certain keywords depending on what they " +
    "mean. Click the button below to change which keywords use which colors.";
            // 
            // highlightCurrentLineCheckBox
            // 
            this.highlightCurrentLineCheckBox.AutoSize = true;
            this.highlightCurrentLineCheckBox.Checked = global::SFMTK.Properties.Settings.Default.TextEditorHighlightCurrentLine;
            this.highlightCurrentLineCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.highlightCurrentLineCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "TextEditorHighlightCurrentLine", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.highlightCurrentLineCheckBox.Location = new System.Drawing.Point(3, 226);
            this.highlightCurrentLineCheckBox.Name = "highlightCurrentLineCheckBox";
            this.highlightCurrentLineCheckBox.Size = new System.Drawing.Size(122, 17);
            this.highlightCurrentLineCheckBox.TabIndex = 1;
            this.highlightCurrentLineCheckBox.Text = "&Highlight current line";
            this.toolTip.SetToolTip(this.highlightCurrentLineCheckBox, "Highlights the line that the cursor is on with a different color");
            this.highlightCurrentLineCheckBox.UseVisualStyleBackColor = true;
            // 
            // scrollBelowDocumentCheckBox
            // 
            this.scrollBelowDocumentCheckBox.AutoSize = true;
            this.scrollBelowDocumentCheckBox.Checked = global::SFMTK.Properties.Settings.Default.TextEditorAllowScrollBelowDocument;
            this.scrollBelowDocumentCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.scrollBelowDocumentCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "TextEditorAllowScrollBelowDocument", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.scrollBelowDocumentCheckBox.Location = new System.Drawing.Point(3, 249);
            this.scrollBelowDocumentCheckBox.Name = "scrollBelowDocumentCheckBox";
            this.scrollBelowDocumentCheckBox.Size = new System.Drawing.Size(191, 17);
            this.scrollBelowDocumentCheckBox.TabIndex = 2;
            this.scrollBelowDocumentCheckBox.Text = "&Allow scrolling below the document";
            this.toolTip.SetToolTip(this.scrollBelowDocumentCheckBox, "If enabled, allow scrolling below the text part of the document");
            this.scrollBelowDocumentCheckBox.UseVisualStyleBackColor = true;
            // 
            // cutCopyWholeLineCheckBox
            // 
            this.cutCopyWholeLineCheckBox.AutoSize = true;
            this.cutCopyWholeLineCheckBox.Checked = global::SFMTK.Properties.Settings.Default.TextEditorCutCopyWholeLine;
            this.cutCopyWholeLineCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cutCopyWholeLineCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "TextEditorCutCopyWholeLine", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cutCopyWholeLineCheckBox.Location = new System.Drawing.Point(3, 272);
            this.cutCopyWholeLineCheckBox.Name = "cutCopyWholeLineCheckBox";
            this.cutCopyWholeLineCheckBox.Size = new System.Drawing.Size(121, 17);
            this.cutCopyWholeLineCheckBox.TabIndex = 3;
            this.cutCopyWholeLineCheckBox.Text = "&Cut/Copy whole line";
            this.toolTip.SetToolTip(this.cutCopyWholeLineCheckBox, "When no text is selected, the entire line of text is affected by the cut/copy ope" +
        "ration");
            this.cutCopyWholeLineCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.flowLayoutPanel11);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox12.Location = new System.Drawing.Point(3, 295);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(379, 113);
            this.groupBox12.TabIndex = 4;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Whitespace";
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.tabsToSpacesCheckBox);
            this.flowLayoutPanel11.Controls.Add(this.showWhitespaceCheckBox);
            this.flowLayoutPanel11.Controls.Add(this.showEndOfLineCheckBox);
            this.flowLayoutPanel11.Controls.Add(this.virtualSpaceCheckBox);
            this.flowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel11.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(373, 94);
            this.flowLayoutPanel11.TabIndex = 15;
            // 
            // tabsToSpacesCheckBox
            // 
            this.tabsToSpacesCheckBox.AutoSize = true;
            this.tabsToSpacesCheckBox.Checked = global::SFMTK.Properties.Settings.Default.TextEditorConvertTabsToSpaces;
            this.tabsToSpacesCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "TextEditorConvertTabsToSpaces", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tabsToSpacesCheckBox.Location = new System.Drawing.Point(3, 3);
            this.tabsToSpacesCheckBox.Name = "tabsToSpacesCheckBox";
            this.tabsToSpacesCheckBox.Size = new System.Drawing.Size(135, 17);
            this.tabsToSpacesCheckBox.TabIndex = 0;
            this.tabsToSpacesCheckBox.Text = "Convert &tabs to spaces";
            this.toolTip.SetToolTip(this.tabsToSpacesCheckBox, "Tabs for indentation will be replaced with spaces instead");
            this.tabsToSpacesCheckBox.UseVisualStyleBackColor = true;
            // 
            // showWhitespaceCheckBox
            // 
            this.showWhitespaceCheckBox.AutoSize = true;
            this.showWhitespaceCheckBox.Checked = global::SFMTK.Properties.Settings.Default.TextEditorShowWhitespace;
            this.showWhitespaceCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "TextEditorShowWhitespace", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.showWhitespaceCheckBox.Location = new System.Drawing.Point(3, 26);
            this.showWhitespaceCheckBox.Name = "showWhitespaceCheckBox";
            this.showWhitespaceCheckBox.Size = new System.Drawing.Size(110, 17);
            this.showWhitespaceCheckBox.TabIndex = 1;
            this.showWhitespaceCheckBox.Text = "Show &whitespace";
            this.toolTip.SetToolTip(this.showWhitespaceCheckBox, "Shows tabs and spaces in text using special characters");
            this.showWhitespaceCheckBox.UseVisualStyleBackColor = true;
            // 
            // showEndOfLineCheckBox
            // 
            this.showEndOfLineCheckBox.AutoSize = true;
            this.showEndOfLineCheckBox.Checked = global::SFMTK.Properties.Settings.Default.TextEditorShowEndOfLine;
            this.showEndOfLineCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "TextEditorShowEndOfLine", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.showEndOfLineCheckBox.Location = new System.Drawing.Point(3, 49);
            this.showEndOfLineCheckBox.Name = "showEndOfLineCheckBox";
            this.showEndOfLineCheckBox.Size = new System.Drawing.Size(105, 17);
            this.showEndOfLineCheckBox.TabIndex = 2;
            this.showEndOfLineCheckBox.Text = "Show &end of line";
            this.toolTip.SetToolTip(this.showEndOfLineCheckBox, "Draws a character for the end of a line");
            this.showEndOfLineCheckBox.UseVisualStyleBackColor = true;
            // 
            // virtualSpaceCheckBox
            // 
            this.virtualSpaceCheckBox.AutoSize = true;
            this.virtualSpaceCheckBox.Checked = global::SFMTK.Properties.Settings.Default.TextEditorEnableVirtualSpace;
            this.virtualSpaceCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "TextEditorEnableVirtualSpace", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.virtualSpaceCheckBox.Location = new System.Drawing.Point(3, 72);
            this.virtualSpaceCheckBox.Name = "virtualSpaceCheckBox";
            this.virtualSpaceCheckBox.Size = new System.Drawing.Size(122, 17);
            this.virtualSpaceCheckBox.TabIndex = 3;
            this.virtualSpaceCheckBox.Text = "Enable &virtual space";
            this.toolTip.SetToolTip(this.virtualSpaceCheckBox, "Enables virtual space mode. Clicking outside of the line will add spaces accordin" +
        "gly when entering more characters");
            this.virtualSpaceCheckBox.UseVisualStyleBackColor = true;
            // 
            // TextEditorPreferencePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainFlowLayoutPanel);
            this.Name = "TextEditorPreferencePage";
            this.Size = new System.Drawing.Size(385, 421);
            this.mainFlowLayoutPanel.ResumeLayout(false);
            this.mainFlowLayoutPanel.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel mainFlowLayoutPanel;
        private InvisibleControl invisibleControl4;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button templateEditorButton;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button syntaxHighlightingButton;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox highlightCurrentLineCheckBox;
        private System.Windows.Forms.CheckBox scrollBelowDocumentCheckBox;
        private System.Windows.Forms.CheckBox cutCopyWholeLineCheckBox;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.CheckBox tabsToSpacesCheckBox;
        private System.Windows.Forms.CheckBox showWhitespaceCheckBox;
        private System.Windows.Forms.CheckBox showEndOfLineCheckBox;
        private System.Windows.Forms.CheckBox virtualSpaceCheckBox;
        private System.Windows.Forms.ToolTip toolTip;
    }
}