﻿using SFMTK.Forms;
using SFMTK.Properties;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SFMTK.Controls.PreferencePages
{
    public partial class AssociationsPreferencePage : PreferencePage
    {
        //BUG: AssociationsPreferencePage - get the sfmtkDmxFileActionButton to stay in the flow, whilst avoiding the RadioGroupPanel ordering issues
        //  -> (should be next to the bottom-most SFMTK option)
        //NYI: AssociationsPreferencePage - content type associations (open/analyse with SFMTK, view mdl in modelviewer)
        //TODO: AssociationsPreferencePage - other possible integration settings (see program.cs todos about shell integrating)
        //  -> could hook into SFM <-> SFMTK using python perhaps

        public AssociationsPreferencePage()
        {
        }

        public override string Path => "Integration/File Associations";

        public override Image Image => Resources.computer_edit;

        public override void OnFirstSelect(object sender, EventArgs e)
        {
            InitializeComponent();

            //Combobox settings
            this.sfmDmxFileSetupComboBox.DisplayMember = "Name";
            this.sfmDmxFileSetupComboBox.ValueMember = null;

            //Populate setup list
            this.sfmDmxFileSetupComboBox.Items.Add(SetupSelection.Active);
            this.sfmDmxFileSetupComboBox.Items.Add(SetupSelection.Prompt);
            this.sfmDmxFileSetupComboBox.Items.AddRange(Settings.Default.Setups.ToArray());

            //Data bindings
            var dmxassocbinding = new CastBinding<DMXAssociation, int>("Selected", Settings.Default, "DMXAssoc", DataSourceUpdateMode.OnPropertyChanged);
            this.dmxAssocRadioGroupPanel.DataBindings.Add(dmxassocbinding);
            var sfmassocbinding = new CastBinding<SFMAssociation, int>("Selected", Settings.Default, "SFMAssoc", DataSourceUpdateMode.OnPropertyChanged);
            this.sfmAssocRadioGroupPanel.DataBindings.Add(sfmassocbinding);
            var sfmtksfmfilebinding = new CastBinding<SFMFileSFMTKOptions, int>("SelectedIndex", Settings.Default, "SFMFileSFMTKAction", DataSourceUpdateMode.OnPropertyChanged);
            this.sfmtkSfmFileActionComboBox.DataBindings.Add(sfmtksfmfilebinding);
            var dmxfilesfmsetupbinding = new Binding("SelectedItem", Settings.Default, "DMXFileSFMSetup", true, DataSourceUpdateMode.OnPropertyChanged);
            dmxfilesfmsetupbinding.Format += this.DmxFileSFMSetupBinding_Format;
            dmxfilesfmsetupbinding.Parse += this.DmxFileSFMSetupBinding_Parse;
            this.sfmDmxFileSetupComboBox.DataBindings.Add(dmxfilesfmsetupbinding);

            //Update association strings
            var sfmtype = IOHelper.FileTypeFromExtension(Constants.SFMTKExtension);
            if (sfmtype != null)
            {
                var assocsfm = sfmtype.ContainsIgnoreCase("SFMTK")
                    ? Settings.Default.SFMPreviousAssociation
                    : IOHelper.GetAssociatedProgram(sfmtype, true);
                if (!string.IsNullOrWhiteSpace(assocsfm))
                {
                    //Update name and selection
                    var exe = GetShortExe(assocsfm);
                    this.currentSfmFileRadioButton.Text = $"Previous association ({exe})";
                }
            }

            var dmxtype = IOHelper.FileTypeFromExtension(Constants.DMXExtension);
            if (dmxtype != null)
            {
                var assocdmx = dmxtype.ContainsIgnoreCase("SFMTK")
                    ? Settings.Default.DMXPreviousAssociation
                    : IOHelper.GetAssociatedProgram(dmxtype, true);
                if (!string.IsNullOrWhiteSpace(assocdmx))
                {
                    //Update name and selection
                    var exe = GetShortExe(assocdmx);
                    this.currentDmxFileRadioButton.Text = $"Previous association ({exe})";
                }
            }

            base.OnFirstSelect(sender, e);



            string GetShortExe(string path)
            {
                return System.IO.Path.GetFileName(path.Replace("\"", string.Empty).Replace("%1", string.Empty));
            }
        }

        private void DmxFileSFMSetupBinding_Format(object sender, ConvertEventArgs e)
        {
            var str = e.Value as string;
            if (str == Constants.SetupSelectionPrompt)
                e.Value = this.sfmDmxFileSetupComboBox.Items.OfType<SetupSelection>().FirstOrDefault(ss => ss.Value == Constants.SetupSelectionPrompt); //SetupSelection.Prompt;
            else if (str == Constants.SetupSelectionActive)
                e.Value = this.sfmDmxFileSetupComboBox.Items.OfType<SetupSelection>().FirstOrDefault(ss => ss.Value == Constants.SetupSelectionActive); //SetupSelection.Active;
            else
            {
                //Check setup
                var setup = this.sfmDmxFileSetupComboBox.Items.OfType<Setup>().FirstOrDefault(s => s.Name == str);
                e.Value = setup;
            }
        }

        private void DmxFileSFMSetupBinding_Parse(object sender, ConvertEventArgs e)
        {
            if (e.Value is SetupSelection ss)
            {
                //Placeholder value
                e.Value = ss.Value;
            }
            else if (e.Value is Setup setup)
            {
                //Setup selected
                e.Value = setup.Name;
            }
            else
            {
                throw new ArgumentNullException(nameof(e.Value));
            }
        }

        private void sfmtkSfmFileActionButton_Click(object sender, EventArgs e)
        {
            var diag = new SFMTKLaunchOptions(Constants.SFMTKExtension, Settings.Default.SFMFileSFMTKParameters);
            if (diag.ShowDialog(this) == DialogResult.OK)
            {
                //Apply new parameters
                Settings.Default.SFMFileSFMTKParameters = diag.LaunchOptions;
            }
        }

        private void sfmDmxFileSetupButton_Click(object sender, EventArgs e)
        {
            //NYI: AssociationsPreferencePage - grey out button if prompt dialog option is selected
            if (this.sfmDmxFileSetupComboBox.SelectedItem is SetupSelection ss)
            {
                //Check which selection it is
                if (ss.Value == SetupSelection.Active.Value)
                {
                    //Configure active setup
                    new EditSetup(Setup.Current).ShowDialog(this);
                }

                //Can not configure "prompt" dialog
            }
            else if (this.sfmDmxFileSetupComboBox.SelectedItem is Setup setup)
            {
                //Configure setup
                new EditSetup(setup).ShowDialog(this);
            }
        }

        private void sfmtkDmxFileActionButton_Click(object sender, EventArgs e)
        {
            var diag = new SFMTKLaunchOptions(Constants.DMXExtension, Settings.Default.DMXFileSFMTKParameters);
            if (diag.ShowDialog(this) == DialogResult.OK)
            {
                //Apply new parameters
                Settings.Default.DMXFileSFMTKParameters = diag.LaunchOptions;
            }
        }
    }
}
