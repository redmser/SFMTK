﻿using System.Drawing;

namespace SFMTK.Controls.PreferencePages
{
    public interface IPreferencePage
    {
        /// <summary>
        /// Gets the display text in the list.
        /// </summary>
        string Text { get; }

        /// <summary>
        /// Gets the path to this preference page. Use slashes '/' to signify nested pages.
        /// </summary>
        string Path { get; }

        /// <summary>
        /// Gets an optional image to associate to the preference page.
        /// </summary>
        Image Image { get; }

        /// <summary>
        /// Gets the path to the page to select.
        /// </summary>
        string TargetPage { get; }

        /// <summary>
        /// Gets or sets whether this element is a root element in the tree view.
        /// </summary>
        bool IsRoot { get; set; }

        /// <summary>
        /// Gets or sets whether this node should be visible after filtering through a search.
        /// </summary>
        bool Filter { get; set; }
    }
}
