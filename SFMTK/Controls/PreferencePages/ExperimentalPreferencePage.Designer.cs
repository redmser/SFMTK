﻿namespace SFMTK.Controls.PreferencePages
{
    partial class ExperimentalPreferencePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExperimentalPreferencePage));
            this.experimentalLabel = new System.Windows.Forms.Label();
            this.mainFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.multipleDocksCheckBox = new System.Windows.Forms.CheckBox();
            this.allowBesidesSFMCheckBox = new System.Windows.Forms.CheckBox();
            this.configFileButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.mainFlowLayoutPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // experimentalLabel
            // 
            this.experimentalLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.experimentalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.experimentalLabel.Location = new System.Drawing.Point(0, 0);
            this.experimentalLabel.Name = "experimentalLabel";
            this.experimentalLabel.Size = new System.Drawing.Size(506, 25);
            this.experimentalLabel.TabIndex = 1;
            this.experimentalLabel.Text = "These settings are experimental and may break things! Use at your own risk.";
            this.experimentalLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip.SetToolTip(this.experimentalLabel, "Seriously, most of these are not tested much and can induce pain and suffering.");
            // 
            // mainFlowLayoutPanel
            // 
            this.mainFlowLayoutPanel.AutoScroll = true;
            this.mainFlowLayoutPanel.Controls.Add(this.multipleDocksCheckBox);
            this.mainFlowLayoutPanel.Controls.Add(this.allowBesidesSFMCheckBox);
            this.mainFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.mainFlowLayoutPanel.Location = new System.Drawing.Point(0, 25);
            this.mainFlowLayoutPanel.Name = "mainFlowLayoutPanel";
            this.mainFlowLayoutPanel.Size = new System.Drawing.Size(506, 103);
            this.mainFlowLayoutPanel.TabIndex = 2;
            this.mainFlowLayoutPanel.WrapContents = false;
            // 
            // multipleDocksCheckBox
            // 
            this.multipleDocksCheckBox.AutoSize = true;
            this.multipleDocksCheckBox.Checked = global::SFMTK.Properties.Settings.Default.MultipleDocks;
            this.multipleDocksCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "MultipleDocks", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.multipleDocksCheckBox.Location = new System.Drawing.Point(3, 3);
            this.multipleDocksCheckBox.Name = "multipleDocksCheckBox";
            this.multipleDocksCheckBox.Size = new System.Drawing.Size(184, 17);
            this.multipleDocksCheckBox.TabIndex = 0;
            this.multipleDocksCheckBox.Text = "Allow &multiple docks of same type";
            this.toolTip.SetToolTip(this.multipleDocksCheckBox, "Allows the same type of window to be opened multiple times.\r\nMight cause issues w" +
        "ith syncing data for certain docks!");
            this.multipleDocksCheckBox.UseVisualStyleBackColor = true;
            // 
            // allowBesidesSFMCheckBox
            // 
            this.allowBesidesSFMCheckBox.AutoSize = true;
            this.allowBesidesSFMCheckBox.Checked = global::SFMTK.Properties.Settings.Default.AllowBesidesSFM;
            this.allowBesidesSFMCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "AllowBesidesSFM", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.allowBesidesSFMCheckBox.Location = new System.Drawing.Point(3, 26);
            this.allowBesidesSFMCheckBox.Name = "allowBesidesSFMCheckBox";
            this.allowBesidesSFMCheckBox.Size = new System.Drawing.Size(151, 17);
            this.allowBesidesSFMCheckBox.TabIndex = 1;
            this.allowBesidesSFMCheckBox.Text = "Allow &outside SFM context";
            this.toolTip.SetToolTip(this.allowBesidesSFMCheckBox, resources.GetString("allowBesidesSFMCheckBox.ToolTip"));
            this.allowBesidesSFMCheckBox.UseVisualStyleBackColor = true;
            // 
            // configFileButton
            // 
            this.configFileButton.Location = new System.Drawing.Point(0, 0);
            this.configFileButton.Name = "configFileButton";
            this.configFileButton.Size = new System.Drawing.Size(129, 23);
            this.configFileButton.TabIndex = 3;
            this.configFileButton.Text = "&Browse user.config file";
            this.toolTip.SetToolTip(this.configFileButton, "Opens the user.config file in the explorer, which stores your application setting" +
        "s.");
            this.configFileButton.UseVisualStyleBackColor = true;
            this.configFileButton.Click += new System.EventHandler(this.configFileButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.configFileButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 128);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(506, 25);
            this.panel1.TabIndex = 3;
            // 
            // ExperimentalPreferencePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainFlowLayoutPanel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.experimentalLabel);
            this.Name = "ExperimentalPreferencePage";
            this.Size = new System.Drawing.Size(506, 153);
            this.mainFlowLayoutPanel.ResumeLayout(false);
            this.mainFlowLayoutPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label experimentalLabel;
        private System.Windows.Forms.FlowLayoutPanel mainFlowLayoutPanel;
        private System.Windows.Forms.CheckBox multipleDocksCheckBox;
        private System.Windows.Forms.CheckBox allowBesidesSFMCheckBox;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button configFileButton;
        private System.Windows.Forms.Panel panel1;
    }
}