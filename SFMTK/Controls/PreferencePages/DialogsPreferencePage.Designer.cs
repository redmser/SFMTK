﻿namespace SFMTK.Controls.PreferencePages
{
    partial class DialogsPreferencePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.autoUnpinHLMVCheckBox = new System.Windows.Forms.CheckBox();
            this.forceFallbackCheckBox = new System.Windows.Forms.CheckBox();
            this.dialogsGroupBox = new System.Windows.Forms.GroupBox();
            this.dialogStatesDataListView = new BrightIdeasSoftware.DataListView();
            this.dialogNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.dialogStateColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.dialogsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeDialogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchDialogsToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.searchDialogsToolStripTextBox = new SFMTK.Controls.ToolStripSearchTextBox();
            this.resetAllDialogsToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.mainFlowLayoutPanel.SuspendLayout();
            this.dialogsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dialogStatesDataListView)).BeginInit();
            this.dialogsContextMenuStrip.SuspendLayout();
            this.searchDialogsToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainFlowLayoutPanel
            // 
            this.mainFlowLayoutPanel.AutoScroll = true;
            this.mainFlowLayoutPanel.AutoSize = true;
            this.mainFlowLayoutPanel.Controls.Add(this.autoUnpinHLMVCheckBox);
            this.mainFlowLayoutPanel.Controls.Add(this.forceFallbackCheckBox);
            this.mainFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.mainFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.mainFlowLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainFlowLayoutPanel.Name = "mainFlowLayoutPanel";
            this.mainFlowLayoutPanel.Size = new System.Drawing.Size(467, 46);
            this.mainFlowLayoutPanel.TabIndex = 5;
            this.mainFlowLayoutPanel.WrapContents = false;
            // 
            // autoUnpinHLMVCheckBox
            // 
            this.autoUnpinHLMVCheckBox.AutoSize = true;
            this.autoUnpinHLMVCheckBox.Checked = global::SFMTK.Properties.Settings.Default.AutoUnpinHLMV;
            this.autoUnpinHLMVCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "AutoUnpinHLMV", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.autoUnpinHLMVCheckBox.Location = new System.Drawing.Point(3, 3);
            this.autoUnpinHLMVCheckBox.Name = "autoUnpinHLMVCheckBox";
            this.autoUnpinHLMVCheckBox.Size = new System.Drawing.Size(164, 17);
            this.autoUnpinHLMVCheckBox.TabIndex = 0;
            this.autoUnpinHLMVCheckBox.Text = "&Unpin Model Viewer on close";
            this.toolTip.SetToolTip(this.autoUnpinHLMVCheckBox, "When closing a pinned Model Viewer widget, the window inside should unpin instead" +
        " of closing with it.");
            this.autoUnpinHLMVCheckBox.UseVisualStyleBackColor = true;
            // 
            // forceFallbackCheckBox
            // 
            this.forceFallbackCheckBox.AutoSize = true;
            this.forceFallbackCheckBox.Checked = global::SFMTK.Properties.Settings.Default.ForceFallbackDialogs;
            this.forceFallbackCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "ForceFallbackDialogs", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.forceFallbackCheckBox.Location = new System.Drawing.Point(3, 26);
            this.forceFallbackCheckBox.Name = "forceFallbackCheckBox";
            this.forceFallbackCheckBox.Size = new System.Drawing.Size(161, 17);
            this.forceFallbackCheckBox.TabIndex = 1;
            this.forceFallbackCheckBox.Text = "Force &simple message boxes";
            this.toolTip.SetToolTip(this.forceFallbackCheckBox, "If you have problems with message boxes, enable this option.\r\nNote that certain d" +
        "ialog box features, such as \"remember my decision\" or detailed information, do n" +
        "ot work with this enabled.");
            this.forceFallbackCheckBox.UseVisualStyleBackColor = true;
            // 
            // dialogsGroupBox
            // 
            this.dialogsGroupBox.Controls.Add(this.dialogStatesDataListView);
            this.dialogsGroupBox.Controls.Add(this.searchDialogsToolStrip);
            this.dialogsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dialogsGroupBox.Location = new System.Drawing.Point(0, 46);
            this.dialogsGroupBox.Name = "dialogsGroupBox";
            this.dialogsGroupBox.Size = new System.Drawing.Size(467, 278);
            this.dialogsGroupBox.TabIndex = 0;
            this.dialogsGroupBox.TabStop = false;
            this.dialogsGroupBox.Text = "Remembered actions";
            // 
            // dialogStatesDataListView
            // 
            this.dialogStatesDataListView.AllColumns.Add(this.dialogNameColumn);
            this.dialogStatesDataListView.AllColumns.Add(this.dialogStateColumn);
            this.dialogStatesDataListView.AutoGenerateColumns = false;
            this.dialogStatesDataListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dialogStatesDataListView.CellEditUseWholeCell = false;
            this.dialogStatesDataListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dialogNameColumn,
            this.dialogStateColumn});
            this.dialogStatesDataListView.ContextMenuStrip = this.dialogsContextMenuStrip;
            this.dialogStatesDataListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.dialogStatesDataListView.DataSource = null;
            this.dialogStatesDataListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dialogStatesDataListView.EmptyListMsg = "This list will show which action will be taken on any dialog you selected a \"reme" +
    "mber this decision\" checkbox or similar.";
            this.dialogStatesDataListView.FullRowSelect = true;
            this.dialogStatesDataListView.HideSelection = false;
            this.dialogStatesDataListView.Location = new System.Drawing.Point(3, 16);
            this.dialogStatesDataListView.Name = "dialogStatesDataListView";
            this.dialogStatesDataListView.ShowGroups = false;
            this.dialogStatesDataListView.Size = new System.Drawing.Size(461, 234);
            this.dialogStatesDataListView.TabIndex = 0;
            this.dialogStatesDataListView.UseCompatibleStateImageBehavior = false;
            this.dialogStatesDataListView.UseFiltering = true;
            this.dialogStatesDataListView.View = System.Windows.Forms.View.Details;
            // 
            // dialogNameColumn
            // 
            this.dialogNameColumn.AspectName = "Identifier";
            this.dialogNameColumn.FillsFreeSpace = true;
            this.dialogNameColumn.Text = "Dialog Name";
            this.dialogNameColumn.UseFiltering = false;
            this.dialogNameColumn.Width = 320;
            // 
            // dialogStateColumn
            // 
            this.dialogStateColumn.AspectName = "Label";
            this.dialogStateColumn.Text = "Default Action";
            this.dialogStateColumn.Width = 90;
            // 
            // dialogsContextMenuStrip
            // 
            this.dialogsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeDialogToolStripMenuItem});
            this.dialogsContextMenuStrip.Name = "shortcutContextMenuStrip";
            this.dialogsContextMenuStrip.Size = new System.Drawing.Size(153, 48);
            this.dialogsContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.dialogsContextMenuStrip_Opening);
            // 
            // removeDialogToolStripMenuItem
            // 
            this.removeDialogToolStripMenuItem.Image = global::SFMTK.Properties.Resources.arrow_undo;
            this.removeDialogToolStripMenuItem.Name = "removeDialogToolStripMenuItem";
            this.removeDialogToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.removeDialogToolStripMenuItem.Text = "Remove";
            this.removeDialogToolStripMenuItem.Click += new System.EventHandler(this.removeDialogToolStripMenuItem_Click);
            // 
            // searchDialogsToolStrip
            // 
            this.searchDialogsToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.searchDialogsToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.searchDialogsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.searchDialogsToolStripTextBox,
            this.resetAllDialogsToolStripButton,
            this.toolStripSeparator2});
            this.searchDialogsToolStrip.Location = new System.Drawing.Point(3, 250);
            this.searchDialogsToolStrip.Name = "searchDialogsToolStrip";
            this.searchDialogsToolStrip.Size = new System.Drawing.Size(461, 25);
            this.searchDialogsToolStrip.TabIndex = 1;
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(44, 22);
            this.toolStripLabel2.Text = "&Search:";
            // 
            // searchDialogsToolStripTextBox
            // 
            this.searchDialogsToolStripTextBox.ActiveTheme = null;
            this.searchDialogsToolStripTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchDialogsToolStripTextBox.ClearImage = null;
            this.searchDialogsToolStripTextBox.Name = "searchDialogsToolStripTextBox";
            this.searchDialogsToolStripTextBox.SearchImage = null;
            this.searchDialogsToolStripTextBox.Size = new System.Drawing.Size(170, 25);
            this.searchDialogsToolStripTextBox.TextChanged += new System.EventHandler(this.searchDialogsToolStripTextBox_TextChanged);
            // 
            // resetAllDialogsToolStripButton
            // 
            this.resetAllDialogsToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.resetAllDialogsToolStripButton.BackColor = System.Drawing.SystemColors.Window;
            this.resetAllDialogsToolStripButton.Enabled = false;
            this.resetAllDialogsToolStripButton.Image = global::SFMTK.Properties.Resources.arrow_undo_star;
            this.resetAllDialogsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.resetAllDialogsToolStripButton.Name = "resetAllDialogsToolStripButton";
            this.resetAllDialogsToolStripButton.Size = new System.Drawing.Size(79, 22);
            this.resetAllDialogsToolStripButton.Text = "&Remove all";
            this.resetAllDialogsToolStripButton.ToolTipText = "Removes all remembered dialog actions";
            this.resetAllDialogsToolStripButton.Click += new System.EventHandler(this.resetAllDialogsToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // DialogsPreferencePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dialogsGroupBox);
            this.Controls.Add(this.mainFlowLayoutPanel);
            this.Name = "DialogsPreferencePage";
            this.Size = new System.Drawing.Size(467, 324);
            this.mainFlowLayoutPanel.ResumeLayout(false);
            this.mainFlowLayoutPanel.PerformLayout();
            this.dialogsGroupBox.ResumeLayout(false);
            this.dialogsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dialogStatesDataListView)).EndInit();
            this.dialogsContextMenuStrip.ResumeLayout(false);
            this.searchDialogsToolStrip.ResumeLayout(false);
            this.searchDialogsToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel mainFlowLayoutPanel;
        private System.Windows.Forms.CheckBox autoUnpinHLMVCheckBox;
        private System.Windows.Forms.CheckBox forceFallbackCheckBox;
        private System.Windows.Forms.GroupBox dialogsGroupBox;
        private BrightIdeasSoftware.DataListView dialogStatesDataListView;
        private BrightIdeasSoftware.OLVColumn dialogNameColumn;
        private BrightIdeasSoftware.OLVColumn dialogStateColumn;
        private System.Windows.Forms.ToolStrip searchDialogsToolStrip;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private ToolStripSearchTextBox searchDialogsToolStripTextBox;
        private System.Windows.Forms.ToolStripButton resetAllDialogsToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ContextMenuStrip dialogsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem removeDialogToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
    }
}