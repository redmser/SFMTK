﻿namespace SFMTK.Controls.PreferencePages
{
    partial class AssociationsPreferencePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.invisibleControl6 = new SFMTK.Controls.InvisibleControl();
            this.label4 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.sfmFileLabel = new System.Windows.Forms.Label();
            this.sfmAssocRadioGroupPanel = new SFMTK.Controls.RadioGroupPanel();
            this.currentSfmFileRadioButton = new System.Windows.Forms.RadioButton();
            this.sfmtkSfmFileRadioButton = new System.Windows.Forms.RadioButton();
            this.panel9 = new System.Windows.Forms.Panel();
            this.sfmtkSfmFileActionButton = new System.Windows.Forms.Button();
            this.sfmtkSfmFileActionComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.dmxFileLabel = new System.Windows.Forms.Label();
            this.dmxAssocRadioGroupPanel = new SFMTK.Controls.RadioGroupPanel();
            this.currentDmxFileRadioButton = new System.Windows.Forms.RadioButton();
            this.askDmxFileRadioButton = new System.Windows.Forms.RadioButton();
            this.sfmDmxFileRadioButton = new System.Windows.Forms.RadioButton();
            this.panel11 = new System.Windows.Forms.Panel();
            this.sfmDmxFileSetupButton = new System.Windows.Forms.Button();
            this.sfmDmxFileSetupComboBox = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.sfmtkDmxFileRadioButton = new System.Windows.Forms.RadioButton();
            this.sfmtkDmxFileActionButton = new System.Windows.Forms.Button();
            this.dmxInfoLabel = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.flowLayoutPanel13.SuspendLayout();
            this.sfmAssocRadioGroupPanel.SuspendLayout();
            this.panel9.SuspendLayout();
            this.dmxAssocRadioGroupPanel.SuspendLayout();
            this.panel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.AutoScroll = true;
            this.flowLayoutPanel13.Controls.Add(this.invisibleControl6);
            this.flowLayoutPanel13.Controls.Add(this.label4);
            this.flowLayoutPanel13.Controls.Add(this.panel8);
            this.flowLayoutPanel13.Controls.Add(this.sfmFileLabel);
            this.flowLayoutPanel13.Controls.Add(this.sfmAssocRadioGroupPanel);
            this.flowLayoutPanel13.Controls.Add(this.label1);
            this.flowLayoutPanel13.Controls.Add(this.panel7);
            this.flowLayoutPanel13.Controls.Add(this.dmxFileLabel);
            this.flowLayoutPanel13.Controls.Add(this.dmxAssocRadioGroupPanel);
            this.flowLayoutPanel13.Controls.Add(this.sfmtkDmxFileActionButton);
            this.flowLayoutPanel13.Controls.Add(this.dmxInfoLabel);
            this.flowLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel13.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(642, 406);
            this.flowLayoutPanel13.TabIndex = 17;
            this.flowLayoutPanel13.WrapContents = false;
            // 
            // invisibleControl6
            // 
            this.invisibleControl6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.invisibleControl6.Location = new System.Drawing.Point(0, 0);
            this.invisibleControl6.Name = "invisibleControl6";
            this.invisibleControl6.Size = new System.Drawing.Size(642, 0);
            this.invisibleControl6.TabIndex = 16;
            this.invisibleControl6.TabStop = false;
            this.invisibleControl6.Text = "invisibleControl6";
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(636, 32);
            this.label4.TabIndex = 0;
            this.label4.Text = "These settings change which programs are used to open certain file types related " +
    "to SFM:";
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 38);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(636, 4);
            this.panel8.TabIndex = 15;
            // 
            // sfmFileLabel
            // 
            this.sfmFileLabel.AutoSize = true;
            this.sfmFileLabel.Location = new System.Drawing.Point(3, 45);
            this.sfmFileLabel.Name = "sfmFileLabel";
            this.sfmFileLabel.Size = new System.Drawing.Size(127, 13);
            this.sfmFileLabel.TabIndex = 1;
            this.sfmFileLabel.Text = "Associate .&sfm files with...";
            // 
            // sfmAssocRadioGroupPanel
            // 
            this.sfmAssocRadioGroupPanel.Controls.Add(this.currentSfmFileRadioButton);
            this.sfmAssocRadioGroupPanel.Controls.Add(this.sfmtkSfmFileRadioButton);
            this.sfmAssocRadioGroupPanel.Controls.Add(this.panel9);
            this.sfmAssocRadioGroupPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.sfmAssocRadioGroupPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.sfmAssocRadioGroupPanel.Location = new System.Drawing.Point(3, 61);
            this.sfmAssocRadioGroupPanel.Name = "sfmAssocRadioGroupPanel";
            this.sfmAssocRadioGroupPanel.Selected = 0;
            this.sfmAssocRadioGroupPanel.Size = new System.Drawing.Size(636, 72);
            this.sfmAssocRadioGroupPanel.TabIndex = 7;
            // 
            // currentSfmFileRadioButton
            // 
            this.currentSfmFileRadioButton.AutoSize = true;
            this.currentSfmFileRadioButton.Checked = true;
            this.currentSfmFileRadioButton.Location = new System.Drawing.Point(2, 2);
            this.currentSfmFileRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.currentSfmFileRadioButton.Name = "currentSfmFileRadioButton";
            this.currentSfmFileRadioButton.Size = new System.Drawing.Size(155, 17);
            this.currentSfmFileRadioButton.TabIndex = 0;
            this.currentSfmFileRadioButton.TabStop = true;
            this.currentSfmFileRadioButton.Tag = "0";
            this.currentSfmFileRadioButton.Text = "&Previous association (none)";
            this.toolTip.SetToolTip(this.currentSfmFileRadioButton, "The program that opens .sfm files will not be changed from your previous setting." +
        "");
            this.currentSfmFileRadioButton.UseVisualStyleBackColor = true;
            // 
            // sfmtkSfmFileRadioButton
            // 
            this.sfmtkSfmFileRadioButton.AutoSize = true;
            this.sfmtkSfmFileRadioButton.Location = new System.Drawing.Point(2, 23);
            this.sfmtkSfmFileRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.sfmtkSfmFileRadioButton.Name = "sfmtkSfmFileRadioButton";
            this.sfmtkSfmFileRadioButton.Size = new System.Drawing.Size(61, 17);
            this.sfmtkSfmFileRadioButton.TabIndex = 1;
            this.sfmtkSfmFileRadioButton.Tag = "1";
            this.sfmtkSfmFileRadioButton.Text = "&SFMTK";
            this.toolTip.SetToolTip(this.sfmtkSfmFileRadioButton, "SFMTK will be used to open .sfm files, allowing quick extraction or modification " +
        "of the content inside.");
            this.sfmtkSfmFileRadioButton.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.sfmtkSfmFileActionButton);
            this.panel9.Controls.Add(this.sfmtkSfmFileActionComboBox);
            this.panel9.Controls.Add(this.label12);
            this.panel9.Location = new System.Drawing.Point(3, 45);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(233, 24);
            this.panel9.TabIndex = 16;
            // 
            // sfmtkSfmFileActionButton
            // 
            this.sfmtkSfmFileActionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sfmtkSfmFileActionButton.Image = global::SFMTK.Properties.Resources.pencil;
            this.sfmtkSfmFileActionButton.Location = new System.Drawing.Point(208, 0);
            this.sfmtkSfmFileActionButton.Name = "sfmtkSfmFileActionButton";
            this.sfmtkSfmFileActionButton.Size = new System.Drawing.Size(24, 24);
            this.sfmtkSfmFileActionButton.TabIndex = 2;
            this.toolTip.SetToolTip(this.sfmtkSfmFileActionButton, "Configure how SFMTK is launched.");
            this.sfmtkSfmFileActionButton.UseVisualStyleBackColor = true;
            this.sfmtkSfmFileActionButton.Click += new System.EventHandler(this.sfmtkSfmFileActionButton_Click);
            // 
            // sfmtkSfmFileActionComboBox
            // 
            this.sfmtkSfmFileActionComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sfmtkSfmFileActionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sfmtkSfmFileActionComboBox.FormattingEnabled = true;
            this.sfmtkSfmFileActionComboBox.Items.AddRange(new object[] {
            "Open in SFMTK",
            "Install for SFM",
            "Ask every time"});
            this.sfmtkSfmFileActionComboBox.Location = new System.Drawing.Point(56, 2);
            this.sfmtkSfmFileActionComboBox.Name = "sfmtkSfmFileActionComboBox";
            this.sfmtkSfmFileActionComboBox.Size = new System.Drawing.Size(152, 21);
            this.sfmtkSfmFileActionComboBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.sfmtkSfmFileActionComboBox, "What to do with the SFM file when opened using SFMTK.");
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Action:";
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(3, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(636, 32);
            this.label1.TabIndex = 2;
            this.label1.Text = "SFM files are used by SFMTK to transfer or store content or mods, similarly to ZI" +
    "P archives.";
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(3, 171);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(636, 4);
            this.panel7.TabIndex = 14;
            // 
            // dmxFileLabel
            // 
            this.dmxFileLabel.AutoSize = true;
            this.dmxFileLabel.Location = new System.Drawing.Point(3, 178);
            this.dmxFileLabel.Name = "dmxFileLabel";
            this.dmxFileLabel.Size = new System.Drawing.Size(130, 13);
            this.dmxFileLabel.TabIndex = 3;
            this.dmxFileLabel.Text = "Associate .&dmx files with...";
            // 
            // dmxAssocRadioGroupPanel
            // 
            this.dmxAssocRadioGroupPanel.Controls.Add(this.currentDmxFileRadioButton);
            this.dmxAssocRadioGroupPanel.Controls.Add(this.askDmxFileRadioButton);
            this.dmxAssocRadioGroupPanel.Controls.Add(this.sfmDmxFileRadioButton);
            this.dmxAssocRadioGroupPanel.Controls.Add(this.panel11);
            this.dmxAssocRadioGroupPanel.Controls.Add(this.sfmtkDmxFileRadioButton);
            this.dmxAssocRadioGroupPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.dmxAssocRadioGroupPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.dmxAssocRadioGroupPanel.Location = new System.Drawing.Point(3, 194);
            this.dmxAssocRadioGroupPanel.Name = "dmxAssocRadioGroupPanel";
            this.dmxAssocRadioGroupPanel.Selected = 0;
            this.dmxAssocRadioGroupPanel.Size = new System.Drawing.Size(636, 124);
            this.dmxAssocRadioGroupPanel.TabIndex = 6;
            // 
            // currentDmxFileRadioButton
            // 
            this.currentDmxFileRadioButton.AutoSize = true;
            this.currentDmxFileRadioButton.Checked = true;
            this.currentDmxFileRadioButton.Location = new System.Drawing.Point(2, 2);
            this.currentDmxFileRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.currentDmxFileRadioButton.Name = "currentDmxFileRadioButton";
            this.currentDmxFileRadioButton.Size = new System.Drawing.Size(155, 17);
            this.currentDmxFileRadioButton.TabIndex = 0;
            this.currentDmxFileRadioButton.TabStop = true;
            this.currentDmxFileRadioButton.Tag = "0";
            this.currentDmxFileRadioButton.Text = "&Previous association (none)";
            this.toolTip.SetToolTip(this.currentDmxFileRadioButton, "The program that opens .dmx files will not be changed from your previous setting." +
        "");
            this.currentDmxFileRadioButton.UseVisualStyleBackColor = true;
            // 
            // askDmxFileRadioButton
            // 
            this.askDmxFileRadioButton.AutoSize = true;
            this.askDmxFileRadioButton.Location = new System.Drawing.Point(2, 23);
            this.askDmxFileRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.askDmxFileRadioButton.Name = "askDmxFileRadioButton";
            this.askDmxFileRadioButton.Size = new System.Drawing.Size(111, 17);
            this.askDmxFileRadioButton.TabIndex = 1;
            this.askDmxFileRadioButton.Tag = "3";
            this.askDmxFileRadioButton.Text = "&Ask when opened";
            this.toolTip.SetToolTip(this.askDmxFileRadioButton, "Asks every time a DMX file is opened. Allows you to select SFM\'s setup in this di" +
        "alog as well.");
            this.askDmxFileRadioButton.UseVisualStyleBackColor = true;
            // 
            // sfmDmxFileRadioButton
            // 
            this.sfmDmxFileRadioButton.AutoSize = true;
            this.sfmDmxFileRadioButton.Location = new System.Drawing.Point(2, 44);
            this.sfmDmxFileRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.sfmDmxFileRadioButton.Name = "sfmDmxFileRadioButton";
            this.sfmDmxFileRadioButton.Size = new System.Drawing.Size(109, 17);
            this.sfmDmxFileRadioButton.TabIndex = 2;
            this.sfmDmxFileRadioButton.Tag = "1";
            this.sfmDmxFileRadioButton.Text = "Source &Filmmaker";
            this.toolTip.SetToolTip(this.sfmDmxFileRadioButton, "Source Filmmaker will be used to open .dmx files, allowing you to quickly open mo" +
        "vie sessions.\r\nThis does not work for all types of DMX files!");
            this.sfmDmxFileRadioButton.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.sfmDmxFileSetupButton);
            this.panel11.Controls.Add(this.sfmDmxFileSetupComboBox);
            this.panel11.Controls.Add(this.label18);
            this.panel11.Location = new System.Drawing.Point(3, 66);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(233, 24);
            this.panel11.TabIndex = 18;
            // 
            // sfmDmxFileSetupButton
            // 
            this.sfmDmxFileSetupButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sfmDmxFileSetupButton.Image = global::SFMTK.Properties.Resources.computer_edit;
            this.sfmDmxFileSetupButton.Location = new System.Drawing.Point(208, 0);
            this.sfmDmxFileSetupButton.Name = "sfmDmxFileSetupButton";
            this.sfmDmxFileSetupButton.Size = new System.Drawing.Size(24, 24);
            this.sfmDmxFileSetupButton.TabIndex = 2;
            this.toolTip.SetToolTip(this.sfmDmxFileSetupButton, "Edit the selected setup.");
            this.sfmDmxFileSetupButton.UseVisualStyleBackColor = true;
            this.sfmDmxFileSetupButton.Click += new System.EventHandler(this.sfmDmxFileSetupButton_Click);
            // 
            // sfmDmxFileSetupComboBox
            // 
            this.sfmDmxFileSetupComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sfmDmxFileSetupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sfmDmxFileSetupComboBox.FormattingEnabled = true;
            this.sfmDmxFileSetupComboBox.Location = new System.Drawing.Point(56, 2);
            this.sfmDmxFileSetupComboBox.Name = "sfmDmxFileSetupComboBox";
            this.sfmDmxFileSetupComboBox.Size = new System.Drawing.Size(152, 21);
            this.sfmDmxFileSetupComboBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.sfmDmxFileSetupComboBox, "Which setup should be used to launch SFM with when opening DMX files.");
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 4);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Se&tup:";
            // 
            // sfmtkDmxFileRadioButton
            // 
            this.sfmtkDmxFileRadioButton.AutoSize = true;
            this.sfmtkDmxFileRadioButton.Location = new System.Drawing.Point(2, 95);
            this.sfmtkDmxFileRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.sfmtkDmxFileRadioButton.Name = "sfmtkDmxFileRadioButton";
            this.sfmtkDmxFileRadioButton.Size = new System.Drawing.Size(61, 17);
            this.sfmtkDmxFileRadioButton.TabIndex = 3;
            this.sfmtkDmxFileRadioButton.Tag = "2";
            this.sfmtkDmxFileRadioButton.Text = "&SFMTK";
            this.toolTip.SetToolTip(this.sfmtkDmxFileRadioButton, "SFMTK will be used to open .dmx files, allowing for quick analysis of their conte" +
        "nt.");
            this.sfmtkDmxFileRadioButton.UseVisualStyleBackColor = true;
            // 
            // sfmtkDmxFileActionButton
            // 
            this.sfmtkDmxFileActionButton.Image = global::SFMTK.Properties.Resources.pencil;
            this.sfmtkDmxFileActionButton.Location = new System.Drawing.Point(3, 324);
            this.sfmtkDmxFileActionButton.Name = "sfmtkDmxFileActionButton";
            this.sfmtkDmxFileActionButton.Size = new System.Drawing.Size(24, 24);
            this.sfmtkDmxFileActionButton.TabIndex = 4;
            this.toolTip.SetToolTip(this.sfmtkDmxFileActionButton, "Configure how SFMTK is launched.");
            this.sfmtkDmxFileActionButton.UseVisualStyleBackColor = true;
            this.sfmtkDmxFileActionButton.Click += new System.EventHandler(this.sfmtkDmxFileActionButton_Click);
            // 
            // dmxInfoLabel
            // 
            this.dmxInfoLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.dmxInfoLabel.Location = new System.Drawing.Point(3, 351);
            this.dmxInfoLabel.Name = "dmxInfoLabel";
            this.dmxInfoLabel.Size = new System.Drawing.Size(636, 48);
            this.dmxInfoLabel.TabIndex = 5;
            this.dmxInfoLabel.Text = "DMX files are used to store binary data. SFM can open them directly as movie sess" +
    "ions, while SFMTK can inspect their contents.";
            // 
            // AssociationsPreferencePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanel13);
            this.Name = "AssociationsPreferencePage";
            this.Size = new System.Drawing.Size(642, 406);
            this.flowLayoutPanel13.ResumeLayout(false);
            this.flowLayoutPanel13.PerformLayout();
            this.sfmAssocRadioGroupPanel.ResumeLayout(false);
            this.sfmAssocRadioGroupPanel.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.dmxAssocRadioGroupPanel.ResumeLayout(false);
            this.dmxAssocRadioGroupPanel.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private InvisibleControl invisibleControl6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label sfmFileLabel;
        private RadioGroupPanel sfmAssocRadioGroupPanel;
        private System.Windows.Forms.RadioButton currentSfmFileRadioButton;
        private System.Windows.Forms.RadioButton sfmtkSfmFileRadioButton;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button sfmtkSfmFileActionButton;
        private System.Windows.Forms.ComboBox sfmtkSfmFileActionComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label dmxFileLabel;
        private RadioGroupPanel dmxAssocRadioGroupPanel;
        private System.Windows.Forms.RadioButton currentDmxFileRadioButton;
        private System.Windows.Forms.RadioButton askDmxFileRadioButton;
        private System.Windows.Forms.RadioButton sfmDmxFileRadioButton;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button sfmDmxFileSetupButton;
        private System.Windows.Forms.ComboBox sfmDmxFileSetupComboBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RadioButton sfmtkDmxFileRadioButton;
        private System.Windows.Forms.Label dmxInfoLabel;
        private System.Windows.Forms.Button sfmtkDmxFileActionButton;
        private System.Windows.Forms.ToolTip toolTip;
    }
}