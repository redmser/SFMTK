﻿namespace SFMTK.Controls.PreferencePages
{
    partial class ModulePreferencePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModulePreferencePage));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.modulesTreeListView = new SFMTK.Controls.EnhancedTreeListView();
            this.moduleNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.moduleFullNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.moduleLoadedColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.moduleActionColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.moduleAuthorColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.moduleVersionColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.modulesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.installModulesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.enableSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disableSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uninstallSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.browseToModuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modulesToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.modulesToolStripSearchTextBox = new SFMTK.Controls.ToolStripSearchTextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.invisibleControl1 = new SFMTK.Controls.InvisibleControl();
            this.infoLabel = new System.Windows.Forms.Label();
            this.helpButton = new System.Windows.Forms.Button();
            this.installButton = new System.Windows.Forms.Button();
            this.enableButton = new System.Windows.Forms.Button();
            this.disableButton = new System.Windows.Forms.Button();
            this.uninstallButton = new System.Windows.Forms.Button();
            this.openFolderButton = new System.Windows.Forms.Button();
            this.commandManager = new SFMTK.Commands.CommandManager();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.modulesTreeListView)).BeginInit();
            this.modulesContextMenuStrip.SuspendLayout();
            this.modulesToolStrip.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.modulesTreeListView);
            this.splitContainer.Panel1.Controls.Add(this.modulesToolStrip);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.flowLayoutPanel1);
            this.splitContainer.Panel2.Controls.Add(this.openFolderButton);
            this.splitContainer.Size = new System.Drawing.Size(533, 466);
            this.splitContainer.SplitterDistance = 360;
            this.splitContainer.TabIndex = 0;
            // 
            // modulesTreeListView
            // 
            this.modulesTreeListView.AllColumns.Add(this.moduleNameColumn);
            this.modulesTreeListView.AllColumns.Add(this.moduleFullNameColumn);
            this.modulesTreeListView.AllColumns.Add(this.moduleLoadedColumn);
            this.modulesTreeListView.AllColumns.Add(this.moduleActionColumn);
            this.modulesTreeListView.AllColumns.Add(this.moduleAuthorColumn);
            this.modulesTreeListView.AllColumns.Add(this.moduleVersionColumn);
            this.modulesTreeListView.AllowDrop = true;
            this.modulesTreeListView.CellEditUseWholeCell = false;
            this.modulesTreeListView.CheckBoxes = true;
            this.modulesTreeListView.CheckedAspectName = "";
            this.modulesTreeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.moduleNameColumn,
            this.moduleAuthorColumn});
            this.modulesTreeListView.ContextMenuStrip = this.modulesContextMenuStrip;
            this.modulesTreeListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.modulesTreeListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modulesTreeListView.FullRowSelect = true;
            this.modulesTreeListView.HeaderCheckBoxUpdatesChildren = true;
            this.modulesTreeListView.HideSelection = false;
            this.modulesTreeListView.Location = new System.Drawing.Point(0, 0);
            this.modulesTreeListView.Name = "modulesTreeListView";
            this.modulesTreeListView.ShowGroups = false;
            this.modulesTreeListView.ShowImagesOnSubItems = true;
            this.modulesTreeListView.Size = new System.Drawing.Size(360, 441);
            this.modulesTreeListView.TabIndex = 0;
            this.modulesTreeListView.UseCompatibleStateImageBehavior = false;
            this.modulesTreeListView.UseFiltering = true;
            this.modulesTreeListView.View = System.Windows.Forms.View.Details;
            this.modulesTreeListView.VirtualMode = true;
            this.modulesTreeListView.CanDrop += new System.EventHandler<BrightIdeasSoftware.OlvDropEventArgs>(this.modulesObjectListView_CanDrop);
            this.modulesTreeListView.FormatRow += new System.EventHandler<BrightIdeasSoftware.FormatRowEventArgs>(this.modulesObjectListView_FormatRow);
            this.modulesTreeListView.SelectionChanged += new System.EventHandler(this.modulesObjectListView_SelectionChanged);
            this.modulesTreeListView.DragDrop += new System.Windows.Forms.DragEventHandler(this.modulesObjectListView_DragDrop);
            // 
            // moduleNameColumn
            // 
            this.moduleNameColumn.AspectName = "";
            this.moduleNameColumn.Text = "Name";
            this.moduleNameColumn.UseFiltering = false;
            this.moduleNameColumn.Width = 200;
            // 
            // moduleFullNameColumn
            // 
            this.moduleFullNameColumn.AspectName = "FullName";
            this.moduleFullNameColumn.DisplayIndex = 1;
            this.moduleFullNameColumn.IsVisible = false;
            this.moduleFullNameColumn.Text = "Full Name";
            this.moduleFullNameColumn.UseFiltering = false;
            this.moduleFullNameColumn.Width = 120;
            // 
            // moduleLoadedColumn
            // 
            this.moduleLoadedColumn.IsVisible = false;
            this.moduleLoadedColumn.MaximumWidth = 16;
            this.moduleLoadedColumn.Text = "";
            this.moduleLoadedColumn.ToolTipText = "Is Loaded?";
            this.moduleLoadedColumn.Width = 16;
            // 
            // moduleActionColumn
            // 
            this.moduleActionColumn.AspectName = "Action";
            this.moduleActionColumn.IsVisible = false;
            this.moduleActionColumn.Text = "Action";
            this.moduleActionColumn.Width = 75;
            // 
            // moduleAuthorColumn
            // 
            this.moduleAuthorColumn.AspectName = "";
            this.moduleAuthorColumn.Text = "Author";
            this.moduleAuthorColumn.Width = 90;
            // 
            // moduleVersionColumn
            // 
            this.moduleVersionColumn.IsVisible = false;
            this.moduleVersionColumn.Text = "Version";
            this.moduleVersionColumn.Width = 80;
            // 
            // modulesContextMenuStrip
            // 
            this.modulesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.installModulesToolStripMenuItem,
            this.toolStripSeparator1,
            this.enableSelectedToolStripMenuItem,
            this.disableSelectedToolStripMenuItem,
            this.uninstallSelectedToolStripMenuItem,
            this.toolStripSeparator2,
            this.browseToModuleToolStripMenuItem});
            this.modulesContextMenuStrip.Name = "modulesContextMenuStrip";
            this.modulesContextMenuStrip.Size = new System.Drawing.Size(210, 126);
            // 
            // installModulesToolStripMenuItem
            // 
            this.commandManager.SetCommand(this.installModulesToolStripMenuItem, "InstallModulesCommand");
            this.installModulesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("installModulesToolStripMenuItem.Image")));
            this.installModulesToolStripMenuItem.Name = "installModulesToolStripMenuItem";
            this.installModulesToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+O";
            this.installModulesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.installModulesToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.installModulesToolStripMenuItem.Text = "&Install Modules...";
            this.installModulesToolStripMenuItem.ToolTipText = "Install Modules...";
            // 
            // toolStripSeparator1
            // 
            this.commandManager.SetCommand(this.toolStripSeparator1, "");
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(206, 6);
            // 
            // enableSelectedToolStripMenuItem
            // 
            this.commandManager.SetCommand(this.enableSelectedToolStripMenuItem, "EnableModulesCommand");
            this.enableSelectedToolStripMenuItem.Enabled = false;
            this.enableSelectedToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("enableSelectedToolStripMenuItem.Image")));
            this.enableSelectedToolStripMenuItem.Name = "enableSelectedToolStripMenuItem";
            this.enableSelectedToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+E";
            this.enableSelectedToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.enableSelectedToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.enableSelectedToolStripMenuItem.Text = "&Enable selected";
            this.enableSelectedToolStripMenuItem.ToolTipText = "Enable selected";
            // 
            // disableSelectedToolStripMenuItem
            // 
            this.commandManager.SetCommand(this.disableSelectedToolStripMenuItem, "DisableModulesCommand");
            this.disableSelectedToolStripMenuItem.Enabled = false;
            this.disableSelectedToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("disableSelectedToolStripMenuItem.Image")));
            this.disableSelectedToolStripMenuItem.Name = "disableSelectedToolStripMenuItem";
            this.disableSelectedToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+D";
            this.disableSelectedToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.disableSelectedToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.disableSelectedToolStripMenuItem.Text = "&Disable selected";
            this.disableSelectedToolStripMenuItem.ToolTipText = "Disable selected";
            // 
            // uninstallSelectedToolStripMenuItem
            // 
            this.commandManager.SetCommand(this.uninstallSelectedToolStripMenuItem, "UninstallModulesCommand");
            this.uninstallSelectedToolStripMenuItem.Enabled = false;
            this.uninstallSelectedToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("uninstallSelectedToolStripMenuItem.Image")));
            this.uninstallSelectedToolStripMenuItem.Name = "uninstallSelectedToolStripMenuItem";
            this.uninstallSelectedToolStripMenuItem.ShortcutKeyDisplayString = "Delete";
            this.uninstallSelectedToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.uninstallSelectedToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.uninstallSelectedToolStripMenuItem.Text = "&Uninstall selected";
            this.uninstallSelectedToolStripMenuItem.ToolTipText = "Uninstall selected";
            // 
            // toolStripSeparator2
            // 
            this.commandManager.SetCommand(this.toolStripSeparator2, "");
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(206, 6);
            // 
            // browseToModuleToolStripMenuItem
            // 
            this.commandManager.SetCommand(this.browseToModuleToolStripMenuItem, "BrowseToModuleCommand");
            this.browseToModuleToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("browseToModuleToolStripMenuItem.Image")));
            this.browseToModuleToolStripMenuItem.Name = "browseToModuleToolStripMenuItem";
            this.browseToModuleToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+B";
            this.browseToModuleToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.browseToModuleToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.browseToModuleToolStripMenuItem.Text = "&Browse to module...";
            this.browseToModuleToolStripMenuItem.ToolTipText = "Browse to module...";
            // 
            // modulesToolStrip
            // 
            this.modulesToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.modulesToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.modulesToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.modulesToolStripSearchTextBox});
            this.modulesToolStrip.Location = new System.Drawing.Point(0, 441);
            this.modulesToolStrip.Name = "modulesToolStrip";
            this.modulesToolStrip.Size = new System.Drawing.Size(360, 25);
            this.modulesToolStrip.TabIndex = 1;
            this.modulesToolStrip.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.commandManager.SetCommand(this.toolStripLabel1, "");
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(44, 22);
            this.toolStripLabel1.Text = "&Search:";
            // 
            // modulesToolStripSearchTextBox
            // 
            this.modulesToolStripSearchTextBox.ActiveTheme = null;
            this.modulesToolStripSearchTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modulesToolStripSearchTextBox.ClearImage = null;
            this.commandManager.SetCommand(this.modulesToolStripSearchTextBox, "");
            this.modulesToolStripSearchTextBox.Name = "modulesToolStripSearchTextBox";
            this.modulesToolStripSearchTextBox.SearchImage = null;
            this.modulesToolStripSearchTextBox.Size = new System.Drawing.Size(150, 25);
            this.modulesToolStripSearchTextBox.TextChanged += new System.EventHandler(this.modulesToolStripSearchTextBox_TextChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.invisibleControl1);
            this.flowLayoutPanel1.Controls.Add(this.infoLabel);
            this.flowLayoutPanel1.Controls.Add(this.helpButton);
            this.flowLayoutPanel1.Controls.Add(this.installButton);
            this.flowLayoutPanel1.Controls.Add(this.enableButton);
            this.flowLayoutPanel1.Controls.Add(this.disableButton);
            this.flowLayoutPanel1.Controls.Add(this.uninstallButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(169, 443);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // invisibleControl1
            // 
            this.invisibleControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.invisibleControl1.Location = new System.Drawing.Point(0, 0);
            this.invisibleControl1.Name = "invisibleControl1";
            this.invisibleControl1.Size = new System.Drawing.Size(169, 0);
            this.invisibleControl1.TabIndex = 0;
            this.invisibleControl1.TabStop = false;
            this.invisibleControl1.Text = "invisibleControl1";
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoLabel.Location = new System.Drawing.Point(3, 0);
            this.infoLabel.MinimumSize = new System.Drawing.Size(0, 120);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(163, 120);
            this.infoLabel.TabIndex = 0;
            // 
            // helpButton
            // 
            this.commandManager.SetCommand(this.helpButton, "HelpCommand;Modules!");
            this.helpButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.helpButton.Image = ((System.Drawing.Image)(resources.GetObject("helpButton.Image")));
            this.helpButton.Location = new System.Drawing.Point(3, 123);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(163, 23);
            this.helpButton.TabIndex = 1;
            this.helpButton.Text = "&Show help...";
            this.helpButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.helpButton, "Shows help about what modules are and how they work.");
            this.helpButton.UseVisualStyleBackColor = true;
            // 
            // installButton
            // 
            this.commandManager.SetCommand(this.installButton, "InstallModulesCommand");
            this.installButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.installButton.Image = ((System.Drawing.Image)(resources.GetObject("installButton.Image")));
            this.installButton.Location = new System.Drawing.Point(3, 152);
            this.installButton.Name = "installButton";
            this.installButton.Size = new System.Drawing.Size(163, 23);
            this.installButton.TabIndex = 2;
            this.installButton.Text = "&Install Modules...";
            this.installButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.installButton, "Install new modules from a file or folder.");
            this.installButton.UseVisualStyleBackColor = true;
            // 
            // enableButton
            // 
            this.commandManager.SetCommand(this.enableButton, "EnableModulesCommand");
            this.enableButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.enableButton.Enabled = false;
            this.enableButton.Image = ((System.Drawing.Image)(resources.GetObject("enableButton.Image")));
            this.enableButton.Location = new System.Drawing.Point(3, 181);
            this.enableButton.Name = "enableButton";
            this.enableButton.Size = new System.Drawing.Size(163, 23);
            this.enableButton.TabIndex = 3;
            this.enableButton.Text = "&Enable selected";
            this.enableButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.enableButton, "Enables the selected modules, loading them.");
            this.enableButton.UseVisualStyleBackColor = true;
            // 
            // disableButton
            // 
            this.commandManager.SetCommand(this.disableButton, "DisableModulesCommand");
            this.disableButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.disableButton.Enabled = false;
            this.disableButton.Image = ((System.Drawing.Image)(resources.GetObject("disableButton.Image")));
            this.disableButton.Location = new System.Drawing.Point(3, 210);
            this.disableButton.Name = "disableButton";
            this.disableButton.Size = new System.Drawing.Size(163, 23);
            this.disableButton.TabIndex = 4;
            this.disableButton.Text = "&Disable selected";
            this.disableButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.disableButton, "Disables the selected modules.");
            this.disableButton.UseVisualStyleBackColor = true;
            // 
            // uninstallButton
            // 
            this.commandManager.SetCommand(this.uninstallButton, "UninstallModulesCommand");
            this.uninstallButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.uninstallButton.Enabled = false;
            this.uninstallButton.Image = ((System.Drawing.Image)(resources.GetObject("uninstallButton.Image")));
            this.uninstallButton.Location = new System.Drawing.Point(3, 239);
            this.uninstallButton.Name = "uninstallButton";
            this.uninstallButton.Size = new System.Drawing.Size(163, 23);
            this.uninstallButton.TabIndex = 5;
            this.uninstallButton.Text = "&Uninstall selected";
            this.uninstallButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.uninstallButton, "Permanently removes the selected modules.");
            this.uninstallButton.UseVisualStyleBackColor = true;
            // 
            // openFolderButton
            // 
            this.commandManager.SetCommand(this.openFolderButton, "BrowseToModuleCommand");
            this.openFolderButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.openFolderButton.Image = ((System.Drawing.Image)(resources.GetObject("openFolderButton.Image")));
            this.openFolderButton.Location = new System.Drawing.Point(0, 443);
            this.openFolderButton.Name = "openFolderButton";
            this.openFolderButton.Size = new System.Drawing.Size(169, 23);
            this.openFolderButton.TabIndex = 0;
            this.openFolderButton.Text = "&Browse to module...";
            this.openFolderButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.openFolderButton, "Opens the folder containing the module(s).");
            this.openFolderButton.UseVisualStyleBackColor = true;
            // 
            // commandManager
            // 
            this.commandManager.CommandHistory = null;
            // 
            // ModulePreferencePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer);
            this.Name = "ModulePreferencePage";
            this.Size = new System.Drawing.Size(533, 466);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.modulesTreeListView)).EndInit();
            this.modulesContextMenuStrip.ResumeLayout(false);
            this.modulesToolStrip.ResumeLayout(false);
            this.modulesToolStrip.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private SFMTK.Controls.EnhancedTreeListView modulesTreeListView;
        private BrightIdeasSoftware.OLVColumn moduleNameColumn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private InvisibleControl invisibleControl1;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Button installButton;
        private System.Windows.Forms.Button openFolderButton;
        private System.Windows.Forms.Button uninstallButton;
        private System.Windows.Forms.Button enableButton;
        private System.Windows.Forms.Button disableButton;
        private System.Windows.Forms.Button helpButton;
        private System.Windows.Forms.ContextMenuStrip modulesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem installModulesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem enableSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disableSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uninstallSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem browseToModuleToolStripMenuItem;
        private Commands.CommandManager commandManager;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStrip modulesToolStrip;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private ToolStripSearchTextBox modulesToolStripSearchTextBox;
        private BrightIdeasSoftware.OLVColumn moduleFullNameColumn;
        private BrightIdeasSoftware.OLVColumn moduleLoadedColumn;
        private BrightIdeasSoftware.OLVColumn moduleAuthorColumn;
        private BrightIdeasSoftware.OLVColumn moduleVersionColumn;
        private BrightIdeasSoftware.OLVColumn moduleActionColumn;
    }
}