﻿using BrightIdeasSoftware;

namespace SFMTK.Controls.PreferencePages
{
    public class PreferencePageFilter : IModelFilter
    {
        /// <summary>
        /// Should the given model be included when this filter is installed
        /// </summary>
        /// <param name="modelObject">The model object to consider</param>
        /// <returns>
        /// Returns true if the model will be included by the filter
        /// </returns>
        public bool Filter(object modelObject)
        {
            var page = modelObject as IPreferencePage;
            if (page == null)
                return false;

            return page.Filter;
        }
    }
}
