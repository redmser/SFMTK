﻿using SFMTK.Properties;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;
using System.Drawing;

namespace SFMTK.Controls.PreferencePages
{
    public partial class DialogsPreferencePage : PreferencePage
    {
        //TODO: DialogsPreferencePage - show windowmanager stored properties in the save listview (use groups to differenciate data)

        public DialogsPreferencePage()
        {
        }

        public override string Path => "Environment/Dialogs";

        public override Image Image => Resources.application_form;

        public override Control ExtendControl => this.mainFlowLayoutPanel;

        public override void OnFirstSelect(object sender, EventArgs e)
        {
            InitializeComponent();

            this.dialogStatesDataListView.DataSource = Settings.Default.RememberDialogSettings;
            this.dialogStatesDataListView.EmptyListMsgOverlay = new Controls.EmptyListOverlay(this.dialogStatesDataListView);
            this.dialogStatesDataListView.DefaultRenderer = new HighlightTextRenderer();

            //Disable force fallback if already falling back
            if (!Ookii.Dialogs.Wpf.TaskDialog.OSSupportsTaskDialogs)
            {
                Settings.Default.ForceFallbackDialogs = true;
                this.forceFallbackCheckBox.Enabled = false;
            }

            base.OnFirstSelect(sender, e);
        }

        private void removeDialogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Remove selected
            Settings.Default.RememberDialogSettings.RemoveAll(s => this.dialogStatesDataListView.SelectedObjects.Contains(s));

            //Refresh all
            this.dialogStatesDataListView.BuildList();
            this.resetAllDialogsToolStripButton.Enabled = Settings.Default.RememberDialogSettings.Any();
        }

        private void dialogsContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            //Enable entry based on selection
            var enable = this.dialogStatesDataListView.SelectedObjects.Count > 0;
            this.removeDialogToolStripMenuItem.Enabled = enable;
        }

        private void searchDialogsToolStripTextBox_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.searchDialogsToolStripTextBox.Text))
            {
                //Stop filter
                this.dialogStatesDataListView.AdditionalFilter = null;
            }
            else
            {
                //Filtering
                this.dialogStatesDataListView.AdditionalFilter = new RememberDialogSettingFilter(this.searchDialogsToolStripTextBox.Text);
                ((HighlightTextRenderer)this.dialogStatesDataListView.DefaultRenderer).Filter
                    = new TextMatchFilter(this.dialogStatesDataListView, this.searchDialogsToolStripTextBox.Text, StringComparison.InvariantCultureIgnoreCase);
            }
        }

        private void resetAllDialogsToolStripButton_Click(object sender, EventArgs e)
        {
            const string identifier = "Reset all remembered actions?";

            if (FallbackRememberDialog.ShowDialog("This will remove all remembered actions.\n\nAre you sure you want to continue?", null,
                identifier, "You can remove individual remembered actions by rightclicking them in the list.", null,
                identifier, FallbackDialogIcon.Warning,
                new FallbackDialogButton(DialogResult.Yes), new FallbackDialogButton(DialogResult.No, false)) == DialogResult.Yes)
            {
                //Reset all! (Except the prompt from just now)
                Settings.Default.RememberDialogSettings.RemoveAll(s => s.Identifier != identifier);

                //Refresh all
                this.dialogStatesDataListView.BuildList();
                this.resetAllDialogsToolStripButton.Enabled = Settings.Default.RememberDialogSettings.Any();
            }
        }

        private void UpdateRememberList()
        {
            //Update dialogs list
            this.dialogStatesDataListView.BuildList();
            this.resetAllDialogsToolStripButton.Enabled = Settings.Default.RememberDialogSettings.Any();
        }

        public override void OnSelect(object sender, EventArgs e)
        {
            base.OnSelect(sender, e);

            UpdateRememberList();
        }
    }
}
