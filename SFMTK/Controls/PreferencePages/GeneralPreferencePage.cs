﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SFMTK.Controls.PreferencePages
{
    public partial class GeneralPreferencePage : PreferencePage
    {
        public GeneralPreferencePage()
        {
        }

        public override string Path => "Environment/General";

        public override Control ExtendControl => this.mainFlowLayoutPanel;

        public override void OnFirstSelect(object sender, EventArgs e)
        {
            InitializeComponent();

            this.themeComboBox.DataSource = UITheme.Themes;
            this.themeComboBox.DisplayMember = "Name";
            this.themeComboBox.ValueMember = "Name";
            var themebinding = new ActionBinding("SelectedItem", Properties.Settings.Default, "SelectedTheme", DataSourceUpdateMode.OnPropertyChanged,
                (s, a) => Properties.Settings.Default.UpdateTheme());
            themebinding.Format += this.ThemeBinding_Format;
            themebinding.Parse += this.ThemeBinding_Parse;
            this.themeComboBox.DataBindings.Add(themebinding);
            var startupbinding = new CastBinding<Properties.OnStartupSetting, int>("SelectedIndex", Properties.Settings.Default, "OnStartup", DataSourceUpdateMode.OnPropertyChanged);
            this.startupComboBox.DataBindings.Add(startupbinding);
            this.undoStepsNumericUpDown.DataBindings.Add("Value", Properties.Settings.Default, "UndoSteps", true, DataSourceUpdateMode.OnPropertyChanged);
            //BUG: GeneralPreferencePage - editing sfm path and then another value will RESET the textbox with sfm path...
            //  -> change update mode to propertychanged and move SFM.UpdateBasePath() to preapplysettings?
            var sfmlocbinding = new PathBinding("Text", Properties.Settings.Default, "SFMPath", DataSourceUpdateMode.OnPropertyChanged);
            this.sfmLocationTextBox.DataBindings.Add(sfmlocbinding);
            var showversionbinding = new ActionBinding("Checked", Properties.Settings.Default, "ShowVersion", DataSourceUpdateMode.OnPropertyChanged,
                (s, a) => Properties.Settings.Default.UpdateMainTitle());
            this.showVersionCheckBox.DataBindings.Add(showversionbinding);
            var showsetupbinding = new ActionBinding("Checked", Properties.Settings.Default, "ShowSetup", DataSourceUpdateMode.OnPropertyChanged,
                (s, a) => Properties.Settings.Default.UpdateMainTitle());
            this.setupTitleCheckBox.DataBindings.Add(showsetupbinding);

            //Needs to be initialized due to data bindings
            this.UpdateErrorMarker();

            base.OnFirstSelect(sender, e);
        }

        private void ThemeBinding_Parse(object sender, ConvertEventArgs e)
        {
            var theme = e.Value as UITheme;
            if (theme == null)
            {
                e.Value = null;
                return;
            }

            e.Value = theme.Name;
        }

        private void ThemeBinding_Format(object sender, ConvertEventArgs e)
        {
            var name = e.Value.ToString();
            e.Value = UITheme.Themes.FirstOrDefault(t => t.Name == name);
            if (e.Value == null)
            {
                //Select default
                e.Value = UITheme.SystemDefaultTheme;
            }

            if (UITheme.SelectedTheme.Equals((UITheme)e.Value))
                return;

            UITheme.SelectedTheme = (UITheme)e.Value;
        }

        private void detectSFMButton_Click(object sender, EventArgs e) => this.DetectSFMDir(true);

        private void browseSFMButton_Click(object sender, EventArgs e) => this.BrowseSFMDir();

        /// <summary>
        /// Detects the Source Filmmaker installation of this PC, saving it to the text box.
        /// </summary>
        /// <param name="notifyUser">If set to <c>true</c>, notify user when unable to find installation.</param>
        private void DetectSFMDir(bool notifyUser)
        {
            try
            {
                //Detect dir
                var dir = SFM.DetectSFMDirectory();
                this.sfmLocationTextBox.Text = dir;
            }
            catch
            {
                //Error? Error!
                this.UpdateErrorMarker();

                //Couldn't find SFM install
                if (notifyUser)
                {
                    //Let user know (if button was pressed only)
                    var res = FallbackTaskDialog.ShowDialogLog("SFMTK could not find an installation of Source Filmmaker on your computer. Would you like to search for it manually?",
                        "Unable to find Source Filmmaker", "Unable to find Source Filmmaker", FallbackDialogIcon.Warning,
                        new FallbackDialogButton(DialogResult.Yes), new FallbackDialogButton(DialogResult.No));

                    if (res == DialogResult.Yes)
                    {
                        //Browse for it now
                        this.BrowseSFMDir();
                    }
                }
            }
        }

        /// <summary>
        /// Opens a file browser for selection the Source Filmmaker installation, saving it to the text box.
        /// </summary>
        private void BrowseSFMDir()
        {
            string dir;
            try
            {
                //Detect maybe
                dir = SFM.DetectSFMDirectory() + "game/";
            }
            catch
            {
                //Could not detect
                dir = null;
            }

            var ofd = new Ookii.Dialogs.Wpf.VistaOpenFileDialog();
            ofd.AddExtension = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            ofd.DefaultExt = "sfm.exe";
            ofd.Filter = "sfm.exe|sfm.exe";
            ofd.InitialDirectory = IOHelper.GetWindowsPath(dir ?? SFM.DetectSteamDirectory() ?? string.Empty);
            ofd.RestoreDirectory = true;
            ofd.ShowReadOnly = false;
            var res = ofd.ShowDialog() ?? false;
            if (res)
            {
                //Update directory (get parent, since dir of sfm.exe is always /game/ )
                this.sfmLocationTextBox.Text = IOHelper.GetStandardizedPath(new FileInfo(ofd.FileName).Directory.Parent.FullName);
            }
        }

        private void sfmLocationTextBox_TextChanged(object sender, EventArgs e) => this.UpdateErrorMarker();

        private void UpdateErrorMarker()
        {
            //Outside-SFM context
            var loc = IOHelper.GetStandardizedPath(this.sfmLocationTextBox.Text);
            if (Properties.Settings.Default.AllowBesidesSFM)
            {
                //Check if folder contains .exe file(s)
                if (loc.EndsWith("/bin/") && Directory.EnumerateFiles(loc, "*.exe", SearchOption.TopDirectoryOnly).Any())
                {
                    this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Warning;
                    this.sfmLocationErrorMarker.Text = "This path seems to point to a binaries directory of a mod, which is invalid. No further verification is done when the experimental option for non-SFM use is enabled.";
                }
                else if (!Directory.EnumerateFiles(loc, "*.exe", SearchOption.TopDirectoryOnly).Any())
                {
                    this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Info;
                    this.sfmLocationErrorMarker.Text = "This path does not contain any executables, and may be invalid. No further verification is done when the experimental option for non-SFM use is enabled.";
                }
                else
                {
                    this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Info;
                    this.sfmLocationErrorMarker.Text = "This path should be a valid game installation. No verification is done when the experimental option for non-SFM use is enabled.";
                }
                return;
            }

            //Update error marker + tooltip
            if (string.IsNullOrEmpty(loc))
            {
                //No text
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.sfmLocationErrorMarker.Text = "Please specify the location to your installation of Source Filmmaker.";
            }
            else if (loc.EndsWith("/sfm.exe", StringComparison.InvariantCultureIgnoreCase))
            {
                //Must be path to sfm install dir
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.sfmLocationErrorMarker.Text = "This path should point to your installation directory of Source Filmmaker, excluding the executable name.";
            }
            else if (loc.EndsWith("/game/", StringComparison.InvariantCultureIgnoreCase))
            {
                //Must be path to sfm install dir
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.sfmLocationErrorMarker.Text = "This path should point to your installation directory of Source Filmmaker, excluding the game directory.";
            }
            else if (!Directory.Exists(loc))
            {
                //Needs to exist
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.sfmLocationErrorMarker.Text = "The specified path is not a valid installation of Source Filmmaker (directory does not exist).";
            }
            else if (!File.Exists(System.IO.Path.Combine(loc, "game", "sfm.exe")))
            {
                //Needs sfm.exe
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.sfmLocationErrorMarker.Text = "The specified path is not a valid installation of Source Filmmaker (could not find sfm.exe).";
            }
            else if (!loc.EndsWith("/SourceFilmmaker/", StringComparison.InvariantCultureIgnoreCase))
            {
                //Not a regular SFM folder
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Warning;
                this.sfmLocationErrorMarker.Text = "While the specified installation of Source Filmmaker seems valid, it is not findable by Steam directly.";
            }
            else
            {
                //Woo, valid!
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Valid;
                this.sfmLocationErrorMarker.Text = "The specified path is a valid installation of Source Filmmaker.";
            }
        }

        private void editThemeButton_Click(object sender, EventArgs e)
        {
            //Edit selected theme
            Program.MainDPM.AddContent(new Contents.ThemeContent(UITheme.SelectedTheme));
            Program.MainForm.Focus();
        }

        private void themeComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //Update selected theme
            this.themeComboBox.DataBindings["SelectedItem"].WriteValue();
        }

        private void startupComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //Show or hide the edit script button
            this.editStartupScriptButton.Visible = (this.startupComboBox.SelectedIndex == (int)Properties.OnStartupSetting.Script);
        }

        private void editStartupScriptButton_Click(object sender, EventArgs e)
        {
            if (PaidHelper.CheckFullVersion("Scripting"))
            {
                //NYI: GeneralPreferencePage - open startup script in a new text editor (either create script file, or open existing)
                //  -> delegate this code to external module
                throw new NotImplementedException();
            }
        }
    }
}
