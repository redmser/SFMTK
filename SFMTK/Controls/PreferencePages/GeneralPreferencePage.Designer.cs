﻿namespace SFMTK.Controls.PreferencePages
{
    partial class GeneralPreferencePage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.invisibleControl1 = new SFMTK.Controls.InvisibleControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.sfmLocationErrorMarker = new SFMTK.Controls.ErrorMarker();
            this.sfmdirLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.detectSFMButton = new System.Windows.Forms.Button();
            this.sfmLocationTextBox = new System.Windows.Forms.TextBox();
            this.browseSFMButton = new System.Windows.Forms.Button();
            this.sfmdirInfoLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.undoStepsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.toolbarPerLayoutCheckBox = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.editStartupScriptButton = new System.Windows.Forms.Button();
            this.startupComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.editThemeButton = new System.Windows.Forms.Button();
            this.themeComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.statusBarCheckBox = new System.Windows.Forms.CheckBox();
            this.showVersionCheckBox = new System.Windows.Forms.CheckBox();
            this.setupTitleCheckBox = new System.Windows.Forms.CheckBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.mainFlowLayoutPanel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.undoStepsNumericUpDown)).BeginInit();
            this.panel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainFlowLayoutPanel
            // 
            this.mainFlowLayoutPanel.AutoScroll = true;
            this.mainFlowLayoutPanel.Controls.Add(this.invisibleControl1);
            this.mainFlowLayoutPanel.Controls.Add(this.groupBox2);
            this.mainFlowLayoutPanel.Controls.Add(this.groupBox1);
            this.mainFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.mainFlowLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainFlowLayoutPanel.Name = "mainFlowLayoutPanel";
            this.mainFlowLayoutPanel.Size = new System.Drawing.Size(522, 403);
            this.mainFlowLayoutPanel.TabIndex = 0;
            this.mainFlowLayoutPanel.WrapContents = false;
            // 
            // invisibleControl1
            // 
            this.invisibleControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.invisibleControl1.Location = new System.Drawing.Point(0, 0);
            this.invisibleControl1.Name = "invisibleControl1";
            this.invisibleControl1.Size = new System.Drawing.Size(522, 0);
            this.invisibleControl1.TabIndex = 0;
            this.invisibleControl1.TabStop = false;
            this.invisibleControl1.Text = "invisibleControl1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.flowLayoutPanel5);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(516, 193);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Application";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.AutoScroll = true;
            this.flowLayoutPanel5.Controls.Add(this.panel3);
            this.flowLayoutPanel5.Controls.Add(this.tableLayoutPanel2);
            this.flowLayoutPanel5.Controls.Add(this.sfmdirInfoLabel);
            this.flowLayoutPanel5.Controls.Add(this.panel2);
            this.flowLayoutPanel5.Controls.Add(this.toolbarPerLayoutCheckBox);
            this.flowLayoutPanel5.Controls.Add(this.panel4);
            this.flowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel5.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(510, 174);
            this.flowLayoutPanel5.TabIndex = 0;
            this.flowLayoutPanel5.WrapContents = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.sfmLocationErrorMarker);
            this.panel3.Controls.Add(this.sfmdirLabel);
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(153, 18);
            this.panel3.TabIndex = 0;
            // 
            // sfmLocationErrorMarker
            // 
            this.sfmLocationErrorMarker.BackColor = System.Drawing.Color.Transparent;
            this.sfmLocationErrorMarker.Location = new System.Drawing.Point(0, 0);
            this.sfmLocationErrorMarker.Name = "sfmLocationErrorMarker";
            this.sfmLocationErrorMarker.Size = new System.Drawing.Size(16, 16);
            this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Valid;
            this.sfmLocationErrorMarker.TabIndex = 0;
            // 
            // sfmdirLabel
            // 
            this.sfmdirLabel.AutoSize = true;
            this.sfmdirLabel.Location = new System.Drawing.Point(16, 2);
            this.sfmdirLabel.Name = "sfmdirLabel";
            this.sfmdirLabel.Size = new System.Drawing.Size(137, 13);
            this.sfmdirLabel.TabIndex = 1;
            this.sfmdirLabel.Text = "Source &Filmmaker directory:";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.Controls.Add(this.detectSFMButton, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.sfmLocationTextBox, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.browseSFMButton, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 27);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(491, 24);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // detectSFMButton
            // 
            this.detectSFMButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detectSFMButton.Image = global::SFMTK.Properties.Resources.find;
            this.detectSFMButton.Location = new System.Drawing.Point(467, 0);
            this.detectSFMButton.Margin = new System.Windows.Forms.Padding(0);
            this.detectSFMButton.Name = "detectSFMButton";
            this.detectSFMButton.Size = new System.Drawing.Size(24, 24);
            this.detectSFMButton.TabIndex = 2;
            this.toolTip.SetToolTip(this.detectSFMButton, "Automatically detect location of sfm.exe");
            this.detectSFMButton.UseVisualStyleBackColor = true;
            this.detectSFMButton.Click += new System.EventHandler(this.detectSFMButton_Click);
            // 
            // sfmLocationTextBox
            // 
            this.sfmLocationTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.sfmLocationTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystemDirectories;
            this.sfmLocationTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sfmLocationTextBox.Location = new System.Drawing.Point(0, 2);
            this.sfmLocationTextBox.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.sfmLocationTextBox.Name = "sfmLocationTextBox";
            this.sfmLocationTextBox.Size = new System.Drawing.Size(443, 20);
            this.sfmLocationTextBox.TabIndex = 0;
            this.toolTip.SetToolTip(this.sfmLocationTextBox, "The directory containing your installation of Source Filmmaker.");
            this.sfmLocationTextBox.TextChanged += new System.EventHandler(this.sfmLocationTextBox_TextChanged);
            // 
            // browseSFMButton
            // 
            this.browseSFMButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browseSFMButton.Image = global::SFMTK.Properties.Resources.folder;
            this.browseSFMButton.Location = new System.Drawing.Point(443, 0);
            this.browseSFMButton.Margin = new System.Windows.Forms.Padding(0);
            this.browseSFMButton.Name = "browseSFMButton";
            this.browseSFMButton.Size = new System.Drawing.Size(24, 24);
            this.browseSFMButton.TabIndex = 1;
            this.toolTip.SetToolTip(this.browseSFMButton, "Browse for sfm.exe");
            this.browseSFMButton.UseVisualStyleBackColor = true;
            this.browseSFMButton.Click += new System.EventHandler(this.browseSFMButton_Click);
            // 
            // sfmdirInfoLabel
            // 
            this.sfmdirInfoLabel.AutoSize = true;
            this.sfmdirInfoLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.sfmdirInfoLabel.Location = new System.Drawing.Point(3, 54);
            this.sfmdirInfoLabel.Name = "sfmdirInfoLabel";
            this.sfmdirInfoLabel.Size = new System.Drawing.Size(491, 13);
            this.sfmdirInfoLabel.TabIndex = 2;
            this.sfmdirInfoLabel.Text = "SFMTK needs to know where Source Filmmaker is installed in order to correctly fin" +
    "d and install content.";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.undoStepsNumericUpDown);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(3, 82);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 15, 3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(145, 22);
            this.panel2.TabIndex = 3;
            // 
            // undoStepsNumericUpDown
            // 
            this.undoStepsNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.undoStepsNumericUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.undoStepsNumericUpDown.Location = new System.Drawing.Point(68, 0);
            this.undoStepsNumericUpDown.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.undoStepsNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.undoStepsNumericUpDown.Name = "undoStepsNumericUpDown";
            this.undoStepsNumericUpDown.Size = new System.Drawing.Size(76, 20);
            this.undoStepsNumericUpDown.TabIndex = 1;
            this.toolTip.SetToolTip(this.undoStepsNumericUpDown, "Amount of commands saved in the undo history. Higher numbers may result in more m" +
        "emory usage.");
            this.undoStepsNumericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "&Undo steps:";
            // 
            // toolbarPerLayoutCheckBox
            // 
            this.toolbarPerLayoutCheckBox.AutoSize = true;
            this.toolbarPerLayoutCheckBox.Checked = global::SFMTK.Properties.Settings.Default.ToolbarPerLayout;
            this.toolbarPerLayoutCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "ToolbarPerLayout", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.toolbarPerLayoutCheckBox.Location = new System.Drawing.Point(3, 110);
            this.toolbarPerLayoutCheckBox.Name = "toolbarPerLayoutCheckBox";
            this.toolbarPerLayoutCheckBox.Size = new System.Drawing.Size(233, 17);
            this.toolbarPerLayoutCheckBox.TabIndex = 4;
            this.toolbarPerLayoutCheckBox.Text = "&Individual toolbar settings per window layout";
            this.toolTip.SetToolTip(this.toolbarPerLayoutCheckBox, "If checked, every window layout has its own set of toolbar settings (including th" +
        "e position).\r\nOtherwise, the toolbar settings get saved and loaded only on start" +
        "up.");
            this.toolbarPerLayoutCheckBox.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.editStartupScriptButton);
            this.panel4.Controls.Add(this.startupComboBox);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Location = new System.Drawing.Point(3, 133);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(237, 23);
            this.panel4.TabIndex = 5;
            // 
            // editStartupScriptButton
            // 
            this.editStartupScriptButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editStartupScriptButton.Image = global::SFMTK.Properties.Resources.pencil;
            this.editStartupScriptButton.Location = new System.Drawing.Point(212, 0);
            this.editStartupScriptButton.Margin = new System.Windows.Forms.Padding(0);
            this.editStartupScriptButton.Name = "editStartupScriptButton";
            this.editStartupScriptButton.Size = new System.Drawing.Size(24, 24);
            this.editStartupScriptButton.TabIndex = 2;
            this.toolTip.SetToolTip(this.editStartupScriptButton, "Opens an editor for the startup script");
            this.editStartupScriptButton.UseVisualStyleBackColor = true;
            this.editStartupScriptButton.Visible = false;
            this.editStartupScriptButton.Click += new System.EventHandler(this.editStartupScriptButton_Click);
            // 
            // startupComboBox
            // 
            this.startupComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.startupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.startupComboBox.FormattingEnabled = true;
            this.startupComboBox.Items.AddRange(new object[] {
            "Empty environment",
            "Open recent files",
            "Run startup script"});
            this.startupComboBox.Location = new System.Drawing.Point(64, 0);
            this.startupComboBox.Name = "startupComboBox";
            this.startupComboBox.Size = new System.Drawing.Size(148, 21);
            this.startupComboBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.startupComboBox, "What to show when starting up SFMTK.");
            this.startupComboBox.SelectionChangeCommitted += new System.EventHandler(this.startupComboBox_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "On &startup:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.flowLayoutPanel4);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 202);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(516, 122);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Appearance";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.AutoScroll = true;
            this.flowLayoutPanel4.Controls.Add(this.panel1);
            this.flowLayoutPanel4.Controls.Add(this.statusBarCheckBox);
            this.flowLayoutPanel4.Controls.Add(this.showVersionCheckBox);
            this.flowLayoutPanel4.Controls.Add(this.setupTitleCheckBox);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(510, 103);
            this.flowLayoutPanel4.TabIndex = 0;
            this.flowLayoutPanel4.WrapContents = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.editThemeButton);
            this.panel1.Controls.Add(this.themeComboBox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(237, 25);
            this.panel1.TabIndex = 0;
            // 
            // editThemeButton
            // 
            this.editThemeButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editThemeButton.Image = global::SFMTK.Properties.Resources.pencil;
            this.editThemeButton.Location = new System.Drawing.Point(212, 0);
            this.editThemeButton.Margin = new System.Windows.Forms.Padding(0);
            this.editThemeButton.Name = "editThemeButton";
            this.editThemeButton.Size = new System.Drawing.Size(24, 24);
            this.editThemeButton.TabIndex = 2;
            this.toolTip.SetToolTip(this.editThemeButton, "Open the theme editor");
            this.editThemeButton.UseVisualStyleBackColor = true;
            this.editThemeButton.Click += new System.EventHandler(this.editThemeButton_Click);
            // 
            // themeComboBox
            // 
            this.themeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.themeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.themeComboBox.FormattingEnabled = true;
            this.themeComboBox.Location = new System.Drawing.Point(48, 1);
            this.themeComboBox.Name = "themeComboBox";
            this.themeComboBox.Size = new System.Drawing.Size(164, 21);
            this.themeComboBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.themeComboBox, "Change the colors of the application\'s user interface.");
            this.themeComboBox.SelectionChangeCommitted += new System.EventHandler(this.themeComboBox_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "&Theme:";
            // 
            // statusBarCheckBox
            // 
            this.statusBarCheckBox.AutoSize = true;
            this.statusBarCheckBox.Checked = global::SFMTK.Properties.Settings.Default.StatusBar;
            this.statusBarCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusBarCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "StatusBar", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.statusBarCheckBox.Location = new System.Drawing.Point(3, 34);
            this.statusBarCheckBox.Name = "statusBarCheckBox";
            this.statusBarCheckBox.Size = new System.Drawing.Size(102, 17);
            this.statusBarCheckBox.TabIndex = 1;
            this.statusBarCheckBox.Text = "Show status &bar";
            this.toolTip.SetToolTip(this.statusBarCheckBox, "Show a status bar on the main window.");
            this.statusBarCheckBox.UseVisualStyleBackColor = true;
            // 
            // showVersionCheckBox
            // 
            this.showVersionCheckBox.AutoSize = true;
            this.showVersionCheckBox.Location = new System.Drawing.Point(3, 57);
            this.showVersionCheckBox.Name = "showVersionCheckBox";
            this.showVersionCheckBox.Size = new System.Drawing.Size(174, 17);
            this.showVersionCheckBox.TabIndex = 2;
            this.showVersionCheckBox.Text = "Show application &version in title";
            this.toolTip.SetToolTip(this.showVersionCheckBox, "If enabled, shows the version of SFMTK in the title bar");
            this.showVersionCheckBox.UseVisualStyleBackColor = true;
            // 
            // setupTitleCheckBox
            // 
            this.setupTitleCheckBox.AutoSize = true;
            this.setupTitleCheckBox.Checked = true;
            this.setupTitleCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.setupTitleCheckBox.Location = new System.Drawing.Point(3, 80);
            this.setupTitleCheckBox.Name = "setupTitleCheckBox";
            this.setupTitleCheckBox.Size = new System.Drawing.Size(144, 17);
            this.setupTitleCheckBox.TabIndex = 3;
            this.setupTitleCheckBox.Text = "Show &active setup in title";
            this.toolTip.SetToolTip(this.setupTitleCheckBox, "Shows the name of the active SFM setup in the title bar of SFMTK");
            this.setupTitleCheckBox.UseVisualStyleBackColor = true;
            // 
            // GeneralPreferencePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainFlowLayoutPanel);
            this.Name = "GeneralPreferencePage";
            this.Size = new System.Drawing.Size(522, 403);
            this.mainFlowLayoutPanel.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.undoStepsNumericUpDown)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel mainFlowLayoutPanel;
        private InvisibleControl invisibleControl1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Panel panel3;
        private ErrorMarker sfmLocationErrorMarker;
        private System.Windows.Forms.Label sfmdirLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button detectSFMButton;
        private System.Windows.Forms.TextBox sfmLocationTextBox;
        private System.Windows.Forms.Button browseSFMButton;
        private System.Windows.Forms.Label sfmdirInfoLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.NumericUpDown undoStepsNumericUpDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox toolbarPerLayoutCheckBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button editThemeButton;
        private System.Windows.Forms.ComboBox themeComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox statusBarCheckBox;
        private System.Windows.Forms.CheckBox showVersionCheckBox;
        private System.Windows.Forms.CheckBox setupTitleCheckBox;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button editStartupScriptButton;
        private System.Windows.Forms.ComboBox startupComboBox;
        private System.Windows.Forms.Label label1;
    }
}
