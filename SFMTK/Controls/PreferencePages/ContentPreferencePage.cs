﻿using SFMTK.Forms;
using System;
using System.Windows.Forms;

namespace SFMTK.Controls.PreferencePages
{
    public partial class ContentPreferencePage : PreferencePage
    {
        public ContentPreferencePage()
        {
        }

        public override string Path => "Content/General";

        public override Control ExtendControl => this.mainFlowLayoutPanel;

        public override void OnFirstSelect(object sender, EventArgs e)
        {
            InitializeComponent();

            var tabtitlebinding = new ActionBinding("SelectedIndex", Properties.Settings.Default, "TabTitleFormat", DataSourceUpdateMode.OnPropertyChanged,
                (s, a) => Properties.Settings.Default.UpdateContentTitles());
            tabtitlebinding.Format += (s, a) => a.Value = (int)a.Value;
            tabtitlebinding.Parse += (s, a) => a.Value = (Properties.TitleFormat)a.Value;
            this.tabTitleComboBox.DataBindings.Add(tabtitlebinding);
            var fulltitlebinding = new ActionBinding("SelectedIndex", Properties.Settings.Default, "FullTitleFormat", DataSourceUpdateMode.OnPropertyChanged,
                (s, a) => { Properties.Settings.Default.UpdateContentTitles(); Properties.Settings.Default.UpdateMainTitle(); });
            fulltitlebinding.Format += (s, a) => a.Value = (int)a.Value;
            fulltitlebinding.Parse += (s, a) => a.Value = (Properties.TitleFormat)a.Value;
            this.fullTitleComboBox.DataBindings.Add(fulltitlebinding);
            var hidetitlebinding = new ActionBinding("Checked", Properties.Settings.Default, "HideTitleName", DataSourceUpdateMode.OnPropertyChanged,
                (s, a) => Properties.Settings.Default.UpdateMainTitle());
            this.hideTitleNameCheckBox.DataBindings.Add(hidetitlebinding);

            base.OnFirstSelect(sender, e);
        }
    }
}
