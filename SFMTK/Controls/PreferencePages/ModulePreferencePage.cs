﻿using SFMTK.Commands;
using SFMTK.Modules;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Threading.Tasks;
using BrightIdeasSoftware;
using System.IO;

namespace SFMTK.Controls.PreferencePages
{
    public partial class ModulePreferencePage : PreferencePage
    {
        //TODO: ModulePreferencePage - when trying to apply, but dependencies aren't met, open a warning message
        //  -> either enable dependencies, or disable dependent, or go back to settings
        //BUG: ModulePreferencePage - changing a modulesetting's state does not allow pressing the Apply button (invoke settingschanged event)
        //BUG: ModulePreferencePage - exceptions thrown during module (un)load will cause the task + progressbar dialog to get stuck forever!!
        //BUG: ModulePreferencePage - disabling modules and restarting SFMTK still reloads the modules

        public ModulePreferencePage()
        {
        }

        public override string Path => "Environment/Modules";

        public override Image Image => Properties.Resources.plugin_add;

        public override void OnSelect(object sender, EventArgs e)
        {
            if (this.firstselect)
            {
                InitializeComponent();

                //Init list
                this.moduleNameColumn.AspectGetter = (o) => ((ModuleSetting)o).GetDisplayText();
                this.moduleVersionColumn.AspectGetter = (o) => ((ModuleSetting)o).Module?.GetVersion();
                this.moduleAuthorColumn.AspectGetter = (o) => ((ModuleSetting)o).Module?.GetCompany();
                this.moduleLoadedColumn.ImageGetter = GetLoadedImage;
                this.moduleLoadedColumn.ClusteringStrategy = new ModuleSettingLoadedClusteringStrategy();
                this.modulesTreeListView.CheckStateGetter = this.GetCheckedState;
                this.modulesTreeListView.CheckStatePutter = this.SetCheckedState;
                this.modulesTreeListView.DropSink = new SimpleDropSink
                {
                    AcceptableLocations = DropTargetLocation.Background
                };
                this.modulesTreeListView.ChildrenGetter = (o) => ((ModuleSetting)o).GetChildren(Properties.Settings.Default.ModuleSettings);
                this.modulesTreeListView.CanExpandGetter = (o) => ((ModuleSetting)o).GetChildren(Properties.Settings.Default.ModuleSettings).Any();
                this.modulesTreeListView.CellToolTipGetter = GetToolTip;

                //Refresh list of AllModules
                Program.MainMM.RefreshModulesList(true);

                //Populate list
                this.modulesTreeListView.SetObjects(Properties.Settings.Default.ModuleSettings.Where(s => s.GetTopLevel()));
                this.modulesTreeListView.ExpandAll();

                //Default states
                this.UpdateInfoLabel();
            }
            else
            {
                //TODO: ModulePreferencePage - see if module list has changed
            }

            //Are any modules missing?
            var missing = Properties.Settings.Default.ModuleSettings.Where(m => m.Module == null);
            if (missing.Any())
            {
                //Mark them for deletion
                foreach (var m in missing)
                {
                    m.Action = ModuleSettingAction.Uninstall;
                }
                UpdateList();
            }

            base.OnSelect(sender, e);
        }

        private static Image GetLoadedImage(object rowObject)
        {
            var mod = ((ModuleSetting)rowObject).Module;
            if (mod == null)
                return null;
            if (mod.IsLoaded())
                return Properties.Resources.tick;
            return Properties.Resources.cross;
        }

        private string GetToolTip(OLVColumn column, object rowObject)
        {
            var mod = ((ModuleSetting)rowObject).Module;
            if (mod == null)
                return null;

            if (column == this.moduleLoadedColumn)
            {
                return mod.IsLoaded() ? "Loaded" : "Not loaded";
            }
            return null;
        }

        private CheckState SetCheckedState(object rowObject, CheckState newValue)
        {
            var ms = rowObject as ModuleSetting;
            switch (ms.Action)
            {
                case ModuleSettingAction.Toggle:
                    //If action is toggle, toggle CheckState and action
                    ms.Action = ModuleSettingAction.None;
                    return newValue;
                case ModuleSettingAction.None:
                    //If action is none, set to toggle and toggle CheckState
                    ms.Action = ModuleSettingAction.Toggle;
                    return newValue;
                case ModuleSettingAction.Uninstall:
                    //Should set back to old value
                    ms.Action = ms.Load ? ModuleSettingAction.None : ModuleSettingAction.Toggle;
                    return newValue;
            }

            //On specific actions, simply keep the old value
            return GetCheckedState(rowObject);
        }

        private CheckState GetCheckedState(object rowObject)
        {
            var ms = rowObject as ModuleSetting;
            switch (ms.Action)
            {
                case ModuleSettingAction.Toggle:
                    //Return the opposite of the Load value
                    return ms.Load ? CheckState.Unchecked : CheckState.Checked;
                case ModuleSettingAction.Uninstall:
                    //Always return unchecked
                    return CheckState.Unchecked;
                default:
                    //Return the load value
                    return ms.Load ? CheckState.Checked : CheckState.Unchecked;
            }
        }

        private void enableButton_Click(object sender, EventArgs e)
        {
            //Enable selected modules
            foreach (var module in this.SelectedModules)
                Program.MainMM.LoadModule(Program.MainMM.GetModuleByName(module.FullName), ModuleLoadingContext.SettingsLoad, true);
        }

        private void disableButton_Click(object sender, EventArgs e)
        {
            //Disable selected modules
            foreach (var module in this.SelectedModules)
                Program.MainMM.UnloadModule(Program.MainMM.GetModuleByName(module.FullName), ModuleUnloadingContext.Disable, true);
        }

        /// <summary>
        /// Gets the currently selected modules.
        /// </summary>
        public IEnumerable<ModuleSetting> SelectedModules => this.modulesTreeListView.SelectedObjects.Cast<ModuleSetting>();

        /// <summary>
        /// Rebuilds the list contents.
        /// </summary>
        public void UpdateList() => this.modulesTreeListView?.BuildList(true);

        private void modulesObjectListView_SelectionChanged(object sender, EventArgs e)
        {
            //Update enabled state
            CommandManager.UpdateCommandsEnabled(nameof(ModulePreferencePage), typeof(EnableModulesCommand), typeof(DisableModulesCommand), typeof(UninstallModulesCommand));

            //Update info box
            this.UpdateInfoLabel();
        }

        private void UpdateInfoLabel()
        {
            if (this.SelectedModules.Any())
            {
                var sel = this.SelectedModules.SingleOrSafeDefault();
                if (sel == null)
                {
                    //Multiple, heck
                    var allfound = "";
                    var notfound = this.SelectedModules.Count(m => m.Module == null);
                    if (notfound == 1)
                        allfound = "\n\nOne module can not be found in SFMTK's module directory and will be deleted.";
                    else if (notfound > 1)
                        allfound = "\n\nMultiple modules can not be found in SFMTK's module directory and will be deleted.";
                    this.infoLabel.Text = $"Selected {this.SelectedModules.Count()} modules:\n\n{string.Join("\n", this.SelectedModules)}{allfound}";
                }
                else
                {
                    //Single module selected, tell me about it
                    var mod = sel.Module;
                    if (mod == null)
                    {
                        //Null module
                        this.infoLabel.Text = $"Name: {sel.FullName}\n\nThis module can no longer be found in SFMTK's module directory and will be deleted.";
                    }
                    else
                    {
                        //Detailed information string
                        var deps = " no modules.";
                        var deplist = mod.GetDependencies();
                        if (deplist != null && deplist.Any())
                            deps = $":\n{string.Join("\n", deplist)}";
                        this.infoLabel.Text = $"Name: {sel.FullName}\nAuthor: {mod.GetCompany()}\nVersion: {mod.GetVersion()}\n\n" +
                            $"Description: {mod.GetDescription()}\n\nDepends on{deps}";
                    }
                }
            }
            else
            {
                //No modules selected!
                this.infoLabel.Text = "Select one or multiple modules to get information about them.";
            }
        }

        private void modulesObjectListView_DoubleClick(object sender, EventArgs e)
        {
            //Toggle selected item's checkboxes
            foreach (var mod in this.SelectedModules)
                mod.Load = !mod.Load;
        }

        private void modulesObjectListView_FormatRow(object sender, FormatRowEventArgs e)
        {
            //Color-coding and formatting
            var sett = ((ModuleSetting)e.Model);
            switch (sett.Action)
            {
                case ModuleSettingAction.None:
                    //Which state do we have now?
                    if (sett.Load)
                    {
                        //Load coloring - default
                        e.Item.ForeColor = UITheme.SelectedTheme.ListTextColor;
                    }
                    else
                    {
                        //Unload coloring
                        e.Item.ForeColor = UITheme.SelectedTheme.SubTextColor;
                    }
                    break;
                case ModuleSettingAction.Toggle:
                    //Which state do we have now?
                    if (sett.Load)
                    {
                        //Unload coloring
                        e.Item.ForeColor = UITheme.SelectedTheme.ErrorTextColor;
                    }
                    else
                    {
                        //Load coloring
                        e.Item.ForeColor = UITheme.SelectedTheme.HighlightTextColor;
                    }
                    break;
                case ModuleSettingAction.Install:
                    //Install coloring
                    e.Item.ForeColor = UITheme.SelectedTheme.HighlightTextColor;
                    e.Item.BackColor = UITheme.SelectedTheme.HighlightBackColor;
                    break;
                case ModuleSettingAction.Uninstall:
                    //Uninstall coloring
                    e.Item.ForeColor = UITheme.SelectedTheme.ErrorTextColor;
                    e.Item.BackColor = UITheme.SelectedTheme.ErrorBackColor;
                    e.Item.Font = new Font(e.Item.Font, FontStyle.Strikeout);
                    break;
            }
        }

        public override async Task OnPreApplySettings(object sender, EventArgs e)
        {
            //Get all mods
            var mods = Properties.Settings.Default.ModuleSettings.Where(ms => ms.Action != ModuleSettingAction.None).ToList();
            if (mods.Count == 0)
                return;

            //We don't want any module watcher events here
            Program.MainMM.StopDirectoryWatcher();

            //Show progress dialog for apply action
            var handler = ProgressInfo.ShowProgress("Applying module settings...", "", true);
            await Task.Run(() =>
            {
                //Iterate over all module settings
                for (var i = 0; i < mods.Count; i++)
                {
                    //Show progress first
                    var ms = mods[i];
                    handler.ReportProgress(new ProgressValue(i + 1, mods.Count) { Text = GetLoadingText(ms, i + 1) });

                    //Cancel if wanted
                    if (handler.Cancel.IsCancellationRequested)
                        break;

                    //Get module
                    var mod = Program.MainMM.GetModuleByName(ms.FullName);

                    bool? newload = null;
                    switch (ms.Action)
                    {
                        case ModuleSettingAction.Toggle:
                            //Check current load state
                            if (ms.Load)
                            {
                                //Disable this module
                                Program.MainMM.UnloadModule(mod, ModuleUnloadingContext.Disable, false);
                                newload = false;
                            }
                            else
                            {
                                //Enable this module
                                Program.MainMM.LoadModule(mod, ModuleLoadingContext.SettingsLoad, false);
                                newload = true;
                            }
                            break;
                        case ModuleSettingAction.Install:
                            //Load new module
                            Program.MainMM.LoadModule(mod, ModuleLoadingContext.SettingsInstall, false);
                            ms.Load = true;

                            //Add to settings list
                            Properties.Settings.Default.ModuleSettings.Add(ms);
                            break;
                        case ModuleSettingAction.Uninstall:
                            //Delete the module's folder and contents
                            //TODO: ModulePreferencePage - should we warn about uninstalling mods beforehand?
                            Program.MainMM.UnloadModule(mod, ModuleUnloadingContext.Uninstall, true);

                            var modpath = System.IO.Path.Combine(IOHelper.GetSFMTKDirectory(Constants.ModulesPath), ms.FullName);
                            if (Directory.Exists(modpath))
                                Directory.Delete(modpath, true);
                            break;
                    }

                    if (newload.HasValue)
                    {
                        //Update already-existing modulesettings
                        ms.Action = ModuleSettingAction.None;
                        ms.Load = newload.Value;
                    }
                }//for-loop

                //We are done
                handler.FinishTask();
            });

            //TODO: ModulePreferencePage - only re-enable if setting wants it
            Program.MainMM.StartDirectoryWatcher();




            string GetLoadingText(ModuleSetting ms, int index)
            {
                var actionstr = "Bothering with";
                switch (ms.Action)
                {
                    case ModuleSettingAction.Install:
                        actionstr = "Installing";
                        break;
                    case ModuleSettingAction.Toggle:
                        actionstr = ms.Load ? "Disabling" : "Enabling";
                        break;
                    case ModuleSettingAction.Uninstall:
                        actionstr = "Uninstalling";
                        break;
                }
                return $"Applying module settings...\n\n{actionstr} {ms.FullName} ({index} of {mods.Count})";
            }
        }

        public override async Task OnPostApplySettings(object sender, EventArgs e)
        {
            base.OnPostApplySettings(sender, e);

            this.UpdateList();
        }

        private void modulesObjectListView_DragDrop(object sender, DragEventArgs e)
        {
            //NYI: ModulePreferencePage - install dropped module file/folder/archive
            throw new NotImplementedException();
        }

        private void modulesObjectListView_CanDrop(object sender, OlvDropEventArgs e)
        {
            //Only allow files being dropped
            var valid = e.DragEventArgs.Data.GetDataPresent(DataFormats.FileDrop);
            e.Effect = valid ? DragDropEffects.Copy : DragDropEffects.None;
        }

        private void modulesToolStripSearchTextBox_TextChanged(object sender, EventArgs e)
        {
            //Redo module setting filtering
            if (string.IsNullOrEmpty(this.modulesToolStripSearchTextBox.Text))
            {
                //Disable the filter
                this.modulesTreeListView.AdditionalFilter = null;
            }
            else
            {
                //Create filter
                this.modulesTreeListView.AdditionalFilter = new ModuleSettingFilter(this.modulesToolStripSearchTextBox.Text);
            }
        }
    }
}
