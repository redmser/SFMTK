﻿namespace SFMTK.Controls.PreferencePages
{
    partial class KeyboardPreferencePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.shortcutObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.shortcutNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.shortcutDescriptionColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.shortcutKeysColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.shortcutCategoryColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.shortcutInternalColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.shortcutContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shortcutSearchToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.searchShortcutToolStripTextBox = new SFMTK.Controls.ToolStripSearchTextBox();
            this.resetAllShortcutsToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.mainFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shortcutObjectListView)).BeginInit();
            this.shortcutContextMenuStrip.SuspendLayout();
            this.shortcutSearchToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.shortcutObjectListView);
            this.groupBox3.Controls.Add(this.shortcutSearchToolStrip);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(499, 317);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Keyboard shortcuts";
            // 
            // shortcutObjectListView
            // 
            this.shortcutObjectListView.AllColumns.Add(this.shortcutNameColumn);
            this.shortcutObjectListView.AllColumns.Add(this.shortcutDescriptionColumn);
            this.shortcutObjectListView.AllColumns.Add(this.shortcutKeysColumn);
            this.shortcutObjectListView.AllColumns.Add(this.shortcutCategoryColumn);
            this.shortcutObjectListView.AllColumns.Add(this.shortcutInternalColumn);
            this.shortcutObjectListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.shortcutObjectListView.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.DoubleClick;
            this.shortcutObjectListView.CellEditTabChangesRows = true;
            this.shortcutObjectListView.CellEditUseWholeCell = false;
            this.shortcutObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.shortcutNameColumn,
            this.shortcutDescriptionColumn,
            this.shortcutKeysColumn});
            this.shortcutObjectListView.ContextMenuStrip = this.shortcutContextMenuStrip;
            this.shortcutObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.shortcutObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shortcutObjectListView.FullRowSelect = true;
            this.shortcutObjectListView.Location = new System.Drawing.Point(3, 16);
            this.shortcutObjectListView.Name = "shortcutObjectListView";
            this.shortcutObjectListView.Size = new System.Drawing.Size(493, 273);
            this.shortcutObjectListView.SortGroupItemsByPrimaryColumn = false;
            this.shortcutObjectListView.TabIndex = 1;
            this.shortcutObjectListView.UseCellFormatEvents = true;
            this.shortcutObjectListView.UseCompatibleStateImageBehavior = false;
            this.shortcutObjectListView.UseFiltering = true;
            this.shortcutObjectListView.View = System.Windows.Forms.View.Details;
            this.shortcutObjectListView.CellEditValidating += new BrightIdeasSoftware.CellEditEventHandler(this.shortcutObjectListView_CellEditValidating);
            this.shortcutObjectListView.FormatCell += new System.EventHandler<BrightIdeasSoftware.FormatCellEventArgs>(this.shortcutObjectListView_FormatCell);
            // 
            // shortcutNameColumn
            // 
            this.shortcutNameColumn.AspectName = "ListName";
            this.shortcutNameColumn.AutoCompleteEditor = false;
            this.shortcutNameColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.shortcutNameColumn.ImageAspectName = "Icon";
            this.shortcutNameColumn.IsEditable = false;
            this.shortcutNameColumn.Text = "Name";
            this.shortcutNameColumn.UseFiltering = false;
            this.shortcutNameColumn.Width = 180;
            // 
            // shortcutDescriptionColumn
            // 
            this.shortcutDescriptionColumn.AspectName = "ToolTipText";
            this.shortcutDescriptionColumn.IsEditable = false;
            this.shortcutDescriptionColumn.Text = "Description";
            this.shortcutDescriptionColumn.UseFiltering = false;
            this.shortcutDescriptionColumn.Width = 190;
            // 
            // shortcutKeysColumn
            // 
            this.shortcutKeysColumn.AspectName = "ShortcutString";
            this.shortcutKeysColumn.AutoCompleteEditor = false;
            this.shortcutKeysColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.shortcutKeysColumn.Hideable = false;
            this.shortcutKeysColumn.Text = "Shortcut";
            this.shortcutKeysColumn.Width = 90;
            // 
            // shortcutCategoryColumn
            // 
            this.shortcutCategoryColumn.AspectName = "Category";
            this.shortcutCategoryColumn.DisplayIndex = 3;
            this.shortcutCategoryColumn.IsEditable = false;
            this.shortcutCategoryColumn.IsVisible = false;
            this.shortcutCategoryColumn.Text = "Category";
            // 
            // shortcutInternalColumn
            // 
            this.shortcutInternalColumn.AspectName = "InternalName";
            this.shortcutInternalColumn.AutoCompleteEditor = false;
            this.shortcutInternalColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.shortcutInternalColumn.DisplayIndex = 3;
            this.shortcutInternalColumn.IsEditable = false;
            this.shortcutInternalColumn.IsVisible = false;
            this.shortcutInternalColumn.Text = "Internal Name";
            this.shortcutInternalColumn.UseFiltering = false;
            this.shortcutInternalColumn.Width = 140;
            // 
            // shortcutContextMenuStrip
            // 
            this.shortcutContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.resetToolStripMenuItem});
            this.shortcutContextMenuStrip.Name = "shortcutContextMenuStrip";
            this.shortcutContextMenuStrip.Size = new System.Drawing.Size(103, 48);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Image = global::SFMTK.Properties.Resources.pencil;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Image = global::SFMTK.Properties.Resources.arrow_undo;
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.resetToolStripMenuItem.Text = "Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // shortcutSearchToolStrip
            // 
            this.shortcutSearchToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.shortcutSearchToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.shortcutSearchToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.searchShortcutToolStripTextBox,
            this.resetAllShortcutsToolStripButton,
            this.toolStripSeparator1});
            this.shortcutSearchToolStrip.Location = new System.Drawing.Point(3, 289);
            this.shortcutSearchToolStrip.Name = "shortcutSearchToolStrip";
            this.shortcutSearchToolStrip.Size = new System.Drawing.Size(493, 25);
            this.shortcutSearchToolStrip.TabIndex = 2;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(44, 22);
            this.toolStripLabel1.Text = "&Search:";
            // 
            // searchShortcutToolStripTextBox
            // 
            this.searchShortcutToolStripTextBox.ActiveTheme = null;
            this.searchShortcutToolStripTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchShortcutToolStripTextBox.ClearImage = null;
            this.searchShortcutToolStripTextBox.Name = "searchShortcutToolStripTextBox";
            this.searchShortcutToolStripTextBox.SearchImage = null;
            this.searchShortcutToolStripTextBox.Size = new System.Drawing.Size(170, 25);
            this.searchShortcutToolStripTextBox.TextChanged += new System.EventHandler(this.searchShortcutToolStripTextBox_TextChanged);
            // 
            // resetAllShortcutsToolStripButton
            // 
            this.resetAllShortcutsToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.resetAllShortcutsToolStripButton.BackColor = System.Drawing.SystemColors.Window;
            this.resetAllShortcutsToolStripButton.Image = global::SFMTK.Properties.Resources.arrow_undo_star;
            this.resetAllShortcutsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.resetAllShortcutsToolStripButton.Name = "resetAllShortcutsToolStripButton";
            this.resetAllShortcutsToolStripButton.Size = new System.Drawing.Size(68, 22);
            this.resetAllShortcutsToolStripButton.Text = "&Reset all";
            this.resetAllShortcutsToolStripButton.ToolTipText = "Reset all shortcuts to their default values";
            this.resetAllShortcutsToolStripButton.Click += new System.EventHandler(this.resetAllShortcutsToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // mainFlowLayoutPanel
            // 
            this.mainFlowLayoutPanel.AutoSize = true;
            this.mainFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.mainFlowLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainFlowLayoutPanel.Name = "mainFlowLayoutPanel";
            this.mainFlowLayoutPanel.Size = new System.Drawing.Size(499, 0);
            this.mainFlowLayoutPanel.TabIndex = 6;
            // 
            // KeyboardPreferencePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.mainFlowLayoutPanel);
            this.Name = "KeyboardPreferencePage";
            this.Size = new System.Drawing.Size(499, 317);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shortcutObjectListView)).EndInit();
            this.shortcutContextMenuStrip.ResumeLayout(false);
            this.shortcutSearchToolStrip.ResumeLayout(false);
            this.shortcutSearchToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private BrightIdeasSoftware.ObjectListView shortcutObjectListView;
        private BrightIdeasSoftware.OLVColumn shortcutNameColumn;
        private BrightIdeasSoftware.OLVColumn shortcutDescriptionColumn;
        private BrightIdeasSoftware.OLVColumn shortcutKeysColumn;
        private System.Windows.Forms.ToolStrip shortcutSearchToolStrip;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private ToolStripSearchTextBox searchShortcutToolStripTextBox;
        private System.Windows.Forms.ToolStripButton resetAllShortcutsToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ContextMenuStrip shortcutContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private BrightIdeasSoftware.OLVColumn shortcutCategoryColumn;
        private System.Windows.Forms.ToolTip toolTip;
        private BrightIdeasSoftware.OLVColumn shortcutInternalColumn;
        private System.Windows.Forms.FlowLayoutPanel mainFlowLayoutPanel;
    }
}