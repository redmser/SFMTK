﻿using SFMTK.Commands;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace SFMTK.Controls.PreferencePages
{
    public partial class KeyboardPreferencePage : PreferencePage
    {
        //TODO: KeyboardPreferencePage - keyboard shortcuts should only show as a conflict if they can be accessed
        //  in the same context (global + specific, global + global, specific + specific)

        //BUG: KeyboardPreferencePage - contextmenu is not disabled when no item is selected

        //TODO: KeyboardPreferencePage - open the corresponding dropdown when pressing the shortcut key
        //  -> Has to somehow display shortcut on dropdown menuitem, since having children means no showy shortcut keys

        //IMP: KeyboardPreferencePage - serialize shortcuts/name/icons of Commands to settings, if possible only saving the edited ones?
        //  -> allow editing these properties inside of the keyboard tab (rightclick items), as well as obviously in the customize toolstrips dialog
        //  -> possibly identify commands in serialization using the command args string too?

        public KeyboardPreferencePage()
        {
        }

        public override string Path => "Environment/Keyboard";

        public override System.Drawing.Image Image => Properties.Resources.keyboard;

        public override Control ExtendControl => this.mainFlowLayoutPanel;

        public override void OnFirstSelect(object sender, EventArgs e)
        {
            InitializeComponent();

            //Set column settings
            //TODO: KeyboardPreferencePage - reorder shortcut groups so global hotkeys are first
            this.shortcutObjectListView.AlwaysGroupByColumn = this.shortcutCategoryColumn;
            this.shortcutObjectListView.DefaultRenderer = new HighlightTextRenderer();
            this.shortcutNameColumn.AspectToStringConverter = (s) => s.ToString().Replace("&", "");
            this.shortcutKeysColumn.ClusteringStrategy = new CommandShortcutClusteringStrategy(Program.MainCM);
            //TODO: KeyboardPreferencePage - fix sorting so numbers in strings are sorted accordingly as well (currently, the lambda is not called for unknown reasons)
            //this.shortcutObjectListView.CustomSorter = (c, o) => this.shortcutObjectListView.ListViewItemSorter = new ColumnComparerWrapper(new NumericStringComparer(), c, o);

            this.shortcutObjectListView.AddObjects(CommandManager.Commands.Where(c => c.ShortcutEditable).ToList());

            //Apply sorting
            //this.shortcutObjectListView.Sort(this.shortcutNameColumn, SortOrder.Ascending);

            base.OnFirstSelect(sender, e);
        }

        private void searchShortcutToolStripTextBox_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.searchShortcutToolStripTextBox.Text))
            {
                //Stop filter
                this.shortcutObjectListView.AdditionalFilter = null;
            }
            else
            {
                //Filtering
                this.shortcutObjectListView.AdditionalFilter = new CommandFilter(this.searchShortcutToolStripTextBox.Text);
                ((HighlightTextRenderer)this.shortcutObjectListView.DefaultRenderer).Filter
                    = new TextMatchFilter(this.shortcutObjectListView, this.searchShortcutToolStripTextBox.Text, StringComparison.InvariantCultureIgnoreCase);
            }
        }

        private void resetAllShortcutsToolStripButton_Click(object sender, EventArgs e)
        {
            if (FallbackRememberDialog.ShowDialog("This will reset all keyboard shortcuts back to their default value.\n\nAre you sure you want to continue?", null,
                "Reset all shortcuts?", "You can reset individual keyboard shortcuts by rightclicking them in the list.", null, null, FallbackDialogIcon.Warning,
                new FallbackDialogButton(DialogResult.Yes), new FallbackDialogButton(DialogResult.No, false)) == DialogResult.Yes)
            {
                //Reset all!
                foreach (CommandBase sel in this.shortcutObjectListView.Objects)
                {
                    //Reset to default shortcut
                    sel.Shortcut = sel.DefaultShortcut;
                }

                //Refresh all
                this.shortcutObjectListView.BuildList();
            }
        }

        private void shortcutObjectListView_CellEditValidating(object sender, BrightIdeasSoftware.CellEditEventArgs e)
        {
            //Check new value
            try
            {
                //Apply value
                var keys = CommandManager.KeysFromString(e.NewValue.ToString());

                //Force update dupes
                var i = CommandManager.Commands.IndexOf((CommandBase)e.RowObject);
                CommandManager.Commands[i].Shortcut = keys;

                //Check for duplicates, only if changing the value too
                var dupes = CommandManager.Commands.Where(c => c.Shortcut == keys).ToList();
                if (keys != Keys.None && dupes.Count > 1)
                {
                    //Redraw OLV
                    this.shortcutObjectListView.RefreshObjects(dupes);

                    //Collect info
                    var scinfo = "";
                    foreach (var sc in dupes)
                    {
                        scinfo += $"\n{sc.Name.Replace("&", "")} ({sc.InternalName})";
                    }

                    if (FallbackRememberDialog.ShowDialog($"The specified shortcut is already in use:\n{scinfo}", "Duplicate shortcut key", "Duplicate shortcut key",
                        null, null, FallbackDialogIcon.Warning, new FallbackDialogButton("Continue", DialogResult.OK),
                        new FallbackDialogButton("Edit Shortcut", DialogResult.Cancel)) == DialogResult.Cancel)
                    {
                        //Allow user to edit again
                        e.Cancel = true;
                    }
                }
                else
                {
                    //Refresh all, since dupes might have been removed
                    this.shortcutObjectListView.BuildList();
                }
            }
            catch (Exception ex)
            {
                //Invalid value!
                e.Cancel = true;

                if (FallbackTaskDialog.ShowDialog(ex.Message, "Unable to save shortcut key", "Invalid shortcut key", FallbackDialogIcon.Error,
                    new FallbackDialogButton(DialogResult.OK), new FallbackDialogButton(DialogResult.Cancel)) == DialogResult.Cancel)
                {
                    //Undo editing if cancelled
                    this.shortcutObjectListView.CancelCellEdit();
                }
            }
        }

        private void shortcutObjectListView_FormatCell(object sender, BrightIdeasSoftware.FormatCellEventArgs e)
        {
            if (e.Column != this.shortcutKeysColumn)
                return;

            var keys = ((CommandBase)e.Model).Shortcut;
            if (keys != Keys.None && CommandManager.Commands.Count(c => c.Shortcut == keys) > 1)
            {
                //Is a duplicate, recolor
                e.SubItem.ForeColor = UITheme.SelectedTheme.ErrorTextColor;
                e.SubItem.BackColor = UITheme.SelectedTheme.ErrorBackColor;
            }
            else
            {
                //Default color
                e.SubItem.ForeColor = UITheme.SelectedTheme.MainTextColor;
                e.SubItem.BackColor = UITheme.SelectedTheme.MainBackColor;
            }
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.shortcutObjectListView.SelectedObjects.Count == 0)
                return;

            //Edit selected row
            this.shortcutObjectListView.EditModel(this.shortcutObjectListView.SelectedObjects[0]);
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (CommandBase sel in this.shortcutObjectListView.SelectedObjects)
            {
                //Reset to default shortcut
                sel.Shortcut = sel.DefaultShortcut;
            }

            //Refresh all, since dupes might have been removed
            this.shortcutObjectListView.BuildList();
        }

        private void shortcutContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            //Enable items based on selection
            var enable = this.shortcutObjectListView.SelectedObjects.Count > 0;
            this.editToolStripMenuItem.Enabled = enable;
            this.resetToolStripMenuItem.Enabled = enable;
        }
    }
}
