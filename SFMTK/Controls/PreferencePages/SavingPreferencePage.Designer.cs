﻿namespace SFMTK.Controls.PreferencePages
{
    partial class SavingPreferencePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SavingPreferencePage));
            this.mainFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.invisibleControl2 = new SFMTK.Controls.InvisibleControl();
            this.autosavingPanel = new System.Windows.Forms.Panel();
            this.autosavingCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.autosaveModeRadioGroupPanel = new SFMTK.Controls.RadioGroupPanel();
            this.internalAutosavesRadioButton = new System.Windows.Forms.RadioButton();
            this.individualAutosaveRadioButton = new System.Windows.Forms.RadioButton();
            this.overrideAutosaveRadioButton = new System.Windows.Forms.RadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.autosaveNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.autoCompilePanel = new System.Windows.Forms.Panel();
            this.autoCompileHelpButton = new System.Windows.Forms.Button();
            this.autoCompileCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.autoCompileRadioGroupPanel = new SFMTK.Controls.RadioGroupPanel();
            this.restartAutoCompileRadioButton = new System.Windows.Forms.RadioButton();
            this.ignoreAutoCompileRadioButton = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.autoCompileModelsCheckBox = new System.Windows.Forms.CheckBox();
            this.autoCompileTexturesCheckBox = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.autoCompileViewButton = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.classicSaveCheckBox = new System.Windows.Forms.CheckBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.commandManager = new SFMTK.Commands.CommandManager();
            this.mainFlowLayoutPanel.SuspendLayout();
            this.autosavingPanel.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.autosaveModeRadioGroupPanel.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.autosaveNumericUpDown)).BeginInit();
            this.autoCompilePanel.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.flowLayoutPanel12.SuspendLayout();
            this.autoCompileRadioGroupPanel.SuspendLayout();
            this.flowLayoutPanel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainFlowLayoutPanel
            // 
            this.mainFlowLayoutPanel.AutoScroll = true;
            this.mainFlowLayoutPanel.Controls.Add(this.invisibleControl2);
            this.mainFlowLayoutPanel.Controls.Add(this.autosavingPanel);
            this.mainFlowLayoutPanel.Controls.Add(this.autoCompilePanel);
            this.mainFlowLayoutPanel.Controls.Add(this.classicSaveCheckBox);
            this.mainFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.mainFlowLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainFlowLayoutPanel.Name = "mainFlowLayoutPanel";
            this.mainFlowLayoutPanel.Size = new System.Drawing.Size(482, 395);
            this.mainFlowLayoutPanel.TabIndex = 19;
            this.mainFlowLayoutPanel.WrapContents = false;
            // 
            // invisibleControl2
            // 
            this.invisibleControl2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.invisibleControl2.Location = new System.Drawing.Point(0, 0);
            this.invisibleControl2.Name = "invisibleControl2";
            this.invisibleControl2.Size = new System.Drawing.Size(482, 0);
            this.invisibleControl2.TabIndex = 0;
            this.invisibleControl2.TabStop = false;
            this.invisibleControl2.Text = "invisibleControl2";
            // 
            // autosavingPanel
            // 
            this.autosavingPanel.Controls.Add(this.autosavingCheckBox);
            this.autosavingPanel.Controls.Add(this.groupBox8);
            this.autosavingPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.autosavingPanel.Location = new System.Drawing.Point(3, 3);
            this.autosavingPanel.Name = "autosavingPanel";
            this.autosavingPanel.Size = new System.Drawing.Size(476, 140);
            this.autosavingPanel.TabIndex = 18;
            // 
            // autosavingCheckBox
            // 
            this.autosavingCheckBox.AutoSize = true;
            this.autosavingCheckBox.Checked = global::SFMTK.Properties.Settings.Default.Autosaving;
            this.autosavingCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autosavingCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "Autosaving", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.autosavingCheckBox.Location = new System.Drawing.Point(8, 1);
            this.autosavingCheckBox.Name = "autosavingCheckBox";
            this.autosavingCheckBox.Size = new System.Drawing.Size(79, 17);
            this.autosavingCheckBox.TabIndex = 0;
            this.autosavingCheckBox.Text = "&Autosaving";
            this.toolTip.SetToolTip(this.autosavingCheckBox, "Enables or disables autosaving functionality for any content loaded into SFMTK.");
            this.autosavingCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.autosaveModeRadioGroupPanel);
            this.groupBox8.Controls.Add(this.panel5);
            this.groupBox8.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", global::SFMTK.Properties.Settings.Default, "Autosaving", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.Enabled = global::SFMTK.Properties.Settings.Default.Autosaving;
            this.groupBox8.Location = new System.Drawing.Point(0, 0);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(476, 140);
            this.groupBox8.TabIndex = 16;
            this.groupBox8.TabStop = false;
            // 
            // autosaveModeRadioGroupPanel
            // 
            this.autosaveModeRadioGroupPanel.Controls.Add(this.internalAutosavesRadioButton);
            this.autosaveModeRadioGroupPanel.Controls.Add(this.individualAutosaveRadioButton);
            this.autosaveModeRadioGroupPanel.Controls.Add(this.overrideAutosaveRadioButton);
            this.autosaveModeRadioGroupPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.autosaveModeRadioGroupPanel.Location = new System.Drawing.Point(8, 52);
            this.autosaveModeRadioGroupPanel.Name = "autosaveModeRadioGroupPanel";
            this.autosaveModeRadioGroupPanel.Selected = 0;
            this.autosaveModeRadioGroupPanel.Size = new System.Drawing.Size(204, 80);
            this.autosaveModeRadioGroupPanel.TabIndex = 0;
            // 
            // internalAutosavesRadioButton
            // 
            this.internalAutosavesRadioButton.AutoSize = true;
            this.internalAutosavesRadioButton.Checked = true;
            this.internalAutosavesRadioButton.Location = new System.Drawing.Point(3, 3);
            this.internalAutosavesRadioButton.Name = "internalAutosavesRadioButton";
            this.internalAutosavesRadioButton.Size = new System.Drawing.Size(112, 17);
            this.internalAutosavesRadioButton.TabIndex = 0;
            this.internalAutosavesRadioButton.TabStop = true;
            this.internalAutosavesRadioButton.Tag = "0";
            this.internalAutosavesRadioButton.Text = "&Internal autosaves";
            this.toolTip.SetToolTip(this.internalAutosavesRadioButton, "A single autosave file is created containing the autosave information about every" +
        " file edited by SFMTK.");
            this.internalAutosavesRadioButton.UseVisualStyleBackColor = true;
            // 
            // individualAutosaveRadioButton
            // 
            this.individualAutosaveRadioButton.AutoSize = true;
            this.individualAutosaveRadioButton.Location = new System.Drawing.Point(3, 26);
            this.individualAutosaveRadioButton.Name = "individualAutosaveRadioButton";
            this.individualAutosaveRadioButton.Size = new System.Drawing.Size(171, 17);
            this.individualAutosaveRadioButton.TabIndex = 1;
            this.individualAutosaveRadioButton.Tag = "1";
            this.individualAutosaveRadioButton.Text = "&Create individual autosave files";
            this.toolTip.SetToolTip(this.individualAutosaveRadioButton, "Creates individual .autosave files for every autosaved content file.");
            this.individualAutosaveRadioButton.UseVisualStyleBackColor = true;
            // 
            // overrideAutosaveRadioButton
            // 
            this.overrideAutosaveRadioButton.AutoSize = true;
            this.overrideAutosaveRadioButton.Location = new System.Drawing.Point(3, 49);
            this.overrideAutosaveRadioButton.Name = "overrideAutosaveRadioButton";
            this.overrideAutosaveRadioButton.Size = new System.Drawing.Size(110, 17);
            this.overrideAutosaveRadioButton.TabIndex = 2;
            this.overrideAutosaveRadioButton.Tag = "2";
            this.overrideAutosaveRadioButton.Text = "&Save over original";
            this.toolTip.SetToolTip(this.overrideAutosaveRadioButton, "Autosave will override the original content file, which may result in data loss.");
            this.overrideAutosaveRadioButton.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.autosaveNumericUpDown);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Location = new System.Drawing.Point(8, 20);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(205, 22);
            this.panel5.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(161, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "minutes";
            // 
            // autosaveNumericUpDown
            // 
            this.autosaveNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.autosaveNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::SFMTK.Properties.Settings.Default, "AutosaveInterval", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.autosaveNumericUpDown.DecimalPlaces = 1;
            this.autosaveNumericUpDown.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.autosaveNumericUpDown.Location = new System.Drawing.Point(84, 0);
            this.autosaveNumericUpDown.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.autosaveNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.autosaveNumericUpDown.Name = "autosaveNumericUpDown";
            this.autosaveNumericUpDown.Size = new System.Drawing.Size(76, 20);
            this.autosaveNumericUpDown.TabIndex = 1;
            this.toolTip.SetToolTip(this.autosaveNumericUpDown, "Specifies an autosave interval, in minutes.");
            this.autosaveNumericUpDown.Value = global::SFMTK.Properties.Settings.Default.AutosaveInterval;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "&Autosave every";
            // 
            // autoCompilePanel
            // 
            this.autoCompilePanel.Controls.Add(this.autoCompileHelpButton);
            this.autoCompilePanel.Controls.Add(this.autoCompileCheckBox);
            this.autoCompilePanel.Controls.Add(this.groupBox9);
            this.autoCompilePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.autoCompilePanel.Location = new System.Drawing.Point(3, 149);
            this.autoCompilePanel.Name = "autoCompilePanel";
            this.autoCompilePanel.Size = new System.Drawing.Size(476, 132);
            this.autoCompilePanel.TabIndex = 20;
            // 
            // autoCompileHelpButton
            // 
            this.commandManager.SetCommand(this.autoCompileHelpButton, "HelpCommand;Autocompiling");
            this.autoCompileHelpButton.Image = ((System.Drawing.Image)(resources.GetObject("autoCompileHelpButton.Image")));
            this.autoCompileHelpButton.Location = new System.Drawing.Point(108, 0);
            this.autoCompileHelpButton.Name = "autoCompileHelpButton";
            this.autoCompileHelpButton.Size = new System.Drawing.Size(24, 24);
            this.autoCompileHelpButton.TabIndex = 1;
            this.toolTip.SetToolTip(this.autoCompileHelpButton, "Displays help about auto-compiling and compile on save.");
            this.autoCompileHelpButton.UseVisualStyleBackColor = true;
            // 
            // autoCompileCheckBox
            // 
            this.autoCompileCheckBox.AutoSize = true;
            this.autoCompileCheckBox.Checked = global::SFMTK.Properties.Settings.Default.CompileOnSave;
            this.autoCompileCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoCompileCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "CompileOnSave", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.autoCompileCheckBox.Location = new System.Drawing.Point(8, 4);
            this.autoCompileCheckBox.Name = "autoCompileCheckBox";
            this.autoCompileCheckBox.Size = new System.Drawing.Size(104, 17);
            this.autoCompileCheckBox.TabIndex = 0;
            this.autoCompileCheckBox.Text = "Compile on sa&ve";
            this.toolTip.SetToolTip(this.autoCompileCheckBox, "SFMTK will automatically compile content whenever it is saved");
            this.autoCompileCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.flowLayoutPanel12);
            this.groupBox9.Controls.Add(this.autoCompileViewButton);
            this.groupBox9.Controls.Add(this.label15);
            this.groupBox9.Location = new System.Drawing.Point(0, 4);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(476, 128);
            this.groupBox9.TabIndex = 16;
            this.groupBox9.TabStop = false;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel12.Controls.Add(this.label13);
            this.flowLayoutPanel12.Controls.Add(this.autoCompileRadioGroupPanel);
            this.flowLayoutPanel12.Controls.Add(this.label14);
            this.flowLayoutPanel12.Controls.Add(this.flowLayoutPanel9);
            this.flowLayoutPanel12.Controls.Add(this.label20);
            this.flowLayoutPanel12.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(4, 20);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(469, 80);
            this.flowLayoutPanel12.TabIndex = 22;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(125, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "&If started while compiling:";
            // 
            // autoCompileRadioGroupPanel
            // 
            this.autoCompileRadioGroupPanel.Controls.Add(this.restartAutoCompileRadioButton);
            this.autoCompileRadioGroupPanel.Controls.Add(this.ignoreAutoCompileRadioButton);
            this.autoCompileRadioGroupPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.autoCompileRadioGroupPanel.Location = new System.Drawing.Point(3, 16);
            this.autoCompileRadioGroupPanel.Name = "autoCompileRadioGroupPanel";
            this.autoCompileRadioGroupPanel.Selected = 0;
            this.autoCompileRadioGroupPanel.Size = new System.Drawing.Size(141, 52);
            this.autoCompileRadioGroupPanel.TabIndex = 1;
            // 
            // restartAutoCompileRadioButton
            // 
            this.restartAutoCompileRadioButton.AutoSize = true;
            this.restartAutoCompileRadioButton.Checked = true;
            this.restartAutoCompileRadioButton.Location = new System.Drawing.Point(3, 3);
            this.restartAutoCompileRadioButton.Name = "restartAutoCompileRadioButton";
            this.restartAutoCompileRadioButton.Size = new System.Drawing.Size(106, 17);
            this.restartAutoCompileRadioButton.TabIndex = 0;
            this.restartAutoCompileRadioButton.TabStop = true;
            this.restartAutoCompileRadioButton.Tag = "0";
            this.restartAutoCompileRadioButton.Text = "&Restart compiling";
            this.toolTip.SetToolTip(this.restartAutoCompileRadioButton, "Cancels the active compile and restarts the compilation of the newly saved file.");
            this.restartAutoCompileRadioButton.UseVisualStyleBackColor = true;
            // 
            // ignoreAutoCompileRadioButton
            // 
            this.ignoreAutoCompileRadioButton.AutoSize = true;
            this.ignoreAutoCompileRadioButton.Location = new System.Drawing.Point(3, 26);
            this.ignoreAutoCompileRadioButton.Name = "ignoreAutoCompileRadioButton";
            this.ignoreAutoCompileRadioButton.Size = new System.Drawing.Size(77, 17);
            this.ignoreAutoCompileRadioButton.TabIndex = 1;
            this.ignoreAutoCompileRadioButton.Tag = "1";
            this.ignoreAutoCompileRadioButton.Text = "&Do nothing";
            this.toolTip.SetToolTip(this.ignoreAutoCompileRadioButton, "Do not try to auto-compile content if saving while compiling.");
            this.ignoreAutoCompileRadioButton.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(150, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "&Which content to consider:";
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel9.Controls.Add(this.autoCompileModelsCheckBox);
            this.flowLayoutPanel9.Controls.Add(this.autoCompileTexturesCheckBox);
            this.flowLayoutPanel9.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(150, 16);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(135, 48);
            this.flowLayoutPanel9.TabIndex = 3;
            // 
            // autoCompileModelsCheckBox
            // 
            this.autoCompileModelsCheckBox.AutoSize = true;
            this.autoCompileModelsCheckBox.Checked = global::SFMTK.Properties.Settings.Default.CompileOnSaveModels;
            this.autoCompileModelsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoCompileModelsCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "CompileOnSaveModels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.autoCompileModelsCheckBox.Location = new System.Drawing.Point(3, 3);
            this.autoCompileModelsCheckBox.Name = "autoCompileModelsCheckBox";
            this.autoCompileModelsCheckBox.Size = new System.Drawing.Size(60, 17);
            this.autoCompileModelsCheckBox.TabIndex = 0;
            this.autoCompileModelsCheckBox.Text = "&Models";
            this.toolTip.SetToolTip(this.autoCompileModelsCheckBox, "Models are compiled if the QC does not contain errors");
            this.autoCompileModelsCheckBox.UseVisualStyleBackColor = true;
            // 
            // autoCompileTexturesCheckBox
            // 
            this.autoCompileTexturesCheckBox.AutoSize = true;
            this.autoCompileTexturesCheckBox.Checked = global::SFMTK.Properties.Settings.Default.CompileOnSaveTextures;
            this.autoCompileTexturesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoCompileTexturesCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "CompileOnSaveTextures", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.autoCompileTexturesCheckBox.Location = new System.Drawing.Point(3, 26);
            this.autoCompileTexturesCheckBox.Name = "autoCompileTexturesCheckBox";
            this.autoCompileTexturesCheckBox.Size = new System.Drawing.Size(67, 17);
            this.autoCompileTexturesCheckBox.TabIndex = 1;
            this.autoCompileTexturesCheckBox.Text = "&Textures";
            this.toolTip.SetToolTip(this.autoCompileTexturesCheckBox, "Textures are converted to VTF files");
            this.autoCompileTexturesCheckBox.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(294, 6);
            this.label20.Margin = new System.Windows.Forms.Padding(6);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(153, 39);
            this.label20.TabIndex = 4;
            this.label20.Text = "These settings apply to both auto-compiling and compile on save.";
            // 
            // autoCompileViewButton
            // 
            this.autoCompileViewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.commandManager.SetCommand(this.autoCompileViewButton, "");
            this.autoCompileViewButton.Location = new System.Drawing.Point(341, 100);
            this.autoCompileViewButton.Name = "autoCompileViewButton";
            this.autoCompileViewButton.Size = new System.Drawing.Size(124, 24);
            this.autoCompileViewButton.TabIndex = 1;
            this.autoCompileViewButton.Text = "&Edit Active Setup";
            this.toolTip.SetToolTip(this.autoCompileViewButton, "Opens the active setup\'s settings to review the additional auto-compile settings." +
        "");
            this.autoCompileViewButton.UseVisualStyleBackColor = true;
            this.autoCompileViewButton.Click += new System.EventHandler(this.autoCompileViewButton_Click);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoEllipsis = true;
            this.label15.Location = new System.Drawing.Point(12, 104);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(323, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Auto-compile settings are found in each Setup\'s configuration.";
            // 
            // classicSaveCheckBox
            // 
            this.classicSaveCheckBox.AutoSize = true;
            this.classicSaveCheckBox.Checked = global::SFMTK.Properties.Settings.Default.SimpleSaveConfirmation;
            this.classicSaveCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Properties.Settings.Default, "SimpleSaveConfirmation", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.classicSaveCheckBox.Location = new System.Drawing.Point(3, 287);
            this.classicSaveCheckBox.Name = "classicSaveCheckBox";
            this.classicSaveCheckBox.Size = new System.Drawing.Size(171, 17);
            this.classicSaveCheckBox.TabIndex = 0;
            this.classicSaveCheckBox.Text = "&Use classic save confirmations";
            this.toolTip.SetToolTip(this.classicSaveCheckBox, "When closing with unsaved documents opened, show a simple save prompt per file in" +
        "stead of the \"modern\" save dialog.");
            this.classicSaveCheckBox.UseVisualStyleBackColor = true;
            // 
            // commandManager
            // 
            this.commandManager.CommandHistory = null;
            // 
            // SavingPreferencePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainFlowLayoutPanel);
            this.Name = "SavingPreferencePage";
            this.Size = new System.Drawing.Size(482, 395);
            this.mainFlowLayoutPanel.ResumeLayout(false);
            this.mainFlowLayoutPanel.PerformLayout();
            this.autosavingPanel.ResumeLayout(false);
            this.autosavingPanel.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.autosaveModeRadioGroupPanel.ResumeLayout(false);
            this.autosaveModeRadioGroupPanel.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.autosaveNumericUpDown)).EndInit();
            this.autoCompilePanel.ResumeLayout(false);
            this.autoCompilePanel.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.flowLayoutPanel12.ResumeLayout(false);
            this.flowLayoutPanel12.PerformLayout();
            this.autoCompileRadioGroupPanel.ResumeLayout(false);
            this.autoCompileRadioGroupPanel.PerformLayout();
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel mainFlowLayoutPanel;
        private InvisibleControl invisibleControl2;
        private System.Windows.Forms.Panel autosavingPanel;
        private System.Windows.Forms.CheckBox autosavingCheckBox;
        private System.Windows.Forms.GroupBox groupBox8;
        private RadioGroupPanel autosaveModeRadioGroupPanel;
        private System.Windows.Forms.RadioButton internalAutosavesRadioButton;
        private System.Windows.Forms.RadioButton individualAutosaveRadioButton;
        private System.Windows.Forms.RadioButton overrideAutosaveRadioButton;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown autosaveNumericUpDown;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel autoCompilePanel;
        private System.Windows.Forms.Button autoCompileHelpButton;
        private System.Windows.Forms.CheckBox autoCompileCheckBox;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.Label label13;
        private RadioGroupPanel autoCompileRadioGroupPanel;
        private System.Windows.Forms.RadioButton restartAutoCompileRadioButton;
        private System.Windows.Forms.RadioButton ignoreAutoCompileRadioButton;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.CheckBox autoCompileModelsCheckBox;
        private System.Windows.Forms.CheckBox autoCompileTexturesCheckBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button autoCompileViewButton;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox classicSaveCheckBox;
        private System.Windows.Forms.ToolTip toolTip;
        private Commands.CommandManager commandManager;
    }
}