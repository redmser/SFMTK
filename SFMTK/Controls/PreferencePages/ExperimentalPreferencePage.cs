﻿using System;

namespace SFMTK.Controls.PreferencePages
{
    public partial class ExperimentalPreferencePage : PreferencePage
    {
        public ExperimentalPreferencePage()
        {
        }

        public override void OnFirstSelect(object sender, EventArgs e)
        {
            InitializeComponent();

            base.OnFirstSelect(sender, e);
        }

        public override string Path => "Experimental";

        public override System.Drawing.Image Image => Properties.Resources.flag_yellow;

        public override System.Windows.Forms.Control ExtendControl => this.mainFlowLayoutPanel;

        private void configFileButton_Click(object sender, EventArgs e)
        {
            //Open userconfig folder
            IOHelper.OpenInExplorer(Properties.Settings.UserConfig.FilePath, false);
        }
    }
}
