﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using SFMTK.Modules;

namespace SFMTK.Controls.PreferencePages
{
    /// <summary>
    /// A page in the preferences dialog.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.UserControl" />
    public abstract partial class PreferencePage : UserControl, IThemeable, IPreferencePage
    {
        //TODO: PreferencePage - allow Pre/PostApplySettings to report progress -> applying settings waiting time should be transparent to the user
        //  (this should also include whatever is done with redirected properties and so on)

        /// <summary>
        /// Initializes a new instance of the <see cref="PreferencePage"/> class.
        /// </summary>
        public PreferencePage()
        {
            //Hide by default
            this.Visible = false;
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control. This property is never set to the same value that it already has.
        /// </summary>
        [Browsable(false)]
        public string ActiveTheme { get; set; }

        /// <summary>
        /// Gets the path to this preference page. Use slashes '/' to signify nested pages.
        /// </summary>
        [Browsable(false)]
        public abstract string Path { get; }

        /// <summary>
        /// Gets the control to allow for extending the contents of a preference page. Most commonly a FlowLayoutPanel.
        /// </summary>
        [Browsable(false)]
        public virtual Control ExtendControl { get; }
        
        /// <summary>
        /// Gets or sets the text associated with this control.
        /// </summary>
        [Browsable(false)]
        public override string Text
        {
            get
            {
                var path = this.Path;
                if (path.EndsWith("/"))
                    path = path.Substring(0, path.Length - 1);

                var lastind = path.LastIndexOf('/');
                if (lastind < 0)
                    return path;
                return path.Substring(lastind + 1);
            }
        }

        /// <summary>
        /// Gets or sets an optional image to associate to the preference page.
        /// </summary>
        [Browsable(false)]
        public virtual Image Image { get; } = Properties.Resources.table;

        /// <summary>
        /// Gets the path to the page to select.
        /// </summary>
        [Browsable(false)]
        public string TargetPage => this.Path;

        /// <summary>
        /// Gets or sets whether this element is a root element in the tree view.
        /// </summary>
        [Browsable(false)]
        public bool IsRoot { get; set; }

        /// <summary>
        /// Gets or sets whether this node should be visible after filtering through a search.
        /// </summary>
        [Browsable(false)]
        public bool Filter { get; set; }

        /// <summary>
        /// Gets or sets the list of controls that got caught by the filter text.
        /// </summary>
        [Browsable(false)]
        public List<Control> FilterControls { get; set; }

        /// <summary>
        /// Called before the settings were applied and saved successfully.
        /// </summary>
        public virtual async Task OnPreApplySettings(object sender, EventArgs e) { }

        /// <summary>
        /// Called after the settings were applied and saved successfully.
        /// </summary>
        public virtual async Task OnPostApplySettings(object sender, EventArgs e) { }

        /// <summary>
        /// Called when the Preference's Load event is fired. Allows for initializing a single time, with the dialog already set up.
        /// </summary>
        public virtual void OnPreferencesLoad(object sender, EventArgs e) => this.firstselect = true;

        /// <summary>
        /// Whether <see cref="OnFirstSelect(object, EventArgs)"/> was not invoked yet (or is being invoked).
        /// </summary>
        protected bool firstselect = true;

        /// <summary>
        /// Called if <see cref="OnSelect(object, EventArgs)"/> was called for the first time.
        /// </summary>
        /// <param name="sender">Original sender of the event.</param>
        public virtual void OnFirstSelect(object sender, EventArgs e)
        {
            //Extend page
            foreach (var mod in Program.MainMM.LoadedModules.OfType<IModuleSettings>())
            {
                var props = mod.GetAdditionalProperties()?.ToList();
                if (props == null)
                    continue;

                if (props.Count > ModuleManager.MaxAdditionalProperties)
                    ModuleManager.ThrowModulePropertyException(mod,
                        $"AdditionalProperties specifies too many entries ({props.Count} > {ModuleManager.MaxAdditionalProperties})");

                foreach (var prop in props)
                {
                    if (prop.Key == this.Path)
                    {
                        if (this.ExtendControl == null)
                            ModuleManager.ThrowModulePropertyException(mod,
                                $"AdditionalProperties points to a property page which does not have any ExtendControl set ({prop.Key}).");

                        this.ExtendControl.Controls.Add(prop.Value);
                        break;
                    }
                }
            }

            //Apply theme
            //TODO: PreferencePage - Load OLV state?
            //BUG: PreferencePage - theme loading seems really buggy, in that each page has some controls that just aren't themed somewhy
            UITheme.ApplyTheme(this);
        }

        /// <summary>
        /// Called when this page was selected and will be displayed in the Preferences.
        /// </summary>
        public virtual void OnSelect(object sender, EventArgs e)
        {
            if (this.firstselect)
            {
                this.OnFirstSelect(sender, e);
                this.firstselect = false;
            }
        }
    }
}
