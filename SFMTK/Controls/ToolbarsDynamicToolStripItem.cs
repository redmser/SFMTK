﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// <see cref="DynamicToolStripItem"/> for populating the list of main form toolbars.
    /// </summary>
    public class ToolbarsDynamicToolStripItem : DynamicToolStripItem
    {
        //FIXME: ToolbarsDynamicToolStripItem - disable autoreload of items, only refresh whenever modules are reloaded
        //  -> we can hide/show toolbars in various ways, besides the menu, so be sure to cover those cases as well then!

        /// <summary>
        /// Returns the list of <see cref="ToolStripItem" />s that this item would be replaced
        /// with if it were to be displayed as an entry somewhere.
        /// </summary>
        public override IEnumerable<ToolStripItem> GetDynamicItems()
        {
            foreach (var toolstrip in Program.MainForm.ToolBars)
            {
                //Populate toolbars context menu
                yield return MenuItemFromToolStrip(toolstrip, true);
            }

            //Static entries - we generate them anyway, since the checked states may change on their own
            yield return new ToolStripSeparator();
            yield return MenuItemFromToolStrip(Program.MainForm.mainStatusStrip, false);
            yield return MenuItemFromToolStrip(Program.MainForm.mainMenuBar, false);
        }

        private static ToolStripMenuItem MenuItemFromToolStrip(ToolStrip toolstrip, bool toolbar)
        {
            var tsi = new ToolStripMenuItem("&" + toolstrip.Text);
            tsi.Name = toolstrip.Name;
            tsi.Click += UpdateToolStripVisibility;
            tsi.Checked = toolstrip.Visible;

            var typestr = (toolbar ? " toolbar" : string.Empty);
            tsi.ToolTipText = $"Enables or disables visibility of the {toolstrip.Text}{typestr}";
            return tsi;
        }
        
        /// <summary>
        /// Updates the tool strip visibility based on the tooltip item toggles of the View menu.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void UpdateToolStripVisibility(object sender, System.EventArgs e)
        {
            var item = sender as ToolStripMenuItem;
            if (item == null)
            {
                throw new System.ArgumentException("The clicked item was not a ToolStripItem, but a " + sender.GetType().Name);
            }

            var name = item.Name;
            var ctrl = ToolStripManager.FindToolStrip(name);
            if (ctrl == null)
            {
                throw new System.ArgumentException("Could not find the specified ToolStrip " + name);
            }

            //Toggle visibility
            ctrl.Visible = !ctrl.Visible;
        }
    }
}
