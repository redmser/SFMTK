﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SFMTK.Controls
{
    /// <summary>
    /// Similar to the MenuButton, except that the menu sets the default action for the button instead of being extra actions.
    /// <para />Rather than handling the Click event of the button, you should handle the menu entries' Click events, since those get raised even by the button itself being pressed.
    /// </summary>
    public class OptionButton : MenuButton
    {
        private string _text;
        private bool _useFallbackText;
        private ToolStripItem _defaultAction;

        /// <summary>
        /// Specifies the default action to run when the button is pressed.
        /// </summary>
        [DefaultValue(null), Category("Menu"),
         Description("Specifies the default action to run when the button is pressed.")]
        public ToolStripItem DefaultAction
        {
            get { return this._defaultAction; }
            set
            {
                if (this._defaultAction == value)
                    return;

                this._defaultAction = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// Returns true.
        /// </summary>
        [Browsable(false)]
        public override bool SplitButton => true;

        /// <summary>
        /// Whether to always use the fallback text instead of the DefaultAction text.
        /// </summary>
        [DefaultValue(false), Category("Menu"),
         Description("Whether to always use the fallback text instead of the DefaultAction text.")]
        public virtual bool UseFallbackText
        {
            get { return this._useFallbackText; }
            set
            {
                if (this._useFallbackText == value)
                    return;

                this._useFallbackText = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// Returns true.
        /// </summary>
        public override bool MenuCancelsClick => true;

        /// <summary>
        /// Returns false.
        /// </summary>
        public override bool MenuRaisesButtonClick => false;

        /// <returns>
        /// The fallback text associated with this control.
        /// </returns>
        public override string Text
        {
            get
            {
                if (this.UseFallbackText || this.DefaultAction == null)
                    return this._text;
                return this.DefaultAction.Text;
            }
            set { this._text = value; }
        }

        /// <summary>
        /// Raises the <see cref="E:MenuItemClicked" /> event.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The <see cref="ToolStripItemClickedEventArgs" /> instance containing the event data.</param>
        protected override void OnMenuItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            base.OnMenuItemClicked(sender, e);

            this.DefaultAction = e.ClickedItem;
        }

        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data. </param>
        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);

            if (this.automatedClick)
            {
                this.automatedClick = false;
            }
            else if (!this.cancelClick)
            {
                this.DefaultAction?.PerformClick();
            }
        }
    }
}
