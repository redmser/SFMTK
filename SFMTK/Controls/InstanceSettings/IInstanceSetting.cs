﻿namespace SFMTK.Controls.InstanceSettings
{
    /// <summary>
    /// Allows the user to change the properties of a to-be-created or converted instance.
    /// If you want this shown in UI, it has to be of type <see cref="System.Windows.Forms.Control" />.
    /// </summary>
    /// <typeparam name="T">The type of object holding the instance setting, such as <see cref="SFMTK.Contents.Content"/> or a content converter interface.</typeparam>
    public interface IInstanceSetting<T>
    {
        //TODO: IInstanceSetting - should be reworked to allow setting properties programatically (possibly by having a dict of all properties and values,
        //  and those being applied with the apply instance settings method - the controls should just update the dict values)

        /// <summary>
        /// Applies the user-specified settings to the specified content instance.
        /// <para/>Returns an error message caused by an invalid value (or <c>null</c>, if all values are valid).
        /// </summary>
        /// <param name="obj">The object instance to apply the user-specified properties to. Can safely be casted to the type you are working with, if certain.</param>
        string ApplyInstanceSettings(T obj);

        /// <summary>
        /// Gets the ToolTipText to give to the control when added to the settings list.
        /// </summary>
        string ToolTipText { get; }
    }
}
