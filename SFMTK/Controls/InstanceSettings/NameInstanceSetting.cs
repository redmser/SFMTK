﻿using System.Windows.Forms;
using SFMTK.Contents;

namespace SFMTK.Controls.InstanceSettings
{
    /// <summary>
    /// Allows the user to give the content a custom file name. Extension may be omitted.
    /// </summary>
    public partial class NameInstanceSetting : UserControl, IInstanceSetting<Content>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NameInstanceSetting"/> class.
        /// </summary>
        protected NameInstanceSetting()
        {
            //Compiler-generated
            InitializeComponent();

            //Dock to top
            this.Dock = DockStyle.Top;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NameInstanceSetting"/> class.
        /// </summary>
        /// <param name="selection">The selected content instance.</param>
        public NameInstanceSetting(Content selection) : this()
        {
            this.nameTextBox.Text = selection.GetNewName();
        }

        /// <summary>
        /// Gets the ToolTipText to give to the control when added to the settings list.
        /// </summary>
        public string ToolTipText => "The file name of the document to be created.";

        /// <summary>
        /// Applies the user-specified settings to the specified content instance.
        /// <para />Returns an error message caused by an invalid value (or <c>null</c>, if all values are valid).
        /// </summary>
        /// <param name="obj">The object instance to apply the user-specified properties to. Can safely be casted to the type you are working with, if certain.</param>
        public string ApplyInstanceSettings(Content obj)
        {
            if (string.IsNullOrWhiteSpace(this.nameTextBox.Text))
                return "File name may not be empty.";

            obj.FilePath = this.nameTextBox.Text;
            //TODO: NameInstanceSetting - validation on path
            return null;
        }
    }
}
