﻿using BrightIdeasSoftware;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace SFMTK
{
    public partial class UITheme
    {
        /// <summary>
        /// Theme to use for all ToolStrips and DockPanels. Should not be serialized for now.
        /// </summary>
        [Browsable(false)]
        public ThemeBase StripTheme { get; set; }

        /// <summary>
        /// Size for tree line connections in lists.
        /// </summary>
        [Description("Size for tree line connections in lists."), Category("Base")]
        public int ListTreeConnectionWidth { get; set; } = 1;

        /// <summary>
        /// A pen with ListTreeConnectionColor.
        /// </summary>
        [Browsable(false)]
        public Pen ListTreeConnectionPen { get; set; }

        /// <summary>
        /// The image to use for the search bar button.
        /// </summary>
        [Description("The image to use for the search bar button."), Category("Images")]
        public Image SearchButtonImage { get; set; } = Properties.Resources.magnifier;

        /// <summary>
        /// The image to use for the search bar delete button.
        /// </summary>
        [Description("The image to use for the search bar delete button."), Category("Images")]
        public Image SearchDeleteButtonImage { get; set; } = Properties.Resources.magnifier_cross;

        /// <summary>
        /// Wherever possible, try using visual styles for rendering controls if they do not need custom theming.
        /// </summary>
        [Description("Wherever possible, try using visual styles for rendering controls if they do not need custom theming."), Category("Base")]
        public bool UseVisualStyles { get; set; } = false;

        /// <summary>
        /// Forces the <see cref="UseVisualStyles"/> option even for controls that usually do not need it, such as CheckBoxes or RadioButtons.
        /// </summary>
        [Description("Forces the \"UseVisualStyles\" option even for controls that usually do not need it, such as CheckBoxes or RadioButtons."), Category("Base")]
        public bool ForceVisualStyles { get; set; } = false;

        /// <summary>
        /// Show line numbers in text editors.
        /// </summary>
        [Description("Show line numbers in text editors."), Category("Editor")]
        public bool ShowLineNumbers { get; set; } = true;

        /// <summary>
        /// How buttons should be styled.
        /// </summary>
        [Description("How buttons should be styled."), Category("Base")]
        public FlatStyle ButtonStyle { get; set; } = FlatStyle.Flat;

        /// <summary>
        /// Whether to show icons for docked documents.
        /// </summary>
        [Description("Whether to show icons for docked documents."), Category("Base")]
        public bool ShowDocumentIcons { get; set; } = true;

        /// <summary>
        /// Where tabs should be placed on docked items.
        /// </summary>
        [Description("Where tabs should be placed on docked items."), Category("Base")]
        public DocumentTabStripLocation TabLocation { get; set; } = DocumentTabStripLocation.Top;

        /// <summary>
        /// How flat buttons should be styled. Must use base implementation modification because ugh.
        /// </summary>
        [Description("How flat buttons should be styled."), Category("Base")]
        public FlatButtonAppearance ButtonAppearance { get; set; } = new Button().FlatAppearance;

        /// <summary>
        /// Rendering of list columns.
        /// </summary>
        [Description("Rendering of list columns."), Category("Base")]
        public HeaderFormatStyle ColumnStyle { get; set; } = new HeaderFormatStyle();

        /// <summary>
        /// Whether to show gridlines on any list that supports it.
        /// </summary>
        [Description("Whether to display grid lines on any list that supports it. Their color can not be modified, and they may not show up under certain circumstances."),
            Category("Base")]
        public bool GridLinesInLists { get; set; } = false;

        /// <summary>
        /// Lists use two background colors that alternate for every list entry.
        /// </summary>
        [Description("If enabled, lists use two background colors that alternate for every list entry. Secondary color is defined in ListAlternateBackColor."),
            Category("Base")]
        public bool AlternatingListBackground { get; set; } = false;

        /// <summary>
        /// Font shown for status messages, mostly inside of docks.
        /// </summary>
        [Description("Font shown for status messages, mostly inside of docks."), Category("Fonts")]
        public Font StatusTextFont { get; set; } = new Font(SystemFonts.DefaultFont.FontFamily, SystemFonts.DefaultFont.SizeInPoints * 1.2f);

        /// <summary>
        /// Font used for pretty much all elements.
        /// </summary>
        [Description("Font used for pretty much all elements."), Category("Fonts")]
        public Font MainFont { get; set; } = SystemFonts.DefaultFont;

        /// <summary>
        /// How big, in pixels, the corner button should be that appears when the main menu bar is hidden. Use 0 to disable.
        /// </summary>
        [Description("How big, in pixels, the corner button should be that appears when the main menu bar is hidden. Use 0 to disable."), Category("Base")]
        public int ReopenMenuSize { get; set; } = 8;

        /// <summary>
        /// A brush with MainTextColor.
        /// </summary>
        [Browsable(false)]
        public Brush TextBrush { get; set; }

        /// <summary>
        /// A brush with MainBackColor.
        /// </summary>
        [Browsable(false)]
        public Brush BackBrush { get; set; }

        /// <summary>
        /// A brush with HighlightBackColor.
        /// </summary>
        [Browsable(false)]
        public Brush HighlightBackBrush { get; set; }
    }
}
