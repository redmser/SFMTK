﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WeifenLuo.WinFormsUI.Docking;

namespace SFMTK
{
    /// <summary>
    /// A converter between UIThemes and their JSON representation.
    /// </summary>
    public class UIThemeConverter : JsonConverter
    {
        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        /// <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType) => objectType == typeof(UITheme);

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader" /> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being read.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>
        /// The object value.
        /// </returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var theme = new UITheme();
            var jsonObject = JObject.Load(reader);

            //Set name of theme
            theme.Name = jsonObject["Name"].Value<string>();

            //Get list of named colors
            if (jsonObject.TryGetValue("Colors", out var colors))
            {
                var filedic = colors.OfType<JProperty>().ToDictionary(key => key.Name, value => ValuesToColor(value.ToObject<string>()));
                theme.Colors = filedic;
            }

            //How to convert the settings
            if (jsonObject.TryGetValue("Settings", out var settings))
            {
                foreach (var prop in settings.ToObject<JObject>().Properties())
                {
                    var stringvalue = prop.Value.ToObject<string>();
                    switch (prop.Name)
                    {
                        case "StripTheme":
                            //Create object of that type
                            Type type = null;
                            foreach (var assembly in _dockPanelAssemblies)
                            {
                                type = assembly.GetType("WeifenLuo.WinFormsUI.Docking." + stringvalue, false, true);
                                if (type != null)
                                    break;
                            }
                            if (type == null)
                                throw new ArgumentNullException(nameof(type), $"Unknown type name \"{type.Name}\" specified for StripTheme - not found in any registered DockPanel libraries.");
                            var instance = Activator.CreateInstance(type) as ThemeBase;
                            if (instance == null)
                                throw new ArgumentNullException(nameof(type), $"Unable to create an instance of the type \"{type.Name}\" for StripTheme. May not be assignable from ThemeBase!");
                            theme.StripTheme = instance;
                            break;
                        case "ButtonBorderSize":
                            SetupButtonAppearance();
                            theme.ButtonAppearance.BorderSize = int.Parse(stringvalue);
                            break;
                        case "ButtonBorderColor":
                            SetupButtonAppearance();
                            theme.ButtonAppearance.BorderColor = NameToColor(stringvalue);
                            break;
                        case "ButtonCheckedBackColor":
                            SetupButtonAppearance();
                            theme.ButtonAppearance.CheckedBackColor = NameToColor(stringvalue);
                            break;
                        case "ButtonMouseDownBackColor":
                            SetupButtonAppearance();
                            theme.ButtonAppearance.MouseDownBackColor = NameToColor(stringvalue);
                            break;
                        case "ButtonMouseOverBackColor":
                            SetupButtonAppearance();
                            theme.ButtonAppearance.MouseOverBackColor = NameToColor(stringvalue);
                            break;
                        case "IsSystemDefault":
                            //Is system default theme!
                            UITheme.SystemDefaultTheme = theme;
                            break;
                        default:
                            //Transfer remaining settings over to UITheme
                            var propinfo = _uiThemeProperties.First(p => p.Name == prop.Name);
                            if (propinfo.PropertyType == typeof(Color))
                            {
                                //Convert using StringToColor
                                propinfo.SetValue(theme, NameToColor(stringvalue));
                            }
                            else
                            {
                                //Attempt default conversion
                                var conv = TypeDescriptor.GetConverter(propinfo.PropertyType);
                                if (conv == null)
                                    throw new InvalidCastException($"Unable to find converter from \"string\" to the type \"{propinfo.PropertyType.Name}\".");
                                try
                                {
                                    propinfo.SetValue(theme, conv.ConvertFrom(stringvalue));
                                }
                                catch (NotSupportedException ex)
                                {
                                    throw new InvalidCastException($"Unable to convert the value \"{stringvalue}\" to the type \"{propinfo.PropertyType.Name}\"!", ex);
                                }
                            }
                            break;
                    }
                }
            }

            theme.DoInitialize();

            return theme;



            void SetupButtonAppearance()
            {
                if (theme.ButtonAppearance == null)
                    theme.ButtonAppearance = new Button().FlatAppearance;
            }

            Color NameToColor(string color)
            {
                if (string.IsNullOrEmpty(color))
                    return Color.Empty;

                if (theme.Colors.TryGetValue(color, out var retval))
                    return retval;
                throw new KeyNotFoundException($"Named color \"{color}\" could not be found!");
            }

            Color ValuesToColor(string color)
            {
                var split = color.Split(' ');
                return Color.FromArgb(int.Parse(split[0]), int.Parse(split[1]), int.Parse(split[2]));
            }
        }

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter" /> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The calling serializer.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            //NYI: UIThemeConverter - create JSON file from UITheme instance
            throw new NotImplementedException();
        }

        private static Assembly[] _dockPanelAssemblies = new[]
        {
            typeof(ThemeBase).Assembly,
            typeof(VS2012DarkTheme).Assembly,
            typeof(VS2013DarkTheme).Assembly,
            typeof(VS2015DarkTheme).Assembly,
        };

        private static PropertyInfo[] _uiThemeProperties = typeof(UITheme).GetProperties();
    }
}
