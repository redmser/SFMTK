﻿namespace SFMTK
{
    /// <summary>
    /// Any top-level control which will be selected for theming (Forms and Widgets) should implement this interface to cache the selected theme locally as well as be able to
    /// determine how to handle a theme change with specific controls.
    /// </summary>
    public interface IThemeable
    {
        /// <summary>
        /// Gets or sets the theme currently active on this control. This property is never set to the same value that it already has.
        /// </summary>
        string ActiveTheme { get; set; }
    }
}
