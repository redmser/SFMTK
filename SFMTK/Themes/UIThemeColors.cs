﻿using System.ComponentModel;
using System.Drawing;

namespace SFMTK
{
    public partial class UITheme
    {
        /// <summary>
        /// General background color. Used for most dialogs and elements.
        /// </summary>
        [Description("General background color. Used for most dialogs and elements."), Category("Colors")]
        public Color MainBackColor { get; set; } = SystemColors.Window;

        /// <summary>
        /// General foreground/text color. Used for most dialogs and elements.
        /// </summary>
        [Description("General foreground/text color. Used for most dialogs and elements."), Category("Colors")]
        public Color MainTextColor { get; set; } = SystemColors.ControlText;

        /// <summary>
        /// A darker background color for less relevant elements.
        /// </summary>
        [Description("A darker background color for less relevant elements."), Category("Colors")]
        public Color DarkBackColor { get; set; } = SystemColors.ControlDark;

        /// <summary>
        /// Foreground/text color for less relevant elements, such as descriptions.
        /// </summary>
        [Description("Foreground/text color for less relevant elements, such as descriptions."), Category("Colors")]
        public Color SubTextColor { get; set; } = SystemColors.ControlDarkDark;

        /// <summary>
        /// Background color for buttons of most dialogs and elements. Defaults to this.MainBackColor.
        /// </summary>
        [Description("Background color for buttons of most dialogs and elements."), Category("Colors")]
        public Color ButtonBackColor
        {
            get
            {
                if (this._buttonBackColor.HasValue)
                    return this._buttonBackColor.Value;
                return this.MainBackColor;
            }
            set => this._buttonBackColor = value;
        }
        private Color? _buttonBackColor = null;

        /// <summary>
        /// Foreground/text color for buttons of most dialogs and elements. Defaults to this.MainTextColor.
        /// </summary>
        [Description("Foreground/text color for buttons of most dialogs and elements."), Category("Colors")]
        public Color ButtonTextColor
        {
            get
            {
                if (this._buttonTextColor.HasValue)
                    return this._buttonTextColor.Value;
                return this.MainTextColor;
            }
            set => this._buttonTextColor = value;
        }
        private Color? _buttonTextColor = null;

        /// <summary>
        /// Foreground/text color for group boxes and list groups.
        /// </summary>
        [Description("Foreground/text color for group boxes."), Category("Colors")]
        public Color GroupTextColor
        {
            get
            {
                //Special case regarding certain themes where group text would be invisible
                if (SystemColors.ActiveCaptionText.ToArgb() == SystemColors.Window.ToArgb())
                    return SystemColors.WindowText;
                return this._groupTextColor;
            }
            set => this._groupTextColor = value;
        }
        private Color _groupTextColor = SystemColors.ActiveCaptionText;

        /// <summary>
        /// Background color for any lists. Defaults to this.MainBackColor
        /// </summary>
        [Description("Background color for any lists."), Category("Colors")]
        public Color ListBackColor
        {
            get
            {
                if (this._listBackColor.HasValue)
                    return this._listBackColor.Value;
                return this.MainBackColor;
            }
            set => this._listBackColor = value;
        }
        private Color? _listBackColor = null;

        /// <summary>
        /// Alternating background color for any lists.
        /// </summary>
        [Description("An alternate background color for lists. Only used if AlternatingListBackground is enabled."), Category("Colors")]
        public Color ListAlternateBackColor { get; set; } = SystemColors.ControlLight;

        /// <summary>
        /// Foreground/text color for any lists. Defaults to this.MainTextColor
        /// </summary>
        [Description("Foreground/text color for any lists."), Category("Colors")]
        public Color ListTextColor
        {
            get
            {
                if (this._listTextColor.HasValue)
                    return this._listTextColor.Value;
                return this.MainTextColor;
            }
            set => this._listTextColor = value;
        }
        private Color? _listTextColor = null;


        /// <summary>
        /// Color for the marker which appears when reordering a list or inserting entries into it. Defaults to this.MainTextColor
        /// </summary>
        [Description("Color for the marker which appears when reordering a list or inserting entries into it."), Category("Colors")]
        public Color ListInsertionMarkColor
        {
            get
            {
                if (this._listInsertionMarkColor.HasValue)
                    return this._listInsertionMarkColor.Value;
                return this.MainTextColor;
            }
            set => this._listInsertionMarkColor = value;
        }
        private Color? _listInsertionMarkColor = null;

        /// <summary>
        /// Color for tree line connections in lists. Defaults to this.ListInsertionMarkColor
        /// </summary>
        [Description("Color for tree line connections in lists."), Category("Colors")]
        public Color ListTreeConnectionColor
        {
            get
            {
                if (this._listTreeConnectionColor.HasValue)
                    return this._listTreeConnectionColor.Value;
                return this.ListInsertionMarkColor;
            }
            set => this._listTreeConnectionColor = value;
        }
        private Color? _listTreeConnectionColor = null;

        /// <summary>
        /// Color for text links.
        /// </summary>
        [Description("Color for text links."), Category("Colors")]
        public Color LinkColor { get; set; } = Color.Blue;

        /// <summary>
        /// Color when a link was already visited.
        /// </summary>
        [Description("Color when a link was already visited."), Category("Colors")]
        public Color LinkVisitedColor { get; set; } = Color.Purple;

        /// <summary>
        /// Color when clicking a link.
        /// </summary>
        [Description("Color when clicking a link."), Category("Colors")]
        public Color LinkClickColor { get; set; } = Color.Red;

        /// <summary>
        /// Text color for displaying errors.
        /// </summary>
        [Description("Text color for displaying errors."), Category("Colors")]
        public Color ErrorTextColor { get; set; } = Color.DarkRed;

        /// <summary>
        /// Background color for displaying errors.
        /// </summary>
        [Description("Background color for displaying errors."), Category("Colors")]
        public Color ErrorBackColor { get; set; } = Color.FromArgb(255, 192, 192);

        /// <summary>
        /// Foreground color for displaying highlighted controls.
        /// </summary>
        [Description("Foreground color for displaying highlighted controls."), Category("Colors")]
        public Color HighlightTextColor { get; set; } = Color.FromArgb(0, 128, 0);

        /// <summary>
        /// Background color for displaying highlighted controls.
        /// </summary>
        [Description("Background color for displaying highlighted controls."), Category("Colors")]
        public Color HighlightBackColor { get; set; } = Color.FromArgb(192, 255, 192);

        /// <summary>
        /// Background color for status bars. Defaults to this.MainBackColor
        /// </summary>
        [Description("Background color for status bars."), Category("Colors")]
        public Color StatusBarBackColor
        {
            get
            {
                if (this._statusBarBackColor.HasValue)
                    return this._statusBarBackColor.Value;
                return this.MainBackColor;
            }
            set => this._statusBarBackColor = value;
        }
        private Color? _statusBarBackColor = null;

        /// <summary>
        /// Background color for loading status bars. Defaults to this.StatusBarBackColor
        /// </summary>
        [Description("Background color for loading status bars."), Category("Colors")]
        public Color StatusBarLoadingBackColor
        {
            get
            {
                if (this._statusBarLoadingBackColor.HasValue)
                    return this._statusBarLoadingBackColor.Value;
                return this.StatusBarBackColor;
            }
            set => this._statusBarLoadingBackColor = value;
        }
        private Color? _statusBarLoadingBackColor = null;

        /// <summary>
        /// Background color for compiling status bars. Defaults to this.StatusBarBackColor
        /// </summary>
        [Description("Background color for status bars while compiling."), Category("Colors")]
        public Color StatusBarCompilingBackColor
        {
            get
            {
                if (this._statusBarCompilingBackColor.HasValue)
                    return this._statusBarCompilingBackColor.Value;
                return this.StatusBarBackColor;
            }
            set => this._statusBarCompilingBackColor = value;
        }
        private Color? _statusBarCompilingBackColor = null;

        /// <summary>
        /// Background color for running SFM status bars. Defaults to this.StatusBarBackColor
        /// </summary>
        [Description("Background color for status bars when SFM is running."), Category("Colors")]
        public Color StatusBarRunningSFMBackColor
        {
            get
            {
                if (this._statusBarRunningSFMBackColor.HasValue)
                    return this._statusBarRunningSFMBackColor.Value;
                return this.StatusBarBackColor;
            }
            set => this._statusBarRunningSFMBackColor = value;
        }
        private Color? _statusBarRunningSFMBackColor = null;

        /// <summary>
        /// Foreground/text color for status bars. Defaults to this.MainTextColor
        /// </summary>
        [Description("Foreground/text color for status bars."), Category("Colors")]
        public Color StatusBarTextColor
        {
            get
            {
                if (this._statusBarTextColor.HasValue)
                    return this._statusBarTextColor.Value;
                return this.MainTextColor;
            }
            set => this._statusBarTextColor = value;
        }
        private Color? _statusBarTextColor = null;

        /// <summary>
        /// The background color for any text editor. Defaults to this.DarkBackColor.
        /// </summary>
        [Description("The background color for any text editor."), Category("Editor")]
        public Color TextEditorBackColor
        {
            get
            {
                if (this._textEditorBackColor.HasValue)
                    return this._textEditorBackColor.Value;
                return this.DarkBackColor;
            }
            set => this._textEditorBackColor = value;
        }
        private Color? _textEditorBackColor = null;

        /// <summary>
        /// The text color for any text editor. Defaults to this.MainTextColor.
        /// </summary>
        [Description("The text color for any text editor."), Category("Editor")]
        public Color TextEditorForeColor
        {
            get
            {
                if (this._textEditorForeColor.HasValue)
                    return this._textEditorForeColor.Value;
                return this.MainTextColor;
            }
            set => this._textEditorForeColor = value;
        }
        private Color? _textEditorForeColor = null;

        /// <summary>
        /// The text color for the line numbers of any text editor. Defaults to this.SubTextColor.
        /// </summary>
        [Description("The text color for the line numbers of any text editor."), Category("Editor")]
        public Color TextEditorLineNumbersColor
        {
            get
            {
                if (this._textEditorLineNumbersColor.HasValue)
                    return this._textEditorLineNumbersColor.Value;
                return this.SubTextColor;
            }
            set => this._textEditorLineNumbersColor = value;
        }
        private Color? _textEditorLineNumbersColor = null;
    }
}
