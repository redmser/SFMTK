﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;
using Newtonsoft.Json.Linq;
using SFMTK.Controls;
using SFMTK.Controls.TextEditors;
using WeifenLuo.WinFormsUI.Docking;
using SFMTK.Modules;
using System.Runtime.CompilerServices;

namespace SFMTK
{
    /// <summary>
    /// A theme to apply to the entire user interface of SFMTK.
    /// </summary>
    [Newtonsoft.Json.JsonConverter(typeof(UIThemeConverter))]
    public partial class UITheme : Data.IData
    {
        //BUG: UITheme - ToolStripDropDownButton looks completely different (and worse) compared to ToolStripSplitButton (on a statusstrip!)
        //  -> is there a way to change the former to the latter's looks? else, just find a way to cope by using the latter everywhere instead
        //FIXME: UITheme - different BG color for group header (objectlistview groups), to make it look clearer

        /// <summary>
        /// Initializes a new instance of the <see cref="UITheme"/> class.
        /// </summary>
        public UITheme()
        {

        }

        /// <summary>
        /// Runs the initialization phase of this theme. Usually involves creation of brushes and pens.
        /// </summary>
        public void DoInitialize()
        {
            this.ListTreeConnectionPen = new Pen(this.ListTreeConnectionColor, 1);
            this.BackBrush = new SolidBrush(this.MainBackColor);
            this.TextBrush = new SolidBrush(this.MainTextColor);
            this.HighlightBackBrush = new SolidBrush(this.HighlightBackColor);
        }

        /// <summary>
        /// Gets or sets the currently selected theme.
        /// </summary>
        public static UITheme SelectedTheme
        {
            get
            {
                if (string.IsNullOrEmpty(SelectedThemeName) || !Themes.Any())
                    return UITheme.SystemDefaultTheme;
                return Themes.FirstOrDefault(t => t.Name.Equals(SelectedThemeName, StringComparison.InvariantCultureIgnoreCase));
            }
            set
            {
                if (!Themes.Any(t => t.Name.Equals(value.Name, StringComparison.InvariantCultureIgnoreCase)))
                    throw new ArgumentException($"The specified theme \"{value.Name}\" is not part of the list of themes. Be sure to add it first.", nameof(value));
                SelectedThemeName = value.Name;
            }
        }

        /// <summary>
        /// Gets or sets the currently selected theme by name.
        /// </summary>
        private static string SelectedThemeName
        {
            get => Properties.Settings.Default.SelectedTheme;
            set => Properties.Settings.Default.SelectedTheme = value;
        }

        /// <summary>
        /// Gets or sets the system default theme. Only one should ever exist, by default named <c>SystemUI.theme</c>!
        /// </summary>
        public static UITheme SystemDefaultTheme { get; set; }

        /// <summary>
        /// Gets a list of currently active forms.
        /// </summary>
        public static IEnumerable<Form> ActiveForms => Application.OpenForms.OfType<Form>();

        /// <summary>
        /// Occurs when a property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises the <see cref="E:PropertyChanged" /> event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) => this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        /// <summary>
        /// Raises the <see cref="E:PropertyChanged" /> event.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="propertyName">Name of the property.</param>
        public virtual void OnPropertyChanged(object sender, [CallerMemberName] string propertyName = null) => this.PropertyChanged?.Invoke(sender, new PropertyChangedEventArgs(propertyName));
        
        /// <summary>
        /// Display name of the theme.
        /// </summary>
        [Description("Display name of the theme."), Category("Base")]
        public string Name { get; set; }

        /// <summary>
        /// Path to the JSON file containing the theme info.
        /// </summary>
        [Browsable(false)]
        public string Path { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => this.Name;

        /// <summary>
        /// Names of different colors associated to this theme.
        /// </summary>
        public Dictionary<string, Color> Colors { get; set; }

        /// <summary>
        /// Gets the list of themes.
        /// </summary>
        public static List<UITheme> Themes { get; } = new List<UITheme>();

        /// <summary>
        /// Populates the list of themes by checking for the list of JSON files in the directory.
        /// </summary>
        public static void LoadThemes()
        {
            //TODO: UITheme - loading themes should not instantly parse the JSON! only figure out name, but read remaining data async/once selecting the theme!
            Themes.Clear();

            try
            { 
                //Check for theme files
                foreach (var file in Directory.EnumerateFiles(IOHelper.GetSFMTKDirectory("Themes"), "*.theme", SearchOption.TopDirectoryOnly))
                {
                    Themes.Add(UITheme.FromFilePath(file));
                }
            }
            catch (Exception ex)
            {
                FallbackRememberDialog.ShowDialogLog($"Could not load theme:\n\n{ex.Message}", "Error loading theme!", "Error loading theme",
                    FallbackRememberDialog.DontNotifyAgainText, null, FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));
            }
        }

        /// <summary>
        /// Returns a new UITheme instance filled with data from the given path and by reading the JSON data inside.
        /// </summary>
        /// <param name="path">The file to parse. JSON file has to exist.</param>
        public static UITheme FromFilePath(string path)
        {
            var contents = File.ReadAllText(path);
            var obj = JObject.Parse(contents);
            var theme = obj.ToObject<UITheme>();
            theme.Path = path;
            return theme;
        }

        /// <summary>
        /// Applies the currently selected theme to the specified themeable control and child controls that support it.
        /// </summary>
        /// <param name="control">The control (including all of its children) to apply the theme to.</param>
        public static void ApplyTheme(Control control) => SelectedTheme?.Apply(control);

        /// <summary>
        /// Applies the currently selected theme to all active forms.
        /// </summary>
        public static void ApplyThemeToAll(bool force = false)
        {
            var forms = ActiveForms.OfType<Control>().ToList();
            if (force)
                foreach (var form in forms.OfType<IThemeable>())
                    form.ActiveTheme = null;
            SelectedTheme?.Apply(forms);
        }

        /// <summary>
        /// Applies the theme to the specified themeable control and child controls that support it.
        /// </summary>
        /// <param name="control">The control (including all of its children) to apply the theme to.</param>
        /// <param name="includeSelf">If set to <c>true</c>, includes self for theming (may cause infinite theme calls).</param>
        public void Apply(Control control, bool includeSelf = true) => this.Apply(new[] { control }, includeSelf);

        /// <summary>
        /// Applies the theme to the specified themeable controls and child controls that support it.
        /// </summary>
        /// <param name="control">The controls (including all of their children) to apply the theme to.</param>
        /// <param name="includeSelf">If set to <c>true</c>, includes self for theming (may cause infinite theme calls).</param>
        /// <exception cref="System.ArgumentException">Type implements IThemeable but is not of type Control!</exception>
        public void Apply(IEnumerable<Control> controls, bool includeSelf = true)
        {
            var errors = false;
            foreach (var control in controls)
            {
                //Skip disposed
                //FIXME: UITheme - should disposed/null controls even be part of this list in the first place?
                if (control == null || control.IsDisposed)
                    continue;

                //HACK: UITheme - avoid theming propertygrid popup, which breaks it since we theme it while editing theme properties
                if (control.GetType().FullName.Contains("PropertyGrid"))
                    continue;

                var applySpecialFormatting = false;
                if (control is IThemeable theme)
                {
                    if (theme.ActiveTheme == null && this.Name == SystemDefaultTheme.Name)
                    {
                        //Null theme is special: only apply "special" formatting of the SystemUITheme
                        applySpecialFormatting = true;
                    }
                    else if (theme.ActiveTheme == this.Name)
                    {
                        //Do not apply theme again, if already applied
                        continue;
                    }
                }

                var ctrl = control as Control;
                if (ctrl == null)
                    throw new ArgumentException("Type implements IThemeable but is not of type Control!");

                try
                {
                    //Do not update drawing of form to make theme switch faster
                    ctrl.SuspendDrawing();

                    //Recurse through controls
                    foreach (var com in ControlsHelper.RecurseControlsAndItems(ctrl, includeSelf))
                    {
                        try
                        {
                            if (applySpecialFormatting)
                            {
                                //Special control theming, only for system UI
                                ApplySystemControlTheme(ctrl, com);
                                ApplyNonDefaultControlTheme(ctrl, com);
                            }
                            else
                            {
                                //Regular control theming
                                ApplyControlTheme(ctrl, com);

                                //Special theming if system theme
                                if (this.Equals(SystemDefaultTheme))
                                    ApplySystemControlTheme(ctrl, com);
                            }
                        }
                        catch (Exception)
                        {
                            errors = true;
                        }
                    }

                    //Recurse through components, if any
                    var comps = ControlsHelper.GetComponents(ctrl);
                    if (comps != null)
                    {
                        foreach (var com in comps.Components.OfType<Component>())
                        {
                            try
                            {
                                ApplyComponentTheme(ctrl, com);
                            }
                            catch (Exception)
                            {
                                errors = true;
                            }
                        }
                    }
                }
                finally
                {
                    //Ensure the drawing is always resumed
                    ctrl.ResumeDrawing();

                    if (control is IThemeable theme2)
                        theme2.ActiveTheme = this.Name;
                }
            }

            if (errors)
                FallbackRememberDialog.ShowDialogLog("While loading your application theme, an error occured.\nYou may want to reset your theme configuration!", "Could not load theme!",
                    "Error while loading theme", FallbackRememberDialog.DontNotifyAgainText, null, FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));
        }

        /// <summary>
        /// Applies this theme to the specified component which is part of the specified form.
        /// Use this call for anything which is part of the components list of the form.
        /// </summary>
        /// <param name="parent">The parent control on which this Component is on.</param>
        /// <param name="component">The component to apply the theme to.</param>
        public void ApplyComponentTheme(Control parent, Component component)
        {
            if (component is ToolTip tt)
            {
                //Theme tooltip - currently does not work due to WiNdOwS
                tt.ForeColor = this.MainTextColor;
                tt.BackColor = this.MainBackColor;
            }
            else if (component is ContextMenuStrip cms)
            {
                //May be programatically opened (and not caught by the ControlTheme)
                this.StripTheme.ApplyTo(cms);
            }

            //Call for all modules
            foreach (var mod in Program.MainMM.LoadedModules.OfType<IModuleThemeable>())
                mod.ApplyComponentTheme(parent, component);
        }

        /// <summary>
        /// Applies the system-default theming to the specified component which is part of the specified form.
        /// Use this call for any control which needs further properties set to return to its system-default theming.
        /// </summary>
        /// <param name="parent">The parent control on which this Component is on.</param>
        /// <param name="component">The component to apply the theme to.</param>
        private void ApplySystemControlTheme(Control parent, Component component)
        {
            if (component is Button b)
            {
                b.BackColor = Color.Empty;
                b.UseVisualStyleBackColor = true;
            }
            else if (component is StatusStrip ss)
            {
                //Make status strips show up as system-rendered
                ss.BackColor = Color.Empty;
                ss.RenderMode = ToolStripRenderMode.System;
            }
            else if (component is ToolStripStatusLabel tssl)
            {
                tssl.BackColor = Color.Empty;
            }
            else if (component is Label l)
            {
                l.ForeColor = this.MainTextColor;
            }

            //Call for all modules
            foreach (var mod in Program.MainMM.LoadedModules.OfType<IModuleThemeable>())
                mod.ApplySystemControlTheme(parent, component);
        }

        /// <summary>
        /// Should contain any theming which is not "default", meaning the standard color of this type is not equal to the expected system default
        /// </summary>
        /// <param name="parent">The parent control on which this Component is on.</param>
        /// <param name="component">The component to apply the theme to.</param>
        private void ApplyNonDefaultControlTheme(Control parent, Component component)
        {
            if (component is Control ctrl)
            {
                if (ctrl is GroupBox)
                {
                    ctrl.ForeColor = this.GroupTextColor;
                }
                else if (ctrl.Dock == DockStyle.Fill && ctrl.Parent == parent && ctrl is Label)
                {
                    ctrl.Font = this.StatusTextFont;
                }
                else if (ctrl is ToolStrip ts)
                {
                    if (ctrl is StatusStrip ss)
                    {
                        ss.ForeColor = this.StatusBarTextColor;
                        if (ctrl is StatusStripEx ex)
                        {
                            //Use main coloring for BG
                            ex.UpdateStatusColor();
                        }
                        else
                        {
                            //General status coloring
                            ss.BackColor = this.StatusBarBackColor;
                        }
                    }

                    //Toolstrip theme
                    this.StripTheme.ApplyTo(ts);
                }
                else if (ctrl is ListView lv)
                {
                    //Properties for any kind of listview
                    lv.ForeColor = this.ListTextColor;
                    lv.BackColor = this.ListBackColor;
                    lv.GridLines = this.GridLinesInLists;
                    lv.InsertionMark.Color = this.ListInsertionMarkColor;

                    if (ctrl is ObjectListView olv)
                    {
                        //Alternate BG color
                        olv.AlternateRowBackColor = this.ListAlternateBackColor;
                        olv.UseAlternatingBackColors = this.AlternatingListBackground;

                        //EmptyMsgOverlay
                        if (olv.EmptyListMsgOverlay is EmptyListOverlay elo)
                        {
                            elo.Font = this.StatusTextFont;
                            elo.TextColor = this.MainTextColor;
                            elo.BorderColor = this.MainTextColor;
                        }

                        //ContextMenu
                        if (olv.FilterMenuBuildStrategy.GetType() == typeof(FilterMenuBuilder))
                            olv.FilterMenuBuildStrategy = new ThemedFilterMenuBuilder();

                        //InsertionMark
                        if (olv.DropSink is SimpleDropSink sds)
                            sds.FeedbackColor = this.ListInsertionMarkColor;

                        //Columns
                        //-> to apply theme, may need to set headerownerdraw to true and set properties on column itself
                        //olv.HeaderUsesThemes = false;
                        //olv.HeaderFormatStyle = this.ColumnStyle;

                        //Only update headers that need a custom draw
                        if (NeedsCustomDraw(olv))
                        {
                            foreach (var column in olv.AllColumns)
                            {
                                column.HeaderForeColor = this.MainTextColor;
                            }
                        }

                        //FIXME: UITheme - if possible, color OLV groups with GroupColor too

                        if (olv is TreeListView tlv)
                        {
                            //Node lines
                            tlv.TreeColumnRenderer.LinePen = this.ListTreeConnectionPen;

                            //Default highlight colors
                            //FIXME: UITheme - set text color of any filtered text (TreeColumnRenderer) to HighlightForeColor
                            //  -> needs to override text drawing method, since no property exposes this (update below too)
                            tlv.TreeColumnRenderer.FillBrush = this.HighlightBackBrush;
                            tlv.TreeColumnRenderer.FramePen = null;
                        }

                        //Default highlight colors
                        if (olv.DefaultRenderer is HighlightTextRenderer htr)
                        {
                            htr.FillBrush = this.HighlightBackBrush;
                            htr.FramePen = null;
                        }

                        //Rebuild OLVs
                        olv.BuildList(true);
                    }
                }
                else if (ctrl is DockPanel panel)
                {
                    if (panel.Theme == this.StripTheme)
                        return;

                    //Docking theme
                    var dpm = DockPanelManager.DockPanelManagers.FirstOrDefault(d => d.DockPanel == panel);
                    dpm.TemporaryCloseContent();
                    panel.Theme = this.StripTheme;
                    dpm.TemporaryOpenContent();

                    panel.DockBackColor = this.DarkBackColor;
                    panel.ShowDocumentIcon = this.ShowDocumentIcons;
                    panel.DocumentTabStripLocation = this.TabLocation;
                }
                else if (ctrl is UserControl uc && !(uc is DockPane))
                {
                    //Recursed call
                    if (component is SearchTextBox stb)
                    {
                        stb.ClearImage = this.SearchDeleteButtonImage;
                        stb.SearchImage = this.SearchButtonImage;
                        this.ApplyControlTheme(parent, stb.TextBox);
                    }
                    else if (uc is TextEditorControl tec)
                    {
                        var text = tec.TextEditor;
                        text.ShowLineNumbers = this.ShowLineNumbers;
                        //TODO: UITheme - if anyone is interested, allow changing the brush type to gradient/image/...
                        text.Background = new System.Windows.Media.SolidColorBrush(this.TextEditorBackColor.ToMediaColor());
                        text.Foreground = new System.Windows.Media.SolidColorBrush(this.TextEditorForeColor.ToMediaColor());
                        text.LineNumbersForeground = new System.Windows.Media.SolidColorBrush(this.TextEditorLineNumbersColor.ToMediaColor());

                        //NYI: UITheme - allow theming of text editor font (compatibility with winforms classes?) and SyntaxHighlighting
                        text.FontFamily = new System.Windows.Media.FontFamily("Consolas");
                        text.FontSize = 12.75f;
                    }
                    else if (uc is Microsoft.Msagl.GraphViewerGdi.GViewer gv)
                    {
                        //Theme the viewer
                        gv.OutsideAreaBrush = this.BackBrush;

                        //Apply graph theme
                        if (gv.Graph != null)
                            this.ApplyGraphTheme(gv.Graph);
                    }
                    else
                    {
                        this.Apply(uc, false);
                    }
                }
            }
            else if (component is ToolStripItem tsi)
            {
                //Can still recolor
                //BUG: UITheme - certain toolstripitems such as toolstriplabel do not conform to the forecolor specified due to their renderer overriding such values
                //BUG: UITheme - despite fixes in SystemUITheme - it seems like setting backcolor still breaks it all for status labels
                //  -> similarly, the statusstrip does not use the system theming (is this due to an applied striptheme??)
                tsi.ForeColor = this.MainTextColor;
                tsi.BackColor = this.MainBackColor;

                //SearchBar
                if (component is ToolStripSearchTextBox tsstb)
                {
                    tsstb.ClearImage = this.SearchDeleteButtonImage;
                    tsstb.SearchImage = this.SearchButtonImage;
                }
            }

            //Call for all modules
            foreach (var mod in Program.MainMM.LoadedModules.OfType<IModuleThemeable>())
                mod.ApplyNonDefaultControlTheme(parent, component);
        }

        /// <summary>
        /// Applies this theme to the specified control which is part of the specified form.
        /// Use this call for anything which is part of the form itself, mostly of type <see cref="Control"/>.
        /// </summary>
        /// <param name="parent">The parent control on which this Component is on.</param>
        /// <param name="component">The component to apply the theme to.</param>
        public void ApplyControlTheme(Control parent, Component component)
        {
            //Control-related shizz - any theming which, when using the default system theme, will not make any changes and wont be called then
            if (component is Control ctrl)
            {
                if (ctrl.Name.Contains("NoTheme"))
                    return;
               
                //General preferences
                ctrl.ForeColor = this.MainTextColor;
                ctrl.BackColor = this.MainBackColor;
                ctrl.Font = new Font(this.MainFont.FontFamily, ctrl.Font.SizeInPoints, ctrl.Font.Style);
               
                //TODO: UITheme - add custom rendering for controls which don't like themes:
                //  Includes: TabControl, GroupBox borders, (NumericUpDown buttons), ToolTips (balloon and normal)
               
                //TODO: UITheme - ButtonBase inside of GroupBox uses ForeColor of GroupBox

                if (ctrl is ButtonBase button && DoCheckBoxRadioButton(button))
                {
                    button.UseVisualStyleBackColor = this.UseVisualStyles;
                    button.FlatStyle = this.ButtonStyle;
                    button.FlatAppearance.BorderColor = this.ButtonAppearance.BorderColor;
                    button.FlatAppearance.BorderSize = this.ButtonAppearance.BorderSize;
                    button.FlatAppearance.CheckedBackColor = this.ButtonAppearance.CheckedBackColor;
                    button.FlatAppearance.MouseDownBackColor = this.ButtonAppearance.MouseDownBackColor;
                    button.FlatAppearance.MouseOverBackColor = this.ButtonAppearance.MouseOverBackColor;
                    ctrl.ForeColor = this.ButtonTextColor;
                    ctrl.BackColor = this.ButtonBackColor;
                }
                else if (ctrl is ComboBox cb)
                {
                    //BUG: UITheme - combo box seems to keep visual styles bg at times when switching
                    cb.FlatStyle = this.ButtonStyle;

                    if (cb is CheckedComboBox cbd)
                    {
                        //Recursed call
                        UITheme.SelectedTheme?.ApplyControlTheme(parent, cbd.Popup.List);
                    }
                }
                else if (ctrl is LinkLabel ll)
                {
                    ll.LinkColor = this.LinkColor;
                    ll.ActiveLinkColor = this.LinkClickColor;
                    ll.VisitedLinkColor = this.LinkVisitedColor;
                }
            }

            //Call for all modules
            foreach (var mod in Program.MainMM.LoadedModules.OfType<IModuleThemeable>())
                mod.ApplyComponentTheme(parent, component);

            this.ApplyNonDefaultControlTheme(parent, component);



            bool DoCheckBoxRadioButton(ButtonBase cbrb)
            {
                if (this.ForceVisualStyles || cbrb is Button ||
                    (cbrb is CheckBox cb && cb.Appearance == Appearance.Button) ||
                    (cbrb is RadioButton rb && rb.Appearance == Appearance.Button))
                    return true;
                return false;
            }
        }

        private static bool NeedsCustomDraw(ObjectListView olv)
        {
            if (olv.HeaderUsesThemes)
                return false;

            if (NeedsCustomDraw(olv.HeaderFormatStyle))
                return true;

            foreach (OLVColumn column in olv.AllColumns)
            {
                if (column.HasHeaderImage ||
                    !column.ShowTextInHeader ||
                    column.IsHeaderVertical ||
                    HasFilterIndicator(olv, column) ||
                    HasCheckBox(column) ||
                    column.TextAlign != column.HeaderTextAlignOrDefault ||
                    (column.Index == 0 && column.HeaderTextAlignOrDefault != HorizontalAlignment.Left) ||
                    NeedsCustomDraw(column.HeaderFormatStyle))
                    return true;
            }

            return false;
        }

        private static bool HasFilterIndicator(ObjectListView olv, OLVColumn column)
        {
            return olv.UseFiltering && olv.UseFilterIndicator && column.HasFilterIndicator;
        }

        private static bool HasCheckBox(OLVColumn column)
        {
            return column.HeaderCheckBox || column.HeaderTriStateCheckBox;
        }

        private static bool NeedsCustomDraw(HeaderFormatStyle style)
        {
            if (style == null)
                return false;

            return (NeedsCustomDraw(style.Normal) ||
                NeedsCustomDraw(style.Hot) ||
                NeedsCustomDraw(style.Pressed));
        }

        private static bool NeedsCustomDraw(HeaderStateStyle style)
        {
            if (style == null)
                return false;

            // If we want fancy colors or frames, we have to custom draw. Oddly enough, we 
            // can handle font changes without custom drawing.
            if (!style.BackColor.IsEmpty)
                return true;

            if (style.FrameWidth > 0f && !style.FrameColor.IsEmpty)
                return true;

            return (!style.ForeColor.IsEmpty && style.ForeColor != Color.Black);
        }

        /// <summary>
        /// Applies this theme to the specified graph contents.
        /// </summary>
        /// <param name="graph"></param>
        public void ApplyGraphTheme(Microsoft.Msagl.Drawing.Graph graph)
        {
            //Not themed?
            if (graph.UserData != null)
                return;

            //Themed!
            graph.UserData = "Themed";

            //Theme the graph
            graph.Attr.BackgroundColor = this.MainBackColor.ToMsaglColor();
            graph.Attr.Color = this.MainBackColor.ToMsaglColor();

            //Theme the nodes
            foreach (var node in graph.Nodes)
            {
                //Label
                if (node.Label != null)
                {
                    node.Label.FontColor = this.MainTextColor.ToMsaglColor();
                    node.Label.FontName = this.StatusTextFont.Name;
                }

                //Node
                node.Attr.Color = this.MainTextColor.ToMsaglColor();
                node.Attr.FillColor = this.MainBackColor.ToMsaglColor();
                node.Attr.LineWidth = 0.25;

#if DEBUG
                //Different theming for uppercase nodes
                if (char.IsUpper(node.LabelText[0]))
                {
                    node.Attr.FillColor = this.StatusBarBackColor.ToMsaglColor();
                }
#endif
            }

            //Theme the edges
            foreach (var edge in graph.Edges)
            {
                //Label
                if (edge.Label != null)
                {
                    edge.Label.FontColor = this.SubTextColor.ToMsaglColor();
                    edge.Label.FontName = this.StatusTextFont.Name;
                }

                //Edge
                edge.Attr.Color = this.MainTextColor.ToMsaglColor();
            }
        }
    }
}
