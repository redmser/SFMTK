﻿using SFMTK.Properties;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace SFMTK
{
    /// <summary>
    /// Base for all enties in the <see cref="SFMTK.Forms.LauncherSelector"/>.
    /// </summary>
    public abstract class LauncherEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LauncherEntry"/> class.
        /// </summary>
        protected LauncherEntry()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LauncherEntry" /> class.
        /// </summary>
        /// <param name="name">The display name and identifier of this launcher entry.</param>
        /// <param name="group">The group to display the launcher entry in, when using the listview display.</param>
        protected LauncherEntry(string name, string group) : base()
        {
            this.Name = name;
            this.Group = group;
        }

        /// <summary>
        /// Display name of the launcher entry.
        /// </summary>
        public virtual string Name { get; protected set; }

        /// <summary>
        /// The group to display the launcher entry in, when using the listview display.
        /// </summary>
        public virtual string Group { get; protected set; }

        /// <summary>
        /// Description to show below the name of the launcher entry.
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Display icon for this launcher entry. Should be 32x32.
        /// </summary>
        public virtual Image Image => null;

        /// <summary>
        /// Launches this entry's executable and returns whether this was successful.
        /// </summary>
        public abstract bool Launch();
    }

    /// <summary>
    /// A launcher entry for launching SFMTK.
    /// </summary>
    public class SFMTKLauncherEntry : LauncherEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SFMTKLauncherEntry" /> class.
        /// </summary>
        /// <param name="name">The display name and identifier of this launcher entry.</param>
        /// <param name="group">The group to display the launcher entry in, when using the listview display.</param>
        /// <param name="args">How SFMTK should be launched (excluding the executable name, only arguments).</param>
        /// <param name="img">Custom display image, must be 32x32.</param>
        public SFMTKLauncherEntry(string name, string group, string args, Image img = null) : base(name, group)
        {
            this.Arguments = args;
            this.Image = img ?? Resources.sfmtk_small.Resize(32, 32);
        }

        /// <summary>
        /// Description to show below the name of the launcher entry.
        /// </summary>
        public override string Description => "SFMTK.exe " + this.Arguments;

        /// <summary>
        /// Gets the command line arguments to launch SFMTK with.
        /// </summary>
        public string Arguments { get; }

        /// <summary>
        /// Display icon for this launcher entry. Should be 32x32.
        /// </summary>
        public override Image Image { get; }

        /// <summary>
        /// Launches this entry's executable and returns whether this was successful.
        /// </summary>
        public override bool Launch()
        {
            //NYI: LauncherEntry - since SFMTK is already running, fake a second launch by passing corresponding command-line arguments
            //  -> shove this into its own method maybe?
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A launcher entry for launching SFM.
    /// </summary>
    /// <seealso cref="SFMTK.LauncherEntry" />
    public class SFMLauncherEntry : LauncherEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SFMLauncherEntry" /> class.
        /// </summary>
        /// <param name="name">The display name and identifier of this launcher entry.</param>
        /// <param name="group">The group to display the launcher entry in, when using the listview display.</param>
        /// <param name="setup">The setup to launch SFM with.</param>
        /// <param name="img">Custom display image, must be 32x32.</param>
        public SFMLauncherEntry(string name, string group, string setup, Image img = null) : base(name, group)
        {
            this.Setup = setup;
            this.Image = img ?? Resources.sfm32;
        }

        /// <summary>
        /// Description to show below the name of the launcher entry.
        /// </summary>
        public override string Description => $"Source Filmmaker with setup \"{this.Setup}\"";

        /// <summary>
        /// Gets the setup to launch SFM with.
        /// </summary>
        public string Setup { get; }

        /// <summary>
        /// Display icon for this launcher entry. Should be 32x32.
        /// </summary>
        public override Image Image { get; }

        /// <summary>
        /// Launches this entry's executable and returns whether this was successful.
        /// </summary>
        public override bool Launch()
        {
            //NYI: LauncherEntry - create a procedure for easily returning the right Setup depending on a string (since the Constants ?PROMPT and ?ACTIVE also have to be handled)
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A custom launcher entry.
    /// </summary>
    public class CustomLauncherEntry : LauncherEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomLauncherEntry" /> class.
        /// </summary>
        /// <param name="name">The display name and identifier of this launcher entry.</param>
        /// <param name="group">The group to display the launcher entry in, when using the listview display.</param>
        /// <param name="description">User-defined description text.</param>
        /// <param name="commandline">The executable to run for this launcher entry.</param>
        /// <param name="args">The arguments to run for the launcher's executable.</param>
        /// <param name="workingDir">The working directory for the executable.</param>
        /// <param name="img">Custom display image, must be 32x32.</param>
        public CustomLauncherEntry(string name, string group, string description, string commandline, string args = "", string workingDir = null, Image img = null)
            : base(name, group)
        {
            this.Description = description;
            this.CommandLine = commandline;
            this.Arguments = args;
            this.WorkingDirectory = workingDir ?? Application.StartupPath;
            this.Image = img ?? Resources.box;
        }

        /// <summary>
        /// Gets the executable to run for this launcher entry.
        /// </summary>
        public string CommandLine { get; }

        /// <summary>
        /// Gets the arguments to run for the launcher's executable.
        /// </summary>
        public string Arguments { get; }

        /// <summary>
        /// Gets the working directory for the executable.
        /// </summary>
        public string WorkingDirectory { get; }

        /// <summary>
        /// Display icon for this launcher entry. Should be 32x32.
        /// </summary>
        public override Image Image { get; }

        /// <summary>
        /// Launches this entry's executable and returns whether this was successful.
        /// </summary>
        public override bool Launch()
        {
            //Launch what the user wanted
            var psi = new ProcessStartInfo(this.CommandLine, this.Arguments);
            psi.WorkingDirectory = this.WorkingDirectory;
            try
            {
                Process.Start(psi);
                return true;
            }
            catch (Exception)
            {
                //Unable to launch the process
                //TODO: LauncherEntry - notify user about what went wrong, possibly even before trying to launch the exe!
                return false;
            }
        }
    }
}
