﻿using System;
using System.Collections;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    /// <summary>
    /// A simple picker of all image resources.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class IconPicker : Form, IThemeable
    {
        //TODO: IconPicker - add search bar and possibly more display options (smallicon, tile, ...) which should be saved as properties
        //TODO: IconPicker - should all SilkIcons be added?

        /// <summary>
        /// Initializes a new instance of the <see cref="IconPicker"/> class.
        /// </summary>
        public IconPicker() : this(false)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IconPicker"/> class.
        /// </summary>
        /// <param name="allowBrowse">If set to <c>true</c>, allow browsing for external images as well.</param>
        public IconPicker(bool allowBrowse)
        {
            //Compiler-generated
            InitializeComponent();

            //TODO: IconPicker - allow specifying a name of the item to be selected on load for the constructor

            //Populate list
            this.nameColumn.ImageAspectName = "Key";
            var imglist = new ImageList();
            imglist.ImageSize = new Size(16, 16);
            imglist.ColorDepth = ColorDepth.Depth32Bit;
            this.iconObjectListView.SmallImageList = imglist;
            this.iconObjectListView.LargeImageList = imglist;

            //Add "No Icon"
            this.iconObjectListView.AddObject(new DictionaryEntry("(No Icon)", null));

            var resources = Properties.Resources.ResourceManager.GetResourceSet(CultureInfo.CurrentCulture, false, true);
            foreach (DictionaryEntry entry in resources)
            {
                if (entry.Value is Bitmap bmp)
                {
                    //Add to list
                    this.iconObjectListView.AddObject(entry);
                    imglist.Images.Add(entry.Key.ToString(), bmp);
                }
            }

            this.browseButton.Visible = allowBrowse;

            //Alphabetical order
            this.iconObjectListView.Sort(this.nameColumn);

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control.
        /// </summary>
        public string ActiveTheme { get; set; }

        /// <summary>
        /// Gets the selected image, or null if none was selected.
        /// </summary>
        public Bitmap Selected { get; private set; }

        private void IconPicker_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Selected == null && this.iconObjectListView.SelectedObject != null)
                this.Selected = (Bitmap)((DictionaryEntry)this.iconObjectListView.SelectedObject).Value;
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            var ofd = new Ookii.Dialogs.Wpf.VistaOpenFileDialog();
            ofd.AddExtension = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            ofd.Filter = "PNG|*.png|All files|*.*"; //TODO: IconPicker - find out which image formats are supported by WinForms and set as filter
            ofd.Multiselect = false;
            ofd.Title = "Select image";

            var res = ofd.ShowDialog();
            if (res ?? false)
            {
                //Pick as image
                try
                {
                    this.Selected = new Bitmap(Image.FromFile(ofd.FileName));
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                catch (Exception)
                {
                    //NYI: IconPicker - error handling
                    throw;
                }
            }
        }

        private void iconObjectListView_DoubleClick(object sender, EventArgs e)
        {
            //Same as pressing OK
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
