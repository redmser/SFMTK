﻿namespace SFMTK.Forms
{
    partial class NewMod
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewMod));
            this.bottomTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.modNameTextBox = new System.Windows.Forms.TextBox();
            this.modDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.modCategoryComboBox = new System.Windows.Forms.ComboBox();
            this.modNameErrorMarker = new SFMTK.Controls.ErrorMarker();
            this.modCategoryErrorMarker = new SFMTK.Controls.ErrorMarker();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.bottomTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomTableLayoutPanel
            // 
            this.bottomTableLayoutPanel.ColumnCount = 3;
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.bottomTableLayoutPanel.Controls.Add(this.okButton, 1, 0);
            this.bottomTableLayoutPanel.Controls.Add(this.cancelButton, 2, 0);
            this.bottomTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomTableLayoutPanel.Location = new System.Drawing.Point(6, 189);
            this.bottomTableLayoutPanel.Name = "bottomTableLayoutPanel";
            this.bottomTableLayoutPanel.RowCount = 1;
            this.bottomTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.bottomTableLayoutPanel.Size = new System.Drawing.Size(342, 26);
            this.bottomTableLayoutPanel.TabIndex = 1;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.okButton.Location = new System.Drawing.Point(186, 1);
            this.okButton.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 24);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cancelButton.Location = new System.Drawing.Point(265, 1);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 24);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "C&ancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.modNameTextBox, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.modDescriptionTextBox, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.modCategoryComboBox, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.modNameErrorMarker, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.modCategoryErrorMarker, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 29);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(342, 160);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(23, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "&Name:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(23, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "&Description:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(23, 55);
            this.label3.Margin = new System.Windows.Forms.Padding(3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "&Category:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // modNameTextBox
            // 
            this.modNameTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modNameTextBox.Location = new System.Drawing.Point(92, 3);
            this.modNameTextBox.Name = "modNameTextBox";
            this.modNameTextBox.Size = new System.Drawing.Size(247, 20);
            this.modNameTextBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.modNameTextBox, "The name of the mod. Used for filtering, identification and for the folder itself" +
        ".");
            this.modNameTextBox.TextChanged += new System.EventHandler(this.modNameTextBox_TextChanged);
            // 
            // modDescriptionTextBox
            // 
            this.modDescriptionTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modDescriptionTextBox.Location = new System.Drawing.Point(92, 29);
            this.modDescriptionTextBox.Name = "modDescriptionTextBox";
            this.modDescriptionTextBox.Size = new System.Drawing.Size(247, 20);
            this.modDescriptionTextBox.TabIndex = 3;
            this.toolTip.SetToolTip(this.modDescriptionTextBox, "A short description of the mod\'s purpose. Only shown inside SFMTK.");
            // 
            // modCategoryComboBox
            // 
            this.modCategoryComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modCategoryComboBox.FormattingEnabled = true;
            this.modCategoryComboBox.Location = new System.Drawing.Point(92, 55);
            this.modCategoryComboBox.Name = "modCategoryComboBox";
            this.modCategoryComboBox.Size = new System.Drawing.Size(247, 21);
            this.modCategoryComboBox.TabIndex = 5;
            this.toolTip.SetToolTip(this.modCategoryComboBox, "A category for sorting mods inside SFMTK.");
            this.modCategoryComboBox.TextUpdate += new System.EventHandler(this.modCategoryComboBox_TextUpdate);
            // 
            // modNameErrorMarker
            // 
            this.modNameErrorMarker.BackColor = System.Drawing.Color.Transparent;
            this.modNameErrorMarker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modNameErrorMarker.Location = new System.Drawing.Point(4, 5);
            this.modNameErrorMarker.Margin = new System.Windows.Forms.Padding(4, 5, 0, 5);
            this.modNameErrorMarker.Name = "modNameErrorMarker";
            this.modNameErrorMarker.Size = new System.Drawing.Size(16, 16);
            this.modNameErrorMarker.State = SFMTK.Controls.ErrorState.Error;
            this.modNameErrorMarker.TabIndex = 8;
            // 
            // modCategoryErrorMarker
            // 
            this.modCategoryErrorMarker.BackColor = System.Drawing.Color.Transparent;
            this.modCategoryErrorMarker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modCategoryErrorMarker.Location = new System.Drawing.Point(4, 57);
            this.modCategoryErrorMarker.Margin = new System.Windows.Forms.Padding(4, 5, 0, 5);
            this.modCategoryErrorMarker.Name = "modCategoryErrorMarker";
            this.modCategoryErrorMarker.Size = new System.Drawing.Size(16, 17);
            this.modCategoryErrorMarker.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Location = new System.Drawing.Point(6, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(342, 23);
            this.label4.TabIndex = 0;
            this.label4.Text = "Please enter basic information about the mod:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // NewMod
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(354, 221);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.bottomTableLayoutPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(251, 183);
            this.Name = "NewMod";
            this.Padding = new System.Windows.Forms.Padding(6);
            this.Text = "New Mod";
            this.Load += new System.EventHandler(this.NewMod_Load);
            this.bottomTableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel bottomTableLayoutPanel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox modNameTextBox;
        private System.Windows.Forms.TextBox modDescriptionTextBox;
        private System.Windows.Forms.ComboBox modCategoryComboBox;
        private Controls.ErrorMarker modNameErrorMarker;
        private Controls.ErrorMarker modCategoryErrorMarker;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label label4;
    }
}