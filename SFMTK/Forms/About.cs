﻿using System;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Modules;

namespace SFMTK.Forms
{
    /// <summary>
    /// About dialog for SFMTK.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class About : Form, IThemeable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="About"/> class.
        /// </summary>
        public About()
        {
            //Compiler-generated
            InitializeComponent();

            //Load some infos
            this.sfmtkLogoPictureBox.Image = Properties.Resources.sfmtk_small;
            this.versionLabel.Text = "Version " + Program.VersionString;
            this.thanksTextBox.Text = Properties.Resources.Thanks;
            this.thanksTextBox.Select(0, 0);
            this.sfmtkLabelNoTheme.ForeColor = Constants.SFMTKLogoColor;

            //Modules list
            this.modulesListView.Items.AddRange(Program.MainMM.LoadedModules.Select(m => new ListViewItem(m.GetName())).ToArray());

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        private void About_Load(object sender, EventArgs e)
        {
            //Center to main window
            this.CenterToParent();
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control.
        /// </summary>
        public string ActiveTheme { get; set; }

        private void licenseLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //Info about license
            //TODO: About - include a local copy of the license in the binary releases
            NetworkFallback.OpenInBrowser(Constants.LicenseURL);
        }

        private void donateButton_Click(object sender, EventArgs e)
        {
            new Donate().Show(this);
        }

        private void gitlabButton_Click(object sender, EventArgs e)
        {
            NetworkFallback.OpenInBrowser(Constants.GitLabURL);
        }

        private void modulesListView_DoubleClick(object sender, EventArgs e)
        {
            //Open module settings
            Program.MainForm.OpenSettings("Environment/Modules", true);
        }
    }
}
