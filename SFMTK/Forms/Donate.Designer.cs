﻿namespace SFMTK.Forms
{
    partial class Donate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Donate));
            this.heartPictureBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.paypalButton = new System.Windows.Forms.Button();
            this.bitcoinButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.heartPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // heartPictureBox
            // 
            this.heartPictureBox.Image = global::SFMTK.Properties.Resources.heartbig;
            this.heartPictureBox.Location = new System.Drawing.Point(8, 8);
            this.heartPictureBox.Name = "heartPictureBox";
            this.heartPictureBox.Size = new System.Drawing.Size(64, 64);
            this.heartPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.heartPictureBox.TabIndex = 0;
            this.heartPictureBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.label1.Location = new System.Drawing.Point(80, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(403, 64);
            this.label1.TabIndex = 0;
            this.label1.Text = "Thank you for your support!";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Location = new System.Drawing.Point(20, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(447, 44);
            this.label2.TabIndex = 1;
            this.label2.Text = "Thank you for your interest in supporting the development of SFMTK!\r\n\r\nPlease sel" +
    "ect your preferred payment method below.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // paypalButton
            // 
            this.paypalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.paypalButton.Image = global::SFMTK.Properties.Resources.paypal24;
            this.paypalButton.Location = new System.Drawing.Point(12, 164);
            this.paypalButton.Name = "paypalButton";
            this.paypalButton.Size = new System.Drawing.Size(156, 36);
            this.paypalButton.TabIndex = 3;
            this.paypalButton.Text = "Donate with &PayPal";
            this.paypalButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.paypalButton.UseVisualStyleBackColor = true;
            this.paypalButton.Click += new System.EventHandler(this.paypalButton_Click);
            // 
            // bitcoinButton
            // 
            this.bitcoinButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bitcoinButton.Image = global::SFMTK.Properties.Resources.Bitcoin24;
            this.bitcoinButton.Location = new System.Drawing.Point(172, 164);
            this.bitcoinButton.Name = "bitcoinButton";
            this.bitcoinButton.Size = new System.Drawing.Size(152, 36);
            this.bitcoinButton.TabIndex = 4;
            this.bitcoinButton.Text = "Donate with &Bitcoin";
            this.bitcoinButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bitcoinButton.UseVisualStyleBackColor = true;
            this.bitcoinButton.Click += new System.EventHandler(this.bitcoinButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(407, 176);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(72, 24);
            this.closeButton.TabIndex = 5;
            this.closeButton.Text = "&Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // addressTextBox
            // 
            this.addressTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addressTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.addressTextBox.Location = new System.Drawing.Point(20, 132);
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.ReadOnly = true;
            this.addressTextBox.Size = new System.Drawing.Size(439, 13);
            this.addressTextBox.TabIndex = 2;
            this.addressTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.addressTextBox.Visible = false;
            this.addressTextBox.Click += new System.EventHandler(this.addressTextBoxDarkTheme_Click);
            this.addressTextBox.DoubleClick += new System.EventHandler(this.addressTextBoxDarkTheme_DoubleClick);
            // 
            // Donate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(488, 210);
            this.Controls.Add(this.addressTextBox);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.bitcoinButton);
            this.Controls.Add(this.paypalButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.heartPictureBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(504, 248);
            this.Name = "Donate";
            this.Text = "Donate";
            this.Load += new System.EventHandler(this.Donate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.heartPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox heartPictureBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button paypalButton;
        private System.Windows.Forms.Button bitcoinButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.TextBox addressTextBox;
    }
}