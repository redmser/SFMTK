﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Controls;

namespace SFMTK.Forms
{
    /// <summary>
    /// List of detected session directories.
    /// </summary>
    public partial class DetectedSessions : Form
    {
        //TODO: DetectedSessions - show how many session files are inside each folder
        
        /// <summary>
        /// Initializes a new instance of the <see cref="DetectedSessions"/> class with automatically populated paths.
        /// </summary>
        public DetectedSessions() : this(null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DetectedSessions"/> class.
        /// </summary>
        /// <param name="paths">Paths to include by default. Use null for automatic detection of paths.</param>
        /// <param name="mode">Verification when the dialog is closed.</param>
        public DetectedSessions(IEnumerable<string> paths, DetectedSessionsVerifyMode mode = DetectedSessionsVerifyMode.WarnIfMissing)
        {
            //Compiler-generated
            InitializeComponent();

            if (paths == null)
                paths = SFM.DetectSessionDirectories();

            foreach (var path in paths)
            {
                this.AddEntry(path, false);
            }

            this.VerifyBeforeClose = mode;

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        private void DetectedSessions_Load(object sender, EventArgs e) => this.CenterToParent();

        private void AddEntry(string path = "", bool applyTheme = true)
        {
            if (path == null)
                path = "";

            //Create new entry
            var dse = new DetectedSessionEntry(path);
            dse.Dock = DockStyle.Top;
            dse.DeleteClick += this.Entry_DeleteClick;
            dse.PathChanged += this.Entry_PathChanged;
            if (applyTheme)
                UITheme.ApplyTheme(dse);
            this.flowLayoutPanel.Controls.Add(dse);
            dse.GiveFocus();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            this.AddEntry();
            this._listChanged = true;
        }

        private void Entry_DeleteClick(object sender, EventArgs e)
        {
            var dse = sender as DetectedSessionEntry;
            if (dse == null)
                throw new ArgumentException("Invalid sender for DeleteClick event.");

            //Delete the sender from the list
            dse.DeleteClick -= this.Entry_DeleteClick;
            this.flowLayoutPanel.Controls.Remove(dse);
            dse.Dispose();
            this._listChanged = true;
        }

        private void Entry_PathChanged(object sender, EventArgs e) => this._listChanged = true;

        /// <summary>
        /// Gets the paths in the list, using standardized formatting.
        /// </summary>
        public IEnumerable<string> Paths => this.flowLayoutPanel.Controls.OfType<DetectedSessionEntry>().Select(e => IOHelper.GetStandardizedPath(e.Path)).Where(p => !string.IsNullOrWhiteSpace(p));

        /// <summary>
        /// Gets or sets how to verify the paths for existing before closing the form.
        /// </summary>
        public DetectedSessionsVerifyMode VerifyBeforeClose { get; set; }

        /// <summary>
        /// Whether the list's contents have changed.
        /// </summary>
        private bool _listChanged;

        private void DetectedSessions_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.Cancel)
                return;

            //Action depends on mode
            switch (this.VerifyBeforeClose)
            {
                case DetectedSessionsVerifyMode.Ignore:
                    //Simply close without any further checks
                    break;
                case DetectedSessionsVerifyMode.WarnIfMissing:
                    //Check for missing directories
                    var missing = this.Paths.Where(p => !Directory.Exists(p));
                    if (missing.Any())
                    {
                        //Warn about missing directories
                        var res = FallbackTaskDialog.ShowDialog("The following session directories do not exist. Please verify that their paths are valid:\n\n" +
                            string.Join("\n", missing), null, "Unable to create session directories", FallbackDialogIcon.Error,
                            new FallbackDialogButton("Edit Paths", DialogResult.Cancel), new FallbackDialogButton(DialogResult.Ignore));

                        if (res == DialogResult.Cancel)
                            e.Cancel = true;
                    }
                    break;
                case DetectedSessionsVerifyMode.CreateIfMissing:
                    //Create any missing directories
                    var failed = new List<string>();
                    foreach (var path in this.Paths.Where(p => !Directory.Exists(p)))
                    {
                        //Try to create
                        try
                        {
                            Directory.CreateDirectory(path);
                        }
                        catch (Exception)
                        {
                            //Store failed path
                            failed.Add(path);
                        }
                    }

                    if (failed.Any())
                    {
                        //Warn about failed directories
                        var res = FallbackTaskDialog.ShowDialogLog("Unable to create the following directories. Please verify that their paths are valid:\n\n" +
                            string.Join("\n", failed), null, "Unable to create session directories", FallbackDialogIcon.Error,
                            new FallbackDialogButton(DialogResult.OK), new FallbackDialogButton(DialogResult.Cancel));

                        if (res == DialogResult.Cancel)
                            e.Cancel = true;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(this.VerifyBeforeClose));
            }
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            //Warn if list has been changed
            if (this._listChanged)
            {
                var res = FallbackRememberDialog.ShowDialog("Would you like to reset the list of session directories to what SFMTK has detected?", "Reset session directory list?",
                    "Reset session directory list", FallbackDialogIcon.Question, new FallbackDialogButton(DialogResult.Yes), new FallbackDialogButton(DialogResult.No));

                if (res != DialogResult.Yes)
                    return;
            }

            //Reset the list!
            this.flowLayoutPanel.Controls.Clear();
            this.flowLayoutPanel.Controls.Add(new InvisibleControl());
            foreach (var path in SFM.DetectSessionDirectories())
            {
                this.AddEntry(path, true);
            }
        }
    }

    /// <summary>
    /// How to verify the paths for existing before closing the form.
    /// </summary>
    public enum DetectedSessionsVerifyMode
    {
        /// <summary>
        /// Ignore any invalid directories, simply close the dialog and save the changes.
        /// </summary>
        Ignore,
        /// <summary>
        /// Warn the user about any missing or invalid directories, prompting them to correct any.
        /// </summary>
        WarnIfMissing,
        /// <summary>
        /// Silently ignores missing directories and instead creates them, if possible.
        /// </summary>
        CreateIfMissing
    }
}
