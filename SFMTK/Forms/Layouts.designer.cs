﻿namespace SFMTK.Forms
{
    partial class Layouts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Layouts));
            this.buttonRename = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonSaveLayout = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.layoutsObjectListView = new BrightIdeasSoftware.FastObjectListView();
            this.nameOlvColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.shortcutOlvColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.layoutContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.applyLayoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editShortcutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.moveUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonMoveDown = new System.Windows.Forms.Button();
            this.buttonMoveUp = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.helpButton = new System.Windows.Forms.Button();
            this.commandManager = new SFMTK.Commands.CommandManager();
            ((System.ComponentModel.ISupportInitialize)(this.layoutsObjectListView)).BeginInit();
            this.layoutContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRename
            // 
            this.buttonRename.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.commandManager.SetCommand(this.buttonRename, "");
            this.buttonRename.Enabled = false;
            this.buttonRename.Location = new System.Drawing.Point(172, 241);
            this.buttonRename.Name = "buttonRename";
            this.buttonRename.Size = new System.Drawing.Size(75, 23);
            this.buttonRename.TabIndex = 4;
            this.buttonRename.Text = "&Rename";
            this.toolTip.SetToolTip(this.buttonRename, "Renames the selected window layout.");
            this.buttonRename.UseVisualStyleBackColor = true;
            this.buttonRename.Click += new System.EventHandler(this.buttonRename_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.commandManager.SetCommand(this.buttonDelete, "");
            this.buttonDelete.Enabled = false;
            this.buttonDelete.Location = new System.Drawing.Point(92, 241);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 3;
            this.buttonDelete.Text = "&Delete";
            this.toolTip.SetToolTip(this.buttonDelete, "Deletes the selected window layout.");
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonSaveLayout
            // 
            this.buttonSaveLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.commandManager.SetCommand(this.buttonSaveLayout, "");
            this.buttonSaveLayout.Location = new System.Drawing.Point(8, 241);
            this.buttonSaveLayout.Name = "buttonSaveLayout";
            this.buttonSaveLayout.Size = new System.Drawing.Size(80, 23);
            this.buttonSaveLayout.TabIndex = 2;
            this.buttonSaveLayout.Text = "&Save Layout";
            this.toolTip.SetToolTip(this.buttonSaveLayout, "Saves the current window layout.");
            this.buttonSaveLayout.UseVisualStyleBackColor = true;
            this.buttonSaveLayout.Click += new System.EventHandler(this.buttonSaveLayout_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(257, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Saved layouts:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // layoutsObjectListView
            // 
            this.layoutsObjectListView.AllColumns.Add(this.nameOlvColumn);
            this.layoutsObjectListView.AllColumns.Add(this.shortcutOlvColumn);
            this.layoutsObjectListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutsObjectListView.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.DoubleClick;
            this.layoutsObjectListView.CellEditTabChangesRows = true;
            this.layoutsObjectListView.CellEditUseWholeCell = false;
            this.layoutsObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameOlvColumn,
            this.shortcutOlvColumn});
            this.layoutsObjectListView.ContextMenuStrip = this.layoutContextMenuStrip;
            this.layoutsObjectListView.FullRowSelect = true;
            this.layoutsObjectListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.layoutsObjectListView.HideSelection = false;
            this.layoutsObjectListView.IsSimpleDragSource = true;
            this.layoutsObjectListView.Location = new System.Drawing.Point(8, 24);
            this.layoutsObjectListView.MultiSelect = false;
            this.layoutsObjectListView.Name = "layoutsObjectListView";
            this.layoutsObjectListView.ShowGroups = false;
            this.layoutsObjectListView.Size = new System.Drawing.Size(273, 213);
            this.layoutsObjectListView.TabIndex = 1;
            this.layoutsObjectListView.UseCellFormatEvents = true;
            this.layoutsObjectListView.UseCompatibleStateImageBehavior = false;
            this.layoutsObjectListView.View = System.Windows.Forms.View.Details;
            this.layoutsObjectListView.VirtualMode = true;
            this.layoutsObjectListView.CellEditFinishing += new BrightIdeasSoftware.CellEditEventHandler(this.OnCellEditFinishing);
            this.layoutsObjectListView.CellEditValidating += new BrightIdeasSoftware.CellEditEventHandler(this.OnCellEditValidating);
            this.layoutsObjectListView.FormatCell += new System.EventHandler<BrightIdeasSoftware.FormatCellEventArgs>(this.layoutsObjectListView_FormatCell);
            this.layoutsObjectListView.ModelDropped += new System.EventHandler<BrightIdeasSoftware.ModelDropEventArgs>(this.OnModelDropped);
            this.layoutsObjectListView.SelectionChanged += new System.EventHandler(this.OnSelectionChanged);
            // 
            // nameOlvColumn
            // 
            this.nameOlvColumn.AspectName = "Name";
            this.nameOlvColumn.AutoCompleteEditor = false;
            this.nameOlvColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.nameOlvColumn.FillsFreeSpace = true;
            this.nameOlvColumn.Text = "Name";
            // 
            // shortcutOlvColumn
            // 
            this.shortcutOlvColumn.AspectName = "ShortcutString";
            this.shortcutOlvColumn.AutoCompleteEditor = false;
            this.shortcutOlvColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.shortcutOlvColumn.Text = "Shortcut";
            this.shortcutOlvColumn.Width = 110;
            // 
            // layoutContextMenuStrip
            // 
            this.layoutContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applyLayoutToolStripMenuItem,
            this.toolStripSeparator1,
            this.deleteToolStripMenuItem,
            this.renameToolStripMenuItem,
            this.editShortcutToolStripMenuItem,
            this.toolStripSeparator2,
            this.moveUpToolStripMenuItem,
            this.moveDownToolStripMenuItem});
            this.layoutContextMenuStrip.Name = "layoutContextMenuStrip";
            this.layoutContextMenuStrip.Size = new System.Drawing.Size(138, 148);
            this.layoutContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.layoutContextMenuStrip_Opening);
            // 
            // applyLayoutToolStripMenuItem
            // 
            this.commandManager.SetCommand(this.applyLayoutToolStripMenuItem, "");
            this.applyLayoutToolStripMenuItem.Image = global::SFMTK.Properties.Resources.tick;
            this.applyLayoutToolStripMenuItem.Name = "applyLayoutToolStripMenuItem";
            this.applyLayoutToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.applyLayoutToolStripMenuItem.Text = "Apply Layout";
            this.applyLayoutToolStripMenuItem.Click += new System.EventHandler(this.applyLayoutToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.commandManager.SetCommand(this.toolStripSeparator1, "");
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(134, 6);
            // 
            // deleteToolStripMenuItem
            // 
            this.commandManager.SetCommand(this.deleteToolStripMenuItem, "");
            this.deleteToolStripMenuItem.Image = global::SFMTK.Properties.Resources.cross;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // renameToolStripMenuItem
            // 
            this.commandManager.SetCommand(this.renameToolStripMenuItem, "");
            this.renameToolStripMenuItem.Image = global::SFMTK.Properties.Resources.pencil;
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.renameToolStripMenuItem.Text = "Rename";
            this.renameToolStripMenuItem.Click += new System.EventHandler(this.buttonRename_Click);
            // 
            // editShortcutToolStripMenuItem
            // 
            this.commandManager.SetCommand(this.editShortcutToolStripMenuItem, "");
            this.editShortcutToolStripMenuItem.Name = "editShortcutToolStripMenuItem";
            this.editShortcutToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.editShortcutToolStripMenuItem.Text = "Edit Shortcut";
            this.editShortcutToolStripMenuItem.Click += new System.EventHandler(this.editShortcutToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.commandManager.SetCommand(this.toolStripSeparator2, "");
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(134, 6);
            // 
            // moveUpToolStripMenuItem
            // 
            this.commandManager.SetCommand(this.moveUpToolStripMenuItem, "");
            this.moveUpToolStripMenuItem.Image = global::SFMTK.Properties.Resources.arrow_up;
            this.moveUpToolStripMenuItem.Name = "moveUpToolStripMenuItem";
            this.moveUpToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.moveUpToolStripMenuItem.Text = "Move up";
            this.moveUpToolStripMenuItem.Click += new System.EventHandler(this.buttonMoveUp_Click);
            // 
            // moveDownToolStripMenuItem
            // 
            this.commandManager.SetCommand(this.moveDownToolStripMenuItem, "");
            this.moveDownToolStripMenuItem.Image = global::SFMTK.Properties.Resources.arrow_down;
            this.moveDownToolStripMenuItem.Name = "moveDownToolStripMenuItem";
            this.moveDownToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.moveDownToolStripMenuItem.Text = "Move down";
            this.moveDownToolStripMenuItem.Click += new System.EventHandler(this.buttonMoveDown_Click);
            // 
            // buttonMoveDown
            // 
            this.buttonMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.commandManager.SetCommand(this.buttonMoveDown, "");
            this.buttonMoveDown.Enabled = false;
            this.buttonMoveDown.Image = global::SFMTK.Properties.Resources.arrow_down;
            this.buttonMoveDown.Location = new System.Drawing.Point(285, 88);
            this.buttonMoveDown.Name = "buttonMoveDown";
            this.buttonMoveDown.Size = new System.Drawing.Size(28, 28);
            this.buttonMoveDown.TabIndex = 7;
            this.toolTip.SetToolTip(this.buttonMoveDown, "Moves the selected window layout down by one slot.");
            this.buttonMoveDown.UseVisualStyleBackColor = true;
            this.buttonMoveDown.Click += new System.EventHandler(this.buttonMoveDown_Click);
            // 
            // buttonMoveUp
            // 
            this.buttonMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.commandManager.SetCommand(this.buttonMoveUp, "");
            this.buttonMoveUp.Enabled = false;
            this.buttonMoveUp.Image = global::SFMTK.Properties.Resources.arrow_up;
            this.buttonMoveUp.Location = new System.Drawing.Point(285, 56);
            this.buttonMoveUp.Name = "buttonMoveUp";
            this.buttonMoveUp.Size = new System.Drawing.Size(28, 28);
            this.buttonMoveUp.TabIndex = 6;
            this.toolTip.SetToolTip(this.buttonMoveUp, "Moves the selected window layout up by one slot.");
            this.buttonMoveUp.UseVisualStyleBackColor = true;
            this.buttonMoveUp.Click += new System.EventHandler(this.buttonMoveUp_Click);
            // 
            // helpButton
            // 
            this.helpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.commandManager.SetCommand(this.helpButton, "HelpCommand;Layouts");
            this.helpButton.Image = ((System.Drawing.Image)(resources.GetObject("helpButton.Image")));
            this.helpButton.Location = new System.Drawing.Point(285, 24);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(28, 28);
            this.helpButton.TabIndex = 5;
            this.toolTip.SetToolTip(this.helpButton, "Shows help for window layouts.");
            this.helpButton.UseVisualStyleBackColor = true;
            // 
            // commandManager
            // 
            this.commandManager.CommandHistory = null;
            // 
            // Layouts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 270);
            this.Controls.Add(this.helpButton);
            this.Controls.Add(this.layoutsObjectListView);
            this.Controls.Add(this.buttonRename);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonSaveLayout);
            this.Controls.Add(this.buttonMoveDown);
            this.Controls.Add(this.buttonMoveUp);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(269, 187);
            this.Name = "Layouts";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Window Layouts";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LayoutsNew_FormClosing);
            this.Load += new System.EventHandler(this.LayoutsNew_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutsObjectListView)).EndInit();
            this.layoutContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRename;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonSaveLayout;
        private System.Windows.Forms.Button buttonMoveDown;
        private System.Windows.Forms.Button buttonMoveUp;
        private System.Windows.Forms.Label label1;
        private BrightIdeasSoftware.FastObjectListView layoutsObjectListView;
        private BrightIdeasSoftware.OLVColumn nameOlvColumn;
        private BrightIdeasSoftware.OLVColumn shortcutOlvColumn;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button helpButton;
        private System.Windows.Forms.ContextMenuStrip layoutContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem applyLayoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editShortcutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem moveUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveDownToolStripMenuItem;
        private Commands.CommandManager commandManager;
    }
}