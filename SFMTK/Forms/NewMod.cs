﻿using SFMTK.Data;
using System;
using System.Linq;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    /// <summary>
    /// Dialog to enter quick info about a new mod.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class NewMod : Form, IThemeable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NewMod"/> class.
        /// </summary>
        public NewMod()
        {
            //Compiler-generated
            InitializeComponent();

            //TODO: NewMod - populate the list of known mod categories

            //Update initial error marker state
            this.UpdateErrorMarker();

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        /// <summary>
        /// Gets the newly created mod instance.
        /// </summary>
        public Mod Mod { get; private set; }

        private void NewMod_Load(object sender, EventArgs e)
        {
            this.CenterToParent();
            this.modNameTextBox.Focus();
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control. This property is never set to the same value that it already has.
        /// </summary>
        public string ActiveTheme { get; set; }

        private void modNameTextBox_TextChanged(object sender, EventArgs e) => this.UpdateErrorMarker();

        private void UpdateErrorMarker()
        {
            if (string.IsNullOrWhiteSpace(this.modNameTextBox.Text))
            {
                //None specified
                this.modNameErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.modNameErrorMarker.Text = "No mod name has been specified.";
            }
            else if (!IOHelper.IsValidDirectoryName(this.modNameTextBox.Text))
            {
                //Invalid name
                //TODO: NewMod - check if mod names have any tighter limits - what about spaces?
                this.modNameErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.modNameErrorMarker.Text = "The specified mod name contains invalid characters.";
            }
            else if (SFM.GameInfo.GetMod(this.modNameTextBox.Text) != null)
            {
                //Already exists
                this.modNameErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.modNameErrorMarker.Text = "A mod with the same name already exists!";
            }
            else
            {
                //Valid!
                this.modNameErrorMarker.State = SFMTK.Controls.ErrorState.Valid;
                this.modNameErrorMarker.Text = "This mod name is valid.";
            }

            this.okButton.Enabled = this.modNameErrorMarker.State != SFMTK.Controls.ErrorState.Error;
        }

        private void modCategoryComboBox_TextUpdate(object sender, EventArgs e)
        {
            //Check if already exists
            bool exists;
            var cat = this.modCategoryComboBox.Text;
            if (string.IsNullOrWhiteSpace(cat))
            {
                //Category "exists" - don't worry about it (empty category is always empty)
                exists = true;
            }
            else
            {
                //Check if it REALLY exists
                var catUp = cat.ToUpperInvariant();
                exists = this.modCategoryComboBox.Items.OfType<string>().Any(s => s.ToUpperInvariant() == catUp);
            }
            this.modCategoryErrorMarker.State = exists ? SFMTK.Controls.ErrorState.None : SFMTK.Controls.ErrorState.Info;
            this.modCategoryErrorMarker.Text = exists ? "" : $"A new category with the name \"{cat}\" will be created.";
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Mod = new Mod(this.modNameTextBox.Text, true) { Description = this.modDescriptionTextBox.Text };
            if (!string.IsNullOrWhiteSpace(this.modCategoryComboBox.Text))
                this.Mod.Category = this.modCategoryComboBox.Text;
            this.Close();
        }
    }
}
