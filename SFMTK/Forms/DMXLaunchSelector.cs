﻿using System;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    /// <summary>
    /// Allows the user to select how to open a DMX file.
    /// </summary>
    public partial class DMXLaunchSelector : Form, IThemeable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DMXLaunchSelector"/> class without a DMX file to open.
        /// </summary>
        public DMXLaunchSelector() : this(null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DMXLaunchSelector"/> class with the specified DMX file to open.
        /// </summary>
        /// <param name="path">The path to the DMX file to open.</param>
        public DMXLaunchSelector(string path)
        {
            //Compiler-generated
            InitializeComponent();

            //Set file path and update visuals
            this.FilePath = path;
            this.titleLabel.Text = string.Format(this.titleLabel.Text, path ?? "a DMX file");

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        public string ActiveTheme { get; set; }

        /// <summary>
        /// Gets or sets the path to the DMX file to open.
        /// </summary>
        public string FilePath { get; set; }

        private void sfmtkButton_Click(object sender, EventArgs e)
        {
            //NYI: DMXLaunchSelector - launch sfmtk to open dmx
            throw new NotImplementedException();
        }

        private void sfmButton_Click(object sender, EventArgs e)
        {
            //NYI: DMXLaunchSelector - launch sfm to open dmx
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes a dialog box key.
        /// </summary>
        /// <param name="keyData">One of the <see cref="T:System.Windows.Forms.Keys" /> values that represents the key to process.</param>
        /// <returns>
        /// true if the keystroke was processed and consumed by the control; otherwise, false to allow further processing.
        /// </returns>
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }
    }
}
