﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Contents;
using SFMTK.Widgets;

namespace SFMTK.Forms
{
    /// <summary>
    /// Modern dialog for displaying a list of unsaved content, with more possibilities than the regular ol' clump of MessageBoxes.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Unsaved : Form, IThemeable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Unsaved"/> class.
        /// </summary>
        public Unsaved()
        {
            //Compiler-generated
            InitializeComponent();

            //ListView setup
            this.unsavedNameColumn.ImageGetter = (o) => ((ContentInfoWidget)o).Content.SmallImage;

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Unsaved"/> class, getting the unsaved files list from the specified DockPanelManager.
        /// </summary>
        /// <param name="manager">The DockPanelManager to retrieve the list of unsaved content from.</param>
        public Unsaved(DockPanelManager manager) : this()
        {
            this.Manager = manager;
        }

        private void Unsaved_Load(object sender, EventArgs e)
        {
            //Center me
            this.CenterToParent();
        }

        /// <summary>
        /// Gets the list of unsaved widgets.
        /// </summary>
        public IEnumerable<ContentInfoWidget> Widgets => this.Manager.UnsavedContents;

        /// <summary>
        /// Gets or sets the DockPanelManager to get the list of Unsaved content from.
        /// </summary>
        public DockPanelManager Manager
        {
            get => this._dpm;
            set
            {
                if (this._dpm == value)
                    return;

                this._dpm = value;
                this.UpdateUnsavedList();
            }
        }
        private DockPanelManager _dpm;

        private void UpdateUnsavedList()
        {
            //Populate unsaved list
            //TODO: Unsaved - will this dialog be modal? if not, may need to use a virtual list or similar, in order to live-update which ones are unsaved
            this.unsavedObjectListView.SetObjects(this.Widgets);
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control.
        /// </summary>
        public string ActiveTheme { get; set; }

        private void unsavedObjectListView_SelectionChanged(object sender, EventArgs e)
        {
            //Allow selected only if actually having a selection
            this.saveSelectedButton.Enabled = this.unsavedObjectListView.SelectedObjects.Count > 0;

            //Open content tab on main window
            var shower = this.unsavedObjectListView.SelectedObjects.OfType<ContentInfoWidget>().LastOrDefault();
            if (shower != null)
                this.Manager.Show(shower);
        }

        private void saveAllButton_Click(object sender, EventArgs e)
        {
            //Save all (unsaved) content
            this.SaveContent(this.Widgets);
        }

        private void saveSelectedButton_Click(object sender, EventArgs e)
        {
            //Save selected content
            this.SaveContent(this.unsavedObjectListView.SelectedObjects.OfType<ContentInfoWidget>());
        }

        private void SaveContent(IEnumerable<Content> content)
        {
            this.DialogResult = DialogResult.OK;
            foreach (var c in content)
            {
                var res = Content.SaveFile(c);
                if (!res)
                    this.DialogResult = DialogResult.Cancel;
            }
        }

        private void SaveContent(IEnumerable<ContentInfoWidget> widgets) => this.SaveContent(widgets.Select(w => w.Content));
    }
}
