﻿using System;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    /// <summary>
    /// A dialog box for selecting the preferred donation method.
    /// </summary>
    public partial class Donate : Form, IThemeable
    {
        //TODO: General - donate options should only show on the free version (together with a "purchase SFMTK" option)
        //  -> they disappear when the base "paid SFMTK module" is loaded

        /// <summary>
        /// Initializes a new instance of the <see cref="Donate"/> class.
        /// </summary>
        public Donate()
        {
            //Compiler-generated
            InitializeComponent();

            //Give text
            this.addressTextBox.Text = Constants.DonateBitcoinAddress;

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        private void Donate_Load(object sender, EventArgs e)
        {
            this.CenterToParent();
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control.
        /// </summary>
        public string ActiveTheme
        {
            get => this._activeTheme;
            set
            {
                this._activeTheme = value;
                this.addressTextBox.ForeColor = UITheme.SelectedTheme.SubTextColor;
            }
        }

        private string _activeTheme;

        private void paypalButton_Click(object sender, EventArgs e)
        {
            NetworkFallback.OpenInBrowser(Constants.DonatePaypalURL);
        }

        private void bitcoinButton_Click(object sender, EventArgs e)
        {
            //Show help if no worky
            this.label2.Text = "Thank you for your interest in supporting the development of SFMTK!\r\n\r\nIf the link does not work, you can find my Bitcoin address here:";
            this.addressTextBox.Visible = true;

            NetworkFallback.OpenInBrowser(Constants.DonateBitcoinURL);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void addressTextBoxDarkTheme_Click(object sender, EventArgs e)
        {
            //Select only
            this.addressTextBox.SelectAll();
        }

        private void addressTextBoxDarkTheme_DoubleClick(object sender, EventArgs e)
        {
            //Copy to clipboard
            Clipboard.SetText(this.addressTextBox.Text);
        }
    }
}
