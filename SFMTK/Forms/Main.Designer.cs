﻿namespace SFMTK.Forms
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.mainMenuBar = new System.Windows.Forms.MenuStrip();
            this.toolstripsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.customizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolbarsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllTabsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.modManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.errorListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modelViewerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.launchSFMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickLaunchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.applySetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageSetupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageLayoutsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applyLayoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.donateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.valveDeveloperCommunityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sMDOrDMXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.notificationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.fileToolStrip = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripSplitButton();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveAllToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.mainDockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.editToolStrip = new System.Windows.Forms.ToolStrip();
            this.undoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.redoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.deleteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.reopenMenuPanel = new System.Windows.Forms.Panel();
            this.launchToolStrip = new System.Windows.Forms.ToolStrip();
            this.setupToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.launchToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.applySetupToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.manageSetupsToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.mainStatusStrip = new SFMTK.Controls.StatusStripEx();
            this.mainToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.mainToolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.cancelToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.notificationsToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.CommandManager = new SFMTK.Commands.CommandManager();
            this.mainMenuBar.SuspendLayout();
            this.toolstripsContextMenuStrip.SuspendLayout();
            this.fileToolStrip.SuspendLayout();
            this.editToolStrip.SuspendLayout();
            this.launchToolStrip.SuspendLayout();
            this.mainStatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenuBar
            // 
            this.mainMenuBar.ContextMenuStrip = this.toolstripsContextMenuStrip;
            this.mainMenuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainMenuBar.Location = new System.Drawing.Point(0, 0);
            this.mainMenuBar.MdiWindowListItem = this.windowToolStripMenuItem;
            this.mainMenuBar.Name = "mainMenuBar";
            this.mainMenuBar.Size = new System.Drawing.Size(928, 24);
            this.mainMenuBar.TabIndex = 0;
            this.mainMenuBar.Text = "Menu Bar";
            this.mainMenuBar.VisibleChanged += new System.EventHandler(this.mainMenuBar_VisibleChanged);
            // 
            // toolstripsContextMenuStrip
            // 
            this.toolstripsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator15,
            this.customizeToolStripMenuItem});
            this.toolstripsContextMenuStrip.Name = "toolstripsContextMenuStrip";
            this.toolstripsContextMenuStrip.OwnerItem = this.toolbarsToolStripMenuItem;
            this.toolstripsContextMenuStrip.Size = new System.Drawing.Size(136, 32);
            // 
            // toolStripSeparator15
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator15, "");
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(132, 6);
            // 
            // customizeToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.customizeToolStripMenuItem, "CustomizeCommand");
            this.customizeToolStripMenuItem.Name = "customizeToolStripMenuItem";
            this.customizeToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.customizeToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.customizeToolStripMenuItem.Text = "&Customize...";
            this.customizeToolStripMenuItem.ToolTipText = "Opens an editor for customizing all toolbars";
            // 
            // toolbarsToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.toolbarsToolStripMenuItem, "");
            this.toolbarsToolStripMenuItem.DropDown = this.toolstripsContextMenuStrip;
            this.toolbarsToolStripMenuItem.Name = "toolbarsToolStripMenuItem";
            this.toolbarsToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.toolbarsToolStripMenuItem.Text = "&Toolbars";
            this.toolbarsToolStripMenuItem.ToolTipText = "Toggle visibility of toolbars or customize them";
            // 
            // fileToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.fileToolStripMenuItem, "");
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newMenuToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator4,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.saveAllToolStripMenuItem,
            this.toolStripSeparator13,
            this.closeToolStripMenuItem,
            this.closeAllTabsToolStripMenuItem,
            this.toolStripSeparator3,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            this.fileToolStripMenuItem.DropDownOpening += new System.EventHandler(this.fileToolStripMenuItem_DropDownOpening);
            // 
            // newMenuToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.newMenuToolStripMenuItem, "NewFileCommand");
            this.newMenuToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newMenuToolStripMenuItem.Image")));
            this.newMenuToolStripMenuItem.Name = "newMenuToolStripMenuItem";
            this.newMenuToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+N";
            this.newMenuToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newMenuToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.newMenuToolStripMenuItem.Text = "&New...";
            this.newMenuToolStripMenuItem.ToolTipText = "Create new content, either from existing files or from scratch";
            // 
            // openToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.openToolStripMenuItem, "OpenFileCommand");
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+O";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.openToolStripMenuItem.Text = "&Open...";
            this.openToolStripMenuItem.ToolTipText = "Open content for editing or analysis";
            // 
            // toolStripSeparator4
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator4, "");
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(208, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.saveToolStripMenuItem, "SaveFileCommand");
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+S";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.ToolTipText = "Save the currently active content";
            // 
            // saveAsToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.saveAsToolStripMenuItem, "SaveFileAsCommand");
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+S";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.saveAsToolStripMenuItem.Text = "Save &as...";
            this.saveAsToolStripMenuItem.ToolTipText = "Save the currently active content to a new file or format";
            // 
            // saveAllToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.saveAllToolStripMenuItem, "SaveAllFilesCommand");
            this.saveAllToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveAllToolStripMenuItem.Image")));
            this.saveAllToolStripMenuItem.Name = "saveAllToolStripMenuItem";
            this.saveAllToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Alt+S";
            this.saveAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
            this.saveAllToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.saveAllToolStripMenuItem.Text = "Sav&e all";
            this.saveAllToolStripMenuItem.ToolTipText = "Save all open content";
            // 
            // toolStripSeparator13
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator13, "");
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(208, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.closeToolStripMenuItem, "CloseTabCommand");
            this.closeToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("closeToolStripMenuItem.Image")));
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+W";
            this.closeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.closeToolStripMenuItem.Text = "&Close Tab";
            this.closeToolStripMenuItem.ToolTipText = "Closes the active tab";
            // 
            // closeAllTabsToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.closeAllTabsToolStripMenuItem, "CloseAllTabsCommand");
            this.closeAllTabsToolStripMenuItem.Name = "closeAllTabsToolStripMenuItem";
            this.closeAllTabsToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+W";
            this.closeAllTabsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.W)));
            this.closeAllTabsToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.closeAllTabsToolStripMenuItem.Text = "Close &all Tabs";
            this.closeAllTabsToolStripMenuItem.ToolTipText = "Closes all active tabs";
            // 
            // toolStripSeparator3
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator3, "");
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(208, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.exitToolStripMenuItem, "ExitCommand");
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.ToolTipText = "Exit the program";
            // 
            // editToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.editToolStripMenuItem, "");
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator1,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.undoToolStripMenuItem, "UndoCommand");
            this.undoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("undoToolStripMenuItem.Image")));
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Z";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.undoToolStripMenuItem.Text = "&Undo";
            this.undoToolStripMenuItem.ToolTipText = "Undoes the last change";
            // 
            // redoToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.redoToolStripMenuItem, "RedoCommand");
            this.redoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("redoToolStripMenuItem.Image")));
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+Z";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.Z)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.redoToolStripMenuItem.Text = "&Redo";
            this.redoToolStripMenuItem.ToolTipText = "Redoes the last undone change";
            // 
            // toolStripSeparator1
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator1, "");
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(164, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.cutToolStripMenuItem, "CutCommand");
            this.cutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripMenuItem.Image")));
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+X";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.cutToolStripMenuItem.Text = "Cu&t";
            this.cutToolStripMenuItem.ToolTipText = "Cuts the selected element(s)";
            // 
            // copyToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.copyToolStripMenuItem, "CopyCommand");
            this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+C";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.copyToolStripMenuItem.Text = "&Copy";
            this.copyToolStripMenuItem.ToolTipText = "Copies the selected element(s)";
            // 
            // pasteToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.pasteToolStripMenuItem, "PasteCommand");
            this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+V";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.pasteToolStripMenuItem.Text = "&Paste";
            this.pasteToolStripMenuItem.ToolTipText = "Pastes element(s) from the clipboard";
            // 
            // deleteToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.deleteToolStripMenuItem, "DeleteCommand");
            this.deleteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("deleteToolStripMenuItem.Image")));
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeyDisplayString = "Delete";
            this.deleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.deleteToolStripMenuItem.Text = "&Delete";
            this.deleteToolStripMenuItem.ToolTipText = "Deletes the selected element(s)";
            // 
            // viewToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.viewToolStripMenuItem, "");
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoHistoryToolStripMenuItem,
            this.toolStripSeparator9,
            this.modManagerToolStripMenuItem,
            this.errorListToolStripMenuItem,
            this.modelViewerToolStripMenuItem,
            this.toolStripSeparator6,
            this.toolbarsToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // undoHistoryToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.undoHistoryToolStripMenuItem, "ShowWidgetCommand;UndoHistoryWidget");
            this.undoHistoryToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("undoHistoryToolStripMenuItem.Image")));
            this.undoHistoryToolStripMenuItem.Name = "undoHistoryToolStripMenuItem";
            this.undoHistoryToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Alt+Z";
            this.undoHistoryToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.Z)));
            this.undoHistoryToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.undoHistoryToolStripMenuItem.Text = "&Undo History";
            this.undoHistoryToolStripMenuItem.ToolTipText = "Shows the &Undo History";
            // 
            // toolStripSeparator9
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator9, "");
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(192, 6);
            // 
            // modManagerToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.modManagerToolStripMenuItem, "ShowWidgetCommand;ModManagerWidget");
            this.modManagerToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("modManagerToolStripMenuItem.Image")));
            this.modManagerToolStripMenuItem.Name = "modManagerToolStripMenuItem";
            this.modManagerToolStripMenuItem.ShortcutKeyDisplayString = "F2";
            this.modManagerToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.modManagerToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.modManagerToolStripMenuItem.Text = "&Mod Manager";
            this.modManagerToolStripMenuItem.ToolTipText = "Shows the &Mod Manager";
            // 
            // errorListToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.errorListToolStripMenuItem, "ShowWidgetCommand;ErrorListWidget");
            this.errorListToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("errorListToolStripMenuItem.Image")));
            this.errorListToolStripMenuItem.Name = "errorListToolStripMenuItem";
            this.errorListToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.errorListToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.errorListToolStripMenuItem.Text = "&Error List";
            this.errorListToolStripMenuItem.ToolTipText = "Shows the &Error List";
            // 
            // modelViewerToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.modelViewerToolStripMenuItem, "ShowWidgetCommand;ModelViewerWidget");
            this.modelViewerToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("modelViewerToolStripMenuItem.Image")));
            this.modelViewerToolStripMenuItem.Name = "modelViewerToolStripMenuItem";
            this.modelViewerToolStripMenuItem.ShortcutKeyDisplayString = "F5";
            this.modelViewerToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.modelViewerToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.modelViewerToolStripMenuItem.Text = "Model &Viewer";
            this.modelViewerToolStripMenuItem.ToolTipText = "Shows the Model &Viewer";
            // 
            // toolStripSeparator6
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator6, "");
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(192, 6);
            // 
            // toolsToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.toolsToolStripMenuItem, "");
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.launchSFMToolStripMenuItem,
            this.applySetupToolStripMenuItem,
            this.manageSetupsToolStripMenuItem,
            this.toolStripSeparator2,
            this.preferencesToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // launchSFMToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.launchSFMToolStripMenuItem, "");
            this.launchSFMToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quickLaunchToolStripMenuItem,
            this.toolStripSeparator14});
            this.launchSFMToolStripMenuItem.Image = global::SFMTK.Properties.Resources.sfm16;
            this.launchSFMToolStripMenuItem.Name = "launchSFMToolStripMenuItem";
            this.launchSFMToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.launchSFMToolStripMenuItem.Text = "Launch &SFM";
            this.launchSFMToolStripMenuItem.ToolTipText = "Launch SFM with different setups";
            // 
            // quickLaunchToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.quickLaunchToolStripMenuItem, "QuickLaunchCommand");
            this.quickLaunchToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("quickLaunchToolStripMenuItem.Image")));
            this.quickLaunchToolStripMenuItem.Name = "quickLaunchToolStripMenuItem";
            this.quickLaunchToolStripMenuItem.ShortcutKeyDisplayString = "F11";
            this.quickLaunchToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.quickLaunchToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickLaunchToolStripMenuItem.Text = "&Quick-Launch";
            this.quickLaunchToolStripMenuItem.ToolTipText = "Launches SFM using the currently selected setup (Default)";
            // 
            // toolStripSeparator14
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator14, "");
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(160, 6);
            // 
            // applySetupToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.applySetupToolStripMenuItem, "");
            this.applySetupToolStripMenuItem.Image = global::SFMTK.Properties.Resources.computer_go;
            this.applySetupToolStripMenuItem.Name = "applySetupToolStripMenuItem";
            this.applySetupToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.applySetupToolStripMenuItem.Text = "Apply Setup";
            this.applySetupToolStripMenuItem.ToolTipText = "Setups allow you to define certain launch configs for SFM";
            // 
            // manageSetupsToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.manageSetupsToolStripMenuItem, "ManageSetupsCommand");
            this.manageSetupsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("manageSetupsToolStripMenuItem.Image")));
            this.manageSetupsToolStripMenuItem.Name = "manageSetupsToolStripMenuItem";
            this.manageSetupsToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.manageSetupsToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.manageSetupsToolStripMenuItem.Text = "&Manage Setups...";
            this.manageSetupsToolStripMenuItem.ToolTipText = "Opens an editor for creating new setups or editing existing ones";
            // 
            // toolStripSeparator2
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator2, "");
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(166, 6);
            // 
            // preferencesToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.preferencesToolStripMenuItem, "PreferencesCommand");
            this.preferencesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("preferencesToolStripMenuItem.Image")));
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.ShortcutKeyDisplayString = "F10";
            this.preferencesToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.preferencesToolStripMenuItem.Text = "&Preferences...";
            this.preferencesToolStripMenuItem.ToolTipText = "Opens the application preferences";
            // 
            // windowToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.windowToolStripMenuItem, "");
            this.windowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageLayoutsToolStripMenuItem,
            this.applyLayoutToolStripMenuItem,
            this.windowToolStripSeparator});
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.windowToolStripMenuItem.Text = "&Window";
            this.windowToolStripMenuItem.DropDownOpening += new System.EventHandler(this.windowToolStripMenuItem_DropDownOpening);
            // 
            // manageLayoutsToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.manageLayoutsToolStripMenuItem, "");
            this.manageLayoutsToolStripMenuItem.Image = global::SFMTK.Properties.Resources.application_form_edit;
            this.manageLayoutsToolStripMenuItem.Name = "manageLayoutsToolStripMenuItem";
            this.manageLayoutsToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.manageLayoutsToolStripMenuItem.Text = "&Manage Layouts...";
            this.manageLayoutsToolStripMenuItem.ToolTipText = "Opens a manager for adding or editing window layouts";
            this.manageLayoutsToolStripMenuItem.Click += new System.EventHandler(this.manageLayoutsToolStripMenuItem_Click);
            // 
            // applyLayoutToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.applyLayoutToolStripMenuItem, "");
            this.applyLayoutToolStripMenuItem.Name = "applyLayoutToolStripMenuItem";
            this.applyLayoutToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.applyLayoutToolStripMenuItem.Text = "&Apply Layout";
            this.applyLayoutToolStripMenuItem.ToolTipText = "Applies a window layout to the interface";
            // 
            // windowToolStripSeparator
            // 
            this.CommandManager.SetCommand(this.windowToolStripSeparator, "");
            this.windowToolStripSeparator.Name = "windowToolStripSeparator";
            this.windowToolStripSeparator.Size = new System.Drawing.Size(162, 6);
            // 
            // helpToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.helpToolStripMenuItem, "");
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.donateToolStripMenuItem,
            this.toolStripSeparator8,
            this.valveDeveloperCommunityToolStripMenuItem,
            this.sMDOrDMXToolStripMenuItem,
            this.toolStripSeparator7,
            this.notificationsToolStripMenuItem,
            this.toolStripSeparator10,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // donateToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.donateToolStripMenuItem, "DonateCommand");
            this.donateToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("donateToolStripMenuItem.Image")));
            this.donateToolStripMenuItem.Name = "donateToolStripMenuItem";
            this.donateToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.donateToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.donateToolStripMenuItem.Text = "&Donate!";
            this.donateToolStripMenuItem.ToolTipText = "If you like this software, a donation is always appreciated!";
            // 
            // toolStripSeparator8
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator8, "");
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(224, 6);
            // 
            // valveDeveloperCommunityToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.valveDeveloperCommunityToolStripMenuItem, "");
            this.valveDeveloperCommunityToolStripMenuItem.Name = "valveDeveloperCommunityToolStripMenuItem";
            this.valveDeveloperCommunityToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.valveDeveloperCommunityToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.valveDeveloperCommunityToolStripMenuItem.Text = "&Valve Developer Community";
            this.valveDeveloperCommunityToolStripMenuItem.ToolTipText = "Opens the Valve Developer Community main page, for searching up anything Source-r" +
    "elated";
            // 
            // sMDOrDMXToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.sMDOrDMXToolStripMenuItem, "SmdOrDmxCommand");
            this.sMDOrDMXToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sMDOrDMXToolStripMenuItem.Image")));
            this.sMDOrDMXToolStripMenuItem.Name = "sMDOrDMXToolStripMenuItem";
            this.sMDOrDMXToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.sMDOrDMXToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.sMDOrDMXToolStripMenuItem.Text = "&SMD or DMX?";
            this.sMDOrDMXToolStripMenuItem.ToolTipText = "Opens a wizard to help you decide whether to use the SMD or DMX format for models" +
    ".";
            // 
            // toolStripSeparator7
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator7, "");
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(224, 6);
            // 
            // notificationsToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.notificationsToolStripMenuItem, "");
            this.notificationsToolStripMenuItem.Name = "notificationsToolStripMenuItem";
            this.notificationsToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.notificationsToolStripMenuItem.Text = "&Notifications";
            this.notificationsToolStripMenuItem.ToolTipText = "Check all active software notifications";
            // 
            // toolStripSeparator10
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator10, "");
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(224, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.aboutToolStripMenuItem, "AboutCommand");
            this.aboutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("aboutToolStripMenuItem.Image")));
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.ToolTipText = "Information about the program and who made it possible";
            // 
            // mainToolStripPanel
            // 
            this.mainToolStripPanel.ContextMenuStrip = this.toolstripsContextMenuStrip;
            this.mainToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.mainToolStripPanel.Location = new System.Drawing.Point(0, 24);
            this.mainToolStripPanel.Name = "mainToolStripPanel";
            this.mainToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.mainToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.mainToolStripPanel.Size = new System.Drawing.Size(928, 0);
            // 
            // fileToolStrip
            // 
            this.fileToolStrip.AllowItemReorder = true;
            this.fileToolStrip.ContextMenuStrip = this.toolstripsContextMenuStrip;
            this.fileToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.fileToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.saveAllToolStripButton});
            this.fileToolStrip.Location = new System.Drawing.Point(0, 24);
            this.fileToolStrip.Name = "fileToolStrip";
            this.fileToolStrip.Size = new System.Drawing.Size(113, 25);
            this.fileToolStrip.TabIndex = 3;
            this.fileToolStrip.Text = "File";
            // 
            // newToolStripButton
            // 
            this.CommandManager.SetCommand(this.newToolStripButton, "NewFileCommand");
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.modToolStripMenuItem,
            this.toolStripSeparator11});
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(32, 22);
            this.newToolStripButton.Text = "&New...";
            this.newToolStripButton.ToolTipText = "New... (Ctrl+N)";
            // 
            // newToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.newToolStripMenuItem, "NewFileCommand");
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+N";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.newToolStripMenuItem.Text = "&New...";
            this.newToolStripMenuItem.ToolTipText = "Create new content, either from existing files or from scratch";
            // 
            // modToolStripMenuItem
            // 
            this.CommandManager.SetCommand(this.modToolStripMenuItem, "NewModCommand");
            this.modToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("modToolStripMenuItem.Image")));
            this.modToolStripMenuItem.Name = "modToolStripMenuItem";
            this.modToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.modToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.modToolStripMenuItem.Text = "&New Mod...";
            this.modToolStripMenuItem.ToolTipText = "Create a new mod and optionally import content into it";
            // 
            // toolStripSeparator11
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator11, "");
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(143, 6);
            // 
            // openToolStripButton
            // 
            this.CommandManager.SetCommand(this.openToolStripButton, "OpenFileCommand");
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "&Open...";
            this.openToolStripButton.ToolTipText = "Open... (Ctrl+O)";
            // 
            // saveToolStripButton
            // 
            this.CommandManager.SetCommand(this.saveToolStripButton, "SaveFileCommand");
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "&Save";
            this.saveToolStripButton.ToolTipText = "Save (Ctrl+S)";
            // 
            // saveAllToolStripButton
            // 
            this.CommandManager.SetCommand(this.saveAllToolStripButton, "SaveAllFilesCommand");
            this.saveAllToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveAllToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveAllToolStripButton.Image")));
            this.saveAllToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveAllToolStripButton.Name = "saveAllToolStripButton";
            this.saveAllToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveAllToolStripButton.Text = "Sav&e all";
            this.saveAllToolStripButton.ToolTipText = "Save all (Ctrl+Alt+S)";
            // 
            // mainDockPanel
            // 
            this.mainDockPanel.AllowDrop = true;
            this.mainDockPanel.ContextMenuStrip = this.toolstripsContextMenuStrip;
            this.mainDockPanel.DefaultFloatWindowSize = new System.Drawing.Size(640, 480);
            this.mainDockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDockPanel.Location = new System.Drawing.Point(0, 24);
            this.mainDockPanel.Name = "mainDockPanel";
            this.mainDockPanel.ShowDocumentIcon = true;
            this.mainDockPanel.Size = new System.Drawing.Size(928, 607);
            this.mainDockPanel.TabIndex = 5;
            this.mainDockPanel.ActiveContentChanged += new System.EventHandler(this.mainDockPanel_ActiveContentChanged);
            this.mainDockPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.mainDockPanel_DragDrop);
            this.mainDockPanel.DragOver += new System.Windows.Forms.DragEventHandler(this.mainDockPanel_DragOver);
            // 
            // editToolStrip
            // 
            this.editToolStrip.ContextMenuStrip = this.toolstripsContextMenuStrip;
            this.editToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.editToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripButton,
            this.redoToolStripButton,
            this.toolStripSeparator5,
            this.cutToolStripButton,
            this.copyToolStripButton,
            this.pasteToolStripButton,
            this.deleteToolStripButton});
            this.editToolStrip.Location = new System.Drawing.Point(124, 24);
            this.editToolStrip.Name = "editToolStrip";
            this.editToolStrip.Size = new System.Drawing.Size(156, 25);
            this.editToolStrip.TabIndex = 9;
            this.editToolStrip.Text = "Edit";
            // 
            // undoToolStripButton
            // 
            this.CommandManager.SetCommand(this.undoToolStripButton, "UndoCommand");
            this.undoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.undoToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("undoToolStripButton.Image")));
            this.undoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.undoToolStripButton.Name = "undoToolStripButton";
            this.undoToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.undoToolStripButton.Text = "&Undo";
            this.undoToolStripButton.ToolTipText = "Undo (Ctrl+Z)";
            // 
            // redoToolStripButton
            // 
            this.CommandManager.SetCommand(this.redoToolStripButton, "RedoCommand");
            this.redoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.redoToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("redoToolStripButton.Image")));
            this.redoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.redoToolStripButton.Name = "redoToolStripButton";
            this.redoToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.redoToolStripButton.Text = "&Redo";
            this.redoToolStripButton.ToolTipText = "Redo (Ctrl+Shift+Z)";
            // 
            // toolStripSeparator5
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator5, "");
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // cutToolStripButton
            // 
            this.CommandManager.SetCommand(this.cutToolStripButton, "CutCommand");
            this.cutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cutToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripButton.Image")));
            this.cutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripButton.Name = "cutToolStripButton";
            this.cutToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.cutToolStripButton.Text = "Cu&t";
            this.cutToolStripButton.ToolTipText = "Cut (Ctrl+X)";
            // 
            // copyToolStripButton
            // 
            this.CommandManager.SetCommand(this.copyToolStripButton, "CopyCommand");
            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripButton.Image")));
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.copyToolStripButton.Text = "&Copy";
            this.copyToolStripButton.ToolTipText = "Copy (Ctrl+C)";
            // 
            // pasteToolStripButton
            // 
            this.CommandManager.SetCommand(this.pasteToolStripButton, "PasteCommand");
            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripButton.Image")));
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.pasteToolStripButton.Text = "&Paste";
            this.pasteToolStripButton.ToolTipText = "Paste (Ctrl+V)";
            // 
            // deleteToolStripButton
            // 
            this.CommandManager.SetCommand(this.deleteToolStripButton, "DeleteCommand");
            this.deleteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteToolStripButton.Image")));
            this.deleteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteToolStripButton.Name = "deleteToolStripButton";
            this.deleteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.deleteToolStripButton.Text = "&Delete";
            this.deleteToolStripButton.ToolTipText = "Delete (Delete)";
            // 
            // reopenMenuPanel
            // 
            this.reopenMenuPanel.BackColor = System.Drawing.Color.Transparent;
            this.reopenMenuPanel.Location = new System.Drawing.Point(0, 0);
            this.reopenMenuPanel.Name = "reopenMenuPanel";
            this.reopenMenuPanel.Size = new System.Drawing.Size(8, 8);
            this.reopenMenuPanel.TabIndex = 17;
            this.reopenMenuPanel.Click += new System.EventHandler(this.reopenMenuPanel_Click);
            this.reopenMenuPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.reopenMenuPanel_Paint);
            // 
            // launchToolStrip
            // 
            this.launchToolStrip.ContextMenuStrip = this.toolstripsContextMenuStrip;
            this.launchToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.launchToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setupToolStripComboBox,
            this.launchToolStripButton,
            this.applySetupToolStripButton,
            this.toolStripSeparator12,
            this.manageSetupsToolStripButton});
            this.launchToolStrip.Location = new System.Drawing.Point(292, 24);
            this.launchToolStrip.Name = "launchToolStrip";
            this.launchToolStrip.Size = new System.Drawing.Size(184, 25);
            this.launchToolStrip.TabIndex = 21;
            this.launchToolStrip.Text = "Launch";
            // 
            // setupToolStripComboBox
            // 
            this.setupToolStripComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.setupToolStripComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CommandManager.SetCommand(this.setupToolStripComboBox, "");
            this.setupToolStripComboBox.Name = "setupToolStripComboBox";
            this.setupToolStripComboBox.Size = new System.Drawing.Size(95, 25);
            // 
            // launchToolStripButton
            // 
            this.CommandManager.SetCommand(this.launchToolStripButton, "");
            this.launchToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.launchToolStripButton.Image = global::SFMTK.Properties.Resources.sfm16;
            this.launchToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.launchToolStripButton.Name = "launchToolStripButton";
            this.launchToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.launchToolStripButton.Text = "Launch SFM with the selected setup";
            // 
            // applySetupToolStripButton
            // 
            this.CommandManager.SetCommand(this.applySetupToolStripButton, "");
            this.applySetupToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.applySetupToolStripButton.Image = global::SFMTK.Properties.Resources.computer_go;
            this.applySetupToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.applySetupToolStripButton.Name = "applySetupToolStripButton";
            this.applySetupToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.applySetupToolStripButton.Text = "Apply selected setup";
            // 
            // toolStripSeparator12
            // 
            this.CommandManager.SetCommand(this.toolStripSeparator12, "");
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 25);
            // 
            // manageSetupsToolStripButton
            // 
            this.CommandManager.SetCommand(this.manageSetupsToolStripButton, "ManageSetupsCommand");
            this.manageSetupsToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.manageSetupsToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("manageSetupsToolStripButton.Image")));
            this.manageSetupsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.manageSetupsToolStripButton.Name = "manageSetupsToolStripButton";
            this.manageSetupsToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.manageSetupsToolStripButton.Text = "&Manage Setups...";
            this.manageSetupsToolStripButton.ToolTipText = "Manage Setups...";
            // 
            // mainStatusStrip
            // 
            this.mainStatusStrip.AllowItemReorder = true;
            this.mainStatusStrip.ContextMenuStrip = this.toolstripsContextMenuStrip;
            this.mainStatusStrip.DataBindings.Add(new System.Windows.Forms.Binding("Visible", global::SFMTK.Properties.Settings.Default, "StatusBar", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.mainStatusStrip.DefaultText = "Ready";
            this.mainStatusStrip.DisplayText = null;
            this.mainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainToolStripStatusLabel,
            this.mainToolStripProgressBar,
            this.cancelToolStripSplitButton,
            this.notificationsToolStripSplitButton});
            this.mainStatusStrip.Location = new System.Drawing.Point(0, 631);
            this.mainStatusStrip.Name = "mainStatusStrip";
            this.mainStatusStrip.ShowItemToolTips = true;
            this.mainStatusStrip.Size = new System.Drawing.Size(928, 22);
            this.mainStatusStrip.TabIndex = 1;
            this.mainStatusStrip.Text = "Status Bar";
            this.mainStatusStrip.Visible = global::SFMTK.Properties.Settings.Default.StatusBar;
            this.mainStatusStrip.DisplayTextChanged += new System.EventHandler(this.mainStatusStrip_DisplayTextChanged);
            // 
            // mainToolStripStatusLabel
            // 
            this.CommandManager.SetCommand(this.mainToolStripStatusLabel, "");
            this.mainToolStripStatusLabel.Name = "mainToolStripStatusLabel";
            this.mainToolStripStatusLabel.Size = new System.Drawing.Size(881, 17);
            this.mainToolStripStatusLabel.Spring = true;
            this.mainToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mainToolStripProgressBar
            // 
            this.CommandManager.SetCommand(this.mainToolStripProgressBar, "");
            this.mainToolStripProgressBar.Name = "mainToolStripProgressBar";
            this.mainToolStripProgressBar.Size = new System.Drawing.Size(90, 16);
            this.mainToolStripProgressBar.Visible = false;
            // 
            // cancelToolStripSplitButton
            // 
            this.cancelToolStripSplitButton.AutoToolTip = false;
            this.CommandManager.SetCommand(this.cancelToolStripSplitButton, "");
            this.cancelToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cancelToolStripSplitButton.DropDownButtonWidth = 0;
            this.cancelToolStripSplitButton.Image = global::SFMTK.Properties.Resources.cancel;
            this.cancelToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cancelToolStripSplitButton.Margin = new System.Windows.Forms.Padding(0, 2, 8, 0);
            this.cancelToolStripSplitButton.Name = "cancelToolStripSplitButton";
            this.cancelToolStripSplitButton.Size = new System.Drawing.Size(21, 20);
            this.cancelToolStripSplitButton.Text = "Cancel";
            this.cancelToolStripSplitButton.ToolTipText = "Cancels the current action";
            this.cancelToolStripSplitButton.Visible = false;
            this.cancelToolStripSplitButton.Click += new System.EventHandler(this.cancelToolStripSplitButton_Click);
            // 
            // notificationsToolStripSplitButton
            // 
            this.notificationsToolStripSplitButton.AutoToolTip = false;
            this.CommandManager.SetCommand(this.notificationsToolStripSplitButton, "");
            this.notificationsToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.notificationsToolStripSplitButton.Image = global::SFMTK.Properties.Resources.flag_gray;
            this.notificationsToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.notificationsToolStripSplitButton.Name = "notificationsToolStripSplitButton";
            this.notificationsToolStripSplitButton.Size = new System.Drawing.Size(32, 20);
            this.notificationsToolStripSplitButton.Text = "Notifications";
            this.notificationsToolStripSplitButton.ToolTipText = "No new notifications";
            this.notificationsToolStripSplitButton.ButtonClick += new System.EventHandler(this.notificationsToolStripSplitButton_ButtonClick);
            // 
            // CommandManager
            // 
            this.CommandManager.CommandHistory = null;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 653);
            this.Controls.Add(this.reopenMenuPanel);
            this.Controls.Add(this.launchToolStrip);
            this.Controls.Add(this.editToolStrip);
            this.Controls.Add(this.fileToolStrip);
            this.Controls.Add(this.mainDockPanel);
            this.Controls.Add(this.mainToolStripPanel);
            this.Controls.Add(this.mainStatusStrip);
            this.Controls.Add(this.mainMenuBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mainMenuBar;
            this.MinimumSize = new System.Drawing.Size(282, 169);
            this.Name = "Main";
            this.Text = "SFMTK";
            this.mainMenuBar.ResumeLayout(false);
            this.mainMenuBar.PerformLayout();
            this.toolstripsContextMenuStrip.ResumeLayout(false);
            this.fileToolStrip.ResumeLayout(false);
            this.fileToolStrip.PerformLayout();
            this.editToolStrip.ResumeLayout(false);
            this.editToolStrip.PerformLayout();
            this.launchToolStrip.ResumeLayout(false);
            this.launchToolStrip.PerformLayout();
            this.mainStatusStrip.ResumeLayout(false);
            this.mainStatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public SFMTK.Controls.StatusStripEx mainStatusStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripPanel mainToolStripPanel;
        private System.Windows.Forms.ToolStrip fileToolStrip;
        public WeifenLuo.WinFormsUI.Docking.DockPanel mainDockPanel;
        private System.Windows.Forms.ToolStripStatusLabel mainToolStripStatusLabel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripButton saveAllToolStripButton;
        private System.Windows.Forms.ToolStrip editToolStrip;
        private System.Windows.Forms.ToolStripButton undoToolStripButton;
        private System.Windows.Forms.ToolStripButton redoToolStripButton;
        private System.Windows.Forms.ToolStripButton cutToolStripButton;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripButton deleteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem modelViewerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolbarsToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip toolstripsContextMenuStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageLayoutsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator windowToolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem applyLayoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem donateToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem valveDeveloperCommunityToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem errorListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem launchSFMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applySetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton newToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem notificationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSplitButton notificationsToolStripSplitButton;
        private System.Windows.Forms.ToolStrip launchToolStrip;
        private System.Windows.Forms.ToolStripComboBox setupToolStripComboBox;
        private System.Windows.Forms.ToolStripButton launchToolStripButton;
        private System.Windows.Forms.ToolStripButton applySetupToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripButton manageSetupsToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem modToolStripMenuItem;
        public Commands.CommandManager CommandManager;
        public System.Windows.Forms.MenuStrip mainMenuBar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sMDOrDMXToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllTabsToolStripMenuItem;
        public System.Windows.Forms.ToolStripProgressBar mainToolStripProgressBar;
        private System.Windows.Forms.ToolStripSplitButton cancelToolStripSplitButton;
        private System.Windows.Forms.ToolStripMenuItem manageSetupsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickLaunchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        internal System.Windows.Forms.Panel reopenMenuPanel;
    }
}

