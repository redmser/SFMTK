﻿namespace SFMTK.Forms
{
    partial class Preferences
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Preferences));
            this.resetAllButton = new System.Windows.Forms.Button();
            this.bottomTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.applyButton = new System.Windows.Forms.Button();
            this.bottomSpacingPanel = new System.Windows.Forms.Panel();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.pageTreeListView = new BrightIdeasSoftware.TreeListView();
            this.pageNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.searchTextBox = new SFMTK.Controls.SearchTextBox();
            this.pagePanel = new System.Windows.Forms.Panel();
            this.bottomTableLayoutPanel.SuspendLayout();
            this.bottomSpacingPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pageTreeListView)).BeginInit();
            this.SuspendLayout();
            // 
            // resetAllButton
            // 
            this.resetAllButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resetAllButton.Location = new System.Drawing.Point(0, 0);
            this.resetAllButton.Margin = new System.Windows.Forms.Padding(0);
            this.resetAllButton.Name = "resetAllButton";
            this.resetAllButton.Size = new System.Drawing.Size(86, 28);
            this.resetAllButton.TabIndex = 3;
            this.resetAllButton.Text = "Reset settings";
            this.resetAllButton.UseVisualStyleBackColor = true;
            this.resetAllButton.Click += new System.EventHandler(this.resetAllButton_Click);
            // 
            // bottomTableLayoutPanel
            // 
            this.bottomTableLayoutPanel.ColumnCount = 5;
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.bottomTableLayoutPanel.Controls.Add(this.resetAllButton, 0, 0);
            this.bottomTableLayoutPanel.Controls.Add(this.okButton, 2, 0);
            this.bottomTableLayoutPanel.Controls.Add(this.cancelButton, 3, 0);
            this.bottomTableLayoutPanel.Controls.Add(this.applyButton, 4, 0);
            this.bottomTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottomTableLayoutPanel.Location = new System.Drawing.Point(0, 4);
            this.bottomTableLayoutPanel.Name = "bottomTableLayoutPanel";
            this.bottomTableLayoutPanel.RowCount = 1;
            this.bottomTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.bottomTableLayoutPanel.Size = new System.Drawing.Size(667, 28);
            this.bottomTableLayoutPanel.TabIndex = 0;
            // 
            // okButton
            // 
            this.okButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.okButton.Location = new System.Drawing.Point(432, 1);
            this.okButton.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 26);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cancelButton.Location = new System.Drawing.Point(511, 1);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 26);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // applyButton
            // 
            this.applyButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.applyButton.Enabled = false;
            this.applyButton.Location = new System.Drawing.Point(590, 1);
            this.applyButton.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(75, 26);
            this.applyButton.TabIndex = 2;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // bottomSpacingPanel
            // 
            this.bottomSpacingPanel.Controls.Add(this.bottomTableLayoutPanel);
            this.bottomSpacingPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomSpacingPanel.Location = new System.Drawing.Point(6, 489);
            this.bottomSpacingPanel.Name = "bottomSpacingPanel";
            this.bottomSpacingPanel.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.bottomSpacingPanel.Size = new System.Drawing.Size(667, 32);
            this.bottomSpacingPanel.TabIndex = 2;
            // 
            // splitContainer
            // 
            this.splitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Location = new System.Drawing.Point(6, 6);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.pageTreeListView);
            this.splitContainer.Panel1.Controls.Add(this.searchTextBox);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.pagePanel);
            this.splitContainer.Size = new System.Drawing.Size(667, 483);
            this.splitContainer.SplitterDistance = 173;
            this.splitContainer.SplitterWidth = 2;
            this.splitContainer.TabIndex = 0;
            // 
            // pageTreeListView
            // 
            this.pageTreeListView.AllColumns.Add(this.pageNameColumn);
            this.pageTreeListView.CellEditUseWholeCell = false;
            this.pageTreeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.pageNameColumn});
            this.pageTreeListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pageTreeListView.FullRowSelect = true;
            this.pageTreeListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.pageTreeListView.HideSelection = false;
            this.pageTreeListView.Location = new System.Drawing.Point(0, 20);
            this.pageTreeListView.MultiSelect = false;
            this.pageTreeListView.Name = "pageTreeListView";
            this.pageTreeListView.ShowGroups = false;
            this.pageTreeListView.Size = new System.Drawing.Size(169, 459);
            this.pageTreeListView.TabIndex = 0;
            this.pageTreeListView.UseCompatibleStateImageBehavior = false;
            this.pageTreeListView.UseFiltering = true;
            this.pageTreeListView.View = System.Windows.Forms.View.Details;
            this.pageTreeListView.VirtualMode = true;
            this.pageTreeListView.SelectionChanged += new System.EventHandler(this.pageTreeListView_SelectionChanged);
            // 
            // pageNameColumn
            // 
            this.pageNameColumn.AspectName = "Text";
            this.pageNameColumn.FillsFreeSpace = true;
            this.pageNameColumn.Text = "Name";
            // 
            // searchTextBox
            // 
            this.searchTextBox.ClearImage = null;
            this.searchTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchTextBox.Location = new System.Drawing.Point(0, 0);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.SearchImage = null;
            this.searchTextBox.Size = new System.Drawing.Size(169, 20);
            this.searchTextBox.TabIndex = 1;
            // 
            // pagePanel
            // 
            this.pagePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pagePanel.Location = new System.Drawing.Point(0, 0);
            this.pagePanel.Name = "pagePanel";
            this.pagePanel.Padding = new System.Windows.Forms.Padding(2);
            this.pagePanel.Size = new System.Drawing.Size(488, 479);
            this.pagePanel.TabIndex = 0;
            // 
            // Preferences
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(679, 527);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.bottomSpacingPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(352, 420);
            this.Name = "Preferences";
            this.Padding = new System.Windows.Forms.Padding(6);
            this.Text = "Preferences";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Preferences_FormClosing);
            this.Load += new System.EventHandler(this.Preferences_Load);
            this.Shown += new System.EventHandler(this.Preferences_Shown);
            this.bottomTableLayoutPanel.ResumeLayout(false);
            this.bottomSpacingPanel.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pageTreeListView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel bottomTableLayoutPanel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.Panel bottomSpacingPanel;
        private System.Windows.Forms.Button resetAllButton;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Panel pagePanel;
        private BrightIdeasSoftware.TreeListView pageTreeListView;
        private BrightIdeasSoftware.OLVColumn pageNameColumn;
        private Controls.SearchTextBox searchTextBox;
    }
}