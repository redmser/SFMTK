﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace SFMTK.Forms
{
    /// <summary>
    /// A dialog for displaying help about a feature.
    /// </summary>
    public partial class HelpPageDialog : Form, IThemeable
    {
        //TODO: HelpPageDialog - allow modules to define a custom wikipage of sorts (do NOT allow direct URL input!)

        /// <summary>
        /// Initializes a new instance of the <see cref="HelpPageDialog"/> class.
        /// </summary>
        public HelpPageDialog()
        {
            //Compiler-generated
            InitializeComponent();

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        public string ActiveTheme { get; set; }

        private void HelpPageDialog_Load(object sender, EventArgs e) => this.CenterToParent();

        /// <summary>
        /// Shows a help page.
        /// </summary>
        /// <param name="subject">Subject/topic of the help page. Used for titles.</param>
        /// <param name="body">Main text body of the help page.</param>
        /// <param name="wikiPage">Name of the wiki page for this subject, if any.</param>
        /// <param name="image">The image to display, if any. Shown in full-size, be considerate about its size!</param>
        public static void ShowHelp(string subject, string body, string wikiPage = null, Image image = null)
        {
            var diag = new HelpPageDialog();
            if (subject != null)
            {
                diag.Text = subject + " Help";
                diag.titleLabel.Text = diag.Text + ":";
            }

            diag.bodyLabel.Text = body;
            diag.bodyTopLabel.Text = body;
            diag._wikiPage = wikiPage;

            diag.onlineButton.Enabled = diag._wikiPage != null;
            if (image != null)
            {
                diag.pictureBox.Image = image;
                diag.pictureBox.Size = image.Size;
                diag.tableLayoutPanel.Visible = true;
                diag.bodyLabel.Visible = false;
                diag.Size = new Size(diag.Width + image.Width, diag.Height + (image.Height / 2));
                diag.MinimumSize = new Size(image.Width + 38, image.Height + 110);
            }
            diag.Show();
        }

        private void okButton_Click(object sender, EventArgs e) => this.Close();

        /// <summary>
        /// Shows the specified help page.
        /// </summary>
        /// <param name="subject">Name of the help page to display, case-insensitive.</param>
        public static void ShowHelp(string subject)
        {
            var page = HelpPages.FirstOrDefault(p => p.Subject.Equals(subject, StringComparison.InvariantCultureIgnoreCase));
            if (page == null)
                throw new ArgumentException("The specified help page subject does not exist.", nameof(subject));

            ShowHelp(page.Subject, page.Body, page.WikiPage, page.Image == null ? null : new Bitmap(Path.Combine(IOHelper.SFMTKDirectory, Constants.HelpPagesPath, page.Image)));
        }

        private string _wikiPage;

        private void onlineButton_Click(object sender, EventArgs e) => NetworkFallback.OpenInBrowser(Constants.GitLabWikiURL + this._wikiPage);

        private static List<HelpPage> HelpPages { get; } = new List<HelpPage>();

        /// <summary>
        /// Path to a config file containing the list of help pages.
        /// </summary>
        public const string HelpPagesFile = Constants.HelpPagesPath + @"\helppages.cfg";

        /// <summary>
        /// Reloads the help page list.
        /// </summary>
        public static void ReloadHelpPages()
        {
            HelpPages.Clear();

            if (!File.Exists(HelpPagesFile))
                throw new FileNotFoundException($"Could not find {HelpPagesFile} file!", HelpPagesFile);

            var txt = File.ReadAllText(HelpPagesFile);
            var arr = JArray.Parse(txt);
            HelpPages.AddRange(arr.ToObject<IEnumerable<HelpPage>>());
        }

        /// <summary>
        /// A help page to display.
        /// </summary>
        private class HelpPage
        {
            /// <summary>
            /// Gets or sets the subject/topic of the help page. Used for titles.
            /// </summary>
            public string Subject { get; set; }

            /// <summary>
            /// Gets or sets the main text body of the help page.
            /// </summary>
            public string Body { get; set; }

            /// <summary>
            /// Gets or sets the name of the wiki page for this subject, if any.
            /// </summary>
            public string WikiPage { get; set; }

            /// <summary>
            /// Gets or sets the filename of the image to display, if any.
            /// </summary>
            public string Image { get; set; }
        }
    }
}
