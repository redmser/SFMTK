﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrightIdeasSoftware;
using SFMTK.Commands;
using SFMTK.Controls;

namespace SFMTK.Forms
{
    /// <summary>
    /// Dialog for managing the list of window layouts as well as creating new ones.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Layouts : Form, IThemeable
    {
        //UNTESTED: Layouts - check if reordering works out with both storing new order and loading corresponding files in
        //BUG: Layouts - saving a new layout, switching to it will crash due to "switch to an invalid layout"

        /// <summary>
        /// Initializes a new instance of the <see cref="Layouts"/> class.
        /// </summary>
        public Layouts()
        {
            //Compiler-generated
            InitializeComponent();

            //Set list properties
            this.layoutsObjectListView.DropSink = new RearrangingDropSinkEx(false);

            //Load layouts to list
            this.layoutsObjectListView.AddObjects(DockLayout.Layouts);

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control.
        /// </summary>
        public string ActiveTheme { get; set; }

        /// <summary>
        /// Generates a XML path for the specified layout.
        /// </summary>
        /// <param name="layout">The layout to generate the path from.</param>
        private static string GenerateLayoutXMLPath(DockLayout layout) => GenerateLayoutXMLPath(layout.Name);

        /// <summary>
        /// Generates a XML path for the specified layout name.
        /// </summary>
        /// <param name="name">The name.</param>
        private static string GenerateLayoutXMLPath(string name) => $"layout_{IOHelper.MakeValidFileName(name)}.xml";

        private void buttonSaveLayout_Click(object sender, EventArgs e)
        {
            var layout = new DockLayout($"Layout {this.layoutsObjectListView.Items.Count + 1}", null);
            DockLayout.Layouts.Add(layout);
            this.layoutsObjectListView.AddObject(layout);
            this.layoutsObjectListView.EditModel(layout);
        }

        private bool _deleteCell = false;
        private bool _cancelCell = false;

        private void OnCellEditValidating(object sender, CellEditEventArgs e)
        {
            if (e.Column == this.nameOlvColumn)
            {
                //Name validation
                NameValidation();
            }
            else if (e.Column == this.shortcutOlvColumn)
            {
                //Shortcut validation
                ShortcutValidation();
            }



            void NameValidation()
            {
                //Verify data (allow editing on cancel here)
                var newname = (string)e.NewValue;

                //If renaming to the same name, ignore
                var layout = ((DockLayout)e.RowObject);
                if (layout.XmlPath != null && ((string)e.Value).Equals(newname, StringComparison.InvariantCultureIgnoreCase))
                {
                    //Do not cancel here, but don't save either!
                    this._cancelCell = true;
                    return;
                }

                var duplicate = DockLayout.Layouts.FirstOrDefault(l => l != layout && l.Name.Equals(newname, StringComparison.InvariantCultureIgnoreCase));
                if (duplicate != null)
                {
                    //Disallow a duplicate name
                    var res = FallbackRememberDialog.ShowDialog("A window layout with this name already exists.\n\nDo you want to replace it with this layout?",
                        "Replace layout?", "Replace existing window layout", FallbackRememberDialog.RememberDecisionText, null, FallbackDialogIcon.Question,
                        new FallbackDialogButton("Replace layout", DialogResult.Yes), new FallbackDialogButton(DialogResult.Cancel));
                    if (res == DialogResult.Yes)
                    {
                        //Replace old with new layout, by saving
                        duplicate.XmlPath = GenerateLayoutXMLPath(newname);
                        duplicate.SaveLayout();

                        //Can't delete it here and now, do so once possible
                        this._deleteCell = true;
                        return;
                    }

                    //Cancel editing, to allow user to rename layout to a unique name
                    e.Cancel = true;
                    return;
                }
            }

            void ShortcutValidation()
            {
                //Check new value
                try
                {
                    //Apply value
                    var keys = CommandManager.KeysFromString(e.NewValue.ToString());

                    //Force update dupes
                    var i = CommandManager.Commands.IndexOf(((DockLayout)e.RowObject).Command);
                    CommandManager.Commands[i].Shortcut = keys;

                    //Check for duplicates, only if changing the value too
                    var dupes = CommandManager.Commands.Where(c => c.Shortcut == keys).ToList();
                    if (keys != Keys.None && dupes.Count > 1)
                    {
                        //Redraw OLV
                        this.layoutsObjectListView.RefreshObjects(dupes);

                        //Collect info
                        var scinfo = "";
                        foreach (var sc in dupes)
                        {
                            scinfo += $"\n{sc.Name.Replace("&", "")} ({sc.InternalName})";
                        }

                        if (FallbackRememberDialog.ShowDialog($"The specified shortcut is already in use:\n{scinfo}", "Duplicate shortcut key", "Duplicate shortcut key",
                            null, null, FallbackDialogIcon.Warning, new FallbackDialogButton("Continue", DialogResult.OK),
                            new FallbackDialogButton("Edit Shortcut", DialogResult.Cancel)) == DialogResult.Cancel)
                        {
                            //Allow user to edit again
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        //Refresh all, since dupes might have been removed
                        this.layoutsObjectListView.BuildList();
                    }
                }
                catch (Exception ex)
                {
                    //Invalid value!
                    e.Cancel = true;

                    if (FallbackTaskDialog.ShowDialogLog(ex.Message, "Unable to save shortcut key", "Invalid shortcut key", FallbackDialogIcon.Error,
                        new FallbackDialogButton(DialogResult.OK), new FallbackDialogButton(DialogResult.Cancel)) == DialogResult.Cancel)
                    {
                        //Undo editing if cancelled
                        this.layoutsObjectListView.CancelCellEdit();
                    }
                }
            }
        }

        private void OnCellEditFinishing(object sender, CellEditEventArgs e)
        {
            if (e.Column != this.nameOlvColumn)
                return;

            //Delete cell if needed
            if (this._deleteCell)
            {
                this._deleteCell = false;
                e.Cancel = true;
                this.layoutsObjectListView.RemoveObject(e.RowObject);
                return;
            }

            //Instantly cancel if needed
            if (this._cancelCell)
            {
                this._cancelCell = false;
                e.Cancel = true;
                return;
            }

            //Verify data (will undo changes on cancel here)
            var newname = (string)e.NewValue;
            if (string.IsNullOrWhiteSpace(newname))
            {
                e.Cancel = true;
                return;
            }

            //Set XML path
            var layout = ((DockLayout)e.RowObject);
            layout.XmlPath = GenerateLayoutXMLPath(newname);

            //Save XML file
            layout.SaveLayout();
        }

        private void layoutsObjectListView_FormatCell(object sender, BrightIdeasSoftware.FormatCellEventArgs e)
        {
            if (e.Column != this.shortcutOlvColumn)
                return;

            var keys = ((DockLayout)e.Model).Shortcut;
            if (keys != Keys.None && CommandManager.Commands.Count(c => c.Shortcut == keys) > 1)
            {
                //Is a duplicate, recolor
                e.SubItem.ForeColor = UITheme.SelectedTheme.ErrorTextColor;
                e.SubItem.BackColor = UITheme.SelectedTheme.ErrorBackColor;
            }
            else
            {
                //Default color
                e.SubItem.ForeColor = UITheme.SelectedTheme.MainTextColor;
                e.SubItem.BackColor = UITheme.SelectedTheme.MainBackColor;
            }
        }

        private void OnSelectionChanged(object sender, EventArgs e) => this.UpdateButtonStates();

        private void OnModelDropped(object sender, ModelDropEventArgs e)
        {
            //HACK: Layouts - select model object with a delay since directly doing so does nothing (is the object added after the event?)
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(50);
                this.Invoke(new Action(() => this.layoutsObjectListView.SelectedObject = e.SourceModels[0]));
            });
        }

        private void buttonDelete_Click(object sender, System.EventArgs e)
        {
            var layout = this.layoutsObjectListView.SelectedObject as DockLayout;

            if (layout == null) return;

            var result = FallbackRememberDialog.ShowDialog($"Are you sure you wish to delete the layout \"{layout.Name}\"?",
                "Delete layout", "Delete selected layout", FallbackRememberDialog.DontAskAgainText, null, FallbackDialogIcon.Question,
                new FallbackDialogButton(DialogResult.Yes), new FallbackDialogButton(DialogResult.No, false));

            if (result == DialogResult.Yes)
            {
                //Delete file
                if (File.Exists(layout.XmlPath))
                    File.Delete(layout.XmlPath);

                //Delete entry & update selection
                DockLayout.Layouts.Remove(layout);
                var index = this.layoutsObjectListView.IndexOf(layout);
                this.layoutsObjectListView.RemoveObject(layout);
                this.layoutsObjectListView.SelectedIndex = index - 1;
            }
        }

        private void buttonRename_Click(object sender, System.EventArgs e)
        {
            this.layoutsObjectListView.EditModel(this.layoutsObjectListView.SelectedObject);
        }

        private void editShortcutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.layoutsObjectListView.EditSubItem(this.layoutsObjectListView.GetItem(this.layoutsObjectListView.SelectedIndex), 1);
        }

        private void applyLayoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //UNTESTED: Layouts - apply selected layout
            ((DockLayout)this.layoutsObjectListView.SelectedObject).LoadLayout();
        }

        private void buttonMoveUp_Click(object sender, System.EventArgs e)
        {
            var obj = this.layoutsObjectListView.SelectedObject;

            if (obj == null) return;

            var index = this.layoutsObjectListView.IndexOf(obj);

            this.layoutsObjectListView.MoveObjects(index - 1, new[] { obj });
            this.layoutsObjectListView.SelectedIndex = index - 1;
        }

        private void buttonMoveDown_Click(object sender, System.EventArgs e)
        {
            var obj = this.layoutsObjectListView.SelectedObject;

            if (obj == null)
                return;

            var index = this.layoutsObjectListView.IndexOf(obj);

            this.layoutsObjectListView.MoveObjects(index + 2, new[] { obj });
            this.layoutsObjectListView.SelectedIndex = index + 1;
        }

        private void UpdateButtonStates()
        {
            var hasSelection = this.layoutsObjectListView.SelectedObject != null;

            this.buttonDelete.Enabled = hasSelection;
            this.buttonRename.Enabled = hasSelection;
            this.buttonMoveUp.Enabled = hasSelection && this.layoutsObjectListView.SelectedIndex > 0;
            this.buttonMoveDown.Enabled = hasSelection && this.layoutsObjectListView.SelectedIndex < this.layoutsObjectListView.Items.Count - 1;
        }

        private void LayoutsNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Save whole layout list
            DockLayout.Layouts.Clear();
            DockLayout.Layouts.AddRange(this.layoutsObjectListView.Objects.Cast<DockLayout>());

            Properties.Settings.Default.LayoutOrder.Clear();
            Properties.Settings.Default.LayoutOrder.AddRange(DockLayout.Layouts.Select(l => Path.GetFileName(l.XmlPath)));
        }

        private void LayoutsNew_Load(object sender, System.EventArgs e)
        {
            //Center me
            this.CenterToParent();
        }

        private void layoutContextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //HACK: Layouts - use command system for layout actions
            var anySelected = this.layoutsObjectListView.SelectedObject != null;
            this.deleteToolStripMenuItem.Enabled = anySelected;
            this.renameToolStripMenuItem.Enabled = anySelected;
            this.editShortcutToolStripMenuItem.Enabled = anySelected;
            this.moveUpToolStripMenuItem.Enabled = anySelected && this.layoutsObjectListView.SelectedIndex > 0;
            this.moveDownToolStripMenuItem.Enabled = anySelected && this.layoutsObjectListView.SelectedIndex < this.layoutsObjectListView.Objects.OfType<DockLayout>().Count() - 1;
        }
    }
}
