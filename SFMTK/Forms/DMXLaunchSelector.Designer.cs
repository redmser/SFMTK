﻿namespace SFMTK.Forms
{
    partial class DMXLaunchSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DMXLaunchSelector));
            this.titleLabel = new System.Windows.Forms.Label();
            this.sfmtkButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.sfmButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.titleLabel.Location = new System.Drawing.Point(8, 8);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(272, 85);
            this.titleLabel.TabIndex = 0;
            this.titleLabel.Text = "Open {0} with...";
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sfmtkButton
            // 
            this.sfmtkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.sfmtkButton.Image = global::SFMTK.Properties.Resources.sfmtk16;
            this.sfmtkButton.Location = new System.Drawing.Point(8, 97);
            this.sfmtkButton.Name = "sfmtkButton";
            this.sfmtkButton.Size = new System.Drawing.Size(116, 31);
            this.sfmtkButton.TabIndex = 1;
            this.sfmtkButton.Text = "SFM&TK";
            this.sfmtkButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.sfmtkButton, "SFMTK will open this DMX file.");
            this.sfmtkButton.UseVisualStyleBackColor = true;
            this.sfmtkButton.Click += new System.EventHandler(this.sfmtkButton_Click);
            // 
            // sfmButton
            // 
            this.sfmButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sfmButton.Image = global::SFMTK.Properties.Resources.sfm16;
            this.sfmButton.Location = new System.Drawing.Point(164, 97);
            this.sfmButton.Name = "sfmButton";
            this.sfmButton.Size = new System.Drawing.Size(116, 31);
            this.sfmButton.TabIndex = 2;
            this.sfmButton.Text = "&Source Filmmaker";
            this.sfmButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.sfmButton, "Source Filmmaker will open this DMX session file.");
            this.sfmButton.UseVisualStyleBackColor = true;
            this.sfmButton.Click += new System.EventHandler(this.sfmButton_Click);
            // 
            // DMXLaunchSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 135);
            this.Controls.Add(this.sfmButton);
            this.Controls.Add(this.sfmtkButton);
            this.Controls.Add(this.titleLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(266, 116);
            this.Name = "DMXLaunchSelector";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Open with...";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Button sfmtkButton;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button sfmButton;
    }
}