﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    /// <summary>
    /// Containing form for a ContentBrowser user control.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class ContentBrowserWindow : Form, IThemeable
    {
        //TODO: ContentBrowserWindow - dialog does not have focus after opening

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentBrowserWindow"/> class.
        /// </summary>
        public ContentBrowserWindow()
        {
            //Compiler-generated
            InitializeComponent();

            //Set accept/cancel buttons
            this.AcceptButton = this.contentBrowser.okButton;
            this.CancelButton = this.contentBrowser.cancelButton;

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentBrowserWindow" /> class.
        /// </summary>
        /// <param name="multiselect">If set to <c>true</c>, allow multiple files to be selected.</param>
        /// <param name="allowNew">If set to <c>true</c>, allow new files to be created by the user.</param>
        /// <param name="selected">Which file is selected by default. Should be a full absolute or relative path, standardized or not.</param>
        public ContentBrowserWindow(bool multiselect, bool allowNew, string selected = null) : this()
        {
            this.MultiSelect = multiselect;
            this.AllowNewFiles = allowNew;

            if (selected != null)
                this.contentBrowser.FullPath = selected;
        }

        /// <summary>
        /// Gets or sets whether to allow new files to be created.
        /// </summary>
        public bool AllowNewFiles
        {
            get => this.contentBrowser.AllowNewFiles;
            set => this.contentBrowser.AllowNewFiles = value;
        }

        /// <summary>
        /// Gets or sets whether to allow multiselection.
        /// </summary>
        public bool MultiSelect
        {
            get => this.contentBrowser.MultiSelect;
            set => this.contentBrowser.MultiSelect = value;
        }

        private void ContentBrowserWindow_Load(object sender, System.EventArgs e)
        {
            //Center dialog
            this.CenterToParent();
        }

        private void ContentBrowserWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Apply dialog result
            if (this.DialogResult == DialogResult.OK)
                this.SelectedFiles = this.contentBrowser.SelectedFiles;
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control. This property is never set to the same value that it already has.
        /// </summary>
        public string ActiveTheme { get; set; }

        /// <summary>
        /// Gets a list of selected files for when this dialog was confirmed.
        /// </summary>
        public IEnumerable<ContentBrowserNode> SelectedFiles { get; private set; }
    }
}
