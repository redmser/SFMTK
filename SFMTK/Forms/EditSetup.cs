﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    /// <summary>
    /// Dialog for editing a single setup.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class EditSetup : Form, IThemeable
    {
        //TODO: HelpPage SFM Themes - add a picture showing multiple custom themes
        //UNTESTED: HelpPage SFM Themes - using the console commands (sfm_reloadstylesheet) and the launch option -qtstylesheet (relative path? qss file parameter?)
        //  Also, can SFMTK really create .qss files in the end?

        //TODO: HelpPage Autocompiling - add a picture (either of graph when autocompile/compileonsave get triggered, or difference between save and compile perhaps)

        //TODO: EditSetup - options to export/import setups (or store them in the app folder as well)
        //TODO: EditSetup - only enable even if QL checkbox if the one left to it is checked! (same for forceclose checkbox)
        //FIXME: EditSetup - figure out all other undocumented launch params and see which could be a thing
        //TODO: EditSetup - figure out the maximum command line length for windows (seems to exist!)
        //  -> warn user about too many options then (try to delegate +cmds and similar to autoexec instead)
        //TODO: EditSetup - which launch-commands could be options? (+cmd ...)
        //TODO: EditSetup - hide some more obscure launch params etc in an "Advanced" category, to avoid overloaded UI
        //TODO: EditSetup - add an error marker to the setup name to make it clearer about what is and isnt allowed
        //TODO: EditSetup - further SFM theming options (sfm_bone_display commands AND editing of icons/...)
        //TODO: EditSetup - better resizing of containing controls (autoscroll, reformat with flowlayout, allow for smaller minimum size too)

        /// <summary>
        /// Initializes a new instance of the <see cref="EditSetup"/> class, editing a new setup instance.
        /// </summary>
        public EditSetup() : this(new Setup())
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditSetup"/> class, editing the specified setup.
        /// </summary>
        public EditSetup(Setup edit)
        {
            //Compiler-generated
            InitializeComponent();

            //Editing setup
            this.EditingSetup = edit;
            this.IsActive = edit.Name == Properties.Settings.Default.ActiveSetup;

            //Databindings
            this.nameTextBox.DataBindings.Add("Text", this.EditingSetup, "Name", false, DataSourceUpdateMode.OnPropertyChanged);

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        private void EditSetup_Load(object sender, EventArgs e)
        {
            this.CenterToParent();
        }

        /// <summary>
        /// Gets the setup which is currently being edited by this form.
        /// </summary>
        public Setup EditingSetup { get; }

        /// <summary>
        /// Gets or sets the theme currently active on this control.
        /// </summary>
        public string ActiveTheme { get; set; }

        /// <summary>
        /// Gets whether currently editing the active setup.
        /// </summary>
        public bool IsActive { get; }

        private void EditSetup_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                //Validate data

                //Name already exists? Count to 1 since we already have renamed ourself
                if (Properties.Settings.Default.Setups.Count(s => s.Name == this.EditingSetup.Name) != 1)
                {
                    FallbackTaskDialog.ShowDialog($"A setup with the specified name \"{this.EditingSetup.Name}\" already exists!", "Could not save setup", "Setup with same name",
                        FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));
                    e.Cancel = true;
                    this.nameTextBox.Focus();
                    this.nameTextBox.SelectAll();
                    return;
                }
                else if (string.IsNullOrWhiteSpace(this.EditingSetup.Name))
                {
                    FallbackTaskDialog.ShowDialog($"Please enter a name for your setup!", "Could not save setup", "Setup with no name",
                        FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));
                    e.Cancel = true;
                    this.nameTextBox.Focus();
                    return;
                }

                //If this is the active setup, update its name!
                if (this.IsActive)
                    Properties.Settings.Default.ActiveSetup = this.nameTextBox.Text;
            }
        }

        private void copyModsButton_Click(object sender, EventArgs e)
        {
            //NYI: EditSetup - copy current mod configuration over to setup
            throw new NotImplementedException();
        }

        private void editModsButton_Click(object sender, EventArgs e)
        {
            //NYI: EditSetup - open the mod manager widget and give it focus, to edit mod config
            //  -> if possible, only temporarily edit user's mod config (sandbox changes for setup only)
            throw new NotImplementedException();
        }
    }
}
