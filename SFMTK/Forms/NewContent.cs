﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;
using SFMTK.Contents;
using SFMTK.Controls.InstanceSettings;

namespace SFMTK.Forms
{
    /// <summary>
    /// Dialog for creating new content.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class NewContent : Form, IThemeable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NewContent"/> class.
        /// </summary>
        public NewContent()
        {
            //Compiler-generated
            InitializeComponent();

            //TODO: NewContent - allow drag&drop of files/string into here similarly to main dialog -> additional behaviour?
            //NYI: NewContent - create new content either from imports (mdl, vtf) or from scratch (scripts, qc, vmt)
            //  -> When selecting content, there should be a decision between creating a new one or importing existing files as new content
            //TODO: NewContent - add checkbox to include ALL content (not only IInstantiatable) -> uses default constructor
            //  -> use a ModelFilter to show/hide these dynamically, instead of filtering the list of objects in advance!
            //  -> maybe link to a checkbox in the properties, to allow toggling these in the File->New menu as well (probably as a new category BELOW the default list of content?)
            //TODO: NewContent - allow creating mods

            //Initialize content type list
            var dtr = new DescribedTaskRenderer();
            dtr.DescriptionAspectName = "InfoText";
            dtr.DescriptionColor = UITheme.SelectedTheme.SubTextColor;
            dtr.TitleFont = new Font(UITheme.SelectedTheme.StatusTextFont, FontStyle.Bold);
            this.typeNameColumn.AspectGetter = (o) => Content.GetTypeName((Content)o);
            this.typeNameColumn.Renderer = dtr;
            this.typesObjectListView.SetObjects(Content.ContentList.OfType<IInstantiatable>());

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        private void NewContent_Load(object sender, EventArgs e)
        {
            //Center in main window
            this.CenterToParent();

            //Select an entry in the list
            this.typesObjectListView.SelectedIndex = 0;
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control.
        /// </summary>
        public string ActiveTheme { get; set; }

        /// <summary>
        /// Gets the new content type which should be created if the OK button was pressed.
        /// </summary>
        public Type ContentType { get; private set; }

        /// <summary>
        /// Gets the loaded instance settings.
        /// </summary>
        public IEnumerable<IInstanceSetting<Content>> Settings => this.settingsFlowLayoutPanel.Controls.OfType<IInstanceSetting<Content>>();

        /// <summary>
        /// Applies all loaded instance settings to the content instance. Returns whether that was successful.
        /// </summary>
        public bool ApplySettings(Content content)
        {
            foreach (var sett in this.Settings)
            {
                var error = sett.ApplyInstanceSettings(content);
                if (error != null)
                {
                    //Show error message directly!
                    FallbackTaskDialog.ShowDialogLog(error, $"Error saving {sett.GetType().Name}:", "Error saving a property",
                        FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));
                    return false;
                }
            }
            return true;
        }

        private void typesObjectListView_SelectionChanged(object sender, EventArgs e)
        {
            //Enable OK based on selection
            this.okButton.Enabled = this.typesObjectListView.SelectedObjects.Count > 0;
            this.ContentType = this.typesObjectListView.SelectedObject?.GetType();

            //Reload instance settings
            while (this.settingsFlowLayoutPanel.Controls.Count > 1)
                this.settingsFlowLayoutPanel.Controls.RemoveAt(1);

            if (this.typesObjectListView.SelectedObject is IInstantiatable inst)
            {
                foreach (var sett in inst.CreateInstanceSettings().Where(s => s is Control))
                {
                    //Add instance setting
                    var ctrl = (Control)sett;
                    this.settingsFlowLayoutPanel.Controls.Add(ctrl);

                    //Give tooltiptext
                    this.toolTip1.SetToolTip(ctrl, sett.ToolTipText);
                }
            }
        }

        private void typesObjectListView_DoubleClick(object sender, EventArgs e)
        {
            //Always has selection for this event - same as pressing OK
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void NewContent_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                //Be sure to save selected content
                this.ContentType = this.typesObjectListView.SelectedObject.GetType();
            }
        }
    }
}
