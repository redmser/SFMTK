﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Properties;

namespace SFMTK.Forms
{
    /// <summary>
    /// Dialog which appears when first launching SFMTK, for setting up basic settings.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class FirstLaunch : Form, IThemeable
    {
        // Notes to anyone looking to extend the FirstLaunch window:
        // - This window should be kept as simple as possible, be sure not to add complex options that may confuse the first time user
        //   or make them wonder which option they should be selecting if they do not understand it.
        // - Any settings keys which use the RerouteDataSource should have their corresponding Update method called directly, since
        //   UpdateAll is not used here when pressing OK.

        //TODO: FirstLaunch - setting for changing the movie sessions folders (since they are used in multiple places)
        //  -> show at a glance the amount of folders and amount of sessions inside, allow editing using DetectedSessions perhaps
        //BUG: FirstLaunch - dialog does not show in taskbar / cant be focussed through taskbar and sometimes does not start focused
        //TODO: FirstLaunch - since from startup until displaying the dialog takes quite some time (this time won't shrink with more features being added),
        //  give user a visual feedback (like a splash screen) while loading everything... see if this is needed for the main procedure as well

        /// <summary>
        /// Initializes a new instance of the <see cref="FirstLaunch"/> class.
        /// </summary>
        public FirstLaunch()
        {
            //Compiler-generated
            InitializeComponent();

            //Set data sources
            this.themeComboBox.DataSource = UITheme.Themes;
            this.themeComboBox.DisplayMember = "Name";
            this.themeComboBox.ValueMember = "Name";
            var themebinding = new Controls.ActionBinding("SelectedItem", Settings.Default, "SelectedTheme", DataSourceUpdateMode.OnPropertyChanged,
                (s, a) => UITheme.ApplyTheme(this));
            themebinding.Format += this.ThemeBinding_Format;
            themebinding.Parse += this.ThemeBinding_Parse;
            this.themeComboBox.DataBindings.Add(themebinding);
            this.dmxAssocRadioGroupPanel.DataBindings.Add("Selected", Settings.Default, "DMXAssoc", false, DataSourceUpdateMode.OnPropertyChanged);
            this.sfmAssocRadioGroupPanel.DataBindings.Add("Selected", Settings.Default, "SFMAssoc", false, DataSourceUpdateMode.OnPropertyChanged);
            var sfmlocbinding = new Controls.PathBinding("Text", Settings.Default, "SFMPath", DataSourceUpdateMode.OnPropertyChanged);
            this.sfmLocationTextBox.DataBindings.Add(sfmlocbinding);

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        private void ThemeBinding_Parse(object sender, ConvertEventArgs e)
        {
            var theme = e.Value as UITheme;
            if (theme == null)
            {
                e.Value = null;
                return;
            }

            e.Value = theme.Name;
        }

        private void ThemeBinding_Format(object sender, ConvertEventArgs e)
        {
            var name = e.Value.ToString();
            e.Value = UITheme.Themes.FirstOrDefault(t => t.Name == name);
            if (e.Value == null)
            {
                //Select default
                e.Value = UITheme.SystemDefaultTheme;
            }

            if (UITheme.SelectedTheme.Equals((UITheme)e.Value))
                return;

            UITheme.SelectedTheme = (UITheme)e.Value;
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control.
        /// </summary>
        public string ActiveTheme { get; set; }

        /// <summary>
        /// Whether any property in this dialog was changed.
        /// </summary>
        private bool _pendingChanges = false;

        /// <summary>
        /// Whether all property-setting methods have been called.
        /// </summary>
        private bool _finishedLoading = false;

        private void FirstLaunch_Load(object sender, EventArgs e)
        {
            //Check associations for .sfm and .dmx files
            var sfmtype = IOHelper.FileTypeFromExtension(Constants.SFMTKExtension);
            if (sfmtype != null)
            {
                var assocsfm = sfmtype.ContainsIgnoreCase("SFMTK")
                    ? Properties.Settings.Default.SFMPreviousAssociation
                    : IOHelper.GetAssociatedProgram(sfmtype, true);
                if (!string.IsNullOrWhiteSpace(assocsfm))
                {
                    //Update name and selection
                    var exe = GetShortExe(assocsfm);
                    this.currentSfmFileRadioButton.Text = $"Current association ({exe})";
                    this.currentSfmFileRadioButton.Checked = true;
                }
            }

            var dmxtype = IOHelper.FileTypeFromExtension(Constants.DMXExtension);
            if (dmxtype != null)
            {
                var assocdmx = dmxtype.ContainsIgnoreCase("SFMTK")
                    ? Properties.Settings.Default.DMXPreviousAssociation
                    : IOHelper.GetAssociatedProgram(dmxtype, true);
                if (!string.IsNullOrWhiteSpace(assocdmx))
                {
                    //Update name and selection
                    var exe = GetShortExe(assocdmx);
                    this.currentDmxFileRadioButton.Text = $"Current association ({exe})";
                    this.currentDmxFileRadioButton.Checked = true;
                }
            }

            //Detect SFM install
            this.DetectSFMDir(false);

            //Finished
            this._finishedLoading = true;



            string GetShortExe(string path)
            {
                return Path.GetFileName(path.Replace("\"", string.Empty).Replace("%1", string.Empty));
            }
        }

        private void themeComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //Text has to be stored since it gets reset when messing with settings
            var text = this.sfmLocationTextBox.Text;

            //Apply the theme
            this.themeComboBox.DataBindings["SelectedItem"].WriteValue();

            this._pendingChanges = true;
        }

        private void FirstLaunch_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                //Check whether needed values are filled in
                this.UpdateErrorMarker();
                if (this.sfmLocationErrorMarker.State == SFMTK.Controls.ErrorState.Error)
                {
                    //ERROR - notify user
                    var res = FallbackTaskDialog.ShowDialog("Without knowing where Source Filmmaker is located, SFMTK will have limited functionality." +
                        "\n\nAre you sure you want to continue?\n(You can also change this path in the preferences later on.)",
                        "Source Filmmaker directory not specified or invalid", "Invalid path to SFM installation", FallbackDialogIcon.Warning,
                        new FallbackDialogButton("Continue", DialogResult.OK), new FallbackDialogButton("Go back", DialogResult.Cancel));
                    if (res == DialogResult.Cancel)
                    {
                        //Cancel and select path
                        e.Cancel = true;
                        this.sfmLocationTextBox.Focus();
                        this.sfmLocationTextBox.SelectAll();
                        return;
                    }
                }

                //Apply any settings that need to be
                Settings.Default.UpdateFileAssociations();
                //UNTESTED: FirstLaunch - is updating the databinding manually needed?
                //this.sfmLocationTextBox.DataBindings["Text"].WriteValue();
                SFM.UpdateBasePath();
            }
            else
            {
                if (this._pendingChanges)
                {
                    //Ask if wanted to close SFMTK only when any properties were changed
                    if (FallbackTaskDialog.ShowDialog("Are you sure you wish to exit SFMTK?\nYou will lose any changes made to the settings.",
                        "Exit SFMTK?", "Exit SFMTK", FallbackDialogIcon.Info,
                        new FallbackDialogButton(DialogResult.Yes), new FallbackDialogButton(DialogResult.No)) == DialogResult.Yes)
                    {
                        //Allow to close
                    }
                    else
                    {
                        //Do not close!
                        e.Cancel = true;
                    }
                }
            }
        }

        private void detectSFMButton_Click(object sender, EventArgs e) => this.DetectSFMDir(true);

        private void browseSFMButton_Click(object sender, EventArgs e) => this.BrowseSFMDir();

        /// <summary>
        /// Detects the Source Filmmaker installation of this PC, saving it to the text box.
        /// </summary>
        /// <param name="notifyUser">If set to <c>true</c>, notify user when unable to find installation.</param>
        private void DetectSFMDir(bool notifyUser)
        {
            try
            {
                //Detect dir
                var dir = SFM.DetectSFMDirectory();
                this.sfmLocationTextBox.Text = dir;
            }
            catch
            {
                //Couldn't find SFM install
                if (notifyUser)
                {
                    //Let user know (if button was pressed only)
                    var res = FallbackTaskDialog.ShowDialog("SFMTK could not find an installation of Source Filmmaker on your computer. Would you like to search for it manually?",
                        "Unable to find Source Filmmaker", "Unable to find Source Filmmaker", FallbackDialogIcon.Warning,
                        new FallbackDialogButton(DialogResult.Yes), new FallbackDialogButton(DialogResult.No));

                    if (res == DialogResult.Yes)
                    {
                        //Browse for it now
                        this.BrowseSFMDir();
                    }
                }

                //Update error marker since text was left unchanged
                this.UpdateErrorMarker();
            }
        }

        /// <summary>
        /// Opens a file browser for selection the Source Filmmaker installation, saving it to the text box.
        /// </summary>
        private void BrowseSFMDir()
        {
            string dir;
            try
            {
                //Detect maybe
                dir = SFM.DetectSFMDirectory() + "game/";
            }
            catch
            {
                //Could not detect
                dir = null;
            }

            var ofd = new Ookii.Dialogs.Wpf.VistaOpenFileDialog();
            ofd.AddExtension = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            ofd.DefaultExt = "sfm.exe";
            ofd.Filter = "sfm.exe|sfm.exe";
            ofd.InitialDirectory = IOHelper.GetWindowsPath(dir ?? DetectAndAppendSteamDir() ?? string.Empty);
            ofd.RestoreDirectory = true;
            ofd.ShowReadOnly = false;
            var res = ofd.ShowDialog() ?? false;
            if (res)
            {
                //Update directory (get parent, since dir of sfm.exe is always /game/ )
                this.sfmLocationTextBox.Text = IOHelper.GetStandardizedPath(new FileInfo(ofd.FileName).Directory.Parent.FullName);
            }



            string DetectAndAppendSteamDir()
            {
                var str = SFM.DetectSteamDirectory();
                if (str == null)
                    return null;
                return Path.Combine(str, "SteamApps", "common");
            }
        }

        private void sfmLocationTextBox_TextChanged(object sender, EventArgs e)
        {
            //Update errors
            this.UpdateErrorMarker();

            //Save in settings, else it'd reset the text on invalidation
            this.sfmLocationTextBox.DataBindings["Text"].WriteValue();

            //Update pending changes (if user-modified)
            if (this._finishedLoading)
                this._pendingChanges = true;
        }

        private void radioGroupPanels_SelectedChanged(object sender, EventArgs e)
        {
            //Update pending changes (if user-modified)
            if (this._finishedLoading)
                this._pendingChanges = true;
        }

        private void UpdateErrorMarker()
        {
            //Update error marker + tooltip
            var loc = IOHelper.GetStandardizedPath(this.sfmLocationTextBox.Text);
            if (string.IsNullOrEmpty(loc))
            {
                //No text
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.sfmLocationErrorMarker.Text = "No location to your installation of Source Filmmaker has been specified.";
            }
            else if (loc.EndsWith("/sfm.exe", StringComparison.InvariantCultureIgnoreCase))
            {
                //Must be path to sfm install dir
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.sfmLocationErrorMarker.Text = "The specified path should point to your installation directory of Source Filmmaker, excluding the executable name.";
            }
            else if (loc.EndsWith("/game/", StringComparison.InvariantCultureIgnoreCase))
            {
                //Must be path to sfm install dir
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.sfmLocationErrorMarker.Text = "The specified path should point to your installation directory of Source Filmmaker, excluding the game directory.";
            }
            else if (!Directory.Exists(loc))
            {
                //Needs to exist
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.sfmLocationErrorMarker.Text = "The specified path is not a valid installation of Source Filmmaker (directory does not exist).";
            }
            else if (!File.Exists(Path.Combine(loc, "game", "sfm.exe")))
            {
                //Needs sfm.exe
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Error;
                this.sfmLocationErrorMarker.Text = "The specified path is not a valid installation of Source Filmmaker (could not find sfm.exe).";
            }
            else if (!loc.EndsWith("/SourceFilmmaker/", StringComparison.InvariantCultureIgnoreCase))
            {
                //Not a regular SFM folder
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Warning;
                this.sfmLocationErrorMarker.Text = "While the specified installation of Source Filmmaker seems valid, it is not findable by Steam directly.";
            }
            else
            {
                //Woo, valid!
                this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Valid;
                this.sfmLocationErrorMarker.Text = "The specified path is a valid installation of Source Filmmaker.";
            }
        }

        /// <summary>
        /// Called when the main form is about to be opened for the first time. Returns whether the setup was finished.
        /// </summary>
        public static bool OnFirstStartup()
        {
            //Show first launch dialog
            Cursor.Current = Cursors.Default;
            var fl = new FirstLaunch();
            var res = fl.ShowDialog();
            Cursor.Current = Cursors.AppStarting;

            if (res != DialogResult.OK)
            {
                //User did not want to run SFMTK now
                return false;
            }

            //Save settings to ensure that any subsequent reload will perform accordingly
            Settings.Default.FirstStartup = false;
            Settings.Default.Save();
            FirstStartup?.Invoke(fl, EventArgs.Empty);
            return true;
        }

        /// <summary>
        /// Occurs when a first startup dialog is closing and SFMTK is about to launch. The MainForm does not exist immediately after this event is invoked.
        /// </summary>
        public static event EventHandler FirstStartup;
    }
}
