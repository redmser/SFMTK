﻿using System;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    public partial class DebugUI : Form
    {
        public DebugUI()
        {
            InitializeComponent();

            this._dynamictsmitest = new Controls.TestDynamicTSMI();
            this.dynamicTSMIToolStripMenuItem.DropDownItems.Insert(1, this._dynamictsmitest);
        }

        private readonly Controls.DynamicToolStripItem _dynamictsmitest;

        private void notificationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Create test notification
            Program.MainForm.AddNotification(new Notifications.TestNotification());
        }

        private void autoreloadItemsToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this._dynamictsmitest.AutoReloadDynamicItems = this.autoreloadItemsToolStripMenuItem.Checked;
        }
    }
}
