﻿using System;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    /// <summary>
    /// A dialog for showing progress of an action, either modal or non-modal.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class ProgressWindow : Form
    {
        //TODO: ProgressWindow - dialog should be fully-modal when using ShowDialog (if clicking main window, show this window instead)
        //  -> when minimizing window (enable titlebar button for this), hide in status bar instead

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressWindow"/> class.
        /// </summary>
        public ProgressWindow() : this("", "", true, false)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressWindow" /> class, determining whether definite progress can be shown.
        /// </summary>
        /// <param name="title">The title of the form.</param>
        /// <param name="text">The main info text of the form.</param>
        /// <param name="marquee">If set to <c>true</c>, use marquee progress bar.</param>
        /// <param name="showpercent">If set to <c>true</c>, shows the percentage above the progress bar.</param>
        public ProgressWindow(string title, string text, bool marquee, bool showpercent)
        {
            //Compiler-generated
            InitializeComponent();

            if (title != null)
                this.Text = title;
            this.MainLabel.Text = text;
            this.ShowPercentage = showpercent;
            this.ProgressBar.Style = marquee ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks;

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        /// <summary>
        /// Gets the progress bar.
        /// </summary>
        public ProgressBar Progress => this.ProgressBar;

        public ProgressBarState State
        {
            get => this._state;
            set
            {
                if (this._state == value)
                    return;

                this._state = value;
                this.ProgressBar.SetState(value);
            }
        }
        private ProgressBarState _state;

        /// <summary>
        /// Gets or sets the percentage to show on the progress bar as a value between 0 and Maximum.
        /// </summary>
        public int Value
        {
            get => this.ProgressBar.Value;
            set
            {
                this.ProgressBar.Value = value;
                this.UpdateLabel();
            }
        }

        /// <summary>
        /// Gets or sets the maximum value of the progress bar.
        /// </summary>
        public int Maximum
        {
            get => this.ProgressBar.Maximum;
            set
            {
                this.ProgressBar.Maximum = value;
                this.UpdateLabel();
            }
        }

        /// <summary>
        /// Gets or sets whether the user can cancel this progress dialog. Mostly used for modal actions, as the dialog cannot be minimized with this set to <c>true</c>.
        /// </summary>
        public bool CanCancel
        {
            get => this.cancelButton.Enabled;
            set
            {
                this.cancelButton.Enabled = value;
                this.ControlBox = value;
            }
        }

        private void UpdateLabel()
        {
            var perc = Math.Round(((double)this.ProgressBar.Value / this.ProgressBar.Maximum) * 100);
            this.progressLabel.Text = perc.ToString("N1") + "%";
        }

        /// <summary>
        /// Gets or sets whether to show a waiting cursor for the lifetime of this dialog.
        /// </summary>
        public bool WaitingCursor
        {
            get => this._waitingCursor;
            set
            {
                this._waitingCursor = value;

                if (value)
                    Cursor.Current = Cursors.AppStarting;
                else
                    Cursor.Current = Cursors.Default;
            }
        }
        private bool _waitingCursor = true;

        /// <summary>
        /// Gets or sets whether to show the percentage above the progress bar.
        /// </summary>
        public bool ShowPercentage
        {
            get => this.progressLabel.Visible;
            set => this.progressLabel.Visible = value;
        }

        /// <summary>
        /// Gets or sets whether to close the window directly on cancel. Otherwise, must be manually closed using the CancelClick event.
        /// </summary>
        public bool CloseOnCancel { get; set; } = true;

        /// <summary>
        /// Gets whether this progress dialog was cancelled.
        /// </summary>
        public bool Cancelled { get; protected set; }

        /// <summary>
        /// Occurs when cancel was clicked.
        /// </summary>
        public event EventHandler CancelClick;

        /// <summary>
        /// Raises the <see cref="E:CancelClick" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnCancelClick(EventArgs e)
        {
            this.cancelButton.Enabled = false;
            this.CancelClick?.Invoke(this, e);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (this.CloseOnCancel)
                this.Close();
            else
                this.OnCancelClick(EventArgs.Empty);
        }

        private void ProgressWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.ForceClosing && e.CloseReason == CloseReason.UserClosing && !this.CloseOnCancel)
            {
                //Should not close
                this.OnCancelClick(EventArgs.Empty);
                e.Cancel = true;
                this.Cancelled = true;
            }
            else
            {
                //Actually close
                this.WaitingCursor = false;
            }
        }

        public void ForceClose()
        {
            this.ForceClosing = true;
            this.Close();
        }

        protected bool ForceClosing;

        private void ProgressWindow_Load(object sender, EventArgs e)
        {
            this.CenterToParent();
        }
    }
}
