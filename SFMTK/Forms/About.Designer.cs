﻿namespace SFMTK.Forms
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.sfmtkLabelNoTheme = new System.Windows.Forms.Label();
            this.sfmtkFullLabel = new System.Windows.Forms.Label();
            this.versionLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.thanksTextBox = new System.Windows.Forms.TextBox();
            this.sfmtkLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.licenseLabel = new System.Windows.Forms.LinkLabel();
            this.gitlabButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.donateButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.modulesListView = new System.Windows.Forms.ListView();
            this.nameColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sfmtkLogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // sfmtkLabelNoTheme
            // 
            this.sfmtkLabelNoTheme.AutoSize = true;
            this.sfmtkLabelNoTheme.Font = new System.Drawing.Font("Trebuchet MS", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sfmtkLabelNoTheme.Location = new System.Drawing.Point(104, 4);
            this.sfmtkLabelNoTheme.Name = "sfmtkLabelNoTheme";
            this.sfmtkLabelNoTheme.Size = new System.Drawing.Size(155, 54);
            this.sfmtkLabelNoTheme.TabIndex = 1;
            this.sfmtkLabelNoTheme.Text = "SFMTK";
            // 
            // sfmtkFullLabel
            // 
            this.sfmtkFullLabel.AutoSize = true;
            this.sfmtkFullLabel.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sfmtkFullLabel.Location = new System.Drawing.Point(110, 50);
            this.sfmtkFullLabel.Name = "sfmtkFullLabel";
            this.sfmtkFullLabel.Size = new System.Drawing.Size(228, 24);
            this.sfmtkFullLabel.TabIndex = 2;
            this.sfmtkFullLabel.Text = "Source Filmmaker Toolkit";
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.Location = new System.Drawing.Point(112, 78);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(100, 18);
            this.versionLabel.TabIndex = 3;
            this.versionLabel.Text = "Version Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(270, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "A tool created by RedMser, for the community.";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.thanksTextBox);
            this.groupBox1.Location = new System.Drawing.Point(8, 128);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(630, 296);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Special Thanks and Licenses";
            // 
            // thanksTextBox
            // 
            this.thanksTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.thanksTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thanksTextBox.HideSelection = false;
            this.thanksTextBox.Location = new System.Drawing.Point(3, 16);
            this.thanksTextBox.Multiline = true;
            this.thanksTextBox.Name = "thanksTextBox";
            this.thanksTextBox.ReadOnly = true;
            this.thanksTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.thanksTextBox.Size = new System.Drawing.Size(624, 277);
            this.thanksTextBox.TabIndex = 0;
            // 
            // sfmtkLogoPictureBox
            // 
            this.sfmtkLogoPictureBox.Location = new System.Drawing.Point(8, 4);
            this.sfmtkLogoPictureBox.Name = "sfmtkLogoPictureBox";
            this.sfmtkLogoPictureBox.Size = new System.Drawing.Size(96, 96);
            this.sfmtkLogoPictureBox.TabIndex = 0;
            this.sfmtkLogoPictureBox.TabStop = false;
            // 
            // licenseLabel
            // 
            this.licenseLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.licenseLabel.LinkArea = new System.Windows.Forms.LinkArea(91, 10);
            this.licenseLabel.Location = new System.Drawing.Point(0, 432);
            this.licenseLabel.Name = "licenseLabel";
            this.licenseLabel.Size = new System.Drawing.Size(476, 28);
            this.licenseLabel.TabIndex = 6;
            this.licenseLabel.TabStop = true;
            this.licenseLabel.Text = "This software is licensed under the GNU General Public License v3.0\r\nFor more inf" +
    "ormation, click here.";
            this.licenseLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.licenseLabel.UseCompatibleTextRendering = true;
            this.licenseLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.licenseLabel_LinkClicked);
            // 
            // gitlabButton
            // 
            this.gitlabButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gitlabButton.Image = global::SFMTK.Properties.Resources.gitlab16;
            this.gitlabButton.Location = new System.Drawing.Point(556, 431);
            this.gitlabButton.Name = "gitlabButton";
            this.gitlabButton.Size = new System.Drawing.Size(79, 28);
            this.gitlabButton.TabIndex = 7;
            this.gitlabButton.Text = "&GitLab";
            this.gitlabButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.gitlabButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.gitlabButton, "Check out the GitLab page if you want to help development or suggest things!");
            this.gitlabButton.UseVisualStyleBackColor = true;
            this.gitlabButton.Click += new System.EventHandler(this.gitlabButton_Click);
            // 
            // donateButton
            // 
            this.donateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.donateButton.Image = global::SFMTK.Properties.Resources.heart;
            this.donateButton.Location = new System.Drawing.Point(472, 431);
            this.donateButton.Name = "donateButton";
            this.donateButton.Size = new System.Drawing.Size(79, 28);
            this.donateButton.TabIndex = 8;
            this.donateButton.Text = "&Donate!";
            this.donateButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.donateButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.donateButton, "If you like this program, a donation would be incredibly appreciated!");
            this.donateButton.UseVisualStyleBackColor = true;
            this.donateButton.Click += new System.EventHandler(this.donateButton_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(440, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Loaded modules:";
            // 
            // modulesListView
            // 
            this.modulesListView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.modulesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameColumnHeader});
            this.modulesListView.FullRowSelect = true;
            this.modulesListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.modulesListView.HideSelection = false;
            this.modulesListView.Location = new System.Drawing.Point(440, 28);
            this.modulesListView.MultiSelect = false;
            this.modulesListView.Name = "modulesListView";
            this.modulesListView.Size = new System.Drawing.Size(200, 100);
            this.modulesListView.TabIndex = 10;
            this.modulesListView.UseCompatibleStateImageBehavior = false;
            this.modulesListView.View = System.Windows.Forms.View.Details;
            this.modulesListView.DoubleClick += new System.EventHandler(this.modulesListView_DoubleClick);
            // 
            // nameColumnHeader
            // 
            this.nameColumnHeader.Text = "Name";
            this.nameColumnHeader.Width = 192;
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 465);
            this.Controls.Add(this.modulesListView);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.donateButton);
            this.Controls.Add(this.gitlabButton);
            this.Controls.Add(this.licenseLabel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.versionLabel);
            this.Controls.Add(this.sfmtkFullLabel);
            this.Controls.Add(this.sfmtkLabelNoTheme);
            this.Controls.Add(this.sfmtkLogoPictureBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(556, 367);
            this.Name = "About";
            this.ShowInTaskbar = false;
            this.Text = "About";
            this.Load += new System.EventHandler(this.About_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sfmtkLogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox sfmtkLogoPictureBox;
        private System.Windows.Forms.Label sfmtkLabelNoTheme;
        private System.Windows.Forms.Label sfmtkFullLabel;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox thanksTextBox;
        private System.Windows.Forms.LinkLabel licenseLabel;
        private System.Windows.Forms.Button gitlabButton;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button donateButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView modulesListView;
        private System.Windows.Forms.ColumnHeader nameColumnHeader;
    }
}