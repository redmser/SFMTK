﻿namespace SFMTK.Forms
{
    partial class FirstLaunch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FirstLaunch));
            this.introLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.outroLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.dmxInfoLabel = new System.Windows.Forms.Label();
            this.sfmAssocRadioGroupPanel = new SFMTK.Controls.RadioGroupPanel();
            this.currentSfmFileRadioButton = new System.Windows.Forms.RadioButton();
            this.sfmtkSfmFileRadioButton = new System.Windows.Forms.RadioButton();
            this.dmxAssocRadioGroupPanel = new SFMTK.Controls.RadioGroupPanel();
            this.currentDmxFileRadioButton = new System.Windows.Forms.RadioButton();
            this.askDmxFileRadioButton = new System.Windows.Forms.RadioButton();
            this.sfmDmxFileRadioButton = new System.Windows.Forms.RadioButton();
            this.sfmtkDmxFileRadioButton = new System.Windows.Forms.RadioButton();
            this.dmxFileLabel = new System.Windows.Forms.Label();
            this.sfmFileLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.invisibleControl1 = new SFMTK.Controls.InvisibleControl();
            this.panel3 = new System.Windows.Forms.Panel();
            this.sfmLocationErrorMarker = new SFMTK.Controls.ErrorMarker();
            this.sfmdirLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.detectSFMButton = new System.Windows.Forms.Button();
            this.browseSFMButton = new System.Windows.Forms.Button();
            this.sfmLocationTextBox = new System.Windows.Forms.TextBox();
            this.sfmdirInfoLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.themeComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.sfmAssocRadioGroupPanel.SuspendLayout();
            this.dmxAssocRadioGroupPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // introLabel
            // 
            this.introLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.introLabel.Location = new System.Drawing.Point(0, 0);
            this.introLabel.Name = "introLabel";
            this.introLabel.Size = new System.Drawing.Size(689, 40);
            this.introLabel.TabIndex = 0;
            this.introLabel.Text = "Thank you for trying out SFMTK!\r\nYou are free to configure some basic application" +
    " settings before starting off:";
            this.introLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(611, 4);
            this.okButton.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 24);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // outroLabel
            // 
            this.outroLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.outroLabel.Location = new System.Drawing.Point(0, 343);
            this.outroLabel.Name = "outroLabel";
            this.outroLabel.Size = new System.Drawing.Size(689, 24);
            this.outroLabel.TabIndex = 2;
            this.outroLabel.Text = "You can change these settings at any time under Edit -> Preferences";
            this.outroLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.okButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 367);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(689, 32);
            this.panel1.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel7);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dmxInfoLabel);
            this.groupBox1.Controls.Add(this.sfmAssocRadioGroupPanel);
            this.groupBox1.Controls.Add(this.dmxAssocRadioGroupPanel);
            this.groupBox1.Controls.Add(this.dmxFileLabel);
            this.groupBox1.Controls.Add(this.sfmFileLabel);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(347, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(339, 297);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "File Associations (which program is used to open a file)";
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Location = new System.Drawing.Point(7, 128);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(325, 4);
            this.panel7.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(8, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(326, 32);
            this.label1.TabIndex = 2;
            this.label1.Text = "SFM files are used by SFMTK to transfer or store content or mods, similarly to ZI" +
    "P archives.";
            // 
            // dmxInfoLabel
            // 
            this.dmxInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dmxInfoLabel.Location = new System.Drawing.Point(8, 244);
            this.dmxInfoLabel.Name = "dmxInfoLabel";
            this.dmxInfoLabel.Size = new System.Drawing.Size(326, 44);
            this.dmxInfoLabel.TabIndex = 5;
            this.dmxInfoLabel.Text = "DMX files are used to store binary data in Source. SFM can open them directly as " +
    "movie sessions, while SFMTK can inspect their contents.";
            // 
            // sfmAssocRadioGroupPanel
            // 
            this.sfmAssocRadioGroupPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sfmAssocRadioGroupPanel.Controls.Add(this.currentSfmFileRadioButton);
            this.sfmAssocRadioGroupPanel.Controls.Add(this.sfmtkSfmFileRadioButton);
            this.sfmAssocRadioGroupPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.sfmAssocRadioGroupPanel.Location = new System.Drawing.Point(16, 36);
            this.sfmAssocRadioGroupPanel.Name = "sfmAssocRadioGroupPanel";
            this.sfmAssocRadioGroupPanel.Selected = 0;
            this.sfmAssocRadioGroupPanel.Size = new System.Drawing.Size(318, 48);
            this.sfmAssocRadioGroupPanel.TabIndex = 1;
            this.sfmAssocRadioGroupPanel.SelectedChanged += new System.EventHandler(this.radioGroupPanels_SelectedChanged);
            // 
            // currentSfmFileRadioButton
            // 
            this.currentSfmFileRadioButton.AutoSize = true;
            this.currentSfmFileRadioButton.Checked = true;
            this.currentSfmFileRadioButton.Location = new System.Drawing.Point(2, 2);
            this.currentSfmFileRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.currentSfmFileRadioButton.Name = "currentSfmFileRadioButton";
            this.currentSfmFileRadioButton.Size = new System.Drawing.Size(148, 17);
            this.currentSfmFileRadioButton.TabIndex = 0;
            this.currentSfmFileRadioButton.TabStop = true;
            this.currentSfmFileRadioButton.Tag = "0";
            this.currentSfmFileRadioButton.Text = "&Current association (none)";
            this.toolTip1.SetToolTip(this.currentSfmFileRadioButton, "The program that opens .sfm files will not be changed.");
            this.currentSfmFileRadioButton.UseVisualStyleBackColor = true;
            // 
            // sfmtkSfmFileRadioButton
            // 
            this.sfmtkSfmFileRadioButton.AutoSize = true;
            this.sfmtkSfmFileRadioButton.Location = new System.Drawing.Point(2, 23);
            this.sfmtkSfmFileRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.sfmtkSfmFileRadioButton.Name = "sfmtkSfmFileRadioButton";
            this.sfmtkSfmFileRadioButton.Size = new System.Drawing.Size(61, 17);
            this.sfmtkSfmFileRadioButton.TabIndex = 1;
            this.sfmtkSfmFileRadioButton.Tag = "1";
            this.sfmtkSfmFileRadioButton.Text = "&SFMTK";
            this.toolTip1.SetToolTip(this.sfmtkSfmFileRadioButton, "SFMTK will be used to open .sfm files, allowing quick extraction or modification " +
        "of the content inside.");
            this.sfmtkSfmFileRadioButton.UseVisualStyleBackColor = true;
            // 
            // dmxAssocRadioGroupPanel
            // 
            this.dmxAssocRadioGroupPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dmxAssocRadioGroupPanel.Controls.Add(this.currentDmxFileRadioButton);
            this.dmxAssocRadioGroupPanel.Controls.Add(this.askDmxFileRadioButton);
            this.dmxAssocRadioGroupPanel.Controls.Add(this.sfmDmxFileRadioButton);
            this.dmxAssocRadioGroupPanel.Controls.Add(this.sfmtkDmxFileRadioButton);
            this.dmxAssocRadioGroupPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.dmxAssocRadioGroupPanel.Location = new System.Drawing.Point(16, 156);
            this.dmxAssocRadioGroupPanel.Name = "dmxAssocRadioGroupPanel";
            this.dmxAssocRadioGroupPanel.Selected = 0;
            this.dmxAssocRadioGroupPanel.Size = new System.Drawing.Size(318, 84);
            this.dmxAssocRadioGroupPanel.TabIndex = 4;
            this.dmxAssocRadioGroupPanel.SelectedChanged += new System.EventHandler(this.radioGroupPanels_SelectedChanged);
            // 
            // currentDmxFileRadioButton
            // 
            this.currentDmxFileRadioButton.AutoSize = true;
            this.currentDmxFileRadioButton.Checked = true;
            this.currentDmxFileRadioButton.Location = new System.Drawing.Point(2, 2);
            this.currentDmxFileRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.currentDmxFileRadioButton.Name = "currentDmxFileRadioButton";
            this.currentDmxFileRadioButton.Size = new System.Drawing.Size(148, 17);
            this.currentDmxFileRadioButton.TabIndex = 0;
            this.currentDmxFileRadioButton.TabStop = true;
            this.currentDmxFileRadioButton.Tag = "0";
            this.currentDmxFileRadioButton.Text = "&Current association (none)";
            this.toolTip1.SetToolTip(this.currentDmxFileRadioButton, "The program that opens .dmx files will not be changed.");
            this.currentDmxFileRadioButton.UseVisualStyleBackColor = true;
            // 
            // askDmxFileRadioButton
            // 
            this.askDmxFileRadioButton.AutoSize = true;
            this.askDmxFileRadioButton.Location = new System.Drawing.Point(2, 23);
            this.askDmxFileRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.askDmxFileRadioButton.Name = "askDmxFileRadioButton";
            this.askDmxFileRadioButton.Size = new System.Drawing.Size(111, 17);
            this.askDmxFileRadioButton.TabIndex = 1;
            this.askDmxFileRadioButton.Tag = "3";
            this.askDmxFileRadioButton.Text = "&Ask when opened";
            this.toolTip1.SetToolTip(this.askDmxFileRadioButton, "Asks every time a DMX file is opened. Allows you to select SFM\'s setup in this di" +
        "alog as well.");
            this.askDmxFileRadioButton.UseVisualStyleBackColor = true;
            // 
            // sfmDmxFileRadioButton
            // 
            this.sfmDmxFileRadioButton.AutoSize = true;
            this.sfmDmxFileRadioButton.Location = new System.Drawing.Point(2, 44);
            this.sfmDmxFileRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.sfmDmxFileRadioButton.Name = "sfmDmxFileRadioButton";
            this.sfmDmxFileRadioButton.Size = new System.Drawing.Size(109, 17);
            this.sfmDmxFileRadioButton.TabIndex = 2;
            this.sfmDmxFileRadioButton.Tag = "1";
            this.sfmDmxFileRadioButton.Text = "Source &Filmmaker";
            this.toolTip1.SetToolTip(this.sfmDmxFileRadioButton, "Source Filmmaker will be used to open .dmx files, allowing you to quickly open mo" +
        "vie sessions.\r\nThis does not work for all types of DMX files!");
            this.sfmDmxFileRadioButton.UseVisualStyleBackColor = true;
            // 
            // sfmtkDmxFileRadioButton
            // 
            this.sfmtkDmxFileRadioButton.AutoSize = true;
            this.sfmtkDmxFileRadioButton.Location = new System.Drawing.Point(2, 65);
            this.sfmtkDmxFileRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.sfmtkDmxFileRadioButton.Name = "sfmtkDmxFileRadioButton";
            this.sfmtkDmxFileRadioButton.Size = new System.Drawing.Size(61, 17);
            this.sfmtkDmxFileRadioButton.TabIndex = 3;
            this.sfmtkDmxFileRadioButton.Tag = "2";
            this.sfmtkDmxFileRadioButton.Text = "&SFMTK";
            this.toolTip1.SetToolTip(this.sfmtkDmxFileRadioButton, "SFMTK will be used to open .dmx files, allowing for quick analysis of their conte" +
        "nt.");
            this.sfmtkDmxFileRadioButton.UseVisualStyleBackColor = true;
            // 
            // dmxFileLabel
            // 
            this.dmxFileLabel.AutoSize = true;
            this.dmxFileLabel.Location = new System.Drawing.Point(8, 136);
            this.dmxFileLabel.Name = "dmxFileLabel";
            this.dmxFileLabel.Size = new System.Drawing.Size(130, 13);
            this.dmxFileLabel.TabIndex = 3;
            this.dmxFileLabel.Text = "Associate .&dmx files with...";
            // 
            // sfmFileLabel
            // 
            this.sfmFileLabel.AutoSize = true;
            this.sfmFileLabel.Location = new System.Drawing.Point(8, 20);
            this.sfmFileLabel.Name = "sfmFileLabel";
            this.sfmFileLabel.Size = new System.Drawing.Size(127, 13);
            this.sfmFileLabel.TabIndex = 0;
            this.sfmFileLabel.Text = "Associate .&sfm files with...";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 40);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 303F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(689, 303);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.flowLayoutPanel3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(338, 297);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "General Settings";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.invisibleControl1);
            this.flowLayoutPanel3.Controls.Add(this.panel3);
            this.flowLayoutPanel3.Controls.Add(this.tableLayoutPanel2);
            this.flowLayoutPanel3.Controls.Add(this.sfmdirInfoLabel);
            this.flowLayoutPanel3.Controls.Add(this.panel2);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(332, 278);
            this.flowLayoutPanel3.TabIndex = 0;
            // 
            // invisibleControl1
            // 
            this.invisibleControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.invisibleControl1.Location = new System.Drawing.Point(0, 0);
            this.invisibleControl1.Name = "invisibleControl1";
            this.invisibleControl1.Size = new System.Drawing.Size(332, 0);
            this.invisibleControl1.TabIndex = 2;
            this.invisibleControl1.TabStop = false;
            this.invisibleControl1.Text = "invisibleControl1";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.sfmLocationErrorMarker);
            this.panel3.Controls.Add(this.sfmdirLabel);
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(153, 18);
            this.panel3.TabIndex = 11;
            // 
            // sfmLocationErrorMarker
            // 
            this.sfmLocationErrorMarker.BackColor = System.Drawing.Color.Transparent;
            this.sfmLocationErrorMarker.Location = new System.Drawing.Point(0, 0);
            this.sfmLocationErrorMarker.Name = "sfmLocationErrorMarker";
            this.sfmLocationErrorMarker.Size = new System.Drawing.Size(16, 16);
            this.sfmLocationErrorMarker.State = SFMTK.Controls.ErrorState.Valid;
            this.sfmLocationErrorMarker.TabIndex = 1;
            // 
            // sfmdirLabel
            // 
            this.sfmdirLabel.AutoSize = true;
            this.sfmdirLabel.Location = new System.Drawing.Point(16, 2);
            this.sfmdirLabel.Name = "sfmdirLabel";
            this.sfmdirLabel.Size = new System.Drawing.Size(137, 13);
            this.sfmdirLabel.TabIndex = 0;
            this.sfmdirLabel.Text = "Source &Filmmaker directory:";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.Controls.Add(this.detectSFMButton, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.browseSFMButton, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.sfmLocationTextBox, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 27);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(326, 24);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // detectSFMButton
            // 
            this.detectSFMButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detectSFMButton.Image = global::SFMTK.Properties.Resources.find;
            this.detectSFMButton.Location = new System.Drawing.Point(302, 0);
            this.detectSFMButton.Margin = new System.Windows.Forms.Padding(0);
            this.detectSFMButton.Name = "detectSFMButton";
            this.detectSFMButton.Size = new System.Drawing.Size(24, 24);
            this.detectSFMButton.TabIndex = 2;
            this.toolTip1.SetToolTip(this.detectSFMButton, "Automatically detect location of sfm.exe");
            this.detectSFMButton.UseVisualStyleBackColor = true;
            this.detectSFMButton.Click += new System.EventHandler(this.detectSFMButton_Click);
            // 
            // browseSFMButton
            // 
            this.browseSFMButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browseSFMButton.Image = global::SFMTK.Properties.Resources.folder;
            this.browseSFMButton.Location = new System.Drawing.Point(278, 0);
            this.browseSFMButton.Margin = new System.Windows.Forms.Padding(0);
            this.browseSFMButton.Name = "browseSFMButton";
            this.browseSFMButton.Size = new System.Drawing.Size(24, 24);
            this.browseSFMButton.TabIndex = 1;
            this.toolTip1.SetToolTip(this.browseSFMButton, "Browse for sfm.exe");
            this.browseSFMButton.UseVisualStyleBackColor = true;
            this.browseSFMButton.Click += new System.EventHandler(this.browseSFMButton_Click);
            // 
            // sfmLocationTextBox
            // 
            this.sfmLocationTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.sfmLocationTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystemDirectories;
            this.sfmLocationTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sfmLocationTextBox.Location = new System.Drawing.Point(0, 2);
            this.sfmLocationTextBox.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.sfmLocationTextBox.Name = "sfmLocationTextBox";
            this.sfmLocationTextBox.Size = new System.Drawing.Size(278, 20);
            this.sfmLocationTextBox.TabIndex = 0;
            this.toolTip1.SetToolTip(this.sfmLocationTextBox, "The directory containing your installation of Source Filmmaker.");
            this.sfmLocationTextBox.TextChanged += new System.EventHandler(this.sfmLocationTextBox_TextChanged);
            // 
            // sfmdirInfoLabel
            // 
            this.sfmdirInfoLabel.AutoSize = true;
            this.sfmdirInfoLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.sfmdirInfoLabel.Location = new System.Drawing.Point(3, 54);
            this.sfmdirInfoLabel.Name = "sfmdirInfoLabel";
            this.sfmdirInfoLabel.Size = new System.Drawing.Size(326, 26);
            this.sfmdirInfoLabel.TabIndex = 0;
            this.sfmdirInfoLabel.Text = "SFMTK needs to know where Source Filmmaker is installed in order to correctly fin" +
    "d and install content.";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.themeComboBox);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(3, 106);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 26, 3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(213, 22);
            this.panel2.TabIndex = 10;
            // 
            // themeComboBox
            // 
            this.themeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.themeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.themeComboBox.FormattingEnabled = true;
            this.themeComboBox.Location = new System.Drawing.Point(48, 0);
            this.themeComboBox.Name = "themeComboBox";
            this.themeComboBox.Size = new System.Drawing.Size(165, 21);
            this.themeComboBox.TabIndex = 1;
            this.toolTip1.SetToolTip(this.themeComboBox, "Currently selected application theme.");
            this.themeComboBox.SelectionChangeCommitted += new System.EventHandler(this.themeComboBox_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "&Theme:";
            // 
            // FirstLaunch
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 399);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.outroLabel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.introLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(582, 414);
            this.Name = "FirstLaunch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SFMTK";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FirstLaunch_FormClosing);
            this.Load += new System.EventHandler(this.FirstLaunch_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.sfmAssocRadioGroupPanel.ResumeLayout(false);
            this.sfmAssocRadioGroupPanel.PerformLayout();
            this.dmxAssocRadioGroupPanel.ResumeLayout(false);
            this.dmxAssocRadioGroupPanel.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label introLabel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label outroLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private Controls.RadioGroupPanel sfmAssocRadioGroupPanel;
        private System.Windows.Forms.RadioButton currentSfmFileRadioButton;
        private System.Windows.Forms.RadioButton sfmtkSfmFileRadioButton;
        private Controls.RadioGroupPanel dmxAssocRadioGroupPanel;
        private System.Windows.Forms.RadioButton currentDmxFileRadioButton;
        private System.Windows.Forms.RadioButton sfmtkDmxFileRadioButton;
        private System.Windows.Forms.RadioButton sfmDmxFileRadioButton;
        private System.Windows.Forms.Label dmxFileLabel;
        private System.Windows.Forms.Label sfmFileLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label dmxInfoLabel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label sfmdirLabel;
        private Controls.InvisibleControl invisibleControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox sfmLocationTextBox;
        private System.Windows.Forms.Label sfmdirInfoLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox themeComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button browseSFMButton;
        private System.Windows.Forms.Button detectSFMButton;
        private Controls.ErrorMarker sfmLocationErrorMarker;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.RadioButton askDmxFileRadioButton;
    }
}