﻿namespace SFMTK.Forms
{
    partial class EditSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditSetup));
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.shadowResComboBox = new System.Windows.Forms.ComboBox();
            this.startupComboBox = new System.Windows.Forms.ComboBox();
            this.additionalParamsTextBox = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.themeHelpButton = new System.Windows.Forms.Button();
            this.noSteamCheckBox = new System.Windows.Forms.CheckBox();
            this.noPerforceCheckBox = new System.Windows.Forms.CheckBox();
            this.monitorQualityComboBox = new System.Windows.Forms.ComboBox();
            this.noAutosaveCheckBox = new System.Windows.Forms.CheckBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.resizeEngineCheckBox = new System.Windows.Forms.CheckBox();
            this.reflectionQualityComboBox = new System.Windows.Forms.ComboBox();
            this.mocapCheckBox = new System.Windows.Forms.CheckBox();
            this.forceCloseCheckBox = new System.Windows.Forms.CheckBox();
            this.closeSFMRadioButton = new System.Windows.Forms.RadioButton();
            this.waitForCloseRadioButton = new System.Windows.Forms.RadioButton();
            this.noLaunchRadioButton = new System.Windows.Forms.RadioButton();
            this.closeSFMTKCheckBox = new System.Windows.Forms.CheckBox();
            this.closeQuickLaunchCheckBox = new System.Windows.Forms.CheckBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.autoCompileHelpbutton = new System.Windows.Forms.Button();
            this.copyModsButton = new System.Windows.Forms.Button();
            this.editModsButton = new System.Windows.Forms.Button();
            this.hqWaterCheckBox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.executionTabPage = new System.Windows.Forms.TabPage();
            this.autoCompileRadioGroupPanel = new SFMTK.Controls.RadioGroupPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.alreadyOpenRadioGroupPanel = new SFMTK.Controls.RadioGroupPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.launchTabPage = new System.Windows.Forms.TabPage();
            this.modsTabPage = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.bottomSpacingPanel = new System.Windows.Forms.Panel();
            this.bottomTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.commandManager = new SFMTK.Commands.CommandManager();
            this.tabControl1.SuspendLayout();
            this.executionTabPage.SuspendLayout();
            this.autoCompileRadioGroupPanel.SuspendLayout();
            this.alreadyOpenRadioGroupPanel.SuspendLayout();
            this.launchTabPage.SuspendLayout();
            this.modsTabPage.SuspendLayout();
            this.bottomSpacingPanel.SuspendLayout();
            this.bottomTableLayoutPanel.SuspendLayout();
            this.headerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.commandManager.SetCommand(this.cancelButton, "");
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cancelButton.Location = new System.Drawing.Point(339, 0);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(0);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 28);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            this.commandManager.SetCommand(this.okButton, "");
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.okButton.Location = new System.Drawing.Point(264, 0);
            this.okButton.Margin = new System.Windows.Forms.Padding(0);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 28);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameTextBox.Location = new System.Drawing.Point(40, 0);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(372, 20);
            this.nameTextBox.TabIndex = 1;
            this.toolTip1.SetToolTip(this.nameTextBox, "How this setup can best be described, what it is used for, ...");
            // 
            // shadowResComboBox
            // 
            this.shadowResComboBox.FormattingEnabled = true;
            this.shadowResComboBox.Location = new System.Drawing.Point(124, 52);
            this.shadowResComboBox.Name = "shadowResComboBox";
            this.shadowResComboBox.Size = new System.Drawing.Size(176, 21);
            this.shadowResComboBox.TabIndex = 4;
            this.shadowResComboBox.Text = "check valid values, give em names";
            this.toolTip1.SetToolTip(this.shadowResComboBox, "Changes the quality of your shadows (-sfm_shadowmapres). Higher numbers result in" +
        " lower framerate, but higher quality.");
            // 
            // startupComboBox
            // 
            this.startupComboBox.FormattingEnabled = true;
            this.startupComboBox.Items.AddRange(new object[] {
            "Do nothing",
            "Show startup wizard",
            "Open with session",
            "Ask for session"});
            this.startupComboBox.Location = new System.Drawing.Point(124, 84);
            this.startupComboBox.Name = "startupComboBox";
            this.startupComboBox.Size = new System.Drawing.Size(176, 21);
            this.startupComboBox.TabIndex = 0;
            this.startupComboBox.Text = "make this prettier (radio)";
            this.toolTip1.SetToolTip(this.startupComboBox, "How SFM should start up. (-sfm_loadsession and -nostartwizard)");
            // 
            // additionalParamsTextBox
            // 
            this.additionalParamsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.additionalParamsTextBox.Location = new System.Drawing.Point(124, 478);
            this.additionalParamsTextBox.Name = "additionalParamsTextBox";
            this.additionalParamsTextBox.Size = new System.Drawing.Size(268, 20);
            this.additionalParamsTextBox.TabIndex = 20;
            this.toolTip1.SetToolTip(this.additionalParamsTextBox, "This list of parameters is added to the end of the generated launch parameter lis" +
        "t");
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(124, 116);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(176, 21);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.Text = "allow for stylesheets too";
            this.toolTip1.SetToolTip(this.comboBox1, "Changes which QT theme is used by SFM.");
            // 
            // themeHelpButton
            // 
            this.commandManager.SetCommand(this.themeHelpButton, "HelpCommand;SFM Themes");
            this.themeHelpButton.Image = ((System.Drawing.Image)(resources.GetObject("themeHelpButton.Image")));
            this.themeHelpButton.Location = new System.Drawing.Point(304, 115);
            this.themeHelpButton.Name = "themeHelpButton";
            this.themeHelpButton.Size = new System.Drawing.Size(24, 24);
            this.themeHelpButton.TabIndex = 9;
            this.toolTip1.SetToolTip(this.themeHelpButton, "Displays help about themes in SFM.");
            this.themeHelpButton.UseVisualStyleBackColor = true;
            // 
            // noSteamCheckBox
            // 
            this.noSteamCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.noSteamCheckBox.AutoSize = true;
            this.noSteamCheckBox.Location = new System.Drawing.Point(12, 432);
            this.noSteamCheckBox.Name = "noSteamCheckBox";
            this.noSteamCheckBox.Size = new System.Drawing.Size(116, 17);
            this.noSteamCheckBox.TabIndex = 17;
            this.noSteamCheckBox.Text = "Run without &Steam";
            this.toolTip1.SetToolTip(this.noSteamCheckBox, "Does not require Steam to be running for SFM to launch (-nosteam). May cause work" +
        "shop features to not work correctly.");
            this.noSteamCheckBox.UseVisualStyleBackColor = true;
            // 
            // noPerforceCheckBox
            // 
            this.noPerforceCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.noPerforceCheckBox.AutoSize = true;
            this.noPerforceCheckBox.Checked = true;
            this.noPerforceCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.noPerforceCheckBox.Location = new System.Drawing.Point(12, 409);
            this.noPerforceCheckBox.Name = "noPerforceCheckBox";
            this.noPerforceCheckBox.Size = new System.Drawing.Size(126, 17);
            this.noPerforceCheckBox.TabIndex = 16;
            this.noPerforceCheckBox.Text = "Run without &Perforce";
            this.toolTip1.SetToolTip(this.noPerforceCheckBox, "By default, SFM uses Perforce for Version Control, slowing down parts of the soft" +
        "ware unnecessarily if you do not use Perforce.\r\nEnable this to gain a performanc" +
        "e boost in most cases (-nop4).");
            this.noPerforceCheckBox.UseVisualStyleBackColor = true;
            // 
            // monitorQualityComboBox
            // 
            this.monitorQualityComboBox.FormattingEnabled = true;
            this.monitorQualityComboBox.Location = new System.Drawing.Point(124, 144);
            this.monitorQualityComboBox.Name = "monitorQualityComboBox";
            this.monitorQualityComboBox.Size = new System.Drawing.Size(176, 21);
            this.monitorQualityComboBox.TabIndex = 11;
            this.monitorQualityComboBox.Text = "check valid values, give em names";
            this.toolTip1.SetToolTip(this.monitorQualityComboBox, "Changes the quality of render target monitors (-monitortexturesize). Higher numbe" +
        "rs result in higher quality.");
            // 
            // noAutosaveCheckBox
            // 
            this.noAutosaveCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.noAutosaveCheckBox.AutoSize = true;
            this.noAutosaveCheckBox.Location = new System.Drawing.Point(12, 456);
            this.noAutosaveCheckBox.Name = "noAutosaveCheckBox";
            this.noAutosaveCheckBox.Size = new System.Drawing.Size(113, 17);
            this.noAutosaveCheckBox.TabIndex = 18;
            this.noAutosaveCheckBox.Text = "Disable &autosaves";
            this.toolTip1.SetToolTip(this.noAutosaveCheckBox, "SFM will not autosave sessions (-sfm_noautosave).");
            this.noAutosaveCheckBox.UseVisualStyleBackColor = true;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(124, 4);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(176, 21);
            this.comboBox3.TabIndex = 1;
            this.comboBox3.Text = "check valid values";
            this.toolTip1.SetToolTip(this.comboBox3, "Changes which resolutions are selectable for movie renders (-sfm_resolution).");
            // 
            // resizeEngineCheckBox
            // 
            this.resizeEngineCheckBox.AutoSize = true;
            this.resizeEngineCheckBox.Checked = true;
            this.resizeEngineCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.resizeEngineCheckBox.Location = new System.Drawing.Point(24, 28);
            this.resizeEngineCheckBox.Name = "resizeEngineCheckBox";
            this.resizeEngineCheckBox.Size = new System.Drawing.Size(132, 17);
            this.resizeEngineCheckBox.TabIndex = 2;
            this.resizeEngineCheckBox.Text = "Resi&ze engine window";
            this.toolTip1.SetToolTip(this.resizeEngineCheckBox, "If enabled, the game engine is resized to fit the specified render resolution (-w" +
        " and -h).\r\nWithout this option, you will get an error message on every launch re" +
        "garding an inaccurate preview.");
            this.resizeEngineCheckBox.UseVisualStyleBackColor = true;
            // 
            // reflectionQualityComboBox
            // 
            this.reflectionQualityComboBox.FormattingEnabled = true;
            this.reflectionQualityComboBox.Location = new System.Drawing.Point(124, 172);
            this.reflectionQualityComboBox.Name = "reflectionQualityComboBox";
            this.reflectionQualityComboBox.Size = new System.Drawing.Size(176, 21);
            this.reflectionQualityComboBox.TabIndex = 13;
            this.reflectionQualityComboBox.Text = "check valid values, give em names";
            this.toolTip1.SetToolTip(this.reflectionQualityComboBox, "Changes the quality of reflections (-reflectiontexturesize). Higher numbers resul" +
        "t in higher quality.");
            // 
            // mocapCheckBox
            // 
            this.mocapCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mocapCheckBox.AutoSize = true;
            this.mocapCheckBox.Location = new System.Drawing.Point(12, 384);
            this.mocapCheckBox.Name = "mocapCheckBox";
            this.mocapCheckBox.Size = new System.Drawing.Size(157, 17);
            this.mocapCheckBox.TabIndex = 15;
            this.mocapCheckBox.Text = "Enable motion &capture tools";
            this.toolTip1.SetToolTip(this.mocapCheckBox, "Adds additional options for motion capture tools (-mocap)");
            this.mocapCheckBox.UseVisualStyleBackColor = true;
            // 
            // forceCloseCheckBox
            // 
            this.forceCloseCheckBox.AutoSize = true;
            this.forceCloseCheckBox.Location = new System.Drawing.Point(184, 28);
            this.forceCloseCheckBox.Name = "forceCloseCheckBox";
            this.forceCloseCheckBox.Size = new System.Drawing.Size(105, 17);
            this.forceCloseCheckBox.TabIndex = 0;
            this.forceCloseCheckBox.Text = "...&forcefully close";
            this.toolTip1.SetToolTip(this.forceCloseCheckBox, "Closes SFM without waiting for it to respond. Any unsaved data is lost!");
            this.forceCloseCheckBox.UseVisualStyleBackColor = true;
            // 
            // closeSFMRadioButton
            // 
            this.closeSFMRadioButton.AutoSize = true;
            this.closeSFMRadioButton.Checked = true;
            this.closeSFMRadioButton.Location = new System.Drawing.Point(3, 3);
            this.closeSFMRadioButton.Name = "closeSFMRadioButton";
            this.closeSFMRadioButton.Size = new System.Drawing.Size(151, 17);
            this.closeSFMRadioButton.TabIndex = 0;
            this.closeSFMRadioButton.TabStop = true;
            this.closeSFMRadioButton.Tag = "0";
            this.closeSFMRadioButton.Text = "&Close any instance of SFM";
            this.toolTip1.SetToolTip(this.closeSFMRadioButton, "Closes any running instance of SFM, asking you to save or close any other dialogs" +
        "");
            this.closeSFMRadioButton.UseVisualStyleBackColor = true;
            // 
            // waitForCloseRadioButton
            // 
            this.waitForCloseRadioButton.AutoSize = true;
            this.waitForCloseRadioButton.Location = new System.Drawing.Point(3, 26);
            this.waitForCloseRadioButton.Name = "waitForCloseRadioButton";
            this.waitForCloseRadioButton.Size = new System.Drawing.Size(127, 17);
            this.waitForCloseRadioButton.TabIndex = 1;
            this.waitForCloseRadioButton.Tag = "1";
            this.waitForCloseRadioButton.Text = "&Wait for SFM to close";
            this.toolTip1.SetToolTip(this.waitForCloseRadioButton, "Waits for SFM to close before attempting to launch a new instance of SFM, allowin" +
        "g you to finish working");
            this.waitForCloseRadioButton.UseVisualStyleBackColor = true;
            // 
            // noLaunchRadioButton
            // 
            this.noLaunchRadioButton.AutoSize = true;
            this.noLaunchRadioButton.Location = new System.Drawing.Point(3, 49);
            this.noLaunchRadioButton.Name = "noLaunchRadioButton";
            this.noLaunchRadioButton.Size = new System.Drawing.Size(92, 17);
            this.noLaunchRadioButton.TabIndex = 2;
            this.noLaunchRadioButton.Tag = "2";
            this.noLaunchRadioButton.Text = "&Do not launch";
            this.toolTip1.SetToolTip(this.noLaunchRadioButton, "Does nothing when SFM is already running and you attempt to run it again (you can" +
        " not have two instances of SFM running at the same time either way)");
            this.noLaunchRadioButton.UseVisualStyleBackColor = true;
            // 
            // closeSFMTKCheckBox
            // 
            this.closeSFMTKCheckBox.AutoSize = true;
            this.closeSFMTKCheckBox.Location = new System.Drawing.Point(20, 128);
            this.closeSFMTKCheckBox.Name = "closeSFMTKCheckBox";
            this.closeSFMTKCheckBox.Size = new System.Drawing.Size(91, 17);
            this.closeSFMTKCheckBox.TabIndex = 4;
            this.closeSFMTKCheckBox.Text = "&Close SFMTK";
            this.toolTip1.SetToolTip(this.closeSFMTKCheckBox, "Closes SFMTK after successfully launching SFM through this setup");
            this.closeSFMTKCheckBox.UseVisualStyleBackColor = true;
            // 
            // closeQuickLaunchCheckBox
            // 
            this.closeQuickLaunchCheckBox.AutoSize = true;
            this.closeQuickLaunchCheckBox.Location = new System.Drawing.Point(120, 128);
            this.closeQuickLaunchCheckBox.Name = "closeQuickLaunchCheckBox";
            this.closeQuickLaunchCheckBox.Size = new System.Drawing.Size(137, 17);
            this.closeQuickLaunchCheckBox.TabIndex = 5;
            this.closeQuickLaunchCheckBox.Text = "...even if &Quick-Launch";
            this.toolTip1.SetToolTip(this.closeQuickLaunchCheckBox, "If checked, even closes SFMTK if the setup was opened through Quick-Launch (or th" +
        "e shortcut key)");
            this.closeQuickLaunchCheckBox.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(3, 49);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(136, 17);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.Tag = "2";
            this.radioButton1.Text = "&While SFM is launching";
            this.toolTip1.SetToolTip(this.radioButton1, "Content is auto-compiled while SFM is launching. If any content is loaded before " +
        "compiling is done, you may need to restart SFM to reload it fully.");
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(3, 26);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(132, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Tag = "1";
            this.radioButton2.Text = "&Launch after compiling";
            this.toolTip1.SetToolTip(this.radioButton2, "SFM will only launch once auto-compiling is done, to ensure that all content will" +
        " definitely be compiled.");
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(3, 72);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(120, 17);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.Tag = "3";
            this.radioButton3.Text = "Do &not auto-compile";
            this.toolTip1.SetToolTip(this.radioButton3, "This setup will never auto-compile any content on launch.");
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Checked = true;
            this.radioButton4.Location = new System.Drawing.Point(3, 3);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(116, 17);
            this.radioButton4.TabIndex = 0;
            this.radioButton4.TabStop = true;
            this.radioButton4.Tag = "0";
            this.radioButton4.Text = "&Dynamically decide";
            this.toolTip1.SetToolTip(this.radioButton4, "Auto-compiling is done while launching if it can finish in time, otherwise SFM\'s " +
        "launch is delayed.");
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // autoCompileHelpbutton
            // 
            this.commandManager.SetCommand(this.autoCompileHelpbutton, "HelpCommand;Autocompiling");
            this.autoCompileHelpbutton.Image = ((System.Drawing.Image)(resources.GetObject("autoCompileHelpbutton.Image")));
            this.autoCompileHelpbutton.Location = new System.Drawing.Point(132, 152);
            this.autoCompileHelpbutton.Name = "autoCompileHelpbutton";
            this.autoCompileHelpbutton.Size = new System.Drawing.Size(24, 24);
            this.autoCompileHelpbutton.TabIndex = 7;
            this.toolTip1.SetToolTip(this.autoCompileHelpbutton, "Displays help about auto-compiling and compile on save.");
            this.autoCompileHelpbutton.UseVisualStyleBackColor = true;
            // 
            // copyModsButton
            // 
            this.copyModsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.commandManager.SetCommand(this.copyModsButton, "");
            this.copyModsButton.Location = new System.Drawing.Point(244, 472);
            this.copyModsButton.Name = "copyModsButton";
            this.copyModsButton.Size = new System.Drawing.Size(151, 27);
            this.copyModsButton.TabIndex = 2;
            this.copyModsButton.Text = "&Use current configuration";
            this.toolTip1.SetToolTip(this.copyModsButton, "Copies the current mod configuration (as configured in the Mod Manager) to be use" +
        "d for this setup.");
            this.copyModsButton.UseVisualStyleBackColor = true;
            this.copyModsButton.Click += new System.EventHandler(this.copyModsButton_Click);
            // 
            // editModsButton
            // 
            this.editModsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.commandManager.SetCommand(this.editModsButton, "");
            this.editModsButton.Location = new System.Drawing.Point(88, 472);
            this.editModsButton.Name = "editModsButton";
            this.editModsButton.Size = new System.Drawing.Size(151, 27);
            this.editModsButton.TabIndex = 1;
            this.editModsButton.Text = "&Edit configuration";
            this.toolTip1.SetToolTip(this.editModsButton, "Edits the mod configuration for this setup.");
            this.editModsButton.UseVisualStyleBackColor = true;
            this.editModsButton.Click += new System.EventHandler(this.editModsButton_Click);
            // 
            // hqWaterCheckBox
            // 
            this.hqWaterCheckBox.AutoSize = true;
            this.hqWaterCheckBox.Checked = true;
            this.hqWaterCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.hqWaterCheckBox.Location = new System.Drawing.Point(12, 200);
            this.hqWaterCheckBox.Name = "hqWaterCheckBox";
            this.hqWaterCheckBox.Size = new System.Drawing.Size(110, 17);
            this.hqWaterCheckBox.TabIndex = 14;
            this.hqWaterCheckBox.Text = "&High quality water";
            this.toolTip1.SetToolTip(this.hqWaterCheckBox, "Enables high quality rendering of water (+r_waterforceexpensive 1 and +r_waterfor" +
        "cereflectentities 1).");
            this.hqWaterCheckBox.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "&Shadow resolution:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "S&tartup:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 176);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Re&flection resolution:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "&Render resolution:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "RT &Monitor resolution:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 120);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "&Theme:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 480);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "A&dditional parameters:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.executionTabPage);
            this.tabControl1.Controls.Add(this.launchTabPage);
            this.tabControl1.Controls.Add(this.modsTabPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(6, 34);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(414, 536);
            this.tabControl1.TabIndex = 1;
            // 
            // executionTabPage
            // 
            this.executionTabPage.BackColor = System.Drawing.SystemColors.Window;
            this.executionTabPage.Controls.Add(this.autoCompileRadioGroupPanel);
            this.executionTabPage.Controls.Add(this.autoCompileHelpbutton);
            this.executionTabPage.Controls.Add(this.label13);
            this.executionTabPage.Controls.Add(this.forceCloseCheckBox);
            this.executionTabPage.Controls.Add(this.label3);
            this.executionTabPage.Controls.Add(this.alreadyOpenRadioGroupPanel);
            this.executionTabPage.Controls.Add(this.closeSFMTKCheckBox);
            this.executionTabPage.Controls.Add(this.label1);
            this.executionTabPage.Controls.Add(this.closeQuickLaunchCheckBox);
            this.executionTabPage.Location = new System.Drawing.Point(4, 22);
            this.executionTabPage.Name = "executionTabPage";
            this.executionTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.executionTabPage.Size = new System.Drawing.Size(406, 510);
            this.executionTabPage.TabIndex = 2;
            this.executionTabPage.Text = "Execution";
            // 
            // autoCompileRadioGroupPanel
            // 
            this.autoCompileRadioGroupPanel.Controls.Add(this.radioButton4);
            this.autoCompileRadioGroupPanel.Controls.Add(this.radioButton2);
            this.autoCompileRadioGroupPanel.Controls.Add(this.radioButton1);
            this.autoCompileRadioGroupPanel.Controls.Add(this.radioButton3);
            this.autoCompileRadioGroupPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.autoCompileRadioGroupPanel.Location = new System.Drawing.Point(16, 176);
            this.autoCompileRadioGroupPanel.Name = "autoCompileRadioGroupPanel";
            this.autoCompileRadioGroupPanel.Selected = 0;
            this.autoCompileRadioGroupPanel.Size = new System.Drawing.Size(148, 104);
            this.autoCompileRadioGroupPanel.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 156);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(118, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Aut&o-compiling content:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(193, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "If SFM is already &open when launching:";
            // 
            // alreadyOpenRadioGroupPanel
            // 
            this.alreadyOpenRadioGroupPanel.Controls.Add(this.closeSFMRadioButton);
            this.alreadyOpenRadioGroupPanel.Controls.Add(this.waitForCloseRadioButton);
            this.alreadyOpenRadioGroupPanel.Controls.Add(this.noLaunchRadioButton);
            this.alreadyOpenRadioGroupPanel.Location = new System.Drawing.Point(18, 24);
            this.alreadyOpenRadioGroupPanel.Name = "alreadyOpenRadioGroupPanel";
            this.alreadyOpenRadioGroupPanel.Selected = 0;
            this.alreadyOpenRadioGroupPanel.Size = new System.Drawing.Size(160, 76);
            this.alreadyOpenRadioGroupPanel.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "&After SFM launched:";
            // 
            // launchTabPage
            // 
            this.launchTabPage.BackColor = System.Drawing.SystemColors.Window;
            this.launchTabPage.Controls.Add(this.hqWaterCheckBox);
            this.launchTabPage.Controls.Add(this.mocapCheckBox);
            this.launchTabPage.Controls.Add(this.reflectionQualityComboBox);
            this.launchTabPage.Controls.Add(this.label9);
            this.launchTabPage.Controls.Add(this.label10);
            this.launchTabPage.Controls.Add(this.label5);
            this.launchTabPage.Controls.Add(this.resizeEngineCheckBox);
            this.launchTabPage.Controls.Add(this.shadowResComboBox);
            this.launchTabPage.Controls.Add(this.startupComboBox);
            this.launchTabPage.Controls.Add(this.comboBox3);
            this.launchTabPage.Controls.Add(this.label4);
            this.launchTabPage.Controls.Add(this.noAutosaveCheckBox);
            this.launchTabPage.Controls.Add(this.label6);
            this.launchTabPage.Controls.Add(this.monitorQualityComboBox);
            this.launchTabPage.Controls.Add(this.additionalParamsTextBox);
            this.launchTabPage.Controls.Add(this.label8);
            this.launchTabPage.Controls.Add(this.label7);
            this.launchTabPage.Controls.Add(this.noPerforceCheckBox);
            this.launchTabPage.Controls.Add(this.comboBox1);
            this.launchTabPage.Controls.Add(this.noSteamCheckBox);
            this.launchTabPage.Controls.Add(this.themeHelpButton);
            this.launchTabPage.Location = new System.Drawing.Point(4, 22);
            this.launchTabPage.Name = "launchTabPage";
            this.launchTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.launchTabPage.Size = new System.Drawing.Size(406, 510);
            this.launchTabPage.TabIndex = 0;
            this.launchTabPage.Text = "Launch";
            // 
            // modsTabPage
            // 
            this.modsTabPage.BackColor = System.Drawing.SystemColors.Window;
            this.modsTabPage.Controls.Add(this.label11);
            this.modsTabPage.Controls.Add(this.editModsButton);
            this.modsTabPage.Controls.Add(this.copyModsButton);
            this.modsTabPage.Location = new System.Drawing.Point(4, 22);
            this.modsTabPage.Name = "modsTabPage";
            this.modsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.modsTabPage.Size = new System.Drawing.Size(406, 510);
            this.modsTabPage.TabIndex = 1;
            this.modsTabPage.Text = "Mods";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoEllipsis = true;
            this.label11.Location = new System.Drawing.Point(8, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(384, 44);
            this.label11.TabIndex = 0;
            this.label11.Text = "Select which mods should be enabled for this setup. This changes what content get" +
    "s loaded when for example selecting models in SFM.";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // bottomSpacingPanel
            // 
            this.bottomSpacingPanel.Controls.Add(this.bottomTableLayoutPanel);
            this.bottomSpacingPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomSpacingPanel.Location = new System.Drawing.Point(6, 570);
            this.bottomSpacingPanel.Name = "bottomSpacingPanel";
            this.bottomSpacingPanel.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.bottomSpacingPanel.Size = new System.Drawing.Size(414, 32);
            this.bottomSpacingPanel.TabIndex = 1;
            // 
            // bottomTableLayoutPanel
            // 
            this.bottomTableLayoutPanel.ColumnCount = 3;
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.bottomTableLayoutPanel.Controls.Add(this.cancelButton, 2, 0);
            this.bottomTableLayoutPanel.Controls.Add(this.okButton, 1, 0);
            this.bottomTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottomTableLayoutPanel.Location = new System.Drawing.Point(0, 4);
            this.bottomTableLayoutPanel.Name = "bottomTableLayoutPanel";
            this.bottomTableLayoutPanel.RowCount = 1;
            this.bottomTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.bottomTableLayoutPanel.Size = new System.Drawing.Size(414, 28);
            this.bottomTableLayoutPanel.TabIndex = 0;
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.label2);
            this.headerPanel.Controls.Add(this.nameTextBox);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(6, 6);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(414, 28);
            this.headerPanel.TabIndex = 23;
            // 
            // commandManager
            // 
            this.commandManager.CommandHistory = null;
            // 
            // EditSetup
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(426, 608);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.headerPanel);
            this.Controls.Add(this.bottomSpacingPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(365, 486);
            this.Name = "EditSetup";
            this.Padding = new System.Windows.Forms.Padding(6);
            this.Text = "Edit Setup";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditSetup_FormClosing);
            this.Load += new System.EventHandler(this.EditSetup_Load);
            this.tabControl1.ResumeLayout(false);
            this.executionTabPage.ResumeLayout(false);
            this.executionTabPage.PerformLayout();
            this.autoCompileRadioGroupPanel.ResumeLayout(false);
            this.autoCompileRadioGroupPanel.PerformLayout();
            this.alreadyOpenRadioGroupPanel.ResumeLayout(false);
            this.alreadyOpenRadioGroupPanel.PerformLayout();
            this.launchTabPage.ResumeLayout(false);
            this.launchTabPage.PerformLayout();
            this.modsTabPage.ResumeLayout(false);
            this.bottomSpacingPanel.ResumeLayout(false);
            this.bottomTableLayoutPanel.ResumeLayout(false);
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox shadowResComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox startupComboBox;
        private System.Windows.Forms.TextBox additionalParamsTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button themeHelpButton;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox monitorQualityComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox noPerforceCheckBox;
        private System.Windows.Forms.CheckBox noSteamCheckBox;
        private System.Windows.Forms.ComboBox reflectionQualityComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox resizeEngineCheckBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.CheckBox noAutosaveCheckBox;
        private System.Windows.Forms.CheckBox mocapCheckBox;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage launchTabPage;
        private System.Windows.Forms.TabPage modsTabPage;
        private System.Windows.Forms.Panel bottomSpacingPanel;
        private System.Windows.Forms.TableLayoutPanel bottomTableLayoutPanel;
        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.TabPage executionTabPage;
        private System.Windows.Forms.CheckBox forceCloseCheckBox;
        private System.Windows.Forms.Label label3;
        private Controls.RadioGroupPanel alreadyOpenRadioGroupPanel;
        private System.Windows.Forms.RadioButton closeSFMRadioButton;
        private System.Windows.Forms.RadioButton waitForCloseRadioButton;
        private System.Windows.Forms.RadioButton noLaunchRadioButton;
        private System.Windows.Forms.CheckBox closeSFMTKCheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox closeQuickLaunchCheckBox;
        private System.Windows.Forms.Button autoCompileHelpbutton;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label13;
        private Controls.RadioGroupPanel autoCompileRadioGroupPanel;
        private System.Windows.Forms.Button copyModsButton;
        private System.Windows.Forms.Button editModsButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox hqWaterCheckBox;
        private Commands.CommandManager commandManager;
    }
}