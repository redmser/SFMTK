﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using SFMTK.Properties;
using SFMTK.Controls.PreferencePages;
using SFMTK.Modules;

namespace SFMTK.Forms
{
    /// <summary>
    /// Preferences dialog box.
    /// </summary>
    /// <seealso cref="Form" />
    public partial class Preferences : Form, IThemeable
    {
        // Notes to anyone looking to extend the Preferences window:
        // - Property pages are to be added as a class in the SFMTK.Controls.PreferencePages namespace, and be part of the "Nodes" list.
        //   Be sure to set their Path and Image properties in the designer, as well.
        //   - For modules to implement custom preference pages, view the documentation on IModule instead.
        // - Most settings keys use simple two-way DataBinding from inside the Designer with an ApplicationSettings key.
        // - For more complex DataBinding, check out the RerouteDataSource class.
        // - All controls inside have tooltips which provide simple explanation for the setting in question.
        // - For verifying data, you may add a ErrorMarker control, but do not disallow closing the Preferences form if data is invalid!

        //FIXME: Preferences - no need to save StatusBar visibility as a separate settings key, since the toolstrip manager already does this
        //TODO: Preferences - as changing theme may start taking longer over time, add a loading dialog box (probably a non-modal task dialog-alike) so people know it aint dying
        //BUG: Preference - preferencepages do not get added/removed when loading/unloading a module by pressing "apply" in the settings
        //  -> also do module.CustomSettings.PropertyChanged -= this.SettingChange for IModuleSettings

        /// <summary>
        /// Whether the dialog closing is due to pressing OK (saving settings) or cancelling (discarding settings).
        /// </summary>
        private bool _saving;

        /// <summary>
        /// The control which should be given focus when the dialog has loaded.
        /// </summary>
        internal string _focus;

        /// <summary>
        /// Whether <see cref="_focus"/> is a <see cref="IPreferencePage"/> path.
        /// </summary>
        internal bool _isPage;

        /// <summary>
        /// Creates a new preferences dialog open on the first tab.
        /// </summary>
        public Preferences()
        {
            //Compiler-generated
            InitializeComponent();

            //Events
            Settings.Default.PropertyChanged += this.SettingChange;
            this.searchTextBox.TextBox.TextChanged += this.searchTextBox_TextChanged;
        }

        private void ReloadPreferencePages()
        {
            //Reset to default list
            this.Nodes = this._defaultNodes;

            //Append module's pages
            foreach (var module in Program.MainMM.LoadedModules.OfType<IModuleSettings>())
            {
                //Try adding event
                if (module.CustomSettings != null)
                    module.CustomSettings.PropertyChanged += this.SettingChange;

                //Do not allow invalid preference page settings
                var pages = module.GetPages()?.ToList();
                if (pages == null)
                    return;

                if (pages.Count > ModuleManager.MaxPreferencePages)
                    ModuleManager.ThrowModulePropertyException(module, $"Pages specifies too many entries ({pages.Count} > {ModuleManager.MaxPreferencePages})");

                //Add pages to list
                this.Nodes.AddRange(pages);
            }
        }

        private void searchTextBox_TextChanged(object sender, EventArgs e)
        {
            //TODO: Preferences - convert this one-off highlighting search textbox to an abstract system which can be applied to any
            //  list of controls (possibly using an interface backed by a manager instance? not sure yet)
            if (!string.IsNullOrWhiteSpace(this.searchTextBox.Text))
            {
                foreach (var node in this.Nodes)
                {
                    //Find pages that have the text in their name
                    node.Filter = node.Text.ContainsIgnoreCase(this.searchTextBox.Text);
                    if (node is PreferencePage pp)
                    {
                        //Secondary check: see if any controls (by name or text) match
                        var match = ControlsHelper.RecurseControls(pp, false)
                            .Where(ctrl =>
                            ctrl.Text.ContainsIgnoreCase(this.searchTextBox.Text) ||
                            (ctrl.Name.ContainsIgnoreCase(this.searchTextBox.Text) && IsValidControl(ctrl)) ||
                            ControlsHelper.GetTooltipText(ctrl, pp).ContainsIgnoreCase(this.searchTextBox.Text))
                            .ToList();
                        node.Filter = node.Filter || match.Any();
                        
                        //Before clearing the filter controls, reset theme!
                        if (pp.FilterControls != null)
                            foreach (var reset in pp.FilterControls.Where(c => !match.Contains(c)))
                                UITheme.SelectedTheme.Apply(reset);

                        //FIXME: Preferences - also highlight the pages in the treeview that have the text in their name (similarly to the controls on the pages)
                        //  -> decoration that highlights only the letters in the textbox, if i remember correctly, exists

                        //Apply now
                        pp.FilterControls = match;
                    }
                }
                
                //Apply the filter and color highlights
                this.pageTreeListView.AdditionalFilter = new PreferencePageFilter();
                this.pageTreeListView.TreeColumnRenderer.Filter = new BrightIdeasSoftware.TextMatchFilter(
                    this.pageTreeListView, this.searchTextBox.Text, StringComparison.InvariantCultureIgnoreCase);
                this.UpdateFilterHighlight();
            }
            else
            {
                //Remove the filter
                this.pageTreeListView.AdditionalFilter = null;

                //Before clearing the filter controls, reset theme!
                foreach (var pp in this.Pages)
                {
                    if (pp.FilterControls == null)
                        continue;

                    foreach (var reset in pp.FilterControls)
                        UITheme.SelectedTheme.Apply(reset);
                    pp.FilterControls = null;
                }
            }



            bool IsValidControl(Control ctrl) => !typeof(Panel).IsAssignableFrom(ctrl.GetType()) && !(ctrl is GroupBox) && !(ctrl is ListView);
        }

        /// <summary>
        /// Creates a new preferences dialog, giving input focus to the specified control.
        /// </summary>
        /// <param name="focus">Name of the control which will be focused after the form is displayed. Recursively searches all preference pages.</param>
        /// <param name="isPage">If set to <c>true</c>, <paramref name="focus"/> is the path to a <see cref="IPreferencePage"/>.</param>
        public Preferences(string focus, bool isPage) : this()
        {
            this._focus = focus;
            this._isPage = isPage;
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control.
        /// </summary>
        public string ActiveTheme { get; set; }

        /// <summary>
        /// Gets the list of preference page nodes to display in the treeview.
        /// </summary>
        public List<IPreferencePage> Nodes { get; private set; }

        //IMP: Preferences - delayed opening times -> don't create all preference page instances on open, but instead only when selecting the page
        //  -> how will be populate the list without creating the instances first, to get path/image/...? is moving InitializeComponent a good idea?
        private List<IPreferencePage> _defaultNodes = new List<IPreferencePage>
        {
            //Environment
            new GeneralPreferencePage(),
            new KeyboardPreferencePage(),
            new AssociationsPreferencePage(),
            new DialogsPreferencePage(),
            new ModulePreferencePage(),
            //Content
            new ContentPreferencePage(),
            new SavingPreferencePage(),
            //Editors
            new TextEditorPreferencePage(),
            //Experimental
            new ExperimentalPreferencePage()
        };

        /// <summary>
        /// Gets the list of preference pages to display.
        /// </summary>
        public IEnumerable<PreferencePage> Pages => this.Nodes.OfType<PreferencePage>();

        /// <summary>
        /// Gets or sets the active page to show.
        /// </summary>
        public IPreferencePage ActivePage
        {
            get => this.Nodes.FirstOrDefault(n => n.Path == this.ActivePagePath);
            set => this.ActivePagePath = value?.Path;
        }

        /// <summary>
        /// Gets or sets the active page by path.
        /// </summary>
        public string ActivePagePath
        {
            get => this._activePagePath;
            set
            {
                this._activePagePath = value;
                //FIXME: Preferences - if the user selects a folder *by themself*, do not snap to the target page!
                this.pageTreeListView.SelectedObject = this.Nodes.FirstOrDefault(n => n.Path == value);
                this.UpdateFilterHighlight();

                foreach (var p in this.Pages)
                {
                    p.Visible = (p.Path == value);
                    if (p.Path == value)
                        p.OnSelect(this, EventArgs.Empty);
                }
            }
        }
        private string _activePagePath;

        /// <summary>
        /// Called when a settings value has changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void SettingChange(object sender, PropertyChangedEventArgs e)
        {
            if (!this._isApplying)
            {
                //Update Apply button if we aren't currently applying
                this.applyButton.Enabled = true;
            }
        }

        private void Preferences_Load(object sender, EventArgs e)
        {
            //Load preference pages list
            this.ReloadPreferencePages();

            //Add all pages to the panel
            this.InitializePages();

            //Treelist setup
            this.pageNameColumn.ImageAspectName = "Image";
            this.pageTreeListView.ChildrenGetter = GetChildren;
            this.pageTreeListView.CanExpandGetter = CanExpand;
            this.pageTreeListView.ExpandAll();

            //Center of main window
            this.CenterToParent();

            //Call load
            foreach (var page in this.Pages)
                page.OnPreferencesLoad(this, EventArgs.Empty);

            //Update theme
            WindowManager.AfterCreateForm(this);



            IEnumerable GetChildren(object model)
            {
                var page = model as IPreferencePage;
                var path = page.Path;

                foreach (var node in this.Nodes.Where(n => n.Path != path && n.Path.StartsWith(path)))
                    yield return node;
            }

            bool CanExpand(object model)
            {
                var page = model as IPreferencePage;
                var path = page.Path;
                return this.Nodes.Any(n => n.Path != path && n.Path.StartsWith(path));
            }
        }

        private void InitializePages()
        {
            //HACK: Preferences - this entire page system, with IPrefPage, PrefPage AND PrefPageNode, is really messy and should probably be thought through better
            //  -> additionally, it currently does not allow for "folders" to have an image EASILY (since it CAN be added to the list in advance)
            this.pagePanel.SuspendLayout();
            this.pagePanel.Controls.Clear();
            foreach (var p in this.Pages.ToList())
            {
                //Add page to panel
                p.Dock = DockStyle.Fill;
                this.pagePanel.Controls.Add(p);
                
                //Add to list - Get all path bits
                var bits = p.Path.Split('/');

                //No bits?
                if (bits.Length == 1)
                {
                    //Since base level pages are never folders, simply make the existing page a root element
                    var i = this.Nodes.IndexOf(this.Nodes.FirstOrDefault(n => n.Path == p.Path));
                    this.Nodes[i].IsRoot = true;
                }
                else if (bits.Length == 0)
                {
                    //No nodes :(
                    throw new ArgumentOutOfRangeException(nameof(p.Path));
                }
                else
                {
                    //Check for if the subnodes already exist
                    var prefixed = bits[0];
                    for (var i = 1; i <= bits.Length; i++)
                    {
                        if (!this.Nodes.Any(n => n.Path == prefixed))
                        {
                            //Check if already the last node
                            if (i == bits.Length)
                            {
                                //Actual page, no need to add since it already is in the list
                                //nodes.Add(new PreferencePageNode(p.Path, p.Image, p.Path, false));
                            }
                            else
                            {
                                //Folder node
                                this.Nodes.Insert(this.Nodes.Count(n => n.IsRoot), new PreferencePageNode(prefixed, null, p.Path, i == 1));
                            }
                        }

                        if (i >= bits.Length)
                            break;

                        prefixed += "/" + bits[i];
                    }
                }
            }
            this.pagePanel.ResumeLayout();

            //Update listview
            this.pageTreeListView.SetObjects(this.Nodes.Where(n => n.IsRoot));
        }

        private void Preferences_Shown(object sender, EventArgs e)
        {
            //Update focus control
            if (!string.IsNullOrWhiteSpace(this._focus))
            {
                this.UpdateFocus();
            }
            else
            {
                //Select first page
                this.ActivePagePath = "Environment";

                //Focus the search bar
                this.searchTextBox.Select();
            }
        }

        /// <summary>
        /// For this call, <see cref="_focus"/> may not be <c>null</c>.
        /// </summary>
        internal void UpdateFocus()
        {
            PreferencePage page = null;
            Control focus = null;
            if (this._isPage)
            {
                //Select page with _focus for a path
                page = this.Nodes.FirstOrDefault(n => n.Path == this._focus) as PreferencePage;
            }
            else
            {
                //Open its page
                focus = ControlsHelper.GetControlWithName(this, this._focus);
                var parent = focus;
                while (parent != null)
                {
                    parent = parent.Parent;
                    if (parent is PreferencePage pp)
                    {
                        //Found page
                        page = pp;
                        break;
                    }
                }
            }
            this.ActivePage = page ?? throw new ArgumentException($"The specified control to focus \"{this._focus}\" has no PreferencePage assigned to it.");

            //Give input focus
            focus?.Select();
        }

        private void UpdateFilterHighlight()
        {
            var pp = this.ActivePage as PreferencePage;
            if (pp?.FilterControls == null)
                return;

            //Theme all controls that are neat
            foreach (var ctrl in ControlsHelper.RecurseControls(pp, false))
            {
                if (pp.FilterControls.Contains(ctrl))
                {
                    //Cool highlight
                    ctrl.ForeColor = UITheme.SelectedTheme.HighlightTextColor;
                    ctrl.BackColor = UITheme.SelectedTheme.HighlightBackColor;
                }
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            //Apply settings and close
            this.ApplySettings();
            this._saving = true;
            this.Close();
        }

        private void applyButton_Click(object sender, EventArgs e) => this.ApplySettings();

        /// <summary>
        /// Whether we are currently applying settings.
        /// </summary>
        private bool _isApplying = false;

        /// <summary>
        /// Applies the settings, saving them to disk.
        /// </summary>
        private async Task ApplySettings()
        {
            //Disable the apply button, and don't allow re-enable until we left
            this.applyButton.Enabled = false;
            this._isApplying = true;

            //Call preapply
            var applytasks = new List<Task>();
            foreach (var page in this.Pages)
                applytasks.Add(page.OnPreApplySettings(this, EventArgs.Empty));

            //Do actual save after all tasks are done
            try
            {
                await Task.WhenAll(applytasks);
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(ex, "Error on pre-apply settings.");
            }

            Properties.Settings.Default.Save();

            //Apply all delegated settings
            Settings.Default.UpdateAll();

            //Call postapply
            applytasks.Clear();
            foreach (var page in this.Pages)
                applytasks.Add(page.OnPostApplySettings(this, EventArgs.Empty));

            //Await all postapply tasks
            try
            {
                await Task.WhenAll(applytasks);
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(ex, "Error on post-apply settings.");
            }

            //We are done here
            this._isApplying = false;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            //Simply close
            this.Close();
        }

        private void Preferences_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Undo any settings changes
            if (!this._saving)
            {
                //BUG: Preferences - cancelling settings saving is critical as it is, since any settings which were changed
                //  UNRELATED to the preferences dialog ALSO get reset by this (for example, if a new dialog was remembered, it will be forgotten on cancel)
                //  similar situation: DISABLE status bar from settings -> save -> ENABLE status bar from main form -> open settings -> cancel -> status bar is DISABLED
                this.UndoSettings();
            }

            //Unsubscribe
            Settings.Default.PropertyChanged -= this.SettingChange;

            foreach (var module in Program.MainMM.LoadedModules.OfType<IModuleSettings>())
            {
                //Try adding event
                if (module.CustomSettings != null)
                    module.CustomSettings.PropertyChanged -= this.SettingChange;
            }

            Program.GC();
        }

        /// <summary>
        /// Undoes the settings changes, reloading from disk.
        /// </summary>
        private void UndoSettings()
        {
            //Reload from disk
            Settings.Default.Reload();

            //Update all again
            //UNTESTED: Preferences - is updateall really required if only pressing Apply updates the properties anyway?
            Settings.Default.UpdateAll();
        }

        private void resetAllButton_Click(object sender, EventArgs e)
        {
            var diag = new FallbackTaskDialog("By continuing, SFMTK will reset all settings to their default values. This cannot be undone!\n\n" +
                "This reset will include your current window layout, all setups and any custom themes.\n\nTo confirm, please select the checkbox below.",
                "Are you sure you want to reset all settings?", "Reset all settings?", FallbackDialogIcon.Warning,
                new[] { new FallbackDialogButton(DialogResult.OK), new FallbackDialogButton(DialogResult.Cancel) });
            diag.VerificationText = "I am fine with this";
            var res = diag.ShowDialogOrFallback();
            if (res.FallbackDialog || res.Result == DialogResult.OK)
            {
                if (diag.IsVerificationChecked)
                {
                    //Reset all settings!
                    Settings.ResetSettings();

                    //Save changes too
                    Settings.Default.Save();

                    //Notify user and restart to finish
                    var res2 = FallbackTaskDialog.ShowDialog("All settings have been reset.\nSFMTK will have to restart in order to complete the reset.", null, "Settings reset",
                        FallbackDialogIcon.Info, new FallbackDialogButton("Restart now", DialogResult.OK), new FallbackDialogButton("Later", DialogResult.Cancel));

                    if (res2 == DialogResult.OK)
                    {
                        //UNTESTED: Preferences - App.Restart on settings reset, does this force-close SFMTK? because of unsaved content!

                        //No need to reload settings here
                        this._saving = true;
                        Application.Restart();
                    }
                    else
                    {
                        //Apply default settings already
                        this._saving = false;
                        this.Close();
                    }
                }
                else
                {
                    //You're such a dum
                    FallbackTaskDialog.ShowDialog("Your settings were left alone since you did not seem to read through the dialog box in advance.", null, "Whew!",
                        FallbackDialogIcon.Info, new FallbackDialogButton("Whew", DialogResult.OK));
                }
            }
        }

        private void pageTreeListView_SelectionChanged(object sender, EventArgs e)
        {
            this.pageTreeListView.Expand(this.pageTreeListView.SelectedObject);
            this.ActivePagePath = (this.pageTreeListView.SelectedObject as IPreferencePage)?.TargetPage;
        }
    }
}
