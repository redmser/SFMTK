﻿using System;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    /// <summary>
    /// Error handling outside of default .NET's dialogs
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class ErrorReport : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorReport"/> class.
        /// </summary>
        public ErrorReport()
        {
            //Compiler-generated
            this.InitializeComponent();

            //Do not apply theme to avoid issues with the system
        }

        /// <summary>
        /// Displays an error dialog box with the specified message, and optionally info about the exception.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public static void Display(string message, Exception ex = null)
        {
            if (ex is NotImplementedException)
            {
                //Simpler dialog
                FallbackTaskDialog.ShowDialog("This feature is currently not implemented. We are sorry for the inconvenience.", null, "Not implemented",
                    FallbackDialogIcon.Info, new FallbackDialogButton(DialogResult.OK));
                return;
            }

            var diag = new ErrorReport();
            try
            {
                diag.errorMessage.AppendLine("Error Log Begin");
                diag.errorMessage.AppendLine($"Error Message: {message}");
                diag.errorMessage.AppendLine($"Exception: {ex.GetInfo()}");
                diag.errorMessage.AppendLine("Error Log End");
            }
            catch (Exception inner)
            {
                //As a safety measure in case the error handler above ever gets extended
                diag.errorMessage.Text = "Could not get any information! Internal crash: " + inner.ToString();
            }
            diag.ShowDialog();
        }

        private void errorMessage_Click(object sender, EventArgs e)
        {
            if (this.errorMessage.SelectionLength == 0)
                this.errorMessage.SelectAll();
        }

        private void buttonContinue_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonReport_Click(object sender, EventArgs e) => NetworkFallback.OpenInBrowser(Constants.GitLabURL + "/issues");

        private void buttonQuit_Click(object sender, EventArgs e)
        {
#if DEBUG
            if (true)
#else
            if (FallbackRememberDialog.ShowDialog("Please note that by quitting the application, any unsaved changes will be lost PERMANENTLY.\n\n" +
                            "Are you sure you wish to quit without saving?", "Quit without saving", "Quit without saving", FallbackRememberDialog.DontAskAgainText,
                            "Quit without saving on error", FallbackDialogIcon.Warning, new FallbackDialogButton(DialogResult.Yes),
                            new FallbackDialogButton(DialogResult.No)) == DialogResult.Yes)
#endif
            {
                //Harshest quit in your area
                Environment.Exit(670);
            }
        }

        private void restartButton_Click(object sender, EventArgs e)
        {
#if DEBUG
            if (true)
#else
            if (FallbackRememberDialog.ShowDialog("Please note that by restarting the application, any unsaved changes will be lost PERMANENTLY.\n\n" +
                            "Are you sure you wish to restart without saving?", "Restart without saving", "Restart without saving", FallbackRememberDialog.DontAskAgainText,
                            "Quit without saving on error", FallbackDialogIcon.Warning, new FallbackDialogButton(DialogResult.Yes),
                            new FallbackDialogButton(DialogResult.No)) == DialogResult.Yes)
#endif
            {
                //UNTESTED: ErrorReport restart application (in release mode - does it do a graceful shutdown at first?)
                Application.Restart();
            }
        }
    }
}
