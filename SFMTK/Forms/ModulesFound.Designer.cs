﻿namespace SFMTK.Forms
{
    partial class ModulesFound
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModulesFound));
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.loadButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.moduleVersionColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.modulesTreeListView = new SFMTK.Controls.EnhancedTreeListView();
            this.moduleNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.moduleFullNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.moduleAuthorColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.modulesTreeListView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(458, 56);
            this.label1.TabIndex = 0;
            this.label1.Text = "The following new module files have been found in your SFMTK directory, but are n" +
    "ot loaded.\r\n\r\nPlease select which modules you would like to load:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.loadButton, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cancelButton, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 296);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(458, 38);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // loadButton
            // 
            this.loadButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.loadButton.Location = new System.Drawing.Point(280, 6);
            this.loadButton.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(103, 28);
            this.loadButton.TabIndex = 0;
            this.loadButton.Text = "&Load modules";
            this.toolTip.SetToolTip(this.loadButton, "Loads the selected modules into SFMTK. Only load modules of trustworthy sources!");
            this.loadButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(383, 6);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 28);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // moduleVersionColumn
            // 
            this.moduleVersionColumn.DisplayIndex = 3;
            this.moduleVersionColumn.IsVisible = false;
            this.moduleVersionColumn.Text = "Version";
            this.moduleVersionColumn.Width = 80;
            // 
            // modulesTreeListView
            // 
            this.modulesTreeListView.AllColumns.Add(this.moduleNameColumn);
            this.modulesTreeListView.AllColumns.Add(this.moduleFullNameColumn);
            this.modulesTreeListView.AllColumns.Add(this.moduleVersionColumn);
            this.modulesTreeListView.AllColumns.Add(this.moduleAuthorColumn);
            this.modulesTreeListView.CellEditUseWholeCell = false;
            this.modulesTreeListView.CheckBoxes = true;
            this.modulesTreeListView.CheckedAspectName = "Load";
            this.modulesTreeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.moduleNameColumn,
            this.moduleFullNameColumn,
            this.moduleAuthorColumn});
            this.modulesTreeListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.modulesTreeListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modulesTreeListView.FullRowSelect = true;
            this.modulesTreeListView.HeaderCheckBoxUpdatesChildren = true;
            this.modulesTreeListView.HideSelection = false;
            this.modulesTreeListView.Location = new System.Drawing.Point(8, 64);
            this.modulesTreeListView.Name = "modulesTreeListView";
            this.modulesTreeListView.ShowGroups = false;
            this.modulesTreeListView.ShowImagesOnSubItems = true;
            this.modulesTreeListView.Size = new System.Drawing.Size(458, 232);
            this.modulesTreeListView.TabIndex = 1;
            this.modulesTreeListView.UseCompatibleStateImageBehavior = false;
            this.modulesTreeListView.UseFiltering = true;
            this.modulesTreeListView.View = System.Windows.Forms.View.Details;
            this.modulesTreeListView.VirtualMode = true;
            this.modulesTreeListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.modulesTreeListView_ItemChecked);
            this.modulesTreeListView.DoubleClick += new System.EventHandler(this.modulesTreeListView_DoubleClick);
            // 
            // moduleNameColumn
            // 
            this.moduleNameColumn.AspectName = "";
            this.moduleNameColumn.HeaderCheckBox = true;
            this.moduleNameColumn.HeaderCheckState = System.Windows.Forms.CheckState.Checked;
            this.moduleNameColumn.Text = "Module Name";
            this.moduleNameColumn.UseFiltering = false;
            this.moduleNameColumn.Width = 160;
            // 
            // moduleFullNameColumn
            // 
            this.moduleFullNameColumn.AspectName = "FullName";
            this.moduleFullNameColumn.Text = "Full Name";
            this.moduleFullNameColumn.UseFiltering = false;
            this.moduleFullNameColumn.Width = 190;
            // 
            // moduleAuthorColumn
            // 
            this.moduleAuthorColumn.Text = "Author";
            this.moduleAuthorColumn.Width = 90;
            // 
            // ModulesFound
            // 
            this.AcceptButton = this.loadButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(474, 342);
            this.Controls.Add(this.modulesTreeListView);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(287, 194);
            this.Name = "ModulesFound";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.Text = "New Modules Found";
            this.Load += new System.EventHandler(this.ModulesFound_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.modulesTreeListView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button cancelButton;
        private SFMTK.Controls.EnhancedTreeListView modulesTreeListView;
        private BrightIdeasSoftware.OLVColumn moduleNameColumn;
        private BrightIdeasSoftware.OLVColumn moduleFullNameColumn;
        private BrightIdeasSoftware.OLVColumn moduleAuthorColumn;
        private BrightIdeasSoftware.OLVColumn moduleVersionColumn;
    }
}