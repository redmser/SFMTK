﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Commands;
using SFMTK.Contents;
using SFMTK.Controls;
using SFMTK.Errors;
using SFMTK.Modules;
using SFMTK.Notifications;
using SFMTK.Properties;
using SFMTK.Tasks;
using SFMTK.Widgets;
using WeifenLuo.WinFormsUI.Docking;

namespace SFMTK.Forms
{
    /// <summary>
    /// Main application form. Opened as <see cref="Program.MainForm"/> if a GUI is expected to launch.
    /// </summary>
    /// <seealso cref="Form" />
    public partial class Main : Form, IThemeable
    {
        // Notes to anyone looking to extend the Main window:
        // - All menu items should have a TooltipText. It is used to display the status bar info.
        // - If you want a menu item to share a shortcut with a toolstrip item, give them the same name "prefix", changing only the type suffix as VS normally would.
        // - Dockable windows can be added in the Widgets namespace, creating a class inheriting from Widget. Use the DockPanelManager methods for working with them here.
        // - Any new actions/commands should use the command pattern, inheriting from either Command (or a fitting subclass) or using CommandGroups.

        //TODO: Main - replace ToolStripPanel with an actual container, allowing for toolstrips on any of the four borders
        //BUG: Main - hover text does not display for dynamically generated menu items as well as items from toolbars
        //TODO: Main - move all toolstrips as close to eachother as possible (to avoid those gaps on default layout - easily visible on non-default theme)
        //  -> default position doesn't seem to like being set. how can this easily be accomplished? what is the reason for odd ordering?
        //TODO: Main - either make it clear that the search toolstrip is only for the VDC, or also have a way to search for text in opened files alas Visual Studio's CTRL+F
        //  -> maybe include multiple search modes (e.g. also allow searching on the web, like on google, etc?)
        //TODO: Main - what should the dockPanelContextMenuStrip display?

        /// <summary>
        /// ErrorManager assigned to this form.
        /// </summary>
        public ErrorManager ErrorManager { get; }

        /// <summary>
        /// DockPanelManager assigned to the main dock panel of this form.
        /// </summary>
        public DockPanelManager DockPanelManager { get; }

        /// <summary>
        /// AutoSaveManager assigned to this form.
        /// </summary>
        public AutoSaveManager AutoSaveManager { get; }

        /// <summary>
        /// CommandHistory assigned to this form.
        /// </summary>
        public CommandHistory CommandHistory { get; }

        /// <summary>
        /// ModuleManager assigned to this form.
        /// </summary>
        public ModuleManager ModuleManager { get; }

        /// <summary>
        /// Gets the toolbars that are part of the main toolstrip panel.
        /// </summary>
        public IEnumerable<ToolStrip> ToolBars => this.mainToolStripPanel.Controls.OfType<ToolStrip>();

        /// <summary>
        /// Gets the toolbar with the specified friendly name.
        /// </summary>
        /// <param name="name">The friendly name of the toolbar, as specified in its Text property. This name is shown in multiple lists related to customizing the UI.</param>
        public ToolStrip GetToolBar(string name) => this.ToolBars.FirstOrDefault(t => t.Text.Replace("&", "") == name);

        /// <summary>
        /// Removes the specified ToolBar from the ToolStripPanel completely.
        /// </summary>
        internal void RemoveToolBar(ToolStrip toolstrip) => this.mainToolStripPanel.Controls.Remove(toolstrip);

        /// <summary>
        /// Removes the specified ToolBar type from the ToolStripPanel completely.
        /// </summary>
        internal void RemoveToolBar(Type type)
        {
            //UNTESTED: Main - Remove ToolBar by type
            foreach (var ts in this.mainToolStripPanel.Controls.OfType<ToolStrip>().Where(t => t.GetType() == type).ToList())
                this.RemoveToolBar(ts);
        }

        /// <summary>
        /// Gets the menu with the specified friendly name.
        /// </summary>
        /// <param name="name">The friendly name of the menubar, as specified in its Text property. This name is shown as the display text of the menu.</param>
        public ToolStripMenuItem GetMenu(string name) => this.mainMenuBar.Items.OfType<ToolStripMenuItem>().FirstOrDefault(m => m.Text.Replace("&", "") == name);

        /// <summary>
        /// Gets all currently displayed notifications.
        /// </summary>
        public NotificationCollection GetNotifications() => Settings.Default.Notifications;

        /// <summary>
        /// Adds a new notification to display.
        /// </summary>
        /// <param name="not">The notification to display.</param>
        public void AddNotification(Notification not) => Settings.Default.Notifications.Add(not);

        /// <summary>
        /// Gets the background tasks left to work on.
        /// </summary>
        public TaskQueue<BackgroundTask, ProgressValue, object> BackgroundTasks { get; } = new TaskQueue<BackgroundTask, ProgressValue, object>();

        /// <summary>
        /// Executes the specified action once the <see cref="Main"/> window is in focus (or instantly, if it already is focused).
        /// </summary>
        /// <param name="remindAction">The action to run once the window has focus.</param>
        public void RemindMe(Action remindAction)
        {
            //Are we already in focus?
            if (this.ContainsFocus)
            {
                //Instantly execute
                remindAction();
            }
            else
            {
                //Queue for later
                this._actionQueue.Add(remindAction);
            }
        }

        private List<Action> _actionQueue = new List<Action>();

        /// <summary>
        /// Gets or sets the theme currently active on this control.
        /// </summary>
        public string ActiveTheme
        {
            get => this._activeTheme;
            set
            {
                this._activeTheme = value;

                //Updated on theme change as a special control
                this.reopenMenuPanel.Size = new Size(UITheme.SelectedTheme.ReopenMenuSize, UITheme.SelectedTheme.ReopenMenuSize);
            }
        }
        private string _activeTheme;

        #region Initialization

        /// <summary>
        /// Initializes a new instance of the <see cref="Main"/> class.
        /// </summary>
        public Main()
        {
            //Compiler-generated
            this.InitializeComponent();

            //Init managers
            this.CommandHistory = new CommandHistory();
            this.AutoSaveManager = new AutoSaveManager();
            this.CommandManager.CommandHistory = this.CommandHistory;
            this.DockPanelManager = new DockPanelManager(this.mainDockPanel, this.CommandManager);
            this.ModuleManager = Program.MainMM;
            this.ErrorManager = new ErrorManager(this.DockPanelManager);

            //Events
            //IMP: Main - this kind of hardcoded junk (UpdateCommandsEnabled on ActiveDocumentChanged) is pretty ugly and should be substituted with a subclass or interface
            this.mainDockPanel.ActiveDocumentChanged += (s, e) => CommandManager.UpdateCommandsEnabled(nameof(Main), typeof(SaveFileCommand), typeof(SaveFileAsCommand), typeof(SaveAllFilesCommand), typeof(EditCommand), typeof(CloseTabCommand));
            this.CommandHistory.CanUndoRedoChanged += (s, e) => CommandManager.UpdateCommandsEnabled(nameof(Main), typeof(UndoCommand), typeof(RedoCommand));
            this.CommandHistory.CommandHistoryChanged += this.UpdateUndoRedoText;
            this.BackgroundTasks.QueueUpdated += this.UpdateBackgroundTaskDisplay;

            //Status strip
            this.mainStatusStrip.Entries.Add(this._hoverEntry);
        }

        private NotificationsDynamicToolStripItem _dynamicNotificationsItem;

        protected override void OnLoad(EventArgs e)
        {
            //Load modules - has to be outside the constructor!
            this.ModuleManager.ReachedStartupStep(ModuleLoadingContext.StartupUserInterface);

            //NYI: Main - respect OnStartup setting (open last session's files at the very end. script should have multiple methods which get called depending
            //  on how far loading is (one before everything else, one after window is first responsive, and one as a loading task to allow for async initialization)
            //  -> this should also be used for custom modules, to allow initializing at different locations

            //Setup for toolstrips
            this.InitializeToolStrips();

            //Setup for menustrips
            foreach (var item in this.mainMenuBar.Items.OfType<ToolStripMenuItem>())
            {
                this.AddStatusHoverInfo(item);
            }

            //Apply command properties
            this.CommandManager.ApplyCommand();
            this.UpdateCommandsEnabled();

            //HACK: Main - how to have command-independent images and text??
            this.modToolStripMenuItem.Text = "&Mod...";
            this.modToolStripMenuItem.Image = Resources.brick;

            //Autosaving
            this._autosaveTimer.Elapsed += (s, a) => this.AutoSaveManager.Autosave(this.DockPanelManager.OpenContent);
            this.UpdateAutosave();

            //Load settings
            this.LoadSettings(true);

            //Dynamic menu items
            this.applyLayoutToolStripMenuItem.DropDownItems.Add(new LayoutsDynamicToolStripItem());
            this.toolstripsContextMenuStrip.Items.Insert(0, new ToolbarsDynamicToolStripItem());
            this.launchSFMToolStripMenuItem.DropDownItems.Add(new SetupsDynamicToolStripItem(launch: true));
            this.applySetupToolStripMenuItem.DropDownItems.Add(new SetupsDynamicToolStripItem(launch: false));
            this._dynamicNotificationsItem = new NotificationsDynamicToolStripItem();
            this.notificationsToolStripSplitButton.DropDownItems.Add(this._dynamicNotificationsItem);
            this.notificationsToolStripMenuItem.DropDown = this.notificationsToolStripSplitButton.DropDown;
            this.newToolStripButton.DropDownItems.Add(new ContentsDynamicToolStripItem());
            this.newToolStripButton.DropDown.ShowItemToolTips = false;

            //Databindings
            //BUG: Main - setup combobox does not work well with data sources and data binding AT ALL
            //  -> use format/parse/bindingcompleted events to help out here
            this.setupToolStripComboBox.ComboBox.DisplayMember = "Name";
            this.setupToolStripComboBox.ComboBox.ValueMember = "Name";
            this.setupToolStripComboBox.ComboBox.DataSource = Settings.Default.Setups;
            this.setupToolStripComboBox.ComboBox.DataBindings.Add("SelectedItem", Settings.Default, "ActiveSetup", false, DataSourceUpdateMode.OnPropertyChanged);
            this.setupToolStripComboBox.ComboBox.BindingContext = this.BindingContext;

            //Late-bound event (ensures no null values)
            Settings.Default.Notifications.CollectionChanged += this.Notifications_CollectionChanged;

            //The form has now loaded
            base.OnLoad(e);
        }

        protected override void OnShown(EventArgs e)
        {
            //Show the context menu once. By doing this, a bug is avoided where the context menu (when opened the first time)
            //appears elsewhere than expected. This is done here, but instantly hidden so no one sees anything. Seems to be a WinForms-level bug.
            this.toolstripsContextMenuStrip.Show(Point.Empty);
            this.toolstripsContextMenuStrip.Hide();

            //Initial status bar
            this.mainStatusStrip.Entries.Add(this._activeTaskEntry);

            //Form loaded!
            WindowManager.AfterCreateMain();

            //The form is now shown
            base.OnShown(e);

            //Update UI-related things now
            this.UpdateTitle();
            this.UpdateNotifications();

            //Loading phase done
            Cursor.Current = Cursors.Default;
        }

        #endregion //Initialization

        /// <summary>
        /// Loads settings from disk.
        /// </summary>
        /// <param name="initial">Whether this LoadSettings call is used to initialize the Main form.</param>
        public void LoadSettings(bool initial)
        {
            try
            {
                //Since this is already done before showing the main dialog, no need to instantly do it again!
                if (!initial)
                {
                    //Upgrade settings if first startup (since new settings are generated for the new program instance)
                    Settings.UpgradeSettings();

                    //Dum defaults
                    Settings.SetDefaultSettings();
                }

                //Async loading
                System.Threading.Tasks.Task.Run(() => Settings.LoadSettingsAsync(initial));

                //NYI: Main - move mod crawling of ContentBrowser to the initial loading: use NTFS Streams (with MFT) reading - if drive in question isn't NTFS,
                //  use regular IO fallback (turn current ModCrawler into this fallback, and create a second variant for MFT)... first load cached version
                //  as a base (serialized ContentTree - this should be an option, as space is BIGGO when compared to performance of MFT reading),
                //  then add/remove any files that mismatch, finally check types for unknown files and optionally calculate dependencies of all files.
                //  -> to simplify these selections for the user, have them set an option on First Launch that states whether you have much space or much memory
                //  (automatically detect default "ratio" value for this)

                //Window State
                if (initial && !Settings.Default.FirstStartup)
                {
                    //Load toolstrip setup
                    ToolStripManager.LoadSettings(this, DockLayout.LastLayoutName);
                }
                else if (Settings.Default.FirstStartup)
                {
                    //Construct a default layout which makes sense
                    this.CreateDefaultLayout();

                    //Center UI since we didn't have a startup position
                    this.CenterToScreen();
                }

                if (!initial)
                {
                    //Close all widgets and reopen afterwards to allow applying theme!
                    this.DockPanelManager.TemporaryCloseContent();
                }

                //Apply dockpanel theme early to avoid doing so later with content
                UITheme.SelectedTheme.ApplyControlTheme(this, this.mainDockPanel);

                //Docking
                if (File.Exists(DockLayout.LastLayoutFile) && !Settings.Default.FirstStartup)
                {
                    //Load and update Widgets list
                    DockLayout.LoadXML(this.DockPanelManager, DockLayout.LastLayoutFile, "Last Layout");

                    //Apply theme to all
                    if (!initial)
                    {
                        foreach (DockContent panel in this.mainDockPanel.Contents)
                        {
                            WindowManager.AfterCreateWidget(panel);
                        }
                    }
                }

                //Reopen content
                if (!initial)
                {
                    this.DockPanelManager.TemporaryOpenContent();
                }

                //Finally, save settings to ensure changes are stored
                //FIXME: Main - do we need to save settings after loading them? when is not having this an issue??
                this.SaveSettings(false);
            }
            catch (Exception ex)
            {
                ErrorReport.Display("Unable to load application settings successfully.", ex);
            }
        }

        /// <summary>
        /// Initializes all initially-generated toolstrip items.
        /// </summary>
        private void InitializeToolStrips()
        {
            //Main work done here
            this.InitializeToolBars(false);

            //Add debug option if wanted
#if DEBUG
            var adddebug = true;
#else
            var adddebug = Program.Options.DebugMode;
#endif
            if (adddebug)
            {
                //Add debug option to UI
                this.toolsToolStripMenuItem.DropDownItems.Insert(0, new ToolStripSeparator());
                this.toolsToolStripMenuItem.DropDownItems.Insert(0,
                    new ToolStripMenuItem("&Debug", Resources.flag_yellow, (s, e) => new DebugUI().Show(this))
                        { ToolTipText = "Opens the debugging window, which can be used to test some UI features." });
            }
        }

        /// <summary>
        /// (Re)initializes all toolbars to the toolstripsContextMenuStrip.
        /// </summary>
        /// <param name="applyTheme">If set to <c>true</c>, applies the current theme to the newly added toolbars.
        /// Only required if this call happens after the main form is already initialized.</param>
        internal void InitializeToolBars(bool applyTheme)
        {
            //BUG: Main - late-init of toolbars does not work (Help toolstrip does not show up in list - do we work with interface?)
            foreach (var toolstrip in this.Controls.OfType<ToolStrip>().Concat(this.mainToolStripPanel.Controls.OfType<ToolStrip>()).OrderBy(c => c.Name))
            {
                if (toolstrip is MenuStrip || toolstrip is StatusStrip)
                    continue;

                this.InitializeToolStrip(toolstrip);
            }

            //Form loaded to panel
            if (applyTheme)
                UITheme.ApplyTheme(this.mainToolStripPanel);
        }

        /// <summary>
        /// Initializes the specified ToolStrip.
        /// </summary>
        private void InitializeToolStrip(ToolStrip toolstrip)
        {
            //Add any regular toolstrip
            //TODO: Main - set position of the added toolstrip, to avoid weird order on default layout
            var type = toolstrip.GetType();

            if (type != typeof(StatusStripEx) && type != typeof(MenuStrip))
                this.mainToolStripPanel.Controls.Add(toolstrip);

            toolstrip.ContextMenuStrip = this.toolstripsContextMenuStrip;

            //Add overflow items
            var hide = new ToolStripMenuItem("&Hide Toolbar", null, (s, e) => toolstrip.Hide());
            hide.ToolTipText = "Hides the " + toolstrip.Text + " toolbar";
            hide.Overflow = ToolStripItemOverflow.Always;
            toolstrip.Items.Add(hide);

            //toolstrip.Items.Add(new ToolStripSeparator { Overflow = ToolStripItemOverflow.Always });

            var overflow = new ToolStripMenuItem("&Customize...", null, (s, e) => new CustomizeToolstrips(toolstrip.Name).Show(this));
            overflow.ToolTipText = "Customize the appearance of the " + toolstrip.Text + " toolbar";
            overflow.Overflow = ToolStripItemOverflow.Always;
            toolstrip.Items.Add(overflow);

            //TODO: Main - allow customizing tool strip overflow - possibly make it an option to disable entirely
            //TODO: Main - make overflow menu look like an actual dropdown menu, since right now it formats more like a toolstrip (horizontal)
        }

        /// <summary>
        /// Saves settings to disk.
        /// </summary>
        /// <param name="exiting">If set to true, closes any hidden or unsaved documents, in order to not save them in the layout. 
        /// <para />When set to false, also does not save the LastLayout file!</param>
        public void SaveSettings(bool exiting)
        {
            try
            {
                //Save settings
                ToolStripManager.SaveSettings(this, DockLayout.LastLayoutName);

                //Save window state of main form (as we control the settings save)
                if (exiting)
                    WindowManager.SaveCoords(this);

                //Save config
                Settings.Default.Save();

                if (exiting)
                {
                    //Remove hidden and unsaved dock contents
                    var rmList = this.DockPanelManager.DockContents.Where(dc => dc.IsHidden || (dc is ContentInfoWidget cw && cw.Content.IsDirty)).ToList();
                    for (var i = rmList.Count - 1; i >= 0; i--)
                    {
                        rmList[i].Dispose();
                    }

                    //Save DockPanel layout
                    this.mainDockPanel.SaveAsXml(DockLayout.LastLayoutFile);
                }
            }
            catch (Exception ex)
            {
                ErrorReport.Display("Unable to save application settings successfully.", ex);
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            //TODO: Main - include some way to mark content how it should behave on close (default: ask for save if not saved.
            //  additionally: auto-save on close, auto-reopen next time (no matter the settings), allow closing in "unsaved" state, reopen in that state)

            //Check for unsaved content
            if (this.DockPanelManager.UnsavedContents.Any())
            {
                if (Settings.Default.SimpleSaveConfirmation)
                {
                    //Simple save confirmation on each content widget
                    foreach (var ciw in this.DockPanelManager.UnsavedContents)
                    {
                        //Show content tab
                        this.DockPanelManager.Show(ciw);

                        //Prompt user
                        var cancel = ciw.Content.ShowSavePrompt() == DialogResult.Cancel;
                        if (cancel)
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }
                else
                {
                    //"Advanced" save dialog
                    var unsaved = new Unsaved(this.DockPanelManager);
                    var res = unsaved.ShowDialog();
                    if (res == DialogResult.Cancel)
                    {
                        //Cancel quit procedure
                        e.Cancel = true;
                        return;
                    }
                }
            }

            //Fire events
            base.OnFormClosing(e);
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            //Base call first!
            base.OnFormClosed(e);

            //Save settings on quit
            this.SaveSettings(true);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //Dispose of components
                this.components?.Dispose();

                //Unload modules
                this.ModuleManager.UnloadModules(ModuleUnloadingContext.Shutdown);
                //TODO: Main - should the closing procedure be cancelled if a module didn't want to unload? maybe prompt user??
                //  -> change how FreeResources is handled in Program.cs then, as it is called after Application.Run!
            }
            base.Dispose(disposing);
        }

        private void UpdateReopenMenuPanel() => this.reopenMenuPanel.Visible = !this.mainMenuBar.Visible;

        public void UpdateAutosave()
        {
            //Convert properties value (minutes) to milliseconds interval
            this._autosaveTimer.Interval = (double)Settings.Default.AutosaveInterval * 60000;
            this._autosaveTimer.Enabled = Settings.Default.Autosaving;
        }

        /// <summary>
        /// Updates the undo/redo menu and tooltip text.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void UpdateUndoRedoText(object sender, EventArgs e)
        {
            //FIXME: Main - set undo/redo text using command's displaytext instead (which can be gotten dynamically)
            //  -> since Text of the menu items etc is only set when created, add a system to Commands to update Text (and similar) together with Enabled

            this.undoToolStripButton.ToolTipText = this.CommandHistory.UndoDisplayText;
            this.undoToolStripMenuItem.Text = this.CommandHistory.UndoDisplayText;

            this.redoToolStripButton.ToolTipText = this.CommandHistory.RedoDisplayText;
            this.redoToolStripMenuItem.Text = this.CommandHistory.RedoDisplayText;
        }

        /// <summary>
        /// Opens the general preferences dialog (non-modal) with default input focus.
        /// </summary>
        public void OpenSettings() => this.OpenSettings(null, false);

        /// <summary>
        /// Opens the general preferences dialog (non-modal) with input focus on the specified control (by name or preference page).
        /// </summary>
        public void OpenSettings(string focus, bool isPage)
        {
            if (this.PreferencesDialog == null)
            {
                //Open settings as accessible variable
                this.PreferencesDialog = new Preferences(focus, isPage);
                this.PreferencesDialog.FormClosed += (s, e) => this.PreferencesDialog = null;
                this.PreferencesDialog.Show(this);
            }
            else
            {
                //Give focus to open preferences dialog
                this.PreferencesDialog._focus = focus;
                this.PreferencesDialog._isPage = isPage;
                if (!string.IsNullOrWhiteSpace(this.PreferencesDialog._focus))
                    this.PreferencesDialog.UpdateFocus();
                this.PreferencesDialog.Activate();
            }
        }

        /// <summary>
        /// Gets the currently open preferences dialog, or <c>null</c> if none is currently open.
        /// </summary>
        public Preferences PreferencesDialog { get; private set; }

        /// <summary>
        /// Updates the enabled state of all user elements in the main window (toolstrips and menuitems) depending on the respective commands.
        /// </summary>
        public void UpdateCommandsEnabled()
        {
            foreach (var menuitem in this.mainMenuBar.Items.OfType<ToolStripMenuItem>())
            {
                CommandManager.UpdateCommandsEnabled(nameof(Main), menuitem, false);
            }

            foreach (var toolstrip in this.ToolBars)
            {
                CommandManager.UpdateCommandsEnabled(nameof(Main), toolstrip, false);
            }
        }

        private void CreateDefaultLayout()
        {
            var mm = new ModManagerWidget();
            mm.Show(this.mainDockPanel, DockState.DockRight);
            new ErrorListWidget().Show(mm.Pane, DockAlignment.Bottom, 0.3);
        }

        internal ToolStripMenuItem GetLayoutMenuItem(int index)
        {
            if (DockLayout.Layouts.Count == 0)
                return null;
            return (ToolStripMenuItem)this.applyLayoutToolStripMenuItem.DropDownItems[index];
        }

        /// <summary>
        /// Update the main form title based on various settings and the program state.
        /// </summary>
        public void UpdateTitle()
        {
            var info = new List<string> { "SFMTK" };

            if (!Settings.Default.HideTitleName)
            {
                //Add active content name to title (if any)
                var content = this.DockPanelManager.ActiveContent?.Content?.GetFormattedName(Settings.Default.FullTitleFormat, true);
                if (content != null)
                    info.Add(content);
            }

            if (Settings.Default.ShowSetup)
                //Show active setup name in title
                info.Add(Settings.Default.ActiveSetup + " Setup");

            if (Settings.Default.ShowVersion)
                //Show version in title
                info.Add("v" + Program.VersionString);

            this.Text = string.Join(" - ", info);
        }
        
        /// <summary>
        /// Adds the specified ToolStripMenuItem to the list of items which should show hover information in the status bar.
        /// </summary>
        public void AddStatusHoverInfo(ToolStripMenuItem item)
        {
            if (item == null)
                return;

            //Update status text to display
            item.DropDown.ShowItemToolTips = false;
            item.MouseEnter -= this.ShowStatusHoverInfo;
            item.MouseEnter += this.ShowStatusHoverInfo;
            item.DropDownClosed -= this.ResetStatusHoverInfo;
            item.DropDownClosed += this.ResetStatusHoverInfo;
            item.DropDownItemClicked -= this.ResetStatusHoverInfo;
            item.DropDownItemClicked += this.ResetStatusHoverInfo;

            foreach (ToolStripItem inner in item.DropDownItems)
            {
                this.AddStatusHoverInfo(inner as ToolStripMenuItem);
            }
        }

        private StatusStripEntry _hoverEntry = new StatusStripEntry(null, -5);
        private void ShowStatusHoverInfo(object sender, EventArgs e) => this._hoverEntry.Text = ((ToolStripMenuItem)sender).ToolTipText;
        private void ResetStatusHoverInfo(object sender, EventArgs e) => this._hoverEntry.Text = null;

        private readonly System.Timers.Timer _autosaveTimer = new System.Timers.Timer();

        #region Event Handlers

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Activated" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            //Run through action queue
            var amt = this._actionQueue.Count;
            for (var i = 0; i < amt; i++)
            {
                var action = this._actionQueue[0];
                this._actionQueue.RemoveAt(0);
                action();
            }
        }

        private void mainDockPanel_ActiveContentChanged(object sender, EventArgs e) => this.UpdateTitle();

        private void manageLayoutsToolStripMenuItem_Click(object sender, EventArgs e) => new Layouts().Show(this);

        private void windowToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            //Only show windowToolStripSeparator if there actually are any windows opened
            //This separates the permanent menu items from the generated list of open windows
            this.windowToolStripSeparator.Visible = this.windowToolStripMenuItem.DropDownItems.OfType<ToolStripItem>().Last() != this.windowToolStripSeparator;
        }

        private void mainMenuBar_VisibleChanged(object sender, EventArgs e) => this.UpdateReopenMenuPanel();

        private void mainDockPanel_DragDrop(object sender, DragEventArgs e)
        {
            //Only gets called if effect isn't None, meaning the dragdrop will have valid data

            //Check which kind of data is dropped
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                //Same as doing "open file" for the specified files
                var files = (string[])e.Data.GetData(DataFormats.FileDrop);
                Content.OpenFile(files, null, true, ContentDetectMode.AutoDetect);
            }
            else if (e.Data.GetDataPresent(DataFormats.Text))
            {
                //Try to parse string as file contents
                var text = e.Data.GetData(DataFormats.Text).ToString();
                var type = Content.DetectFileType(null, new StringReader(text));
                var askedcontent = false;
                if (type == null)
                {
                    //Ask user which type the content is
                    askedcontent = true;
                    type = AskContentType();

                    //Cancel
                    if (type == null)
                        return;
                }

                //Add content widget
                var fake = Path.GetTempFileName();
                File.WriteAllText(fake, text);
                var content = Content.NewContentOfType(fake, true, type);

                //Is a fake file since we got the data without a real reference file
                content.FakeFile = true;
                this.DockPanelManager.AddContent(content, true);
            }



            Type AskContentType()
            {
                //NYI: Main - ask user about which content type the file to create is
                return null;
            }
        }

        private void mainDockPanel_DragOver(object sender, DragEventArgs e)
        {
            //Set effect if dragged data is file or text, disallowing other types of content
            //There currently is no need for differenciating between the different types of drag and drop actions
            var valid = e.Data.GetDataPresent(DataFormats.FileDrop) || e.Data.GetDataPresent(DataFormats.Text);
            e.Effect = valid ? DragDropEffects.Move : DragDropEffects.None;
        }

        private void reopenMenuPanel_Paint(object sender, PaintEventArgs e)
        {
            //Draw the "hidden menu reopen button"
            e.Graphics.FillPolygon(UITheme.SelectedTheme?.TextBrush ?? Brushes.Cyan, new[] { Point.Empty, new Point(0, e.ClipRectangle.Bottom), new Point(e.ClipRectangle.Right, 0) });
        }

        private void reopenMenuPanel_Click(object sender, EventArgs e)
        {
            //Show the menu again, hide the reopen button
            this.mainMenuBar.Show();
        }

        private void fileToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            //Determine New subentries
            if (Settings.Default.NewMenu)
            {
                //Submenu
                this.newMenuToolStripMenuItem.Text = "&New";
                this.newMenuToolStripMenuItem.ShortcutKeys = Keys.None;
                this.newMenuToolStripMenuItem.DropDown = this.newToolStripButton.DropDown;
            }
            else
            {
                //Simple entry
                this.newMenuToolStripMenuItem.Text = "&New...";
                this.newMenuToolStripMenuItem.ShortcutKeys = this.newToolStripMenuItem.ShortcutKeys;
                this.newMenuToolStripMenuItem.ShortcutKeyDisplayString = CommandManager.StringFromKeys(this.newToolStripMenuItem.ShortcutKeys);
                this.newMenuToolStripMenuItem.DropDown = null;
            }
        }

        private void Settings_ActiveSetupChanged(object sender, EventArgs e)
        {
            //NYI: Main - update the ActiveSetup property to be rerouted, so it changing can be detected in various locations
            //  -> title of Main, preferences setup list(s), etc

            //Update window title
            this.UpdateTitle();

            //Update combobox selection
            this.setupToolStripComboBox.Text = Settings.Default.ActiveSetup;
        }

        /// <summary>
        /// Handles the TextChanged event of the mainStatusStrip control, updating the main status label.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void mainStatusStrip_DisplayTextChanged(object sender, EventArgs e) => this.mainToolStripStatusLabel.Text = this.mainStatusStrip.DisplayText;

        #endregion //Event Handlers

        #region Notifications

        private void cancelToolStripSplitButton_Click(object sender, EventArgs e)
        {
            lock (this.BackgroundTasks.ActiveTaskLock)
            {
                //Cancel the currently active task
                if (this.BackgroundTasks.ActiveTask != null)
                {
                    this.BackgroundTasks.ActiveTask.Cancel();
                }
                else
                {
                    //Whoops! Let's better update the UI
                    this.UpdateBackgroundTaskDisplay(null, EventArgs.Empty);
                }
            }
        }

        private void UpdateBackgroundTaskDisplay(object sender, EventArgs e)
        {
            lock (this.BackgroundTasks.ActiveTaskLock)
            {
                try
                {
                    this.InvokeIfRequired(() =>
                    {
                        if (this.BackgroundTasks.ActiveTask == null)
                        {
                            //No active task
                            this.cancelToolStripSplitButton.Visible = false;
                            this.mainToolStripProgressBar.Visible = false;
                            this.mainToolStripProgressBar.Style = ProgressBarStyle.Blocks;
                            this._activeTaskEntry.Text = null;
                        }
                        else
                        {
                            //An active task, display info about it
                            this.cancelToolStripSplitButton.Visible = this.BackgroundTasks.ActiveTask.CanCancel;
                            this.mainToolStripProgressBar.Visible = this.BackgroundTasks.ActiveTask.CanReportProgress || this.mainToolStripProgressBar.Style == ProgressBarStyle.Marquee;
                            this._activeTaskEntry.Text = this.BackgroundTasks.ActiveTask.Name;
                        }
                    });
                }
                catch (InvalidOperationException)
                {
                    //We might be updating the UI too early/late for WinForms to handle
                }
            }
        }

        internal StatusStripEntry _activeTaskEntry = new StatusStripEntry(null, 0, StatusStripState.Loading);

        /// <summary>
        /// Gets the notification image for the currently pending notifications.
        /// </summary>
        private Image GetNotificationIcon()
        {
            //Get fitting notification image
            if (Settings.Default.Notifications.Any())
            {
                //Check for highest priority
                return Settings.Default.Notifications.MaxOf(n => n.Priority).Icon;
            }

            //Icon for no notification
            return Resources.flag_gray;
        }

        /// <summary>
        /// Called when the list of notifications has changed.
        /// </summary>
        public void UpdateNotifications()
        {
            this.notificationsToolStripSplitButton.ToolTipText = Settings.Default.Notifications.Any() ?
                StringHelper.GetPlural(Settings.Default.Notifications.Count, "notification") : "No new notifications";
            this.notificationsToolStripMenuItem.Text = $"&Notifications ({Settings.Default.Notifications.Count})";

            var icon = this.GetNotificationIcon();
            this.notificationsToolStripSplitButton.Image = icon;
            this.notificationsToolStripMenuItem.Image = icon;

            this._dynamicNotificationsItem.ReloadDynamicItems();
        }

        private void Notifications_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) => this.UpdateNotifications();

        private void notificationsToolStripSplitButton_ButtonClick(object sender, EventArgs e)
        {
            var not = Settings.Default.Notifications.MaxOf(n => n.Priority);
            if (not == null)
                return;

            //"Click" on the highest priority notification
            var remove = not.OnClick();
            if (remove)
                Settings.Default.Notifications.Remove(not);
        }

        #endregion //Notifications
    }
}
