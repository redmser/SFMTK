﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using SFMTK.Controls;
using SFMTK.Properties;

namespace SFMTK.Forms
{
    /// <summary>
    /// Dialog for managing all setups.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Setups : Form, IThemeable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Setups"/> class.
        /// </summary>
        public Setups()
        {
            //Compiler-generated
            InitializeComponent();

            //Add setups
            this.setupsObjectListView.AddObjects(Settings.Default.Setups);
            this.setupsObjectListView.DropSink = new RearrangingDropSinkEx(false);
            this.nameOlvColumn.ImageGetter = (o) => (((Setup)o).Name == Settings.Default.ActiveSetup) ? Resources.tick : null;

            //TODO: Setups - context menu for more operations (apply, move up, move down, rename, delete)

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        private void Setups_Load(object sender, EventArgs e)
        {
            this.CenterToParent();
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control.
        /// </summary>
        public string ActiveTheme { get; set; }

        private void helpButton_Click(object sender, EventArgs e)
        {
            //NYI: Setups - open wiki page on gitlab about setups (explain what they are, why you want to use them, etc)
            //  -> use HelpCommand
            throw new NotImplementedException();
        }

        private void newSetupButton_Click(object sender, EventArgs e)
        {
            //IMP: Setup - try to have setup system similar to custom theme system: all setups base off of the
            //  Default setup (you can change default RAW), and mimic non-explicit changes of the setup that this setup is cloned from
            //  -> how to tell the user this? rename button to clone?
            var setup = new Setup();
            var edit = new EditSetup(setup);
            var res = edit.ShowDialog();
            if (res == DialogResult.OK)
            {
                Settings.Default.Setups.Add(setup);
                this.setupsObjectListView.AddObject(setup);
            } //TODO: Setups - have option to make it the active setup (in editsetup dialog), then update checkbox here
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            new EditSetup((Setup)this.setupsObjectListView.SelectedObject).ShowDialog();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            //NYI: Setups - delete the selected setup (what if it's default? warn user? fallback?)
            throw new NotImplementedException();
        }

        private void setupsObjectListView_ModelDropped(object sender, BrightIdeasSoftware.ModelDropEventArgs e)
        {
            //HACK: Setups - select model object with a delay since directly doing so does nothing (is the object added after the event?)
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(50);
                this.Invoke(new Action(() => this.setupsObjectListView.SelectedObject = e.SourceModels[0]));
            });
        }

        private void setupsObjectListView_DoubleClick(object sender, EventArgs e)
        {
            //Apply selected setup
            ((Setup)this.setupsObjectListView.SelectedObject).Apply();
        }

        private void setupsObjectListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e) => this.UpdateButtonStates();

        private void buttonMoveUp_Click(object sender, System.EventArgs e)
        {
            var obj = this.setupsObjectListView.SelectedObject;

            if (obj == null)
                return;

            var index = this.setupsObjectListView.IndexOf(obj);

            this.setupsObjectListView.MoveObjects(index - 1, new[] { obj });
            this.setupsObjectListView.SelectedIndex = index - 1;
        }

        private void buttonMoveDown_Click(object sender, System.EventArgs e)
        {
            var obj = this.setupsObjectListView.SelectedObject;

            if (obj == null)
                return;

            var index = this.setupsObjectListView.IndexOf(obj);

            this.setupsObjectListView.MoveObjects(index + 2, new[] { obj });
            this.setupsObjectListView.SelectedIndex = index + 1;
        }

        private void UpdateButtonStates()
        {
            var hasSelection = this.setupsObjectListView.SelectedObject != null;

            this.buttonDelete.Enabled = hasSelection;
            this.buttonEdit.Enabled = hasSelection;
            this.buttonMoveUp.Enabled = hasSelection && this.setupsObjectListView.SelectedIndex > 0;
            this.buttonMoveDown.Enabled = hasSelection && this.setupsObjectListView.SelectedIndex < this.setupsObjectListView.Items.Count - 1;
        }
    }
}
