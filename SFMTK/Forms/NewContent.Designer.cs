﻿namespace SFMTK.Forms
{
    partial class NewContent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewContent));
            this.bottomTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.typesObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.typeNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.settingsFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.invisibleControl1 = new SFMTK.Controls.InvisibleControl();
            this.bottomTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.typesObjectListView)).BeginInit();
            this.settingsFlowLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomTableLayoutPanel
            // 
            this.bottomTableLayoutPanel.ColumnCount = 3;
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.bottomTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.bottomTableLayoutPanel.Controls.Add(this.okButton, 1, 0);
            this.bottomTableLayoutPanel.Controls.Add(this.cancelButton, 2, 0);
            this.bottomTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomTableLayoutPanel.Location = new System.Drawing.Point(6, 430);
            this.bottomTableLayoutPanel.Name = "bottomTableLayoutPanel";
            this.bottomTableLayoutPanel.RowCount = 1;
            this.bottomTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.bottomTableLayoutPanel.Size = new System.Drawing.Size(547, 26);
            this.bottomTableLayoutPanel.TabIndex = 1;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.okButton.Location = new System.Drawing.Point(391, 1);
            this.okButton.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 24);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cancelButton.Location = new System.Drawing.Point(470, 1);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 24);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // typesObjectListView
            // 
            this.typesObjectListView.AllColumns.Add(this.typeNameColumn);
            this.typesObjectListView.CellEditUseWholeCell = false;
            this.typesObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.typeNameColumn});
            this.typesObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.typesObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.typesObjectListView.FullRowSelect = true;
            this.typesObjectListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.typesObjectListView.HideSelection = false;
            this.typesObjectListView.Location = new System.Drawing.Point(6, 6);
            this.typesObjectListView.MultiSelect = false;
            this.typesObjectListView.Name = "typesObjectListView";
            this.typesObjectListView.RowHeight = 32;
            this.typesObjectListView.ShowGroups = false;
            this.typesObjectListView.Size = new System.Drawing.Size(547, 424);
            this.typesObjectListView.TabIndex = 2;
            this.typesObjectListView.UseCompatibleStateImageBehavior = false;
            this.typesObjectListView.View = System.Windows.Forms.View.Details;
            this.typesObjectListView.SelectionChanged += new System.EventHandler(this.typesObjectListView_SelectionChanged);
            this.typesObjectListView.DoubleClick += new System.EventHandler(this.typesObjectListView_DoubleClick);
            // 
            // typeNameColumn
            // 
            this.typeNameColumn.AspectName = "";
            this.typeNameColumn.FillsFreeSpace = true;
            this.typeNameColumn.ImageAspectName = "Image";
            this.typeNameColumn.Text = "Name";
            // 
            // settingsFlowLayoutPanel
            // 
            this.settingsFlowLayoutPanel.AutoSize = true;
            this.settingsFlowLayoutPanel.Controls.Add(this.invisibleControl1);
            this.settingsFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.settingsFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.settingsFlowLayoutPanel.Location = new System.Drawing.Point(6, 430);
            this.settingsFlowLayoutPanel.Name = "settingsFlowLayoutPanel";
            this.settingsFlowLayoutPanel.Size = new System.Drawing.Size(547, 0);
            this.settingsFlowLayoutPanel.TabIndex = 3;
            // 
            // invisibleControl1
            // 
            this.invisibleControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.invisibleControl1.Location = new System.Drawing.Point(0, 0);
            this.invisibleControl1.Name = "invisibleControl1";
            this.invisibleControl1.Size = new System.Drawing.Size(547, 0);
            this.invisibleControl1.TabIndex = 0;
            this.invisibleControl1.TabStop = false;
            this.invisibleControl1.Text = "invisibleControl1";
            // 
            // NewContent
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(559, 462);
            this.Controls.Add(this.typesObjectListView);
            this.Controls.Add(this.settingsFlowLayoutPanel);
            this.Controls.Add(this.bottomTableLayoutPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(348, 277);
            this.Name = "NewContent";
            this.Padding = new System.Windows.Forms.Padding(6);
            this.ShowInTaskbar = false;
            this.Text = "New File";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NewContent_FormClosing);
            this.Load += new System.EventHandler(this.NewContent_Load);
            this.bottomTableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.typesObjectListView)).EndInit();
            this.settingsFlowLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel bottomTableLayoutPanel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private BrightIdeasSoftware.ObjectListView typesObjectListView;
        private BrightIdeasSoftware.OLVColumn typeNameColumn;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.FlowLayoutPanel settingsFlowLayoutPanel;
        private Controls.InvisibleControl invisibleControl1;
    }
}