﻿using System;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Commands;

namespace SFMTK.Forms
{
    /// <summary>
    /// A dialog for editing the toolstrips of the application.
    /// </summary>
    public partial class CustomizeToolstrips : Form, IThemeable
    {
        //TODO: CustomizeToolstrips - turn into a widget - it does not make sense to have this as a separate window
        //  -> possibly create as floating window by default?

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomizeToolstrips"/> class.
        /// </summary>
        public CustomizeToolstrips()
        {
            //Compiler-generated
            InitializeComponent();

            //OLVs
            this.commandsObjectListView.AlwaysGroupByColumn = this.commandCategoryColumn;
            this.commandNameColumn.AspectToStringConverter = (s) => s.ToString().Replace("&", "");
            this.commandsObjectListView.AddObjects(CommandManager.Commands);

            this.toolbarNameColumn.AspectGetter = (o) => o is StatusStrip ? "Status Bar" : ((ToolStrip)o).Text;
            this.toolbarTypeColumn.AspectGetter = (o) => (o is MenuStrip || o is StatusStrip) ? "Menus" : "Toolbar";
            this.toolbarsObjectListView.AlwaysGroupByColumn = this.toolbarTypeColumn;
            this.toolbarsObjectListView.CheckStateGetter = (o) => ((ToolStrip)o).Visible ? CheckState.Checked : CheckState.Unchecked;
            this.toolbarsObjectListView.CheckStatePutter = (o, v) =>
            {
                ((ToolStrip)o).Visible = v == CheckState.Checked;
                if (o is MenuStrip)
                    Program.MainForm.reopenMenuPanel.Visible = v == CheckState.Unchecked;
                return v;
            };
            this.toolbarsObjectListView.AddObjects(Program.MainForm.ToolBars.ToList());
            this.toolbarsObjectListView.AddObject(Program.MainForm.mainStatusStrip);
            this.toolbarsObjectListView.AddObject(Program.MainForm.mainMenuBar);

            //TODO: CustomizeToolstrips - allow creating your own toolstrips/menus as well (gonna be tough though?!)
            //TODO: CustomizeToolstrips - show the name of the image currently selected (possibly simply as a tooltip)
            //BUG: CustomizeToolstrips - resize images that are > 16x16 inside of the objectlistviews
            this.itemNameColumn.AspectToStringConverter = (s) => s.ToString().Replace("&", "");
            this.itemsObjectListView.ChildrenGetter = (i) => ((ToolStripMenuItem)i).DropDownItems;
            this.itemsObjectListView.CanExpandGetter = (i) => i is ToolStripMenuItem tsmi && tsmi.HasDropDownItems;
            this.itemTypeColumn.AspectGetter = (i) => i.GetType().Name;
            this.itemNameColumn.AspectGetter = (i) =>
            {
                if (i is ToolStripSeparator)
                    return new string('-', 20);
                else if (i is ToolStripTextBox tstb)
                    return $"Text Box ({tstb.Name})";
                else if (i is ToolStripComboBox tscb)
                    return $"Combo Box ({tscb.Name})";
                else if (i is ToolStripProgressBar tspb)
                    return $"Progress Bar ({tspb.Name})";
                else if (i is ToolStripButton tsb && string.IsNullOrEmpty(tsb.Text))
                    return $"Button ({tsb.Name})";
                else if (i is ToolStripDropDownButton tsddb && string.IsNullOrEmpty(tsddb.Text))
                    return $"Dropdown Button ({tsddb.Name})";
                else if (i is ToolStripSplitButton tssb && string.IsNullOrEmpty(tssb.Text))
                    return $"Split Button ({tssb.Name})";
                else if (i is ToolStripItem tsi)
                    return tsi.Text;
                throw new ArgumentOutOfRangeException(nameof(i));
            };

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomizeToolstrips"/> class.
        /// </summary>
        /// <param name="stripname">Name of the strip to select.</param>
        public CustomizeToolstrips(string stripname) : this()
        {
            this._selectStrip = Program.MainForm.ToolBars.First(t => t.Name == stripname);
        }

        private void CustomizeToolstrips_Load(object sender, EventArgs e)
        {
            //Select strip, if any
            if (this._selectStrip != null)
            {
                this.toolbarsObjectListView.SelectedObject = this._selectStrip;
                this.toolbarsObjectListView.Focus();
            }

            //Center me
            this.CenterToParent();
        }

        private ToolStrip _selectStrip;

        private void commandsObjectListView_SelectionChanged(object sender, EventArgs e)
        {
            //Allow if selection exists
            var hasselection = this.commandsObjectListView.SelectedObject != null;
            this.splitContainer1.Panel2.Enabled = hasselection;
            if (hasselection)
            {
                //Update databindings
                this.commandNameTextBox.DataBindings.Clear();
                this.commandNameTextBox.Text = SelectedCommand.Name;
                this.commandNameTextBox.DataBindings.Add("Text", SelectedCommand, "Name", false, DataSourceUpdateMode.OnPropertyChanged);
                this.commandNameTextBox.Enabled = SelectedCommand.GetType().GetProperty("Name").CanWrite; //Name can be read-only if overridden property
                this.commandImagePictureBox.Image = SelectedCommand.Icon;

                //Update stationary properties
                this.commandDescriptionLabel.Text = $"&Description: {SelectedCommand.ToolTipText}";
                this.commandShortcutLabel.Text = $"&Shortcut: {SelectedCommand.ShortcutString}";

                //NYI: CustomizeToolstrips - update "Used By" list to show which toolstrip items and menu items call this command
                //TODO: CustomizeToolstrips - allow editing the shortcut keys of commands inside of the customize dialog
            }
        }

        private CommandBase SelectedCommand => this.commandsObjectListView.SelectedObject as CommandBase;

        private void editCommandImageButton_Click(object sender, EventArgs e)
        {
            //Open the icon picker
            var cmd = SelectedCommand;
            var picker = new IconPicker(true);
            if (picker.ShowDialog(this) == DialogResult.OK)
            {
                this.commandImagePictureBox.Image = picker.Selected;
                SelectedCommand.Icon = picker.Selected;
            }
        }

        private void toolbarsObjectListView_SelectionChanged(object sender, EventArgs e)
        {
            //Allow if selection exists
            var hasselection = this.toolbarsObjectListView.SelectedObject != null;
            this.splitContainer2.Panel2.Enabled = hasselection;
            if (hasselection)
            {
                //Update item list
                var items = SelectedToolStrip.Items.OfType<ToolStripItem>().Where(i => i.Overflow != ToolStripItemOverflow.Always);

                //Refresh dynamic entries first
                //TODO: CustomizeToolStrips - show all classes inheriting from DynamicToolStripItem (separate group) when
                //  adding a command to the menu/toolstrip. in the OLV, show a friendly name as the entry (allow expanding to preview current dynamic items)
                foreach (var parent in items)
                    foreach (var dyna in ControlsHelper.RecurseItems(parent, true).OfType<Controls.DynamicToolStripItem>())
                        dyna.ReloadDynamicItems();

                this.itemsObjectListView.SetObjects(items);
                this.itemsObjectListView.ExpandAll();
                //TODO: CustomizeToolStrips - split menu bar into multiple entries "File Menu" etc...

                //Update stationary properties
                this.nameLabel.Text = $"Name: {this.toolbarNameColumn.GetStringValue(this.toolbarsObjectListView.SelectedObject)}";

                //NYI: CustomizeToolstrips - allow to reorder TSI list (drag&drop too), implement toolstrip's editing items (as contextmenu too),
                //  only allow editing items with commands assigned
                //TODO: CustomizeToolstrips - show ContextMenuStrips in list of editable menus as well (components)
            }
        }

        private ToolStrip SelectedToolStrip => this.toolbarsObjectListView.SelectedObject as ToolStrip;

        public string ActiveTheme { get; set; }

        private void addItemToolStripButton_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void removeItemToolStripButton_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void moveItemUpToolStripButton_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void moveItemDownToolStripButton_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void editItemToolStripButton_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void okButton_Click(object sender, EventArgs e) => this.Close();
    }
}
