﻿using BrightIdeasSoftware;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    /// <summary>
    /// Allows for selecting different launch procedures for the Source Filmmaker and SFMTK.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class LauncherSelector : Form, IThemeable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LauncherSelector"/> class.
        /// </summary>
        public LauncherSelector()
        {
            //Compiler-generated
            InitializeComponent();

            //Initialize launcher list
            //BUG: LauncherSelector - messed up theme loading results in weird listview images (?)
            var dtr = new DescribedTaskRenderer();
            dtr.DescriptionAspectName = "Description";
            dtr.DescriptionColor = UITheme.SelectedTheme.SubTextColor;
            dtr.TitleFont = new Font(UITheme.SelectedTheme.StatusTextFont, FontStyle.Bold);
            this.launcherNameColumn.Renderer = dtr;

            //Populate list
            this.launcherObjectListView.SetObjects(new LauncherEntry[] { new SFMTKLauncherEntry("TODO", "GROUPNAME", "none") });
            this.launcherObjectListView.SelectedIndex = 0;

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        public string ActiveTheme { get; set; }

        private void launcherObjectListView_DoubleClick(object sender, EventArgs e)
        {
            //Send OK-press if there is a selected entry
            if (this.SelectedEntry == null)
                return;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void launcherObjectListView_SelectionChanged(object sender, EventArgs e)
        {
            //Update OK enabled state
            this.okButton.Enabled = this.SelectedEntry != null;
        }

        /// <summary>
        /// Gets the selected launcher entry, or null if none is selected.
        /// </summary>
        public LauncherEntry SelectedEntry => this.launcherObjectListView.SelectedObject as LauncherEntry;

        /// <summary>
        /// Shows the launcher and returns the selected entry, if any.
        /// </summary>
        public static LauncherEntry ShowLauncher()
        {
            var ls = new LauncherSelector();
            var res = ls.ShowDialog();
            if (res == DialogResult.Cancel)
                return null;
            return ls.SelectedEntry;
        }
    }
}
