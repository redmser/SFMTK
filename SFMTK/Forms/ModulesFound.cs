﻿using SFMTK.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    /// <summary>
    /// A dialog for when new modules have been found that were not known to SFMTK before.
    /// </summary>
    public partial class ModulesFound : Form, IThemeable
    {
        //TODO: ModulesFound - add a "open settings" button which doesn't change loaded state, but just opens the modules settings page
        //TODO: ModulesFound - add a context menu to enable or disable all selected modules, as well as browsing to their folder

        /// <summary>
        /// Initializes a new instance of the <see cref="ModulesFound" /> class.
        /// </summary>
        /// <param name="settings">The ModuleSettings that were found.</param>
        public ModulesFound(IEnumerable<ModuleSetting> settings)
        {
            //Compiler-generated
            InitializeComponent();

            //Keep only top-level settings
            if (settings != null)
            {
                this.AllSettings = settings.ToList();
                settings = settings.Where(s => s.GetTopLevel(this.AllSettings));
            }

            this.moduleAuthorColumn.AspectGetter = (o) => ((ModuleSetting)o).Module?.GetCompany();
            this.moduleVersionColumn.AspectGetter = (o) => ((ModuleSetting)o).Module?.GetVersion();
            this.moduleNameColumn.AspectGetter = (o) => ((ModuleSetting)o).GetDisplayText();
            this.modulesTreeListView.ChildrenGetter = (o) => ((ModuleSetting)o).GetChildren(this.AllSettings);
            this.modulesTreeListView.CanExpandGetter = (o) => ((ModuleSetting)o).GetChildren(this.AllSettings).Any();

            //Add list of found modules
            if (settings != null)
            {
                this.modulesTreeListView.SetObjects(settings.OrderBy(s => s.GetDisplayText()));
                this.modulesTreeListView.ExpandAll();
            }

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModulesFound"/> class.
        /// </summary>
        public ModulesFound() : this(null)
        {

        }

        public string ActiveTheme { get; set; }

        private void modulesTreeListView_DoubleClick(object sender, EventArgs e)
        {
            //Toggle selected item's checkbox
            var mod = (ModuleSetting)this.modulesTreeListView.SelectedObject;
            if (mod != null)
                mod.Load = !mod.Load;
        }

        /// <summary>
        /// Gets the module settings that are displayed in the listview.
        /// </summary>
        public IEnumerable<ModuleSetting> Settings => this.modulesTreeListView.Objects.Cast<ModuleSetting>();

        /// <summary>
        /// Gets all module settings known at this point.
        /// </summary>
        public List<ModuleSetting> AllSettings { get; }

        private void ModulesFound_Load(object sender, EventArgs e)
        {
            this.CenterToParent();
        }

        private void modulesTreeListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            //Update header checked state
            this.moduleNameColumn.HeaderCheckState = this.modulesTreeListView.CheckedObjects.Count == 0 ? CheckState.Unchecked :
                (this.modulesTreeListView.CheckedObjects.Count == AllSettings.Count() ? CheckState.Checked : CheckState.Indeterminate);
            this.modulesTreeListView.RebuildColumns();
        }
    }
}
