﻿using System;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    /// <summary>
    /// Dialog for configuring a launch of SFMTK.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class SFMTKLaunchOptions : Form, IThemeable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SFMTKLaunchOptions"/> class.
        /// </summary>
        public SFMTKLaunchOptions() : this(null, "")
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SFMTKLaunchOptions"/> class.
        /// </summary>
        public SFMTKLaunchOptions(string extension, string defaultValue)
        {
            //Compiler-generated
            InitializeComponent();

            //Apply extension format
            if (string.IsNullOrEmpty(extension))
                this.headerLabel.Text = string.Format(this.headerLabel.Text, "");
            else
                this.headerLabel.Text = string.Format(this.headerLabel.Text, extension + " ");

            //Default value
            this.launchOptionsTextBox.Text = defaultValue;

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        private void SFMTKLaunchOptions_Load(object sender, System.EventArgs e)
        {
            this.CenterToParent();
        }

        /// <summary>
        /// Gets the currently set launch options. Replace {0} with the list of files to open.
        /// </summary>
        public string LaunchOptions => this.launchOptionsTextBox.Text;

        public string ActiveTheme { get; set; }
    }
}
