﻿using System;
using System.Windows.Forms;

namespace SFMTK.Forms
{
    /// <summary>
    /// A prompt dialog box for specifying text.
    /// </summary>
    public partial class RenamePrompt : Form, IThemeable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RenamePrompt"/> class.
        /// </summary>
        public RenamePrompt()
        {
            //Compiler-generated
            InitializeComponent();

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        public string ActiveTheme { get; set; }

        /// <summary>
        /// Displays a prompt dialog box for specifying text (single-lined). Returns that text, or null if cancelled.
        /// </summary>
        /// <param name="title">Title of the dialog box. Defaults to "Rename".</param>
        /// <param name="instructions">Main instruction label of the dialog box. Defaults to a prompt telling the user to enter a new name.</param>
        /// <param name="defaultText">The default contents of the text box.</param>
        /// <param name="owner">Optionally specify the owner of this dialog box.</param>
        public static string ShowPrompt(string title, string instructions, string defaultText, IWin32Window owner = null)
        {
            var prompt = new RenamePrompt();
            prompt.Text = title ?? "Rename";
            prompt.mainLabel.Text = instructions ?? "Please enter a new name:";
            prompt.mainTextBox.Text = defaultText;
            if (prompt.ShowDialog(owner ?? Program.MainForm) == DialogResult.OK)
                return prompt.mainTextBox.Text;
            return null;
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            this.CenterToParent();
            this.mainTextBox.Focus();
            this.mainTextBox.SelectAll();
        }
    }
}
