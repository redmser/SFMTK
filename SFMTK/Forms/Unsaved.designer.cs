﻿namespace SFMTK.Forms
{
    partial class Unsaved
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.saveAllButton = new System.Windows.Forms.Button();
            this.saveSelectedButton = new System.Windows.Forms.Button();
            this.discardButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.unsavedObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.unsavedNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.unsavedObjectListView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(362, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "The following files have unsaved changes. Please select one of the following opti" +
    "ons:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // saveAllButton
            // 
            this.saveAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveAllButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.saveAllButton.Location = new System.Drawing.Point(276, 48);
            this.saveAllButton.Name = "saveAllButton";
            this.saveAllButton.Size = new System.Drawing.Size(90, 23);
            this.saveAllButton.TabIndex = 2;
            this.saveAllButton.Text = "Save &All";
            this.saveAllButton.UseVisualStyleBackColor = true;
            this.saveAllButton.Click += new System.EventHandler(this.saveAllButton_Click);
            // 
            // saveSelectedButton
            // 
            this.saveSelectedButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveSelectedButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.saveSelectedButton.Enabled = false;
            this.saveSelectedButton.Location = new System.Drawing.Point(276, 76);
            this.saveSelectedButton.Name = "saveSelectedButton";
            this.saveSelectedButton.Size = new System.Drawing.Size(90, 23);
            this.saveSelectedButton.TabIndex = 3;
            this.saveSelectedButton.Text = "&Save Selected";
            this.saveSelectedButton.UseVisualStyleBackColor = true;
            this.saveSelectedButton.Click += new System.EventHandler(this.saveSelectedButton_Click);
            // 
            // discardButton
            // 
            this.discardButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.discardButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.discardButton.Location = new System.Drawing.Point(276, 104);
            this.discardButton.Name = "discardButton";
            this.discardButton.Size = new System.Drawing.Size(90, 23);
            this.discardButton.TabIndex = 4;
            this.discardButton.Text = "&Discard";
            this.discardButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(276, 132);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(90, 23);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // unsavedObjectListView
            // 
            this.unsavedObjectListView.AllColumns.Add(this.unsavedNameColumn);
            this.unsavedObjectListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.unsavedObjectListView.CellEditUseWholeCell = false;
            this.unsavedObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.unsavedNameColumn});
            this.unsavedObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.unsavedObjectListView.FullRowSelect = true;
            this.unsavedObjectListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.unsavedObjectListView.HideSelection = false;
            this.unsavedObjectListView.Location = new System.Drawing.Point(8, 48);
            this.unsavedObjectListView.Name = "unsavedObjectListView";
            this.unsavedObjectListView.ShowGroups = false;
            this.unsavedObjectListView.Size = new System.Drawing.Size(262, 150);
            this.unsavedObjectListView.TabIndex = 1;
            this.unsavedObjectListView.UseCompatibleStateImageBehavior = false;
            this.unsavedObjectListView.View = System.Windows.Forms.View.Details;
            this.unsavedObjectListView.SelectionChanged += new System.EventHandler(this.unsavedObjectListView_SelectionChanged);
            // 
            // unsavedNameColumn
            // 
            this.unsavedNameColumn.AspectName = "TabText";
            this.unsavedNameColumn.FillsFreeSpace = true;
            this.unsavedNameColumn.Text = "Name";
            // 
            // Unsaved
            // 
            this.AcceptButton = this.saveAllButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(375, 205);
            this.Controls.Add(this.unsavedObjectListView);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.discardButton);
            this.Controls.Add(this.saveSelectedButton);
            this.Controls.Add(this.saveAllButton);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(240, 196);
            this.Name = "Unsaved";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Unsaved Changes";
            this.Load += new System.EventHandler(this.Unsaved_Load);
            ((System.ComponentModel.ISupportInitialize)(this.unsavedObjectListView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button saveAllButton;
        private System.Windows.Forms.Button saveSelectedButton;
        private System.Windows.Forms.Button discardButton;
        private System.Windows.Forms.Button cancelButton;
        private BrightIdeasSoftware.ObjectListView unsavedObjectListView;
        private BrightIdeasSoftware.OLVColumn unsavedNameColumn;
    }
}