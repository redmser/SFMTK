﻿namespace SFMTK.Forms
{
    partial class LauncherSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LauncherSelector));
            this.label1 = new System.Windows.Forms.Label();
            this.launcherObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.launcherNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.label2 = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cancelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.launcherObjectListView)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(10, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(438, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select what you would like to launch.";
            // 
            // launcherObjectListView
            // 
            this.launcherObjectListView.AllColumns.Add(this.launcherNameColumn);
            this.launcherObjectListView.CellEditUseWholeCell = false;
            this.launcherObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.launcherNameColumn});
            this.launcherObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.launcherObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.launcherObjectListView.FullRowSelect = true;
            this.launcherObjectListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.launcherObjectListView.HideSelection = false;
            this.launcherObjectListView.Location = new System.Drawing.Point(10, 36);
            this.launcherObjectListView.MultiSelect = false;
            this.launcherObjectListView.Name = "launcherObjectListView";
            this.launcherObjectListView.RowHeight = 32;
            this.launcherObjectListView.Size = new System.Drawing.Size(438, 232);
            this.launcherObjectListView.TabIndex = 2;
            this.launcherObjectListView.UseCompatibleStateImageBehavior = false;
            this.launcherObjectListView.View = System.Windows.Forms.View.Details;
            this.launcherObjectListView.SelectionChanged += new System.EventHandler(this.launcherObjectListView_SelectionChanged);
            this.launcherObjectListView.DoubleClick += new System.EventHandler(this.launcherObjectListView_DoubleClick);
            // 
            // launcherNameColumn
            // 
            this.launcherNameColumn.AspectName = "Name";
            this.launcherNameColumn.FillsFreeSpace = true;
            this.launcherNameColumn.ImageAspectName = "Image";
            this.launcherNameColumn.Text = "Name";
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(270, 29);
            this.label2.TabIndex = 0;
            this.label2.Text = "This launcher can be customized in SFMTK\'s options.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.okButton.Enabled = false;
            this.okButton.Location = new System.Drawing.Point(279, 3);
            this.okButton.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 26);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.cancelButton, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.okButton, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 268);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(438, 29);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cancelButton.Location = new System.Drawing.Point(360, 3);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 26);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // LauncherSelector
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(458, 307);
            this.Controls.Add(this.launcherObjectListView);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(193, 145);
            this.Name = "LauncherSelector";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SFMTK Launcher";
            ((System.ComponentModel.ISupportInitialize)(this.launcherObjectListView)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private BrightIdeasSoftware.ObjectListView launcherObjectListView;
        private BrightIdeasSoftware.OLVColumn launcherNameColumn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button cancelButton;
    }
}