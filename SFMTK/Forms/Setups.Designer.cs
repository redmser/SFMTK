﻿namespace SFMTK.Forms
{
    partial class Setups
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Setups));
            this.setupsObjectListView = new BrightIdeasSoftware.FastObjectListView();
            this.nameOlvColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.newSetupButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonMoveDown = new System.Windows.Forms.Button();
            this.buttonMoveUp = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.helpButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.setupsObjectListView)).BeginInit();
            this.SuspendLayout();
            // 
            // setupsObjectListView
            // 
            this.setupsObjectListView.AllColumns.Add(this.nameOlvColumn);
            this.setupsObjectListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.setupsObjectListView.CellEditEnterChangesRows = true;
            this.setupsObjectListView.CellEditTabChangesRows = true;
            this.setupsObjectListView.CellEditUseWholeCell = false;
            this.setupsObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameOlvColumn});
            this.setupsObjectListView.FullRowSelect = true;
            this.setupsObjectListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.setupsObjectListView.HideSelection = false;
            this.setupsObjectListView.IsSimpleDragSource = true;
            this.setupsObjectListView.Location = new System.Drawing.Point(8, 25);
            this.setupsObjectListView.MultiSelect = false;
            this.setupsObjectListView.Name = "setupsObjectListView";
            this.setupsObjectListView.ShowGroups = false;
            this.setupsObjectListView.Size = new System.Drawing.Size(282, 228);
            this.setupsObjectListView.TabIndex = 1;
            this.setupsObjectListView.UseCompatibleStateImageBehavior = false;
            this.setupsObjectListView.View = System.Windows.Forms.View.Details;
            this.setupsObjectListView.VirtualMode = true;
            this.setupsObjectListView.ModelDropped += new System.EventHandler<BrightIdeasSoftware.ModelDropEventArgs>(this.setupsObjectListView_ModelDropped);
            this.setupsObjectListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.setupsObjectListView_ItemSelectionChanged);
            this.setupsObjectListView.DoubleClick += new System.EventHandler(this.setupsObjectListView_DoubleClick);
            // 
            // nameOlvColumn
            // 
            this.nameOlvColumn.AspectName = "Name";
            this.nameOlvColumn.AutoCompleteEditor = false;
            this.nameOlvColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.nameOlvColumn.FillsFreeSpace = true;
            this.nameOlvColumn.Text = "Name";
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonEdit.Enabled = false;
            this.buttonEdit.Location = new System.Drawing.Point(172, 257);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(75, 23);
            this.buttonEdit.TabIndex = 4;
            this.buttonEdit.Text = "&Edit...";
            this.toolTip1.SetToolTip(this.buttonEdit, "Edit the selected SFM setup.");
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDelete.Enabled = false;
            this.buttonDelete.Location = new System.Drawing.Point(92, 257);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 3;
            this.buttonDelete.Text = "&Delete";
            this.toolTip1.SetToolTip(this.buttonDelete, "Deletes the selected SFM setup.");
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // newSetupButton
            // 
            this.newSetupButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.newSetupButton.Location = new System.Drawing.Point(8, 257);
            this.newSetupButton.Name = "newSetupButton";
            this.newSetupButton.Size = new System.Drawing.Size(80, 23);
            this.newSetupButton.TabIndex = 2;
            this.newSetupButton.Text = "&New Setup";
            this.toolTip1.SetToolTip(this.newSetupButton, "Creates a new SFM setup.");
            this.newSetupButton.UseVisualStyleBackColor = true;
            this.newSetupButton.Click += new System.EventHandler(this.newSetupButton_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(4, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(266, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Saved setups:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // buttonMoveDown
            // 
            this.buttonMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMoveDown.Enabled = false;
            this.buttonMoveDown.Image = global::SFMTK.Properties.Resources.arrow_down;
            this.buttonMoveDown.Location = new System.Drawing.Point(294, 89);
            this.buttonMoveDown.Name = "buttonMoveDown";
            this.buttonMoveDown.Size = new System.Drawing.Size(28, 28);
            this.buttonMoveDown.TabIndex = 7;
            this.toolTip1.SetToolTip(this.buttonMoveDown, "Moves the selected SFM setup down by one slot.");
            this.buttonMoveDown.UseVisualStyleBackColor = true;
            // 
            // buttonMoveUp
            // 
            this.buttonMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMoveUp.Enabled = false;
            this.buttonMoveUp.Image = global::SFMTK.Properties.Resources.arrow_up;
            this.buttonMoveUp.Location = new System.Drawing.Point(294, 57);
            this.buttonMoveUp.Name = "buttonMoveUp";
            this.buttonMoveUp.Size = new System.Drawing.Size(28, 28);
            this.buttonMoveUp.TabIndex = 6;
            this.toolTip1.SetToolTip(this.buttonMoveUp, "Moves the selected SFM setup up by one slot.");
            this.buttonMoveUp.UseVisualStyleBackColor = true;
            // 
            // helpButton
            // 
            this.helpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.helpButton.Image = global::SFMTK.Properties.Resources.help;
            this.helpButton.Location = new System.Drawing.Point(294, 25);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(28, 28);
            this.helpButton.TabIndex = 5;
            this.toolTip1.SetToolTip(this.helpButton, "Shows help for setups.");
            this.helpButton.UseVisualStyleBackColor = true;
            this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
            // 
            // Setups
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 285);
            this.Controls.Add(this.helpButton);
            this.Controls.Add(this.setupsObjectListView);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.newSetupButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonMoveDown);
            this.Controls.Add(this.buttonMoveUp);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(269, 187);
            this.Name = "Setups";
            this.Text = "Manage Setups";
            this.Load += new System.EventHandler(this.Setups_Load);
            ((System.ComponentModel.ISupportInitialize)(this.setupsObjectListView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private BrightIdeasSoftware.FastObjectListView setupsObjectListView;
        private BrightIdeasSoftware.OLVColumn nameOlvColumn;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button newSetupButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonMoveDown;
        private System.Windows.Forms.Button buttonMoveUp;
        private System.Windows.Forms.Button helpButton;
    }
}