﻿namespace SFMTK.Forms
{
    partial class ContentBrowserWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContentBrowserWindow));
            this.contentBrowser = new SFMTK.Controls.ContentBrowser();
            this.SuspendLayout();
            // 
            // contentBrowser
            // 
            this.contentBrowser.ActiveTheme = null;
            this.contentBrowser.AllowNewFiles = false;
            this.contentBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentBrowser.FullPath = "";
            this.contentBrowser.Location = new System.Drawing.Point(0, 0);
            this.contentBrowser.MultiSelect = false;
            this.contentBrowser.Name = "contentBrowser";
            this.contentBrowser.Size = new System.Drawing.Size(951, 581);
            this.contentBrowser.TabIndex = 0;
            // 
            // ContentBrowserWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 581);
            this.Controls.Add(this.contentBrowser);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(560, 381);
            this.Name = "ContentBrowserWindow";
            this.Text = "Content Browser";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ContentBrowserWindow_FormClosing);
            this.Load += new System.EventHandler(this.ContentBrowserWindow_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.ContentBrowser contentBrowser;
    }
}