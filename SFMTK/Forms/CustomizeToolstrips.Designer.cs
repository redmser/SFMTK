﻿namespace SFMTK.Forms
{
    partial class CustomizeToolstrips
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomizeToolstrips));
            this.bottomSpacingPanel = new System.Windows.Forms.Panel();
            this.okButton = new System.Windows.Forms.Button();
            this.commandCategoryColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.commandsTabPage = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.commandsObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.commandNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.commandShortcutLabel = new System.Windows.Forms.Label();
            this.editCommandImageButton = new System.Windows.Forms.Button();
            this.commandImagePictureBox = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.commandNameTextBox = new System.Windows.Forms.TextBox();
            this.commandDescriptionLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolbarsTabPage = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.toolbarsObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.toolbarNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.toolbarTypeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.itemsObjectListView = new BrightIdeasSoftware.TreeListView();
            this.itemNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.itemDescriptionColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.itemInternalNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.itemTypeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.itemsToolStrip = new System.Windows.Forms.ToolStrip();
            this.addItemToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.removeItemToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.moveItemUpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.moveItemDownToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.editItemToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.label5 = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.bottomSpacingPanel.SuspendLayout();
            this.commandsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.commandsObjectListView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandImagePictureBox)).BeginInit();
            this.toolbarsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toolbarsObjectListView)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemsObjectListView)).BeginInit();
            this.itemsToolStrip.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomSpacingPanel
            // 
            this.bottomSpacingPanel.Controls.Add(this.okButton);
            this.bottomSpacingPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomSpacingPanel.Location = new System.Drawing.Point(8, 422);
            this.bottomSpacingPanel.Name = "bottomSpacingPanel";
            this.bottomSpacingPanel.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.bottomSpacingPanel.Size = new System.Drawing.Size(519, 32);
            this.bottomSpacingPanel.TabIndex = 3;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(441, 4);
            this.okButton.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 26);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // commandCategoryColumn
            // 
            this.commandCategoryColumn.AspectName = "Category";
            this.commandCategoryColumn.IsVisible = false;
            this.commandCategoryColumn.Text = "Category";
            // 
            // commandsTabPage
            // 
            this.commandsTabPage.BackColor = System.Drawing.SystemColors.Window;
            this.commandsTabPage.Controls.Add(this.splitContainer1);
            this.commandsTabPage.Location = new System.Drawing.Point(4, 22);
            this.commandsTabPage.Name = "commandsTabPage";
            this.commandsTabPage.Size = new System.Drawing.Size(511, 388);
            this.commandsTabPage.TabIndex = 2;
            this.commandsTabPage.Text = "Commands";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.commandsObjectListView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.commandShortcutLabel);
            this.splitContainer1.Panel2.Controls.Add(this.editCommandImageButton);
            this.splitContainer1.Panel2.Controls.Add(this.commandImagePictureBox);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.commandNameTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.commandDescriptionLabel);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Enabled = false;
            this.splitContainer1.Size = new System.Drawing.Size(511, 388);
            this.splitContainer1.SplitterDistance = 146;
            this.splitContainer1.TabIndex = 0;
            // 
            // commandsObjectListView
            // 
            this.commandsObjectListView.AllColumns.Add(this.commandNameColumn);
            this.commandsObjectListView.AllColumns.Add(this.commandCategoryColumn);
            this.commandsObjectListView.CellEditUseWholeCell = false;
            this.commandsObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.commandNameColumn});
            this.commandsObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.commandsObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commandsObjectListView.FullRowSelect = true;
            this.commandsObjectListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.commandsObjectListView.HideSelection = false;
            this.commandsObjectListView.Location = new System.Drawing.Point(0, 0);
            this.commandsObjectListView.MultiSelect = false;
            this.commandsObjectListView.Name = "commandsObjectListView";
            this.commandsObjectListView.Size = new System.Drawing.Size(146, 388);
            this.commandsObjectListView.SortGroupItemsByPrimaryColumn = false;
            this.commandsObjectListView.TabIndex = 0;
            this.commandsObjectListView.UseCompatibleStateImageBehavior = false;
            this.commandsObjectListView.View = System.Windows.Forms.View.Details;
            this.commandsObjectListView.SelectionChanged += new System.EventHandler(this.commandsObjectListView_SelectionChanged);
            // 
            // commandNameColumn
            // 
            this.commandNameColumn.AspectName = "Name";
            this.commandNameColumn.FillsFreeSpace = true;
            this.commandNameColumn.ImageAspectName = "Icon";
            this.commandNameColumn.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(136, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Recommended size: 16x16";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "&Used by:";
            // 
            // commandShortcutLabel
            // 
            this.commandShortcutLabel.AutoSize = true;
            this.commandShortcutLabel.Location = new System.Drawing.Point(8, 124);
            this.commandShortcutLabel.Name = "commandShortcutLabel";
            this.commandShortcutLabel.Size = new System.Drawing.Size(50, 13);
            this.commandShortcutLabel.TabIndex = 6;
            this.commandShortcutLabel.Text = "&Shortcut:";
            this.toolTip.SetToolTip(this.commandShortcutLabel, "Currently used shortcut for this command.");
            // 
            // editCommandImageButton
            // 
            this.editCommandImageButton.Location = new System.Drawing.Point(72, 88);
            this.editCommandImageButton.Name = "editCommandImageButton";
            this.editCommandImageButton.Size = new System.Drawing.Size(60, 23);
            this.editCommandImageButton.TabIndex = 4;
            this.editCommandImageButton.Text = "Edit...";
            this.toolTip.SetToolTip(this.editCommandImageButton, "Select the image to use for the command. Displayed on menu items.");
            this.editCommandImageButton.UseVisualStyleBackColor = true;
            this.editCommandImageButton.Click += new System.EventHandler(this.editCommandImageButton_Click);
            // 
            // commandImagePictureBox
            // 
            this.commandImagePictureBox.Location = new System.Drawing.Point(48, 92);
            this.commandImagePictureBox.Name = "commandImagePictureBox";
            this.commandImagePictureBox.Size = new System.Drawing.Size(16, 16);
            this.commandImagePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.commandImagePictureBox.TabIndex = 4;
            this.commandImagePictureBox.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "&Image:";
            // 
            // commandNameTextBox
            // 
            this.commandNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.commandNameTextBox.Location = new System.Drawing.Point(48, 10);
            this.commandNameTextBox.Name = "commandNameTextBox";
            this.commandNameTextBox.Size = new System.Drawing.Size(306, 20);
            this.commandNameTextBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.commandNameTextBox, "The name is used to display the command in lists, menu items and buttons. The und" +
        "o history may still display the command differently.");
            // 
            // commandDescriptionLabel
            // 
            this.commandDescriptionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.commandDescriptionLabel.AutoEllipsis = true;
            this.commandDescriptionLabel.Location = new System.Drawing.Point(8, 40);
            this.commandDescriptionLabel.Name = "commandDescriptionLabel";
            this.commandDescriptionLabel.Size = new System.Drawing.Size(346, 40);
            this.commandDescriptionLabel.TabIndex = 2;
            this.commandDescriptionLabel.Text = "&Description:\r\n";
            this.toolTip.SetToolTip(this.commandDescriptionLabel, "A short description of the command. Shown in form of a tooltip or status text for" +
        " corresponding items.");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "&Name:";
            // 
            // toolbarsTabPage
            // 
            this.toolbarsTabPage.BackColor = System.Drawing.SystemColors.Window;
            this.toolbarsTabPage.Controls.Add(this.splitContainer2);
            this.toolbarsTabPage.Location = new System.Drawing.Point(4, 22);
            this.toolbarsTabPage.Name = "toolbarsTabPage";
            this.toolbarsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.toolbarsTabPage.Size = new System.Drawing.Size(511, 388);
            this.toolbarsTabPage.TabIndex = 0;
            this.toolbarsTabPage.Text = "Toolbars & Menus";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.toolbarsObjectListView);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.panel1);
            this.splitContainer2.Panel2.Controls.Add(this.label5);
            this.splitContainer2.Panel2.Controls.Add(this.nameLabel);
            this.splitContainer2.Panel2.Enabled = false;
            this.splitContainer2.Size = new System.Drawing.Size(505, 382);
            this.splitContainer2.SplitterDistance = 146;
            this.splitContainer2.TabIndex = 2;
            // 
            // toolbarsObjectListView
            // 
            this.toolbarsObjectListView.AllColumns.Add(this.toolbarNameColumn);
            this.toolbarsObjectListView.AllColumns.Add(this.toolbarTypeColumn);
            this.toolbarsObjectListView.CellEditUseWholeCell = false;
            this.toolbarsObjectListView.CheckBoxes = true;
            this.toolbarsObjectListView.CheckedAspectName = "";
            this.toolbarsObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.toolbarNameColumn});
            this.toolbarsObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.toolbarsObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolbarsObjectListView.FullRowSelect = true;
            this.toolbarsObjectListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.toolbarsObjectListView.HideSelection = false;
            this.toolbarsObjectListView.Location = new System.Drawing.Point(0, 0);
            this.toolbarsObjectListView.MultiSelect = false;
            this.toolbarsObjectListView.Name = "toolbarsObjectListView";
            this.toolbarsObjectListView.Size = new System.Drawing.Size(146, 382);
            this.toolbarsObjectListView.SortGroupItemsByPrimaryColumn = false;
            this.toolbarsObjectListView.TabIndex = 0;
            this.toolbarsObjectListView.UseCompatibleStateImageBehavior = false;
            this.toolbarsObjectListView.View = System.Windows.Forms.View.Details;
            this.toolbarsObjectListView.SelectionChanged += new System.EventHandler(this.toolbarsObjectListView_SelectionChanged);
            // 
            // toolbarNameColumn
            // 
            this.toolbarNameColumn.AspectName = "";
            this.toolbarNameColumn.FillsFreeSpace = true;
            this.toolbarNameColumn.ImageAspectName = "Icon";
            this.toolbarNameColumn.Text = "Name";
            // 
            // toolbarTypeColumn
            // 
            this.toolbarTypeColumn.IsVisible = false;
            this.toolbarTypeColumn.Text = "Type";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.itemsObjectListView);
            this.panel1.Controls.Add(this.itemsToolStrip);
            this.panel1.Location = new System.Drawing.Point(8, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(340, 320);
            this.panel1.TabIndex = 3;
            // 
            // itemsObjectListView
            // 
            this.itemsObjectListView.AllColumns.Add(this.itemNameColumn);
            this.itemsObjectListView.AllColumns.Add(this.itemDescriptionColumn);
            this.itemsObjectListView.AllColumns.Add(this.itemInternalNameColumn);
            this.itemsObjectListView.AllColumns.Add(this.itemTypeColumn);
            this.itemsObjectListView.CellEditUseWholeCell = false;
            this.itemsObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.itemNameColumn,
            this.itemDescriptionColumn});
            this.itemsObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.itemsObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemsObjectListView.FullRowSelect = true;
            this.itemsObjectListView.HideSelection = false;
            this.itemsObjectListView.Location = new System.Drawing.Point(0, 25);
            this.itemsObjectListView.Name = "itemsObjectListView";
            this.itemsObjectListView.ShowGroups = false;
            this.itemsObjectListView.Size = new System.Drawing.Size(340, 295);
            this.itemsObjectListView.TabIndex = 3;
            this.itemsObjectListView.UseCompatibleStateImageBehavior = false;
            this.itemsObjectListView.UseFiltering = true;
            this.itemsObjectListView.View = System.Windows.Forms.View.Details;
            this.itemsObjectListView.VirtualMode = true;
            // 
            // itemNameColumn
            // 
            this.itemNameColumn.AspectName = "";
            this.itemNameColumn.ImageAspectName = "Image";
            this.itemNameColumn.Text = "Name";
            this.itemNameColumn.UseFiltering = false;
            this.itemNameColumn.Width = 160;
            // 
            // itemDescriptionColumn
            // 
            this.itemDescriptionColumn.AspectName = "ToolTipText";
            this.itemDescriptionColumn.Text = "Description";
            this.itemDescriptionColumn.UseFiltering = false;
            this.itemDescriptionColumn.Width = 170;
            // 
            // itemInternalNameColumn
            // 
            this.itemInternalNameColumn.AspectName = "Name";
            this.itemInternalNameColumn.IsVisible = false;
            this.itemInternalNameColumn.Text = "Internal Name";
            this.itemInternalNameColumn.UseFiltering = false;
            // 
            // itemTypeColumn
            // 
            this.itemTypeColumn.DisplayIndex = 2;
            this.itemTypeColumn.IsVisible = false;
            this.itemTypeColumn.Text = "Type";
            // 
            // itemsToolStrip
            // 
            this.itemsToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.itemsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addItemToolStripButton,
            this.removeItemToolStripButton,
            this.toolStripSeparator1,
            this.moveItemUpToolStripButton,
            this.moveItemDownToolStripButton,
            this.toolStripSeparator2,
            this.editItemToolStripButton});
            this.itemsToolStrip.Location = new System.Drawing.Point(0, 0);
            this.itemsToolStrip.Name = "itemsToolStrip";
            this.itemsToolStrip.Size = new System.Drawing.Size(340, 25);
            this.itemsToolStrip.TabIndex = 2;
            // 
            // addItemToolStripButton
            // 
            this.addItemToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addItemToolStripButton.Image = global::SFMTK.Properties.Resources.add;
            this.addItemToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addItemToolStripButton.Name = "addItemToolStripButton";
            this.addItemToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.addItemToolStripButton.Text = "&Add Item";
            this.addItemToolStripButton.Click += new System.EventHandler(this.addItemToolStripButton_Click);
            // 
            // removeItemToolStripButton
            // 
            this.removeItemToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.removeItemToolStripButton.Enabled = false;
            this.removeItemToolStripButton.Image = global::SFMTK.Properties.Resources.cross;
            this.removeItemToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.removeItemToolStripButton.Name = "removeItemToolStripButton";
            this.removeItemToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.removeItemToolStripButton.Text = "&Delete Item";
            this.removeItemToolStripButton.Click += new System.EventHandler(this.removeItemToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // moveItemUpToolStripButton
            // 
            this.moveItemUpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.moveItemUpToolStripButton.Enabled = false;
            this.moveItemUpToolStripButton.Image = global::SFMTK.Properties.Resources.arrow_up;
            this.moveItemUpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.moveItemUpToolStripButton.Name = "moveItemUpToolStripButton";
            this.moveItemUpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.moveItemUpToolStripButton.Text = "Move &Up";
            this.moveItemUpToolStripButton.Click += new System.EventHandler(this.moveItemUpToolStripButton_Click);
            // 
            // moveItemDownToolStripButton
            // 
            this.moveItemDownToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.moveItemDownToolStripButton.Enabled = false;
            this.moveItemDownToolStripButton.Image = global::SFMTK.Properties.Resources.arrow_down;
            this.moveItemDownToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.moveItemDownToolStripButton.Name = "moveItemDownToolStripButton";
            this.moveItemDownToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.moveItemDownToolStripButton.Text = "Move Do&wn";
            this.moveItemDownToolStripButton.Click += new System.EventHandler(this.moveItemDownToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // editItemToolStripButton
            // 
            this.editItemToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editItemToolStripButton.Enabled = false;
            this.editItemToolStripButton.Image = global::SFMTK.Properties.Resources.pencil;
            this.editItemToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editItemToolStripButton.Name = "editItemToolStripButton";
            this.editItemToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.editItemToolStripButton.Text = "&Edit Item";
            this.editItemToolStripButton.Click += new System.EventHandler(this.editItemToolStripButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "&Items:";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(8, 12);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(38, 13);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "&Name:";
            this.toolTip.SetToolTip(this.nameLabel, "Name of the toolstrip or menu.");
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.toolbarsTabPage);
            this.tabControl.Controls.Add(this.commandsTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(8, 8);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(519, 414);
            this.tabControl.TabIndex = 0;
            // 
            // CustomizeToolstrips
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 462);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.bottomSpacingPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(441, 295);
            this.Name = "CustomizeToolstrips";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.Text = "Customize";
            this.Load += new System.EventHandler(this.CustomizeToolstrips_Load);
            this.bottomSpacingPanel.ResumeLayout(false);
            this.commandsTabPage.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.commandsObjectListView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandImagePictureBox)).EndInit();
            this.toolbarsTabPage.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.toolbarsObjectListView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemsObjectListView)).EndInit();
            this.itemsToolStrip.ResumeLayout(false);
            this.itemsToolStrip.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel bottomSpacingPanel;
        private System.Windows.Forms.Button okButton;
        private BrightIdeasSoftware.OLVColumn commandCategoryColumn;
        private System.Windows.Forms.TabPage commandsTabPage;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private BrightIdeasSoftware.ObjectListView commandsObjectListView;
        private BrightIdeasSoftware.OLVColumn commandNameColumn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label commandShortcutLabel;
        private System.Windows.Forms.Button editCommandImageButton;
        private System.Windows.Forms.PictureBox commandImagePictureBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox commandNameTextBox;
        private System.Windows.Forms.Label commandDescriptionLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage toolbarsTabPage;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private BrightIdeasSoftware.ObjectListView toolbarsObjectListView;
        private BrightIdeasSoftware.OLVColumn toolbarNameColumn;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TabControl tabControl;
        private BrightIdeasSoftware.OLVColumn toolbarTypeColumn;
        private BrightIdeasSoftware.TreeListView itemsObjectListView;
        private BrightIdeasSoftware.OLVColumn itemNameColumn;
        private System.Windows.Forms.Label label5;
        private BrightIdeasSoftware.OLVColumn itemDescriptionColumn;
        private BrightIdeasSoftware.OLVColumn itemInternalNameColumn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStrip itemsToolStrip;
        private System.Windows.Forms.ToolStripButton addItemToolStripButton;
        private System.Windows.Forms.ToolStripButton removeItemToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton moveItemUpToolStripButton;
        private System.Windows.Forms.ToolStripButton moveItemDownToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton editItemToolStripButton;
        private BrightIdeasSoftware.OLVColumn itemTypeColumn;
        private System.Windows.Forms.ToolTip toolTip;
    }
}