﻿using System;
using System.Runtime.Serialization;

namespace SFMTK
{
    // Notes to anyone looking to create additional exceptions:
    //  - Only create a new Exception type if none of the default exceptions work for what you are trying to do (due to naming or functionality).
    //  - You may also store additional info as properties in the exception, to retrieve for detailed error messages.

    namespace Parsing.KeyValues
    {

        /// <summary>
        /// An exception that occurred while tokenizing a KeyValues document.
        /// </summary>
        /// <seealso cref="System.Exception" />
        [Serializable]
        public class KeyValuesTokenizingException : Exception
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="KeyValuesParsingException"/> class.
            /// </summary>
            /// <param name="message">The error message.</param>
            /// <param name="buffer">The contents of the string buffer at the time of the exception.</param>
            public KeyValuesTokenizingException(string message, string buffer) : base(message)
            {
                this.Buffer = buffer;
            }

            /// <summary>
            /// Gets what data is currently loaded into memory.
            /// </summary>
            public string Buffer { get; }

            /// <summary>
            /// When overridden in a derived class, sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with information about the exception.
            /// </summary>
            /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
            /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
            public override void GetObjectData(SerializationInfo info, StreamingContext context)
            {
                if (info == null)
                    throw new ArgumentNullException("info");
                info.AddValue("Buffer", this.Buffer);
                base.GetObjectData(info, context);
            }
        }

        /// <summary>
        /// An exception that occurred while parsing a KeyValues document.
        /// </summary>
        /// <seealso cref="System.Exception" />
        [Serializable]
        public class KeyValuesParsingException : Exception
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="KeyValuesParsingException"/> class.
            /// </summary>
            /// <param name="message">The error message.</param>
            /// <param name="token">Which token this exception was thrown on.</param>
            public KeyValuesParsingException(string message, KVToken token) : base(message)
            {
                this.Token = token;
            }

            /// <summary>
            /// Gets which token this exception was thrown on.
            /// </summary>
            public KVToken Token { get; }

            /// <summary>
            /// When overridden in a derived class, sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with information about the exception.
            /// </summary>
            /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
            /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
            public override void GetObjectData(SerializationInfo info, StreamingContext context)
            {
                if (info == null)
                    throw new ArgumentNullException("info");
                info.AddValue("Token", this.Token);
                base.GetObjectData(info, context);
            }
        }

    }

    namespace Contents
    {

        /// <summary>
        /// An exception that occurred while loading or saving content.
        /// </summary>
        /// <seealso cref="System.Exception" />
        [Serializable]
        public class ContentIOException : Exception
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ContentIOException"/> class.
            /// </summary>
            public ContentIOException() : base()
            {

            }

            /// <summary>
            /// Initializes a new instance of the <see cref="ContentIOException"/> class.
            /// </summary>
            /// <param name="message">The message that describes the error.</param>
            public ContentIOException(string message) : base(message)
            {

            }
        }

    }

    namespace Modules
    {

        /// <summary>
        /// An exception that occurred when a module's property was found to be invalid and should be investigated.
        /// <para/>Should be thrown using the <see cref="SFMTK.Modules.ModuleManager.ThrowModulePropertyException"/> method.
        /// </summary>
        [Serializable]
        public class ModulePropertyException : Exception
        {
            internal ModulePropertyException() : base() { }
            internal ModulePropertyException(string message) : base(message) { }
        }

        /// <summary>
        /// An exception that occurred during a module's loading code. Inner exception may contain further load information.
        /// </summary>
        public class ModuleLoadException : Exception
        {
            internal ModuleLoadException() : base() { }
            internal ModuleLoadException(string message) : base(message) { }
            internal ModuleLoadException(string message, Exception inner) : base(message, inner) { }
        }

    }
}
