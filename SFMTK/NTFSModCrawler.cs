﻿using System.Collections.Generic;
using System.Linq;
using SFMTK.Data;

namespace SFMTK
{
    /// <summary>
    /// A <see cref="ModCrawler"/> which uses the MFT (available only on NTFS drives) to return the files in all mods.
    /// </summary>
    public class NTFSModCrawler : ModCrawler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NTFSModCrawler"/> class.
        /// </summary>
        public NTFSModCrawler() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NTFSModCrawler"/> class.
        /// </summary>
        /// <param name="mods">The list of mods to iterate through. Order determines which files to include.</param>
        public NTFSModCrawler(IEnumerable<Mod> mods) : base(mods)
        {
        }

        /// <summary>
        /// Crawls through all mods in the ModList, returning files in standardized format.
        /// </summary>
        public override IEnumerable<string> CrawlAll()
        {
            //Get all files in SFM folder
            //TODO: NTFSModCrawler - CrawlAll does not take into account symlinked mod folders
            //FIXME: NTFSModCrawler - does this call have to be blocking? can't we get nodes as we read them?
            var files = IOHelper.GetFilesUsingMFT(SFM.GamePath, true);

            if (this.CheckDuplicates)
            {
                //Generate duplicate entries for all files
                //Ordered by their mod index, for later duplicate handling and correct return order for progress display
                var dupes = files.Select(f => new DuplicateEntry(IOHelper.GetStandardizedPath(f)))
                    .Where(e => e.ModName != null)
                    .OrderBy(e => e.ModIndex);

                //UNTESTED: NTFSModCrawler duplicates checking
                //Check which ones are unique
                return new OrderedSet<DuplicateEntry>(dupes).Select(m => this.AbsolutePath ? m.FullPath : $"{m.ModName}/{m.RelativePath}");
            }
            else
            {
                //Reformat and return if we want all files
                var dupes = files.Select(f => new DuplicateEntry(IOHelper.GetStandardizedPath(f)))
                                 .Where(e => e.ModName != null)
                                 .OrderBy(e => e.ModIndex);
                //TODO: NTFSModCrawler - is the relative path actually correct??? (in ContentBrowser, filepath of content
                //  seems to be set to ".../game/mod/mod/materials/..."
                return dupes.Select(m => this.AbsolutePath ? m.FullPath : $"{m.ModName}/{m.RelativePath}");
            }
        }

        protected class DuplicateEntry
        {
            public DuplicateEntry() { }

            /// <param name="path">Full absolute standardized path to a file.</param>
            public DuplicateEntry(string path) : this()
            {
                this.FullPath = path;
                if (this.FullPath.EndsWith("/"))
                    return;

                var (relpath, modname) = GetRelativePath(path);
                this.RelativePath = relpath;
                this.ModName = modname;
                if (modname == null)
                    return;

                var mod = SFM.GameInfo.GetMod(this.ModName);
                if (mod == null)
                {
                    this.ModName = null;
                    return;
                }

                this.ModIndex = SFM.GameInfo.Mods.IndexOf(mod);
            }

            private static (string relpath, string modname) GetRelativePath(string path)
            {
                path = path.Replace(SFM.GamePath, string.Empty);
                var io = path.IndexOf('/');
                if (io < 0)
                    return (null, null);
                return (path.Substring(io + 1), path.Substring(0, io));
            }

            public string FullPath { get; }
            public string RelativePath { get; }
            public string ModName { get; }
            public int ModIndex { get; }

            public override int GetHashCode() => this.RelativePath.GetHashCode();
        }
    }
}
