﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SFMTK")]
[assembly: AssemblyDescription("A new take on the content creation and manipulation pipeline of Source Filmmaker.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("RedMser")]
[assembly: AssemblyProduct("SFMTK")]
[assembly: AssemblyCopyright("Copyright © RedMser 2017")]
[assembly: AssemblyTrademark("SFMTK")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("0623e108-faff-4da7-a3a9-68a9045fe480")]

[assembly: AssemblyVersion("0.0.0.0")]
[assembly: AssemblyFileVersion("0.0.0.0")]
[assembly: NeutralResourcesLanguage("en")]
[assembly: DisablePrivateReflection]

