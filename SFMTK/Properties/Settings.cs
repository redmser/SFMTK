﻿using SFMTK.Notifications;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SFMTK.Properties {

    /// <summary>
    /// An entry in a setup selection combobox which carries additional, non-localized metadata.
    /// </summary>
    public class SetupSelection
    {
        /// <summary>
        /// Gets a SetupSelection for the active setup.
        /// </summary>
        public static SetupSelection Active { get; } = new SetupSelection("(Use active setup)", Constants.SetupSelectionActive);

        /// <summary>
        /// Gets a SetupSelection for prompting which setup to use.
        /// </summary>
        public static SetupSelection Prompt { get; } = new SetupSelection("(Ask every time)", Constants.SetupSelectionPrompt);

        /// <summary>
        /// Initializes a new instance of the <see cref="SetupSelection"/> class.
        /// </summary>
        /// <param name="name">The display text for this setup selection entry.</param>
        /// <param name="value">The value of the setup selection entry.</param>
        public SetupSelection(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }

        /// <summary>
        /// Gets or sets the display text for this setup selection entry.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value of the setup selection entry.
        /// </summary>
        public string Value { get; set; }
    }

    /// <summary>
    /// List of possible associations for .sfm files.
    /// </summary>
    public enum SFMAssociation
    {
        /// <summary>
        /// Keeps the assocation of .sfm files to the system default.
        /// </summary>
        SystemDefined = 0,
        /// <summary>
        /// Opens .sfm files using SFMTK.
        /// </summary>
        OpenWithSFMTK = 1
    }

    /// <summary>
    /// List of possible associations for .dmx files.
    /// </summary>
    public enum DMXAssociation
    {
        /// <summary>
        /// Keeps the assocation of .dmx files to the system default.
        /// </summary>
        SystemDefined = 0,
        /// <summary>
        /// Opens .dmx files using SFM (sessions).
        /// </summary>
        OpenWithSFM = 1,
        /// <summary>
        /// Opens .dmx files using SFMTK.
        /// </summary>
        OpenWithSFMTK = 2,
        /// <summary>
        /// Ask what program to use to open the DMX file.
        /// </summary>
        AskEveryTime = 3
    }

    /// <summary>
    /// How to format titles of content.
    /// </summary>
    public enum TitleFormat
    {
        /// <summary>
        /// Include full file path.
        /// </summary>
        FullPath = 0,
        /// <summary>
        /// Include the local path to the file, starting at the mod directory's name.
        /// </summary>
        LocalPath = 1,
        /// <summary>
        /// Include the local path to the file, not including the mod directory's name.
        /// </summary>
        LocalPathWithoutMod = 2,
        /// <summary>
        /// Only include the name of the file (with extension).
        /// </summary>
        FileName = 3
    }

    /// <summary>
    /// How to store autosaves.
    /// </summary>
    public enum AutoSaveMode
    {
        /// <summary>
        /// Use an internal autosave format. Every file has autosaved versions stored in one per-user database.
        /// </summary>
        Internal = 0,
        /// <summary>
        /// Each file has its own .autosave version(s) included beside it.
        /// </summary>
        Individual = 1,
        /// <summary>
        /// Autosaves override the existing file. Not recommended to avoid data loss.
        /// </summary>
        Override = 2
    }

    /// <summary>
    /// How to handle saving with compile on save and with autocompiling while already compiling.
    /// </summary>
    public enum CompileOnSaveCancel
    {
        /// <summary>
        /// Force-restart the compile for the newly saved content.
        /// </summary>
        RestartCompile = 0,
        /// <summary>
        /// Do nothing, let the compile finish.
        /// </summary>
        Ignore = 1,
    }

    /// <summary>
    /// Opening an SFM file using SFMTK.
    /// </summary>
    public enum SFMFileSFMTKOptions
    {
        /// <summary>
        /// Directly open SFMTK + file already opened.
        /// </summary>
        OpenInSFMTK = 0,
        /// <summary>
        /// Do not show SFMTK, instead ask where/how to install directly.
        /// </summary>
        InstallForSFM = 1,
        /// <summary>
        /// Ask user with a simple dialog box what to do.
        /// </summary>
        AskEveryTime = 2
    }

    /// <summary>
    /// What to do on startup.
    /// </summary>
    public enum OnStartupSetting
    {
        /// <summary>
        /// Open the empty environment.
        /// </summary>
        Nothing = 0,
        /// <summary>
        /// Re-opens the files of the last open session.
        /// </summary>
        RecentFiles = 1,
        /// <summary>
        /// Executes the specified startup script.
        /// </summary>
        Script = 2
    }

    //The SettingChanging event is raised before a setting's value is changed.
    //The PropertyChanged event is raised after a setting's value is changed.
    //The SettingsLoaded event is raised after the setting values are loaded.
    //The SettingsSaving event is raised before the setting values are saved.

    /// <summary>
    /// Extended application settings keys.
    /// </summary>
    /// <seealso cref="System.Configuration.ApplicationSettingsBase" />
    public sealed partial class Settings {

        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        public Settings()
        {
            
        }

        /// <summary>
        /// Setting changing event handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Configuration.SettingChangingEventArgs"/> instance containing the event data.</param>
        private void SettingChangingEventHandler(object sender, System.Configuration.SettingChangingEventArgs e)
        {
            // Add code to handle the SettingChangingEvent event here.
        }

        /// <summary>
        /// Settings saving event handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void SettingsSavingEventHandler(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Add code to handle the SettingsSaving event here.
        }

        /// <summary>
        /// Loads only the theme settings, so SelectedTheme is initialized. Returns whether it was successful.
        /// </summary>
        public static bool LoadThemeSettings()
        {
            if (Program.Options.CommandMode)
                return false;

            try
            {
                Program.InitGUI();

                if (!UITheme.Themes.Any())
                    UITheme.LoadThemes();
                return true;
            }
            catch (Exception)
            {
                UITheme.SelectedTheme = UITheme.SystemDefaultTheme ?? new UITheme();
                return false;
            }
        }

        /// <summary>
        /// Sets the default settings values for every key that does not have a sensible default value and is currently set to its default.
        /// </summary>
        public static void SetDefaultSettings()
        {
            if (Default.LayoutOrder == null)
                Default.LayoutOrder = new List<string>();

            if (Default.RememberDialogSettings == null)
                Default.RememberDialogSettings = new List<RememberDialogSetting>();
            
            if (Default.Setups == null)
                Default.Setups = new List<Setup> { Setup.Default };

            if (Default.Notifications == null)
                Default.Notifications = new NotificationCollection();

            if (Default.ModInfo == null)
                Default.ModInfo = new List<Data.Mod>();

            if (string.IsNullOrEmpty(Default.DMXFileSFMSetup))
                Default.DMXFileSFMSetup = Constants.SetupSelectionActive;

            if (Default.Geometry == null)
                Default.Geometry = new List<WindowCoords>();

            if (Default.OlvStates == null)
                Default.OlvStates = new List<WindowOlvState>();

            UpdateDefaultModuleSettings();
        }

        internal static void UpdateDefaultModuleSettings()
        {
            if (Default.ModuleSettings == null)
                Default.ModuleSettings = new Modules.ModuleSettingCollection(Modules.ModuleManager.GetDefaultModules().Select(m => new Modules.ModuleSetting(m)));
        }

        /// <summary>
        /// Resets all settings to their default value, without confirmation and no undo possibility!
        /// </summary>
        public static void ResetSettings()
        {
            //TODO: Settings - reset settings that are redirected manually (e.g. file associations)

            //Reset regular properties
            Settings.Default.Reset();

            //Delete lastLayout.xml
            if (File.Exists(DockLayout.LastLayoutFile))
                File.Delete(DockLayout.LastLayoutFile);

            //Reload defaults so we don't get crashes
            SetDefaultSettings();
        }

        /// <summary>
        /// Upgrades the settings if old settings existed.
        /// </summary>
        public static void UpgradeSettings()
        {
            if (Settings.Default.FirstStartup)
            {
                try
                {
                    //This may throw a NullReferenceException if no previous version exists
                    //FIXME: Settings - shouldn't this copy over the CURRENT value of FirstStartup, and not the PreviousVersion's???
                    var wasfirst = (bool)Settings.Default.GetPreviousVersion("FirstStartup");

                    //BUG: Settings - upgrade does not copy over the old toolstrip settings!
                    Settings.Default.Upgrade();
                    Settings.Default.Save();

                    //Update whether it really was first startup
                    Settings.Default.FirstStartup = wasfirst;
                }
                catch (NullReferenceException)
                {
                    //Do not do anything else
                }
            }
        }

        /// <summary>
        /// Settings which are loaded asynchronously due to their irrelevant nature for the runtime or for longer calculation time.
        /// </summary>
        /// <param name="startup">If set to <c>true</c>, this is called from a startup procedure.</param>
        public static void LoadSettingsAsync(bool startup)
        {
            var taskdict = new Dictionary<Action, string>
            {
                { SFM.UpdateBasePath, "Reloading game info" },
                { DockLayout.LoadLayouts, "Parsing layout list" },
                { Data.Material.UpdateKnownShaders, "Parsing known shaders list" },
                { Forms.HelpPageDialog.ReloadHelpPages, "Help page list" },
                { Program.MainForm.AutoSaveManager.ScanAutosaveDatabase, "Autosave database" },
                //This step should be last, as it spawns a (for this context modal) dialog
                { ModuleLoading, "Lazy-loading modules" },
                //After modules are known, we can start watching the module directory
                //TODO: Settings - make directory watching of modules folder an option! -> see ModulePreferencePage call as well
                { Program.MainMM.StartDirectoryWatcher, "Monitoring modules directory" },
                //Do garbage collection to be safe
                { Program.GC, "Cleaning up" }
            };

            Program.MainForm.BackgroundTasks.AddTask(Tasks.BackgroundTask.FromDelegate(taskdict.Keys, taskdict.Values, true, "Loading settings... ({0})"));



            void ModuleLoading()
            {
                if (startup)
                {
                    //Load async modules last
                    Program.MainMM.ReachedStartupStep(Modules.ModuleLoadingContext.StartupLazyLoaded);

                    //Don't bother user on first startup, he won't have custom modules here anyway
                    if (Default.FirstStartup)
                        return;

                    //Are any new modules found in the directory?
                    Program.MainForm.InvokeIfRequired(Program.MainMM.CheckNewModules);
                }
            }
        }

        /// <summary>
        /// Gets the userconfig instance.
        /// </summary>
        public static Configuration UserConfig => ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);

        /// <summary>
        /// Updates the titles of all opened content. Automatically called on property change.
        /// </summary>
        public void UpdateContentTitles()
        {
            foreach (var c in Program.MainDPM.DockContentsTyped<Widgets.ContentInfoWidget>())
            {
                c.UpdateTitle();
            }
        }

        /// <summary>
        /// Updates the title of the main form. Automatically called on property change.
        /// </summary>
        public void UpdateMainTitle() => Program.MainForm.UpdateTitle();

        /// <summary>
        /// Updates the file associations using the values of SFMAssoc and DMXAssoc.
        /// </summary>
        public void UpdateFileAssociations()
        {
            //Store "last known association" if changing from "current assoc" to a SFM(TK) association
            var sfmassoc = IOHelper.FileTypeFromExtension(Constants.SFMTKExtension);
            if (!sfmassoc.ContainsIgnoreCase("SFMTK"))
                Settings.Default.SFMPreviousAssociation = sfmassoc;

            var dmxassoc = IOHelper.FileTypeFromExtension(Constants.DMXExtension);
            if (!dmxassoc.ContainsIgnoreCase("SFMTK"))
                Settings.Default.DMXPreviousAssociation = dmxassoc;

            //Apply SFM association
            switch (Settings.Default.SFMAssoc)
            {
                case SFMAssociation.SystemDefined:
                    //Use previous association, if any
                    IOHelper.SetAssociatedProgram(Constants.SFMTKExtension, Settings.Default.SFMPreviousAssociation, null);
                    break;
                case SFMAssociation.OpenWithSFMTK:
                    //Pass file(s) as input for SFMTK.exe, adjust flags based on action
                    IOHelper.SetAssociatedProgram(Constants.SFMTKExtension, Constants.SFMFileAssoc,
                        SFMApplicationFromAction(Settings.Default.SFMFileSFMTKAction, Settings.Default.SFMFileSFMTKParameters));
                    break;
            }

            //Apply DMX association
            switch (Settings.Default.DMXAssoc)
            {
                case DMXAssociation.SystemDefined:
                    //Use previous association, if any
                    IOHelper.SetAssociatedProgram(Constants.DMXExtension, Settings.Default.DMXPreviousAssociation, null);
                    break;
                default:
                    //Pass file(s) as input for SFMTK.exe, adjust flags based on action
                    IOHelper.SetAssociatedProgram(Constants.DMXExtension, Constants.DMXFileAssoc,
                        DMXApplicationFromAction(Settings.Default.DMXAssoc, Settings.Default.DMXFileSFMSetup, Settings.Default.DMXFileSFMTKParameters));
                    break;
            }



            string DMXApplicationFromAction(DMXAssociation assoc, string setupname, string parameters)
            {
                var prefix = "";
                var suffix = "";
                var file = "\"%1\"";
                switch (assoc)
                {
                    case DMXAssociation.OpenWithSFM:
                        //UNTESTED: RerouteDataSource - Use SFMTK to apply setup and launch SFM with DMX file as session file
                        //  If possible, check if valid session file beforehand? (Header should suffice!)
                        var setup = (setupname == Constants.SetupSelectionActive) ? string.Empty : $"s {setupname}";
                        prefix = $"-q{setup} --sfm-parameters \"-sfm_loadsession ";
                        file = "\\\"%1\\\"";
                        suffix = "\" --sfm-launch";
                        break;
                    case DMXAssociation.OpenWithSFMTK:
                        //No parameters need to be added when straight-up opening files
                        break;
                    case DMXAssociation.AskEveryTime:
                        //UNTESTED: RerouteDataSource - Prompt user about what to do with the DMX file, but only if it's a session file
                        //  Since SFM can't open other DMX data, simply use SFMTK in that case
                        prefix = "-q --prompt ";
                        break;
                }

                //Construct the executable string
                return $"\"{Application.ExecutablePath}\" {prefix}{string.Format(parameters, file)}{suffix} %*";
            }

            string SFMApplicationFromAction(SFMFileSFMTKOptions action, string parameters)
            {
                var prefix = "";
                var suffix = "";
                var file = "\"%1\"";
                switch (action)
                {
                    case SFMFileSFMTKOptions.AskEveryTime:
                        //UNTESTED: RerouteDataSource - Add flag to ask user what to do with .sfm file
                        prefix = "-q --prompt ";
                        break;
                    case SFMFileSFMTKOptions.InstallForSFM:
                        //UNTESTED: RerouteDataSource - Add flag to install .sfm file
                        prefix = "-q --install ";
                        break;
                    case SFMFileSFMTKOptions.OpenInSFMTK:
                        //No parameters need to be added when straight-up opening files
                        break;
                }

                //Construct the executable string
                return $"\"{Application.ExecutablePath}\" {prefix}{string.Format(parameters, file)}{suffix} %*";
            }
        }

        /// <summary>
        /// Updates the command history to fit the maximum undo step count.
        /// </summary>
        public void UpdateUndoSteps() => Commands.CommandHistory.UpdateAllBounds();

        /// <summary>
        /// Updates the currently active theme.
        /// </summary>
        public void UpdateTheme() => UITheme.ApplyThemeToAll();

        /// <summary>
        /// Updates the autosaving logic.
        /// </summary>
        public void UpdateAutosave() => Program.MainForm.UpdateAutosave();

        /// <summary>
        /// Updates the SFM base path.
        /// </summary>
        public void UpdateBasePath() => SFM.UpdateBasePath();

        /// <summary>
        /// Runs all update functions.
        /// </summary>
        public void UpdateAll()
        {
            //Updates all
            this.UpdateBasePath();
            this.UpdateFileAssociations();
            this.UpdateUndoSteps();
            this.UpdateMainTitle();
            this.UpdateContentTitles();
            this.UpdateTheme();
            this.UpdateAutosave();
        }



        /// <summary>
        /// Gets or sets the order of the layouts.
        /// </summary>
        /// <value>
        /// The layout order.
        /// </value>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public List<string> LayoutOrder
        {
            get => (List<string>)this["LayoutOrder"];
            set => this["LayoutOrder"] = value;
        }

        /// <summary>
        /// Gets or sets the remember dialog settings.
        /// </summary>
        /// <value>
        /// The remember dialog settings.
        /// </value>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public List<RememberDialogSetting> RememberDialogSettings
        {
            get => (List<RememberDialogSetting>)this["RememberDialogSettings"];
            set => this["RememberDialogSettings"] = value;
        }

        /// <summary>
        /// Gets or sets the list of setups.
        /// </summary>
        /// <value>
        /// The list of setups.
        /// </value>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public List<Setup> Setups
        {
            get => (List<Setup>)this["Setups"];
            set => this["Setups"] = value;
        }

        /// <summary>
        /// Gets or sets the list of notifications.
        /// </summary>
        /// <value>
        /// The list of notifications.
        /// </value>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public NotificationCollection Notifications
        {
            get => (NotificationCollection)this["Notifications"];
            set => this["Notifications"] = value;
        }


        /// <summary>
        /// Gets or sets the mod information. This should only be used to populate data in the gameinfo.txt, but not to generate the mod list!
        /// </summary>
        /// <value>
        /// The mod information.
        /// </value>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public List<Data.Mod> ModInfo
        {
            get => (List<Data.Mod>)this["ModInfo"];
            set => this["ModInfo"] = value;
        }

        /// <summary>
        /// Gets or sets the settings of all modules known.
        /// </summary>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public Modules.ModuleSettingCollection ModuleSettings
        {
            get => (Modules.ModuleSettingCollection)this["ModuleSettings"];
            set => this["ModuleSettings"] = value;
        }

        /// <summary>
        /// Gets or sets all default window geometry.
        /// </summary>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public List<WindowCoords> Geometry
        {
            get => (List<WindowCoords>)this["Geometry"];
            set => this["Geometry"] = value;
        }

        /// <summary>
        /// Gets or sets all default window geometry.
        /// </summary>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public List<WindowOlvState> OlvStates
        {
            get => (List<WindowOlvState>)this["OlvStates"];
            set => this["OlvStates"] = value;
        }
    }
}
