﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SFMTK.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.6.0.0")]
    public sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool MultipleDocks {
            get {
                return ((bool)(this["MultipleDocks"]));
            }
            set {
                this["MultipleDocks"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool StatusBar {
            get {
                return ((bool)(this["StatusBar"]));
            }
            set {
                this["StatusBar"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SFMPath {
            get {
                return ((string)(this["SFMPath"]));
            }
            set {
                this["SFMPath"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool AllowBesidesSFM {
            get {
                return ((bool)(this["AllowBesidesSFM"]));
            }
            set {
                this["AllowBesidesSFM"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SelectedTheme {
            get {
                return ((string)(this["SelectedTheme"]));
            }
            set {
                this["SelectedTheme"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("100")]
        public int UndoSteps {
            get {
                return ((int)(this["UndoSteps"]));
            }
            set {
                this["UndoSteps"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("OpenWithSFM")]
        public global::SFMTK.Properties.DMXAssociation DMXAssoc {
            get {
                return ((global::SFMTK.Properties.DMXAssociation)(this["DMXAssoc"]));
            }
            set {
                this["DMXAssoc"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("OpenWithSFMTK")]
        public global::SFMTK.Properties.SFMAssociation SFMAssoc {
            get {
                return ((global::SFMTK.Properties.SFMAssociation)(this["SFMAssoc"]));
            }
            set {
                this["SFMAssoc"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SimpleSaveConfirmation {
            get {
                return ((bool)(this["SimpleSaveConfirmation"]));
            }
            set {
                this["SimpleSaveConfirmation"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool AutoUnpinHLMV {
            get {
                return ((bool)(this["AutoUnpinHLMV"]));
            }
            set {
                this["AutoUnpinHLMV"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool ForceFallbackDialogs {
            get {
                return ((bool)(this["ForceFallbackDialogs"]));
            }
            set {
                this["ForceFallbackDialogs"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool ToolbarPerLayout {
            get {
                return ((bool)(this["ToolbarPerLayout"]));
            }
            set {
                this["ToolbarPerLayout"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("LocalPathWithoutMod")]
        public global::SFMTK.Properties.TitleFormat TabTitleFormat {
            get {
                return ((global::SFMTK.Properties.TitleFormat)(this["TabTitleFormat"]));
            }
            set {
                this["TabTitleFormat"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("FullPath")]
        public global::SFMTK.Properties.TitleFormat FullTitleFormat {
            get {
                return ((global::SFMTK.Properties.TitleFormat)(this["FullTitleFormat"]));
            }
            set {
                this["FullTitleFormat"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool ShowVersion {
            get {
                return ((bool)(this["ShowVersion"]));
            }
            set {
                this["ShowVersion"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool HideTitleName {
            get {
                return ((bool)(this["HideTitleName"]));
            }
            set {
                this["HideTitleName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool Autosaving {
            get {
                return ((bool)(this["Autosaving"]));
            }
            set {
                this["Autosaving"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("5")]
        public decimal AutosaveInterval {
            get {
                return ((decimal)(this["AutosaveInterval"]));
            }
            set {
                this["AutosaveInterval"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Internal")]
        public global::SFMTK.Properties.AutoSaveMode AutosaveMode {
            get {
                return ((global::SFMTK.Properties.AutoSaveMode)(this["AutosaveMode"]));
            }
            set {
                this["AutosaveMode"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool ShowSetup {
            get {
                return ((bool)(this["ShowSetup"]));
            }
            set {
                this["ShowSetup"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool FirstStartup {
            get {
                return ((bool)(this["FirstStartup"]));
            }
            set {
                this["FirstStartup"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool NewMenu {
            get {
                return ((bool)(this["NewMenu"]));
            }
            set {
                this["NewMenu"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Default")]
        public string ActiveSetup {
            get {
                return ((string)(this["ActiveSetup"]));
            }
            set {
                this["ActiveSetup"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool NativeFileBrowser {
            get {
                return ((bool)(this["NativeFileBrowser"]));
            }
            set {
                this["NativeFileBrowser"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("RestartCompile")]
        public global::SFMTK.Properties.CompileOnSaveCancel CompileOnSaveCancel {
            get {
                return ((global::SFMTK.Properties.CompileOnSaveCancel)(this["CompileOnSaveCancel"]));
            }
            set {
                this["CompileOnSaveCancel"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool CompileOnSave {
            get {
                return ((bool)(this["CompileOnSave"]));
            }
            set {
                this["CompileOnSave"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool CompileOnSaveModels {
            get {
                return ((bool)(this["CompileOnSaveModels"]));
            }
            set {
                this["CompileOnSaveModels"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool CompileOnSaveTextures {
            get {
                return ((bool)(this["CompileOnSaveTextures"]));
            }
            set {
                this["CompileOnSaveTextures"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("InstallForSFM")]
        public global::SFMTK.Properties.SFMFileSFMTKOptions SFMFileSFMTKAction {
            get {
                return ((global::SFMTK.Properties.SFMFileSFMTKOptions)(this["SFMFileSFMTKAction"]));
            }
            set {
                this["SFMFileSFMTKAction"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("{0}")]
        public string SFMFileSFMTKParameters {
            get {
                return ((string)(this["SFMFileSFMTKParameters"]));
            }
            set {
                this["SFMFileSFMTKParameters"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("{0}")]
        public string DMXFileSFMTKParameters {
            get {
                return ((string)(this["DMXFileSFMTKParameters"]));
            }
            set {
                this["DMXFileSFMTKParameters"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string DMXFileSFMSetup {
            get {
                return ((string)(this["DMXFileSFMSetup"]));
            }
            set {
                this["DMXFileSFMSetup"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SFMPreviousAssociation {
            get {
                return ((string)(this["SFMPreviousAssociation"]));
            }
            set {
                this["SFMPreviousAssociation"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string DMXPreviousAssociation {
            get {
                return ((string)(this["DMXPreviousAssociation"]));
            }
            set {
                this["DMXPreviousAssociation"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool TextEditorHighlightCurrentLine {
            get {
                return ((bool)(this["TextEditorHighlightCurrentLine"]));
            }
            set {
                this["TextEditorHighlightCurrentLine"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool TextEditorAllowScrollBelowDocument {
            get {
                return ((bool)(this["TextEditorAllowScrollBelowDocument"]));
            }
            set {
                this["TextEditorAllowScrollBelowDocument"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool TextEditorCutCopyWholeLine {
            get {
                return ((bool)(this["TextEditorCutCopyWholeLine"]));
            }
            set {
                this["TextEditorCutCopyWholeLine"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool TextEditorEnableVirtualSpace {
            get {
                return ((bool)(this["TextEditorEnableVirtualSpace"]));
            }
            set {
                this["TextEditorEnableVirtualSpace"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool TextEditorConvertTabsToSpaces {
            get {
                return ((bool)(this["TextEditorConvertTabsToSpaces"]));
            }
            set {
                this["TextEditorConvertTabsToSpaces"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool TextEditorShowEndOfLine {
            get {
                return ((bool)(this["TextEditorShowEndOfLine"]));
            }
            set {
                this["TextEditorShowEndOfLine"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool TextEditorShowWhitespace {
            get {
                return ((bool)(this["TextEditorShowWhitespace"]));
            }
            set {
                this["TextEditorShowWhitespace"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::SFMTK.Controls.FilterControlSettings DefaultFilterSettings {
            get {
                return ((global::SFMTK.Controls.FilterControlSettings)(this["DefaultFilterSettings"]));
            }
            set {
                this["DefaultFilterSettings"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Nothing")]
        public global::SFMTK.Properties.OnStartupSetting OnStartup {
            get {
                return ((global::SFMTK.Properties.OnStartupSetting)(this["OnStartup"]));
            }
            set {
                this["OnStartup"] = value;
            }
        }
    }
}
