﻿using System;
using System.IO;

namespace SFMTK
{
    /// <summary>
    /// An autosaved content file.
    /// </summary>
    public class AutoSavedContent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AutoSavedContent"/> class.
        /// </summary>
        public AutoSavedContent()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoSavedContent"/> class.
        /// </summary>
        public AutoSavedContent(string filepath)
        {
            this.FilePath = filepath;
            this.Id = Guid.NewGuid();
        }

        /// <summary>
        /// Gets or sets the local path to the autosaved content file.
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Gets the full path to the autosaved content file.
        /// </summary>
        public string FullPath => SFM.BasePath + this.FilePath;

        /// <summary>
        /// Gets or sets a Guid for identifying this content file's autosaved data.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets whether the file specified at FilePath is not found.
        /// </summary>
        public bool MissingFile { get; private set; }

        /// <summary>
        /// Updates the MissingFile state.
        /// </summary>
        public void UpdateMissingState() => this.MissingFile = !File.Exists(this.FullPath);
    }
}
