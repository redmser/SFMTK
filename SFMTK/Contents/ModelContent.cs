﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using SFMTK.Controls.ContentWidgets;
using SFMTK.Controls.InstanceSettings;
using SFMTK.Converters;

namespace SFMTK.Contents
{
    /// <summary>
    /// Any 3d meshes to be placed inside of your scene, may it be for scenebuilds or your main characters.
    /// </summary>
    public class ModelContent : Content, IInstantiatable
    {
        //IMP: ModelContent - how to handle source files vs compiled model content?
        //  -> should QC files be a separate content type? do users expect to be able to edit a model's QC just by opening the mdl file directly?
        //  -> opening a compiled model (any part of it) may just as well allow for ONLY hex editing certain data, analysing data, or to decompile for "full moddability"

        /// <summary>
        /// Creates a new instance of a widget which should display the Content data and be linked to it.
        /// <para />Any changes to the Content data from inside SFMTK (even ones that aren't done through the widget) should be shown.
        /// </summary>
        protected override ContentWidget CreateWidgetInternal() => new ContentWidget { Content = this }; //NYI: ModelContent - custom content widget for models

        /// <summary>
        /// Gets the default content converter for this content type. Used to save and load this content to its default format.
        /// </summary>
        public override DefaultContentConverter DefaultConverter => throw new NotImplementedException();

        /// <summary>
        /// Regex of the path format for identifying files corresponding to this content type. Defaults to a regex matching any files with the MainExtension.
        /// </summary>
        public override string PathFormat => @".+\.(mdl|sw\.vtx|dx90\.vtx|dx80\.vtx|vvd|phy|ani|phz|pre|cloth)$";

        /// <summary>
        /// Image to identify this content type. Size should be 32x32 (use Resize extension method if this is not guaranteed to be the case).
        /// </summary>
        public override Bitmap Image => new Bitmap(Properties.Resources.box);

        /// <summary>
        /// The main file extension to use for contents of this type (including the period). Used as the default extension for saving the content as a new file.
        /// </summary>
        public override string MainExtension => ".mdl";

        /// <summary>
        /// Gets a short informative text about the content type. Displayed in the New Content dialog, as a description.
        /// </summary>
        public string InfoText => "Any 3d meshes to be placed inside of your scene, may it be for scenebuilds or your main characters.";

        /// <summary>
        /// Returns a list of controls which are added to a flow panel at the bottom of the New Content dialog allowing the user to change
        /// the properties of the to-be-created content (such as the file name).
        /// </summary>
        public IEnumerable<IInstanceSetting<Content>> CreateInstanceSettings() => new[] { new NameInstanceSetting(this) };

        /// <summary>
        /// Matcheses the contents.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="path">The path.</param>
        public override bool MatchesContents(TextReader stream, string path) => false;
        //NYI: ModelContent - detect model by reading first couple of bytes
    }
}
