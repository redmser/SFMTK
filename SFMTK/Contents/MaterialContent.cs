﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using SFMTK.Controls.ContentWidgets;
using SFMTK.Controls.InstanceSettings;
using SFMTK.Converters;
using SFMTK.Data;
using SFMTK.Parsing.KeyValues;

namespace SFMTK.Contents
{
    /// <summary>
    /// Materials define how meshes or geometry are rendered by giving them properties and assigning textures and shaders.
    /// </summary>
    public class MaterialContent : Content, IInstantiatable
    {
        /// <summary>
        /// The main file extension to use for contents of this type (including the period!). Used as the default extension for saving the content as a new file.
        /// </summary>
        public override string MainExtension => ".vmt";

        /// <summary>
        /// Creates a new instance of a widget which should display the Content data and be linked to it.
        /// <para />Any changes to the Content data from inside SFMTK (even ones that aren't done through the widget) should be shown.
        /// </summary>
        protected override ContentWidget CreateWidgetInternal() => new MaterialContentWidget();

        /// <summary>
        /// Gets the default content converter for this content type. Used to save and load this content to its default format.
        /// </summary>
        public override DefaultContentConverter DefaultConverter { get; } = new MaterialVMTConverter();

        /// <summary>
        /// Returns all content that this content instance depends on.
        /// </summary>
        protected override IEnumerable<Content> GetDependenciesInternal()
        {
            var kvp = new KeyValuesTokenizer();
            kvp.Read(this.FilePath);
            foreach (var kv in kvp.Tokens.OfType<KVPair>())
            {
                //Any keys that have paths as value should be new content instances
                //FIXME: MaterialContent - better check for "paths as values"
                if (kv.Value == null || (!kv.Value.Contains("/") && !kv.Value.Contains("\\")))
                    continue;

                //NYI: MaterialContent - type of content: either material or (mostly) texture!
                //  !! This is creating a new content each time. use cached version (probably a separate function to get cached Content if possible!)
                yield return Content.NewContentOfType(kv.Value, false, typeof(MaterialContent));
            }
        }

        /// <summary>
        /// Gets a short informative text about the content type. Displayed in the New Content dialog, as a description.
        /// </summary>
        public string InfoText => "Materials define how meshes or geometry are rendered by giving them properties and assigning textures and shaders.";

        /// <summary>
        /// Returns a list of controls which are added to a flow panel at the bottom of the New Content dialog allowing the user to change
        /// the properties of the to-be-created content (such as the file name).
        /// </summary>
        public IEnumerable<IInstanceSetting<Content>> CreateInstanceSettings() => new[] { new NameInstanceSetting(this) };

        /// <summary>
        /// Regex of the path format for identifying files corresponding to this content type. Defaults to a regex matching any files with the MainExtension.
        /// </summary>
        public override string PathFormat => @".+\.(vmt|spr|sht)$";

        /// <summary>
        /// Image to identify this content type. Size should be 32x32 (use Resize extension method if this is not guaranteed to be the case).
        /// </summary>
        public override Bitmap Image => new Bitmap(Properties.Resources.material);

        /// <summary>
        /// Returns whether the specified contents of a file (given through a stream) match the expected Content of this type.
        /// </summary>
        /// <param name="stream">Content data read off the stream. May be null!</param>
        /// <param name="path">Use this path to manually read this data in case the content stream is null.</param>
        public override bool MatchesContents(TextReader stream, string path)
        {
            /*
            //Check if first line is a known shader
            stream = stream ?? new StreamReader(File.OpenRead(path));
            try
            {
                var foundshader = false;
                var kvp = new KeyValuesParser();
                var nodes = new List<KeyValue>();
                do
                {
                    var ln = stream.ReadLine();
                    nodes.AddRange(kvp.ParsePart(ln));
                    foundshader = nodes.Any(n => Shader.IsShader(n.Key));
                    if (nodes.Count > 1)
                        break; //Checked more than one entry without finding a known shader
                } while (!foundshader);

                //TODO: MaterialContent - possibly check for known $keys if unknown shader?
                return foundshader;
            }
            finally
            {
                stream.Close();
            }
            */
            return false;
        }
    }
}
