﻿using System;

namespace SFMTK.Contents
{
    /// <summary>
    /// An attribute to specify a Content's type name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class ContentTypeNameAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentTypeNameAttribute"/> class.
        /// </summary>
        /// <param name="name">The type name of the content.</param>
        public ContentTypeNameAttribute(string name)
        {
            this.TypeName = name;
        }

        /// <summary>
        /// Gets the type name of the content.
        /// </summary>
        public string TypeName { get; }
    }
}
