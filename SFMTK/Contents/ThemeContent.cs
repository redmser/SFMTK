﻿using System.Collections.Generic;
using SFMTK.Controls.ContentWidgets;
using SFMTK.Controls.InstanceSettings;
using SFMTK.Converters;
using SFMTK.Widgets;

namespace SFMTK.Contents
{
    /// <summary>
    /// A theme file for SFMTK, allowing you to change how the program's interface looks like. Backing data lies in UITheme instances.
    /// </summary>
    [ContentTypeName("SFMTK Theme")]
    public class ThemeContent : Content, IInstantiatable
    {
        //BUG: ThemeContent - renaming a theme currently does not work well, since "SelectedTheme" would then return null

        /// <summary>
        /// Initializes a new instance of the <see cref="ThemeContent"/> class.
        /// </summary>
        public ThemeContent() : this(new UITheme())
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ThemeContent"/> class.
        /// </summary>
        /// <param name="theme">The theme to load.</param>
        public ThemeContent(UITheme theme) : base()
        {
            this.InitializeData(theme);
        }

        /// <summary>
        /// Full file path of the content's data. This only represents the immediately linked content file,
        /// meaning there may be multiple files actually responsible for the content.
        /// <para />If the file is not moved after changing <see cref="FilePath" />, you may want to set <see cref="IsDirty" /> to <c>true</c>.
        /// </summary>
        /// <remarks>
        /// This may not be a valid path if <see cref="FakeFile" /> is set to <c>true</c>, since then it only represents a default filename and file title.
        /// </remarks>
        public override string FilePath
        {
            get => this.Theme.Path;
            set
            {
                this.Theme.Path = value;
                if (this.FakeFile)
                    this.Theme.Name = value.Replace(".theme", "");
            }
        }

        public UITheme Theme => (UITheme)this.Data;

        /// <summary>
        /// Gets the default content converter for this content type. Used to save and load this content to its default format.
        /// </summary>
        public override DefaultContentConverter DefaultConverter => new ThemeFileConverter();

        /// <summary>
        /// The main file extension to use for contents of this type (including the period!). Used as the default extension for saving the content as a new file.
        /// </summary>
        public override string MainExtension => ".theme";

        /// <summary>
        /// Creates a new instance of a widget which should display the Content data and be linked to it.
        /// <para />Any changes to the Content data from inside SFMTK (even ones that aren't done through the widget) should be shown.
        /// </summary>
        protected override ContentWidget CreateWidgetInternal() => new ThemeContentWidget();

        /// <summary>
        /// Gets a short informative text about the content type. Displayed in the New Content dialog, as a description.
        /// </summary>
        public string InfoText => "A theme file for SFMTK, allowing you to change how the program's interface looks like.";

        /// <summary>
        /// Returns a list of controls which are added to a flow panel at the bottom of the New Content dialog allowing the user to change
        /// the properties of the to-be-created content (such as the file name).
        /// </summary>
        public IEnumerable<IInstanceSetting<Content>> CreateInstanceSettings() => new[] { new NameInstanceSetting(this) };
    }
}
