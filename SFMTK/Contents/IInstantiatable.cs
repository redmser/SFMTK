﻿using System.Collections.Generic;
using SFMTK.Controls.InstanceSettings;

namespace SFMTK.Contents
{
    /// <summary>
    /// Represents a content type which can be instantiated using the New Content dialog and menu options.
    /// </summary>
    public interface IInstantiatable
    {
        /// <summary>
        /// Gets a short informative text about the content type. Displayed in the New Content dialog, as a description.
        /// </summary>
        string InfoText { get; }

        /// <summary>
        /// Returns a list of settings allowing the user to change the properties of the to-be-created content (such as the file name).
        /// They are added as controls to a flow panel at the bottom of the New Content dialog if of type <see cref="System.Windows.Forms.Control"/>.
        /// </summary>
        IEnumerable<IInstanceSetting<Content>> CreateInstanceSettings();
    }
}
