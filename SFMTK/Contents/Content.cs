﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Microsoft.Msagl.Drawing;
using SFMTK.Controls.ContentWidgets;
using SFMTK.Converters;
using SFMTK.Data;
using SFMTK.Errors;
using SFMTK.Widgets;

namespace SFMTK.Contents
{
    /// <summary>
    /// Container of any data linked to SFM's file system, mostly displayed in a corresponding content widget.
    /// <para/>Should implement <see cref="IInstantiatable"/> if you want it to be creatable using the New Content dialog and similar.
    /// </summary>
    public abstract class Content
    {
        // Notes to anyone looking to extend the Content system:
        // - Any new content may create its own content subclass and must have a ContentWidget assigned to it.
        // - Content may link to other content, instead of containing info of multiple files inside of it
        //   (unless the files do not work independently of each other and it does not make sense to keep them separate).
        // - How the data is stored (separate class, properties, etc.) is up to the implementation.
        //   It should not, however, work DIRECTLY on an external data source, but store and modify it in form of variables or sorts.

        //IMP: Content - the node tree generated in the ContentBrowser has to be stored as a tree of Content instances
        //  -> used to get the same Content instances across different use cases (such as for calculating dependencies)
        //  -> only a constantly growing cache - should not contain ALL content instances of ALL files known to SFM!

        //IMP: Content - any calls to I/O files should be cached using a per-Content global system (do not hide inside of the untyped Data property!)
        //  -> should also support only reading parts of a file and later finishing that read (such as for checking headers, later appending main content, etc)
        //  -> allow for streaming data that is only needed in certain contexts (such as vertex data in models - it is not really required to keep it all in memory at all times)

        //TODO: Content - open gameinfo.txt: option to either open mod manager OR to raw-edit the txt file
        //  -> "Would you like to use the graphical Mod Manager window to modify your gameinfo.txt file instead?" with a "Remember this decision" checkbox

        //TODO: Content - figure the max limit of mods/content before user having to figure out with sfm crashes on startup/while loading said content
        //  -> similarly, show warnings/errors in the error list about too high values for certain values (such as the 2048 dispinfos on maps, 128 flex controllers, ...)

        /// <summary>
        /// Avoid using the constructor, as the method <see cref="NewContentOfType"/> handles content creation.
        /// </summary>
        protected Content()
        {
            
        }

        /// <summary>
        /// List of basic <see cref="Content"/> instances.
        /// Since this list is only instanciated once, the instances should not be modified directly.
        /// </summary>
        public static List<Content> ContentList { get; } = Reflection.GetSubclassInstances<Content>(Modules.ModuleScope.None).ToList();

        /// <summary>
        /// Gets or sets the data assigned to this Content. Default value is used for newly created content instances.
        /// <para/>If using a constructor to populate data, you may want to use <see cref="InitializeData(IData)"/> instead.
        /// <para/>Updating the value of this property sets <see cref="IsDirty"/> to <c>true</c> if data was loaded before, since the data was modified.
        /// </summary>
        //UNTESTED: Content - how is content instanciated? do we need to use a factory for creating the default data instance, or can
        //  we simply init it once and let it be used everywhere?
        public virtual IData Data
        {
            get => this._data;
            set
            {
                if (this._data == value)
                    return;

                var wasdatanull = this._data == null;

                //Initialize events etc on data
                this.InitializeData(value, true);

                //Only make dirty if there was data loaded before
                if (!wasdatanull)
                {
                    //Has changed data itself
                    this.OnDataChanged();
                }
            }
        }
        private IData _data;

        /// <summary>
        /// Initializes the value of <see cref="Data" /> without causing the <see cref="E:DataChanged"/> event to be raised. Used when initializing the data.
        /// </summary>
        /// <param name="data">New value for the data.</param>
        /// <param name="events">If set to <c>true</c>, updates the events associated with the <see cref="IData"/> as well.</param>
        public void InitializeData(IData data, bool events = true)
        {
            if (events)
            {
                if (this._data != null)
                    this._data.PropertyChanged -= this.Data_PropertyChanged;

                this._data = data;

                if (this._data != null)
                    this._data.PropertyChanged += this.Data_PropertyChanged;
            }
            else
            {
                this._data = data;
            }
        }

        /// <summary>
        /// Occurs when any data assigned to the content has changed.
        /// </summary>
        public event PropertyChangedEventHandler DataChanged;

        /// <summary>
        /// Raises the <see cref="E:DataChanged" /> event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnDataChanged([CallerMemberName] string propertyName = null) => this.Data_PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

        private void Data_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.IsDirty = true;
            this.DataChanged?.Invoke(sender, e);
        }

        /// <summary>
        /// Returns the formatted file path, by applying the specified format.
        /// </summary>
        /// <param name="format">The format to apply to the file path.</param>
        /// <param name="includeSuffix">If set to <c>true</c>, includes the unsaved file suffix.</param>
        public virtual string GetFormattedName(Properties.TitleFormat format, bool includeSuffix)
        {
            var suffix = (includeSuffix && this.IsDirty ? "*" : "");
            var path = StringHelper.FormatPath(this.FilePath, format);
            if (string.IsNullOrEmpty(path))
                return path;
            return path + suffix;
        }

        /// <summary>
        /// Full file path of the content's data. This only represents the immediately linked content file,
        /// meaning there may be multiple files actually responsible for the content.
        /// <para/>If the file is not moved after changing <see cref="FilePath"/>, you may want to set <see cref="IsDirty"/> to <c>true</c>.
        /// </summary>
        /// <remarks>This may not be a valid path if <see cref="FakeFile"/> is set to <c>true</c>, since then it only represents a default filename and file title.</remarks>
        public virtual string FilePath
        {
            get => this._filePath;
            set
            {
                if (this._filePath == value)
                    return;

                if (this.FakeFile)
                {
                    //Set directly
                    this._filePath = value;
                }
                else
                {
                    //Fix up path when setting
                    this._filePath = value.EndsWith(this.MainExtension, StringComparison.InvariantCultureIgnoreCase) ? value : value + this.MainExtension;
                }

                this.OnFilePathChanged(EventArgs.Empty);
            }
        }
        private string _filePath;

        /// <summary>
        /// Shows the save prompt for this Content, even if it is not dirty. Returns the pressed button of the dialog, or Cancel if an error has occured while saving.
        /// </summary>
        /// <remarks>Never uses the "modern save dialog" style, which is only used for multiple Content instances.</remarks>
        public virtual DialogResult ShowSavePrompt()
        {
            var res = FallbackTaskDialog.ShowDialog($"Save changes to \"{this.GetFormattedName(Properties.Settings.Default.FullTitleFormat, false)}\"?",
                "Save pending changes?", "Save changes", FallbackDialogIcon.Info,
                new FallbackDialogButton(DialogResult.Yes), new FallbackDialogButton(DialogResult.No), new FallbackDialogButton(DialogResult.Cancel));

            if (res == DialogResult.Yes)
            {
                var err = SaveFile(this);
                if (!err)
                    return DialogResult.Cancel;
            }
            return res;
        }

        /// <summary>
        /// Gets the default content converter for this content type. Used to save and load this content to its default format.
        /// </summary>
        public abstract DefaultContentConverter DefaultConverter { get; }

        /// <summary>
        /// Loads data from the file assigned to this content.
        /// </summary>
        /// <exception cref="ContentIOException">An error occurred loading the content.</exception>
        public virtual void Load() => this.Load(this.FilePath, false);

        /// <summary>
        /// Saves data of this content to the assigned file.
        /// </summary>
        /// <exception cref="ContentIOException">An error occurred saving the content.</exception>
        public virtual void Save() => this.Save(this.FilePath, false);

        /// <summary>
        /// Loads data from the specified file.
        /// </summary>
        /// <param name="path">Standardized file path to load data from.</param>
        /// <param name="updatePath">If set to <c>true</c>, updates the FilePath property after loading has finished.</param>
        /// <exception cref="ContentIOException">An error occurred loading the content.</exception>
        public virtual void Load(string path, bool updatePath)
        {
            this.Data = this.DefaultConverter.Import(path);
            if (updatePath)
                this.FilePath = path;
        }

        /// <summary>
        /// Saves data to the specified file.
        /// </summary>
        /// <param name="path">Standardized file path to save data to.</param>
        /// <param name="updatePath">If set to <c>true</c>, updates the FilePath property after loading has finished.</param>
        /// <exception cref="ContentIOException">An error occurred saving the content.</exception>
        public virtual void Save(string path, bool updatePath)
        {
            this.DefaultConverter.Export(this.Data, path);
            if (updatePath)
                this.FilePath = path;
        }

        /// <summary>
        /// Returns a default name for a new content of this type. May be duplicate if not all contents exist in form of a widget on the main DockPanel.
        /// </summary>
        public virtual string GetNewName() => Program.MainDPM.GetNewContentName(this);

        /// <summary>
        /// Returns whether this content instance depends on the specified content.
        /// </summary>
        /// <param name="content"></param>
        public bool DependsOn(Content content)
        {
            //Get dependencies
            var dep = this.Dependencies;

            //Check if content is part of them
            return dep.Any(c => c == content);
        }

        /// <summary>
        /// Returns whether this content instance is a dependency of the specified content.
        /// </summary>
        public bool DependencyOf(Content content) => content.DependsOn(this);

        /// <summary>
        /// Returns whether this content's main file exists and can be written to/read from.
        /// </summary>
        public virtual bool CanReadWrite => !this.FakeFile && !string.IsNullOrWhiteSpace(this.FilePath) && File.Exists(this.FilePath);

        /// <summary>
        /// Gets all content that this content instance depends on.
        /// </summary>
        public IEnumerable<Content> Dependencies
        {
            get
            {
                //TODO: Content - should dependencies internally be stored using Content instances?
                //  -> see ContentTreeSerializer TODO about dependencies for some ideas
                if (this._dependencies == null)
                    this._dependencies = this.GetDependencies(false);
                return this._dependencies;
            }
        }
        private IEnumerable<Content> _dependencies;

        /// <summary>
        /// Sets the list of dependencies of this content.
        /// </summary>
        /// <param name="deps">The new list of dependencies.</param>
        public void SetDependencies(IEnumerable<Content> deps) => this._dependencies = deps;

        /// <summary>
        /// Returns all content that this content instance depends on, unless the Content has not been saved.
        /// </summary>
        /// <param name="loadContent">If set to <c>true</c>, will load all returned content, considering the corresponding files also exist.</param>
        public virtual IEnumerable<Content> GetDependencies(bool loadContent)
        {
            if (this.FakeFile)
                yield break;
            foreach (var dep in this.GetDependenciesInternal())
            {
                if (loadContent && dep.CanReadWrite)
                    dep.Load();
                yield return dep;
            }
        }

        /// <summary>
        /// Returns all content that this content instance depends on. Any created content should not be loaded on its own.
        /// </summary>
        protected virtual IEnumerable<Content> GetDependenciesInternal()
        {
            yield break;
        }

        //IMP: Content - why is this so messy (CreateWidget(Internal) and CreateContainer)? just use a simple fucking system wtf

        /// <summary>
        /// Creates a new instance of a widget which should display the Content data and be linked to it.
        /// <para />This call may return <c>null</c> if the widget should not be created. This should not be due to errors, but due to user's choice.
        /// </summary>
        public ContentWidget CreateWidget()
        {
            var cw = this.CreateWidgetInternal();
            cw.Content = this;
            return cw;
        }

        /// <summary>
        /// Creates a new instance of a widget which should display the Content data and be linked to it.
        /// <para/>Any changes to the Content data from inside SFMTK (even ones that aren't done through the widget) should be shown.
        /// </summary>
        protected abstract ContentWidget CreateWidgetInternal();

        /// <summary>
        /// Creates the main widget to display both Content data and general toggles which are shown for every Content container.
        /// <para/>Rather than overriding this in any custom Content class, try to generalize the ContentInfoWidget more.
        /// </summary>
        public ContentInfoWidget CreateContainer() => new ContentInfoWidget(this.CreateWidget());
        //TODO: Content - provide properties that allow modifying how ContentInfoWidget should be instantiated (such as excluding/editing the header toolstrip)

        /// <summary>
        /// Gets a list of errors which can be looked at from an Error List widget.
        /// <para/>Is updated as watched data changes. Get will also update the <see cref="ContentError.Content"/> property.
        /// </summary>
        //TODO: Content - when saving (compiling?) content with errors, notify user
        public ContentErrorList Errors
        {
            get
            {
                var errors = this.ErrorHandlers.Errors;
                foreach (var error in errors)
                    error.Content = this;
                return errors;
            }
        }

        /// <summary>
        /// Gets a list of error handlers which automatically manage the error list for this content.
        /// <para/>This list is populated using the <see cref="ErrorHandlerContentTypeAttribute"/> attribute.
        /// </summary>
        public ErrorHandlerList ErrorHandlers { get; } = new ErrorHandlerList();
        
        /// <summary>
        /// Gets or sets whether the content is dirty, meaning whether it has modified data which should be saved.
        /// </summary>
        public virtual bool IsDirty
        {
            get => this._isDirty;
            set
            {
                if (this._isDirty == value)
                    return;

                this._isDirty = value;
                this.OnIsDirtyChanged(EventArgs.Empty);
            }
        }

        private bool _isDirty;

        /// <summary>
        /// Gets the current dependency graph, or generates a new one if none has been created before. Use UpdateDependencyGraph to recreate.
        /// </summary>
        public Graph DependencyGraph
        {
            get
            {
                if (this._dependencyGraph == null)
                    this.UpdateDependencyGraph();
                return this._dependencyGraph;
            }
        }
        private Graph _dependencyGraph;

        /// <summary>
        /// Updates the dependency graph of this content.
        /// </summary>
        public virtual void UpdateDependencyGraph()
        {
            this._dependencyGraph = this.UpdateDependencyGraphInternal();

            //Theme the graph
            UITheme.SelectedTheme.ApplyGraphTheme(this._dependencyGraph);

            //Special coloring
            //TODO: Content/DependencyGraphViewer/UITheme - graph-only settings for all of their colors
            var e = this._dependencyGraph.FindNode(Constants.FindUsagesNodeId).Edges.Single();
            e.TargetNode.Attr.FillColor = UITheme.SelectedTheme.StatusBarLoadingBackColor.ToMsaglColor();
            e.SourceNode.Attr.FillColor = UITheme.SelectedTheme.StatusBarBackColor.ToMsaglColor();
        }

        /// <summary>
        /// Updates the dependency graph of this content.
        /// </summary>
        protected virtual Graph UpdateDependencyGraphInternal()
        {
            var graph = new Graph();

            //Create own Content node pointing to "unknown" parents
            var e = graph.AddEdge(this.FilePath, Constants.FindUsagesNodeId);
            e.SourceNode.LabelText = this.GetFormattedName(Properties.TitleFormat.FileName, false);
            e.TargetNode.LabelText = "Double-click to find usages...";

            //TODO: DependencyGraphViewer/Content - add icons to graph nodes -> custom node renderer (abstact to theme?)
            /*e.SourceNode.DrawNodeDelegate = (n, o) =>
            {
                if (o is Graphics g)
                {
                    //Get content
                    //var c = n.UserData as Content;
                    //if (c != null)
                    {
                        //Draw image (why is the Y coordinate on the bounding box's bottom?)
                        g.FillRectangle(Brushes.Green, (int)n.BoundingBox.Left, (int)n.BoundingBox.Bottom, (int)n.BoundingBox.Width, (int)n.BoundingBox.Height);
                    }
                }
                return true;
            };*/

            //Add all content used by this content
#if DEBUG
            //Add some testing elements
            graph.AddEdge("Materials", this.FilePath);
            graph.AddEdge("Meshes", this.FilePath);
            foreach (var dep in this.Dependencies)
            {
                graph.AddEdge(dep.GetFormattedName(Properties.TitleFormat.FileName, false), "Materials");
            }
#endif

            return graph;
        }

        /// <summary>
        /// If set to <c>true</c>, this Content is fake. It shows up like a new content, and has to be saved first,
        /// but can have a (mostly temporary) FilePath assigned to it to get its data from.
        /// <para/>Use when creating Content from data without it having a file to represent this data. Will be set to false once saved.
        /// </summary>
        public bool FakeFile
        {
            get => this._fakeFile;
            set
            {
                if (this._fakeFile == value)
                    return;

                this._fakeFile = value;
                this.OnFakeFileChanged(EventArgs.Empty);
            }
        }
        private bool _fakeFile;

        /// <summary>
        /// Regex of the path format for identifying files corresponding to this content type. Defaults to a regex matching any files with the MainExtension.
        /// </summary>
        public virtual string PathFormat => @".+\" + this.MainExtension;

        /// <summary>
        /// The main file extension to use for contents of this type (including the period!). Used as the default extension for saving the content as a new file.
        /// </summary>
        public abstract string MainExtension { get; }

        /// <summary>
        /// Image to identify this content type. Size should be 32x32 (use Resize extension method if this is not guaranteed to be the case).
        /// </summary>
        public virtual Bitmap Image => ErrorImage;

        private static Bitmap ErrorImage { get; } = DrawingHelper.GetErrorImage().Resize(32, 32);

        /// <summary>
        /// Image to identify this content type. Size should be 16x16, generated from the Image property by default.
        /// </summary>
        public virtual Bitmap SmallImage
        {
            get
            {
                if (this._smallImage == null)
                    this._smallImage = this.Image.Resize(16, 16);
                return this._smallImage;
            }
        }
        private Bitmap _smallImage;

        /// <summary>
        /// Returns whether the specified path matches the PathFormat regex, meaning this path correspond to the Content of this type.
        /// <para/>By default, the PathFormat regex is matched on the given full file path.
        /// </summary>
        /// <param name="path">The full file path to validate.</param>
        public virtual bool MatchesPath(string path) => Regex.IsMatch(path, this.PathFormat);

        /// <summary>
        /// Returns whether the specified contents of a file (given through a stream) match the expected Content of this type.
        /// </summary>
        /// <param name="stream">Content data read off the stream. May be null!</param>
        /// <param name="path">Use this path to manually read this data in case the content stream is null.</param>
        public virtual bool MatchesContents(TextReader stream, string path) => false;

        /// <summary>
        /// Raises the <see cref="E:IsDirtyChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnIsDirtyChanged(EventArgs e) => this.IsDirtyChanged?.Invoke(this, e);

        /// <summary>
        /// Raises the <see cref="E:FilePathChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnFilePathChanged(EventArgs e) => this.FilePathChanged?.Invoke(this, e);

        /// <summary>
        /// Raises the <see cref="E:FakeFileChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnFakeFileChanged(EventArgs e) => this.FakeFileChanged?.Invoke(this, e);

        /// <summary>
        /// Occurs when IsDirty changed - the file's data was modified.
        /// </summary>
        public event EventHandler IsDirtyChanged;

        /// <summary>
        /// Occurs when the FilePath property has changed.
        /// </summary>
        public event EventHandler FilePathChanged;

        /// <summary>
        /// Occurs when the FakeFile property has changed.
        /// </summary>
        public event EventHandler FakeFileChanged;

        #region Static Methods

        /// <summary>
        /// Gets the type name of the specified content type.
        /// </summary>
        /// <param name="type">Type of the content.</param>
        public static string GetTypeName(Type type)
        {
            if (type == null)
                return null;

            //Get custom attribute of type
            var ctn = (ContentTypeNameAttribute)type.GetCustomAttributes(typeof(ContentTypeNameAttribute), true).SingleOrDefault();

            //Fallback value
            if (ctn == null)
            {
                var name = type.Name;
                return name.EndsWith(nameof(Content)) ? name.Substring(0, name.Length - nameof(Content).Length) : name;
            }

            //Overridden type name
            return ctn.TypeName;
        }

        /// <summary>
        /// Gets the type name of the specified content type.
        /// </summary>
        /// <typeparam name="T">Type of the content.</typeparam>
        public static string GetTypeName<T>() where T : Content => GetTypeName(typeof(T));

        /// <summary>
        /// Gets the type name of the specified content instance.
        /// </summary>
        /// <param name="content">The content to get the type of.</param>
        public static string GetTypeName(Content content) => GetTypeName(content?.GetType());

        /// <summary>
        /// Gets the type name of this content instance.
        /// </summary>
        public string TypeName => GetTypeName(this);

        /// <summary>
        /// Creates a new Content of the specified type, using the specified file as an input.
        /// </summary>
        /// <param name="path">The path to the content, used for file type detection and set as the initial file path of the Content.</param>
        /// <param name="load">If set to <c>true</c>, also loads the specified file after creating it.</param>
        /// <param name="type">The type of content to create (any class inheriting from Content). Use null to make it auto detect using the given mode.</param>
        /// <param name="mode">How to detect the content type. Only used if Type is null.</param>
        /// <exception cref="ArgumentException">Could not determine the corresponding content type for the specified file.</exception>
        public static Content NewContentOfType(string path, bool load, Type type, ContentDetectMode mode = ContentDetectMode.AutoDetect)
        {
            //Get content Type from filetype
            var t = type ?? DetectFileType(path, null, mode);
            var ins = (Content)Activator.CreateInstance(t);
            if (ins == null)
                throw new ArgumentException("Could not determine the corresponding content type for the specified file.");
            ins.FilePath = path;
            if (load)
                ins.Load();
            return ins;
        }

        /// <summary>
        /// Returns the default content with the specified Type. Taken from <see cref="ContentList"/>, reinstantiate instead!
        /// </summary>
        /// <param name="t">The type to search for.</param>
        public static Content ContentByType(Type t) => ContentList.FirstOrDefault(c => c.GetType() == t);

        /// <summary>
        /// Detects the file type of the specified string using the specified detect mode.
        /// <para />If detection failed or if the file type is unknown, null is returned.
        /// </summary>
        /// <param name="path">Path of the file. If null, will not be used to detect.</param>
        /// <param name="contents">Contents of the file. If null, will not be used to detect.</param>
        /// <param name="type">The detection mode for the data given.</param>
        /// <exception cref="ArgumentNullException">contents - Both contents and path are null.</exception>
        /// <exception cref="InvalidOperationException">ContentFileType was not a detect type.</exception>
        public static Type DetectFileType(string path, TextReader contents, ContentDetectMode type = ContentDetectMode.AutoDetect)
        {
            //Skip check
            if (type == ContentDetectMode.None)
                return null;

            //Null check
            if (contents == null && path == null)
                throw new ArgumentNullException(nameof(contents), "Both contents and path are null.");

            //Return null if detecting for contents and having no contents
            if (type == ContentDetectMode.DetectFromContents && contents == null)
                return null;

            //Return null if detecting for extension and having no extension
            if (type == ContentDetectMode.DetectFromPath && path == null)
                return null;

            //GetFileType using contents / path
            switch (type)
            {
                case ContentDetectMode.DetectFromContents:
                    return DetectFromContents(contents, path);
                case ContentDetectMode.DetectFromPath:
                    return DetectFromPath(path);
                case ContentDetectMode.AutoDetect:
                    return DetectFromPath(path) ?? DetectFromContents(contents, path);
            }

            throw new InvalidOperationException("ContentFileType was not a detect type.");
        }

        /// <summary>
        /// Detect the ContentFileType from the given file contents. Returns null if the type could not be detected.
        /// </summary>
        /// <param name="path">The path to read the string data from, in case no contents were specified.</param>
        /// <param name="contents">The file contents to use for detecting the file type.</param>
        /// <param name="allowNull">If set to <c>true</c>, allow null as a path as well (returning null immediately) - otherwise throws an exception.</param>
        /// <exception cref="System.ArgumentNullException">contents - Can't detect file type for null contents, additionally no path for fallback specified.</exception>
        /// <exception cref="ArgumentNullException">contents - Can't detect file type for null contents.</exception>
        private static Type DetectFromContents(TextReader contents, string path, bool allowNull = true)
        {
            if (path == null && contents == null)
                return allowNull ? (Type)null : throw new ArgumentNullException(nameof(contents), "Can't detect file type for null contents, additionally no path for fallback specified.");

            //Match all content
            foreach (var c in ContentList)
            {
                //May not open a non-existing file
                if (contents == null && !File.Exists(path))
                    continue;

                if (c.MatchesContents(contents, path))
                    return c.GetType();
            }

            return null;
        }

        /// <summary>
        /// Detect the ContentFileType from the given path. Returns null if the type could not be detected.
        /// </summary>
        /// <param name="path">The file path to use for detecting the file type.</param>
        /// <param name="allowNull">If set to <c>true</c>, allow null as a path as well (returning null immediately) - otherwise throws an exception.</param>
        /// <exception cref="ArgumentNullException">path - Can't detect file type for null path.</exception>
        private static Type DetectFromPath(string path, bool allowNull = true)
        {
            if (path == null)
                return allowNull ? (Type)null: throw new ArgumentNullException(nameof(path), "Can't detect file type for null path.");

            //Match all path regexes
            foreach (var c in ContentList)
            {
                if (c.MatchesPath(path))
                    return c.GetType();
            }

            return null;
        }


        /// <summary>
        /// Saves the content to its file path, or asking for a directory if the path is invalid or not yet specified.
        /// <para />Returns whether the Content was saved.
        /// </summary>
        /// <param name="content">The content to save.</param>
        /// <param name="showUI">If set to <c>true</c>, shows additional user interface elements, such as an error message box or exporter settings.</param>
        public static bool SaveFile(Content content, bool showUI = true)
        {
            string path;
            if (content.FakeFile || string.IsNullOrWhiteSpace(content.FilePath))
            {
                //Prompt user on path
                path = IOHelper.SaveFileDialog(content, true, showUI);

                if (path == null)
                    return false;
            }
            else
            {
                //Already have a path
                path = content.FilePath;
            }

            //Save the content on the path
            try
            {
                content.Save(path, false);
            }
            catch (ContentIOException ex)
            {
                if (showUI)
                    Content.ShowSaveError(ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Shows an error message about being unable to save certain content.
        /// </summary>
        /// <param name="message">The text body of the dialog box.</param>
        public static void ShowSaveError(string message)
            => FallbackTaskDialog.ShowDialogLog(message, "Unable to save content", "Error saving content", FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));

        /// <summary>
        /// Saves the content to the user-selected path.
        /// <para/>Returns whether the Content was saved.
        /// </summary>
        /// <param name="content">The content data to be saving.</param>
        /// <param name="showUI">If set to <c>true</c>, shows additional user interface elements, such as an error message box or exporter settings.</param>
        public static bool SaveFileAs(Content content, bool showUI = true)
        {
            var path = IOHelper.SaveFileDialog(content, true, showUI);
            if (path == null)
                return false;

            //Save file to given path
            try
            {
                content.Save(path, true);
            }
            catch (ContentIOException ex)
            {
                if (showUI)
                    Content.ShowSaveError(ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Opens the specified file as a new content widget of the corresponding type. This method does not throw an exception, but displays an error message.
        /// </summary>
        /// <param name="file">The file to open as a full path.</param>
        /// <param name="type">The file type for the specified file. Use null to auto-detect using the given mode.</param>
        /// <param name="forceOpen">If set to <c>true</c>, opens the content even if a widget for it already exists.</param>
        /// <param name="mode">The mode to auto detect the file type with (if type is set to null).</param>
        public static void OpenFile(string file, Type type, bool forceOpen = false, ContentDetectMode mode = ContentDetectMode.AutoDetect)
        {
            try
            {
                OpenFileInternal(Program.MainDPM, file, type ?? Content.DetectFileType(file, null, mode), forceOpen);
            }
            catch (Exception ex)
            {
                FallbackTaskDialog.ShowDialogLog($"An error occured while trying to open the file \"{file}\".", "Error opening file", "Error opening file",
                    ex.Message, FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));
            }
        }

        /// <summary>
        /// Opens the specified list of files as a new content widget for each, detecting using the specified mode. This method does not throw an exception, but displays an error message.
        /// </summary>
        /// <param name="files">The files to open, full path to each.</param>
        /// <param name="type">The file type for the specified file. Use null to auto-detect using the given mode.</param>
        /// <param name="forceOpen">If set to <c>true</c>, opens the content even if a widget for it already exists.</param>
        /// <param name="mode">The mode to auto detect the file type with (if type is set to null).</param>
        public static void OpenFile(IList<string> files, Type type, bool forceOpen = false, ContentDetectMode mode = ContentDetectMode.AutoDetect)
        {
            for (var i = 0; i < files.Count; i++)
            {
                var file = files[i];
                try
                {
                    OpenFileInternal(Program.MainDPM, file, type ?? Content.DetectFileType(file, null, mode), forceOpen);
                }
                catch (Exception ex)
                {
                    if (i == files.Count - 1)
                    {
                        //Last file, different message box
                        FallbackTaskDialog.ShowDialogLog($"An error occured while trying to open the file \"{file}\".", "Error opening file", "Error opening file",
                            ex.Message, FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));
                        return;
                    }

                    //Can cancel or skip file
                    var res = FallbackTaskDialog.ShowDialogLog($"An error occured while trying to open the file \"{file}\".", "Error opening file", "Error opening file",
                        ex.Message, FallbackDialogIcon.Error, new FallbackDialogButton("Skip file", DialogResult.OK), new FallbackDialogButton(DialogResult.Cancel));
                    if (res == DialogResult.Cancel)
                        break;
                }
            }
        }

        /// <summary>
        /// Opens the specified file as a new content widget of the corresponding type.
        /// </summary>
        /// <param name="manager">To which manager the file should be added.</param>
        /// <param name="file">The file to open as a full path.</param>
        /// <param name="type">The file type for the specified file. Use null to auto-detect using the given mode.</param>
        /// <param name="forceOpen">If set to <c>true</c>, opens the content even if a widget for it already exists.</param>
        /// <param name="mode">The file type for the specified file.</param>
        /// <exception cref="FileNotFoundException">Tried to open a file which does not exist.</exception>
        /// <exception cref="FileLoadException">Unable to open the selected file. It probably is an unknown file type!</exception>
        private static void OpenFileInternal(DockPanelManager manager, string file, Type type, bool forceOpen = false, ContentDetectMode mode = ContentDetectMode.None)
        {
            //TODO: Content - this overload should accept TypeName instead, and include another overload for a (new) content instance

            if (!File.Exists(file))
                throw new FileNotFoundException("Tried to open a file which does not exist: " + file);

            var content = Content.NewContentOfType(file, true, type);
            if (content == null)
                throw new FileLoadException("Unable to open the selected file. It probably is an unknown file type!");

            manager.AddContent(content, forceOpen);
        }

        #endregion //Static Methods
    }

    /// <summary>
    /// How to detect a content file type.
    /// </summary>
    public enum ContentDetectMode
    {
        /// <summary>
        /// Automatically detect file type, first using path and then (either if unknown path, none specified or ambiguous) using contents.
        /// </summary>
        AutoDetect,
        /// <summary>
        /// Automatically detect file type from its path.
        /// </summary>
        DetectFromPath,
        /// <summary>
        /// Automatically detect file type from its contents.
        /// </summary>
        DetectFromContents,
        /// <summary>
        /// Do not automatically detect the file type.
        /// </summary>
        None
    }
}
