﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SFMTK.Data;

namespace SFMTK.Converters
{
    /// <summary>
    /// Base class for importers, exporters or converters between content and data types.
    /// <para/>Should implement one or more of the following interfaces: <see cref="IImportContent"/>, <see cref="IExportContent"/>, <see cref="IConvertContent"/>.
    /// <para/>If user-defined settings are wanted, use the interfaces <see cref="IImporterSettings"/>, <see cref="IExporterSettings"/>, <see cref="IConverterSettings"/> instead.
    /// </summary>
    public abstract class ContentConverter
    {
        //IMP: ContentConverter - since path format filters are in here, should those be used for matching content types by path?
        //  -> see IOHelper.FilterString, since that also uses filter strings. DEFINITELY populate that list using the converters!!

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentConverter"/> class.
        /// </summary>
        protected ContentConverter()
        {

        }

        /// <summary>
        /// Imports the content of the specified type, if the converter supports importing.
        /// </summary>
        public T Import<T>(string path) where T : class, IData
        {
            //Check for valid interface
            if (this is IImportContent ic)
            {
                //Check for valid type
                if (!ic.DataType.IsAssignableFrom(typeof(T)))
                    throw new InvalidCastException($"Unable to convert imported data from \"{typeof(T).Name}\" to \"{ic.DataType.Name}\".");

                return ic.Import(path) as T;
            }
            throw new InvalidOperationException("This converter can not import content.");
        }

        /// <summary>
        /// Exports the content of the specified type, if the converter supports exporting.
        /// </summary>
        public void Export<T>(T data, string path) where T : class, IData
        {
            //Check for valid interface
            if (this is IExportContent ec)
            {
                //Check for valid type
                if (!ec.DataType.IsAssignableFrom(typeof(T)))
                    throw new InvalidCastException($"Unable to convert data to export from \"{typeof(T).Name}\" to \"{ec.DataType.Name}\".");

                ec.Export(data, path);
                return;
            }
            throw new InvalidOperationException("This converter can not export content.");
        }

        /// <summary>
        /// Converts the content of the specified type to another type, if the converter supports converting.
        /// </summary>
        public TDestination Convert<TSource, TDestination>(TSource data) where TSource : class, IData where TDestination : class, IData
        {
            //Check for valid interface
            if (this is IConvertContent cc)
            {
                //Check for valid types
                if (!cc.SourceDataType.IsAssignableFrom(typeof(TSource)))
                    throw new InvalidCastException($"Unable to convert source data from \"{typeof(TSource).Name}\" to \"{cc.SourceDataType.Name}\".");
                if (!cc.DestinationDataType.IsAssignableFrom(typeof(TDestination)))
                    throw new InvalidCastException($"Unable to convert target data from \"{typeof(TDestination).Name}\" to \"{cc.DestinationDataType.Name}\".");
                
                return cc.Convert(data) as TDestination;
            }
            throw new InvalidOperationException("This converter can not convert content.");
        }

        static ContentConverter() => InitializeConverterList();

        /// <summary>
        /// Populates the <see cref="ConverterList"/> with all currently known <see cref="ContentConverter"/> types.
        /// Done only as the static constructor, if possible add instances manually instead.
        /// </summary>
        public static void InitializeConverterList()
        {
            ConverterList.Clear();

            //Add all known converter types - module's are added later on
            ConverterList.AddRange(Reflection.GetSubclassInstances<ContentConverter>(Modules.ModuleScope.None));
        }

        /// <summary>
        /// Gets the list of known content converters as instances.
        /// </summary>
        public static ContentConverterCollection ConverterList { get; } = new ContentConverterCollection();

        /// <summary>
        /// Gets all known <see cref="ContentConverter"/> that implement <see cref="IImportContent"/>.
        /// </summary>
        /// <param name="strict">If set to <c>true</c>, the <see cref="ContentConverter"/> must *only* allow importing content.</param>
        public static IEnumerable<ContentConverter> GetImporters(bool strict)
            => ConverterList.FilterConverters(ContentConverterType.Importer, strict);

        /// <summary>
        /// Gets all known <see cref="ContentConverter"/> that implement <see cref="IExportContent"/>.
        /// </summary>
        /// <param name="strict">If set to <c>true</c>, the <see cref="ContentConverter"/> must *only* allow exporting content.</param>
        public static IEnumerable<ContentConverter> GetExporters(bool strict)
            => ConverterList.FilterConverters(ContentConverterType.Exporter, strict);

        /// <summary>
        /// Gets all known <see cref="ContentConverter"/> that implement <see cref="IConvertContent"/>.
        /// </summary>
        /// <param name="strict">If set to <c>true</c>, the <see cref="ContentConverter"/> must *only* allow converting content.</param>
        public static IEnumerable<ContentConverter> GetConverters(bool strict)
            => ConverterList.FilterConverters(ContentConverterType.Converter, strict);

        /// <summary>
        /// Gets all known <see cref="ContentConverter" /> that implement <see cref="IImportContent" /> and can import the specified data type.
        /// </summary>
        /// <param name="strict">If set to <c>true</c>, the <see cref="ContentConverter"/> must *only* allow importing content.</param>
        /// <param name="importDataType">Type of the data that can be imported.</param>
        /// <param name="checkInheritance">If set to <c>true</c>, doesn't check if the types are equal, but instead whether the type is a subclass.</param>
        public static IEnumerable<ContentConverter> GetImporters(bool strict, Type importDataType, bool checkInheritance)
            => ConverterList.FilterConverters(ContentConverterType.Importer, strict, importDataType, null, checkInheritance);

        /// <summary>
        /// Gets all known <see cref="ContentConverter" /> that implement <see cref="IExportContent" /> and can export the specified data type.
        /// </summary>
        /// <param name="strict">If set to <c>true</c>, the <see cref="ContentConverter" /> must *only* allow exporting content.</param>
        /// <param name="importDataType">Type of the data that can be exported.</param>
        /// <param name="checkInheritance">If set to <c>true</c>, doesn't check if the types are equal, but instead whether the type is a subclass.</param>
        /// <returns></returns>
        public static IEnumerable<ContentConverter> GetExporters(bool strict, Type exportDataType, bool checkInheritance)
            => ConverterList.FilterConverters(ContentConverterType.Exporter, strict, exportDataType, null, checkInheritance);

        /// <summary>
        /// Gets all known <see cref="ContentConverter" /> that implement <see cref="IConvertContent" /> and can convert from one specific data type to another.
        /// </summary>
        /// <param name="strict">If set to <c>true</c>, the <see cref="ContentConverter" /> must *only* allow converting content.</param>
        /// <param name="sourceDataType">Type of the data that can be converted from. Use <c>null</c> to signify that this type can be any.</param>
        /// <param name="destinationDataType">Type of the data that can be converted to. Use <c>null</c> to signify that this type can be any.</param>
        /// <param name="checkInheritance">If set to <c>true</c>, doesn't check if the types are equal, but instead whether the type is a subclass.</param>
        /// <returns></returns>
        public static IEnumerable<ContentConverter> GetConverters(bool strict, Type sourceDataType, Type destinationDataType, bool checkInheritance)
            => ConverterList.FilterConverters(ContentConverterType.Converter, strict, sourceDataType, destinationDataType, checkInheritance);
    }

    /// <summary>
    /// Collection of known <see cref="ContentConverter"/>, with methods to effectively return them by certain criteria.
    /// </summary>
    public class ContentConverterCollection : ICollection<ContentConverter>
    {
        private readonly List<ContentConverter> _backingList = new List<ContentConverter>();

        /// <summary>
        /// Filters the <see cref="ContentConverterCollection"/> by <see cref="ContentConverterType"/>.
        /// </summary>
        /// <param name="types">The OR-ed list of to-be-returned converters by <see cref="ContentConverterType"/>.</param>
        /// <param name="mustSatisfyAll">If set to <c>true</c>, only returns <see cref="ContentConverter"/> which are part of all types.
        /// Otherwise, returns <see cref="ContentConverter"/> which satisfy one or more type conditions.</param>
        public IEnumerable<ContentConverter> FilterConverters(ContentConverterType types, bool mustSatisfyAll)
            => FilterConverters(_backingList, types, mustSatisfyAll);

        /// <summary>
        /// Filters the specified converters by <see cref="ContentConverterType" />.
        /// </summary>
        /// <param name="converters">The converters to filter.</param>
        /// <param name="types">The OR-ed list of to-be-returned converters by <see cref="ContentConverterType" />.</param>
        /// <param name="mustSatisfyAll">If set to <c>true</c>, only returns <see cref="ContentConverter" /> which are part of all types.
        /// Otherwise, returns <see cref="ContentConverter" /> which satisfy one or more type conditions.</param>
        public static IEnumerable<ContentConverter> FilterConverters(IEnumerable<ContentConverter> converters,
            ContentConverterType types, bool mustSatisfyAll)
        {
            if (types == ContentConverterType.None)
                return Enumerable.Empty<ContentConverter>();
            else if (types == ContentConverterType.All)
                return converters;

            //Actual filtering job
            if (mustSatisfyAll)
                return converters.Where(c => GetConverterTypes(c) == types);
            return converters.Where(c => (GetConverterTypes(c) & types) != 0);
        }

        /// <summary>
        /// Filters the <see cref="ContentConverterCollection"/> by their data types used.
        /// </summary>
        /// <param name="importExportType">Specifies either the data type that is imported to, export from, or is used as source type for conversions.
        /// Use <c>null</c> to accept any value for this parameter.</param>
        /// <param name="convertDestinationType">Specified the data type that conversions use as destination type.
        /// Use <c>null</c> to accept any value for this parameter.</param>
        /// <param name="checkInheritance">If set to <c>true</c>, doesn't check if the types are equal, but instead whether the type is a subclass.</param>
        public IEnumerable<ContentConverter> FilterConverters(Type importExportType, Type convertDestinationType, bool checkInheritance)
            => FilterConverters(_backingList, importExportType, convertDestinationType, checkInheritance);

        /// <summary>
        /// Filters the specified converters by their data types used.
        /// </summary>
        /// <param name="converters">The converters to filter.</param>
        /// <param name="importExportType">Specifies either the data type that is imported to, export from, or is used as source type for conversions.
        /// Use <c>null</c> to accept any value for this parameter.</param>
        /// <param name="convertDestinationType">Specified the data type that conversions use as destination type.
        /// Use <c>null</c> to accept any value for this parameter.</param>
        /// <param name="checkInheritance">If set to <c>true</c>, doesn't check if the types are equal, but instead whether the type is a subclass.</param>
        public static IEnumerable<ContentConverter> FilterConverters(IEnumerable<ContentConverter> converters,
            Type importExportType, Type convertDestinationType, bool checkInheritance)
        {
            foreach (var cc in converters)
            {
                if (cc is IImportContent iic && TypesEqual(iic.DataType, importExportType))
                {
                    yield return cc;
                    continue;
                }
                if (cc is IExportContent iec && TypesEqual(iec.DataType, importExportType))
                {
                    yield return cc;
                    continue;
                }
                if (cc is IConvertContent icc && TypesEqual(icc.SourceDataType, importExportType) && TypesEqual(icc.DestinationDataType, convertDestinationType))
                {
                    yield return cc;
                    continue;
                }
            }



            bool TypesEqual(Type tDesc, Type tBase)
            {
                if (tBase == null)
                    return true;
                if (checkInheritance)
                    return tBase.IsAssignableFrom(tDesc);
                return tBase == tDesc;
            }
        }

        /// <summary>
        /// Filters the <see cref="ContentConverterCollection"/> first by their <see cref="ContentConverterType"/>, and then by their data types used.
        /// </summary>
        /// <param name="types">The OR-ed list of to-be-returned converters by <see cref="ContentConverterType" />.</param>
        /// <param name="mustSatisfyAll">If set to <c>true</c>, only returns <see cref="ContentConverter" /> which are part of all types.
        /// Otherwise, returns <see cref="ContentConverter" /> which satisfy one or more type conditions.</param>
        /// <param name="importExportType">Specifies either the data type that is imported to, export from, or is used as source type for conversions.
        /// Use <c>null</c> to accept any value for this parameter.</param>
        /// <param name="convertDestinationType">Specified the data type that conversions use as destination type.
        /// Use <c>null</c> to accept any value for this parameter.</param>
        /// <param name="checkInheritance">If set to <c>true</c>, doesn't check if the types are equal, but instead whether the type is a subclass.</param>
        public IEnumerable<ContentConverter> FilterConverters(ContentConverterType types, bool mustSatisfyAll,
            Type importExportType, Type convertDestinationType, bool checkInheritance)
                => FilterConverters(FilterConverters(types, mustSatisfyAll), importExportType, convertDestinationType, checkInheritance);

        /// <summary>
        /// Gets the OR-ed <see cref="ContentConverterType"/> of the specified <see cref="ContentConverter"/>.
        /// </summary>
        /// <param name="converter">The <see cref="ContentConverter"/> to get the type of.</param>
        public static ContentConverterType GetConverterTypes(ContentConverter converter)
        {
            var cct = ContentConverterType.None;
            if (converter is IExportContent)
                cct |= ContentConverterType.Exporter;
            if (converter is IImportContent)
                cct |= ContentConverterType.Importer;
            if (converter is IConvertContent)
                cct |= ContentConverterType.Converter;
            return cct;
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count => this._backingList.Count;

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly => false;

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(ContentConverter item) => this._backingList.Add(item);

        /// <summary>
        /// Adds the elements of the specified collection to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="items">The collection whose elements should be added to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// The collection itself cannot be null, but it can contain elements that are null.</param>
        public void AddRange(IEnumerable<ContentConverter> collection) => this._backingList.AddRange(collection);

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear() => this._backingList.Clear();

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(ContentConverter item) => this._backingList.Contains(item);

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.Generic.ICollection`1" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins.</param>
        public void CopyTo(ContentConverter[] array, int arrayIndex) => this._backingList.CopyTo(array, arrayIndex);

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// An enumerator that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<ContentConverter> GetEnumerator() => this._backingList.GetEnumerator();

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(ContentConverter item) => this._backingList.Remove(item);

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator() => this._backingList.GetEnumerator();
    }

    /// <summary>
    /// Combinable enum of all known types of content converters - known ways to convert content from one format to another.
    /// </summary>
    [Flags]
    public enum ContentConverterType
    {
        /// <summary>
        /// No type.
        /// </summary>
        None = 0,
        /// <summary>
        /// Import data from file.
        /// </summary>
        Importer = 1,
        /// <summary>
        /// Export data to file.
        /// </summary>
        Exporter = 2,
        /// <summary>
        /// Convert data to another data format.
        /// </summary>
        Converter = 4,
        /// <summary>
        /// All types combined.
        /// </summary>
        All = 8 - 1
    }
}
