﻿using System;
using SFMTK.Data;

namespace SFMTK.Converters
{
    /// <summary>
    /// A converter which can import content from a file to data.
    /// </summary>
    public interface IImportContent
    {
        /// <summary>
        /// Imports the content from the given file path. This method should not check for valid input files in advance.
        /// </summary>
        /// <remarks>Should throw a ContentIOException for any format errors.</remarks>
        /// <param name="path">Path to the file to be imported.</param>
        /// <returns>The imported data.</returns>
        IData Import(string path);

        /// <summary>
        /// Gets the full name of the file format that this converter can convert from.
        /// </summary>
        string FormatName { get; }

        /// <summary>
        /// Gets the file type filter string that this converter can convert from.
        /// </summary>
        /// <remarks>Formatted like <c>*.ex1;*.ex2;...</c></remarks>
        string FormatFilter { get; }

        /// <summary>
        /// Gets the data type that this converter can convert to. Should implement the <see cref="IData"/> interface.
        /// </summary>
        Type DataType { get; }
    }
}
