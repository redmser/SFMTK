﻿using System;
using SFMTK.Contents;
using SFMTK.Data;

namespace SFMTK.Converters
{
    /// <summary>
    /// A <see cref="ContentConverter"/> which can be used as the default converter for <see cref="Content"/> instances.
    /// </summary>
    public abstract class DefaultContentConverter : ContentConverter, IImportContent, IExportContent
    {
        //TODO: Content/DefaultContentConverter - move format information to a separate class (embedded as a property),
        //  since file formats include a lot of information, of which more and more may become relevant

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultContentConverter"/> class without any format information.
        /// </summary>
        protected DefaultContentConverter() : base()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultContentConverter"/> class. Format information is provided manually.
        /// </summary>
        /// <param name="formatname">The full name of the file format that this converter can convert from and to.</param>
        /// <param name="formatfilter">The file type filter string that this converter can convert from and to. Formatted like <c>*.ex1;*.ex2;...</c></param>
        protected DefaultContentConverter(string formatname, string formatfilter)
        {
            this.CustomFormatName = formatname;
            this.CustomFormatFilter = formatfilter;
        }

        /// <summary>
        /// Gets the full name of the file format that this converter can convert from and to.
        /// </summary>
        public virtual string FormatName => CustomFormatName ?? Content.GetTypeName(this.DataType);

        /// <summary>
        /// Custom <see cref="FormatName"/> value.
        /// </summary>
        protected string CustomFormatName;

        /// <summary>
        /// Gets the file type filter string that this converter can convert from and to.
        /// </summary>
        /// <remarks>
        /// Formatted like <c>*.ex1;*.ex2;...</c>
        /// </remarks>
        //TODO: DefaultContentConverter - since PathFormat would include all extensions, but uses a regex formatting instead, try to find a middle-ground
        //  -> FileFormat collection for all possible extensions/formats which can then be reformatted to EITHER regex OR windows extension string etc
        public virtual string FormatFilter => CustomFormatFilter ?? "*" + this.DefaultContent.MainExtension;

        /// <summary>
        /// Custom <see cref="FormatFilter"/> value.
        /// </summary>
        protected string CustomFormatFilter;

        /// <summary>
        /// Gets the data type that this converter can convert from and to. Should implement the <see cref="IData"/> interface.
        /// </summary>
        public abstract Type DataType { get; }

        /// <summary>
        /// Gets the default content instance assigned to this converter.
        /// </summary>
        protected virtual Content DefaultContent => Content.ContentByType(this.ContentType);

        /// <summary>
        /// Gets the content type of this converter.
        /// </summary>
        protected abstract Type ContentType { get; }

        /// <summary>
        /// Exports the content to the given file path. This method should not check for valid input files or data in advance.
        /// </summary>
        /// <remarks>Should throw a ContentIOException for any format errors.</remarks>
        /// <param name="data">Data to be exported.</param>
        /// <param name="path">Path to the file to be exported.</param>
        /// <returns>The exported data.</returns>
        public abstract void Export(IData data, string path);

        /// <summary>
        /// Imports the content from the given file path. This method should not check for valid input files in advance.
        /// </summary>
        /// <remarks>Should throw a ContentIOException for any format errors.</remarks>
        /// <param name="path">Path to the file to be imported.</param>
        /// <returns>The imported data.</returns>
        public abstract IData Import(string path);
    }
}
