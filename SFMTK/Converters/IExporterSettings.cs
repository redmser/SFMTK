﻿using System.Collections.Generic;
using SFMTK.Controls.InstanceSettings;

namespace SFMTK.Converters
{
    /// <summary>
    /// Defines a content exporter with user-configurable settings.
    /// </summary>
    public interface IExporterSettings : IExportContent
    {
        /// <summary>
        /// Creates the controls which allow specifying user-configurable settings for how content should be exported.
        /// </summary>
        IEnumerable<IInstanceSetting<IExportContent>> CreateExporterSettings();
    }
}
