﻿using System;
using SFMTK.Data;

namespace SFMTK.Converters
{
    /// <summary>
    /// A converter which converts data of one type to data of another type.
    /// </summary>
    public interface IConvertContent
    {
        /// <summary>
        /// Converts the specified content to another type. This method should not check for valid input data in advance.
        /// </summary>
        /// <remarks>Should throw a ContentIOException for any format errors.</remarks>
        /// <param name="source">The data to convert.</param>
        IData Convert(IData source);

        /// <summary>
        /// Gets the data type that this converter can convert from.
        /// </summary>
        Type SourceDataType { get; }

        /// <summary>
        /// Gets the data type that this converter can convert to.
        /// </summary>
        Type DestinationDataType { get; }
    }
}
