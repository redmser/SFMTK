﻿using System;
using SFMTK.Contents;
using SFMTK.Data;

namespace SFMTK.Converters
{
    public class ThemeFileConverter : DefaultContentConverter
    {
        protected override Type ContentType => typeof(ThemeContent);

        public override Type DataType => typeof(UITheme);

        public override void Export(IData data, string path) => throw new NotImplementedException();

        public override IData Import(string path) => UITheme.FromFilePath(path);
    }
}
