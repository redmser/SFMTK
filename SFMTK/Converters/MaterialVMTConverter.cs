﻿using System;
using System.Linq;
using SFMTK.Contents;
using SFMTK.Data;
using SFMTK.Parsing.KeyValues;

namespace SFMTK.Converters
{
    public class MaterialVMTConverter : DefaultContentConverter
    {
        protected override Type ContentType => typeof(MaterialContent);

        public override Type DataType => typeof(Material);

        public override IData Import(string path)
        {
            var kvp = new KeyValuesTokenizer();
            kvp.Read(path);
            var nodes = kvp.Parse();
            var shaderkvp = nodes.FirstOrDefault() as KVPair;
            if (shaderkvp == null)
                throw new ContentIOException("No shader definition is included.");

            var mat = new Material(shaderkvp.Key);
            foreach (var node in shaderkvp.Children)
            {
                //Shader parameters
                //NYI: MaterialVMTConverter - figure out which shader parameter type to add depending on what the parameter accepts AND what the value is
                throw new NotImplementedException();
            }
            return mat;
        }

        public override void Export(IData data, string path) => throw new NotImplementedException();
    }
}
