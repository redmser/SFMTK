﻿using System.Collections.Generic;
using SFMTK.Controls.InstanceSettings;

namespace SFMTK.Converters
{
    /// <summary>
    /// Defines a content converter with user-configurable settings.
    /// </summary>
    public interface IConverterSettings : IConvertContent
    {
        /// <summary>
        /// Creates the controls which allow specifying user-configurable settings for how data should be converted.
        /// </summary>
        IEnumerable<IInstanceSetting<IConvertContent>> CreateConverterSettings();
    }
}
