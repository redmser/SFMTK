﻿using System.Collections.Generic;
using SFMTK.Controls.InstanceSettings;

namespace SFMTK.Converters
{
    /// <summary>
    /// Defines a content importer with user-configurable settings.
    /// </summary>
    public interface IImporterSettings : IImportContent
    {
        /// <summary>
        /// Creates the controls which allow specifying user-configurable settings for how content should be imported.
        /// </summary>
        IEnumerable<IInstanceSetting<IImportContent>> CreateImporterSettings();
    }
}
