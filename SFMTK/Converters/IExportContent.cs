﻿using System;
using SFMTK.Data;

namespace SFMTK.Converters
{
    /// <summary>
    /// A converter which can export content from data to a file.
    /// </summary>
    public interface IExportContent
    {
        /// <summary>
        /// Exports the content to the given file path. This method should not check for valid input files or data in advance.
        /// </summary>
        /// <remarks>Should throw a ContentIOException for any format errors.</remarks>
        /// <param name="data">Data to be exported.</param>
        /// <param name="path">Path to the file to be exported.</param>
        /// <returns>The exported data.</returns>
        void Export(IData data, string path);

        /// <summary>
        /// Gets the data type that this converter can convert from. Should implement the <see cref="IData"/> interface.
        /// </summary>
        Type DataType { get; }

        /// <summary>
        /// Gets the full name of the file format that this converter can convert to.
        /// </summary>
        string FormatName { get; }

        /// <summary>
        /// Gets the file type filter string that this converter can convert to.
        /// </summary>
        /// <remarks>Formatted like <c>*.ex1;*.ex2;...</c></remarks>
        string FormatFilter { get; }
    }
}
