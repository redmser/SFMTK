﻿namespace SFMTK.Widgets
{
    partial class PreviewWidget
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreviewWidget));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.modeToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.viewportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.screenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cropToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.transformToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(800, 425);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(800, 450);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip);
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modeToolStripDropDownButton,
            this.cropToolStripButton,
            this.transformToolStripComboBox});
            this.toolStrip.Location = new System.Drawing.Point(3, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(220, 25);
            this.toolStrip.TabIndex = 0;
            // 
            // modeToolStripDropDownButton
            // 
            this.modeToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.modeToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewportToolStripMenuItem,
            this.screenToolStripMenuItem,
            this.imageFileToolStripMenuItem});
            this.modeToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("modeToolStripDropDownButton.Image")));
            this.modeToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.modeToolStripDropDownButton.Name = "modeToolStripDropDownButton";
            this.modeToolStripDropDownButton.Size = new System.Drawing.Size(29, 22);
            this.modeToolStripDropDownButton.Text = "&Mode";
            // 
            // viewportToolStripMenuItem
            // 
            this.viewportToolStripMenuItem.Name = "viewportToolStripMenuItem";
            this.viewportToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.viewportToolStripMenuItem.Text = "Viewport";
            // 
            // screenToolStripMenuItem
            // 
            this.screenToolStripMenuItem.Name = "screenToolStripMenuItem";
            this.screenToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.screenToolStripMenuItem.Text = "Screen";
            // 
            // imageFileToolStripMenuItem
            // 
            this.imageFileToolStripMenuItem.Name = "imageFileToolStripMenuItem";
            this.imageFileToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.imageFileToolStripMenuItem.Text = "Image file";
            // 
            // cropToolStripButton
            // 
            this.cropToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cropToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("cropToolStripButton.Image")));
            this.cropToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cropToolStripButton.Name = "cropToolStripButton";
            this.cropToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.cropToolStripButton.Text = "&Crop";
            // 
            // transformToolStripComboBox
            // 
            this.transformToolStripComboBox.Name = "transformToolStripComboBox";
            this.transformToolStripComboBox.Size = new System.Drawing.Size(121, 25);
            // 
            // PreviewWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.toolStripContainer1);
            this.MenuText = "&Image Preview";
            this.Name = "PreviewWidget";
            this.TabText = "Image Preview";
            this.Text = "Image Preview";
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripDropDownButton modeToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem viewportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem screenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton cropToolStripButton;
        private System.Windows.Forms.ToolStripComboBox transformToolStripComboBox;
    }
}