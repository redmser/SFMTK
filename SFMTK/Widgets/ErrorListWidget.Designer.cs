﻿namespace SFMTK.Widgets
{
    public partial class ErrorListWidget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorListWidget));
            this.errorObjectListView = new SFMTK.Controls.EnhancedObjectListView();
            this.errorTextColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.errorSeverityColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.errorDescriptionColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.errorFileColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.errorLineColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.errorCodeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.errorListToolStripSearchTextBox = new SFMTK.Controls.ToolStripSearchTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorObjectListView)).BeginInit();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // errorObjectListView
            // 
            this.errorObjectListView.AllColumns.Add(this.errorTextColumn);
            this.errorObjectListView.AllColumns.Add(this.errorSeverityColumn);
            this.errorObjectListView.AllColumns.Add(this.errorDescriptionColumn);
            this.errorObjectListView.AllColumns.Add(this.errorFileColumn);
            this.errorObjectListView.AllColumns.Add(this.errorLineColumn);
            this.errorObjectListView.AllColumns.Add(this.errorCodeColumn);
            this.errorObjectListView.CellEditUseWholeCell = false;
            this.errorObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.errorTextColumn,
            this.errorSeverityColumn,
            this.errorDescriptionColumn,
            this.errorFileColumn,
            this.errorLineColumn});
            this.errorObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.errorObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.errorObjectListView.EmptyListMsg = "Any information about content\'s errors or warnings will be shown in this list.";
            this.errorObjectListView.FullRowSelect = true;
            this.errorObjectListView.HideSelection = false;
            this.errorObjectListView.Location = new System.Drawing.Point(0, 25);
            this.errorObjectListView.Name = "errorObjectListView";
            this.errorObjectListView.ShowGroups = false;
            this.errorObjectListView.Size = new System.Drawing.Size(284, 237);
            this.errorObjectListView.TabIndex = 0;
            this.errorObjectListView.UseCompatibleStateImageBehavior = false;
            this.errorObjectListView.UseFiltering = true;
            this.errorObjectListView.View = System.Windows.Forms.View.Details;
            this.errorObjectListView.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.errorObjectListView_ColumnWidthChanging);
            // 
            // errorTextColumn
            // 
            this.errorTextColumn.AspectName = "Text";
            this.errorTextColumn.Text = "Description";
            this.errorTextColumn.Width = 160;
            this.errorTextColumn.WordWrap = true;
            // 
            // errorSeverityColumn
            // 
            this.errorSeverityColumn.MaximumWidth = 16;
            this.errorSeverityColumn.Text = "";
            this.errorSeverityColumn.ToolTipText = "Severity";
            this.errorSeverityColumn.Width = 16;
            // 
            // errorDescriptionColumn
            // 
            this.errorDescriptionColumn.AspectName = "Description";
            this.errorDescriptionColumn.Text = "Details";
            this.errorDescriptionColumn.Width = 100;
            this.errorDescriptionColumn.WordWrap = true;
            // 
            // errorFileColumn
            // 
            this.errorFileColumn.AspectName = "";
            this.errorFileColumn.DisplayIndex = 4;
            this.errorFileColumn.Text = "File";
            // 
            // errorLineColumn
            // 
            this.errorLineColumn.AspectName = "LineNumber";
            this.errorLineColumn.DisplayIndex = 3;
            this.errorLineColumn.Text = "Line";
            this.errorLineColumn.Width = 30;
            // 
            // errorCodeColumn
            // 
            this.errorCodeColumn.AspectName = "ID";
            this.errorCodeColumn.IsVisible = false;
            this.errorCodeColumn.Text = "Code";
            // 
            // toolStrip
            // 
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.errorListToolStripSearchTextBox});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(284, 25);
            this.toolStrip.TabIndex = 1;
            // 
            // errorListToolStripSearchTextBox
            // 
            this.errorListToolStripSearchTextBox.ActiveTheme = null;
            this.errorListToolStripSearchTextBox.ClearImage = null;
            this.errorListToolStripSearchTextBox.Name = "errorListToolStripSearchTextBox";
            this.errorListToolStripSearchTextBox.SearchImage = null;
            this.errorListToolStripSearchTextBox.Size = new System.Drawing.Size(100, 25);
            this.errorListToolStripSearchTextBox.TextChanged += new System.EventHandler(this.errorListToolStripSearchTextBox_TextChanged);
            // 
            // ErrorListWidget
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.errorObjectListView);
            this.Controls.Add(this.toolStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MenuText = "&Error List";
            this.Name = "ErrorListWidget";
            this.TabText = "Error List";
            this.Text = "Error List";
            ((System.ComponentModel.ISupportInitialize)(this.errorObjectListView)).EndInit();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SFMTK.Controls.EnhancedObjectListView errorObjectListView;
        private BrightIdeasSoftware.OLVColumn errorTextColumn;
        private BrightIdeasSoftware.OLVColumn errorDescriptionColumn;
        private BrightIdeasSoftware.OLVColumn errorSeverityColumn;
        private System.Windows.Forms.ToolStrip toolStrip;
        private Controls.ToolStripSearchTextBox errorListToolStripSearchTextBox;
        private BrightIdeasSoftware.OLVColumn errorLineColumn;
        private BrightIdeasSoftware.OLVColumn errorFileColumn;
        private BrightIdeasSoftware.OLVColumn errorCodeColumn;
    }
}
