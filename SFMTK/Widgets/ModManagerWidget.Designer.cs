﻿namespace SFMTK.Widgets
{
    partial class ModManagerWidget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModManagerWidget));
            this.modObjectListView = new BrightIdeasSoftware.DataListView();
            this.modNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.modOrderColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.modDescriptionColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.modCategoryColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.errorLinkLabel = new System.Windows.Forms.LinkLabel();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.addModToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.removeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.importToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.searchToolStripTextBox = new SFMTK.Controls.ToolStripSearchTextBox();
            this.commandManager = new SFMTK.Commands.CommandManager();
            ((System.ComponentModel.ISupportInitialize)(this.modObjectListView)).BeginInit();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // modObjectListView
            // 
            this.modObjectListView.AllColumns.Add(this.modNameColumn);
            this.modObjectListView.AllColumns.Add(this.modOrderColumn);
            this.modObjectListView.AllColumns.Add(this.modDescriptionColumn);
            this.modObjectListView.AllColumns.Add(this.modCategoryColumn);
            this.modObjectListView.AllowDrop = true;
            this.modObjectListView.AutoGenerateColumns = false;
            this.modObjectListView.BackColor = System.Drawing.SystemColors.ControlDark;
            this.modObjectListView.CellEditUseWholeCell = false;
            this.modObjectListView.CheckBoxes = true;
            this.modObjectListView.CheckedAspectName = "Enabled";
            this.modObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.modNameColumn,
            this.modDescriptionColumn,
            this.modCategoryColumn});
            this.modObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.modObjectListView.DataSource = null;
            this.modObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modObjectListView.FullRowSelect = true;
            this.modObjectListView.HideSelection = false;
            this.modObjectListView.IsSimpleDragSource = true;
            this.modObjectListView.Location = new System.Drawing.Point(0, 0);
            this.modObjectListView.Name = "modObjectListView";
            this.modObjectListView.ShowGroups = false;
            this.modObjectListView.Size = new System.Drawing.Size(284, 262);
            this.modObjectListView.TabIndex = 0;
            this.modObjectListView.UseCompatibleStateImageBehavior = false;
            this.modObjectListView.UseFiltering = true;
            this.modObjectListView.View = System.Windows.Forms.View.Details;
            this.modObjectListView.ModelDropped += new System.EventHandler<BrightIdeasSoftware.ModelDropEventArgs>(this.modObjectListView_ModelDropped);
            this.modObjectListView.SelectionChanged += new System.EventHandler(this.modObjectListView_SelectionChanged);
            // 
            // modNameColumn
            // 
            this.modNameColumn.AspectName = "Name";
            this.modNameColumn.Text = "Name";
            this.modNameColumn.UseFiltering = false;
            this.modNameColumn.Width = 130;
            // 
            // modOrderColumn
            // 
            this.modOrderColumn.DisplayIndex = 1;
            this.modOrderColumn.IsVisible = false;
            this.modOrderColumn.Text = "Order";
            this.modOrderColumn.UseFiltering = false;
            // 
            // modDescriptionColumn
            // 
            this.modDescriptionColumn.AspectName = "Description";
            this.modDescriptionColumn.Text = "Description";
            this.modDescriptionColumn.UseFiltering = false;
            this.modDescriptionColumn.Width = 250;
            // 
            // modCategoryColumn
            // 
            this.modCategoryColumn.AspectName = "Category";
            this.modCategoryColumn.Text = "Category";
            this.modCategoryColumn.Width = 100;
            // 
            // errorLinkLabel
            // 
            this.errorLinkLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.errorLinkLabel.LinkArea = new System.Windows.Forms.LinkArea(39, 24);
            this.errorLinkLabel.Location = new System.Drawing.Point(0, 0);
            this.errorLinkLabel.Name = "errorLinkLabel";
            this.errorLinkLabel.Size = new System.Drawing.Size(284, 262);
            this.errorLinkLabel.TabIndex = 2;
            this.errorLinkLabel.TabStop = true;
            this.errorLinkLabel.Text = "Unable to load mod list!\r\nBe sure your path to SFM\'s executable is configured acc" +
    "ordingly!";
            this.errorLinkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.errorLinkLabel.UseCompatibleTextRendering = true;
            this.errorLinkLabel.Visible = false;
            this.errorLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.errorLinkLabel_LinkClicked);
            // 
            // toolStrip
            // 
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addModToolStripButton,
            this.removeToolStripButton,
            this.toolStripSeparator1,
            this.importToolStripButton,
            this.toolStripSeparator2,
            this.searchToolStripTextBox});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(284, 25);
            this.toolStrip.TabIndex = 3;
            this.toolStrip.Visible = false;
            // 
            // addModToolStripButton
            // 
            this.commandManager.SetCommand(this.addModToolStripButton, "NewModCommand");
            this.addModToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addModToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("addModToolStripButton.Image")));
            this.addModToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addModToolStripButton.Name = "addModToolStripButton";
            this.addModToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.addModToolStripButton.Text = "&New Mod...";
            this.addModToolStripButton.ToolTipText = "New Mod...";
            // 
            // removeToolStripButton
            // 
            this.commandManager.SetCommand(this.removeToolStripButton, "DeleteCommand");
            this.removeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.removeToolStripButton.Enabled = false;
            this.removeToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("removeToolStripButton.Image")));
            this.removeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.removeToolStripButton.Name = "removeToolStripButton";
            this.removeToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.removeToolStripButton.Text = "&Delete";
            this.removeToolStripButton.ToolTipText = "Delete (Delete)";
            this.removeToolStripButton.Click += new System.EventHandler(this.removeToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.commandManager.SetCommand(this.toolStripSeparator1, "");
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // importToolStripButton
            // 
            this.commandManager.SetCommand(this.importToolStripButton, "");
            this.importToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.importToolStripButton.Enabled = false;
            this.importToolStripButton.Image = global::SFMTK.Properties.Resources.database_go;
            this.importToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.importToolStripButton.Name = "importToolStripButton";
            this.importToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.importToolStripButton.Text = "Import Content";
            this.importToolStripButton.ToolTipText = "Import content to mod";
            this.importToolStripButton.Click += new System.EventHandler(this.importToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.commandManager.SetCommand(this.toolStripSeparator2, "");
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // searchToolStripTextBox
            // 
            this.searchToolStripTextBox.ActiveTheme = null;
            this.searchToolStripTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchToolStripTextBox.ClearImage = ((System.Drawing.Image)(resources.GetObject("searchToolStripTextBox.ClearImage")));
            this.commandManager.SetCommand(this.searchToolStripTextBox, "");
            this.searchToolStripTextBox.Name = "searchToolStripTextBox";
            this.searchToolStripTextBox.SearchImage = ((System.Drawing.Image)(resources.GetObject("searchToolStripTextBox.SearchImage")));
            this.searchToolStripTextBox.Size = new System.Drawing.Size(100, 25);
            this.searchToolStripTextBox.TextChanged += new System.EventHandler(this.searchToolStripTextBox_TextChanged);
            // 
            // commandManager
            // 
            this.commandManager.CommandHistory = null;
            // 
            // ModManagerWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.errorLinkLabel);
            this.Controls.Add(this.modObjectListView);
            this.Controls.Add(this.toolStrip);
            this.DefaultShortcut = System.Windows.Forms.Keys.F2;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MenuText = "&Mod Manager";
            this.Name = "ModManagerWidget";
            this.TabText = "Mod Manager";
            this.Text = "Mod Manager";
            ((System.ComponentModel.ISupportInitialize)(this.modObjectListView)).EndInit();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BrightIdeasSoftware.DataListView modObjectListView;
        private BrightIdeasSoftware.OLVColumn modNameColumn;
        private System.Windows.Forms.LinkLabel errorLinkLabel;
        private BrightIdeasSoftware.OLVColumn modCategoryColumn;
        private BrightIdeasSoftware.OLVColumn modDescriptionColumn;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton addModToolStripButton;
        private System.Windows.Forms.ToolStripButton importToolStripButton;
        private System.Windows.Forms.ToolStripButton removeToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private Controls.ToolStripSearchTextBox searchToolStripTextBox;
        private BrightIdeasSoftware.OLVColumn modOrderColumn;
        private Commands.CommandManager commandManager;
    }
}
