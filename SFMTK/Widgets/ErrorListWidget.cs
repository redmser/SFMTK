﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;
using SFMTK.Errors;

namespace SFMTK.Widgets
{
    /// <summary>
    /// A widget for displaying errors of all opened content.
    /// </summary>
    /// <seealso cref="WeifenLuo.WinFormsUI.Docking.DockContent" />
    public partial class ErrorListWidget : Widget
    {
        //FIXME: ErrorListWidget - either find a OLV-like control that allows separate RowHeight, or have an option to disable the word-wrap (, or both?)

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorListWidget"/> class.
        /// </summary>
        public ErrorListWidget()
        {
            //Compiler-generated
            InitializeComponent();

            //OLV setup
            //FIXME: ErrorListWidget - should the emptylistoverlay exist? it makes you think there's something in the list when there isn't!
            this.errorSeverityColumn.DisplayIndex = 0;
            this.errorSeverityColumn.ClusteringStrategy = new ErrorListSeverityClusteringStrategy();
            this.errorSeverityColumn.ImageGetter = ListGetSeverityImage;
            this.errorFileColumn.AspectGetter = ListGetFileAspect;
            //TODO: ErrorListWidget - change line number clustering to "beginning/middle/end of file" or "unknown"
            //  -> needs total length of file :/
            this.errorLineColumn.AspectToStringConverter = ListConvertLineAspect;
            this.errorTextColumn.ClusteringStrategy = new ErrorListCodeClusteringStrategy();
            this.errorDescriptionColumn.ClusteringStrategy = new ErrorListCodeClusteringStrategy();

            this.errorObjectListView.EmptyListMsgOverlay = new Controls.EmptyListOverlay(this.errorObjectListView);
            this.errorObjectListView.CellToolTipGetter = ListGetToolTip;
            this.UpdateListFilter();
        }

        private static string ListConvertLineAspect(object value)
        {
            var line = (int)value;
            if (line < 1)
                return null;
            return line.ToString();
        }

        private static object ListGetFileAspect(object rowObject)
        {
            if (!(rowObject is ContentError err))
                return null;

            return err.Content.GetFormattedName(Properties.Settings.Default.TabTitleFormat, false);
        }

        private void UpdateListFilter()
        {
            this.errorObjectListView.AdditionalFilter = null;
            this.errorObjectListView.AdditionalFilter = new ErrorListFilter(this.errorListToolStripSearchTextBox.Text);
        }

        private static object ListGetSeverityImage(object rowObject)
        {
            if (!(rowObject is ContentError err))
                return null;

            return err.Image;
        }

        private string ListGetToolTip(OLVColumn column, object modelObject)
        {
            if (!(modelObject is ContentError err))
                return null;

            if (column == this.errorSeverityColumn)
            {
                //Severity name
                return err.Severity.ToString();
            }
            else if (column == this.errorTextColumn)
            {
                //Text
                return err.Text;
            }
            else if (column == this.errorDescriptionColumn)
            {
                //Description
                return err.Description;
            }
            return null;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //ErrorManagement
            Program.MainEM.ErrorsChanged += (s, a) => this.ReloadErrorList();
            this.ReloadErrorList();
        }

        /// <summary>
        /// Forces a reload of the error list contents. This is done automatically whenever the error list changes.
        /// </summary>
        public void ReloadErrorList()
        {
            this.errorObjectListView.SetObjects(Program.MainEM.GetErrors());
            UpdateRowHeight();
        }

        private void errorListToolStripSearchTextBox_TextChanged(object sender, EventArgs e) => this.UpdateListFilter();

        private int CalculateRowHeight()
        {
            if (this.errorObjectListView.Objects == null)
                return -1;

            //Inefficiently get highest row
            var max = -1;
            foreach (var err in this.errorObjectListView.Objects.Cast<ContentError>())
            {
                var h1 = CalculateRowHeight(err.Text, this.errorTextColumn.Width);
                var h2 = CalculateRowHeight(err.Description, this.errorDescriptionColumn.Width);
                var highest = h1 > h2 ? h1 : h2;
                if (highest > max)
                    max = highest;
            }
            if (max > 256)
                return 256;
            return max;
        }

        private int CalculateRowHeight(string text, int width)
        {
            var size = _tempgr.MeasureString(text, this.errorObjectListView.Font, width);
            return (int)size.Height;
        }

        private static Bitmap _tempbmp;
        private static Graphics _tempgr;

        static ErrorListWidget()
        {
            _tempbmp = new Bitmap(1, 1);
            _tempgr = Graphics.FromImage(_tempbmp);
        }

        private void UpdateRowHeight() => this.errorObjectListView.RowHeight = CalculateRowHeight();

        private void errorObjectListView_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e) => UpdateRowHeight();
    }
}