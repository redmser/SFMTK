﻿namespace SFMTK.Widgets
{
    partial class ContentInfoWidget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.pathTextBox = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.viewPathButton = new System.Windows.Forms.Button();
            this.browseButton = new System.Windows.Forms.Button();
            this.modComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.contentInfoPanel = new System.Windows.Forms.Panel();
            this.dependencyGraphViewer = new SFMTK.Controls.DependencyGraphViewer();
            this.togglesToolStrip = new System.Windows.Forms.ToolStrip();
            this.infoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.contentWidgetPanel = new System.Windows.Forms.Panel();
            this.contentInfoPanel.SuspendLayout();
            this.togglesToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "&Content path:";
            // 
            // pathTextBox
            // 
            this.pathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pathTextBox.Location = new System.Drawing.Point(76, 6);
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.Size = new System.Drawing.Size(127, 20);
            this.pathTextBox.TabIndex = 1;
            this.toolTip1.SetToolTip(this.pathTextBox, "Local path to the content\'s file");
            // 
            // viewPathButton
            // 
            this.viewPathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.viewPathButton.Image = global::SFMTK.Properties.Resources.arrow_go;
            this.viewPathButton.Location = new System.Drawing.Point(227, 4);
            this.viewPathButton.Name = "viewPathButton";
            this.viewPathButton.Size = new System.Drawing.Size(24, 23);
            this.viewPathButton.TabIndex = 3;
            this.toolTip1.SetToolTip(this.viewPathButton, "Open in Explorer");
            this.viewPathButton.UseVisualStyleBackColor = true;
            this.viewPathButton.Click += new System.EventHandler(this.viewPathButton_Click);
            // 
            // browseButton
            // 
            this.browseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseButton.Image = global::SFMTK.Properties.Resources.folder;
            this.browseButton.Location = new System.Drawing.Point(203, 4);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(24, 23);
            this.browseButton.TabIndex = 2;
            this.toolTip1.SetToolTip(this.browseButton, "Browse for content path");
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // modComboBox
            // 
            this.modComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.modComboBox.FormattingEnabled = true;
            this.modComboBox.Location = new System.Drawing.Point(36, 32);
            this.modComboBox.Name = "modComboBox";
            this.modComboBox.Size = new System.Drawing.Size(211, 21);
            this.modComboBox.TabIndex = 5;
            this.toolTip1.SetToolTip(this.modComboBox, "Which mod this content is a part of.");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "&Mod:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "&Dependencies:";
            // 
            // contentInfoPanel
            // 
            this.contentInfoPanel.Controls.Add(this.dependencyGraphViewer);
            this.contentInfoPanel.Controls.Add(this.label1);
            this.contentInfoPanel.Controls.Add(this.label3);
            this.contentInfoPanel.Controls.Add(this.pathTextBox);
            this.contentInfoPanel.Controls.Add(this.modComboBox);
            this.contentInfoPanel.Controls.Add(this.browseButton);
            this.contentInfoPanel.Controls.Add(this.label2);
            this.contentInfoPanel.Controls.Add(this.viewPathButton);
            this.contentInfoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentInfoPanel.Enabled = false;
            this.contentInfoPanel.Location = new System.Drawing.Point(0, 25);
            this.contentInfoPanel.Name = "contentInfoPanel";
            this.contentInfoPanel.Size = new System.Drawing.Size(251, 225);
            this.contentInfoPanel.TabIndex = 7;
            // 
            // dependencyGraphViewer
            // 
            this.dependencyGraphViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dependencyGraphViewer.Content = null;
            this.dependencyGraphViewer.Location = new System.Drawing.Point(4, 80);
            this.dependencyGraphViewer.Name = "dependencyGraphViewer";
            this.dependencyGraphViewer.Size = new System.Drawing.Size(244, 140);
            this.dependencyGraphViewer.TabIndex = 7;
            // 
            // togglesToolStrip
            // 
            this.togglesToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.togglesToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.infoToolStripButton});
            this.togglesToolStrip.Location = new System.Drawing.Point(0, 0);
            this.togglesToolStrip.Name = "togglesToolStrip";
            this.togglesToolStrip.Size = new System.Drawing.Size(251, 25);
            this.togglesToolStrip.TabIndex = 0;
            this.togglesToolStrip.Text = "toolStrip1";
            // 
            // infoToolStripButton
            // 
            this.infoToolStripButton.CheckOnClick = true;
            this.infoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.infoToolStripButton.Image = global::SFMTK.Properties.Resources.chart_organisation;
            this.infoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.infoToolStripButton.Margin = new System.Windows.Forms.Padding(1, 1, 0, 2);
            this.infoToolStripButton.Name = "infoToolStripButton";
            this.infoToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.infoToolStripButton.Text = "Toggle content info";
            // 
            // contentWidgetPanel
            // 
            this.contentWidgetPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentWidgetPanel.Location = new System.Drawing.Point(0, 25);
            this.contentWidgetPanel.Name = "contentWidgetPanel";
            this.contentWidgetPanel.Size = new System.Drawing.Size(251, 225);
            this.contentWidgetPanel.TabIndex = 8;
            // 
            // ContentInfoWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 250);
            this.Controls.Add(this.contentInfoPanel);
            this.Controls.Add(this.contentWidgetPanel);
            this.Controls.Add(this.togglesToolStrip);
            this.DockAreas = ((WeifenLuo.WinFormsUI.Docking.DockAreas)((WeifenLuo.WinFormsUI.Docking.DockAreas.Float | WeifenLuo.WinFormsUI.Docking.DockAreas.Document)));
            this.GenerateShowCommand = false;
            this.Name = "ContentInfoWidget";
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.Document;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ContentInfoWidget_FormClosing);
            this.contentInfoPanel.ResumeLayout(false);
            this.contentInfoPanel.PerformLayout();
            this.togglesToolStrip.ResumeLayout(false);
            this.togglesToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox pathTextBox;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button viewPathButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox modComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel contentInfoPanel;
        private System.Windows.Forms.ToolStrip togglesToolStrip;
        private System.Windows.Forms.ToolStripButton infoToolStripButton;
        private System.Windows.Forms.Panel contentWidgetPanel;
        private Controls.DependencyGraphViewer dependencyGraphViewer;
    }
}
