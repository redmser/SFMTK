﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using SFMTK.Forms;

namespace SFMTK.Widgets
{
    /// <summary>
    /// A widget for embedding a HLMV.exe (model viewer) instance inside of SFMTK.
    /// </summary>
    /// <seealso cref="WeifenLuo.WinFormsUI.Docking.DockContent" />
    public partial class ModelViewerWidget : Widget
    {
        //TODO: ModelViewer - attempt to resize the controls inside, to make the tab stuff at the bottom smaller (or simply hide/show it with a toggle
        //  overlayed onto the widget, ofc as a setting!)
        //TODO: ModelViewer - make unpin mechanic clearer (such as, when closing SFMTK, warn that HLMV is closing too)
        //TODO: ModelViewer - include a toggle button to enable/disable the modelviewer inside the widget (save this value in the persist)
        //FIXME: ModelViewer - if hlmv is unfocused, try to refresh the display again to show the model
        //FIXME: ModelViewer - apply theme to controls of the HLMV (menu bar, bottom tab)

        private Process _process;
        private readonly NativeMethods.WindowTrapper _trapper = new NativeMethods.WindowTrapper();

        /// <summary>
        /// Name of the model viewer's executable.
        /// </summary>
        public const string ModelViewerExecutable = "hlmv.exe";

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelViewerWidget"/> class without any files opened.
        /// </summary>
        public ModelViewerWidget() : this(null)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelViewerWidget"/> class with the specified file opened, verifying it as a valid .mdl file beforehand.
        /// </summary>
        /// <param name="file">Full path to the file.</param>
        public ModelViewerWidget(string file)
        {
            //Compiler-generated
            InitializeComponent();

            //Setup process
            string mdlinfo;

            //Change extension to MDL
            file = Path.ChangeExtension(file, "mdl");
            if (!string.IsNullOrWhiteSpace(file) && File.Exists(file))
            {
                _launchmdl = $" \"{file}\"";
                mdlinfo = $"\n\n{SFM.GetLocalPath(file)}";
            }
            else
            {
                mdlinfo = string.Empty;
            }

            this.loadingLabel.LinkArea = new System.Windows.Forms.LinkArea(0, 0);
            this.loadingLabel.Text = $"Loading Model Viewer...{mdlinfo}";
        }

        private string _launchmdl = string.Empty;

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            SFM.BasePathChanged += this.OnBasePathUpdated;
        }

        /// <summary>
        /// Called when this widget is about to be added to a DockPanel, but has not been loaded from a layout XML (see <see cref="InitializePersist(string)" />).
        /// </summary>
        public override void InitializeInstance()
        {
            //Launch a new HLMV if newly created widget
            this.StartProcess(_launchmdl);
        }

        /// <summary>
        /// Called after creating a widget from persist, but before being added to the DockPanel. Used to initialize further information for the widget.
        /// <para />Returns whether the widget should be added to the DockPanel (this should be <c>false</c> for any critical parsing errors).
        /// </summary>
        /// <param name="persistString">The persist string that was</param>
        /// <remarks>This widget should also override <see cref="GetPersist" />, to change how to store the string in the first place.</remarks>
        public override bool InitializePersist(string persistString)
        {
            //TODO: ModelViewerWidget - see if we can load the last opened file (if this is wanted)

            if (string.IsNullOrEmpty(persistString))
            {
                //Same routine as Initializing it regularly
                this.InitializeInstance();
                return true;
            }
            else if (persistString.StartsWith("PID:"))
            {
                //Re-trap process with specified process id
                //BUG: ModelViewerWidget - when trying to re-trap the hlmv, it sometimes does not show up (either causing infinite "Loading HLMV" text, or an empty widget)
                this._process = Process.GetProcessById(int.Parse(persistString.Substring(4)));
                var inittask = this.InitializeProcess();
                return true;
            }

            //Invalid persist
            throw new ArgumentException("Could not handle persist string of model viewer: " + persistString, nameof(persistString));
        }

        /// <summary>
        /// Returns additional persist information to be stored in the widget when saving.
        /// </summary>
        /// <remarks>Override <see cref="InitializePersist" /> to change how to handle this data.</remarks>
        protected override string GetPersist()
        {
            if (Program.MainDPM.IsReloadingContent)
            {
                //Store process ID and show the same hlmv instance again
                return $"PID:{this._process.Id}";
            }
            else
            {
                //TODO: ModelViewerWidget - Store opened file's path (from title of the window?) (so we can reload it when restarting sfmtk)
            }
            return null;
        }

        private void OnBasePathUpdated(object sender, EventArgs e)
        {
            //UNTESTED: ModelViewerWidget - restart hlmv code

            //Close running process, if any
            this.CloseProcess();

            //Reopen process
            this.StartProcess();
        }

        private async void StartProcess(string mdl = "")
        {
            try
            { 
                var path = SFM.BinPath + ModelViewerExecutable;
                if (!File.Exists(path))
                {
                    //Notify user of missing hlmv.exe
                    this.loadingLabel.Text = "Unable to load model viewer!\nMake sure your path to SFM's executable is configured accordingly!";
                    this.loadingLabel.LinkArea = new System.Windows.Forms.LinkArea(44, 24);
                    return;
                }

                this._process = Process.Start(path, $"-game \"{SFM.UsermodPath}\"{mdl}");
                if (this._process == null)
                {
                    //Invalid process launched
                    ErrorReport.Display($"Could not launch Model Viewer: the file was not an executable ({path})");
                    this.Close();
                    return;
                }
                else
                {
                    await this.InitializeProcess();
                }
            }
            catch (Exception ex)
            {
                ErrorReport.Display("Could not launch Model Viewer!", ex);
                this.Close();
                return;
            }
        }

        private async Task InitializeProcess()
        {
            //Wait with trapping
            this._process.EnableRaisingEvents = true;
            this._process.Exited += this.Process_Exited;

            try
            {
                this._trappingTask = Task.Factory.StartNew(this.DelayTrapping);
                await this._trappingTask;
            }
            catch (OperationCanceledException)
            {
                //Do not continue
                return;
            }

            //Trap it
            this.TrapModelViewer();

            //Update UI
            this.loadingLabel.Hide();
            this.unpinButton.Show();
        }

        private Task _trappingTask;

        /// <summary>
        /// Gets the main window handle of the currently running HLMV process, or a null pointer if it is not, or no window is assigned to the process.
        /// </summary>
        private IntPtr ProcessHandle
        {
            get
            {
                try
                {
                    if (this._process == null)
                        return IntPtr.Zero;
                    return this._process.MainWindowHandle;
                }
                catch (InvalidOperationException)
                {
                    return IntPtr.Zero;
                }
            }
        }

        /// <summary>
        /// Run when the process has exited.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void Process_Exited(object sender, EventArgs eventArgs)
        {
            //Close the window as well
            this.InvokeIfRequired(this.Close);
        }

        /// <summary>
        /// Recreation of the tab window, for example when un/redocking.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModelViewer_HandleCreated(object sender, EventArgs e)
        {
            this.TrapModelViewer();
        }

        /// <summary>
        /// Recreation of the tab window, for example when un/redocking.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModelViewer_HandleDestroyed(object sender, EventArgs e)
        {
            this.ReleaseModelViewer();
        }

        /// <summary>
        /// Asynchronous opening/closing action of process.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DelayTrapping()
        {
            if (this.IsDisposed)
            {
                //Close process
                this.CloseProcess();
            }
            else
            {
                //Trap - Have to wait first
                this._process.WaitForInputIdle();

                //Wait for the handle
                SpinWait.SpinUntil(() => this.ProcessHandle != IntPtr.Zero);

                //Events
                this.HandleCreated += this.ModelViewer_HandleCreated;
                this.HandleDestroyed += this.ModelViewer_HandleDestroyed;
            }
        }

        /// <summary>
        /// Traps the HLMV.exe in the containerPanel, if a main window exists.
        /// </summary>
        private void TrapModelViewer()
        {
            if (this._trapper.Trapped || this._process == null || this.ProcessHandle == IntPtr.Zero)
                return;

            this._trapper.ParentControl = this.containerPanel;
            this._trapper.ChildControl = this.ProcessHandle;
            this._trapper.TrapChild();
            //TODO: ModelViewer - hide taskbar icon of hlmv while trapped
        }

        /// <summary>
        /// Releases the HLMV.exe from the containerPanel and places it as a window on the desktop again.
        /// </summary>
        private void ReleaseModelViewer()
        {
            if (!this._trapper.Trapped || this._process == null || this.ProcessHandle == IntPtr.Zero)
                return;

            this._trapper.ParentControl = null;
            this._trapper.ChildControl = this.ProcessHandle;
            this._trapper.TrapChild();
        }

        /// <summary>
        /// Run once the tab is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (Program.MainDPM.IsReloadingContent)
            {
                //Only reloading close - be sure to release and later re-open same HLMV
                this.ReleaseModelViewer();
                return;
            }

            if (this._trapper.Trapped || !this._trappingTask.IsCompleted)
            {
                //Close HLMV too
                if (this._process == null)
                    return;

                //TODO: ModelViewer - if it still shows "loading hlmv", then always quit process when closing the widget, no matter the settings key
                //BUG: ModelViewer - crashed when closing while loading, with unpin on close turned on
                if (this.ProcessHandle != IntPtr.Zero)
                {
                    if (Properties.Settings.Default.AutoUnpinHLMV)
                    {
                        //Untrap me
                        this.ReleaseModelViewer();
                    }
                    else
                    {
                        //Kill me now
                        this.CloseProcess();
                    }
                }
            }
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            //Events
            SFM.BasePathChanged -= this.OnBasePathUpdated;

            base.OnFormClosed(e);
        }

        /// <summary>
        /// Resizes the HLMV.exe if the parent changes size.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnClientSizeChanged(EventArgs e)
        {
            base.OnClientSizeChanged(e);

            if (!this._trapper.Trapped || this._process == null || this.ProcessHandle == IntPtr.Zero)
                return;

            this._trapper.ResizeChild();
        }

        /// <summary>
        /// Unpins the model viewer and closes the tab.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void unpinButton_Click(object sender, EventArgs e)
        {
            this.ReleaseModelViewer();
            this.Close();

            //TODO: ModelViewer - after unpinning, give focus to HLMV
        }

        /// <summary>
        /// Closes the HLMV.exe process.
        /// </summary>
        private void CloseProcess()
        {
            this._process.Exited -= this.Process_Exited;
            if (!this._process.HasExited)
                this._process.Kill();
        }

        private void loadingLabel_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            //Open settings
            Program.MainForm.OpenSettings("Environment/General", true);
        }
    }
}
