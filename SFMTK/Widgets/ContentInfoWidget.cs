﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using SFMTK.Commands;
using SFMTK.Contents;
using SFMTK.Controls.ContentWidgets;
using SFMTK.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace SFMTK.Widgets
{
    /// <summary>
    /// General widget for displaying Content info as well as the Content-specific widget.
    /// </summary>
    /// <seealso cref="WeifenLuo.WinFormsUI.Docking.DockContent" />
    public partial class ContentInfoWidget : Widget, IEditableData
    {
        //TODO: ContentInfoWidget - on browse, update mod selection as well - similarly, if entering a path in the textbox with a mod, update the selected mod too
        //  -> only sanitize the path textbox when saving the content -> how should save (as) be handled compared to the path textbox? is an export option needed instead?
        //TODO: ContentInfoWidget - notify user if he's either saving a content to a different file path than before (without using save as), AND warn when saving to not existing mod!
        //  -> both should be remember dialogs, since they're just warnings
        //  -> when pressing CTRL+S on new content, allow user to pick target folder only if the specified filepath in the textbox has no directory in it
        //  -> the textbox should ACCEPT any type, but otherwise show RELATIVE to the "regular" dir inside (meaning a vmt path will start showing everything AFTER the "materials" folder)

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentInfoWidget"/> class. This constructor is only to be used by the designed.
        /// </summary>
        public ContentInfoWidget()
        {
            //Compiler-generated
            InitializeComponent();

            //Databindings
            this.contentInfoPanel.DataBindings.Add("Visible", this.infoToolStripButton, "Checked", false, DataSourceUpdateMode.OnPropertyChanged);
            if (SFM.ValidPaths)
            {
                this.modComboBox.DataSource = SFM.GameInfo.Mods;
                this.modComboBox.DisplayMember = "Name";
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentInfoWidget"/> class containing the specified ContentWidget.
        /// </summary>
        /// <param name="contained">The contained ContentWidget.</param>
        /// <exception cref="ArgumentNullException">
        /// contained - ContentWidget must have Content assigned.
        /// </exception>
        public ContentInfoWidget(ContentWidget contained) : this() => this.LoadContentWidget(contained);

        /// <summary>
        /// Displays the specified ContentWidget in this info widget.
        /// </summary>
        public void LoadContentWidget(ContentWidget contentwidget)
        {
            //Check content
            if (contentwidget == null)
                throw new ArgumentNullException(nameof(contentwidget));

            if (contentwidget.Content == null)
                throw new ArgumentNullException(nameof(contentwidget.Content), "ContentWidget must have Content assigned.");

            //Assign widget
            contentwidget.Dock = DockStyle.Fill;
            this.ContentWidget = contentwidget;
            this.contentWidgetPanel.Controls.Clear();
            this.contentWidgetPanel.Controls.Add(contentwidget);

            //Extend header strip
            if (contentwidget.HeaderStrip != null)
            {
                //TODO: ContentInfoWidget - if there was already a CW loaded, be sure to remove its old headerstrip items first
                this.togglesToolStrip.Items.Add(new ToolStripSeparator());
                foreach (var item in contentwidget.HeaderStrip.Items.OfType<ToolStripItem>())
                {
                    this.togglesToolStrip.Items.Add(item);
                }
            }
        }

        /// <summary>
        /// Gets the content widget to show inside of the main info panel.
        /// </summary>
        public ContentWidget ContentWidget
        {
            get => this._contentWidget;
            protected set
            {
                if (this._contentWidget == value)
                    return;

                if (value.Content == null)
                    throw new ArgumentNullException(nameof(value), "ContentWidget must have Content assigned.");

                //Remove events
                if (this.Content != null)
                {
                    this.Content.IsDirtyChanged -= this.Content_UpdateTitle;
                    this.Content.FilePathChanged -= this.Content_UpdateTitle;
                }

                this._contentWidget = value;
                this.contentInfoPanel.Enabled = this.Content != null;

                //Update icon
                Icon icon = null;
                var bmp = this.Content?.Image;
                if (bmp != null)
                    icon = Icon.FromHandle(bmp.GetHicon());
                this.Icon = icon ?? Properties.Resources.exclamation; //Fallback icon otherwise

                this.UpdateTitle();

                //Add events
                this.Content.IsDirtyChanged += this.Content_UpdateTitle;
                this.Content.FilePathChanged += this.Content_UpdateTitle;

                //Databindings
                this.pathTextBox.DataBindings.Clear();
                this.pathTextBox.DataBindings.Add("Text", this.Content, "FilePath", false, DataSourceUpdateMode.OnPropertyChanged);

                //TODO: ContentInfoWidget - only load dependency graph either if already cached or once the tab is opened (even then, if possible do async loading)
                this.dependencyGraphViewer.Content = this.Content;
            }
        }

        private void Content_UpdateTitle(object sender, EventArgs e) => this.UpdateTitle();

        /// <summary>
        /// Updates the title of this widget.
        /// </summary>
        public void UpdateTitle()
        {
            if (this.Content?.FilePath == null)
            {
                this.Text = Properties.Resources.NoContentError;
                this.TabText = Properties.Resources.NoContentError;
            }
            else
            {
                this.Text = this.Content.GetFormattedName(Properties.Settings.Default.FullTitleFormat, true);
                this.TabText = this.Content.GetFormattedName(Properties.Settings.Default.TabTitleFormat, true);
            }
        }

        private ContentWidget _contentWidget;

        /// <summary>
        /// The content assigned to this info widget.
        /// </summary>
        public Content Content => this.ContentWidget?.Content;

        public Command CutCommand => (this.ContentWidget as IEditableData)?.CutCommand;
        public Command CopyCommand => (this.ContentWidget as IEditableData)?.CopyCommand;
        public Command PasteCommand => (this.ContentWidget as IEditableData)?.PasteCommand;
        public Command DeleteCommand => (this.ContentWidget as IEditableData)?.DeleteCommand;
        public bool CanDelete => this.infoToolStripButton.Checked ? false : (this.ContentWidget as IEditableData)?.CanDelete ?? false;
        public bool CanCopy => this.infoToolStripButton.Checked ? false : (this.ContentWidget as IEditableData)?.CanCopy ?? false;
        public bool CanPaste => this.infoToolStripButton.Checked ? false : (this.ContentWidget as IEditableData)?.CanPaste ?? false;

        private void browseButton_Click(object sender, EventArgs e)
        {
            //"Save" content to new place
            if (Properties.Settings.Default.NativeFileBrowser)
            {
                //Native file selection
                var res = IOHelper.SaveFileDialog(this.Content, false, false);
                if (res != null)
                    this.pathTextBox.Text = SFM.GetLocalPath(res, RelativeTo.GameDir);
            }
            else
            {
                //Use content browser
                var cb = new ContentBrowserWindow(false, true);
                var res = cb.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    //Update path
                    this.pathTextBox.Text = SFM.GetLocalPath(cb.SelectedFiles.SingleOrDefault().NodePath, RelativeTo.GameDir);
                }
            }
        }

        private void viewPathButton_Click(object sender, EventArgs e)
        {
            //Open content path in explorer
            IOHelper.OpenInExplorer(this.pathTextBox.Text, true);
        }

        private void ContentInfoWidget_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Only prompt if unsaved content and *user* closed the widget (CloseReason == e.UserClosing)
            //When form closes (CloseReason == MdiFormClosing), should possibly be handled with the all-in-one dialog
            if (e.CloseReason == CloseReason.UserClosing && this.Content != null && this.Content.IsDirty)
            {
                //Save content?
                e.Cancel = this.Content.ShowSavePrompt() == DialogResult.Cancel;
            }
        }

        /// <summary>
        /// Returns additional persist information to be stored in the widget when saving.
        /// </summary>
        /// <remarks>Override <see cref="InitializePersist" /> to change how to handle this data.</remarks>
        protected override string GetPersist()
        {
            //If a saved content file was opened, store its full file path
            if (this.Content != null && !this.Content.FakeFile)
                return this.Content.FilePath;
            return null;
        }

        /// <summary>
        /// Called after creating a widget from persist, but before being added to the DockPanel. Used to initialize further information for the widget.
        /// <para />Returns whether the widget should be added to the DockPanel (this should be <c>false</c> for any critical parsing errors).
        /// </summary>
        /// <param name="persistString">The persist string that was</param>
        /// <remarks>This widget should also override <see cref="GetPersist" />, to change how to store the string in the first place.</remarks>
        public override bool InitializePersist(string persistString)
        {
            if (string.IsNullOrEmpty(persistString))
                return true;

            //If a content file was stored in persist, load it
            if (!System.IO.File.Exists(persistString))
            {
                //ERROR
                FallbackTaskDialog.ShowDialog("Unable to reload Content, as its file was not found: " + persistString, null, "Unable to reload Content",
                    FallbackDialogIcon.Error, new FallbackDialogButton(DialogResult.OK));
                return false;
            }

            //Get content instance
            var content = Content.NewContentOfType(persistString, true, type: null);
            this.LoadContentWidget(content.CreateWidget());
            return true;
        }
    }
}
