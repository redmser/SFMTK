﻿using System.ComponentModel;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace SFMTK.Widgets
{
    /// <summary>
    /// Extension of dockable content for DockPanels.
    /// </summary>
    public class Widget : DockContent, IThemeable
    {
        /// <summary>
        /// Gets or sets the theme currently active on this control.
        /// </summary>
        public virtual string ActiveTheme { get; set; }

        /// <summary>
        /// Gets or sets whether Text and TabText should be separate properties.
        /// <para/>Text refers to what is shown in the Window menu, while TabText is what is displayed on the tab when docked.
        /// </summary>
        [Description("Text and TabText should be separate properties."), DefaultValue(false)]
        public bool SeparateTextAndTabText { get; set; } = false;

        /// <summary>
        /// Gets or sets the text to show in the Window menu. Full title of the widget.
        /// </summary>
        public override string Text
        {
            get => this.SeparateTextAndTabText ? base.Text : this.TabText;
            set
            {
                if (this.SeparateTextAndTabText)
                    base.Text = value;
                else
                    this.TabText = value;
            }
        }

        /// <summary>
        /// Gets or sets the text to use for a generic instance of this type, for example for labeling in menu entries. Should contain accelerator keys '&amp;'.
        /// </summary>
        [Description("Text for use in menu entries. Should use accelerator keys '&'.")]
        public virtual string MenuText
        {
            get => this._menuText ?? "&" + this.TabText;
            set => this._menuText = value;
        }
        private string _menuText;

        /// <summary>
        /// Gets or sets whether this Widget should have a command associated to it.
        /// </summary>
        [DefaultValue(true), Description("If this Widget should have a command associated to it.")]
        public bool GenerateShowCommand { get; set; } = true;

        /// <summary>
        /// Gets or sets the default shortcut for this widget.
        /// </summary>
        [Description("The default shortcut for this widget.")]
        public virtual Keys DefaultShortcut { get; set; }

        /// <summary>
        /// Called after creating a widget from persist, but before being added to the DockPanel. Used to initialize further information for the widget.
        /// <para/>Returns whether the widget should be added to the DockPanel (this should be <c>false</c> for any critical parsing errors).
        /// </summary>
        /// <param name="persistString">The persist string that was stored for this widget.</param>
        /// <remarks>This widget should also override <see cref="GetPersist"/>, to change how to store the string in the first place.</remarks>
        public virtual bool InitializePersist(string persistString) => true;

        /// <summary>
        /// Returns the full type name, and optionally (if not <c>null</c>) the data of <see cref="GetPersist"/> delimited by a space.
        /// </summary>
        protected sealed override string GetPersistString()
        {
            var persist = this.GetPersist();
            if (persist == null)
                return this.GetType().FullName;
            return $"{this.GetType().FullName} {persist}";
        }

        /// <summary>
        /// Returns additional persist information to be stored in the widget when saving.
        /// </summary>
        /// <remarks>Override <see cref="InitializePersist"/> to change how to handle this data.</remarks>
        protected virtual string GetPersist() => null;

        /// <summary>
        /// Called when this widget is about to be added to a DockPanel, but has not been loaded from a layout XML (see <see cref="InitializePersist(string)"/>).
        /// </summary>
        public virtual void InitializeInstance()
        {

        }
    }
}
