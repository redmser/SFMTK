﻿namespace SFMTK.Widgets
{
    /// <summary>
    /// A widget for displaying undo history.
    /// </summary>
    /// <seealso cref="WeifenLuo.WinFormsUI.Docking.DockContent" />
    public partial class UndoHistoryWidget : Widget
    {
        //NYI: Undo History widget (does not live-update right now!)

        /// <summary>
        /// Initializes a new instance of the <see cref="UndoHistoryWidget"/> class.
        /// </summary>
        public UndoHistoryWidget()
        {
            InitializeComponent();

            //Add data source
            this.historyVirtualObjectListView.VirtualListDataSource = Program.MainCH?.DataSource;

            //Empty list message
            this.historyVirtualObjectListView.EmptyListMsgOverlay = new Controls.EmptyListOverlay(this.historyVirtualObjectListView);
        }
    }
}
