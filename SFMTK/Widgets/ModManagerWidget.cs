﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrightIdeasSoftware;
using SFMTK.Commands;
using SFMTK.Controls;
using SFMTK.Data;

namespace SFMTK.Widgets
{
    /// <summary>
    /// Widget for getting a list of loaded mods and modifying it.
    /// </summary>
    /// <seealso cref="SFMTK.IEditableData" />
    /// <seealso cref="WeifenLuo.WinFormsUI.Docking.DockContent" />
    public partial class ModManagerWidget : Widget, IEditableData
    {
        //TODO: ModManagerWidget - list context menu for different actions
        //TODO: ModManagerWidget - option to migrate content from one mod to another
        //TODO: ModManagerWidget - cell editing for name/description/category (as well as rightclick option to edit?)
        //TODO: ModManagerWidget - list all mod folders which are in SFM's game dir as well, but not part of the gameinfo.txt file (maybe inform about this when enabling them)
        //TODO: ModManagerWidget - highlight any invalid mod setups (missing directory, directory without mod entry, ...) using an icon and maybe text color
        //TODO: ModManagerWidget - EITHER integrate a treeview into the mod manager to browse for content inside of the mods (similar to VS' solution explorer) - only works if checkboxes are root-level only
        //  OR have a second widget which allows for editing of .sfm files (since otherwise they're seen as purely content, which would make them useless compared to mods)

        /// <summary>
        /// Initializes a new instance of the <see cref="ModManagerWidget"/> class.
        /// </summary>
        public ModManagerWidget()
        {
            //Compiler generated
            InitializeComponent();

            //OLV setup
            this.UpdateReorderDropSink();
            this.modOrderColumn.AspectGetter = (o) => SFM.GameInfo.Mods.IndexOf((Mod)o);
            this.modObjectListView.DefaultRenderer = new HighlightTextRenderer();

            //TODO: ModManagerWidget - allow drag&dropping folder in as a new mod folder, allow dropping content pack / sfm pack as new mod
            //BUG: ModManagerWidget - pretty weird drag&drop behaviour in certain case:
            //  If layout with a ModManager is loaded, dragging&dropping a mod will cause the list to go broken (IsDisposed = true, doesn't allow more reorder actions)
            //  oldIndex == newIndex here (despite the items supposedly moving due to the DropSink)
            //  If your layout had a ModManager in it, even creating a NEW ModManager will break that one - you have to load a layout without any ModManager in it
        }

        protected override void OnLoad(EventArgs e)
        {
            //Events
            SFM.BasePathChanged += this.OnBasePathUpdated;
            this.OnBasePathUpdated(null, EventArgs.Empty);

            base.OnLoad(e);
        }

        //If reordering the list while having search text, the entry gets repositioned very badly -> simply disallow reorder then
        private void UpdateReorderDropSink()
            => this.modObjectListView.DropSink = string.IsNullOrEmpty(this.searchToolStripTextBox.Text) ? new RearrangingDropSinkEx(false) { UpdateDataListView = false } : null;

        private void modObjectListView_SelectionChanged(object sender, EventArgs e) => CommandManager.UpdateCommandsEnabled(nameof(Forms.Main), typeof(EditCommand));

        private void OnBasePathUpdated(object sender, EventArgs e)
        {
            //If valid, remove error, else show it!
            this.InvokeIfRequired(() =>
            {
                this.errorLinkLabel.Visible = !SFM.ValidPaths;
                this.toolStrip.Visible = SFM.ValidPaths;

                if (SFM.GameInfo?.Mods != null)
                    SFM.GameInfo.Mods.CollectionChanged -= this.ModsList_CollectionChanged;

                this.modObjectListView.DataSource = SFM.ValidPaths ? SFM.GameInfo.Mods : null;

                if (SFM.GameInfo?.Mods != null)
                    SFM.GameInfo.Mods.CollectionChanged += this.ModsList_CollectionChanged;
            });
        }

        private void ModsList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //Rebuild the mod list
            this.modObjectListView.BuildList(true);
        }

        private void errorLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //Open settings to adjust sfm path
            Program.MainForm.OpenSettings("Environment/General", true);
        }

        private void importToolStripButton_Click(object sender, EventArgs e)
        {
            //NYI: ModManagerWidget - Create content pack from imported content list (either to new mod or to existing mod)
            //OR import content to a mod
            //OR import mod from existing folder
            throw new NotImplementedException();
        }

        private void removeToolStripButton_Click(object sender, EventArgs e)
        {
            //NYI: ModManagerWidget - Choice: Remove but migrate content, Remove and kill all content (CONFIRM AS MUCH AS POSSIBLE -> UE4 did this nicely with used content), and cancel
            throw new NotImplementedException();
        }

        private void modObjectListView_ModelDropped(object sender, BrightIdeasSoftware.ModelDropEventArgs e)
        {
            //Store selections
            var selected = this.modObjectListView.SelectedIndices.Cast<int>().ToDictionary(i => this.modObjectListView.GetModelObject(i));

            //HACK: ModManagerWidget - this delay is needed to ensure that the drop event has occured at this point
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(50);
                this.Invoke(new Action(() =>
                {
                    //BUG: ModManagerWidget: calculating new index (especially on multiple items) needs to be improved

                    //Add command(s)
                    if (e.SourceModels.Count == 1)
                    {
                        //Single command
                        var mod = e.SourceModels[0] as Mod;
                        var oldindex = selected[mod];
                        var newindex = this.modObjectListView.IndexOf(mod);
                        if (oldindex != newindex)
                            Program.MainCH.AddCommand(new ReorderModManagerCommand(mod, oldindex, newindex));
                    }
                    else
                    {
                        //Group it!
                        var commands = new List<Command>();
                        for (var i = 0; i < e.SourceModels.Count; i++)
                        {
                            var mod = e.SourceModels[i] as Mod;
                            var oldindex = selected[mod];
                            var newindex = this.modObjectListView.IndexOf(mod); //+ (e.SourceModels.Count - i) - 1;
                            if (oldindex != newindex)
                                commands.Add(new ReorderModManagerCommand(mod, oldindex, newindex));
                        }
                        if (commands.Count > 0)
                            Program.MainCH.AddCommand(new CommandGroup($"Reorder {e.SourceModels.Count} Mods", commands));
                    }
                }));
            });
        }

        private void searchToolStripTextBox_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.searchToolStripTextBox.Text))
            {
                //Stop filter
                this.modObjectListView.AdditionalFilter = null;
            }
            else
            {
                //Filtering
                this.modObjectListView.AdditionalFilter = new ModFilter(this.searchToolStripTextBox.Text);
                ((HighlightTextRenderer)this.modObjectListView.DefaultRenderer).Filter
                    = new TextMatchFilter(this.modObjectListView, this.searchToolStripTextBox.Text, StringComparison.InvariantCultureIgnoreCase);
            }

            this.UpdateReorderDropSink();
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            //Events
            SFM.BasePathChanged -= this.OnBasePathUpdated;

            base.OnFormClosed(e);
        }

        //NYI: ModManagerWidget - implement EditableData interface
        public Command CutCommand => throw new NotImplementedException();
        public Command CopyCommand => throw new NotImplementedException();
        public Command PasteCommand => throw new NotImplementedException();
        public Command DeleteCommand => throw new NotImplementedException();
        public bool CanDelete => this.modObjectListView.SelectedObjects.Count > 0 && (this.modObjectListView.SelectedObject as Mod)?.Name != "usermod";
        public bool CanCopy => this.modObjectListView.SelectedObjects.Count > 0;
        public bool CanPaste => Clipboard.ContainsData(Constants.ModClipboardFormat);
    }
}
