﻿namespace SFMTK.Widgets
{
    partial class UndoHistoryWidget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UndoHistoryWidget));
            this.historyVirtualObjectListView = new BrightIdeasSoftware.VirtualObjectListView();
            this.nameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.historyVirtualObjectListView)).BeginInit();
            this.SuspendLayout();
            // 
            // historyVirtualObjectListView
            // 
            this.historyVirtualObjectListView.AllColumns.Add(this.nameColumn);
            this.historyVirtualObjectListView.CellEditUseWholeCell = false;
            this.historyVirtualObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameColumn});
            this.historyVirtualObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.historyVirtualObjectListView.EmptyListMsg = "Your undo history is currently empty.";
            this.historyVirtualObjectListView.FullRowSelect = true;
            this.historyVirtualObjectListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.historyVirtualObjectListView.HideSelection = false;
            this.historyVirtualObjectListView.Location = new System.Drawing.Point(0, 0);
            this.historyVirtualObjectListView.Name = "historyVirtualObjectListView";
            this.historyVirtualObjectListView.ShowGroups = false;
            this.historyVirtualObjectListView.Size = new System.Drawing.Size(284, 262);
            this.historyVirtualObjectListView.TabIndex = 0;
            this.historyVirtualObjectListView.UseCompatibleStateImageBehavior = false;
            this.historyVirtualObjectListView.View = System.Windows.Forms.View.Details;
            this.historyVirtualObjectListView.VirtualMode = true;
            // 
            // nameColumn
            // 
            this.nameColumn.AspectName = "DisplayText";
            this.nameColumn.AspectToStringFormat = "";
            this.nameColumn.FillsFreeSpace = true;
            this.nameColumn.Text = "Name";
            // 
            // UndoHistoryWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.historyVirtualObjectListView);
            this.DefaultShortcut = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.Z)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MenuText = "&Undo History";
            this.Name = "UndoHistoryWidget";
            this.TabText = "Undo History";
            this.Text = "Undo History";
            ((System.ComponentModel.ISupportInitialize)(this.historyVirtualObjectListView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.VirtualObjectListView historyVirtualObjectListView;
        private BrightIdeasSoftware.OLVColumn nameColumn;
    }
}
