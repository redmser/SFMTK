﻿namespace SFMTK.Widgets
{
    partial class ModelViewerWidget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModelViewerWidget));
            this.containerPanel = new System.Windows.Forms.Panel();
            this.loadingLabel = new System.Windows.Forms.LinkLabel();
            this.unpinButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // containerPanel
            // 
            this.containerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.containerPanel.Location = new System.Drawing.Point(0, 0);
            this.containerPanel.Name = "containerPanel";
            this.containerPanel.Size = new System.Drawing.Size(624, 442);
            this.containerPanel.TabIndex = 0;
            // 
            // loadingLabel
            // 
            this.loadingLabel.BackColor = System.Drawing.SystemColors.Control;
            this.loadingLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loadingLabel.Location = new System.Drawing.Point(0, 0);
            this.loadingLabel.Name = "loadingLabel";
            this.loadingLabel.Size = new System.Drawing.Size(624, 442);
            this.loadingLabel.TabIndex = 1;
            this.loadingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.loadingLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.loadingLabel_LinkClicked);
            // 
            // unpinButton
            // 
            this.unpinButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.unpinButton.Image = global::SFMTK.Properties.Resources.link_break;
            this.unpinButton.Location = new System.Drawing.Point(601, 0);
            this.unpinButton.Name = "unpinButton";
            this.unpinButton.Size = new System.Drawing.Size(23, 23);
            this.unpinButton.TabIndex = 0;
            this.toolTip1.SetToolTip(this.unpinButton, "Unpins the model viewer from SFMTK");
            this.unpinButton.UseVisualStyleBackColor = true;
            this.unpinButton.Visible = false;
            this.unpinButton.Click += new System.EventHandler(this.unpinButton_Click);
            // 
            // ModelViewerWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 442);
            this.Controls.Add(this.loadingLabel);
            this.Controls.Add(this.unpinButton);
            this.Controls.Add(this.containerPanel);
            this.DefaultShortcut = System.Windows.Forms.Keys.F5;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MenuText = "Model &Viewer";
            this.Name = "ModelViewerWidget";
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.Document;
            this.TabText = "Model Viewer";
            this.Text = "Model Viewer";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel containerPanel;
        private System.Windows.Forms.Button unpinButton;
        private System.Windows.Forms.LinkLabel loadingLabel;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
