﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SFMTK.Tasks
{
    /// <summary>
    /// Represents a task which can be run. Optionally tracks progress and allows for cancelling.
    /// </summary>
    /// <typeparam name="TProgress">The type of the progress values to report.</typeparam>
    /// <typeparam name="TValue">The type of the value to return.</typeparam>
    public abstract class TaskBase<TProgress, TValue>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskBase"/> class.
        /// </summary>
        protected TaskBase()
        {

        }

        /// <summary>
        /// Gets or sets the display name for the task. Should describe what it is doing in a short way.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a short description for the task. Optional, can provide additional information.
        /// </summary>
        public string Description { get; set; }

        protected IProgress<TProgress> Progress { get; set; }

        /// <summary>
        /// Gets whether this <see cref="TaskBase{TProgress}"/> is able to report progress while running.
        /// </summary>
        public virtual bool CanReportProgress => this.Progress != null;

        protected CancellationTokenSource CancelTokenSource { get; set; }

        /// <summary>
        /// Gets whether this <see cref="TaskBase{TProgress}"/> is able to cancel its task while running.
        /// </summary>
        public virtual bool CanCancel => this.CancelTokenSource != null;

        /// <summary>
        /// Gets whether this <see cref="TaskBase{TProgress}"/> is currently running.
        /// </summary>
        public bool IsRunning { get; protected set; }

        /// <summary>
        /// Gets whether this task was cancelled.
        /// </summary>
        public bool Cancelled { get; protected set; }

        /// <summary>
        /// Runs the task, raising the <see cref="TaskFinished" /> event once finished.
        /// </summary>
        /// <exception cref="InvalidOperationException">This task is already running.</exception>
        public Task<TValue> Run()
        {
            if (this.IsRunning)
                throw new InvalidOperationException("This task is already running.");

            //Reset values
            this.Cancelled = false;

            //Indicate that work is being done
            this.IsRunning = true;

            //Do work
            this.PrepareWork();
            var task = this.DoWork();
            task.ContinueWith(this.OnFinishedWork);
            
            return task;
        }

        protected virtual void PrepareWork() { }

        /// <summary>
        /// Tells the currently running task to cancel.
        /// </summary>
        /// <exception cref="InvalidOperationException">This task can not be cancelled.</exception>
        public virtual void Cancel()
        {
            if (!this.CanCancel)
                throw new InvalidOperationException("This task can not be cancelled.");

            this.Cancelled = true;
            this.CancelTokenSource.Cancel();
            this.TaskShouldCancel?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Reports progress of the task.
        /// </summary>
        /// <param name="progress">The new progress value.</param>
        /// <exception cref="InvalidOperationException">This task can not report progress.</exception>
        public virtual void ReportProgress(TProgress progress)
        {
            if (!this.CanReportProgress)
                throw new InvalidOperationException("This task can not report progress.");

            this.Progress.Report(progress);
        }

        /// <summary>
        /// Invoked once the running task has finished.
        /// </summary>
        /// <param name="obj">The task which finished running.</param>
        protected virtual void OnFinishedWork(Task<TValue> obj)
        {
            this.IsRunning = false;
            this.TaskFinished?.Invoke(this, new TaskFinishedEventArgs<TValue>(obj, this.Cancelled));
        }

        protected abstract Task<TValue> DoWork();

        /// <summary>
        /// Occurs when a task finished executing.
        /// </summary>
        public event EventHandler<TaskFinishedEventArgs<TValue>> TaskFinished;

        /// <summary>
        /// Occurrs when the cancel method is called. The task is not yet cancelled.
        /// </summary>
        public event EventHandler TaskShouldCancel;
    }

    /// <summary>
    /// Event arguments for the <see cref="TaskBase{TProgress, TValue}.TaskFinished"/> event.
    /// </summary>
    /// <seealso cref="System.EventArgs" />
    public class TaskFinishedEventArgs<TValue> : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskFinishedEventArgs{TValue}"/> class.
        /// </summary>
        public TaskFinishedEventArgs(Task<TValue> task, bool cancelled) : base()
        {
            this.Task = task;
            this.Cancelled = cancelled;
        }

        /// <summary>
        /// Gets the task that finished running.
        /// </summary>
        public Task<TValue> Task { get; }

        /// <summary>
        /// Gets the result of the task.
        /// </summary>
        public TValue Result => this.Task.Result;

        /// <summary>
        /// Gets whether this task finished due to being cancelled.
        /// </summary>
        public bool Cancelled { get; }
    }
}
