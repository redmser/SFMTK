﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SFMTK.Tasks
{
    /// <summary>
    /// A queue for background tasks.
    /// </summary>
    public class TaskQueue<TTask, TProgress, TValue> : IEnumerable<TTask>, IEnumerable, ICollection, IReadOnlyCollection<TTask> where TTask : TaskBase<TProgress, TValue>
    {
        /// <summary>
        /// Gets the currently running task.
        /// </summary>
        public TTask ActiveTask { get; private set; }

        /// <summary>
        /// Gets the lock used to restrict access to the <see cref="ActiveTask"/> property.
        /// </summary>
        public object ActiveTaskLock { get; } = new object();

        /// <summary>
        /// Gets whether a background task is currently running.
        /// </summary>
        public bool IsBusy => this.ActiveTask != null;

        /// <summary>
        /// Adds a new task to handle to the end of the queue. Returns the task that was started.
        /// </summary>
        public Task<TValue> AddTask(TTask task)
        {
            if (task.IsRunning)
                throw new InvalidOperationException("A task added to the queue may not already be running!");

            this._backingQueue.Enqueue(task);
            return this.UpdateQueue();
        }

        private Task<TValue> UpdateQueue()
        {
            //Is a task active?
            if (this.ActiveTask != null && this.ActiveTask.IsRunning)
            {
                //Is busy, let it be...
                return null;
            }

            //No task active, continue down the line
            if (this._backingQueue.Count == 0)
            {
                //Stop work
                lock (this.ActiveTaskLock)
                {
                    this.ActiveTask = null;
                }
                this.OnQueueUpdated(EventArgs.Empty);
                return null;
            }

            //Start next task
            lock (this.ActiveTaskLock)
            {
                this.ActiveTask = this._backingQueue.Dequeue();
            }
            this.OnQueueUpdated(EventArgs.Empty);
            this.ActiveTask.TaskFinished += (s, e) => this.UpdateQueue();
            return this.ActiveTask.Run();
        }

        /// <summary>
        /// Occurs when the queue state is updated, either by changing running task, or by no task running anymore.
        /// </summary>
        public event EventHandler QueueUpdated;

        /// <summary>
        /// Raises the <see cref="E:QueueUpdated" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnQueueUpdated(EventArgs e) => this.QueueUpdated?.Invoke(this, e);

        private Queue<TTask> _backingQueue = new Queue<TTask>();

        public int Count => ((IReadOnlyCollection<TTask>)this._backingQueue).Count;

        public object SyncRoot => ((ICollection)this._backingQueue).SyncRoot;

        public bool IsSynchronized => ((ICollection)this._backingQueue).IsSynchronized;

        public IEnumerator<TTask> GetEnumerator() => ((IReadOnlyCollection<TTask>)this._backingQueue).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => ((IReadOnlyCollection<TTask>)this._backingQueue).GetEnumerator();
        public void CopyTo(Array array, int index) => ((ICollection)this._backingQueue).CopyTo(array, index);
    }
}
