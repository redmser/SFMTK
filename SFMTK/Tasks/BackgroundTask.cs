﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SFMTK.Tasks
{
    /// <summary>
    /// A task which runs in the background. Progress is displayed on the status bar, as well as optional progress report and cancel button.
    /// </summary>
    /// <seealso cref="SFMTK.Tasks.TaskBase{SFMTK.ProgressValue, TValue}" />
    public abstract class BackgroundTask : TaskBase<ProgressValue, object>
    {
        /// <summary>
        /// Creates a new BackgroundTask which executed the given function.
        /// </summary>
        /// <param name="function">The function to execute asynchronously.</param>
        /// <param name="name">The name of the task to display on the status strip.</param>
        /// <param name="canreportprogress">Whether progress is reported on a progress bar.
        /// If <c>null</c>, instead of showing no progress bar at all, show marquee instead.</param>
        /// <param name="cancancel">If set to <c>true</c>, the operation can be cancelled using a button.
        /// You should also set <paramref name="canreportprogress"/> to either <c>true</c> or <c>null</c> in this case, for prettier UI display.</param>
        public static BackgroundTask FromDelegate(Func<object> function, string name, bool? canreportprogress = false, bool cancancel = false)
            => new FunctionBackgroundTask(function, name, canreportprogress, cancancel);

        /// <summary>
        /// Creates a new BackgroundTask which executed the given action.
        /// </summary>
        /// <param name="action">The action to execute asynchronously.</param>
        /// <param name="name">The name of the task to display on the status strip.</param>
        /// <param name="canreportprogress">Whether progress is reported on a progress bar.
        /// If <c>null</c>, instead of showing no progress bar at all, show marquee instead.</param>
        /// <param name="cancancel">If set to <c>true</c>, the operation can be cancelled using a button.
        /// You should also set <paramref name="canreportprogress"/> to either <c>true</c> or <c>null</c> in this case, for prettier UI display.</param>
        public static BackgroundTask FromDelegate(Action action, string name, bool? canreportprogress = false, bool cancancel = false)
            => new FunctionBackgroundTask(() => { action(); return null; }, name, canreportprogress, cancancel);

        /// <summary>
        /// Creates a new BackgroundTask which executed the given list of actions in order, dynamically updating their names.
        /// Progress is reported based on which task is currently being ran.
        /// </summary>
        /// <param name="actions">The list of actions to execute asynchronously one after another.</param>
        /// <param name="names">The list of names of the tasks to display on the status strip.</param>
        /// <param name="cancancel">If set to <c>true</c>, the operation can be cancelled using a button.
        /// You should also set <paramref name="canreportprogress" /> to either <c>true</c> or <c>null</c> in this case, for prettier UI display.</param>
        /// <param name="nameformat">Optionally re-format each of the names in the list.</param>
        public static BackgroundTask FromDelegate(IEnumerable<Action> actions, IEnumerable<string> names, bool cancancel = false, string nameformat = "{0}")
            => new ChainedBackgroundTask(actions, names, cancancel, nameformat);

        private class FunctionBackgroundTask : BackgroundTask
        {
            public FunctionBackgroundTask(Func<object> function, string name, bool? canreportprogress, bool cancancel)
            {
                //IMP: FunctionBackgroundTask - move this initialization code to the base BackgroundTask instead... only the DoWork should be different here!
                //  -> same for ChainedBackgroundTask...
                if (cancancel)
                    this.CancelTokenSource = new CancellationTokenSource();
                if (!canreportprogress.HasValue)
                {
                    //Null? MARQUEE
                    Program.MainForm.InvokeIfRequired(() =>
                    {
                        Program.MainForm.mainToolStripProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
                        Program.MainForm.mainToolStripProgressBar.Visible = true;
                    });
                }
                else if (canreportprogress.Value)
                {
                    var prog = new Progress<ProgressValue>();
                    prog.ProgressChanged += Prog_ProgressChanged;
                    this.Progress = prog;
                }
                this.Name = name;
                this._function = function;



                void Prog_ProgressChanged(object sender, ProgressValue e)
                {
                    Program.MainForm.InvokeIfRequired(() =>
                    {
                        Program.MainForm.mainToolStripProgressBar.ApplyProgress(e);
                        Program.MainForm.mainToolStripProgressBar.Visible = true;
                        Program.MainForm._activeTaskEntry.Text = e.Name;
                    });
                }
            }

            private Func<object> _function;

            protected override Task<object> DoWork() => Task.Run(this._function);
        }

        private class ChainedBackgroundTask : BackgroundTask
        {
            public ChainedBackgroundTask(IEnumerable<Action> actions, IEnumerable<string> names, bool cancancel, string nameformat)
            {
                if (cancancel)
                    this.CancelTokenSource = new CancellationTokenSource();

                var prog = new Progress<ProgressValue>();
                prog.ProgressChanged += Prog_ProgressChanged;
                this.Progress = prog;
                this._actions = actions;
                this._names = names;
                this._count = names.Count();
                this._nameformat = nameformat;



                void Prog_ProgressChanged(object sender, ProgressValue e)
                {
                    Program.MainForm.InvokeIfRequired(() =>
                    {
                        Program.MainForm.mainToolStripProgressBar.ApplyProgress(e);
                        Program.MainForm.mainToolStripProgressBar.Visible = true;
                        Program.MainForm._activeTaskEntry.Text = e.Name;
                    });
                }
            }

            private string _nameformat;
            private IEnumerable<Action> _actions;
            private IEnumerable<string> _names;
            private int _count;

            protected override Task<object> DoWork()
            {
                return Task.Run<object>(() =>
                {
                    var fn = _actions.GetEnumerator();
                    var nm = _names.GetEnumerator();
                    var i = 0;
                    while (fn.MoveNext())
                    {
                        i++;
                        nm.MoveNext();

                        this.ReportProgress(new ProgressValue(i, this._count) { Name = string.Format(this._nameformat, nm.Current) });

                        //Invoke the function
                        fn.Current();

                        if (this.CancelTokenSource.IsCancellationRequested)
                            break;
                    }

                    return null;
                });
            }
        }
    }
}
