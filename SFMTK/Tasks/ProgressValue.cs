﻿using System;
using System.Threading;

namespace SFMTK
{
    /// <summary>
    /// A simple value to pass for the <see cref="IProgress{T}"/> interface.
    /// </summary>
    public struct ProgressValue : IEquatable<ProgressValue>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressValue" /> struct.
        /// </summary>
        /// <param name="value">The progress value. Between 0 and Maximum.</param>
        /// <param name="maximum">The maximum for <paramref name="value"/>. Must be greater than zero.</param>
        public ProgressValue(decimal value = 0, decimal maximum = 100)
        {
            this.Value = value;
            this.Maximum = maximum;
            this.Marquee = false;
            this.State = ProgressBarState.Default;
            this.IsDone = false;
            this.Name = null;
            this.Text = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressValue"/> struct.
        /// </summary>
        /// <param name="marquee">Whether the exact progress is unknown, resulting in a progress bar with a marquee effect.</param>
        /// <param name="state">The state of the progress bar, determining its display color.</param>
        public ProgressValue(bool marquee, ProgressBarState state)
        {
            this.Value = 0;
            this.Maximum = 100;
            this.Marquee = marquee;
            this.State = state;
            this.IsDone = false;
            this.Name = null;
            this.Text = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressValue"/> struct.
        /// </summary>
        /// <param name="done">If set to <c>true</c>, report that the action is completed and UI should be cleaned up.</param>
        internal ProgressValue(bool done) : this()
        {
            this.IsDone = true;
        }

        /// <summary>
        /// Gets whether this progress action is done.
        /// </summary>
        public bool IsDone { get; }

        /// <summary>
        /// Gets or sets the progress value. Between 0 and Maximum.
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Gets or sets the maximum for <see cref="Value"/>. Must be greater than zero.
        /// </summary>
        public decimal Maximum { get; set; }

        /// <summary>
        /// Gets or sets the percentage as a value between 0 and 100.
        /// </summary>
        public decimal Percentage
        {
            get => Value / Maximum * 100;
            set => Value = value / 100 * Maximum;
        }

        /// <summary>
        /// Gets or sets whether the exact progress is unknown, resulting in a progress bar with a marquee effect.
        /// </summary>
        public bool Marquee { get; set; }

        /// <summary>
        /// Gets or sets the state of the progress bar, determining its display color.
        /// </summary>
        public ProgressBarState State { get; set; }

        /// <summary>
        /// Gets or sets a name or title string. Will not update the value if <c>null</c>.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a text or description string. Will not update the value if <c>null</c>.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Determines whether the specified <see cref="object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
                return false;

            return this.Equals((ProgressValue)obj);
        }

        /// <summary>
        /// Determines whether the specified <see cref="ProgressValue" />, is equal to this instance.
        /// </summary>
        /// <param name="value">The <see cref="ProgressValue" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="ProgressValue" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public bool Equals(ProgressValue value) => this.GetHashCode() == value.GetHashCode();

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        public static bool operator ==(ProgressValue v1, ProgressValue v2) => v1.Equals(v2);

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        public static bool operator !=(ProgressValue v1, ProgressValue v2) => !(v1.Equals(v2));

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode() => (this.Value, this.Maximum, this.Marquee, this.State, this.IsDone).GetHashCode();

        /// <summary>
        /// Performs an implicit conversion from <see cref="ProgressValue"/> to <see cref="decimal"/>.
        /// </summary>
        public static implicit operator decimal(ProgressValue v) => v.Value;

        /// <summary>
        /// Performs an implicit conversion from <see cref="decimal"/> to <see cref="ProgressValue"/>.
        /// </summary>
        public static implicit operator ProgressValue(decimal d) => new ProgressValue(d);

        /// <summary>
        /// Returns a <see cref="string" /> that represents the percentual value of this <see cref="ProgressValue"/> instance in the format "XX.X%".
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString() => Value.ToString("N1") + "%";
    }

    /// <summary>
    /// Data used to work with a progress operation.
    /// </summary>
    public class ProgressHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressHandler"/> class.
        /// </summary>
        public ProgressHandler()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressHandler" /> class.
        /// </summary>
        /// <param name="progress">The progress instance for this operation.</param>
        /// <param name="token">The CancellationTokenSource used to cancel a progress operation.</param>
        /// <param name="window">The progress window used to show progress, if any is created.</param>
        public ProgressHandler(Progress<ProgressValue> progress, CancellationTokenSource token, Forms.ProgressWindow window)
        {
            this.Progress = progress;
            this.Cancel = token;
            this.Window = window;
        }

        /// <summary>
        /// Gets the progress instance for this operation.
        /// <para/>If setting marquee and using CLI display, the bar will only update if you repeatedly call Report.
        /// The percentage specified does not change this behaviour at all.
        /// </summary>
        public IProgress<ProgressValue> Progress { get; }

        /// <summary>
        /// Gets the token source used to cancel a progress operation.
        /// </summary>
        public CancellationTokenSource Cancel { get; }

        /// <summary>
        /// Gets the progress window used to show progress, if any is created.
        /// </summary>
        public Forms.ProgressWindow Window { get; }

        /// <summary>
        /// Report progress to the display.
        /// </summary>
        public void ReportProgress(ProgressValue progress) => this.Progress.Report(progress);

        /// <summary>
        /// Tells the display that the task is completed, removing/hiding any user elements related to displaying the progress.
        /// <para/>If showing UI without a running application, be sure to also call ExitThread here.
        /// </summary>
        public void FinishTask() => this.Progress.Report(new ProgressValue(done: true));
    }

    /// <summary>
    /// The current state of the progress bar.
    /// </summary>
    public enum ProgressBarState
    {
        /// <summary>
        /// Default progress bar color. In UI, this is green by default. In CLI, this is white by default.
        /// </summary>
        Default = 1,
        /// <summary>
        /// Error progress bar color. In UI and CLI, this is red by default. Marquee can not be enabled on UI when this is set.
        /// </summary>
        Error = 2,
        /// <summary>
        /// Paused progress bar color. In UI and CLI, this is yellow by default. Marquee can not be enabled on UI when this is set.
        /// </summary>
        Paused = 3
    }
}
