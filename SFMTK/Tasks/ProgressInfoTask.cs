﻿using System.Threading.Tasks;

namespace SFMTK.Tasks
{
    /// <summary>
    /// A task which uses the ProgressInfo helper to show progress.
    /// </summary>
    /// <typeparam name="TValue">The type of the value to return.</typeparam>
    /// <seealso cref="SFMTK.Tasks.TaskBase{SFMTK.ProgressValue, TValue}" />
    public abstract class ProgressInfoTask<TValue> : TaskBase<ProgressValue, TValue>
    {
        /// <summary>
        /// Gets the default progress value, used to initialize the task.
        /// </summary>
        protected virtual ProgressValue DefaultProgressValue { get; }

        protected ProgressHandler ProgressHandler { get; set; }

        /// <summary>
        /// Always returns true - the task can be cancelled.
        /// </summary>
        public override bool CanCancel => true;

        /// <summary>
        /// Tells the currently running task to cancel.
        /// </summary>
        public override void Cancel()
        {
            this.CancelTokenSource.Cancel();
        }

        protected override void PrepareWork()
        {
            this.ProgressHandler = ProgressInfo.ShowProgress(this.Name, this.Description, this.CanCancel, this.DefaultProgressValue);
            this.Progress = this.ProgressHandler.Progress;
            this.CancelTokenSource = this.ProgressHandler.Cancel;
        }

        protected override void OnFinishedWork(Task<TValue> obj)
        {
            this.ProgressHandler.FinishTask();
            base.OnFinishedWork(obj);
        }
    }
}
