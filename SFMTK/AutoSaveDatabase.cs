﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SFMTK
{
    /// <summary>
    /// Internal database for storing multiple autosaved content files.
    /// </summary>
    public class AutoSaveDatabase
    {
        //UNTESTED: AutoSaveDatabase saving and loading
        //TODO: AutoSaveDatabase and contenttree share this code quite a bunch - maybe create a base for all serializable data
        //  with a subclass for storing and loading the basepath too

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoSaveDatabase"/> class.
        /// </summary>
        public AutoSaveDatabase()
        {
            this.BasePath = SFM.BasePath;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoSaveDatabase" /> class.
        /// </summary>
        /// <param name="path">The path to the database file.</param>
        /// <param name="load">If set to <c>true</c>, loads the specified database file directly.</param>
        public AutoSaveDatabase(string path, bool load) : this()
        {
            this.DataPath = path;

            if (load)
                this.Load();
        }

        /// <summary>
        /// Gets the active <see cref="AutoSaveDatabaseSerializer"/>.
        /// </summary>
        private readonly AutoSaveDatabaseSerializer _serializer = new AutoSaveDatabaseSerializer();

        /// <summary>
        /// Gets the autosaved content files loaded in the database.
        /// </summary>
        public List<AutoSavedContent> Files { get; } = new List<AutoSavedContent>();

        /// <summary>
        /// Gets or sets the path to the database file to load or save to by default.
        /// </summary>
        public string DataPath { get; set; }

        /// <summary>
        /// Gets SFM's base path as it was specified in the database file. Will be updated after a Load call.
        /// </summary>
        public string BasePath { get; private set; }

        /// <summary>
        /// Loads the database.
        /// </summary>
        public void Load() => this.Load(this.DataPath);

        /// <summary>
        /// Loads the specified database file.
        /// </summary>
        /// <param name="path">Path to the database.</param>
        public void Load(string path)
        {
            //Update path, if valid
            this.CheckPath(path, false);
            this.DataPath = path;

            //Read data from file, update variables from file
            this.Files.Clear();
            this.Files.AddRange(this._serializer.Deserialize(path));
            this.BasePath = this._serializer.BasePath;
        }

        /// <summary>
        /// Saves the database.
        /// </summary>
        public void Save() => this.Save(this.DataPath);

        /// <summary>
        /// Saves the database to the specified file.
        /// </summary>
        /// <param name="path">Path to the database.</param>
        public void Save(string path)
        {
            //Update path, if valid
            this.CheckPath(path, true);
            this.DataPath = path;

            //Write data to file, pass current variables
            this._serializer.BasePath = SFM.BasePath;
            this._serializer.Serialize(this.Files, path);
        }

        /// <summary>
        /// Checks if the specified path is a valid file database.
        /// </summary>
        /// <param name="path"></param>
        private void CheckPath(string path, bool save)
        {
            //Check if any path is specified
            if (string.IsNullOrWhiteSpace(path))
                throw new ArgumentNullException(nameof(path));

            if (!save && !File.Exists(path))
            {
                //When loading, the file has to exist
                throw new FileNotFoundException("Could not find the specified database file.", path);
            }
            else if (save)
            {
                //When saving, create the directory that the file will be stored in
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            }
        }

        private class AutoSaveDatabaseSerializer : IDisposable
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="AutoSaveDatabaseSerializer"/> class.
            /// </summary>
            public AutoSaveDatabaseSerializer()
            {

            }

            private BinaryReader _reader;
            private BinaryWriter _writer;

            private bool _disposed;

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected void Dispose(bool disposing)
            {
                if (!_disposed)
                {
                    if (disposing)
                    {
                        //Managed resources
                        _reader.Dispose();
                        _writer.Dispose();
                    }
                    _disposed = true;
                }
            }

            ~AutoSaveDatabaseSerializer()
            {
                Dispose(false);
            }

            /// <summary>
            /// Gets or sets SFM's base path, as it will be serialized to the data file. Will be updated on Deserialize.
            /// </summary>
            public string BasePath { get; set; }

            /// <summary>
            /// The static header for the file format: ToolKit AutoSave.
            /// </summary>
            public const string AutoSaveHeader = "TKAS";

            /// <summary>
            /// The static footer for the file format. Show's over.
            /// </summary>
            public const string AutoSaveFooter = "OVER";

            /// <summary>
            /// The version of the AutoSaveDatabase serialized data.
            /// </summary>
            public enum AutosaveVersion : byte
            {
                /// <summary>
                /// An unknown version of the AutoSaveDatabase. Most likely an invalid format.
                /// </summary>
                Unknown = 0,
                /// <summary>
                /// Latest version.
                /// </summary>
                Latest = 1

                //When updating the version, be sure to change the "Latest" number,
                //  create a new entry for the old version and write down what it *can't do* compared to the latest!
            }

            /// <summary>
            /// Serializes the specified files, writing the result to the specified stream.
            /// </summary>
            /// <param name="content">Root elements in the content list. Only files with a null Parent are included.</param>
            /// <param name="stream">The stream to write data on.</param>
            public void Serialize(IList<AutoSavedContent> content, Stream stream)
            {
                //Check writability
                if (!stream.CanWrite)
                    throw new ArgumentException("Cannot write to specified stream for serializing.", nameof(stream));

                if (string.IsNullOrWhiteSpace(this.BasePath))
                    throw new InvalidDataException("SFM base path is not specified for serializing the autosave data.");

                //Initialize writer
                this._writer = new BinaryWriter(stream);

                //Header (4 bytes)
                this._writer.Write(AutoSaveHeader.ToCharArray());

                //Version (1 byte)
                this._writer.Write((byte)AutosaveVersion.Latest);

                //SFM Base Path (Length-Prefixed)
                this._writer.Write(this.BasePath);

                //File Count (4 bytes)
                this._writer.Write(content.Count);

                //File List
                foreach (var file in content)
                {
                    //Recursive writing
                    this.WriteFile(file);
                }

                //Footer (4 bytes)
                this._writer.Write(AutoSaveFooter.ToCharArray());
            }

            private void WriteFile(AutoSavedContent file)
            {
                //File guid (16 bytes)
                this._writer.Write(file.Id.ToByteArray());

                //File path (Length-Prefixed)
                this._writer.Write(file.FilePath);
            }

            /// <summary>
            /// Serializes the specified files, returning the resulting data to serialize.
            /// </summary>
            /// <param name="files">Files to serialize.</param>
            public byte[] Serialize(IList<AutoSavedContent> files)
            {
                var stream = new MemoryStream();
                this.Serialize(files, stream);
                return stream.GetBuffer();
            }

            /// <summary>
            /// Serializes the specified files, writing the result to the specified file.
            /// </summary>
            /// <param name="files">Files to serialize.</param>
            /// <param name="path">The path of the file to write to.</param>
            public void Serialize(IList<AutoSavedContent> files, string path)
            {
                using (var file = File.OpenWrite(path))
                {
                    this.Serialize(files, file);
                }
            }

            /// <summary>
            /// Deserializes the database data from the specified stream, returning the stored files.
            /// </summary>
            /// <param name="stream">The stream to read the serialized data from.</param>
            public IEnumerable<AutoSavedContent> Deserialize(Stream stream)
            {
                //Check readability
                if (!stream.CanRead)
                    throw new ArgumentException("Cannot read from specified stream for deserializing.", nameof(stream));

                //Initialize reader
                this._reader = new BinaryReader(stream);

                //Header (4 bytes)
                var header = this._reader.ReadChars(4);
                if (!header.SequenceEqual(AutoSaveHeader.ToCharArray()))
                    throw new FormatException("The header of the database file does not match: " + new string(header));

                //Version (1 byte)
                var vernum = this._reader.ReadByte();
                if (!Enum.IsDefined(typeof(AutosaveVersion), vernum))
                    throw new FormatException("Unknown version in database file: " + vernum);
                var version = (AutosaveVersion)vernum;

                //SFM Base Path (Length-Prefixed)
                this.BasePath = this._reader.ReadString();

                //File Count (4 bytes)
                var filecnt = this._reader.ReadInt32();

                //File List
                for (var i = 0; i < filecnt; i++)
                {
                    var file = this.ReadFile();
                    yield return file;
                }

                //Footer (4 bytes)
                var footer = this._reader.ReadChars(4);
                if (!footer.SequenceEqual(AutoSaveFooter.ToCharArray()))
                    throw new FormatException("The footer of the database file does not match: " + new string(footer));
            }

            private AutoSavedContent ReadFile()
            {
                var asc = new AutoSavedContent();

                //File guid (16 bytes)
                asc.Id = new Guid(this._reader.ReadBytes(16));

                //File path (Length-Prefixed)
                asc.FilePath = this._reader.ReadString();

                return asc;
            }

            /// <summary>
            /// Deserializes the specified autosave data, returning the stored files.
            /// </summary>
            /// <param name="data">The byte array with the autosave data.</param>
            public IEnumerable<AutoSavedContent> Deserialize(byte[] data)
            {
                foreach (var file in this.Deserialize(new MemoryStream(data)))
                    yield return file;
            }

            /// <summary>
            /// Deserializes the autosave data from the specified file, returning the stored files.
            /// </summary>
            /// <param name="path">The file to read the serialized data from.</param>
            public IEnumerable<AutoSavedContent> Deserialize(string path)
            {
                using (var file = File.OpenRead(path))
                {
                    foreach (var asc in this.Deserialize(file))
                        yield return asc;
                }
            }
        }
    }
}
