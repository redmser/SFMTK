﻿namespace OrderCLParser
{
    /// <summary>
    /// A value which was implied to be at the end of the command line.
    /// </summary>
    public class CLImpliedValue : CLValue
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CLImpliedValue" /> class.
        /// </summary>
        /// <param name="value">The value string that was specified.</param>
        public CLImpliedValue(string value) : base(value, null, null, true)
        {
        }
    }
}
