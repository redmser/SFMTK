﻿namespace OrderCLParser
{
    /// <summary>
    /// A flag with a value, basically. Including a default value makes that option always exist in the resulting list, ordered according to token list.
    /// </summary>
    public class CLOption : CLFlag
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CLOption" /> class.
        /// </summary>
        /// <param name="shortname">The shortname of the option. Can be batched like -abc</param>
        /// <param name="name">The longname of the option. Called like --name</param>
        /// <param name="positional">If set to <c>true</c>, position of this option matters (not just a regular option, but a command).</param>
        /// <param name="defaultValue">The default value of this option, in case it is not specified.</param>
        /// <param name="description">The help text to display for this option.</param>
        /// <param name="optional">If set to <c>true</c>, this option is optional.</param>
        public CLOption(char shortname, string name, bool positional, string defaultValue = null, string description = null, bool optional = true)
            : base(shortname, name, positional, description, optional)
        {
            this.Value = defaultValue;
        }

        /// <summary>
        /// Gets or sets the current value of this option.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"{{ {GetType().Name} {ShortName}/{Name} = {Value} ({Description}) }}";
    }
}
