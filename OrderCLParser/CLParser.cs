﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrderCLParser
{
    /// <summary>
    /// Parses command line arguments.
    /// </summary>
    public class CLParser
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CLParser"/> class.
        /// </summary>
        /// <param name="tokens">The tokens.</param>
        public CLParser(params CLToken[] tokens)
        {
            this.Tokens.AddRange(tokens);
        }

        /// <summary>
        /// Gets the list of tokens to populate for parsing. Order is important for CLValues.
        /// </summary>
        public List<CLToken> Tokens { get; } = new List<CLToken>();

        /// <summary>
        /// Loads or reloads the <see cref="CLParser"/> to its default state.
        /// </summary>
        public void Initialize()
        {
            doubleMinus = false;
            mode = ParserMode.ExpectToken;
            valueIndex = 0;
            sb.Clear();
            expectsValue = null;
            inQuotes = false;
        }

        /// <summary>
        /// Parses the specified string array as command-line arguments, where each new entry is separated by a space.
        /// </summary>
        /// <param name="args">The arguments to parse.</param>
        public IEnumerable<CLToken> Parse(IEnumerable<string> args) => this.Parse(string.Join(" ", args));

        /// <summary>
        /// Parses the specified string as command-line arguments.
        /// </summary>
        /// <param name="args">The arguments to parse.</param>
        public IEnumerable<CLToken> Parse(string args)
        {
            //Reset values
            this.Initialize();

            //TODO: CLParser - convert stringbuilder to raw char array (see KVtokenizer for reference)

            //Add a space to toss out whatever is being parsed (ideally)
            //This doesn't really work if inQuotes is true at the end (which it shouldn't be if the person writing the command line knows what he's doing)
            args += " ";

            var tokens = new List<CLToken>();
            for (var i = 0; i < args.Length; i++)
            {
                var ch = args[i];

                switch (this.mode)
                {
                    case ParserMode.ExpectToken:
                        if (ch == '-' && !doubleMinus)
                        {
                            //Special character, peek what comes next
                            var peek = args[i + 1];
                            if (peek == '-')
                            {
                                //Double-minus! Is this the end of the option list?
                                peek = args[i + 2];
                                if (peek == ' ')
                                {
                                    //End of options, any minus is now seen as a value instead
                                    //UNTESTED: CLParser - double minus mode
                                    doubleMinus = true;
                                    expectsValue = null;
                                    i += 2;
                                }
                                else
                                {
                                    //Expect a long-name now
                                    mode = ParserMode.ExpectLongName;
                                    i++;
                                }
                            }
                            else
                            {
                                //Expect one or multiple short-names now
                                mode = ParserMode.ExpectShortName;
                            }
                        }
                        else
                        {
                            //Spotted a value in the wild, retake that char
                            mode = ParserMode.ExpectEndOfValue;
                            i--;
                        }
                        break;
                    case ParserMode.ExpectShortName:
                        if (ch == ' ')
                        {
                            //Show's over, sorry lads, no more short names
                            mode = ParserMode.ExpectToken;
                            continue;
                        }

                        //Check which CLFlag (or CLOption) has this shortname
                        var flag = this.Tokens.OfType<CLFlag>().SingleOrDefault(f => f.ShortName == ch);
                        if (flag == null)
                        {
                            //This flag does not exist!
                            tokens.Add(new CLUnknownFlag(ch, null));
                        }
                        else
                        {
                            //Did we find a flag or an option?
                            if (flag is CLOption option)
                            {
                                //Damn, we will need a value
                                //TODO: CLParser - warn if no space or equalsign comes after this option (meaning that they don't know a value is needed!)
                                i++;
                                mode = ParserMode.ExpectEndOfValue;
                                expectsValue = option;
                            }
                            else
                            {
                                //We're done, just add it and move along
                                tokens.Add(flag);
                            }
                        }
                        break;
                    case ParserMode.ExpectLongName:
                        //Let's just read everything you give me until you give me a space or equals or something
                        if (ch == ' ' || ch == '=')
                        {
                            //End of name, does that flag/option even exist?
                            var flag2 = this.Tokens.OfType<CLFlag>().SingleOrDefault(f => f.Name == sb.ToString());
                            sb.Clear();
                            if (flag2 == null)
                            {
                                //NOPE! Just heck it
                                tokens.Add(new CLUnknownFlag('\0', sb.ToString()));
                                mode = ParserMode.ExpectToken;
                            }
                            else
                            {
                                //YUP! Check if there can even be a value next
                                if (flag2 is CLOption option)
                                {
                                    //Definitely, ask for it
                                    mode = ParserMode.ExpectEndOfValue;
                                    expectsValue = option;
                                }
                                else
                                {
                                    //Nope, just go for more tokens now
                                    tokens.Add(flag2);
                                    mode = ParserMode.ExpectToken;
                                }
                            }
                        }
                        else
                        {
                            //Build up our buffer
                            sb.Append(ch);
                        }
                        break;
                    case ParserMode.ExpectEndOfValue:
                        if (ch == '"')
                        {
                            //Unescaped quote, toggle quotes mode
                            inQuotes = !inQuotes;
                        }
                        else if (ch == '\\')
                        {
                            //Escaping character, see what it's for
                            var peek = args[i + 1];
                            if (peek == '"')
                            {
                                //Escaped a quote, simply add to buffer
                                sb.Append('"');
                                i++;
                            }
                            else if (peek == ' ')
                            {
                                //Escaped a space, simply add to buffer
                                sb.Append(' ');
                                i++;
                            }
                            else
                            {
                                //Add escaping char as it is, since we don't know what it does
                                sb.Append('\\');
                            }
                        }
                        else if (ch == ' ')
                        {
                            //See if we are in quotes
                            if (inQuotes)
                            {
                                //Add to buffer
                                sb.Append(' ');
                            }
                            else
                            {
                                //End of value is found! Check if we are parsing for an option or for a value
                                if (expectsValue == null)
                                {
                                    //Associate with corresponding value in token list.
                                    var value = this.Tokens.OfType<CLValue>().ElementAtOrDefault(valueIndex);
                                    if (value == null)
                                    {
                                        //No value was expected! Return as a list value
                                        tokens.Add(new CLImpliedValue(sb.ToString()));
                                    }
                                    else
                                    {
                                        //Set value of value (hehe)
                                        value.Value = sb.ToString();
                                        tokens.Add(value);
                                    }
                                    valueIndex++;
                                }
                                else
                                {
                                    //Let's give our option the value and return it
                                    expectsValue.Value = sb.ToString();
                                    tokens.Add(expectsValue);
                                    expectsValue = null;
                                }
                                sb.Clear();
                                mode = ParserMode.ExpectToken;
                            }
                        }
                        else
                        {
                            //Some char, let's add it
                            sb.Append(ch);
                        }
                        break;
                }
            }

            //NYI: CLParser - reinforce the "Optional" property

            //Find the default value tokens that are not yet specified, toss those in as well
            foreach (var tok in this.Tokens.OfType<CLOption>().Where(t => t.Value != null && !tokens.Contains(t)))
                tokens.Add(tok);

            return tokens;
        }

        /// <summary>
        /// Whether -- was specified, telling the parser to interpret anything afterwards as values only.
        /// </summary>
        protected bool doubleMinus;

        /// <summary>
        /// Whether the parser is inside of quotes. Changes how whitespace is handled. Only possible for values.
        /// </summary>
        protected bool inQuotes;

        /// <summary>
        /// Which option, if any, expects the currently parsed value to be written back to.
        /// </summary>
        protected CLOption expectsValue;

        /// <summary>
        /// Current parser mode.
        /// </summary>
        protected ParserMode mode;

        /// <summary>
        /// Buffer for building names during parsing.
        /// </summary>
        protected StringBuilder sb = new StringBuilder();

        /// <summary>
        /// Current index of values.
        /// </summary>
        protected int valueIndex;

        /// <summary>
        /// What the parser is currently doing or expecting.
        /// </summary>
        protected enum ParserMode
        {
            /// <summary>
            /// Wait for a new token. No left overs, could be any new token.
            /// </summary>
            ExpectToken,
            /// <summary>
            /// Expect a long name after -- with no trailing space.
            /// </summary>
            ExpectLongName,
            /// <summary>
            /// Expect a list of short names after a -.
            /// </summary>
            ExpectShortName,
            /// <summary>
            /// Expect the end of the value next, currently parsing a value (either CLValue, or the Value property of a CLOption -> expectsValue is set).
            /// </summary>
            ExpectEndOfValue
        }
    }
}
