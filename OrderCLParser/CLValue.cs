﻿namespace OrderCLParser
{
    /// <summary>
    /// A value in the command line. Does not need a flag before it, as is determined by location. Index corresponds with order in the token list.
    /// </summary>
    public class CLValue : CLToken
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CLValue"/> class.
        /// </summary>
        /// <param name="name">The longname of the value. Used for display in the help.</param>
        /// <param name="defaultValue">The default value of this value, in case it is not specified.</param>
        /// <param name="description">The help text to display for this value.</param>
        /// <param name="optional">If set to <c>true</c>, this value is optional.</param>
        public CLValue(string defaultValue, string name = null, string description = null, bool optional = true) : base(name, true, description, optional)
        {
            this.Value = defaultValue;
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"{{ {GetType().Name} {Name} = {Value} ({Description}) }}";
    }
}
