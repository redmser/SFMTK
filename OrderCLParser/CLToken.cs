﻿namespace OrderCLParser
{
    /// <summary>
    /// Base class for all command line tokens.
    /// </summary>
    public abstract class CLToken
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CLToken"/> class.
        /// </summary>
        /// <param name="name">The name of the token. Used for identification.</param>
        /// <param name="positional">If set to <c>true</c>, position of this token matters (not just a regular option, but a command).</param>
        /// <param name="description">The help text to display for this token.</param>
        /// <param name="optional">If set to <c>true</c>, this token is optional.</param>
        protected CLToken(string name = null, bool positional = true, string description = null, bool optional = true)
        {
            this.Name = name;
            this.Description = description;
            this.Optional = optional;
            this.Positional = positional;
        }

        /// <summary>
        /// Gets or sets the long name of this token. Used for identification or for calling with --name.
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Gets or sets the help text to display for this token.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets whether this token is optional.
        /// </summary>
        public bool Optional { get; set; }

        /// <summary>
        /// Gets or sets whether the position of this token matters (not just a regular option, but a command).
        /// </summary>
        public bool Positional { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"{{ {GetType().Name} {Name} ({Description}) }}";
    }
}
