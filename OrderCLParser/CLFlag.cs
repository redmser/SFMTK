﻿namespace OrderCLParser
{
    /// <summary>
    /// A flag is similar to a CLOption, except that it does not have a value. Its existence is enough.
    /// </summary>
    public class CLFlag : CLToken
    {
        //TODO: CLFlag - allow flags to have a default value as well (true or false, null to not have any)

        /// <summary>
        /// Initializes a new instance of the <see cref="CLFlag"/> class.
        /// </summary>
        /// <param name="shortname">The shortname of the flag. Can be batched like -abc</param>
        /// <param name="name">The longname of the flag. Called like --name</param>
        /// <param name="positional">If set to <c>true</c>, position of this flag matters (not just a regular option, but a command).</param>
        /// <param name="description">The help text to display for this flag.</param>
        /// <param name="optional">If set to <c>true</c>, this flag is optional.</param>
        public CLFlag(char shortname, string name, bool positional, string description = null, bool optional = true) : base(name, positional, description, optional)
        {
            this.ShortName = shortname;
        }

        /// <summary>
        /// Gets or sets the shortname of the flag. Can be batched like -abc
        /// </summary>
        public char ShortName { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"{{ {GetType().Name} {ShortName}/{Name} ({Description}) }}";
    }
}
