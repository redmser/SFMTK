﻿namespace OrderCLParser
{
    /// <summary>
    /// A flag which is not specified in the list of tokens.
    /// </summary>
    public class CLUnknownFlag : CLFlag
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CLUnknownFlag"/> class.
        /// </summary>
        /// <param name="shortname">The shortname of the flag. Can be batched like -abc</param>
        /// <param name="name">The longname of the flag. Called like --name</param>
        public CLUnknownFlag(char shortname, string name) : base(shortname, name, false, null, true)
        {
        }
    }
}
