﻿namespace SFMTK.Modules.Paid
{
    /// <summary>
    /// The core module for all paid features of SFMTK. When this module is loaded, allow other paid modules to load as well.
    /// </summary>
    public class PaidModule : IModule
    {
        public ModuleLoadingContext GetLoadingContext() => ModuleLoadingContext.StartupCommandLine;

        public void OnStartup(ModuleLoadingContext context)
        {
        }

        public bool OnLoad(ModuleLoadingContext context)
        {
            Program.SetFullVersion(true);
            return true;
        }

        public bool OnUnload(ModuleUnloadingContext context)
        {
            Program.SetFullVersion(false);
            return true;
        }

        public string[] GetDependencies() => null;

        public string GetParent() => null;
    }
}
