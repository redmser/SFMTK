﻿namespace SFMTK.Modules.Paid.Wizards
{
    /// <summary>
    /// Wizards allow the user to create and interact with multi-step dialogs that normally guide you through a more complicated procedure.
    /// <para/>This module is only available for the paid release of SFMTK.
    /// </summary>
    public class WizardsModule : IModule
    {
        /// <summary>
        /// Gets the loading context of the module, determining on which context the OnLoad method should be called.
        /// </summary>
        public ModuleLoadingContext GetLoadingContext() => ModuleLoadingContext.StartupCommandLine;

        /// <summary>
        /// Called when the module is told to load, either on startup
        /// (only called once, with the context specified on <see cref="M:SFMTK.Modules.IModule.GetLoadingContext" />) or when manually loaded (can be any context).
        /// <para />Returns whether the module has been loaded successfully and is now enabled.
        /// </summary>
        public bool OnLoad(ModuleLoadingContext context) => true;

        /// <summary>
        /// Called on every step of the startup procedure (entries in <see cref="T:SFMTK.Modules.ModuleLoadingContext" /> prefixed with <c>Startup</c>).
        /// <para />If the module is late-loaded (such as enabling in the settings), all steps of the startup procedure are called directly after one another.
        /// </summary>
        public void OnStartup(ModuleLoadingContext context) { }

        /// <summary>
        /// Called when the module is unloaded, after having been initialized.
        /// <para />Returns whether the module has been unloaded successfully and is now disabled.
        /// </summary>
        public bool OnUnload(ModuleUnloadingContext context) => true;

        public string[] GetDependencies() => new[] { Constants.PaidSFMTKModule };
        public string GetParent() => Constants.PaidSFMTKModule;
    }
}
