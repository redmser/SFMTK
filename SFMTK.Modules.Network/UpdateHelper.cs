﻿using SFMTK.Modules.Network.Notifications;
using SFMTK.Modules.Network.Properties;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SFMTK.Modules.Network
{
    internal static class UpdateHelper
    {
        /// <summary>
        /// Checks whether to start an update check due to auto-updating (depending on the interval - and whether auto-updating is enabled).
        /// </summary>
        public static Task CheckForAutoUpdates()
        {
            if (!Settings.Default.AutoUpdate)
                return null;

            TimeSpan timediff;
            switch (Settings.Default.UpdateCheck)
            {
                case UpdateCheckFrequency.OnStartup:
                    return CheckForUpdates(UpdateInitiationMode.Automatic);
                case UpdateCheckFrequency.Daily:
                    timediff = new TimeSpan(1, 0, 0, 0);
                    break;
                case UpdateCheckFrequency.Weekly:
                    timediff = new TimeSpan(7, 0, 0, 0);
                    break;
                case UpdateCheckFrequency.Monthly:
                    timediff = new TimeSpan(30, 0, 0, 0);
                    break;
                default: //Invalid check interval
                    throw new ArgumentException("Auto-update check uses unknown checking interval: " + (int)Settings.Default.UpdateCheck, nameof(timediff));
            }

            //Check if last update was that long ago
            var last = Settings.Default.CachedUpdateInfo.LastChecked;
            if (last != DateTime.MinValue && DateTime.Now - last >= timediff)
            {
                //Do update check!
                return CheckForUpdates(UpdateInitiationMode.Automatic);
            }
            return null;
        }

        /// <summary>
        /// Starts an update check, keeping track of any progress in the status bar.
        /// </summary>
        /// <param name="mode">How this update check was initialized.</param>
        public static async Task CheckForUpdates(UpdateInitiationMode mode)
        {
            //Try waiting for mainform to start existing
            if (!System.Threading.SpinWait.SpinUntil(() => NetworkModule.MainForm != null, 10000))
                return;

            NLog.LogManager.GetCurrentClassLogger().Debug("Checking for updates... {0}", mode.ToString());

            var updatechecklabel = new SFMTK.Controls.StatusStripEntry(Resources.UpdateCheckStatusLabel, 10, StatusStripState.Loading);
            NetworkModule.MainForm.mainStatusStrip.Entries.Add(updatechecklabel);

            //Wait for update check
            try
            {
                var task = Tasks.BackgroundTask.FromDelegate(() => UpdateInfo.DoUpdateCheck(mode), null);
                await NetworkModule.MainForm.BackgroundTasks.AddTask(task);

                //Successful update get - notify user based on which option they were looking for
                if (Settings.Default.CachedUpdateInfo.UpdateAvailable)
                    UpdateAvailable(false, mode);
                else
                    UpdateAvailable(true, mode);
            }
            catch (Exception ex)
            {
                //Error occurred :(
                if (mode == UpdateInitiationMode.Manual)
                {
                    //Feedback is expected after a manual check
                    UpdateInfo.ShowErrorMessage(ex);
                }
                else
                {
                    //Check if should not notify about this
                    var rem = SFMTK.Properties.Settings.Default.RememberDialogSettings.FirstOrDefault(s => s.Identifier == "Update Check Error");
                    if (rem != null)
                        return;

                    //Add a new update error notification to the status bar
                    NetworkModule.MainForm.AddNotification(new UpdateCheckErrorNotification(ex));
                }
            }
            finally
            {
                NetworkModule.MainForm.mainStatusStrip.Entries.Remove(updatechecklabel);
            }
        }

        /// <summary>
        /// Shows the update available notifications which the user set up in the Settings.
        /// </summary>
        /// <param name="none">If set to <c>true</c>, means that no update is available.</param>
        /// <param name="mode">The UpdateInitiationMode that was used for this update check.</param>
        public static void UpdateAvailable(bool none, UpdateInitiationMode mode)
        {
            switch (Settings.Default.UpdateAvailable)
            {
                case UpdateAvailableMode.ShowStatusNotification:
                    if (none)
                    {
                        //A simple context status message should be enough
                        NetworkModule.MainForm.mainStatusStrip.Entries.Add(Resources.NoUpdatesStatusLabel, 15, StatusStripState.Default, 5000);
                    }
                    else
                    {
                        //Update available status bar notification
                        NetworkModule.MainForm.AddNotification(new NewUpdateFoundNotification());
                    }
                    break;
                case UpdateAvailableMode.ShowDialogBox:
                    if (none)
                    {
                        if (mode != UpdateInitiationMode.Manual)
                        {
                            //Don't bother showing no update dialogbox if it's not a manual update check, since that's an expected response
                            NetworkModule.MainForm.mainStatusStrip.Entries.Add(Resources.NoUpdatesStatusLabel, 3, StatusStripState.Default, 3000);
                        }
                        else
                        {
                            //UNTESTED: Simple dialog box for no update
                            FallbackTaskDialog.ShowDialog("You are currently running the latest version of SFMTK!", "Already up-to-date!", "Up-to-date",
                                FallbackDialogIcon.Info, new FallbackDialogButton(DialogResult.OK));
                        }
                    }
                    else
                    {
                        UpdateInfo.UpdateAvailableDialog();
                    }
                    break;
            }
        }
    }
}
