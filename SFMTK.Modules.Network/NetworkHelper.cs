﻿using SFMTK.Modules.Network.Properties;
using SFMTK.Modules.Network.Widgets;
using System;
using System.Diagnostics;
using System.Net;

namespace SFMTK.Modules.Network
{
    /// <summary>
    /// Helper class for any network and update calls.
    /// </summary>
    public static class NetworkHelper
    {
        /// <summary>
        /// Gets the proxy to use for any network calls, as set by the settings.
        /// </summary>
        public static IWebProxy GetProxy()
        {
            if (Properties.Settings.Default.UseProxy)
                return WebRequest.GetSystemWebProxy();
            return WebRequest.DefaultWebProxy;
        }

        /// <summary>
        /// Opens a web browser for displaying the specified URL. Whether the external or internal browser is used depends on the settings key <c>ExternalBrowser</c>.
        /// <para />Ensure in advance whether the URL is valid, and doesn't try to do weird stuff!
        /// </summary>
        /// <param name="url">The URL to open.</param>
        public static void ShowWebsite(string url)
        {
            if (Settings.Default.ExternalBrowser)
            {
                NetworkHelper.OpenInBrowser(url, true);
            }
            else
            {
                //Browser widget
                if (NetworkModule.MainForm.mainDockPanel.ActiveDocument is WebBrowserWidget wb)
                {
                    //TODO: Main - only re-use a browser widget if the user did not navigate away from the initial page yet!

                    //Navigate to new page
                    wb.TryNavigate(url);

                    //Give focus to browser
                    NetworkModule.MainForm.DockPanelManager.Show(wb);
                }
                else
                {
                    //Add new browser
                    NetworkModule.MainForm.DockPanelManager.Add(new WebBrowserWidget(url));
                }
            }
        }

        /// <summary>
        /// Opens the specified URL in a new external browser window.
        /// </summary>
        /// <param name="url">The URL to open the browser with.</param>
        /// <param name="suggestChange">If set to <c>true</c>, the error message may suggest changing to the internal browser.</param>
        public static void OpenInBrowser(string url, bool suggestChange = false)
        {
            try
            {
                //Open URL in default browser
                //A check for the process being null would be counterproductive, as any shell execute (such as the browser opening) will also make it return null
                Process.Start(url);
                //UNTESTED: Main - exception launching external browser
            }
            catch (Exception ex)
            {
                FallbackTaskDialog.ShowDialogLog("The requested web page could not be opened using your default browser." +
                    (suggestChange ? "\nIf this error continues to happen, you can try changing your settings so that the internal browser is used instead." : string.Empty),
                    "Oops!", "Unable to open page", "Here's the error that occurred: " + ex.Message,
                    FallbackDialogIcon.Error, new FallbackDialogButton(System.Windows.Forms.DialogResult.OK));
            }
        }
    }
}
