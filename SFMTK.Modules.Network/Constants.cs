﻿namespace SFMTK.Modules.Network
{
    /// <summary>
    /// Constants for network usage.
    /// </summary>
    public static class NetworkConstants
    {
        /// <summary>
        /// Base URL to the Valve Developer Community.
        /// </summary>
        public const string VDCBase = "https://developer.valvesoftware.com";

        /// <summary>
        /// A URL for the main Valve Developer Community page.
        /// </summary>
        public const string VDCMainURL = VDCBase + "/wiki/Main_Page";

        /// <summary>
        /// A URL for searching the Valve Developer Community page. Append your search query (with correctly formatted characters).
        /// </summary>
        public const string VDCSearchURL = VDCBase + "/wiki/Special:Search?search=";

        /// <summary>
        /// A URL for search suggestions of the Valve Developer Community. Append your search query (with correctly formatted characters).
        /// </summary>
        public const string VDCSuggestURL = VDCBase + "/w/api.php?action=opensearch&search=";

        /// <summary>
        /// A URL for getting a fav icon file from a specified appended URL.
        /// </summary>
        public const string GetFavIconURL = "http://www.google.com/s2/favicons?domain=";

        /// <summary>
        /// The gitlab project ID of SFMTK. Used for referring to the project with the API.
        /// </summary>
        public const int GitLabId = 3483903;
    }
}
