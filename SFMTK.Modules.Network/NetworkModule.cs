﻿using System.Collections.Generic;
using OrderCLParser;
using System.Net;
using SFMTK.Controls.PreferencePages;
using SFMTK.Modules.Network.Properties;
using System.ComponentModel;
using System.Windows.Forms;
using SFMTK.Modules.Network.Controls;
using System.Threading.Tasks;

namespace SFMTK.Modules.Network
{
    /// <summary>
    /// Enables all network actions inside of SFMTK, including updating the software.
    /// </summary>
    public class NetworkModule : IModule, IModuleTokens, IModuleSettings, IModuleToolBars
    {
        //TODO: Modules.Network - updating system
        //  -> display changes as a cumulative change log (show ALL changes from current to latest version)
        //  -> group changes into their individual groups/categories, and maybe show version on mouseover (needs a naming system to make sense)
        //  -> automatically handle replacing/restarting (if user accepts, of course!)
        //  -> compatibility with the module system (allow modules to also check for updates, perhaps move the gitversioning approach over to
        //     something self-hosted, like a JSON update database file? what about external modules, can they specify their own?

        /// <summary>
        /// Gets the WebClient for search suggestions.
        /// </summary>
        internal static WebClient SearchSuggestionClient { get; private set; }

        /// <summary>
        /// Gets or sets whether the <see cref="SearchSuggestionClient"/> is busy.
        /// </summary>
        //BUG: NetworkModule - SearchSuggestionClientBusy does not do the job of detecting concurrent webclient operations
        internal static bool SearchSuggestionClientBusy { get; set; }

        /// <summary>
        /// Gets the WebClient for retrieving the favicon.
        /// </summary>
        internal static WebClient FavIconClient { get; private set; }

        /// <summary>
        /// Lock object for <see cref="FavIconClient"/>.
        /// </summary>
        internal static object FavIconLock = new object();

        /// <summary>
        /// Gets the main form that this module will be applied on.
        /// </summary>
        public static SFMTK.Forms.Main MainForm { get; private set; }

        /// <summary>
        /// Gets the loading context of the module, determining on which context the OnLoad method should be called.
        /// </summary>
        public ModuleLoadingContext GetLoadingContext() => ModuleLoadingContext.StartupCommandLine;

        /// <summary>
        /// Called on every step of the startup procedure (entries in <see cref="T:SFMTK.Modules.ModuleLoadingContext" /> prefixed with <c>Startup</c>).
        /// <para />If the module is late-loaded (such as enabling in the settings), all steps of the startup procedure are called directly after one another.
        /// </summary>
        /// <param name="context"></param>
        public void OnStartup(ModuleLoadingContext context)
        {
            if (context == ModuleLoadingContext.StartupUserInterface)
            {
                //UI additions
                MainForm = Program.MainForm;

                MainForm.InvokeIfRequired(() =>
                {
                    //Update Check
                    MainForm.GetMenu("Help").DropDownItems.Insert(6,
                        Program.MainCM.CreateToolStripMenuItem(new Commands.CheckForUpdatesCommand()));

                    //Browser
                    MainForm.GetMenu("View").DropDownItems.Insert(5,
                        Program.MainCM.CreateToolStripMenuItem(SFMTK.Commands.CommandManager.GetWidgetCommand<Widgets.WebBrowserWidget>()));

                    //Check for first startup
                    SFMTK.Forms.FirstLaunch.FirstStartup += (s, e) => UpdateHelper.CheckForUpdates(UpdateInitiationMode.Initial);
                });

                //Update registry: IE compatibility mode
                const int ForcedIE8Mode = 8888;
                Microsoft.Win32.Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BROWSER_EMULATION",
                    System.AppDomain.CurrentDomain.FriendlyName, ForcedIE8Mode, Microsoft.Win32.RegistryValueKind.DWord);

#if DEBUG
                //Do so for the vshost exe as well
                Microsoft.Win32.Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BROWSER_EMULATION",
                    System.AppDomain.CurrentDomain.FriendlyName.Replace(".exe", ".vshost.exe"), ForcedIE8Mode, Microsoft.Win32.RegistryValueKind.DWord);
#endif
            }
            else if (context == ModuleLoadingContext.StartupLazyLoaded)
            {
                //Lazy-loaded
                var inittask = Tasks.BackgroundTask.FromDelegate(() => this.InitializeWebClients(), "Initializing network connection...", null, true);
                inittask.TaskShouldCancel += (s, e) => SearchSuggestionClient.CancelAsync();
                MainForm.BackgroundTasks.AddTask(inittask);

                if (!SFMTK.Properties.Settings.Default.FirstStartup)
                {
                    //Update check if conditions are set
                    var autoupdate = UpdateHelper.CheckForAutoUpdates();
                    if (autoupdate != null)
                    {
                        //NYI: NetworkModule - Do autoupdate as background task too
                        System.Threading.Tasks.Task.Run(() => autoupdate);
                        //MainForm.BackgroundTasks.AddTask(Tasks.BackgroundTask.FromTask(autoupdate));
                    }
                }
            }
        }

        /// <summary>
        /// Called when the module is told to load, either on startup
        /// (only called once, with the context specified on <see cref="M:SFMTK.Modules.IModule.GetLoadingContext" />) or when manually loaded (can be any context).
        /// <para />Returns whether the module has been loaded successfully and is now enabled.
        /// </summary>
        public bool OnLoad(ModuleLoadingContext context)
        {
            //Default values
            ServicePointManager.Expect100Continue = false;

            return true;
        }

        /// <summary>
        /// Called when the module is unloaded, after having been initialized.
        /// <para />Returns whether the module has been unloaded successfully and is now disabled.
        /// </summary>
        /// <param name="context"></param>
        public bool OnUnload(ModuleUnloadingContext context)
        {
            //Save settings
            Settings.Default.Save();

            //Free resources
            if (SearchSuggestionClient != null)
                SearchSuggestionClient.Dispose();
            if (FavIconClient != null)
                FavIconClient.Dispose();
            return true;
        }

        /// <summary>
        /// Initializes a test connection on a WebClient, in order to check for internet connectivity and to speed up further WebClient calls.
        /// </summary>
        private void InitializeWebClients()
        {
            //Create the client
            SearchSuggestionClient = new WebClient();
            FavIconClient = new WebClient();

            //Do a test-connection, no matter if it failed or not
            try
            {
                SearchSuggestionClient.Proxy = NetworkHelper.GetProxy();
                SearchSuggestionClientBusy = true;
                SearchSuggestionClient.DownloadString(NetworkConstants.VDCSuggestURL);
            }
            catch
            {
                //May ignore since only initializing!
            }
            SearchSuggestionClientBusy = false;
        }

        /// <summary>
        /// Gets the tokens which will be registered when loading the module. Usually, <see cref="T:OrderCLParser.CLFlag" /> and <see cref="T:OrderCLParser.CLOption" /> are used.
        /// </summary>
        public IEnumerable<CLToken> Tokens { get; } = new CLToken[]
        {
            new CLFlag('\0', "net-expect100", false, "Expect 100-Continue messages from servers"),
            new CLOption('\0', "net-limit", false, "8", "Maximum amount of simultaneous connections\nto internet resources"),
            new CLFlag('\0', "update-check", true, "Starts an update check on startup")
        };

        /// <summary>
        /// Gets the preference pages to add to the settings dialog. Use instances deriving from the <see cref="T:SFMTK.Controls.PreferencePages.PreferencePage" /> class to create new pages.
        /// <para />Be sure to set the unique properties to customize the page's display in the tree view.
        /// </summary>
        public IEnumerable<PreferencePage> GetPages() => new PreferencePage[] { new Controls.PreferencePages.NetworkPreferencePage() };

        /// <summary>
        /// Gets the toolbars to be registered for this module.
        /// </summary>
        public IEnumerable<ToolStrip> GetToolBars() => new ToolStrip[] { new SearchToolStrip() };

        /// <summary>
        /// Gets the additional properties to add to existing preference pages. Key is the path to the PreferencePage, Value is the user control to append.
        /// </summary>
        public Dictionary<string, Control> GetAdditionalProperties() => new Dictionary<string, Control>
        {
            { "Experimental", new AdditionalExperimentalOptions() },
            { "Environment/Dialogs", ExternalBrowserCheckBox() }
        };

        /// <summary>
        /// Gets an instance pointing to the default settings instance of this module.
        /// </summary>
        public INotifyPropertyChanged CustomSettings => Properties.Settings.Default;

        private static UserControl ExternalBrowserCheckBox()
        {
            //HACK: Network.ExternalBrowserCheckBox - container and contents (tooltip) may not be disposed!
            var ctrl = new UserControl();
            ctrl.AutoSize = true;
            var container = new Container();
            var toolTip = new ToolTip(container);

            var externalBrowserCheckBox = new CheckBox();
            externalBrowserCheckBox.AutoSize = true;
            externalBrowserCheckBox.Size = new System.Drawing.Size(100, 20);
            externalBrowserCheckBox.DataBindings.Add(new Binding("Checked", Settings.Default, "ExternalBrowser", true, DataSourceUpdateMode.OnPropertyChanged));
            externalBrowserCheckBox.Name = "externalBrowserCheckBox";
            externalBrowserCheckBox.Text = "Open &websites in external browser";
            toolTip.SetToolTip(externalBrowserCheckBox, "Instead of using the built-in browser for displaying web pages, opens the default" +
        " web browser.\r\nThis setting may be ignored depending on the context.");
            externalBrowserCheckBox.UseVisualStyleBackColor = true;

            ctrl.Controls.Add(externalBrowserCheckBox);
            return ctrl;
        }

        /// <summary>
        /// Called when a positional token defined by this module was found while parsing the command-line arguments during the startup procedure.
        /// </summary>
        /// <param name="token">The token that was found by the parser.</param>
        public void ParseCommandToken(CLToken token)
        {
            switch (token.Name)
            {
                //UNTESTED: --update-check
                case "update-check":
                    NLog.LogManager.GetCurrentClassLogger().Info("Checking for updates...");
                    Task.Run(() => UpdateHelper.CheckForUpdates(UpdateInitiationMode.Initial));
                    break;
            }
        }

        /// <summary>
        /// Called when a non-positional token defined by this module was found while parsing the command-line arguments during the startup procedure.
        /// <para />It is consumed from the list of tokens in the command-line after returning from this method.
        /// </summary>
        /// <param name="token">The token that was found by the parser.</param>
        public void ParseOptionToken(CLToken token)
        {
            var opt = token as CLOption;

            switch (token.Name)
            {
                case "net-expect100":
                    ServicePointManager.Expect100Continue = true;
                    break;
                case "net-limit":
                    ServicePointManager.DefaultConnectionLimit = int.Parse(opt.Value);
                    break;
            }
        }

        public void SetDefaultSettings()
        {
            if (Settings.Default.CachedVDCQueries == null)
                Settings.Default.CachedVDCQueries = new List<string>();

            if (Settings.Default.CachedVDCResults == null)
                Settings.Default.CachedVDCResults = new List<string>();

            if (Settings.Default.CachedUpdateInfo == null)
                Settings.Default.CachedUpdateInfo = new UpdateInfo();

            if (string.IsNullOrWhiteSpace(Settings.Default.Homepage))
                Settings.Default.Homepage = NetworkConstants.VDCMainURL;
        }

        public string[] GetDependencies() => null;
        public string GetParent() => null;
    }
}
