﻿using System.Collections.Generic;
using System.Configuration;

namespace SFMTK.Modules.Network.Properties {

    /// <summary>
    /// How updates being available should be communicated to the user.
    /// </summary>
    public enum UpdateAvailableMode
    {
        /// <summary>
        /// Notify user using a notification in the status bar (and menu bar).
        /// </summary>
        ShowStatusNotification = 0,
        /// <summary>
        /// Notify user using a dialog box.
        /// </summary>
        ShowDialogBox = 1,
        /// <summary>
        /// Do not notify user of updates.
        /// </summary>
        DoNothing = 2
    }

    /// <summary>
    /// How frequently to check for updates.
    /// </summary>
    public enum UpdateCheckFrequency
    {
        /// <summary>
        /// Check updates every startup.
        /// </summary>
        OnStartup = 0,
        /// <summary>
        /// Check updates daily.
        /// </summary>
        Daily = 1,
        /// <summary>
        /// Check updates weekly (7 days).
        /// </summary>
        Weekly = 2,
        /// <summary>
        /// Check updates monthly (30 days).
        /// </summary>
        Monthly = 3
    }

    // This class allows you to handle specific events on the settings class:
    //  The SettingChanging event is raised before a setting's value is changed.
    //  The PropertyChanged event is raised after a setting's value is changed.
    //  The SettingsLoaded event is raised after the setting values are loaded.
    //  The SettingsSaving event is raised before the setting values are saved.
    internal sealed partial class Settings {
        
        public Settings() {
            // // To add event handlers for saving and changing settings, uncomment the lines below:
            //
            // this.SettingChanging += this.SettingChangingEventHandler;
            //
            // this.SettingsSaving += this.SettingsSavingEventHandler;
            //
        }
        
        private void SettingChangingEventHandler(object sender, System.Configuration.SettingChangingEventArgs e) {
            // Add code to handle the SettingChangingEvent event here.
        }
        
        private void SettingsSavingEventHandler(object sender, System.ComponentModel.CancelEventArgs e) {
            // Add code to handle the SettingsSaving event here.
        }

        /// <summary>
        /// Gets or sets the cached VDC queries.
        /// </summary>
        /// <value>
        /// The cached VDC queries.
        /// </value>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public List<string> CachedVDCQueries
        {
            get => (List<string>)this["CachedVDCQueries"];
            set => this["CachedVDCQueries"] = value;
        }

        /// <summary>
        /// Gets or sets the cached VDC results.
        /// </summary>
        /// <value>
        /// The cached VDC results.
        /// </value>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public List<string> CachedVDCResults
        {
            get => (List<string>)this["CachedVDCResults"];
            set => this["CachedVDCResults"] = value;
        }

        /// <summary>
        /// Gets or sets the cached update information.
        /// </summary>
        /// <value>
        /// The cached update information.
        /// </value>
        [DefaultSettingValue(null)]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [UserScopedSetting]
        public UpdateInfo CachedUpdateInfo
        {
            get => (UpdateInfo)this["CachedUpdateInfo"];
            set => this["CachedUpdateInfo"] = value;
        }
    }
}
