﻿using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using SFMTK.Controls;
using SFMTK.Widgets;

namespace SFMTK.Modules.Network.Widgets
{
    /// <summary>
    /// An internal web browser.
    /// </summary>
    /// <seealso cref="DockContent" />
    public partial class WebBrowserWidget : Widget
    {
        //TODO: WebBrowserWidget - add a search box next to the address bar (working the same exact way as the one in the help toolbar, except maybe selecting other search providers too!)
        //TODO: WebBrowserWidget - replace text boxes with dropdowns for recent entries (OR use autocomplete suggest mode)
        //TODO: WebBrowserWidget - while loading a page, change "refresh" button to a "stop loading" button

        //Since all controls use this inconsistent naming scheme, ignore this error for non-compiler-generated controls
#pragma warning disable IDE1006 // Naming Styles
        private ToolStripSpringTextBox addressBarToolStripSpringTextBox;
#pragma warning restore IDE1006 // Naming Styles

        /// <summary>
        /// Initializes a new instance of the <see cref="WebBrowserWidget"/> class, opening the default page (homepage).
        /// </summary>
        public WebBrowserWidget() : this(Properties.Settings.Default.Homepage)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WebBrowserWidget" /> class, opening the specified page URL.
        /// </summary>
        /// <param name="page">The page.</param>
        public WebBrowserWidget(string page)
        {
            //Compiler-generated
            InitializeComponent();

            _loadpage = page;
        }

        private string _loadpage;

        protected override void OnLoad(EventArgs e)
        {
            //Add address bar
            this.addressBarToolStripSpringTextBox = new ToolStripSpringTextBox();
            this.addressBarToolStripSpringTextBox.Text = _loadpage;
            this.addressBarToolStripSpringTextBox.KeyUp += this.addressBarToolStripSpringTextBox_KeyUp;
            this.browserControlsToolStrip.Items.Add(this.addressBarToolStripSpringTextBox);

            //Add status text
            Program.MainForm.mainStatusStrip.Entries.Add(this._statusText);
            this._statusText.Text = "Initializing...";

            //Navigate
            this.TryNavigate(this._loadpage);

            //Events
            this.browser.Navigated += this.Browser_Navigated;
            this.browser.DocumentTitleChanged += this.Browser_DocumentTitleChanged;
            this.browser.ProgressChanged += this.Browser_ProgressChanged;
            this.browser.DocumentCompleted += this.Browser_DocumentCompleted;
            this.browser.CanGoBackChanged += this.Browser_CanGoBackChanged;
            this.browser.CanGoForwardChanged += this.Browser_CanGoForwardChanged;

            base.OnLoad(e);
        }

        private void Browser_CanGoBackChanged(object sender, EventArgs e) => this.backToolStripButton.Enabled = this.browser.CanGoBack;
        private void Browser_CanGoForwardChanged(object sender, EventArgs e) => this.forwardToolStripButton.Enabled = this.browser.CanGoForward;

        private void Browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            this._statusText.Text = "";
        }

        private void Browser_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {
            if (e.CurrentProgress >= e.MaximumProgress)
            {
                this._statusText.Text = "";
            }
            else
            {
                this._statusText.Text = $"Loading page... ({new FileSize((ulong)e.CurrentProgress)} of {new FileSize((ulong)e.MaximumProgress)})";
            }
        }

        private void Browser_DocumentTitleChanged(object sender, EventArgs e)
        {
            this.Text = $"Web Browser - {this.browser.DocumentTitle}";
            this.TabText = this.browser.DocumentTitle;
        }

        private async void Browser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            this._statusText.Text = $"Navigating... {e.Url}";
            this.addressBarToolStripSpringTextBox.Text = this.browser.Url.ToString();

            //Get and update favicon
            //BUG: WebBrowserWidget - icon does not update until mouse-overed -> find way to invalidate that area
            var favicon = await Task.Run(() => GetFavIcon(this.browser.Url.ToString()));
            this.Icon = favicon;
        }

        private StatusStripEntry _statusText = new StatusStripEntry(null, -2);

        /// <summary>
        /// Gets the fav icon for the specified URL.
        /// </summary>
        /// <param name="url">URL to get fav icon for.</param>
        /// <exception cref="WebException">The specified URL is invalid. -or- The icon file could not be downloaded.</exception>
        public static Icon GetFavIcon(string url)
        {
            var file = Path.GetTempFileName();
            Icon icon;
            Bitmap bmp;
            lock (NetworkModule.FavIconLock)
            {
                var client = NetworkModule.FavIconClient;
                client.Proxy = NetworkHelper.GetProxy();
                client.DownloadFile(NetworkConstants.GetFavIconURL + HttpUtility.UrlEncode(url), file);
                bmp = new Bitmap(file);
                icon = Icon.FromHandle(bmp.GetHicon());
            }
            bmp.Dispose();
            File.Delete(file);
            return icon;
        }

        private void addressBarToolStripSpringTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //Open URL on enter
            if (e.KeyCode == Keys.Enter)
            {
                var uri = ConvertUrl(this.addressBarToolStripSpringTextBox.Text);
                this.TryNavigate(uri);
            }
        }

        private static Uri ConvertUrl(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
                return null;
            var uri = new Uri(url);
            if (!uri.Scheme.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
                return null;
            return uri;
        }

        public void TryNavigate(Uri uri)
        {
            if (uri == null)
                return;
            this.browser.Navigate(uri);
        }

        public void TryNavigate(string url) => this.TryNavigate(ConvertUrl(url));

        private void refreshToolStripButton_Click(object sender, EventArgs e)
        {
            //Refresh page
            //TODO: WebBrowserWidget - shift-click for full refresh
            this.browser.Refresh();
        }

        private void homepageToolStripButton_Click(object sender, EventArgs e) => this.TryNavigate(Properties.Settings.Default.Homepage);

        private void backToolStripButton_Click(object sender, EventArgs e) => this.browser.GoBack();

        private void forwardToolStripButton_Click(object sender, EventArgs e) => this.browser.GoForward();

        private void WebBrowserWidget_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Clean up
            Program.MainForm.mainStatusStrip.Entries.Remove(this._statusText);
        }
    }
}
