﻿namespace SFMTK.Modules.Network.Controls.PreferencePages
{
    partial class NetworkPreferencePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.invisibleControl5 = new SFMTK.Controls.InvisibleControl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.checkForUpdatesButton = new System.Windows.Forms.Button();
            this.updateStatusLabel = new System.Windows.Forms.Label();
            this.autoUpdateCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.updateRadioGroupPanel = new SFMTK.Controls.RadioGroupPanel();
            this.updateStatusBarRadioButton = new System.Windows.Forms.RadioButton();
            this.updateDialogBoxRadioButton = new System.Windows.Forms.RadioButton();
            this.updateNothingRadioButton = new System.Windows.Forms.RadioButton();
            this.autoDownloadUpdateCheckBox = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.updateCheckComboBox = new System.Windows.Forms.ComboBox();
            this.proxyCheckBox = new System.Windows.Forms.CheckBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.homepageTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.mainFlowLayoutPanel.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.updateRadioGroupPanel.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainFlowLayoutPanel
            // 
            this.mainFlowLayoutPanel.AutoScroll = true;
            this.mainFlowLayoutPanel.Controls.Add(this.invisibleControl5);
            this.mainFlowLayoutPanel.Controls.Add(this.groupBox6);
            this.mainFlowLayoutPanel.Controls.Add(this.proxyCheckBox);
            this.mainFlowLayoutPanel.Controls.Add(this.panel6);
            this.mainFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.mainFlowLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainFlowLayoutPanel.Name = "mainFlowLayoutPanel";
            this.mainFlowLayoutPanel.Size = new System.Drawing.Size(450, 393);
            this.mainFlowLayoutPanel.TabIndex = 3;
            this.mainFlowLayoutPanel.WrapContents = false;
            // 
            // invisibleControl5
            // 
            this.invisibleControl5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.invisibleControl5.Location = new System.Drawing.Point(0, 0);
            this.invisibleControl5.Name = "invisibleControl5";
            this.invisibleControl5.Size = new System.Drawing.Size(450, 0);
            this.invisibleControl5.TabIndex = 4;
            this.invisibleControl5.TabStop = false;
            this.invisibleControl5.Text = "invisibleControl5";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.checkForUpdatesButton);
            this.groupBox6.Controls.Add(this.updateStatusLabel);
            this.groupBox6.Controls.Add(this.autoUpdateCheckBox);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Location = new System.Drawing.Point(3, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox6.Size = new System.Drawing.Size(444, 273);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Updates";
            // 
            // checkForUpdatesButton
            // 
            this.checkForUpdatesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkForUpdatesButton.Location = new System.Drawing.Point(8, 241);
            this.checkForUpdatesButton.Name = "checkForUpdatesButton";
            this.checkForUpdatesButton.Size = new System.Drawing.Size(132, 23);
            this.checkForUpdatesButton.TabIndex = 3;
            this.checkForUpdatesButton.Text = "&Check for updates";
            this.toolTip.SetToolTip(this.checkForUpdatesButton, "Manually check for updates now");
            this.checkForUpdatesButton.UseVisualStyleBackColor = true;
            this.checkForUpdatesButton.Click += new System.EventHandler(this.checkForUpdatesButton_Click);
            // 
            // updateStatusLabel
            // 
            this.updateStatusLabel.AutoSize = true;
            this.updateStatusLabel.Location = new System.Drawing.Point(8, 180);
            this.updateStatusLabel.Name = "updateStatusLabel";
            this.updateStatusLabel.Size = new System.Drawing.Size(130, 13);
            this.updateStatusLabel.TabIndex = 2;
            this.updateStatusLabel.Text = "Could not get version info!\r\n";
            // 
            // autoUpdateCheckBox
            // 
            this.autoUpdateCheckBox.AutoSize = true;
            this.autoUpdateCheckBox.Checked = global::SFMTK.Modules.Network.Properties.Settings.Default.AutoUpdate;
            this.autoUpdateCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoUpdateCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Modules.Network.Properties.Settings.Default, "AutoUpdate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.autoUpdateCheckBox.Location = new System.Drawing.Point(16, 20);
            this.autoUpdateCheckBox.Name = "autoUpdateCheckBox";
            this.autoUpdateCheckBox.Size = new System.Drawing.Size(114, 17);
            this.autoUpdateCheckBox.TabIndex = 0;
            this.autoUpdateCheckBox.Text = "Automatic updates";
            this.autoUpdateCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tableLayoutPanel1);
            this.groupBox7.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", global::SFMTK.Modules.Network.Properties.Settings.Default, "AutoUpdate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox7.Enabled = global::SFMTK.Modules.Network.Properties.Settings.Default.AutoUpdate;
            this.groupBox7.Location = new System.Drawing.Point(8, 21);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(428, 151);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.updateRadioGroupPanel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.updateCheckComboBox, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(422, 132);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(152, 27);
            this.label7.TabIndex = 0;
            this.label7.Text = "&Check for updates:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // updateRadioGroupPanel
            // 
            this.updateRadioGroupPanel.Controls.Add(this.updateStatusBarRadioButton);
            this.updateRadioGroupPanel.Controls.Add(this.updateDialogBoxRadioButton);
            this.updateRadioGroupPanel.Controls.Add(this.updateNothingRadioButton);
            this.updateRadioGroupPanel.Controls.Add(this.autoDownloadUpdateCheckBox);
            this.updateRadioGroupPanel.Location = new System.Drawing.Point(161, 30);
            this.updateRadioGroupPanel.Name = "updateRadioGroupPanel";
            this.updateRadioGroupPanel.Selected = 0;
            this.updateRadioGroupPanel.Size = new System.Drawing.Size(144, 96);
            this.updateRadioGroupPanel.TabIndex = 3;
            // 
            // updateStatusBarRadioButton
            // 
            this.updateStatusBarRadioButton.AutoSize = true;
            this.updateStatusBarRadioButton.Checked = true;
            this.updateStatusBarRadioButton.Location = new System.Drawing.Point(3, 3);
            this.updateStatusBarRadioButton.Name = "updateStatusBarRadioButton";
            this.updateStatusBarRadioButton.Size = new System.Drawing.Size(130, 17);
            this.updateStatusBarRadioButton.TabIndex = 0;
            this.updateStatusBarRadioButton.TabStop = true;
            this.updateStatusBarRadioButton.Tag = "0";
            this.updateStatusBarRadioButton.Text = "Notify user (&status bar)";
            this.toolTip.SetToolTip(this.updateStatusBarRadioButton, "Notifies you about the update in form of a notification shown on the status bar");
            this.updateStatusBarRadioButton.UseVisualStyleBackColor = true;
            // 
            // updateDialogBoxRadioButton
            // 
            this.updateDialogBoxRadioButton.AutoSize = true;
            this.updateDialogBoxRadioButton.Location = new System.Drawing.Point(3, 26);
            this.updateDialogBoxRadioButton.Name = "updateDialogBoxRadioButton";
            this.updateDialogBoxRadioButton.Size = new System.Drawing.Size(132, 17);
            this.updateDialogBoxRadioButton.TabIndex = 1;
            this.updateDialogBoxRadioButton.Tag = "1";
            this.updateDialogBoxRadioButton.Text = "Notify user (&dialog box)";
            this.toolTip.SetToolTip(this.updateDialogBoxRadioButton, "Notifies you about the update with a modal dialog box, where an action can be dec" +
        "ided");
            this.updateDialogBoxRadioButton.UseVisualStyleBackColor = true;
            // 
            // updateNothingRadioButton
            // 
            this.updateNothingRadioButton.AutoSize = true;
            this.updateNothingRadioButton.Location = new System.Drawing.Point(3, 49);
            this.updateNothingRadioButton.Name = "updateNothingRadioButton";
            this.updateNothingRadioButton.Size = new System.Drawing.Size(77, 17);
            this.updateNothingRadioButton.TabIndex = 2;
            this.updateNothingRadioButton.Tag = "2";
            this.updateNothingRadioButton.Text = "&Do nothing";
            this.toolTip.SetToolTip(this.updateNothingRadioButton, "You will not be notified of an update, but it is still checked for automatically." +
        "\r\nThis way, you will instantly see whether an update is available when manually " +
        "checking for it.");
            this.updateNothingRadioButton.UseVisualStyleBackColor = true;
            // 
            // autoDownloadUpdateCheckBox
            // 
            this.autoDownloadUpdateCheckBox.AutoSize = true;
            this.autoDownloadUpdateCheckBox.Checked = global::SFMTK.Modules.Network.Properties.Settings.Default.AutoDownloadUpdates;
            this.autoDownloadUpdateCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoDownloadUpdateCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Modules.Network.Properties.Settings.Default, "AutoDownloadUpdates", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.autoDownloadUpdateCheckBox.Location = new System.Drawing.Point(3, 72);
            this.autoDownloadUpdateCheckBox.Name = "autoDownloadUpdateCheckBox";
            this.autoDownloadUpdateCheckBox.Size = new System.Drawing.Size(137, 17);
            this.autoDownloadUpdateCheckBox.TabIndex = 3;
            this.autoDownloadUpdateCheckBox.Text = "&Automatically download";
            this.toolTip.SetToolTip(this.autoDownloadUpdateCheckBox, "If checked, starts downloading the update once available, and displays a notifica" +
        "tion as specified above.\r\nThis will not install the update, but simply download " +
        "the file before notifying you.");
            this.autoDownloadUpdateCheckBox.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(3, 30);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 102);
            this.label8.TabIndex = 2;
            this.label8.Text = "&When an update is available:";
            // 
            // updateCheckComboBox
            // 
            this.updateCheckComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.updateCheckComboBox.FormattingEnabled = true;
            this.updateCheckComboBox.Items.AddRange(new object[] {
            "on startup",
            "every day",
            "every week",
            "every month"});
            this.updateCheckComboBox.Location = new System.Drawing.Point(161, 3);
            this.updateCheckComboBox.Name = "updateCheckComboBox";
            this.updateCheckComboBox.Size = new System.Drawing.Size(144, 21);
            this.updateCheckComboBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.updateCheckComboBox, "When to check for updates");
            // 
            // proxyCheckBox
            // 
            this.proxyCheckBox.AutoSize = true;
            this.proxyCheckBox.Checked = global::SFMTK.Modules.Network.Properties.Settings.Default.UseProxy;
            this.proxyCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SFMTK.Modules.Network.Properties.Settings.Default, "UseProxy", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.proxyCheckBox.Location = new System.Drawing.Point(3, 282);
            this.proxyCheckBox.Name = "proxyCheckBox";
            this.proxyCheckBox.Size = new System.Drawing.Size(131, 17);
            this.proxyCheckBox.TabIndex = 1;
            this.proxyCheckBox.Text = "Connect using a &proxy";
            this.toolTip.SetToolTip(this.proxyCheckBox, "If you need to connect to the internet using a proxy or get any issues on connect" +
        "ion, enable this checkbox.\r\nMay cause first network calls to be slower than usua" +
        "l if enabled.");
            this.proxyCheckBox.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.homepageTextBox);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(3, 305);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(444, 23);
            this.panel6.TabIndex = 2;
            // 
            // homepageTextBox
            // 
            this.homepageTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.homepageTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SFMTK.Modules.Network.Properties.Settings.Default, "Homepage", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.homepageTextBox.Location = new System.Drawing.Point(72, 2);
            this.homepageTextBox.Name = "homepageTextBox";
            this.homepageTextBox.Size = new System.Drawing.Size(369, 20);
            this.homepageTextBox.TabIndex = 1;
            this.homepageTextBox.Text = global::SFMTK.Modules.Network.Properties.Settings.Default.Homepage;
            this.toolTip.SetToolTip(this.homepageTextBox, "The homepage for the built-in web browser.");
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 4);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "&Homepage:";
            // 
            // NetworkPreferencePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainFlowLayoutPanel);
            this.Name = "NetworkPreferencePage";
            this.Size = new System.Drawing.Size(450, 393);
            this.mainFlowLayoutPanel.ResumeLayout(false);
            this.mainFlowLayoutPanel.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.updateRadioGroupPanel.ResumeLayout(false);
            this.updateRadioGroupPanel.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel mainFlowLayoutPanel;
        private SFMTK.Controls.InvisibleControl invisibleControl5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button checkForUpdatesButton;
        private System.Windows.Forms.Label updateStatusLabel;
        private System.Windows.Forms.CheckBox autoUpdateCheckBox;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label7;
        private SFMTK.Controls.RadioGroupPanel updateRadioGroupPanel;
        private System.Windows.Forms.RadioButton updateStatusBarRadioButton;
        private System.Windows.Forms.RadioButton updateDialogBoxRadioButton;
        private System.Windows.Forms.RadioButton updateNothingRadioButton;
        private System.Windows.Forms.CheckBox autoDownloadUpdateCheckBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox updateCheckComboBox;
        private System.Windows.Forms.CheckBox proxyCheckBox;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox homepageTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ToolTip toolTip;
    }
}