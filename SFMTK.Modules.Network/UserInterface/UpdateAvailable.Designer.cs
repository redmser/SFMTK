﻿namespace SFMTK.Modules.Network.Forms
{
    partial class UpdateAvailable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateAvailable));
            this.updateButton = new System.Windows.Forms.Button();
            this.remindButton = new SFMTK.Controls.MenuButton();
            this.remindContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.remindMeIn1DayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remindMeIn1WeekToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remindMeIn1MonthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.skipButton = new System.Windows.Forms.Button();
            this.headerLabel = new System.Windows.Forms.Label();
            this.versionLabel = new System.Windows.Forms.Label();
            this.remindContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // updateButton
            // 
            this.updateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.updateButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.updateButton.Image = ((System.Drawing.Image)(resources.GetObject("updateButton.Image")));
            this.updateButton.Location = new System.Drawing.Point(396, 428);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(111, 31);
            this.updateButton.TabIndex = 0;
            this.updateButton.Text = "&Update now";
            this.updateButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.updateButton.UseVisualStyleBackColor = true;
            // 
            // remindButton
            // 
            this.remindButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.remindButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.remindButton.DrawSeparationLine = false;
            this.remindButton.Image = ((System.Drawing.Image)(resources.GetObject("remindButton.Image")));
            this.remindButton.Location = new System.Drawing.Point(264, 428);
            this.remindButton.Menu = this.remindContextMenuStrip;
            this.remindButton.MenuCancelsClick = false;
            this.remindButton.Name = "remindButton";
            this.remindButton.Size = new System.Drawing.Size(128, 31);
            this.remindButton.SplitButton = false;
            this.remindButton.TabIndex = 1;
            this.remindButton.Text = "&Remind me later";
            this.remindButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.remindButton.UseVisualStyleBackColor = true;
            this.remindButton.Click += new System.EventHandler(this.remindMeIn1DayToolStripMenuItem_Click);
            // 
            // remindContextMenuStrip
            // 
            this.remindContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.remindMeIn1DayToolStripMenuItem,
            this.remindMeIn1WeekToolStripMenuItem,
            this.remindMeIn1MonthToolStripMenuItem});
            this.remindContextMenuStrip.Name = "remindContextMenuStrip";
            this.remindContextMenuStrip.Size = new System.Drawing.Size(180, 70);
            // 
            // remindMeIn1DayToolStripMenuItem
            // 
            this.remindMeIn1DayToolStripMenuItem.Name = "remindMeIn1DayToolStripMenuItem";
            this.remindMeIn1DayToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.remindMeIn1DayToolStripMenuItem.Text = "Remind me in 1 day";
            this.remindMeIn1DayToolStripMenuItem.Click += new System.EventHandler(this.remindMeIn1DayToolStripMenuItem_Click);
            // 
            // remindMeIn1WeekToolStripMenuItem
            // 
            this.remindMeIn1WeekToolStripMenuItem.Name = "remindMeIn1WeekToolStripMenuItem";
            this.remindMeIn1WeekToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.remindMeIn1WeekToolStripMenuItem.Text = "Remind me in 1 week";
            this.remindMeIn1WeekToolStripMenuItem.Click += new System.EventHandler(this.remindMeIn1WeekToolStripMenuItem_Click);
            // 
            // remindMeIn1MonthToolStripMenuItem
            // 
            this.remindMeIn1MonthToolStripMenuItem.Name = "remindMeIn1MonthToolStripMenuItem";
            this.remindMeIn1MonthToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.remindMeIn1MonthToolStripMenuItem.Text = "Remind me in 1 month";
            this.remindMeIn1MonthToolStripMenuItem.Click += new System.EventHandler(this.remindMeIn1MonthToolStripMenuItem_Click);
            // 
            // skipButton
            // 
            this.skipButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.skipButton.DialogResult = System.Windows.Forms.DialogResult.Ignore;
            this.skipButton.Image = ((System.Drawing.Image)(resources.GetObject("skipButton.Image")));
            this.skipButton.Location = new System.Drawing.Point(12, 428);
            this.skipButton.Name = "skipButton";
            this.skipButton.Size = new System.Drawing.Size(124, 31);
            this.skipButton.TabIndex = 3;
            this.skipButton.Text = "&Skip this version";
            this.skipButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.skipButton.UseVisualStyleBackColor = true;
            // 
            // headerLabel
            // 
            this.headerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.headerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headerLabel.Location = new System.Drawing.Point(16, 12);
            this.headerLabel.Name = "headerLabel";
            this.headerLabel.Size = new System.Drawing.Size(492, 28);
            this.headerLabel.TabIndex = 4;
            this.headerLabel.Text = "A new version of SFMTK is available!";
            // 
            // versionLabel
            // 
            this.versionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.Location = new System.Drawing.Point(16, 40);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(492, 76);
            this.versionLabel.TabIndex = 5;
            this.versionLabel.Text = "Latest version: {0}\r\nInstalled version: {1}\r\n\r\nWhat\'s changed:";
            // 
            // UpdateAvailable
            // 
            this.AcceptButton = this.updateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.remindButton;
            this.ClientSize = new System.Drawing.Size(517, 471);
            this.Controls.Add(this.versionLabel);
            this.Controls.Add(this.headerLabel);
            this.Controls.Add(this.skipButton);
            this.Controls.Add(this.remindButton);
            this.Controls.Add(this.updateButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(396, 280);
            this.Name = "UpdateAvailable";
            this.Text = "Update Available";
            this.remindContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button updateButton;
        private SFMTK.Controls.MenuButton remindButton;
        private System.Windows.Forms.ContextMenuStrip remindContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem remindMeIn1DayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remindMeIn1WeekToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remindMeIn1MonthToolStripMenuItem;
        private System.Windows.Forms.Button skipButton;
        private System.Windows.Forms.Label headerLabel;
        private System.Windows.Forms.Label versionLabel;
    }
}