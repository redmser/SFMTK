﻿using SFMTK.Controls.PreferencePages;
using SFMTK.Modules.Network.Properties;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SFMTK.Modules.Network.Controls.PreferencePages
{
    public partial class NetworkPreferencePage : PreferencePage
    {
        //NYI: NetworkPreferencePage - use CheckForUpdatesCommand for button
        
        public NetworkPreferencePage()
        {
        }

        public override string Path => "Environment/Network & Updates";

        public override System.Drawing.Image Image => Resources.world;

        public override Control ExtendControl => this.mainFlowLayoutPanel;

        public override void OnFirstSelect(object sender, EventArgs e)
        {
            InitializeComponent();

            var updateavailablebinding = new SFMTK.Controls.CastBinding<UpdateAvailableMode, int>
                ("Selected", Settings.Default, "UpdateAvailable", DataSourceUpdateMode.OnPropertyChanged);
            this.updateRadioGroupPanel.DataBindings.Add(updateavailablebinding);
            var updatecheckbinding = new SFMTK.Controls.CastBinding<UpdateCheckFrequency, int>
                ("SelectedIndex", Settings.Default, "UpdateCheck", DataSourceUpdateMode.OnPropertyChanged);
            this.updateCheckComboBox.DataBindings.Add(updatecheckbinding);
            
            //Needs to be initialized due to method calls and cached values
            this.UpdateUpdateLabel();

            base.OnFirstSelect(sender, e);
        }

        private async void checkForUpdatesButton_Click(object sender, EventArgs e)
        {
            if (Settings.Default.CachedUpdateInfo.UpdateAvailable)
            {
                //NYI: NetworkPreferencePage - Install updates
                throw new NotImplementedException();
            }
            else
            {
                //Check for updates
                this.UpdateUpdateLabel("Checking for updates...");
                this.checkForUpdatesButton.Enabled = false;

                try
                {
                    var task = Task.Factory.StartNew(() => UpdateInfo.DoUpdateCheck(UpdateInitiationMode.Manual));
                    await task;

                    //Update label with update info!
                    this.UpdateUpdateLabel();
                }
                catch (Exception ex)
                {
                    //Update label with update info!
                    this.UpdateUpdateLabel("Unable to check for updates.");

                    //Error occurred :(
                    UpdateInfo.ShowErrorMessage(ex);
                }
                finally
                {
                    this.checkForUpdatesButton.Enabled = true;
                }
            }
        }

        /// <summary>
        /// Updates the update label (heh) to reflect changes in known latest version and similar.
        /// </summary>
        private void UpdateUpdateLabel(string addtext = "")
        {
            var last = "Never checked for updates before.";
            if (Settings.Default.CachedUpdateInfo.LastChecked != DateTime.MinValue)
            {
                //Update to correct last version info
                //TODO: NetworkPreferencePage - update status info should say "last checked" as a relative time, with as small units as possible (1 hour ago etc)!! -> theres a library for that
                last = $"Latest version: {Settings.Default.CachedUpdateInfo.LatestVersion} (last checked {Settings.Default.CachedUpdateInfo.LastChecked})";
            }

            if (!string.IsNullOrEmpty(addtext))
                addtext += "\n\n";
            else
            {
                if (Settings.Default.CachedUpdateInfo.UpdateAvailable)
                {
                    addtext = "A new version is available!\n\n";
                    this.checkForUpdatesButton.Text = "Install update";
                }
                else
                {
                    addtext = "You are running the latest version.\n\n";
                }
            }

            this.updateStatusLabel.Text = $"{addtext}Your version: {UpdateInfo.CurrentVersion}\n{last}";
        }
    }
}
