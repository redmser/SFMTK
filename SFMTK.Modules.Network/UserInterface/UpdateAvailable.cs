﻿using System.Windows.Forms;

namespace SFMTK.Modules.Network.Forms
{
    /// <summary>
    /// The dialog to display when a new version of the software is available.
    /// </summary>
    /// <remarks>The DialogResult "Ignore" is used for the "Skip this version" option.</remarks>
    public partial class UpdateAvailable : Form, IThemeable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateAvailable"/> class.
        /// </summary>
        public UpdateAvailable() : this(null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateAvailable"/> class.
        /// </summary>
        /// <param name="latestUpdate">The latest update information entry.</param>
        /// <param name="installedVersion">The installed version number. Use null to detect.</param>
        public UpdateAvailable(UpdateInfoEntry latestUpdate, string installedVersion = null)
        {
            //Compiler-generated
            InitializeComponent();

            //Update version info
            if (installedVersion == null)
                installedVersion = UpdateInfo.CurrentVersion.ToString();
            this.versionLabel.Text = string.Format(this.versionLabel.Text, latestUpdate.ToString(), installedVersion);

            //Form loaded
            WindowManager.AfterCreateForm(this);
        }

        /// <summary>
        /// Gets or sets the theme currently active on this control. This property is never set to the same value that it already has.
        /// </summary>
        public string ActiveTheme
        {
            get => this._activeTheme;
            set
            {
                if (this._activeTheme == value)
                    return;

                this._activeTheme = value;
                this.headerLabel.Font = new System.Drawing.Font(UITheme.SelectedTheme.StatusTextFont, System.Drawing.FontStyle.Bold);
                this.versionLabel.Font = UITheme.SelectedTheme.StatusTextFont;
            }
        }
        private string _activeTheme;

        /// <summary>
        /// Gets the amount of days that the "Remind me later action" should delay this update notification for.
        /// </summary>
        public int RemindLaterDays { get; private set; }

        private void remindMeIn1DayToolStripMenuItem_Click(object sender, System.EventArgs e) => RemindMeLater(1);

        private void remindMeIn1WeekToolStripMenuItem_Click(object sender, System.EventArgs e) => RemindMeLater(7);

        private void remindMeIn1MonthToolStripMenuItem_Click(object sender, System.EventArgs e) => RemindMeLater(30);

        private void RemindMeLater(int days)
        {
            this.RemindLaterDays = days;
            this.Close();
        }
    }
}
