﻿using Newtonsoft.Json.Linq;
using SFMTK.Modules.Network.Properties;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace SFMTK.Modules.Network.Controls
{
    public class SearchToolStrip : ToolStrip
    {
        public SearchToolStrip() : base()
        {
            //Initialize components
            this._searchButton = new ToolStripButton(SFMTK.Properties.Resources.magnifier);
            this._searchButton.Click += this.searchButton_Click;
            this._searchButton.ToolTipText = "Search in Valve Developer Community";
            this._searchButton.Name = "searchButton";

            this._searchTextBox = new ToolStripTextBox();
            this._searchTextBox.KeyUp += this.searchTextBox_KeyUp;
            this._searchTextBox.Click += this.searchTextBox_Click;
            this._searchTextBox.TextChanged += this.searchTextBox_TextChanged;
            this._searchTextBox.Name = "searchTextBox";
            this._searchTextBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            this._searchTextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;

            this.Items.Add(this._searchTextBox);
            this.Items.Add(this._searchButton);
            this.Text = "Help";
            this.Name = "helpToolStrip";
            this.Dock = DockStyle.None;
        }

        private ToolStripButton _searchButton;
        private ToolStripTextBox _searchTextBox;

        /// <summary>
        /// Updates the search suggestions list for the VDC search (help toolbar).
        /// </summary>
        /// <param name="query">The query to look up.</param>
        private async void UpdateSearchSuggestions(string query)
        {
            //UNTESTED: Network.SearchToolStrip UpdateSearchSuggestions without internet connection

            //Since the list of entries gets even more populated only by *adding* characters, and in order to avoid spamming the page with web requests, the following is done:
            //  - If string length is short enough, check list of cached queries
            //  - If already checked for this string, show cached result
            //  - Else return from the web request

            //Don't update if too long query since it is unlikely to add new results
            if (query.Length > 4)
            {
                ShowCachedResults();
                return;
            }

            //Check query list
            var fatquery = query.ToUpperInvariant();
            if (Settings.Default.CachedVDCQueries.Contains(fatquery))
            {
                ShowCachedResults();
                return;
            }

            //Check if can start another request
            if (NetworkModule.SearchSuggestionClientBusy)
                return;

            //Get new results
            Settings.Default.CachedVDCQueries.Add(fatquery);

            var suggestions = await Task.Factory.StartNew(() =>
            {
                //TODO: Network.SearchToolStrip - autocomplete list should show entries with spaces in it even if user is entering underscores (for example for entity names)
                //  Currently using a cheap workaround to even show them at all

                //Get string response
                NetworkModule.SearchSuggestionClientBusy = true;
                var txt = NetworkModule.SearchSuggestionClient.DownloadString(NetworkConstants.VDCSuggestURL + HttpUtility.UrlEncode(query.Replace("_", "")));
                NetworkModule.SearchSuggestionClientBusy = false;
                var suggarr = JArray.Parse(txt).Last;
                return suggarr.Values<string>();
            });

            //Apply list of suggestions to autocomplete list
            ApplySearchSuggestions(suggestions);



            void ShowCachedResults()
            {
                this._searchTextBox.AutoCompleteCustomSource.Clear();
                this._searchTextBox.AutoCompleteCustomSource.AddRange(Settings.Default.CachedVDCResults.ToArray());
            }
        }

        /// <summary>
        /// Applies the specified search suggestions list to the auto completes list.
        /// </summary>
        /// <param name="suggestions">The suggestions to apply.</param>
        private void ApplySearchSuggestions(IEnumerable<string> suggestions)
        {
            //Only add new suggestions
            var newsus = new List<string>();
            foreach (var s in suggestions)
            {
                if (!this._searchTextBox.AutoCompleteCustomSource.Contains(s))
                {
                    newsus.Add(s);
                    Settings.Default.CachedVDCResults.Add(s);
                }
            }

            if (newsus.Count == 0)
                return;

            //Add to list!
            this._searchTextBox.AutoCompleteCustomSource.AddRange(newsus.ToArray());
        }

        private void searchTextBox_TextChanged(object sender, EventArgs e)
        {
            var query = this._searchTextBox.Text;

            if (string.IsNullOrWhiteSpace(query))
            {
                //NYI: Network.SearchToolStrip - Show recently entered queries - or if clicked (can this even be done with the autocomplete list? since sth has to be entered for it to show up, it seems)
            }
            else
            {
                //Show suggestions for entered query
                this.UpdateSearchSuggestions(query);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            var query = this._searchTextBox.Text;

            if (string.IsNullOrWhiteSpace(query))
            {
                //No query, simply open VDC
                this.valveDeveloperCommunityToolStripMenuItem_Click(this, EventArgs.Empty);
            }
            else
            {
                //Open browser with search results
                NetworkHelper.ShowWebsite(NetworkConstants.VDCSearchURL + HttpUtility.UrlEncode(query));
            }
        }

        private void searchTextBox_Click(object sender, EventArgs e)
        {
            //Auto-select search box if no selection
            if (this._searchTextBox.SelectionLength == 0)
                this._searchTextBox.SelectAll();
        }

        private void searchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //If pressing enter key, same as pressing search button
            if (e.KeyCode == Keys.Enter)
                this.searchButton_Click(this, EventArgs.Empty);
        }

        private void valveDeveloperCommunityToolStripMenuItem_Click(object sender, EventArgs e) => NetworkHelper.ShowWebsite(NetworkConstants.VDCMainURL);

        private void checkForUpdatesToolStripMenuItem_Click(object sender, EventArgs e) => UpdateHelper.CheckForUpdates(UpdateInitiationMode.Manual);
    }
}
