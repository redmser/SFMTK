﻿namespace SFMTK.Modules.Network.Controls
{
    partial class AdditionalExperimentalOptions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new System.Windows.Forms.Panel();
            this.gitlabTokenTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.gitlabTokenTextBox);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(293, 23);
            this.panel4.TabIndex = 5;
            // 
            // gitlabTokenTextBox
            // 
            this.gitlabTokenTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gitlabTokenTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SFMTK.Modules.Network.Properties.Settings.Default, "AuthToken", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.gitlabTokenTextBox.Location = new System.Drawing.Point(100, 2);
            this.gitlabTokenTextBox.Name = "gitlabTokenTextBox";
            this.gitlabTokenTextBox.Size = new System.Drawing.Size(192, 20);
            this.gitlabTokenTextBox.TabIndex = 1;
            this.gitlabTokenTextBox.Text = global::SFMTK.Modules.Network.Properties.Settings.Default.AuthToken;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "&GitLab auth token:";
            // 
            // AdditionalExperimentalOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel4);
            this.Name = "AdditionalExperimentalOptions";
            this.Size = new System.Drawing.Size(294, 25);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox gitlabTokenTextBox;
        private System.Windows.Forms.Label label9;
    }
}
