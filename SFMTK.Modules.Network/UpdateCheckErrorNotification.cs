﻿using SFMTK.Notifications;
using System;

namespace SFMTK.Modules.Network.Notifications
{
    /// <summary>
    /// A notification that pops up when an update check failed.
    /// </summary>
    [Serializable]
    public class UpdateCheckErrorNotification : Notification
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateCheckErrorNotification"/> class.
        /// </summary>
        /// <param name="error">The update error that occurred.</param>
        public UpdateCheckErrorNotification(Exception error) : base("Update check error", NotificationColor.Red, 5)
        {
            this.Exception = error;
        }

        /// <summary>
        /// Gets or sets the update error that occurred.
        /// </summary>
        public Exception Exception { get; set; }

        /// <summary>
        /// Called when the notification was clicked. Returns whether the notification should be removed after exiting the method.
        /// </summary>
        public override bool OnClick()
        {
            //Show specified update error
            UpdateInfo.ShowErrorMessage(this.Exception);
            return true;
        }
    }
}
