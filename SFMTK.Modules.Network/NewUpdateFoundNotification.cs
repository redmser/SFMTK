﻿using System;
using SFMTK.Notifications;

namespace SFMTK.Modules.Network.Notifications
{
    /// <summary>
    /// A notification for finding a new update to install.
    /// </summary>
    [Serializable]
    public class NewUpdateFoundNotification : Notification
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NewUpdateFoundNotification"/> class.
        /// </summary>
        public NewUpdateFoundNotification() : base(Properties.Resources.UpdateFoundStatusLabel, NotificationColor.Green, 10)
        {

        }

        /// <summary>
        /// Called when the notification was clicked. Returns whether the notification should be removed after exiting the method.
        /// </summary>
        public override bool OnClick()
        {
            //Found the update
            UpdateInfo.UpdateAvailableDialog();
            return true;
        }
    }
}
