﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using GitlabVersioning;
using SFMTK.Forms;
using SFMTK.Modules.Network.Properties;

namespace SFMTK.Modules.Network
{
    /// <summary>
    /// Storage for all information of the known updates and versions of SFMTK.
    /// </summary>
    [Serializable]
    public class UpdateInfo : IReadOnlyList<UpdateInfoEntry>
    {
        //Notes regarding the update system:
        //  - The personal access token is not included in the public source code. If you wish to compile SFMTK with API functionality:
        //      - For testing, use the experimental option "GitLab auth token" - this saves the token in plaintext inside of %LOCALAPPDATA%/SFMTK/.../user.config!
        //      - Embed the token as a resource by creating a file named TOKEN in the project folder and setting its Build Action to Embedded Resource
        //  - The release notes of each tag have a strict format to follow in order to be recognized by the updating system. Check the contribution guide on GitLab for more info!
        //FIXME: Contribution guide is not yet set up!

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateInfo"/> class.
        /// </summary>
        public UpdateInfo() { }

        protected readonly List<UpdateInfoEntry> backingList = new List<UpdateInfoEntry>();

        /// <summary>
        /// Gets the latest known update info of SFMTK, or null if none are stored.
        /// </summary>
        public UpdateInfoEntry LatestVersion => this.FirstOrDefault();

        /// <summary>
        /// Gets the current update info of SFMTK.
        /// </summary>
        public static UpdateInfoEntry CurrentVersion => new UpdateInfoEntry(FileVersionInfo.GetVersionInfo(Reflection.SFMTKAssembly.Location).ProductVersion);

        /// <summary>
        /// Gets or sets when the program last checked for updates (only successful update checks), or DateTime.MinValue if never checked before.
        /// </summary>
        public DateTime LastChecked { get; set; } = DateTime.MinValue;

        /// <summary>
        /// Gets whether an update is available (a newer version than the CurrentVersion is installed). This value is cached, updated when calling UpdateEntries.
        /// </summary>
        public bool UpdateAvailable { get; private set; } = false;

        /// <summary>
        /// Gets the number of elements in the collection.
        /// </summary>
        public int Count => this.backingList.Count;

        /// <summary>
        /// Gets the <see cref="UpdateInfoEntry"/> at the specified index.
        /// </summary>
        /// <value>
        /// The <see cref="UpdateInfoEntry"/>.
        /// </value>
        /// <param name="index">The index.</param>
        public UpdateInfoEntry this[int index] => this.backingList[index];

        /// <summary>
        /// Initiates an update check.
        /// </summary>
        /// <param name="mode">How the check was initiated.</param>
        public static void DoUpdateCheck(UpdateInitiationMode mode)
        {
             //Update info
             Settings.Default.CachedUpdateInfo.UpdateEntries();
             
             //NYI: Main - download release if an update is available (show progress in status bar)
             if (Settings.Default.AutoDownloadUpdates && Settings.Default.CachedUpdateInfo.UpdateAvailable)
             {
                 throw new NotImplementedException();
             }
        }

        /// <summary>
        /// Shows an error message for when updating failed due to a certain reason.
        /// </summary>
        /// <param name="ex">The exception thrown by any step of the update procedure.</param>
        public static void ShowErrorMessage(Exception ex)
        {
            var skipextra = false;
            string msg = null;
            if (ex is MissingMemberException)
                msg = "The servers responded with no version list. This may be because you aren't using an official version of SFMTK.";
            else if (ex is InvalidDataException)
            {
                //Copy message over since it is helpful for the developer
                skipextra = true;
                msg = ex.Message;
            }
            else if (ex is WebException wx)
            {
                //Multiple causes for the WebException
                if (wx.Response is HttpWebResponse res)
                {
                    if (res.StatusCode == HttpStatusCode.OK)
                        msg = "The servers returned information that could not be processed.";
                    else if (res.StatusCode == HttpStatusCode.NotFound) //FIXME: UpdateInfo - this should be avoided, and instead retried with an API-gotten URL
                        msg = "Could not find the project at its regular URL. It might have moved (try looking for it and updating manually).";
                    else if (res.StatusCode == HttpStatusCode.Unauthorized)
                        msg = "The servers did not allow us to connect, since we do not have permission. You can either specify a personal access token yourself, or simply update manually.";
                    else
                        msg = $"The server responded with the unexpected error code {(int)res.StatusCode} ({res.StatusCode})";
                }
                else
                    msg = "Unexpected answer from server.";
            }
            else
                //Throw since we are not sure what happened
                ErrorReport.Display("Update service failed.", ex);

            if (msg != null)
                FallbackRememberDialog.ShowDialogLog(msg, "Could not update!", "Error while updating", skipextra ? null : "Internal error message: " + ex.Message,
                    "Do not notify again", "Update Check Error", FallbackDialogIcon.Error, new FallbackDialogButton(System.Windows.Forms.DialogResult.OK));
        }

        /// <summary>
        /// Shows a dialog for an update to SFMTK being available.
        /// </summary>
        public static void UpdateAvailableDialog()
        {
            var updatediag = new Forms.UpdateAvailable(Settings.Default.CachedUpdateInfo.LatestVersion);
            switch (updatediag.ShowDialog())
            {
                //NYI: Main - update available options
                case System.Windows.Forms.DialogResult.OK:
                    //Install update
                    break;
                case System.Windows.Forms.DialogResult.Cancel:
                    //Remind me later, using updatediag.RemindLaterDays
                    break;
                case System.Windows.Forms.DialogResult.Ignore:
                    //Ignore this version until another version is out
                    break;
            }
        }

        /// <summary>
        /// Updates the contents of this UpdateInfo with the latest list of updates.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Unable to retrieve update information.</exception>
        /// <exception cref="System.MissingMemberException">The project does not have any tags.</exception>
        /// <exception cref="InvalidDataException">In order to work with GitLab authentification, you must provide your own personal access token.</exception>
        /// <exception cref="WebException">The servers returned an invalid response.</exception>
        public void UpdateEntries()
        {
            //Get tag list
            var taglist = GetUpdateInfo();

            //Check for contents
            if (!taglist.IsInitialized)
            {
                //TODO: UpdateInfo - error handling
                throw taglist.LastError ?? new InvalidOperationException("Unable to retrieve update information.");
            }

            //No tags
            if (taglist.Count == 0)
                throw new MissingMemberException("The project does not have any tags (API returned empty array).");

            //Update entries
            this.backingList.Clear();
            foreach (var tag in taglist)
            {
                var version = ParseVersion(tag.TagName);
                var entry = new UpdateInfoEntry();
                entry.Version = version;
                entry.Summary = tag.Description;
                foreach (var ch in tag.Changes)
                {
                    entry.Changes.Add(ch.Category, ch.Text);
                }
                foreach (var rel in tag.Releases)
                {
                    entry.Releases.Add(rel.Name, rel.Url);
                }
                this.backingList.Add(entry);
            }

            //Check if update available
            var curr = UpdateInfo.CurrentVersion.Version;
            this.UpdateAvailable = this.Any(u => u.Version > curr);
            this.LastChecked = DateTime.Now;



            Version ParseVersion(string ver)
            {
                if (char.ToUpperInvariant(ver[0]) == 'V')
                    ver = ver.Substring(1);
                var split = ver.Split('.').Select(s => int.Parse(s)).ToList();
                return new Version(split[0], split[1]);
            }
        }

        /// <summary>
        /// Gets the update information from an API lookup. Result should be cached as a List of UpdateInfoEntry.
        /// <para />Use IsInitialized and LastError to check for results.
        /// </summary>
        private static TagList GetUpdateInfo()
        {
            //Do API lookup
            var taglist = new TagList(NetworkConstants.GitLabId, GetAuthToken());
            taglist.Proxy = NetworkHelper.GetProxy();
            taglist.ProjectUrl = GetProjectURL(false);
            taglist.PreloadAbsoluteUrl = false;
            taglist.LoadTags();
            return taglist;
        }

        /// <summary>
        /// Gets the GitLab project URL of SFMTK. May return null if the lookup failed.
        /// </summary>
        /// <param name="lookup">If set to <c>true</c>, does a (blocking) API lookup. Only use once the constant value proves to be invalid.</param>
        public static string GetProjectURL(bool lookup)
        {
            //TODO: UpdateInfo - automatically check for 404 / redirect to main page / whatever happens, and try with API
            //  (if API failed, retry next time, but if still errors, then sth is wrong!)
            if (lookup)
            {
                try
                {
                    return GetProjectURL(NetworkConstants.GitLabId);
                }
                catch (Exception)
                {
                    //Return null to signify lookup failed
                    return null;
                }
            }
            return Constants.GitLabURL;
        }

        /// <summary>
        /// Gets the project URL of the specified GitLab project ID (not the IID!). Blocks calling thread until a response from the server.
        /// </summary>
        /// <param name="projectid">The project ID.</param>
        /// <exception cref="System.InvalidOperationException">Invalid state trying to get project URL</exception>
        private static string GetProjectURL(int projectid)
        {
            //Use API lookup
            var templist = new TagList(projectid, GetAuthToken());
            templist.Proxy = NetworkHelper.GetProxy();
            templist.PreloadAbsoluteUrl = false;
            if (templist.LoadProjectInfo())
            {
                //Successfully loaded info!
                return templist.ProjectUrl;
            }

            //Not successful, fallback
            throw templist.LastError ?? new InvalidOperationException("Invalid state trying to get project URL");
        }

        /// <summary>
        /// Returns the authentification token to use for any GitLab API calls.
        /// </summary>
        /// <exception cref="InvalidDataException">In order to work with GitLab authentification, you must provide your own personal access token.</exception>
        [SecurityCritical]
        private static string GetAuthToken()
        {
            //Return override token
            if (!string.IsNullOrEmpty(Settings.Default.AuthToken))
                return Settings.Default.AuthToken;

            try
            {
                var content = Reflection.SFMTKAssembly.GetManifestResourceStream("SFMTK.TOKEN");
                if (content == null)
                    throw new FileNotFoundException("The resource stream SFMTK.TOKEN was not found.");
                using (var read = new StreamReader(content))
                    return read.ReadLine();
            }
            catch (FileNotFoundException ex)
            {
                //Needs TOKEN file in project directory when compiled
                throw new InvalidDataException("In order to work with GitLab authentification, you must provide your own personal access token." +
                    "\n\nThis can be done by either adding a file named TOKEN to the project directory, or by using the experimental setting as an override.", ex);
            }
        }

        public IEnumerator<UpdateInfoEntry> GetEnumerator() => ((IReadOnlyList<UpdateInfoEntry>)this.backingList).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => ((IReadOnlyList<UpdateInfoEntry>)this.backingList).GetEnumerator();
    }

    /// <summary>
    /// Needed information about a specific update.
    /// </summary>
    [Serializable]
    public class UpdateInfoEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateInfoEntry"/> class.
        /// </summary>
        public UpdateInfoEntry() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateInfoEntry"/> class.
        /// </summary>
        /// <param name="version">The version of this update.</param>
        public UpdateInfoEntry(Version version) : this()
        {
            this.Version = version;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateInfoEntry"/> class.
        /// </summary>
        /// <param name="version">The version.</param>
        public UpdateInfoEntry(string version) : this(new Version(version)) { }

        /// <summary>
        /// The version of this update. Does not have to contain the revision.
        /// </summary>
        public Version Version { get; set; }

        /// <summary>
        /// Summarized changes of this update.
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// Complete list of changes in this update. Key is the category, value is the display text.
        /// </summary>
        public Dictionary<string, string> Changes { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Complete list of releases for this update. Key is the identifier, value is the absolute URL for downloading.
        /// </summary>
        public Dictionary<string, string> Releases { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance's version and build number.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance's version and build number.
        /// </returns>
        public override string ToString() => $"{this.Version.Major}.{this.Version.Minor} (build #{this.Version.Build})";
    }

    /// <summary>
    /// How an update check was initiated. Changes the feedback the user gets based on what is expected.
    /// </summary>
    public enum UpdateInitiationMode
    {
        /// <summary>
        /// A manually started update check. Since the user expects feedback, provide most visible responses.
        /// </summary>
        Manual,
        /// <summary>
        /// An automatically started update check. Since the user may be working at this time, have less blocking responses (for error handling).
        /// </summary>
        Automatic,
        /// <summary>
        /// The initial update check when doing the first launch. Since the user does not expect any update check at all, silently notify if there is no update (since it is expected).
        /// </summary>
        Initial
    }
}
