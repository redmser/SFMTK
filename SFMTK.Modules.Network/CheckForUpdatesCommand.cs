﻿using SFMTK.Commands;

namespace SFMTK.Modules.Network.Commands
{
    public class CheckForUpdatesCommand : Command
    {
        public CheckForUpdatesCommand() : base()
        {
            this.Name = "&Check for Updates";
            this.Icon = SFMTK.Properties.Resources.arrow_refresh;
        }

        public override string ToolTipText => "Manually checks for software updates";
        public override string Category => "Help";

        public override bool CanBeExecuted(CommandExecuteContext context) => _allowExecute;

        public override string Execute()
        {
            //Start an update check
            _allowExecute = false;
            UpdateEnabled();
            UpdateHelper.CheckForUpdates(UpdateInitiationMode.Manual).ContinueWith((t) => { _allowExecute = true; UpdateEnabled(); });
            return null;

            void UpdateEnabled() => CommandManager.UpdateCommandsEnabled(nameof(CheckForUpdatesCommand), typeof(CheckForUpdatesCommand));
        }

        private static bool _allowExecute = true;
    }
}
